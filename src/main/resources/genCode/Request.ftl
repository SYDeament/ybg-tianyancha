package ${parent};
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
#foreach ($subColmonPackagePath in $subColmonPackagePaths)
import $subColmonPackagePath;
#end
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*$!{tianyancha.fname}
* $!{tianyancha.fdesc}
* $!{tianyancha.openUrl}
*@author ${author}
**/

public interface ${className}Request extends BaseRequest<${className}DTO ,${className}>{

}

