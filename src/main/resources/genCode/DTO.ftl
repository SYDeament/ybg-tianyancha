package ${parent};
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*$!{tianyancha.fname}
* $!{tianyancha.fdesc}
* $!{tianyancha.openUrl}
*@author ${author}
**/

@TYCURL(value="$!{tianyancha.openUrl}")
public class ${className}DTO implements Serializable{

#foreach ($column in $columns)
    /**
     *    $column.remark
     *
    **/
    @ParamRequire(require = $column.require)
    private $column.type $column.name;
#end


#foreach ($column in $columns)
    /**
    *   设置 $column.remark
    **/
    public void set${column.attrName}($column.type $column.name) {
      this.$column.name = $column.name;
    }
    /**
    *   获取 $column.remark
    **/
    public $column.type get${column.attrName}() {
      return $column.name;
    }
#end



}

