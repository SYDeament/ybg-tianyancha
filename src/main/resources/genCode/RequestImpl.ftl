package ${parent};
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
#foreach ($subColmonPackagePath in $subColmonPackagePaths)
import $subColmonPackagePath;
#end
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*$!{tianyancha.fname}
* $!{tianyancha.fdesc}
* $!{tianyancha.openUrl}
*@author ${author}
**/
@Component("${className2}RequestImpl")
public class ${className}RequestImpl extends BaseRequestImpl<${className},${className}DTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"$!{tianyancha.openUrl}";
    }
}

