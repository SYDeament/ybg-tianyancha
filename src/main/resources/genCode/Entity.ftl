package ${parent};
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
#foreach ($subColmonPackagePath in $subColmonPackagePaths)
import $subColmonPackagePath;
#end
/**
*$!{tianyancha.fname}
* $!{tianyancha.fdesc}
* $!{tianyancha.openUrl}
*@author ${author}
**/

public class ${className} implements Serializable{

#foreach ($column in $columns)
    /**
     *    $column.remark
    **/
    @JSONField(name="$column.name")
    private $column.type $column.name;
#end


#foreach ($column in $columns)
    /**
    *   设置 $column.remark
    **/
    public void set${column.attrName}($column.type $column.name) {
      this.$column.name = $column.name;
    }
    /**
    *   获取 $column.remark
    **/
    public $column.type get${column.attrName}() {
      return $column.name;
    }
#end



}

