package com.gitee.deament.tianyancha.config;

import com.gitee.deament.tianyancha.core.HttpRequest;
import com.gitee.deament.tianyancha.core.aop.HttpRequestAop;
import com.gitee.deament.tianyancha.core.aop.ParamCheckAop;
import com.gitee.deament.tianyancha.core.aop.TianyanchaCacheAop;
import com.gitee.deament.tianyancha.core.base.HttpRequestI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author deament
 */
@Configuration
public class HttpConfig {

    /**
     * 配置请求类
     *
     * @param token
     * @return
     */
    @Bean(value = "httpRequest")
    public HttpRequestI httpRequest(@Value("${tianyancha.token}") String token) {
        return new HttpRequest(token);
    }


    /**
     * 配置天眼查生效的切面（Demo）
     */
    @Bean(value = "httpRequestAop")
    public HttpRequestAop httpRequestAop() {
        return new HttpRequestAop();
    }

    /**
     * 配置天眼查缓存的切面
     */

    @Bean(value = "tianyanchaRequestAop")
    public TianyanchaCacheAop tianyanchaRequestAop() {
        return new TianyanchaCacheAop();
    }

    /**
     * 配置天眼查参数校验的切面(最先拦截)
     * @return
     */
    @Bean(value = "paramCheckAop")
    public ParamCheckAop paramCheckAop() {
        return new ParamCheckAop();
    }

}
