package com.gitee.deament.tianyancha.core.remote.mrcreditchina.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrcreditchina.entity.MrCreditChina;
import com.gitee.deament.tianyancha.core.remote.mrcreditchina.dto.MrCreditChinaDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*行政处罚-其他来源
* 可以通过公司名称或ID获取企业行政处罚信息，行政处罚信息包括行政处罚明细、行政处罚公告、行政处罚内容、公示日期、处罚依据、处罚状态等字段的详细信息
* /services/open/mr/creditChina/2.0
*@author deament
**/
@Component("mrCreditChinaRequestImpl")
public class MrCreditChinaRequestImpl extends BaseRequestImpl<MrCreditChina,MrCreditChinaDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/creditChina/2.0";
    }
}

