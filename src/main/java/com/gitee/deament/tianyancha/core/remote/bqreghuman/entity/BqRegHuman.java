package com.gitee.deament.tianyancha.core.remote.bqreghuman.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.bqreghuman.entity.BqRegHumanBaseInfo;
import com.gitee.deament.tianyancha.core.remote.bqreghuman.entity.BqRegHumanResult;
import com.gitee.deament.tianyancha.core.remote.bqreghuman.entity.BqRegHumanRegInfo;
/**
*建筑资质-注册人员详情
* 可以通过公司名称或ID获取建筑资质注册人员信息，包括姓名、注册类别、注册号（执业印章号）、注册专业、证书编号等字段的相关信息
* /services/open/bq/regHuman/detail/2.0
*@author deament
**/

public class BqRegHuman implements Serializable{

    /**
     *    基本信息
    **/
    @JSONField(name="baseInfo")
    private BqRegHumanBaseInfo baseInfo;
    /**
     *    执业注册信息
    **/
    @JSONField(name="regInfo")
    private BqRegHumanRegInfo regInfo;


    /**
    *   设置 基本信息
    **/
    public void setBaseInfo(BqRegHumanBaseInfo baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 基本信息
    **/
    public BqRegHumanBaseInfo getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 执业注册信息
    **/
    public void setRegInfo(BqRegHumanRegInfo regInfo) {
      this.regInfo = regInfo;
    }
    /**
    *   获取 执业注册信息
    **/
    public BqRegHumanRegInfo getRegInfo() {
      return regInfo;
    }



}

