package com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity.MrMortgageInfo;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.dto.MrMortgageInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*动产抵押
* 可以通过公司名称或ID获取企业动产抵押公告信息，企业动产抵押公告信息包括被担保债权类型、数额、登记机关等字段信息
* /services/open/mr/mortgageInfo/2.0
*@author deament
**/

public interface MrMortgageInfoRequest extends BaseRequest<MrMortgageInfoDTO ,MrMortgageInfo>{

}

