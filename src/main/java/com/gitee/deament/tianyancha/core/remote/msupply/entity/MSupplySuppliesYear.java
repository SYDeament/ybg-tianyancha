package com.gitee.deament.tianyancha.core.remote.msupply.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*供应商
* 属于MSupply
* /services/open/m/supply/2.0
*@author deament
**/

public class MSupplySuppliesYear implements Serializable{

    /**
     *    年份（数量）
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    年份
    **/
    @JSONField(name="value")
    private String value;


    /**
    *   设置 年份（数量）
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 年份（数量）
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 年份
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 年份
    **/
    public String getValue() {
      return value;
    }



}

