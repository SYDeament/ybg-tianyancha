package com.gitee.deament.tianyancha.core.remote.jrcourtannouncement.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrcourtannouncement.entity.JrCourtAnnouncement;
import com.gitee.deament.tianyancha.core.remote.jrcourtannouncement.dto.JrCourtAnnouncementDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*法院公告
* 可以通过公司名称或ID获取企业法院公告，法院公告信息包括执行法院、案件内容、公告类型、刊登日期、公司名、当事人等字段的详细信息
* /services/open/jr/courtAnnouncement/2.0
*@author deament
**/
@Component("jrCourtAnnouncementRequestImpl")
public class JrCourtAnnouncementRequestImpl extends BaseRequestImpl<JrCourtAnnouncement,JrCourtAnnouncementDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/jr/courtAnnouncement/2.0";
    }
}

