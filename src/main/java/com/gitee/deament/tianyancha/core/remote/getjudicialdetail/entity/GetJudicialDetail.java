package com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity.GetJudicialDetailRemoveFrozen;
import com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity.GetJudicialDetailAssDetail;
import com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity.GetJudicialDetailKeepFrozen;
import com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity.GetJudicialDetailShareholderChange;
import com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity.GetJudicialDetailFrozen;
import com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity.GetJudicialDetailInvalidationFrozen;
/**
*司法协助详情
* 根据司法协助ID获取司法协助详情，判断司法协助类型，包括股权变更、股权冻结、结解除冻结、司法协助续行、股权数额、司法冻结失效及对应的详细信息
* /services/v4/open/getJudicialDetail
*@author deament
**/

public class GetJudicialDetail implements Serializable{

    /**
     *    解除冻结
    **/
    @JSONField(name="removeFrozen")
    private GetJudicialDetailRemoveFrozen removeFrozen;
    /**
     *    司法协助基本详情
    **/
    @JSONField(name="assDetail")
    private GetJudicialDetailAssDetail assDetail;
    /**
     *    司法协助续行
    **/
    @JSONField(name="keepFrozen")
    private GetJudicialDetailKeepFrozen keepFrozen;
    /**
     *    股权变更
    **/
    @JSONField(name="shareholderChange")
    private GetJudicialDetailShareholderChange shareholderChange;
    /**
     *    股权冻结
    **/
    @JSONField(name="frozen")
    private GetJudicialDetailFrozen frozen;
    /**
     *    司法冻结失效
    **/
    @JSONField(name="invalidationFrozen")
    private GetJudicialDetailInvalidationFrozen invalidationFrozen;


    /**
    *   设置 解除冻结
    **/
    public void setRemoveFrozen(GetJudicialDetailRemoveFrozen removeFrozen) {
      this.removeFrozen = removeFrozen;
    }
    /**
    *   获取 解除冻结
    **/
    public GetJudicialDetailRemoveFrozen getRemoveFrozen() {
      return removeFrozen;
    }
    /**
    *   设置 司法协助基本详情
    **/
    public void setAssDetail(GetJudicialDetailAssDetail assDetail) {
      this.assDetail = assDetail;
    }
    /**
    *   获取 司法协助基本详情
    **/
    public GetJudicialDetailAssDetail getAssDetail() {
      return assDetail;
    }
    /**
    *   设置 司法协助续行
    **/
    public void setKeepFrozen(GetJudicialDetailKeepFrozen keepFrozen) {
      this.keepFrozen = keepFrozen;
    }
    /**
    *   获取 司法协助续行
    **/
    public GetJudicialDetailKeepFrozen getKeepFrozen() {
      return keepFrozen;
    }
    /**
    *   设置 股权变更
    **/
    public void setShareholderChange(GetJudicialDetailShareholderChange shareholderChange) {
      this.shareholderChange = shareholderChange;
    }
    /**
    *   获取 股权变更
    **/
    public GetJudicialDetailShareholderChange getShareholderChange() {
      return shareholderChange;
    }
    /**
    *   设置 股权冻结
    **/
    public void setFrozen(GetJudicialDetailFrozen frozen) {
      this.frozen = frozen;
    }
    /**
    *   获取 股权冻结
    **/
    public GetJudicialDetailFrozen getFrozen() {
      return frozen;
    }
    /**
    *   设置 司法冻结失效
    **/
    public void setInvalidationFrozen(GetJudicialDetailInvalidationFrozen invalidationFrozen) {
      this.invalidationFrozen = invalidationFrozen;
    }
    /**
    *   获取 司法冻结失效
    **/
    public GetJudicialDetailInvalidationFrozen getInvalidationFrozen() {
      return invalidationFrozen;
    }



}

