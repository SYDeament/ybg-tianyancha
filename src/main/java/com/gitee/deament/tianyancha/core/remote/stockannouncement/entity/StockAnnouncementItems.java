package com.gitee.deament.tianyancha.core.remote.stockannouncement.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*上市公告
* 属于StockAnnouncement
* /services/open/stock/announcement/2.0
*@author deament
**/

public class StockAnnouncementItems implements Serializable{

    /**
     *    股票简称
    **/
    @JSONField(name="stock_name")
    private String stock_name;
    /**
     *    公司名称
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    股票名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    发布日期
    **/
    @JSONField(name="time")
    private String time;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    股票号
    **/
    @JSONField(name="stock_code")
    private String stock_code;
    /**
     *    uuid
    **/
    @JSONField(name="uuid")
    private String uuid;


    /**
    *   设置 股票简称
    **/
    public void setStock_name(String stock_name) {
      this.stock_name = stock_name;
    }
    /**
    *   获取 股票简称
    **/
    public String getStock_name() {
      return stock_name;
    }
    /**
    *   设置 公司名称
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名称
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 股票名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 股票名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 发布日期
    **/
    public void setTime(String time) {
      this.time = time;
    }
    /**
    *   获取 发布日期
    **/
    public String getTime() {
      return time;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 股票号
    **/
    public void setStock_code(String stock_code) {
      this.stock_code = stock_code;
    }
    /**
    *   获取 股票号
    **/
    public String getStock_code() {
      return stock_code;
    }
    /**
    *   设置 uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 uuid
    **/
    public String getUuid() {
      return uuid;
    }



}

