package com.gitee.deament.tianyancha.core.remote.mcustomer.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcustomer.entity.MCustomer;
import com.gitee.deament.tianyancha.core.remote.mcustomer.dto.MCustomerDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*客户
* 可以通过公司名称或ID获取企业相关客户信息，企业相关客户信息包括客户、销售占比、销售金额、报告期等字段的详细信息
* /services/open/m/customer/2.0
*@author deament
**/

public interface MCustomerRequest extends BaseRequest<MCustomerDTO ,MCustomer>{

}

