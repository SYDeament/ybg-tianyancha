package com.gitee.deament.tianyancha.core.remote.oipublicinvest.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oipublicinvest.entity.OiPublicInvest;
import com.gitee.deament.tianyancha.core.remote.oipublicinvest.dto.OiPublicInvestDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*公开投资事件
* 可以通过投资机构名称获取公开投资产品，包括产品名称、所属公司、参与轮次、投资时间、投资金额、产品介绍等字段的详细信息
* /services/open/oi/publicInvest/2.0
*@author deament
**/
@Component("oiPublicInvestRequestImpl")
public class OiPublicInvestRequestImpl extends BaseRequestImpl<OiPublicInvest,OiPublicInvestDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/oi/publicInvest/2.0";
    }
}

