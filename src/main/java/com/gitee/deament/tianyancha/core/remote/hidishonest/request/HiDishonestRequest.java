package com.gitee.deament.tianyancha.core.remote.hidishonest.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hidishonest.entity.HiDishonest;
import com.gitee.deament.tianyancha.core.remote.hidishonest.dto.HiDishonestDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*历史失信人
* 可以通过公司名称或ID获取企业历史的失信情况，历史失信信息包括失信人名称、组织机构代码、履行情况、失信行为具体情形等字段信息
* /services/open/hi/dishonest/2.0
*@author deament
**/

public interface HiDishonestRequest extends BaseRequest<HiDishonestDTO ,HiDishonest>{

}

