package com.gitee.deament.tianyancha.core.remote.hiicp.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiicp.entity.HiIcp;
import com.gitee.deament.tianyancha.core.remote.hiicp.dto.HiIcpDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史网站备案
* 可以通过公司名称或ID获取历史的网站备案有关信息，历史网站备案信息包括网站名称、网站首页、域名、网站备案/许可证号等字段信息
* /services/open/hi/icp/2.0
*@author deament
**/
@Component("hiIcpRequestImpl")
public class HiIcpRequestImpl extends BaseRequestImpl<HiIcp,HiIcpDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/icp/2.0";
    }
}

