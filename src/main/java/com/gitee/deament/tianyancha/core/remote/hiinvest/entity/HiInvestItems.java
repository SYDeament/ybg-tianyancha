package com.gitee.deament.tianyancha.core.remote.hiinvest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史对外投资
* 属于HiInvest
* /services/open/hi/invest/2.0
*@author deament
**/

public class HiInvestItems implements Serializable{

    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    金额
    **/
    @JSONField(name="amount")
    private Integer amount;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblishTime")
    private Long estiblishTime;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    评分
    **/
    @JSONField(name="pencertileScore")
    private Integer pencertileScore;
    /**
     *    占比
    **/
    @JSONField(name="percent")
    private String percent;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    经营范围
    **/
    @JSONField(name="business_scope")
    private String business_scope;
    /**
     *    公司类型
    **/
    @JSONField(name="orgType")
    private String orgType;
    /**
     *    法人id
    **/
    @JSONField(name="legalPersonId")
    private Integer legalPersonId;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    公司名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    行业
    **/
    @JSONField(name="category")
    private String category;
    /**
     *    法人类型
    **/
    @JSONField(name="personType")
    private Integer personType;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 金额
    **/
    public void setAmount(Integer amount) {
      this.amount = amount;
    }
    /**
    *   获取 金额
    **/
    public Integer getAmount() {
      return amount;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblishTime(Long estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立日期
    **/
    public Long getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 评分
    **/
    public void setPencertileScore(Integer pencertileScore) {
      this.pencertileScore = pencertileScore;
    }
    /**
    *   获取 评分
    **/
    public Integer getPencertileScore() {
      return pencertileScore;
    }
    /**
    *   设置 占比
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 占比
    **/
    public String getPercent() {
      return percent;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 经营范围
    **/
    public void setBusiness_scope(String business_scope) {
      this.business_scope = business_scope;
    }
    /**
    *   获取 经营范围
    **/
    public String getBusiness_scope() {
      return business_scope;
    }
    /**
    *   设置 公司类型
    **/
    public void setOrgType(String orgType) {
      this.orgType = orgType;
    }
    /**
    *   获取 公司类型
    **/
    public String getOrgType() {
      return orgType;
    }
    /**
    *   设置 法人id
    **/
    public void setLegalPersonId(Integer legalPersonId) {
      this.legalPersonId = legalPersonId;
    }
    /**
    *   获取 法人id
    **/
    public Integer getLegalPersonId() {
      return legalPersonId;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 公司名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 行业
    **/
    public void setCategory(String category) {
      this.category = category;
    }
    /**
    *   获取 行业
    **/
    public String getCategory() {
      return category;
    }
    /**
    *   设置 法人类型
    **/
    public void setPersonType(Integer personType) {
      this.personType = personType;
    }
    /**
    *   获取 法人类型
    **/
    public Integer getPersonType() {
      return personType;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }



}

