package com.gitee.deament.tianyancha.core.remote.humanriskinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.humanriskinfo.entity.HumanRiskInfo;
import com.gitee.deament.tianyancha.core.remote.humanriskinfo.dto.HumanRiskInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*人员天眼风险
* 可以通过公司名称或ID和人名获取人员相关天眼风险列表，包括人员周边/预警风险信息
* /services/v4/open/humanRiskInfo
*@author deament
**/
@Component("humanRiskInfoRequestImpl")
public class HumanRiskInfoRequestImpl extends BaseRequestImpl<HumanRiskInfo,HumanRiskInfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/humanRiskInfo";
    }
}

