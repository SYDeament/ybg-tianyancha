package com.gitee.deament.tianyancha.core.remote.cdfindtzanli.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdfindtzanli.entity.CdFindTzanli;
import com.gitee.deament.tianyancha.core.remote.cdfindtzanli.dto.CdFindTzanliDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*投资事件
* 可以通过公司名称或ID获取企业投资事件信息，企业投资事件信息包括投资事件轮次、金额、投资方、投资产品、行业、业务等字段的详细信息
* /services/open/cd/findTzanli/2.0
*@author deament
**/
@Component("cdFindTzanliRequestImpl")
public class CdFindTzanliRequestImpl extends BaseRequestImpl<CdFindTzanli,CdFindTzanliDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cd/findTzanli/2.0";
    }
}

