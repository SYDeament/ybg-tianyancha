package com.gitee.deament.tianyancha.core.remote.cdsearchbrandandagency.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*搜索项目品牌/投资机构
* 属于CdSearchBrandAndAgency
* /services/open/cd/searchBrandAndAgency/2.0
*@author deament
**/

public class CdSearchBrandAndAgencyItems implements Serializable{

    /**
     *    申请时间
    **/
    @JSONField(name="setupDate")
    private String setupDate;
    /**
     *    轮次
    **/
    @JSONField(name="round")
    private String round;
    /**
     *    产品介绍
    **/
    @JSONField(name="intro")
    private String intro;
    /**
     *    所属公司
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    产品logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    竞品数量
    **/
    @JSONField(name="competingCount")
    private Integer competingCount;
    /**
     *    投资数量
    **/
    @JSONField(name="eventCount")
    private Integer eventCount;
    /**
     *    ID
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    1-品牌 2-机构
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    基金数量
    **/
    @JSONField(name="fundCount")
    private Integer fundCount;
    /**
     *    省份
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 申请时间
    **/
    public void setSetupDate(String setupDate) {
      this.setupDate = setupDate;
    }
    /**
    *   获取 申请时间
    **/
    public String getSetupDate() {
      return setupDate;
    }
    /**
    *   设置 轮次
    **/
    public void setRound(String round) {
      this.round = round;
    }
    /**
    *   获取 轮次
    **/
    public String getRound() {
      return round;
    }
    /**
    *   设置 产品介绍
    **/
    public void setIntro(String intro) {
      this.intro = intro;
    }
    /**
    *   获取 产品介绍
    **/
    public String getIntro() {
      return intro;
    }
    /**
    *   设置 所属公司
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 所属公司
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 产品logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 产品logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 竞品数量
    **/
    public void setCompetingCount(Integer competingCount) {
      this.competingCount = competingCount;
    }
    /**
    *   获取 竞品数量
    **/
    public Integer getCompetingCount() {
      return competingCount;
    }
    /**
    *   设置 投资数量
    **/
    public void setEventCount(Integer eventCount) {
      this.eventCount = eventCount;
    }
    /**
    *   获取 投资数量
    **/
    public Integer getEventCount() {
      return eventCount;
    }
    /**
    *   设置 ID
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 ID
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 1-品牌 2-机构
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-品牌 2-机构
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 基金数量
    **/
    public void setFundCount(Integer fundCount) {
      this.fundCount = fundCount;
    }
    /**
    *   获取 基金数量
    **/
    public Integer getFundCount() {
      return fundCount;
    }
    /**
    *   设置 省份
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份
    **/
    public String getBase() {
      return base;
    }



}

