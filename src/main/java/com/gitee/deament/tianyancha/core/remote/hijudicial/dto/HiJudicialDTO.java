package com.gitee.deament.tianyancha.core.remote.hijudicial.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*历史司法协助详情
* 根据司法协助ID获取历史的司法协助详情，判断司法协助类型，历史司法协助详情包括股权变更、股权冻结、结解除冻结、司法协助续行、股权数额、司法冻结失效及对应的详细信息
* /services/open/hi/judicial/detail/2.0
*@author deament
**/

@TYCURL(value="/services/open/hi/judicial/detail/2.0")
public class HiJudicialDTO implements Serializable{

    /**
     *    司法协助id
     *
    **/
    @ParamRequire(require = true)
    private String businessId;


    /**
    *   设置 司法协助id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 司法协助id
    **/
    public String getBusinessId() {
      return businessId;
    }



}

