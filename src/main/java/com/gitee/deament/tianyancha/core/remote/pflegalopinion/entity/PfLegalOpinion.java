package com.gitee.deament.tianyancha.core.remote.pflegalopinion.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*法律意见书信息
* 可以通过公司名称或ID获取相关私募基金法律意见书信息，包括法律意见书状态等
* /services/open/pf/legalOpinion/2.0
*@author deament
**/

public class PfLegalOpinion implements Serializable{

    /**
     *    法律意见书状态
    **/
    @JSONField(name="legal_opinion_status")
    private String legal_opinion_status;
    /**
     *    企业id
    **/
    @JSONField(name="graph_id")
    private Integer graph_id;


    /**
    *   设置 法律意见书状态
    **/
    public void setLegal_opinion_status(String legal_opinion_status) {
      this.legal_opinion_status = legal_opinion_status;
    }
    /**
    *   获取 法律意见书状态
    **/
    public String getLegal_opinion_status() {
      return legal_opinion_status;
    }
    /**
    *   设置 企业id
    **/
    public void setGraph_id(Integer graph_id) {
      this.graph_id = graph_id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getGraph_id() {
      return graph_id;
    }



}

