package com.gitee.deament.tianyancha.core.remote.stocksharestructure.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stocksharestructure.entity.StockShareStructureDataList;
/**
*股本结构
* 可以通过公司名称或ID获取上市公司股本结构信息，股本结构信息包括总股本、A股总股本、流通A股、限售A股等
* /services/open/stock/shareStructure/2.0
*@author deament
**/

public class StockShareStructure implements Serializable{

    /**
     *    时间列表
    **/
    @JSONField(name="timeList")
    private List<String> timeList;
    /**
     *    
    **/
    @JSONField(name="dataList")
    private List<StockShareStructureDataList> dataList;


    /**
    *   设置 时间列表
    **/
    public void setTimeList(List<String> timeList) {
      this.timeList = timeList;
    }
    /**
    *   获取 时间列表
    **/
    public List<String> getTimeList() {
      return timeList;
    }
    /**
    *   设置 
    **/
    public void setDataList(List<StockShareStructureDataList> dataList) {
      this.dataList = dataList;
    }
    /**
    *   获取 
    **/
    public List<StockShareStructureDataList> getDataList() {
      return dataList;
    }



}

