package com.gitee.deament.tianyancha.core.remote.stockprospectus.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockprospectus.entity.StockProspectus;
import com.gitee.deament.tianyancha.core.remote.stockprospectus.dto.StockProspectusDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*招股书
* 可以通过公司名称或ID获取上市公司招股书信息，招股书信息包括招股书标题、股票名称、招股书内容等
* /services/open/stock/prospectus/2.0
*@author deament
**/
@Component("stockProspectusRequestImpl")
public class StockProspectusRequestImpl extends BaseRequestImpl<StockProspectus,StockProspectusDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/prospectus/2.0";
    }
}

