package com.gitee.deament.tianyancha.core.remote.jrlawsuit.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrlawsuit.entity.JrLawSuit;
import com.gitee.deament.tianyancha.core.remote.jrlawsuit.dto.JrLawSuitDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*法律诉讼
* 可以通过公司名称或ID获取企业法律诉讼信息，法律诉讼包括案件名称、案由、案件身份、案号等字段的详细信息
* /services/open/jr/lawSuit/2.0
*@author deament
**/
@Component("jrLawSuitRequestImpl")
public class JrLawSuitRequestImpl extends BaseRequestImpl<JrLawSuit,JrLawSuitDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/jr/lawSuit/2.0";
    }
}

