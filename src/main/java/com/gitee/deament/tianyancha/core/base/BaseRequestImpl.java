package com.gitee.deament.tianyancha.core.base;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;

import com.gitee.deament.tianyancha.core.HttpRequest;
import com.gitee.deament.tianyancha.core.TianyanchaResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * 请求实现类
 *
 * @author deament
 */
public abstract class BaseRequestImpl<VO, DTO> implements BaseRequest<VO, DTO> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private HttpRequestI httpRequest;

    @Override
    public TianyanchaResult<VO> get( DTO dto) throws Exception {
        String url= getUrl();
        String result = httpRequest.get(url, BeanUtil.beanToMap(dto));
        logger.info("得到的结果："+result);
        TianyanchaResult<VO> vo = JSONObject.parseObject(result, TianyanchaResult.class);
        logger.info("转成对于的实体类:"+JSONObject.toJSONString(vo));
        return vo;
    }

    public abstract String getUrl();
}
