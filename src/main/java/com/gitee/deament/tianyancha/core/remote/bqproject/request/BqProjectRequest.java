package com.gitee.deament.tianyancha.core.remote.bqproject.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProject;
import com.gitee.deament.tianyancha.core.remote.bqproject.dto.BqProjectDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*建筑资质-工程项目详情
* 可以通过建筑资质工程项目ID获取建筑资质工程项目信息，包括项目编号、项目名称、项目属地、项目类别、建设单位、招投标、合同备案、施工许可等字段信息
* /services/open/bq/project/detail/2.0
*@author deament
**/

public interface BqProjectRequest extends BaseRequest<BqProjectDTO ,BqProject>{

}

