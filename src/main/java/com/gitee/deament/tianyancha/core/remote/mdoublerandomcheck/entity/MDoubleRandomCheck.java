package com.gitee.deament.tianyancha.core.remote.mdoublerandomcheck.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mdoublerandomcheck.entity.MDoubleRandomCheckTaskList;
import com.gitee.deament.tianyancha.core.remote.mdoublerandomcheck.entity.MDoubleRandomCheckItems;
/**
*双随机抽查
* 可以通过公司名称或ID获取企业双随机抽查信息，企业双随机抽查信息包括抽查类型、抽查机关等字段的详细信息
* /services/open/m/doubleRandomCheck/2.0
*@author deament
**/

public class MDoubleRandomCheck implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MDoubleRandomCheckItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MDoubleRandomCheckItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MDoubleRandomCheckItems> getItems() {
      return items;
    }



}

