package com.gitee.deament.tianyancha.core.remote.match.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*验证企业
* 可以查询公司名称或ID和企业信用代码是否对应
* /services/v4/open/match
*@author deament
**/

@TYCURL(value="/services/v4/open/match")
public class MatchDTO implements Serializable{

    /**
     *    公司名
     *
    **/
    @ParamRequire(require = true)
    private String name;
    /**
     *    注册号/组织机构代码/统一社会信用代码
     *
    **/
    @ParamRequire(require = true)
    private String keyword;


    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 注册号/组织机构代码/统一社会信用代码
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 注册号/组织机构代码/统一社会信用代码
    **/
    public String getKeyword() {
      return keyword;
    }



}

