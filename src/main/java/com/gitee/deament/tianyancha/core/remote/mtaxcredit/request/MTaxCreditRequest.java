package com.gitee.deament.tianyancha.core.remote.mtaxcredit.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mtaxcredit.entity.MTaxCredit;
import com.gitee.deament.tianyancha.core.remote.mtaxcredit.dto.MTaxCreditDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*税务评级
* 可以通过公司名称或ID获取企业税务评级信息，企业税务评级信息包括评价年度、纳税人信用级别、类型、纳税人识别号、评价单位等字段的详细信息
* /services/open/m/taxCredit/2.0
*@author deament
**/

public interface MTaxCreditRequest extends BaseRequest<MTaxCreditDTO ,MTaxCredit>{

}

