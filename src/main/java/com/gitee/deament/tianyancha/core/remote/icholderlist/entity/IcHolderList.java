package com.gitee.deament.tianyancha.core.remote.icholderlist.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icholderlist.entity.IcHolderListCapital;
import com.gitee.deament.tianyancha.core.remote.icholderlist.entity.IcHolderListCapitalActl;
import com.gitee.deament.tianyancha.core.remote.icholderlist.entity.IcHolderListItems;
/**
*公司公示-股东出资
* 可以通过公司名称或ID获取股东及出自信息，股东及出资信息包括股东名、出资比例、出资金额、股东总数等字段的详细信息
* /services/open/ic/holderList/2.0
*@author deament
**/

public class IcHolderList implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcHolderListItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcHolderListItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcHolderListItems> getItems() {
      return items;
    }



}

