package com.gitee.deament.tianyancha.core.remote.oiextfunds.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
* 对外投资基金
* 可以通过投资机构名称获取投资机构对外投资基金信息，包括企业名称、法人、注册资本、成立日期、投资方等字段信息
* /services/open/oi/extFunds/2.0
*@author deament
**/

@TYCURL(value="/services/open/oi/extFunds/2.0")
public class OiExtFundsDTO implements Serializable{

    /**
     *    投资机构名称
     *
    **/
    @ParamRequire(require = true)
    private String name;
    /**
     *    每页条数（默认20，最大20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    当前页（默认1）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 投资机构名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 投资机构名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（默认20，最大20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20，最大20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 当前页（默认1）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

