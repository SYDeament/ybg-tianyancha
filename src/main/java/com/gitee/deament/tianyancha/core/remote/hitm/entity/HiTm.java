package com.gitee.deament.tianyancha.core.remote.hitm.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hitm.entity.HiTmItems;
/**
*历史商标信息
* 可以通过公司名称或ID获取历史的商标有关信息，历史商标信息包括商标图片、注册号、国际分类等字段信息
* /services/open/hi/tm/2.0
*@author deament
**/

public class HiTm implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private String total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiTmItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(String total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public String getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiTmItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiTmItems> getItems() {
      return items;
    }



}

