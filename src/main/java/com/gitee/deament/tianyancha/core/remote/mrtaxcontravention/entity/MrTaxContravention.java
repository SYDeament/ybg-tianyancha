package com.gitee.deament.tianyancha.core.remote.mrtaxcontravention.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*税收违法详情
* 根据税收违法ID获取税收违法详情，税收违法详情包括纳税人名称、纳税人识别号、注册地址、违法事实、财务负责人信息、负有责任的中介信息等字段的详细信息
* /services/open/mr/taxContravention/detail/2.0
*@author deament
**/

public class MrTaxContravention implements Serializable{

    /**
     *    负有责任的财务负责人证件名称
    **/
    @JSONField(name="res_person_id_type")
    private String res_person_id_type;
    /**
     *    负有直接责任的财务负责人性别
    **/
    @JSONField(name="res_person_sex")
    private String res_person_sex;
    /**
     *    注册地址
    **/
    @JSONField(name="address")
    private String address;
    /**
     *    所属税务机关
    **/
    @JSONField(name="responsible_department")
    private String responsible_department;
    /**
     *    负有直接责任的中介机构从业人员姓名
    **/
    @JSONField(name="res_department_name")
    private String res_department_name;
    /**
     *    组织机构代码
    **/
    @JSONField(name="taxpayer_code")
    private String taxpayer_code;
    /**
     *    主要违法事实、相关法律依据及税务处理处罚情况
    **/
    @JSONField(name="case_info")
    private String case_info;
    /**
     *    法定代表人或负责人性别
    **/
    @JSONField(name="legal_person_sex")
    private String legal_person_sex;
    /**
     *    案件性质
    **/
    @JSONField(name="case_type")
    private String case_type;
    /**
     *    法定代表人或负责人证件号码
    **/
    @JSONField(name="legal_person_id_number")
    private String legal_person_id_number;
    /**
     *    负有直接责任的财务负责人姓名
    **/
    @JSONField(name="res_person_name")
    private String res_person_name;
    /**
     *    负有直接责任的中介机构从业人员性别
    **/
    @JSONField(name="res_department_sex")
    private String res_department_sex;
    /**
     *    纳税人名称
    **/
    @JSONField(name="taxpayer_name")
    private String taxpayer_name;
    /**
     *    公司id
    **/
    @JSONField(name="companyGraphId")
    private Integer companyGraphId;
    /**
     *    法定代表人或负责人证件名称
    **/
    @JSONField(name="legal_person_id_type")
    private String legal_person_id_type;
    /**
     *    发布时间
    **/
    @JSONField(name="publish_time")
    private String publish_time;
    /**
     *    法定代表人或负责人
    **/
    @JSONField(name="legal_person_name")
    private String legal_person_name;
    /**
     *    负有责任的财务负责人证件号码
    **/
    @JSONField(name="res_person_id_number")
    private String res_person_id_number;
    /**
     *    纳税人识别号
    **/
    @JSONField(name="taxpayer_number")
    private String taxpayer_number;
    /**
     *    负有直接责任的中介机构
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    负有直接责任的中介机构从业人员证件名称
    **/
    @JSONField(name="res_department_id_type")
    private String res_department_id_type;
    /**
     *    负有直接责任的中介机构从业人员证件号码
    **/
    @JSONField(name="res_department_id_number")
    private String res_department_id_number;


    /**
    *   设置 负有责任的财务负责人证件名称
    **/
    public void setRes_person_id_type(String res_person_id_type) {
      this.res_person_id_type = res_person_id_type;
    }
    /**
    *   获取 负有责任的财务负责人证件名称
    **/
    public String getRes_person_id_type() {
      return res_person_id_type;
    }
    /**
    *   设置 负有直接责任的财务负责人性别
    **/
    public void setRes_person_sex(String res_person_sex) {
      this.res_person_sex = res_person_sex;
    }
    /**
    *   获取 负有直接责任的财务负责人性别
    **/
    public String getRes_person_sex() {
      return res_person_sex;
    }
    /**
    *   设置 注册地址
    **/
    public void setAddress(String address) {
      this.address = address;
    }
    /**
    *   获取 注册地址
    **/
    public String getAddress() {
      return address;
    }
    /**
    *   设置 所属税务机关
    **/
    public void setResponsible_department(String responsible_department) {
      this.responsible_department = responsible_department;
    }
    /**
    *   获取 所属税务机关
    **/
    public String getResponsible_department() {
      return responsible_department;
    }
    /**
    *   设置 负有直接责任的中介机构从业人员姓名
    **/
    public void setRes_department_name(String res_department_name) {
      this.res_department_name = res_department_name;
    }
    /**
    *   获取 负有直接责任的中介机构从业人员姓名
    **/
    public String getRes_department_name() {
      return res_department_name;
    }
    /**
    *   设置 组织机构代码
    **/
    public void setTaxpayer_code(String taxpayer_code) {
      this.taxpayer_code = taxpayer_code;
    }
    /**
    *   获取 组织机构代码
    **/
    public String getTaxpayer_code() {
      return taxpayer_code;
    }
    /**
    *   设置 主要违法事实、相关法律依据及税务处理处罚情况
    **/
    public void setCase_info(String case_info) {
      this.case_info = case_info;
    }
    /**
    *   获取 主要违法事实、相关法律依据及税务处理处罚情况
    **/
    public String getCase_info() {
      return case_info;
    }
    /**
    *   设置 法定代表人或负责人性别
    **/
    public void setLegal_person_sex(String legal_person_sex) {
      this.legal_person_sex = legal_person_sex;
    }
    /**
    *   获取 法定代表人或负责人性别
    **/
    public String getLegal_person_sex() {
      return legal_person_sex;
    }
    /**
    *   设置 案件性质
    **/
    public void setCase_type(String case_type) {
      this.case_type = case_type;
    }
    /**
    *   获取 案件性质
    **/
    public String getCase_type() {
      return case_type;
    }
    /**
    *   设置 法定代表人或负责人证件号码
    **/
    public void setLegal_person_id_number(String legal_person_id_number) {
      this.legal_person_id_number = legal_person_id_number;
    }
    /**
    *   获取 法定代表人或负责人证件号码
    **/
    public String getLegal_person_id_number() {
      return legal_person_id_number;
    }
    /**
    *   设置 负有直接责任的财务负责人姓名
    **/
    public void setRes_person_name(String res_person_name) {
      this.res_person_name = res_person_name;
    }
    /**
    *   获取 负有直接责任的财务负责人姓名
    **/
    public String getRes_person_name() {
      return res_person_name;
    }
    /**
    *   设置 负有直接责任的中介机构从业人员性别
    **/
    public void setRes_department_sex(String res_department_sex) {
      this.res_department_sex = res_department_sex;
    }
    /**
    *   获取 负有直接责任的中介机构从业人员性别
    **/
    public String getRes_department_sex() {
      return res_department_sex;
    }
    /**
    *   设置 纳税人名称
    **/
    public void setTaxpayer_name(String taxpayer_name) {
      this.taxpayer_name = taxpayer_name;
    }
    /**
    *   获取 纳税人名称
    **/
    public String getTaxpayer_name() {
      return taxpayer_name;
    }
    /**
    *   设置 公司id
    **/
    public void setCompanyGraphId(Integer companyGraphId) {
      this.companyGraphId = companyGraphId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCompanyGraphId() {
      return companyGraphId;
    }
    /**
    *   设置 法定代表人或负责人证件名称
    **/
    public void setLegal_person_id_type(String legal_person_id_type) {
      this.legal_person_id_type = legal_person_id_type;
    }
    /**
    *   获取 法定代表人或负责人证件名称
    **/
    public String getLegal_person_id_type() {
      return legal_person_id_type;
    }
    /**
    *   设置 发布时间
    **/
    public void setPublish_time(String publish_time) {
      this.publish_time = publish_time;
    }
    /**
    *   获取 发布时间
    **/
    public String getPublish_time() {
      return publish_time;
    }
    /**
    *   设置 法定代表人或负责人
    **/
    public void setLegal_person_name(String legal_person_name) {
      this.legal_person_name = legal_person_name;
    }
    /**
    *   获取 法定代表人或负责人
    **/
    public String getLegal_person_name() {
      return legal_person_name;
    }
    /**
    *   设置 负有责任的财务负责人证件号码
    **/
    public void setRes_person_id_number(String res_person_id_number) {
      this.res_person_id_number = res_person_id_number;
    }
    /**
    *   获取 负有责任的财务负责人证件号码
    **/
    public String getRes_person_id_number() {
      return res_person_id_number;
    }
    /**
    *   设置 纳税人识别号
    **/
    public void setTaxpayer_number(String taxpayer_number) {
      this.taxpayer_number = taxpayer_number;
    }
    /**
    *   获取 纳税人识别号
    **/
    public String getTaxpayer_number() {
      return taxpayer_number;
    }
    /**
    *   设置 负有直接责任的中介机构
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 负有直接责任的中介机构
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 负有直接责任的中介机构从业人员证件名称
    **/
    public void setRes_department_id_type(String res_department_id_type) {
      this.res_department_id_type = res_department_id_type;
    }
    /**
    *   获取 负有直接责任的中介机构从业人员证件名称
    **/
    public String getRes_department_id_type() {
      return res_department_id_type;
    }
    /**
    *   设置 负有直接责任的中介机构从业人员证件号码
    **/
    public void setRes_department_id_number(String res_department_id_number) {
      this.res_department_id_number = res_department_id_number;
    }
    /**
    *   获取 负有直接责任的中介机构从业人员证件号码
    **/
    public String getRes_department_id_number() {
      return res_department_id_number;
    }



}

