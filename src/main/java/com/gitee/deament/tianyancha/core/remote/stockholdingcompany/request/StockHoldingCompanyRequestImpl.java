package com.gitee.deament.tianyancha.core.remote.stockholdingcompany.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockholdingcompany.entity.StockHoldingCompany;
import com.gitee.deament.tianyancha.core.remote.stockholdingcompany.dto.StockHoldingCompanyDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*参股控股
* 可以通过公司名称或ID获取上市公司参股控股股东信息，参股控股股东信息包括参股控股关联公司、参股关系、参股比例、投资金额、被参股公司净利润等
* /services/open/stock/holdingCompany/2.0
*@author deament
**/
@Component("stockHoldingCompanyRequestImpl")
public class StockHoldingCompanyRequestImpl extends BaseRequestImpl<StockHoldingCompany,StockHoldingCompanyDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/holdingCompany/2.0";
    }
}

