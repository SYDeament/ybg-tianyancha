package com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史动产抵押
* 属于HiMortgageInfo
* /services/open/hi/mortgageInfo/2.0
*@author deament
**/

public class HiMortgageInfoItems implements Serializable{

    /**
     *    基本信息
    **/
    @JSONField(name="baseInfo")
    private HiMortgageInfoBaseInfo baseInfo;
    /**
     *    抵押人列表
    **/
    @JSONField(name="peopleInfo")
    private List<HiMortgageInfoPeopleInfo> peopleInfo;
    /**
     *    抵押物
    **/
    @JSONField(name="pawnInfoList")
    private List<HiMortgageInfoPawnInfoList> pawnInfoList;
    /**
     *    
    **/
    @JSONField(name="changeInfoList")
    private List<HiMortgageInfoChangeInfoList> changeInfoList;


    /**
    *   设置 基本信息
    **/
    public void setBaseInfo(HiMortgageInfoBaseInfo baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 基本信息
    **/
    public HiMortgageInfoBaseInfo getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 抵押人列表
    **/
    public void setPeopleInfo(List<HiMortgageInfoPeopleInfo> peopleInfo) {
      this.peopleInfo = peopleInfo;
    }
    /**
    *   获取 抵押人列表
    **/
    public List<HiMortgageInfoPeopleInfo> getPeopleInfo() {
      return peopleInfo;
    }
    /**
    *   设置 抵押物
    **/
    public void setPawnInfoList(List<HiMortgageInfoPawnInfoList> pawnInfoList) {
      this.pawnInfoList = pawnInfoList;
    }
    /**
    *   获取 抵押物
    **/
    public List<HiMortgageInfoPawnInfoList> getPawnInfoList() {
      return pawnInfoList;
    }
    /**
    *   设置 
    **/
    public void setChangeInfoList(List<HiMortgageInfoChangeInfoList> changeInfoList) {
      this.changeInfoList = changeInfoList;
    }
    /**
    *   获取 
    **/
    public List<HiMortgageInfoChangeInfoList> getChangeInfoList() {
      return changeInfoList;
    }



}

