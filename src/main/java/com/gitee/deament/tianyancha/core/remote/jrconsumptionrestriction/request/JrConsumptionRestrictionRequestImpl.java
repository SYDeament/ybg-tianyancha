package com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.entity.JrConsumptionRestriction;
import com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.dto.JrConsumptionRestrictionDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*限制消费令
* 可以通过公司名称或ID获取限制消费令信息，限制消费令信息包括执行法院、案件内容、被执行人名称等字段的详细信息
* /services/open/jr/consumptionRestriction/2.0
*@author deament
**/
@Component("jrConsumptionRestrictionRequestImpl")
public class JrConsumptionRestrictionRequestImpl extends BaseRequestImpl<JrConsumptionRestriction,JrConsumptionRestrictionDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/jr/consumptionRestriction/2.0";
    }
}

