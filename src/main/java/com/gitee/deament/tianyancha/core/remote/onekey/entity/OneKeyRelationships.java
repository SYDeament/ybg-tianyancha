package com.gitee.deament.tianyancha.core.remote.onekey.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业图谱
* 属于OneKey
* /services/v4/open/oneKey/c
*@author deament
**/

public class OneKeyRelationships implements Serializable{

    /**
     *    开始节点
    **/
    @JSONField(name="startNode")
    private String startNode;
    /**
     *    关系id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    关系类型
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    尾部节点
    **/
    @JSONField(name="endNode")
    private String endNode;
    /**
     *    关系属性
    **/
    @JSONField(name="properties")
    private OneKeyProperties properties;


    /**
    *   设置 开始节点
    **/
    public void setStartNode(String startNode) {
      this.startNode = startNode;
    }
    /**
    *   获取 开始节点
    **/
    public String getStartNode() {
      return startNode;
    }
    /**
    *   设置 关系id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 关系id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 关系类型
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 关系类型
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 尾部节点
    **/
    public void setEndNode(String endNode) {
      this.endNode = endNode;
    }
    /**
    *   获取 尾部节点
    **/
    public String getEndNode() {
      return endNode;
    }
    /**
    *   设置 关系属性
    **/
    public void setProperties(OneKeyProperties properties) {
      this.properties = properties;
    }
    /**
    *   获取 关系属性
    **/
    public OneKeyProperties getProperties() {
      return properties;
    }



}

