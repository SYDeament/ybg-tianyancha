package com.gitee.deament.tianyancha.core.remote.hipunishment.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hipunishment.entity.HiPunishment;
import com.gitee.deament.tianyancha.core.remote.hipunishment.dto.HiPunishmentDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史行政处罚-其他来源
* 可以通过公司名称或ID获取企业历史的行政处罚信息，历史行政处罚信息包括行政处罚明细、行政处罚公告、行政处罚内容、公示日期、处罚依据、处罚状态等字段信息
* /services/open/hi/punishment/creditChina/2.0
*@author deament
**/
@Component("hiPunishmentRequestImpl")
public class HiPunishmentRequestImpl extends BaseRequestImpl<HiPunishment,HiPunishmentDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/punishment/creditChina/2.0";
    }
}

