package com.gitee.deament.tianyancha.core.remote.mbids.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*招投标
* 属于MBids
* /services/open/m/bids/2.0
*@author deament
**/

public class MBidsItems implements Serializable{

    /**
     *    发布时间
    **/
    @JSONField(name="publishTime")
    private String publishTime;
    /**
     *    代理机构
    **/
    @JSONField(name="proxy")
    private String proxy;
    /**
     *    天眼查链接
    **/
    @JSONField(name="bidUrl")
    private String bidUrl;
    /**
     *    摘要信息
    **/
    @JSONField(name="abs")
    private String abs;
    /**
     *    采购人
    **/
    @JSONField(name="purchaser")
    private String purchaser;
    /**
     *    正文简介
    **/
    @JSONField(name="intro")
    private String intro;
    /**
     *    详细信息链接
    **/
    @JSONField(name="link")
    private String link;
    /**
     *    id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    uuid
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    正文信息
    **/
    @JSONField(name="content")
    private String content;


    /**
    *   设置 发布时间
    **/
    public void setPublishTime(String publishTime) {
      this.publishTime = publishTime;
    }
    /**
    *   获取 发布时间
    **/
    public String getPublishTime() {
      return publishTime;
    }
    /**
    *   设置 代理机构
    **/
    public void setProxy(String proxy) {
      this.proxy = proxy;
    }
    /**
    *   获取 代理机构
    **/
    public String getProxy() {
      return proxy;
    }
    /**
    *   设置 天眼查链接
    **/
    public void setBidUrl(String bidUrl) {
      this.bidUrl = bidUrl;
    }
    /**
    *   获取 天眼查链接
    **/
    public String getBidUrl() {
      return bidUrl;
    }
    /**
    *   设置 摘要信息
    **/
    public void setAbs(String abs) {
      this.abs = abs;
    }
    /**
    *   获取 摘要信息
    **/
    public String getAbs() {
      return abs;
    }
    /**
    *   设置 采购人
    **/
    public void setPurchaser(String purchaser) {
      this.purchaser = purchaser;
    }
    /**
    *   获取 采购人
    **/
    public String getPurchaser() {
      return purchaser;
    }
    /**
    *   设置 正文简介
    **/
    public void setIntro(String intro) {
      this.intro = intro;
    }
    /**
    *   获取 正文简介
    **/
    public String getIntro() {
      return intro;
    }
    /**
    *   设置 详细信息链接
    **/
    public void setLink(String link) {
      this.link = link;
    }
    /**
    *   获取 详细信息链接
    **/
    public String getLink() {
      return link;
    }
    /**
    *   设置 id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 uuid
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 正文信息
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 正文信息
    **/
    public String getContent() {
      return content;
    }



}

