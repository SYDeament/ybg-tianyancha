package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryDefendant implements Serializable{

    /**
     *    人或公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    人或公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 人或公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 人或公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 人或公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 人或公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }



}

