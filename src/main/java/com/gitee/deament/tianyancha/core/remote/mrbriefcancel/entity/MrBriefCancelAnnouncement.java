package com.gitee.deament.tianyancha.core.remote.mrbriefcancel.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*简易注销
* 属于MrBriefCancel
* /services/open/mr/briefCancel/2.0
*@author deament
**/

public class MrBriefCancelAnnouncement implements Serializable{

    /**
     *    登记机关
    **/
    @JSONField(name="reg_authority")
    private String reg_authority;
    /**
     *    企业名称
    **/
    @JSONField(name="company_name")
    private String company_name;
    /**
     *    统一社会信用代码/注册号
    **/
    @JSONField(name="credit_code")
    private String credit_code;
    /**
     *    简易注销id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    公告期
    **/
    @JSONField(name="announcement_term")
    private String announcement_term;
    /**
     *    结束日期
    **/
    @JSONField(name="announcement_end_date")
    private String announcement_end_date;
    /**
     *    承诺书地址
    **/
    @JSONField(name="ossPath")
    private String ossPath;


    /**
    *   设置 登记机关
    **/
    public void setReg_authority(String reg_authority) {
      this.reg_authority = reg_authority;
    }
    /**
    *   获取 登记机关
    **/
    public String getReg_authority() {
      return reg_authority;
    }
    /**
    *   设置 企业名称
    **/
    public void setCompany_name(String company_name) {
      this.company_name = company_name;
    }
    /**
    *   获取 企业名称
    **/
    public String getCompany_name() {
      return company_name;
    }
    /**
    *   设置 统一社会信用代码/注册号
    **/
    public void setCredit_code(String credit_code) {
      this.credit_code = credit_code;
    }
    /**
    *   获取 统一社会信用代码/注册号
    **/
    public String getCredit_code() {
      return credit_code;
    }
    /**
    *   设置 简易注销id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 简易注销id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 公告期
    **/
    public void setAnnouncement_term(String announcement_term) {
      this.announcement_term = announcement_term;
    }
    /**
    *   获取 公告期
    **/
    public String getAnnouncement_term() {
      return announcement_term;
    }
    /**
    *   设置 结束日期
    **/
    public void setAnnouncement_end_date(String announcement_end_date) {
      this.announcement_end_date = announcement_end_date;
    }
    /**
    *   获取 结束日期
    **/
    public String getAnnouncement_end_date() {
      return announcement_end_date;
    }
    /**
    *   设置 承诺书地址
    **/
    public void setOssPath(String ossPath) {
      this.ossPath = ossPath;
    }
    /**
    *   获取 承诺书地址
    **/
    public String getOssPath() {
      return ossPath;
    }



}

