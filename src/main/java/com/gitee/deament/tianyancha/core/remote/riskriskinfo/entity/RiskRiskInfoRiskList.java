package com.gitee.deament.tianyancha.core.remote.riskriskinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业天眼风险
* 属于RiskRiskInfo
* /services/open/risk/riskInfo/2.0
*@author deament
**/

public class RiskRiskInfoRiskList implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="count")
    private Integer count;
    /**
     *    风险分类（周边风险，预警提醒，自身风险）
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    
    **/
    @JSONField(name="list")
    private List<RiskRiskInfoList> list;
    /**
     *    
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 总数
    **/
    public void setCount(Integer count) {
      this.count = count;
    }
    /**
    *   获取 总数
    **/
    public Integer getCount() {
      return count;
    }
    /**
    *   设置 风险分类（周边风险，预警提醒，自身风险）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 风险分类（周边风险，预警提醒，自身风险）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 
    **/
    public void setList(List<RiskRiskInfoList> list) {
      this.list = list;
    }
    /**
    *   获取 
    **/
    public List<RiskRiskInfoList> getList() {
      return list;
    }
    /**
    *   设置 
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 
    **/
    public Integer getType() {
      return type;
    }



}

