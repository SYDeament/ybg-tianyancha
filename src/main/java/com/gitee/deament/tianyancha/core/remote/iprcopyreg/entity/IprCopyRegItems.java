package com.gitee.deament.tianyancha.core.remote.iprcopyreg.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*软件著作权
* 属于IprCopyReg
* /services/open/ipr/copyReg/2.0
*@author deament
**/

public class IprCopyRegItems implements Serializable{

    /**
     *    登记日期
    **/
    @JSONField(name="regtime")
    private String regtime;
    /**
     *    跳转到天眼查链接
    **/
    @JSONField(name="connList")
    private List<String> connList;
    /**
     *    无用
    **/
    @JSONField(name="searchType")
    private String searchType;
    /**
     *    无用
    **/
    @JSONField(name="_type")
    private String _type;
    /**
     *    简称
    **/
    @JSONField(name="simplename")
    private String simplename;
    /**
     *    分类号
    **/
    @JSONField(name="catnum")
    private String catnum;
    /**
     *    版本号
    **/
    @JSONField(name="version")
    private String version;
    /**
     *    唯一标识符
    **/
    @JSONField(name="uni")
    private String uni;
    /**
     *    首次发表日期
    **/
    @JSONField(name="publishtime")
    private String publishtime;
    /**
     *    批准日期
    **/
    @JSONField(name="eventTime")
    private String eventTime;
    /**
     *    著作权人
    **/
    @JSONField(name="authorNationality")
    private String authorNationality;
    /**
     *    登记号
    **/
    @JSONField(name="regnum")
    private String regnum;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    全称
    **/
    @JSONField(name="fullname")
    private String fullname;


    /**
    *   设置 登记日期
    **/
    public void setRegtime(String regtime) {
      this.regtime = regtime;
    }
    /**
    *   获取 登记日期
    **/
    public String getRegtime() {
      return regtime;
    }
    /**
    *   设置 跳转到天眼查链接
    **/
    public void setConnList(List<String> connList) {
      this.connList = connList;
    }
    /**
    *   获取 跳转到天眼查链接
    **/
    public List<String> getConnList() {
      return connList;
    }
    /**
    *   设置 无用
    **/
    public void setSearchType(String searchType) {
      this.searchType = searchType;
    }
    /**
    *   获取 无用
    **/
    public String getSearchType() {
      return searchType;
    }
    /**
    *   设置 无用
    **/
    public void set_type(String _type) {
      this._type = _type;
    }
    /**
    *   获取 无用
    **/
    public String get_type() {
      return _type;
    }
    /**
    *   设置 简称
    **/
    public void setSimplename(String simplename) {
      this.simplename = simplename;
    }
    /**
    *   获取 简称
    **/
    public String getSimplename() {
      return simplename;
    }
    /**
    *   设置 分类号
    **/
    public void setCatnum(String catnum) {
      this.catnum = catnum;
    }
    /**
    *   获取 分类号
    **/
    public String getCatnum() {
      return catnum;
    }
    /**
    *   设置 版本号
    **/
    public void setVersion(String version) {
      this.version = version;
    }
    /**
    *   获取 版本号
    **/
    public String getVersion() {
      return version;
    }
    /**
    *   设置 唯一标识符
    **/
    public void setUni(String uni) {
      this.uni = uni;
    }
    /**
    *   获取 唯一标识符
    **/
    public String getUni() {
      return uni;
    }
    /**
    *   设置 首次发表日期
    **/
    public void setPublishtime(String publishtime) {
      this.publishtime = publishtime;
    }
    /**
    *   获取 首次发表日期
    **/
    public String getPublishtime() {
      return publishtime;
    }
    /**
    *   设置 批准日期
    **/
    public void setEventTime(String eventTime) {
      this.eventTime = eventTime;
    }
    /**
    *   获取 批准日期
    **/
    public String getEventTime() {
      return eventTime;
    }
    /**
    *   设置 著作权人
    **/
    public void setAuthorNationality(String authorNationality) {
      this.authorNationality = authorNationality;
    }
    /**
    *   获取 著作权人
    **/
    public String getAuthorNationality() {
      return authorNationality;
    }
    /**
    *   设置 登记号
    **/
    public void setRegnum(String regnum) {
      this.regnum = regnum;
    }
    /**
    *   获取 登记号
    **/
    public String getRegnum() {
      return regnum;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 全称
    **/
    public void setFullname(String fullname) {
      this.fullname = fullname;
    }
    /**
    *   获取 全称
    **/
    public String getFullname() {
      return fullname;
    }



}

