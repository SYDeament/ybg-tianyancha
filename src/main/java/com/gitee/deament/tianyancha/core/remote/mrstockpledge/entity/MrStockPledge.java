package com.gitee.deament.tianyancha.core.remote.mrstockpledge.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrstockpledge.entity.MrStockPledgeTrend;
/**
*质押走势
* 可以通过公司名称或ID获取股权质押明细信息，股权质押走势信息包括股东名称、质押股份数量、质押股份市值等字段的详细信息
* /services/open/mr/stockPledge/trend/2.0
*@author deament
**/

public class MrStockPledge implements Serializable{

    /**
     *    
    **/
    @JSONField(name="trend")
    private List<MrStockPledgeTrend> trend;


    /**
    *   设置 
    **/
    public void setTrend(List<MrStockPledgeTrend> trend) {
      this.trend = trend;
    }
    /**
    *   获取 
    **/
    public List<MrStockPledgeTrend> getTrend() {
      return trend;
    }



}

