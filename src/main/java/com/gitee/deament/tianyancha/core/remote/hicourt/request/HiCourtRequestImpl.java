package com.gitee.deament.tianyancha.core.remote.hicourt.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hicourt.entity.HiCourt;
import com.gitee.deament.tianyancha.core.remote.hicourt.dto.HiCourtDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史法院公告
* 可以通过公司名称或ID获取企业历史的法院公告，历史法院公告信息包括执行法院、案件内容、公告类型、刊登日期、公司名、当事人等字段信息
* /services/open/hi/court/2.0
*@author deament
**/
@Component("hiCourtRequestImpl")
public class HiCourtRequestImpl extends BaseRequestImpl<HiCourt,HiCourtDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/court/2.0";
    }
}

