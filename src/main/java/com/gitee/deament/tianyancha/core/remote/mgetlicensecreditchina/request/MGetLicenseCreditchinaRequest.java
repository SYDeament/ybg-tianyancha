package com.gitee.deament.tianyancha.core.remote.mgetlicensecreditchina.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mgetlicensecreditchina.entity.MGetLicenseCreditchina;
import com.gitee.deament.tianyancha.core.remote.mgetlicensecreditchina.dto.MGetLicenseCreditchinaDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*行政许可-其他来源
* 可以通过公司名称或ID获取企业行政许可信息，企业行政许可信息包括行政许可决定文书号、许可内容、许可机关、审核类型等字段的详细信息
* /services/open/m/getLicenseCreditchina/2.0
*@author deament
**/

public interface MGetLicenseCreditchinaRequest extends BaseRequest<MGetLicenseCreditchinaDTO ,MGetLicenseCreditchina>{

}

