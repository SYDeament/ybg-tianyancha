package com.gitee.deament.tianyancha.core.remote.bqproject.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-工程项目详情
* 属于BqProject
* /services/open/bq/project/detail/2.0
*@author deament
**/

public class BqProjectResult implements Serializable{

    /**
     *    面积（平方米）
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    排序
    **/
    @JSONField(name="index")
    private String index;
    /**
     *    施工许可证编号
    **/
    @JSONField(name="constructionPermitNo")
    private String constructionPermitNo;
    /**
     *    合同金额（万元）
    **/
    @JSONField(name="contractAmount")
    private String contractAmount;
    /**
     *    发证日期
    **/
    @JSONField(name="issueDate")
    private String issueDate;
    /**
     *    省级施工许可证编号
    **/
    @JSONField(name="provincialConstructionPermitNo")
    private String provincialConstructionPermitNo;


    /**
    *   设置 面积（平方米）
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 面积（平方米）
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 排序
    **/
    public void setIndex(String index) {
      this.index = index;
    }
    /**
    *   获取 排序
    **/
    public String getIndex() {
      return index;
    }
    /**
    *   设置 施工许可证编号
    **/
    public void setConstructionPermitNo(String constructionPermitNo) {
      this.constructionPermitNo = constructionPermitNo;
    }
    /**
    *   获取 施工许可证编号
    **/
    public String getConstructionPermitNo() {
      return constructionPermitNo;
    }
    /**
    *   设置 合同金额（万元）
    **/
    public void setContractAmount(String contractAmount) {
      this.contractAmount = contractAmount;
    }
    /**
    *   获取 合同金额（万元）
    **/
    public String getContractAmount() {
      return contractAmount;
    }
    /**
    *   设置 发证日期
    **/
    public void setIssueDate(String issueDate) {
      this.issueDate = issueDate;
    }
    /**
    *   获取 发证日期
    **/
    public String getIssueDate() {
      return issueDate;
    }
    /**
    *   设置 省级施工许可证编号
    **/
    public void setProvincialConstructionPermitNo(String provincialConstructionPermitNo) {
      this.provincialConstructionPermitNo = provincialConstructionPermitNo;
    }
    /**
    *   获取 省级施工许可证编号
    **/
    public String getProvincialConstructionPermitNo() {
      return provincialConstructionPermitNo;
    }



}

