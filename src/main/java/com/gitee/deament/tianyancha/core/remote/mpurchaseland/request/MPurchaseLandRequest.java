package com.gitee.deament.tianyancha.core.remote.mpurchaseland.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mpurchaseland.entity.MPurchaseLand;
import com.gitee.deament.tianyancha.core.remote.mpurchaseland.dto.MPurchaseLandDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*购地信息
* 可以通过公司名称或ID获取企业购地信用信息，企业购地信用信息包括土地坐落、土地用途、总面积、供应方式等字段的详细信息
* /services/open/m/purchaseLand/2.0
*@author deament
**/

public interface MPurchaseLandRequest extends BaseRequest<MPurchaseLandDTO ,MPurchaseLand>{

}

