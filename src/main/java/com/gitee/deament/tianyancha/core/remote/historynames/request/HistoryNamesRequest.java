package com.gitee.deament.tianyancha.core.remote.historynames.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.historynames.entity.HistoryNames;
import com.gitee.deament.tianyancha.core.remote.historynames.dto.HistoryNamesDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业曾用名
* 可以通过公司名称获取企业曾用名
* /services/v4/open/historyNames
*@author deament
**/

public interface HistoryNamesRequest extends BaseRequest<HistoryNamesDTO ,HistoryNames>{

}

