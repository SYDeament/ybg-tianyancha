package com.gitee.deament.tianyancha.core.remote.mlandtransfer.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mlandtransfer.entity.MLandTransfer;
import com.gitee.deament.tianyancha.core.remote.mlandtransfer.dto.MLandTransferDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*土地转让详情
* 可以通过土地转让ID获取企业土地转让详情，企业土地转让详情包括土地坐落、原/现土地使用人、面积、转让价格、转让方式等字段的详细信息
* /services/open/m/landTransfer/detail/2.0
*@author deament
**/

public interface MLandTransferRequest extends BaseRequest<MLandTransferDTO ,MLandTransfer>{

}

