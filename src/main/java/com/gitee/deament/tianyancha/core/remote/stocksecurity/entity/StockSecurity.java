package com.gitee.deament.tianyancha.core.remote.stocksecurity.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*证券信息
* 可以通过公司名称或ID获取上市公司证券信息，证券信息包括A股代码、A股简称、律师事务所、会计师事务所、股票类型等
* /services/open/stock/security/2.0
*@author deament
**/

public class StockSecurity implements Serializable{

    /**
     *    律师事务所
    **/
    @JSONField(name="law_firm_name")
    private String law_firm_name;
    /**
     *    股票类型
    **/
    @JSONField(name="sec_type")
    private String sec_type;
    /**
     *    会计师事务所
    **/
    @JSONField(name="accounting_firm_name")
    private String accounting_firm_name;
    /**
     *    A股简称
    **/
    @JSONField(name="ASTOCK_CN")
    private String ASTOCK_CN;
    /**
     *    A股代码
    **/
    @JSONField(name="ASTOCK_CODE")
    private String ASTOCK_CODE;


    /**
    *   设置 律师事务所
    **/
    public void setLaw_firm_name(String law_firm_name) {
      this.law_firm_name = law_firm_name;
    }
    /**
    *   获取 律师事务所
    **/
    public String getLaw_firm_name() {
      return law_firm_name;
    }
    /**
    *   设置 股票类型
    **/
    public void setSec_type(String sec_type) {
      this.sec_type = sec_type;
    }
    /**
    *   获取 股票类型
    **/
    public String getSec_type() {
      return sec_type;
    }
    /**
    *   设置 会计师事务所
    **/
    public void setAccounting_firm_name(String accounting_firm_name) {
      this.accounting_firm_name = accounting_firm_name;
    }
    /**
    *   获取 会计师事务所
    **/
    public String getAccounting_firm_name() {
      return accounting_firm_name;
    }
    /**
    *   设置 A股简称
    **/
    public void setASTOCK_CN(String ASTOCK_CN) {
      this.ASTOCK_CN = ASTOCK_CN;
    }
    /**
    *   获取 A股简称
    **/
    public String getASTOCK_CN() {
      return ASTOCK_CN;
    }
    /**
    *   设置 A股代码
    **/
    public void setASTOCK_CODE(String ASTOCK_CODE) {
      this.ASTOCK_CODE = ASTOCK_CODE;
    }
    /**
    *   获取 A股代码
    **/
    public String getASTOCK_CODE() {
      return ASTOCK_CODE;
    }



}

