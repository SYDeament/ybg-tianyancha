package com.gitee.deament.tianyancha.core.remote.pfintegrity.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*诚信信息
* 可以通过公司名称或ID获取相关私募基金诚信信息，包括机构信息最后更新时间、特别提示信息
* /services/open/pf/integrity/2.0
*@author deament
**/

public class PfIntegrity implements Serializable{

    /**
     *    机构信息最后更新时间
    **/
    @JSONField(name="amac_last_updatetime")
    private String amac_last_updatetime;
    /**
     *    企业id
    **/
    @JSONField(name="graph_id")
    private Integer graph_id;
    /**
     *    特别提示信息
    **/
    @JSONField(name="special_msg")
    private String special_msg;


    /**
    *   设置 机构信息最后更新时间
    **/
    public void setAmac_last_updatetime(String amac_last_updatetime) {
      this.amac_last_updatetime = amac_last_updatetime;
    }
    /**
    *   获取 机构信息最后更新时间
    **/
    public String getAmac_last_updatetime() {
      return amac_last_updatetime;
    }
    /**
    *   设置 企业id
    **/
    public void setGraph_id(Integer graph_id) {
      this.graph_id = graph_id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getGraph_id() {
      return graph_id;
    }
    /**
    *   设置 特别提示信息
    **/
    public void setSpecial_msg(String special_msg) {
      this.special_msg = special_msg;
    }
    /**
    *   获取 特别提示信息
    **/
    public String getSpecial_msg() {
      return special_msg;
    }



}

