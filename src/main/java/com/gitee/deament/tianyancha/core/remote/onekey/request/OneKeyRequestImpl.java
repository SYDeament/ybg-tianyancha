package com.gitee.deament.tianyancha.core.remote.onekey.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.onekey.entity.OneKey;
import com.gitee.deament.tianyancha.core.remote.onekey.dto.OneKeyDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*企业图谱
* 可以通过公司ID获取企业关系图谱，包含法人、股东、高管、对外投资、分支机构列表
* /services/v4/open/oneKey/c
*@author deament
**/
@Component("oneKeyRequestImpl")
public class OneKeyRequestImpl extends BaseRequestImpl<OneKey,OneKeyDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/oneKey/c";
    }
}

