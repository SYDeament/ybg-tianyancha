package com.gitee.deament.tianyancha.core.remote.pfintegrity.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfintegrity.entity.PfIntegrity;
import com.gitee.deament.tianyancha.core.remote.pfintegrity.dto.PfIntegrityDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*诚信信息
* 可以通过公司名称或ID获取相关私募基金诚信信息，包括机构信息最后更新时间、特别提示信息
* /services/open/pf/integrity/2.0
*@author deament
**/
@Component("pfIntegrityRequestImpl")
public class PfIntegrityRequestImpl extends BaseRequestImpl<PfIntegrity,PfIntegrityDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/pf/integrity/2.0";
    }
}

