package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype29 implements Serializable{

    /**
     *    来源ID
    **/
    @JSONField(name="sourceId")
    private String sourceId;
    /**
     *    无用
    **/
    @JSONField(name="uniqueHash")
    private String uniqueHash;
    /**
     *    拍卖起止时间
    **/
    @JSONField(name="scopeDate")
    private String scopeDate;
    /**
     *    公布时间
    **/
    @JSONField(name="pub_time")
    private String pub_time;
    /**
     *    无意义
    **/
    @JSONField(name="recordHash")
    private String recordHash;
    /**
     *    无用
    **/
    @JSONField(name="eventTime")
    private String eventTime;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    
    **/
    @JSONField(name="detail")
    private List<RiskDetailDetail> detail;
    /**
     *    标的名称
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    简介
    **/
    @JSONField(name="introduction")
    private String introduction;
    /**
     *    公告url
    **/
    @JSONField(name="url")
    private String url;


    /**
    *   设置 来源ID
    **/
    public void setSourceId(String sourceId) {
      this.sourceId = sourceId;
    }
    /**
    *   获取 来源ID
    **/
    public String getSourceId() {
      return sourceId;
    }
    /**
    *   设置 无用
    **/
    public void setUniqueHash(String uniqueHash) {
      this.uniqueHash = uniqueHash;
    }
    /**
    *   获取 无用
    **/
    public String getUniqueHash() {
      return uniqueHash;
    }
    /**
    *   设置 拍卖起止时间
    **/
    public void setScopeDate(String scopeDate) {
      this.scopeDate = scopeDate;
    }
    /**
    *   获取 拍卖起止时间
    **/
    public String getScopeDate() {
      return scopeDate;
    }
    /**
    *   设置 公布时间
    **/
    public void setPub_time(String pub_time) {
      this.pub_time = pub_time;
    }
    /**
    *   获取 公布时间
    **/
    public String getPub_time() {
      return pub_time;
    }
    /**
    *   设置 无意义
    **/
    public void setRecordHash(String recordHash) {
      this.recordHash = recordHash;
    }
    /**
    *   获取 无意义
    **/
    public String getRecordHash() {
      return recordHash;
    }
    /**
    *   设置 无用
    **/
    public void setEventTime(String eventTime) {
      this.eventTime = eventTime;
    }
    /**
    *   获取 无用
    **/
    public String getEventTime() {
      return eventTime;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 
    **/
    public void setDetail(List<RiskDetailDetail> detail) {
      this.detail = detail;
    }
    /**
    *   获取 
    **/
    public List<RiskDetailDetail> getDetail() {
      return detail;
    }
    /**
    *   设置 标的名称
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标的名称
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 简介
    **/
    public void setIntroduction(String introduction) {
      this.introduction = introduction;
    }
    /**
    *   获取 简介
    **/
    public String getIntroduction() {
      return introduction;
    }
    /**
    *   设置 公告url
    **/
    public void setUrl(String url) {
      this.url = url;
    }
    /**
    *   获取 公告url
    **/
    public String getUrl() {
      return url;
    }



}

