package com.gitee.deament.tianyancha.core.enums;

/**
 * 天眼查
 */
public enum TYCResultCodeEnum {
    /**
     * 请求成功
     */
    OK(0, "请求成功"),
    /**
     * 无数据
     */
    EMPTY_DATA(300000, "无数据"),
    /**
     * 请求失败
     */
    REQUEST_FAIL(300001, "请求失败"),
    /**
     * 账号失效
     */
    ACCOUNT_FAIL(300002, "账号失效"),
    /**
     * 账号过期
     */
    ACCOUNT_EXPIRE(300003, "账号过期"),
    /**
     * 访问频率过快
     */
    REQUEST_FAST(300004, "访问频率过快"),
    /**
     * 无权限访问此api
     */
    FOBIDEN(300005, "无权限访问此api"),
    /**
     * 余额不足
     */
    NO_MONEY(300006, "余额不足"),
    /**
     * 剩余次数不足
     */
    NO_TIMES(300007, "剩余次数不足"),
    /**
     * 缺少必要参数
     */
    MISS_PARAMS(300008, "缺少必要参数"),
    /**
     * 账号信息有误
     */
    ACCOUNT_INFO_ERROR(300009, "账号信息有误"),
    /**
     * URL不存在
     */
    URL_UNEXIST(300010, "URL不存在"),
    /**
     * 此IP无权限访问此api
     */
    IP_ERROR(300011, "此IP无权限访问此api"),
    /**
     * 报告生成中
     */
    REPORT_GENING(300012, "报告生成中"),
    ;

    int code;
    String desc;

    TYCResultCodeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
