package com.gitee.deament.tianyancha.core.remote.oistats.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*统计分析
* 属于OiStats
* /services/open/oi/stats/2.0
*@author deament
**/

public class OiStatsMonth_count implements Serializable{

    /**
     *    月份
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    数量
    **/
    @JSONField(name="value")
    private String value;


    /**
    *   设置 月份
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 月份
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 数量
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 数量
    **/
    public String getValue() {
      return value;
    }



}

