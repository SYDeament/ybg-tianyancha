package com.gitee.deament.tianyancha.core.remote.mrbriefcancel.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrbriefcancel.entity.MrBriefCancelResult;
import com.gitee.deament.tianyancha.core.remote.mrbriefcancel.entity.MrBriefCancelAnnouncement;
import com.gitee.deament.tianyancha.core.remote.mrbriefcancel.entity.MrBriefCancelObjection;
/**
*简易注销
* 可以通过公司名称或ID获取企业简易注销公告信息，企业简易注销公告信息包括全体投资人承诺书、异议信息、简易注销结果等字段的详细信息
* /services/open/mr/briefCancel/2.0
*@author deament
**/

public class MrBriefCancel implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private MrBriefCancelResult result;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="announcement")
    private MrBriefCancelAnnouncement announcement;
    /**
     *    
    **/
    @JSONField(name="objection")
    private MrBriefCancelObjection objection;


    /**
    *   设置 
    **/
    public void setResult(MrBriefCancelResult result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public MrBriefCancelResult getResult() {
      return result;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setAnnouncement(MrBriefCancelAnnouncement announcement) {
      this.announcement = announcement;
    }
    /**
    *   获取 
    **/
    public MrBriefCancelAnnouncement getAnnouncement() {
      return announcement;
    }
    /**
    *   设置 
    **/
    public void setObjection(MrBriefCancelObjection objection) {
      this.objection = objection;
    }
    /**
    *   获取 
    **/
    public MrBriefCancelObjection getObjection() {
      return objection;
    }



}

