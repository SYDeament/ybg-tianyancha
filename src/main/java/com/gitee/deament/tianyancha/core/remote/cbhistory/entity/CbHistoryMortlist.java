package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryMortlist implements Serializable{

    /**
     *    被担保债权数额
    **/
    @JSONField(name="amount")
    private String amount;
    /**
     *    注销日期
    **/
    @JSONField(name="cancelDate")
    private String cancelDate;
    /**
     *    抵押物
    **/
    @JSONField(name="pawnList")
    private List<CbHistoryPawnList> pawnList;
    /**
     *    变更记录
    **/
    @JSONField(name="changeList")
    private List<CbHistoryChangeList> changeList;
    /**
     *    公示日期
    **/
    @JSONField(name="publishDate")
    private String publishDate;
    /**
     *    登记日期
    **/
    @JSONField(name="regDate")
    private String regDate;
    /**
     *    备注
    **/
    @JSONField(name="remark")
    private String remark;
    /**
     *    被担保债权种类
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    概况备注
    **/
    @JSONField(name="overviewRemark")
    private String overviewRemark;
    /**
     *    概况债务人履行债务的期限
    **/
    @JSONField(name="overviewTerm")
    private String overviewTerm;
    /**
     *    抵押人
    **/
    @JSONField(name="peopleList")
    private List<CbHistoryPeopleList> peopleList;
    /**
     *    登记编号
    **/
    @JSONField(name="regNum")
    private String regNum;
    /**
     *    登记机关
    **/
    @JSONField(name="regDepartment")
    private String regDepartment;
    /**
     *    概况数额
    **/
    @JSONField(name="overviewAmount")
    private String overviewAmount;
    /**
     *    概况担保的范围
    **/
    @JSONField(name="overviewScope")
    private String overviewScope;
    /**
     *    担保范围
    **/
    @JSONField(name="scope")
    private String scope;
    /**
     *    概况种类
    **/
    @JSONField(name="overviewType")
    private String overviewType;
    /**
     *    债务人履行债务的期限
    **/
    @JSONField(name="term")
    private String term;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    注销原因
    **/
    @JSONField(name="cancelReason")
    private String cancelReason;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;
    /**
     *    状态
    **/
    @JSONField(name="status")
    private String status;


    /**
    *   设置 被担保债权数额
    **/
    public void setAmount(String amount) {
      this.amount = amount;
    }
    /**
    *   获取 被担保债权数额
    **/
    public String getAmount() {
      return amount;
    }
    /**
    *   设置 注销日期
    **/
    public void setCancelDate(String cancelDate) {
      this.cancelDate = cancelDate;
    }
    /**
    *   获取 注销日期
    **/
    public String getCancelDate() {
      return cancelDate;
    }
    /**
    *   设置 抵押物
    **/
    public void setPawnList(List<CbHistoryPawnList> pawnList) {
      this.pawnList = pawnList;
    }
    /**
    *   获取 抵押物
    **/
    public List<CbHistoryPawnList> getPawnList() {
      return pawnList;
    }
    /**
    *   设置 变更记录
    **/
    public void setChangeList(List<CbHistoryChangeList> changeList) {
      this.changeList = changeList;
    }
    /**
    *   获取 变更记录
    **/
    public List<CbHistoryChangeList> getChangeList() {
      return changeList;
    }
    /**
    *   设置 公示日期
    **/
    public void setPublishDate(String publishDate) {
      this.publishDate = publishDate;
    }
    /**
    *   获取 公示日期
    **/
    public String getPublishDate() {
      return publishDate;
    }
    /**
    *   设置 登记日期
    **/
    public void setRegDate(String regDate) {
      this.regDate = regDate;
    }
    /**
    *   获取 登记日期
    **/
    public String getRegDate() {
      return regDate;
    }
    /**
    *   设置 备注
    **/
    public void setRemark(String remark) {
      this.remark = remark;
    }
    /**
    *   获取 备注
    **/
    public String getRemark() {
      return remark;
    }
    /**
    *   设置 被担保债权种类
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 被担保债权种类
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 概况备注
    **/
    public void setOverviewRemark(String overviewRemark) {
      this.overviewRemark = overviewRemark;
    }
    /**
    *   获取 概况备注
    **/
    public String getOverviewRemark() {
      return overviewRemark;
    }
    /**
    *   设置 概况债务人履行债务的期限
    **/
    public void setOverviewTerm(String overviewTerm) {
      this.overviewTerm = overviewTerm;
    }
    /**
    *   获取 概况债务人履行债务的期限
    **/
    public String getOverviewTerm() {
      return overviewTerm;
    }
    /**
    *   设置 抵押人
    **/
    public void setPeopleList(List<CbHistoryPeopleList> peopleList) {
      this.peopleList = peopleList;
    }
    /**
    *   获取 抵押人
    **/
    public List<CbHistoryPeopleList> getPeopleList() {
      return peopleList;
    }
    /**
    *   设置 登记编号
    **/
    public void setRegNum(String regNum) {
      this.regNum = regNum;
    }
    /**
    *   获取 登记编号
    **/
    public String getRegNum() {
      return regNum;
    }
    /**
    *   设置 登记机关
    **/
    public void setRegDepartment(String regDepartment) {
      this.regDepartment = regDepartment;
    }
    /**
    *   获取 登记机关
    **/
    public String getRegDepartment() {
      return regDepartment;
    }
    /**
    *   设置 概况数额
    **/
    public void setOverviewAmount(String overviewAmount) {
      this.overviewAmount = overviewAmount;
    }
    /**
    *   获取 概况数额
    **/
    public String getOverviewAmount() {
      return overviewAmount;
    }
    /**
    *   设置 概况担保的范围
    **/
    public void setOverviewScope(String overviewScope) {
      this.overviewScope = overviewScope;
    }
    /**
    *   获取 概况担保的范围
    **/
    public String getOverviewScope() {
      return overviewScope;
    }
    /**
    *   设置 担保范围
    **/
    public void setScope(String scope) {
      this.scope = scope;
    }
    /**
    *   获取 担保范围
    **/
    public String getScope() {
      return scope;
    }
    /**
    *   设置 概况种类
    **/
    public void setOverviewType(String overviewType) {
      this.overviewType = overviewType;
    }
    /**
    *   获取 概况种类
    **/
    public String getOverviewType() {
      return overviewType;
    }
    /**
    *   设置 债务人履行债务的期限
    **/
    public void setTerm(String term) {
      this.term = term;
    }
    /**
    *   获取 债务人履行债务的期限
    **/
    public String getTerm() {
      return term;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 注销原因
    **/
    public void setCancelReason(String cancelReason) {
      this.cancelReason = cancelReason;
    }
    /**
    *   获取 注销原因
    **/
    public String getCancelReason() {
      return cancelReason;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }
    /**
    *   设置 状态
    **/
    public void setStatus(String status) {
      this.status = status;
    }
    /**
    *   获取 状态
    **/
    public String getStatus() {
      return status;
    }



}

