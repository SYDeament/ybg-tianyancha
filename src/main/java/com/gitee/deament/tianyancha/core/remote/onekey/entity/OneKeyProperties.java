package com.gitee.deament.tianyancha.core.remote.onekey.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业图谱
* 属于OneKey
* /services/v4/open/oneKey/c
*@author deament
**/

public class OneKeyProperties implements Serializable{

    /**
     *    节点简称
    **/
    @JSONField(name="aias")
    private String aias;
    /**
     *    节点名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    节点logo地址
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    节点类型
    **/
    @JSONField(name="ntype")
    private String ntype;


    /**
    *   设置 节点简称
    **/
    public void setAias(String aias) {
      this.aias = aias;
    }
    /**
    *   获取 节点简称
    **/
    public String getAias() {
      return aias;
    }
    /**
    *   设置 节点名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 节点名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 节点logo地址
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 节点logo地址
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 节点类型
    **/
    public void setNtype(String ntype) {
      this.ntype = ntype;
    }
    /**
    *   获取 节点类型
    **/
    public String getNtype() {
      return ntype;
    }



}

