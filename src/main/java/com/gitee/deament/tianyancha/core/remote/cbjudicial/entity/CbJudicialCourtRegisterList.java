package com.gitee.deament.tianyancha.core.remote.cbjudicial.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*司法风险
* 属于CbJudicial
* /services/open/cb/judicial/2.0
*@author deament
**/

public class CbJudicialCourtRegisterList implements Serializable{

    /**
     *    地区
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    公诉人/原告/上诉人/申请人
    **/
    @JSONField(name="plaintiff")
    private String plaintiff;
    /**
     *    当事人id
    **/
    @JSONField(name="litigantGids")
    private String litigantGids;
    /**
     *    当事人
    **/
    @JSONField(name="litigant")
    private String litigant;
    /**
     *    立案时间
    **/
    @JSONField(name="filingDate")
    private String filingDate;
    /**
     *    案件状态
    **/
    @JSONField(name="caseStatus")
    private String caseStatus;
    /**
     *    法官
    **/
    @JSONField(name="assistant")
    private String assistant;
    /**
     *    来源
    **/
    @JSONField(name="source")
    private String source;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    案号
    **/
    @JSONField(name="caseNo")
    private String caseNo;
    /**
     *    案件详情
    **/
    @JSONField(name="content")
    private String content;
    /**
     *    案由
    **/
    @JSONField(name="caseReason")
    private String caseReason;
    /**
     *    案件类型
    **/
    @JSONField(name="caseType")
    private String caseType;
    /**
     *    结案时间
    **/
    @JSONField(name="closeDate")
    private String closeDate;
    /**
     *    源链接
    **/
    @JSONField(name="sourceUrl")
    private String sourceUrl;
    /**
     *    第三人
    **/
    @JSONField(name="third")
    private String third;
    /**
     *    被告人/被告/被上诉人/被申请人
    **/
    @JSONField(name="defendant")
    private String defendant;
    /**
     *    创建时间
    **/
    @JSONField(name="createTime")
    private String createTime;
    /**
     *    法官助理
    **/
    @JSONField(name="juge")
    private String juge;
    /**
     *    开始时间
    **/
    @JSONField(name="startTime")
    private String startTime;
    /**
     *    承办部门
    **/
    @JSONField(name="department")
    private String department;


    /**
    *   设置 地区
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 地区
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 公诉人/原告/上诉人/申请人
    **/
    public void setPlaintiff(String plaintiff) {
      this.plaintiff = plaintiff;
    }
    /**
    *   获取 公诉人/原告/上诉人/申请人
    **/
    public String getPlaintiff() {
      return plaintiff;
    }
    /**
    *   设置 当事人id
    **/
    public void setLitigantGids(String litigantGids) {
      this.litigantGids = litigantGids;
    }
    /**
    *   获取 当事人id
    **/
    public String getLitigantGids() {
      return litigantGids;
    }
    /**
    *   设置 当事人
    **/
    public void setLitigant(String litigant) {
      this.litigant = litigant;
    }
    /**
    *   获取 当事人
    **/
    public String getLitigant() {
      return litigant;
    }
    /**
    *   设置 立案时间
    **/
    public void setFilingDate(String filingDate) {
      this.filingDate = filingDate;
    }
    /**
    *   获取 立案时间
    **/
    public String getFilingDate() {
      return filingDate;
    }
    /**
    *   设置 案件状态
    **/
    public void setCaseStatus(String caseStatus) {
      this.caseStatus = caseStatus;
    }
    /**
    *   获取 案件状态
    **/
    public String getCaseStatus() {
      return caseStatus;
    }
    /**
    *   设置 法官
    **/
    public void setAssistant(String assistant) {
      this.assistant = assistant;
    }
    /**
    *   获取 法官
    **/
    public String getAssistant() {
      return assistant;
    }
    /**
    *   设置 来源
    **/
    public void setSource(String source) {
      this.source = source;
    }
    /**
    *   获取 来源
    **/
    public String getSource() {
      return source;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 案号
    **/
    public void setCaseNo(String caseNo) {
      this.caseNo = caseNo;
    }
    /**
    *   获取 案号
    **/
    public String getCaseNo() {
      return caseNo;
    }
    /**
    *   设置 案件详情
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 案件详情
    **/
    public String getContent() {
      return content;
    }
    /**
    *   设置 案由
    **/
    public void setCaseReason(String caseReason) {
      this.caseReason = caseReason;
    }
    /**
    *   获取 案由
    **/
    public String getCaseReason() {
      return caseReason;
    }
    /**
    *   设置 案件类型
    **/
    public void setCaseType(String caseType) {
      this.caseType = caseType;
    }
    /**
    *   获取 案件类型
    **/
    public String getCaseType() {
      return caseType;
    }
    /**
    *   设置 结案时间
    **/
    public void setCloseDate(String closeDate) {
      this.closeDate = closeDate;
    }
    /**
    *   获取 结案时间
    **/
    public String getCloseDate() {
      return closeDate;
    }
    /**
    *   设置 源链接
    **/
    public void setSourceUrl(String sourceUrl) {
      this.sourceUrl = sourceUrl;
    }
    /**
    *   获取 源链接
    **/
    public String getSourceUrl() {
      return sourceUrl;
    }
    /**
    *   设置 第三人
    **/
    public void setThird(String third) {
      this.third = third;
    }
    /**
    *   获取 第三人
    **/
    public String getThird() {
      return third;
    }
    /**
    *   设置 被告人/被告/被上诉人/被申请人
    **/
    public void setDefendant(String defendant) {
      this.defendant = defendant;
    }
    /**
    *   获取 被告人/被告/被上诉人/被申请人
    **/
    public String getDefendant() {
      return defendant;
    }
    /**
    *   设置 创建时间
    **/
    public void setCreateTime(String createTime) {
      this.createTime = createTime;
    }
    /**
    *   获取 创建时间
    **/
    public String getCreateTime() {
      return createTime;
    }
    /**
    *   设置 法官助理
    **/
    public void setJuge(String juge) {
      this.juge = juge;
    }
    /**
    *   获取 法官助理
    **/
    public String getJuge() {
      return juge;
    }
    /**
    *   设置 开始时间
    **/
    public void setStartTime(String startTime) {
      this.startTime = startTime;
    }
    /**
    *   获取 开始时间
    **/
    public String getStartTime() {
      return startTime;
    }
    /**
    *   设置 承办部门
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 承办部门
    **/
    public String getDepartment() {
      return department;
    }



}

