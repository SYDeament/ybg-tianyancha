package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryShareHolderList implements Serializable{

    /**
     *    认缴
    **/
    @JSONField(name="capital")
    private List<CbHistoryCapital> capital;
    /**
     *    股东名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    实缴
    **/
    @JSONField(name="capitalActl")
    private String capitalActl;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    
    **/
    @JSONField(name="type")
    private String type;


    /**
    *   设置 认缴
    **/
    public void setCapital(List<CbHistoryCapital> capital) {
      this.capital = capital;
    }
    /**
    *   获取 认缴
    **/
    public List<CbHistoryCapital> getCapital() {
      return capital;
    }
    /**
    *   设置 股东名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 股东名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 实缴
    **/
    public void setCapitalActl(String capitalActl) {
      this.capitalActl = capitalActl;
    }
    /**
    *   获取 实缴
    **/
    public String getCapitalActl() {
      return capitalActl;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 
    **/
    public String getType() {
      return type;
    }



}

