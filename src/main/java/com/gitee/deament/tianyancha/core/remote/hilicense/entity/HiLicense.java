package com.gitee.deament.tianyancha.core.remote.hilicense.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hilicense.entity.HiLicenseItems;
/**
*历史行政许可-其他来源
* 可以通过公司名称或ID获取企业历史的行政许可信息，历史行政许可信息包括行政许可决定文书号、许可内容、许可机关、审核类型等字段信息
* /services/open/hi/license/creditChina/2.0
*@author deament
**/

public class HiLicense implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiLicenseItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiLicenseItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiLicenseItems> getItems() {
      return items;
    }



}

