package com.gitee.deament.tianyancha.core.remote.jrendcase.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrendcase.entity.JrEndCase;
import com.gitee.deament.tianyancha.core.remote.jrendcase.dto.JrEndCaseDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*终本案件
* 可以通过公司名称或ID获取终本案件信息，终本案件信息包括执行法院、案件内容、被执行人名称等字段的详细信息
* /services/open/jr/endCase/2.0
*@author deament
**/
@Component("jrEndCaseRequestImpl")
public class JrEndCaseRequestImpl extends BaseRequestImpl<JrEndCase,JrEndCaseDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/jr/endCase/2.0";
    }
}

