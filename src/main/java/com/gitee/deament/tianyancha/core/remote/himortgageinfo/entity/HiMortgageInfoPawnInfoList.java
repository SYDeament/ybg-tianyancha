package com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史动产抵押
* 属于HiMortgageInfo
* /services/open/hi/mortgageInfo/2.0
*@author deament
**/

public class HiMortgageInfoPawnInfoList implements Serializable{

    /**
     *    名称
    **/
    @JSONField(name="pawnName")
    private String pawnName;
    /**
     *    所有权归属
    **/
    @JSONField(name="ownership")
    private String ownership;
    /**
     *    数量、质量、状况、所在地等情况
    **/
    @JSONField(name="detail")
    private String detail;


    /**
    *   设置 名称
    **/
    public void setPawnName(String pawnName) {
      this.pawnName = pawnName;
    }
    /**
    *   获取 名称
    **/
    public String getPawnName() {
      return pawnName;
    }
    /**
    *   设置 所有权归属
    **/
    public void setOwnership(String ownership) {
      this.ownership = ownership;
    }
    /**
    *   获取 所有权归属
    **/
    public String getOwnership() {
      return ownership;
    }
    /**
    *   设置 数量、质量、状况、所在地等情况
    **/
    public void setDetail(String detail) {
      this.detail = detail;
    }
    /**
    *   获取 数量、质量、状况、所在地等情况
    **/
    public String getDetail() {
      return detail;
    }



}

