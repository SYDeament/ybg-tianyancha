package com.gitee.deament.tianyancha.core.remote.ipricp.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.ipricp.entity.IprIcpItems;
/**
*网站备案
* 可以通过公司名称或ID获取网站备案的有关信息，包括网站名称、网站首页、域名、网站备案/许可证号等字段信息
* /services/open/ipr/icp/3.0
*@author deament
**/

public class IprIcp implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IprIcpItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IprIcpItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IprIcpItems> getItems() {
      return items;
    }



}

