package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcObjectionList implements Serializable{

    /**
     *    异议内容
    **/
    @JSONField(name="objection_content")
    private String objection_content;
    /**
     *    异议时间
    **/
    @JSONField(name="objection_date")
    private String objection_date;
    /**
     *    异议申请人
    **/
    @JSONField(name="objection_apply_person")
    private String objection_apply_person;


    /**
    *   设置 异议内容
    **/
    public void setObjection_content(String objection_content) {
      this.objection_content = objection_content;
    }
    /**
    *   获取 异议内容
    **/
    public String getObjection_content() {
      return objection_content;
    }
    /**
    *   设置 异议时间
    **/
    public void setObjection_date(String objection_date) {
      this.objection_date = objection_date;
    }
    /**
    *   获取 异议时间
    **/
    public String getObjection_date() {
      return objection_date;
    }
    /**
    *   设置 异议申请人
    **/
    public void setObjection_apply_person(String objection_apply_person) {
      this.objection_apply_person = objection_apply_person;
    }
    /**
    *   获取 异议申请人
    **/
    public String getObjection_apply_person() {
      return objection_apply_person;
    }



}

