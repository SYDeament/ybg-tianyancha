package com.gitee.deament.tianyancha.core.remote.stockbalancesheet.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockbalancesheet.entity.StockBalanceSheet;
import com.gitee.deament.tianyancha.core.remote.stockbalancesheet.dto.StockBalanceSheetDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*资产负债表
* 可以通过公司名称或ID获取上市公司资产负债表数据，资产负债表数据包括流动资产、流动资产合计、非流动资产等
* /services/open/stock/balanceSheet/2.0
*@author deament
**/
@Component("stockBalanceSheetRequestImpl")
public class StockBalanceSheetRequestImpl extends BaseRequestImpl<StockBalanceSheet,StockBalanceSheetDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/balanceSheet/2.0";
    }
}

