package com.gitee.deament.tianyancha.core.remote.riskriskinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.riskriskinfo.entity.RiskRiskInfoList;
import com.gitee.deament.tianyancha.core.remote.riskriskinfo.entity.RiskRiskInfoList;
import com.gitee.deament.tianyancha.core.remote.riskriskinfo.entity.RiskRiskInfoRiskList;
/**
*企业天眼风险
* 可以通过公司名称或ID获取企业相关天眼风险列表，包括企业自身/周边/预警风险信息
* /services/open/risk/riskInfo/2.0
*@author deament
**/

public class RiskRiskInfo implements Serializable{

    /**
     *    风险等级
    **/
    @JSONField(name="riskLevel")
    private String riskLevel;
    /**
     *    
    **/
    @JSONField(name="riskList")
    private List<RiskRiskInfoRiskList> riskList;


    /**
    *   设置 风险等级
    **/
    public void setRiskLevel(String riskLevel) {
      this.riskLevel = riskLevel;
    }
    /**
    *   获取 风险等级
    **/
    public String getRiskLevel() {
      return riskLevel;
    }
    /**
    *   设置 
    **/
    public void setRiskList(List<RiskRiskInfoRiskList> riskList) {
      this.riskList = riskList;
    }
    /**
    *   获取 
    **/
    public List<RiskRiskInfoRiskList> getRiskList() {
      return riskList;
    }



}

