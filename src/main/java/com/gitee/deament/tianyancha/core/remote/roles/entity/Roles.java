package com.gitee.deament.tianyancha.core.remote.roles.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.roles.entity.RolesLegalList;
import com.gitee.deament.tianyancha.core.remote.roles.entity.RolesHolderList;
import com.gitee.deament.tianyancha.core.remote.roles.entity.RolesOfficeList;
/**
*人员所有角色
* 可以通过公司名称或ID和人名获取人员的所有商业角色，包括其担任法人、股东、董监高的职位信息
* /services/v4/open/roles
*@author deament
**/

public class Roles implements Serializable{

    /**
     *    法人
    **/
    @JSONField(name="legalList")
    private List<RolesLegalList> legalList;
    /**
     *    股东列表
    **/
    @JSONField(name="holderList")
    private List<RolesHolderList> holderList;
    /**
     *    高管列表
    **/
    @JSONField(name="officeList")
    private List<RolesOfficeList> officeList;


    /**
    *   设置 法人
    **/
    public void setLegalList(List<RolesLegalList> legalList) {
      this.legalList = legalList;
    }
    /**
    *   获取 法人
    **/
    public List<RolesLegalList> getLegalList() {
      return legalList;
    }
    /**
    *   设置 股东列表
    **/
    public void setHolderList(List<RolesHolderList> holderList) {
      this.holderList = holderList;
    }
    /**
    *   获取 股东列表
    **/
    public List<RolesHolderList> getHolderList() {
      return holderList;
    }
    /**
    *   设置 高管列表
    **/
    public void setOfficeList(List<RolesOfficeList> officeList) {
      this.officeList = officeList;
    }
    /**
    *   获取 高管列表
    **/
    public List<RolesOfficeList> getOfficeList() {
      return officeList;
    }



}

