package com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*上市公司企业简介
* 属于StockCompanyInfo
* /services/open/stock/companyInfo/2.0
*@author deament
**/

public class StockCompanyInfoChairman implements Serializable{

    /**
     *    类型 1公司 2 人
    **/
    @JSONField(name="cType")
    private Integer cType;
    /**
     *    董事长
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    董事长id
    **/
    @JSONField(name="id")
    private String id;


    /**
    *   设置 类型 1公司 2 人
    **/
    public void setCType(Integer cType) {
      this.cType = cType;
    }
    /**
    *   获取 类型 1公司 2 人
    **/
    public Integer getCType() {
      return cType;
    }
    /**
    *   设置 董事长
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 董事长
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 董事长id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 董事长id
    **/
    public String getId() {
      return id;
    }



}

