package com.gitee.deament.tianyancha.core.remote.stockholdingcompany.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockholdingcompany.entity.StockHoldingCompanyItems;
/**
*参股控股
* 可以通过公司名称或ID获取上市公司参股控股股东信息，参股控股股东信息包括参股控股关联公司、参股关系、参股比例、投资金额、被参股公司净利润等
* /services/open/stock/holdingCompany/2.0
*@author deament
**/

public class StockHoldingCompany implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<StockHoldingCompanyItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<StockHoldingCompanyItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<StockHoldingCompanyItems> getItems() {
      return items;
    }



}

