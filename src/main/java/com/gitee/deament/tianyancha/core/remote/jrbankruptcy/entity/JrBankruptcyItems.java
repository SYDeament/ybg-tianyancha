package com.gitee.deament.tianyancha.core.remote.jrbankruptcy.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*破产重整
* 属于JrBankruptcy
* /services/open/jr/bankruptcy/2.0
*@author deament
**/

public class JrBankruptcyItems implements Serializable{

    /**
     *    公开时间，发布时间
    **/
    @JSONField(name="submitTime")
    private String submitTime;
    /**
     *    被申请人
    **/
    @JSONField(name="respondent")
    private String respondent;
    /**
     *    uuid
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    案号
    **/
    @JSONField(name="caseNo")
    private String caseNo;
    /**
     *    案件类型
    **/
    @JSONField(name="caseType")
    private String caseType;
    /**
     *    申请人对象
    **/
    @JSONField(name="applicant")
    private List<JrBankruptcyApplicant> applicant;


    /**
    *   设置 公开时间，发布时间
    **/
    public void setSubmitTime(String submitTime) {
      this.submitTime = submitTime;
    }
    /**
    *   获取 公开时间，发布时间
    **/
    public String getSubmitTime() {
      return submitTime;
    }
    /**
    *   设置 被申请人
    **/
    public void setRespondent(String respondent) {
      this.respondent = respondent;
    }
    /**
    *   获取 被申请人
    **/
    public String getRespondent() {
      return respondent;
    }
    /**
    *   设置 uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 uuid
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 案号
    **/
    public void setCaseNo(String caseNo) {
      this.caseNo = caseNo;
    }
    /**
    *   获取 案号
    **/
    public String getCaseNo() {
      return caseNo;
    }
    /**
    *   设置 案件类型
    **/
    public void setCaseType(String caseType) {
      this.caseType = caseType;
    }
    /**
    *   获取 案件类型
    **/
    public String getCaseType() {
      return caseType;
    }
    /**
    *   设置 申请人对象
    **/
    public void setApplicant(List<JrBankruptcyApplicant> applicant) {
      this.applicant = applicant;
    }
    /**
    *   获取 申请人对象
    **/
    public List<JrBankruptcyApplicant> getApplicant() {
      return applicant;
    }



}

