package com.gitee.deament.tianyancha.core.remote.stockcorpcontactinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockcorpcontactinfo.entity.StockCorpContactInfo;
import com.gitee.deament.tianyancha.core.remote.stockcorpcontactinfo.dto.StockCorpContactInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*联系信息
* 可以通过公司名称或ID获取上市公司联系信息，联系信息包括联系电话、电子邮箱、传真、公司网址、办公地址、注册地址等
* /services/open/stock/corpContactInfo/2.0
*@author deament
**/

public interface StockCorpContactInfoRequest extends BaseRequest<StockCorpContactInfoDTO ,StockCorpContactInfo>{

}

