package com.gitee.deament.tianyancha.core.remote.hiabnormal.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*历史经营异常
* 可以通过公司名称或ID获取企业历史的经营异常信息，历史经营异常信息包括列入/移除原因、时间、做出决定机关等字段信息
* /services/open/hi/abnormal/2.0
*@author deament
**/

@TYCURL(value="/services/open/hi/abnormal/2.0")
public class HiAbnormalDTO implements Serializable{

    /**
     *    企业名称
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数（默认20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    企业id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    当前页（默认1）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 企业名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（默认20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 企业id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 当前页（默认1）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

