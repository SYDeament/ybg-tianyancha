package com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity.CbDevelopmentPrivateFundList;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity.CbDevelopmentJpList;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity.CbDevelopmentTzList;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity.CbDevelopmentTeamList;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity.CbDevelopmentInvestOrgList;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity.CbDevelopmentRongziList;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity.CbDevelopmentProductList;
/**
*企业发展
* 可以通过公司名称或ID获取包含核心团队、企业业务、融资历史、投资事件、竞品信息、相关项目品牌/投资机构、投资动态等维度的相关信息
* /services/open/cb/development/2.0
*@author deament
**/

public class CbDevelopment implements Serializable{

    /**
     *    私募基金
    **/
    @JSONField(name="privateFundList")
    private List<CbDevelopmentPrivateFundList> privateFundList;
    /**
     *    竞品
    **/
    @JSONField(name="jpList")
    private List<CbDevelopmentJpList> jpList;
    /**
     *    投资事件
    **/
    @JSONField(name="tzList")
    private List<CbDevelopmentTzList> tzList;
    /**
     *    核心团队
    **/
    @JSONField(name="teamList")
    private List<CbDevelopmentTeamList> teamList;
    /**
     *    投资机构
    **/
    @JSONField(name="investOrgList")
    private List<CbDevelopmentInvestOrgList> investOrgList;
    /**
     *    融资历史
    **/
    @JSONField(name="rongziList")
    private List<CbDevelopmentRongziList> rongziList;
    /**
     *    企业业务
    **/
    @JSONField(name="productList")
    private List<CbDevelopmentProductList> productList;


    /**
    *   设置 私募基金
    **/
    public void setPrivateFundList(List<CbDevelopmentPrivateFundList> privateFundList) {
      this.privateFundList = privateFundList;
    }
    /**
    *   获取 私募基金
    **/
    public List<CbDevelopmentPrivateFundList> getPrivateFundList() {
      return privateFundList;
    }
    /**
    *   设置 竞品
    **/
    public void setJpList(List<CbDevelopmentJpList> jpList) {
      this.jpList = jpList;
    }
    /**
    *   获取 竞品
    **/
    public List<CbDevelopmentJpList> getJpList() {
      return jpList;
    }
    /**
    *   设置 投资事件
    **/
    public void setTzList(List<CbDevelopmentTzList> tzList) {
      this.tzList = tzList;
    }
    /**
    *   获取 投资事件
    **/
    public List<CbDevelopmentTzList> getTzList() {
      return tzList;
    }
    /**
    *   设置 核心团队
    **/
    public void setTeamList(List<CbDevelopmentTeamList> teamList) {
      this.teamList = teamList;
    }
    /**
    *   获取 核心团队
    **/
    public List<CbDevelopmentTeamList> getTeamList() {
      return teamList;
    }
    /**
    *   设置 投资机构
    **/
    public void setInvestOrgList(List<CbDevelopmentInvestOrgList> investOrgList) {
      this.investOrgList = investOrgList;
    }
    /**
    *   获取 投资机构
    **/
    public List<CbDevelopmentInvestOrgList> getInvestOrgList() {
      return investOrgList;
    }
    /**
    *   设置 融资历史
    **/
    public void setRongziList(List<CbDevelopmentRongziList> rongziList) {
      this.rongziList = rongziList;
    }
    /**
    *   获取 融资历史
    **/
    public List<CbDevelopmentRongziList> getRongziList() {
      return rongziList;
    }
    /**
    *   设置 企业业务
    **/
    public void setProductList(List<CbDevelopmentProductList> productList) {
      this.productList = productList;
    }
    /**
    *   获取 企业业务
    **/
    public List<CbDevelopmentProductList> getProductList() {
      return productList;
    }



}

