package com.gitee.deament.tianyancha.core.remote.mdoublerandomcheckdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*双随机抽查详情
* 属于MDoubleRandomCheckDetail
* /services/open/m/doubleRandomCheckDetail/2.0
*@author deament
**/

public class MDoubleRandomCheckDetailItems implements Serializable{

    /**
     *    检查事项
    **/
    @JSONField(name="checkItem")
    private String checkItem;
    /**
     *    检查结果
    **/
    @JSONField(name="checkResult")
    private String checkResult;


    /**
    *   设置 检查事项
    **/
    public void setCheckItem(String checkItem) {
      this.checkItem = checkItem;
    }
    /**
    *   获取 检查事项
    **/
    public String getCheckItem() {
      return checkItem;
    }
    /**
    *   设置 检查结果
    **/
    public void setCheckResult(String checkResult) {
      this.checkResult = checkResult;
    }
    /**
    *   获取 检查结果
    **/
    public String getCheckResult() {
      return checkResult;
    }



}

