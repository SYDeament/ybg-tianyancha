package com.gitee.deament.tianyancha.core.remote.stockequitychange.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*股本变动
* 属于StockEquityChange
* /services/open/stock/equityChange/2.0
*@author deament
**/

public class StockEquityChangeItems implements Serializable{

    /**
     *    变动后限售A股(股)
    **/
    @JSONField(name="afterLimit")
    private String afterLimit;
    /**
     *    变动后流通A股(股)
    **/
    @JSONField(name="afterNoLimit")
    private String afterNoLimit;
    /**
     *    变动原因
    **/
    @JSONField(name="changeReason")
    private String changeReason;
    /**
     *    变动后A股总股本(股)
    **/
    @JSONField(name="afterAll")
    private String afterAll;
    /**
     *    变动日期
    **/
    @JSONField(name="changeDate")
    private Long changeDate;


    /**
    *   设置 变动后限售A股(股)
    **/
    public void setAfterLimit(String afterLimit) {
      this.afterLimit = afterLimit;
    }
    /**
    *   获取 变动后限售A股(股)
    **/
    public String getAfterLimit() {
      return afterLimit;
    }
    /**
    *   设置 变动后流通A股(股)
    **/
    public void setAfterNoLimit(String afterNoLimit) {
      this.afterNoLimit = afterNoLimit;
    }
    /**
    *   获取 变动后流通A股(股)
    **/
    public String getAfterNoLimit() {
      return afterNoLimit;
    }
    /**
    *   设置 变动原因
    **/
    public void setChangeReason(String changeReason) {
      this.changeReason = changeReason;
    }
    /**
    *   获取 变动原因
    **/
    public String getChangeReason() {
      return changeReason;
    }
    /**
    *   设置 变动后A股总股本(股)
    **/
    public void setAfterAll(String afterAll) {
      this.afterAll = afterAll;
    }
    /**
    *   获取 变动后A股总股本(股)
    **/
    public String getAfterAll() {
      return afterAll;
    }
    /**
    *   设置 变动日期
    **/
    public void setChangeDate(Long changeDate) {
      this.changeDate = changeDate;
    }
    /**
    *   获取 变动日期
    **/
    public Long getChangeDate() {
      return changeDate;
    }



}

