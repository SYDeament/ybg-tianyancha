package com.gitee.deament.tianyancha.core.remote.cbjudicial.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*司法风险
* 属于CbJudicial
* /services/open/cb/judicial/2.0
*@author deament
**/

public class CbJudicialSendAnnouncementList implements Serializable{

    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    内容
    **/
    @JSONField(name="content")
    private String content;
    /**
     *    发布日期
    **/
    @JSONField(name="startDate")
    private String startDate;


    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 内容
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 内容
    **/
    public String getContent() {
      return content;
    }
    /**
    *   设置 发布日期
    **/
    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 发布日期
    **/
    public String getStartDate() {
      return startDate;
    }



}

