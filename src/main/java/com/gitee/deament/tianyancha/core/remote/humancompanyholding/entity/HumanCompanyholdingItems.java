package com.gitee.deament.tianyancha.core.remote.humancompanyholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*人员控股企业
* 属于HumanCompanyholding
* /services/open/human/companyholding/2.0
*@author deament
**/

public class HumanCompanyholdingItems implements Serializable{

    /**
     *    法人id
    **/
    @JSONField(name="legalPersonId")
    private Integer legalPersonId;
    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    投资链
    **/
    @JSONField(name="chainList")
    private List<List<HumanCompanyholdingChainList>> chainList;
    /**
     *    法人类型 1-人 2-公司
    **/
    @JSONField(name="legalType")
    private Integer legalType;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    控股企业
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    企业logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    投资比例
    **/
    @JSONField(name="percent")
    private String percent;
    /**
     *    控股企业id
    **/
    @JSONField(name="cid")
    private String cid;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;


    /**
    *   设置 法人id
    **/
    public void setLegalPersonId(Integer legalPersonId) {
      this.legalPersonId = legalPersonId;
    }
    /**
    *   获取 法人id
    **/
    public Integer getLegalPersonId() {
      return legalPersonId;
    }
    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 投资链
    **/
    public void setChainList(List<List<HumanCompanyholdingChainList>> chainList) {
      this.chainList = chainList;
    }
    /**
    *   获取 投资链
    **/
    public List<List<HumanCompanyholdingChainList>> getChainList() {
      return chainList;
    }
    /**
    *   设置 法人类型 1-人 2-公司
    **/
    public void setLegalType(Integer legalType) {
      this.legalType = legalType;
    }
    /**
    *   获取 法人类型 1-人 2-公司
    **/
    public Integer getLegalType() {
      return legalType;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 控股企业
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 控股企业
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 企业logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 企业logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 投资比例
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 投资比例
    **/
    public String getPercent() {
      return percent;
    }
    /**
    *   设置 控股企业id
    **/
    public void setCid(String cid) {
      this.cid = cid;
    }
    /**
    *   获取 控股企业id
    **/
    public String getCid() {
      return cid;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }



}

