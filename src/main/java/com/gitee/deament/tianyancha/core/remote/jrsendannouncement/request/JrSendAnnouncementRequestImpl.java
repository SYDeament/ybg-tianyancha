package com.gitee.deament.tianyancha.core.remote.jrsendannouncement.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrsendannouncement.entity.JrSendAnnouncement;
import com.gitee.deament.tianyancha.core.remote.jrsendannouncement.dto.JrSendAnnouncementDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*送达公告
* 可以通过公司名称或ID获取送达公告信息，送达公告信息包括公告名称、法院名称、公告内容、发布日期等字段的详细信息
* /services/open/jr/sendAnnouncement/2.0
*@author deament
**/
@Component("jrSendAnnouncementRequestImpl")
public class JrSendAnnouncementRequestImpl extends BaseRequestImpl<JrSendAnnouncement,JrSendAnnouncementDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/jr/sendAnnouncement/2.0";
    }
}

