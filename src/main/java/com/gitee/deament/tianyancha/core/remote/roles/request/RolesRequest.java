package com.gitee.deament.tianyancha.core.remote.roles.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.roles.entity.Roles;
import com.gitee.deament.tianyancha.core.remote.roles.dto.RolesDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*人员所有角色
* 可以通过公司名称或ID和人名获取人员的所有商业角色，包括其担任法人、股东、董监高的职位信息
* /services/v4/open/roles
*@author deament
**/

public interface RolesRequest extends BaseRequest<RolesDTO ,Roles>{

}

