package com.gitee.deament.tianyancha.core.remote.iprpatents.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*专利信息
* 属于IprPatents
* /services/open/ipr/patents/2.0
*@author deament
**/

public class IprPatentsItems implements Serializable{

    /**
     *    代理人
    **/
    @JSONField(name="agent")
    private String agent;
    /**
     *    名称
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    申请号/专利号
    **/
    @JSONField(name="patentNum")
    private String patentNum;
    /**
     *    uuid
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    申请公布号
    **/
    @JSONField(name="pubnumber")
    private String pubnumber;
    /**
     *    申请日
    **/
    @JSONField(name="applicationTime")
    private String applicationTime;
    /**
     *    分类
    **/
    @JSONField(name="cat")
    private String cat;
    /**
     *    申请人
    **/
    @JSONField(name="applicantname")
    private String applicantname;
    /**
     *    无用
    **/
    @JSONField(name="eventTime")
    private String eventTime;
    /**
     *    发明人
    **/
    @JSONField(name="inventor")
    private String inventor;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    法律状态
    **/
    @JSONField(name="lawStatus")
    private List<IprPatentsLawStatus> lawStatus;
    /**
     *    地址
    **/
    @JSONField(name="address")
    private String address;
    /**
     *    代理机构
    **/
    @JSONField(name="agency")
    private String agency;
    /**
     *    无用
    **/
    @JSONField(name="searchType")
    private String searchType;
    /**
     *    跳转到天眼查链接
    **/
    @JSONField(name="connList")
    private List<String> connList;
    /**
     *    摘要
    **/
    @JSONField(name="abstracts")
    private String abstracts;
    /**
     *    无用
    **/
    @JSONField(name="_type")
    private String _type;
    /**
     *    申请人
    **/
    @JSONField(name="applicantName")
    private String applicantName;
    /**
     *    公开公告日
    **/
    @JSONField(name="pubDate")
    private String pubDate;
    /**
     *    申请公布日
    **/
    @JSONField(name="applicationPublishTime")
    private String applicationPublishTime;
    /**
     *    申请号
    **/
    @JSONField(name="appnumber")
    private String appnumber;
    /**
     *    专利类型
    **/
    @JSONField(name="patentType")
    private String patentType;
    /**
     *    图片url
    **/
    @JSONField(name="imgUrl")
    private String imgUrl;
    /**
     *    唯一标识符
    **/
    @JSONField(name="uni")
    private String uni;
    /**
     *    主分类号
    **/
    @JSONField(name="mainCatNum")
    private String mainCatNum;
    /**
     *    创建时间
    **/
    @JSONField(name="createTime")
    private String createTime;
    /**
     *    专利名称
    **/
    @JSONField(name="patentName")
    private String patentName;
    /**
     *    申请公布号（废弃）
    **/
    @JSONField(name="applicationPublishNum")
    private String applicationPublishNum;
    /**
     *    全部分类号
    **/
    @JSONField(name="allCatNum")
    private String allCatNum;


    /**
    *   设置 代理人
    **/
    public void setAgent(String agent) {
      this.agent = agent;
    }
    /**
    *   获取 代理人
    **/
    public String getAgent() {
      return agent;
    }
    /**
    *   设置 名称
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 名称
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 申请号/专利号
    **/
    public void setPatentNum(String patentNum) {
      this.patentNum = patentNum;
    }
    /**
    *   获取 申请号/专利号
    **/
    public String getPatentNum() {
      return patentNum;
    }
    /**
    *   设置 uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 uuid
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 申请公布号
    **/
    public void setPubnumber(String pubnumber) {
      this.pubnumber = pubnumber;
    }
    /**
    *   获取 申请公布号
    **/
    public String getPubnumber() {
      return pubnumber;
    }
    /**
    *   设置 申请日
    **/
    public void setApplicationTime(String applicationTime) {
      this.applicationTime = applicationTime;
    }
    /**
    *   获取 申请日
    **/
    public String getApplicationTime() {
      return applicationTime;
    }
    /**
    *   设置 分类
    **/
    public void setCat(String cat) {
      this.cat = cat;
    }
    /**
    *   获取 分类
    **/
    public String getCat() {
      return cat;
    }
    /**
    *   设置 申请人
    **/
    public void setApplicantname(String applicantname) {
      this.applicantname = applicantname;
    }
    /**
    *   获取 申请人
    **/
    public String getApplicantname() {
      return applicantname;
    }
    /**
    *   设置 无用
    **/
    public void setEventTime(String eventTime) {
      this.eventTime = eventTime;
    }
    /**
    *   获取 无用
    **/
    public String getEventTime() {
      return eventTime;
    }
    /**
    *   设置 发明人
    **/
    public void setInventor(String inventor) {
      this.inventor = inventor;
    }
    /**
    *   获取 发明人
    **/
    public String getInventor() {
      return inventor;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 法律状态
    **/
    public void setLawStatus(List<IprPatentsLawStatus> lawStatus) {
      this.lawStatus = lawStatus;
    }
    /**
    *   获取 法律状态
    **/
    public List<IprPatentsLawStatus> getLawStatus() {
      return lawStatus;
    }
    /**
    *   设置 地址
    **/
    public void setAddress(String address) {
      this.address = address;
    }
    /**
    *   获取 地址
    **/
    public String getAddress() {
      return address;
    }
    /**
    *   设置 代理机构
    **/
    public void setAgency(String agency) {
      this.agency = agency;
    }
    /**
    *   获取 代理机构
    **/
    public String getAgency() {
      return agency;
    }
    /**
    *   设置 无用
    **/
    public void setSearchType(String searchType) {
      this.searchType = searchType;
    }
    /**
    *   获取 无用
    **/
    public String getSearchType() {
      return searchType;
    }
    /**
    *   设置 跳转到天眼查链接
    **/
    public void setConnList(List<String> connList) {
      this.connList = connList;
    }
    /**
    *   获取 跳转到天眼查链接
    **/
    public List<String> getConnList() {
      return connList;
    }
    /**
    *   设置 摘要
    **/
    public void setAbstracts(String abstracts) {
      this.abstracts = abstracts;
    }
    /**
    *   获取 摘要
    **/
    public String getAbstracts() {
      return abstracts;
    }
    /**
    *   设置 无用
    **/
    public void set_type(String _type) {
      this._type = _type;
    }
    /**
    *   获取 无用
    **/
    public String get_type() {
      return _type;
    }
    /**
    *   设置 申请人
    **/
    public void setApplicantName(String applicantName) {
      this.applicantName = applicantName;
    }
    /**
    *   获取 申请人
    **/
    public String getApplicantName() {
      return applicantName;
    }
    /**
    *   设置 公开公告日
    **/
    public void setPubDate(String pubDate) {
      this.pubDate = pubDate;
    }
    /**
    *   获取 公开公告日
    **/
    public String getPubDate() {
      return pubDate;
    }
    /**
    *   设置 申请公布日
    **/
    public void setApplicationPublishTime(String applicationPublishTime) {
      this.applicationPublishTime = applicationPublishTime;
    }
    /**
    *   获取 申请公布日
    **/
    public String getApplicationPublishTime() {
      return applicationPublishTime;
    }
    /**
    *   设置 申请号
    **/
    public void setAppnumber(String appnumber) {
      this.appnumber = appnumber;
    }
    /**
    *   获取 申请号
    **/
    public String getAppnumber() {
      return appnumber;
    }
    /**
    *   设置 专利类型
    **/
    public void setPatentType(String patentType) {
      this.patentType = patentType;
    }
    /**
    *   获取 专利类型
    **/
    public String getPatentType() {
      return patentType;
    }
    /**
    *   设置 图片url
    **/
    public void setImgUrl(String imgUrl) {
      this.imgUrl = imgUrl;
    }
    /**
    *   获取 图片url
    **/
    public String getImgUrl() {
      return imgUrl;
    }
    /**
    *   设置 唯一标识符
    **/
    public void setUni(String uni) {
      this.uni = uni;
    }
    /**
    *   获取 唯一标识符
    **/
    public String getUni() {
      return uni;
    }
    /**
    *   设置 主分类号
    **/
    public void setMainCatNum(String mainCatNum) {
      this.mainCatNum = mainCatNum;
    }
    /**
    *   获取 主分类号
    **/
    public String getMainCatNum() {
      return mainCatNum;
    }
    /**
    *   设置 创建时间
    **/
    public void setCreateTime(String createTime) {
      this.createTime = createTime;
    }
    /**
    *   获取 创建时间
    **/
    public String getCreateTime() {
      return createTime;
    }
    /**
    *   设置 专利名称
    **/
    public void setPatentName(String patentName) {
      this.patentName = patentName;
    }
    /**
    *   获取 专利名称
    **/
    public String getPatentName() {
      return patentName;
    }
    /**
    *   设置 申请公布号（废弃）
    **/
    public void setApplicationPublishNum(String applicationPublishNum) {
      this.applicationPublishNum = applicationPublishNum;
    }
    /**
    *   获取 申请公布号（废弃）
    **/
    public String getApplicationPublishNum() {
      return applicationPublishNum;
    }
    /**
    *   设置 全部分类号
    **/
    public void setAllCatNum(String allCatNum) {
      this.allCatNum = allCatNum;
    }
    /**
    *   获取 全部分类号
    **/
    public String getAllCatNum() {
      return allCatNum;
    }



}

