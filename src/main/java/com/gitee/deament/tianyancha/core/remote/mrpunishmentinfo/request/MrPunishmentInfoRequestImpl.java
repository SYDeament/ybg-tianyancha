package com.gitee.deament.tianyancha.core.remote.mrpunishmentinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrpunishmentinfo.entity.MrPunishmentInfo;
import com.gitee.deament.tianyancha.core.remote.mrpunishmentinfo.dto.MrPunishmentInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*行政处罚-工商局
* 可以通过公司名称或ID获取企业行政处罚信息，行政处罚信息包括行政处罚明细、行政处罚公告、行政处罚内容、公示日期等字段的详细信息
* /services/open/mr/punishmentInfo/2.0
*@author deament
**/
@Component("mrPunishmentInfoRequestImpl")
public class MrPunishmentInfoRequestImpl extends BaseRequestImpl<MrPunishmentInfo,MrPunishmentInfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/punishmentInfo/2.0";
    }
}

