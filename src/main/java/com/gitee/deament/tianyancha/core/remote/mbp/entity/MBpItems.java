package com.gitee.deament.tianyancha.core.remote.mbp.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业招聘-百聘
* 属于MBp
* /services/open/m/bp/employments/2.0
*@author deament
**/

public class MBpItems implements Serializable{

    /**
     *    教育背景
    **/
    @JSONField(name="education")
    private String education;
    /**
     *    城市
    **/
    @JSONField(name="city")
    private String city;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    百聘地址
    **/
    @JSONField(name="webInfoPath")
    private String webInfoPath;
    /**
     *    来源网站
    **/
    @JSONField(name="source")
    private String source;
    /**
     *    职位
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    工作经验
    **/
    @JSONField(name="experience")
    private String experience;
    /**
     *    薪资
    **/
    @JSONField(name="salary")
    private String salary;
    /**
     *    福利待遇
    **/
    @JSONField(name="welfare")
    private String welfare;
    /**
     *    公司id
    **/
    @JSONField(name="companyGid")
    private Integer companyGid;
    /**
     *    发布时间
    **/
    @JSONField(name="startDate")
    private String startDate;


    /**
    *   设置 教育背景
    **/
    public void setEducation(String education) {
      this.education = education;
    }
    /**
    *   获取 教育背景
    **/
    public String getEducation() {
      return education;
    }
    /**
    *   设置 城市
    **/
    public void setCity(String city) {
      this.city = city;
    }
    /**
    *   获取 城市
    **/
    public String getCity() {
      return city;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 百聘地址
    **/
    public void setWebInfoPath(String webInfoPath) {
      this.webInfoPath = webInfoPath;
    }
    /**
    *   获取 百聘地址
    **/
    public String getWebInfoPath() {
      return webInfoPath;
    }
    /**
    *   设置 来源网站
    **/
    public void setSource(String source) {
      this.source = source;
    }
    /**
    *   获取 来源网站
    **/
    public String getSource() {
      return source;
    }
    /**
    *   设置 职位
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 职位
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 工作经验
    **/
    public void setExperience(String experience) {
      this.experience = experience;
    }
    /**
    *   获取 工作经验
    **/
    public String getExperience() {
      return experience;
    }
    /**
    *   设置 薪资
    **/
    public void setSalary(String salary) {
      this.salary = salary;
    }
    /**
    *   获取 薪资
    **/
    public String getSalary() {
      return salary;
    }
    /**
    *   设置 福利待遇
    **/
    public void setWelfare(String welfare) {
      this.welfare = welfare;
    }
    /**
    *   获取 福利待遇
    **/
    public String getWelfare() {
      return welfare;
    }
    /**
    *   设置 公司id
    **/
    public void setCompanyGid(Integer companyGid) {
      this.companyGid = companyGid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCompanyGid() {
      return companyGid;
    }
    /**
    *   设置 发布时间
    **/
    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 发布时间
    **/
    public String getStartDate() {
      return startDate;
    }



}

