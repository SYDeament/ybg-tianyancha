package com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity.MrMortgageInfoBaseInfo;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity.MrMortgageInfoPeopleInfo;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity.MrMortgageInfoPawnInfoList;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity.MrMortgageInfoChangeInfoList;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity.MrMortgageInfoItems;
/**
*动产抵押
* 可以通过公司名称或ID获取企业动产抵押公告信息，企业动产抵押公告信息包括被担保债权类型、数额、登记机关等字段信息
* /services/open/mr/mortgageInfo/2.0
*@author deament
**/

public class MrMortgageInfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MrMortgageInfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MrMortgageInfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MrMortgageInfoItems> getItems() {
      return items;
    }



}

