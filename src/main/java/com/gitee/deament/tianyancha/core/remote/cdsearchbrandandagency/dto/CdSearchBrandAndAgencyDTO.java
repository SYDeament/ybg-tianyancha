package com.gitee.deament.tianyancha.core.remote.cdsearchbrandandagency.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*搜索项目品牌/投资机构
* 可以通过关键词获取项目品牌和投资机构信息，包括项目品牌和投资机构名称、logo、融资轮次或投资事件等字段的详细信息
* /services/open/cd/searchBrandAndAgency/2.0
*@author deament
**/

@TYCURL(value="/services/open/cd/searchBrandAndAgency/2.0")
public class CdSearchBrandAndAgencyDTO implements Serializable{

    /**
     *    关键词
     *
    **/
    @ParamRequire(require = true)
    private String word;


    /**
    *   设置 关键词
    **/
    public void setWord(String word) {
      this.word = word;
    }
    /**
    *   获取 关键词
    **/
    public String getWord() {
      return word;
    }



}

