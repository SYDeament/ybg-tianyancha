package com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*司法协助详情
* 属于GetJudicialDetail
* /services/v4/open/getJudicialDetail
*@author deament
**/

public class GetJudicialDetailInvalidationFrozen implements Serializable{

    /**
     *    失效原因
    **/
    @JSONField(name="invalidationReason")
    private String invalidationReason;
    /**
     *    失效日期
    **/
    @JSONField(name="invalidationDate")
    private Long invalidationDate;


    /**
    *   设置 失效原因
    **/
    public void setInvalidationReason(String invalidationReason) {
      this.invalidationReason = invalidationReason;
    }
    /**
    *   获取 失效原因
    **/
    public String getInvalidationReason() {
      return invalidationReason;
    }
    /**
    *   设置 失效日期
    **/
    public void setInvalidationDate(Long invalidationDate) {
      this.invalidationDate = invalidationDate;
    }
    /**
    *   获取 失效日期
    **/
    public Long getInvalidationDate() {
      return invalidationDate;
    }



}

