package com.gitee.deament.tianyancha.core.remote.hilicense.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hilicense.entity.HiLicense;
import com.gitee.deament.tianyancha.core.remote.hilicense.dto.HiLicenseDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史行政许可-其他来源
* 可以通过公司名称或ID获取企业历史的行政许可信息，历史行政许可信息包括行政许可决定文书号、许可内容、许可机关、审核类型等字段信息
* /services/open/hi/license/creditChina/2.0
*@author deament
**/
@Component("hiLicenseRequestImpl")
public class HiLicenseRequestImpl extends BaseRequestImpl<HiLicense,HiLicenseDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/license/creditChina/2.0";
    }
}

