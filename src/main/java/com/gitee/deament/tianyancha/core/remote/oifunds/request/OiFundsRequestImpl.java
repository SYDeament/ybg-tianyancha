package com.gitee.deament.tianyancha.core.remote.oifunds.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oifunds.entity.OiFunds;
import com.gitee.deament.tianyancha.core.remote.oifunds.dto.OiFundsDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*管理基金
* 可以通过投资机构名称获取投资机构管理基金信息，包括企业名称、法定代表人、注册资本、成立日期等字段信息
* /services/open/oi/funds/2.0
*@author deament
**/
@Component("oiFundsRequestImpl")
public class OiFundsRequestImpl extends BaseRequestImpl<OiFunds,OiFundsDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/oi/funds/2.0";
    }
}

