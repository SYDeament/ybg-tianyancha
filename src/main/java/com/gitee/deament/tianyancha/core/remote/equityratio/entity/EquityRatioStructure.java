package com.gitee.deament.tianyancha.core.remote.equityratio.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*股权结构图
* 属于EquityRatio
* /services/v4/open/equityRatio
*@author deament
**/

public class EquityRatioStructure implements Serializable{

    /**
     *    金额
    **/
    @JSONField(name="amount")
    private String amount;
    /**
     *    注册资金
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    H 人 C公司
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    占比
    **/
    @JSONField(name="percent")
    private String percent;
    /**
     *    （无法识别的字段 请手动修改）
    **/
    @JSONField(name="tagList")
    private List<EquityRatioTagList> tagList;
    /**
     *    股东类型
    **/
    @JSONField(name="sh_type")
    private String sh_type;
    /**
     *    父节点名
    **/
    @JSONField(name="parentName")
    private String parentName;
    /**
     *    子节点
    **/
    @JSONField(name="children")
    private List<EquityRatioChildren> children;
    /**
     *    是否控股
    **/
    @JSONField(name="actualHolding")
    private Boolean actualHolding;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    父节点名称
    **/
    @JSONField(name="cid")
    private Integer cid;
    /**
     *    无用
    **/
    @JSONField(name="info")
    private String info;


    /**
    *   设置 金额
    **/
    public void setAmount(String amount) {
      this.amount = amount;
    }
    /**
    *   获取 金额
    **/
    public String getAmount() {
      return amount;
    }
    /**
    *   设置 注册资金
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资金
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 H 人 C公司
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 H 人 C公司
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 占比
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 占比
    **/
    public String getPercent() {
      return percent;
    }
    /**
    *   设置 （无法识别的字段 请手动修改）
    **/
    public void setTagList(List<EquityRatioTagList> tagList) {
      this.tagList = tagList;
    }
    /**
    *   获取 （无法识别的字段 请手动修改）
    **/
    public List<EquityRatioTagList> getTagList() {
      return tagList;
    }
    /**
    *   设置 股东类型
    **/
    public void setSh_type(String sh_type) {
      this.sh_type = sh_type;
    }
    /**
    *   获取 股东类型
    **/
    public String getSh_type() {
      return sh_type;
    }
    /**
    *   设置 父节点名
    **/
    public void setParentName(String parentName) {
      this.parentName = parentName;
    }
    /**
    *   获取 父节点名
    **/
    public String getParentName() {
      return parentName;
    }
    /**
    *   设置 子节点
    **/
    public void setChildren(List<EquityRatioChildren> children) {
      this.children = children;
    }
    /**
    *   获取 子节点
    **/
    public List<EquityRatioChildren> getChildren() {
      return children;
    }
    /**
    *   设置 是否控股
    **/
    public void setActualHolding(Boolean actualHolding) {
      this.actualHolding = actualHolding;
    }
    /**
    *   获取 是否控股
    **/
    public Boolean getActualHolding() {
      return actualHolding;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 父节点名称
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 父节点名称
    **/
    public Integer getCid() {
      return cid;
    }
    /**
    *   设置 无用
    **/
    public void setInfo(String info) {
      this.info = info;
    }
    /**
    *   获取 无用
    **/
    public String getInfo() {
      return info;
    }



}

