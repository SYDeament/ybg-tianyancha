package com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业发展
* 属于CbDevelopment
* /services/open/cb/development/2.0
*@author deament
**/

public class CbDevelopmentRongziList implements Serializable{

    /**
     *    融资时间
    **/
    @JSONField(name="date")
    private String date;
    /**
     *    金额
    **/
    @JSONField(name="money")
    private String money;
    /**
     *    轮次
    **/
    @JSONField(name="round")
    private String round;
    /**
     *    新闻url
    **/
    @JSONField(name="newsUrl")
    private String newsUrl;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    披露时间
    **/
    @JSONField(name="pubTime")
    private String pubTime;
    /**
     *    新闻标题
    **/
    @JSONField(name="newsTitle")
    private String newsTitle;
    /**
     *    投资企业
    **/
    @JSONField(name="investorName")
    private String investorName;
    /**
     *    投资比例
    **/
    @JSONField(name="share")
    private String share;
    /**
     *    估值
    **/
    @JSONField(name="value")
    private String value;


    /**
    *   设置 融资时间
    **/
    public void setDate(String date) {
      this.date = date;
    }
    /**
    *   获取 融资时间
    **/
    public String getDate() {
      return date;
    }
    /**
    *   设置 金额
    **/
    public void setMoney(String money) {
      this.money = money;
    }
    /**
    *   获取 金额
    **/
    public String getMoney() {
      return money;
    }
    /**
    *   设置 轮次
    **/
    public void setRound(String round) {
      this.round = round;
    }
    /**
    *   获取 轮次
    **/
    public String getRound() {
      return round;
    }
    /**
    *   设置 新闻url
    **/
    public void setNewsUrl(String newsUrl) {
      this.newsUrl = newsUrl;
    }
    /**
    *   获取 新闻url
    **/
    public String getNewsUrl() {
      return newsUrl;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 披露时间
    **/
    public void setPubTime(String pubTime) {
      this.pubTime = pubTime;
    }
    /**
    *   获取 披露时间
    **/
    public String getPubTime() {
      return pubTime;
    }
    /**
    *   设置 新闻标题
    **/
    public void setNewsTitle(String newsTitle) {
      this.newsTitle = newsTitle;
    }
    /**
    *   获取 新闻标题
    **/
    public String getNewsTitle() {
      return newsTitle;
    }
    /**
    *   设置 投资企业
    **/
    public void setInvestorName(String investorName) {
      this.investorName = investorName;
    }
    /**
    *   获取 投资企业
    **/
    public String getInvestorName() {
      return investorName;
    }
    /**
    *   设置 投资比例
    **/
    public void setShare(String share) {
      this.share = share;
    }
    /**
    *   获取 投资比例
    **/
    public String getShare() {
      return share;
    }
    /**
    *   设置 估值
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 估值
    **/
    public String getValue() {
      return value;
    }



}

