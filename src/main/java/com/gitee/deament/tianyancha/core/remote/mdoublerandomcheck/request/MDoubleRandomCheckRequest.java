package com.gitee.deament.tianyancha.core.remote.mdoublerandomcheck.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mdoublerandomcheck.entity.MDoubleRandomCheck;
import com.gitee.deament.tianyancha.core.remote.mdoublerandomcheck.dto.MDoubleRandomCheckDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*双随机抽查
* 可以通过公司名称或ID获取企业双随机抽查信息，企业双随机抽查信息包括抽查类型、抽查机关等字段的详细信息
* /services/open/m/doubleRandomCheck/2.0
*@author deament
**/

public interface MDoubleRandomCheckRequest extends BaseRequest<MDoubleRandomCheckDTO ,MDoubleRandomCheck>{

}

