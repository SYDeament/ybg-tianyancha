package com.gitee.deament.tianyancha.core.remote.oiextfunds.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
* 对外投资基金
* 属于OiExtFunds
* /services/open/oi/extFunds/2.0
*@author deament
**/

public class OiExtFundsInvestor_list implements Serializable{

    /**
     *    企业名
    **/
    @JSONField(name="investor_name")
    private String investor_name;
    /**
     *    企业id
    **/
    @JSONField(name="investor_id")
    private Integer investor_id;
    /**
     *    投资比例
    **/
    @JSONField(name="percent")
    private String percent;


    /**
    *   设置 企业名
    **/
    public void setInvestor_name(String investor_name) {
      this.investor_name = investor_name;
    }
    /**
    *   获取 企业名
    **/
    public String getInvestor_name() {
      return investor_name;
    }
    /**
    *   设置 企业id
    **/
    public void setInvestor_id(Integer investor_id) {
      this.investor_id = investor_id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getInvestor_id() {
      return investor_id;
    }
    /**
    *   设置 投资比例
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 投资比例
    **/
    public String getPercent() {
      return percent;
    }



}

