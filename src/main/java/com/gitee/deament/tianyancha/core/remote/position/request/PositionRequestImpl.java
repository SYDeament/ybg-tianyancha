package com.gitee.deament.tianyancha.core.remote.position.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.position.entity.Position;
import com.gitee.deament.tianyancha.core.remote.position.dto.PositionDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*企业经纬度
* 可以通过公司名称或ID获取企业经纬度信息
* /services/v4/open/position
*@author deament
**/
@Component("positionRequestImpl")
public class PositionRequestImpl extends BaseRequestImpl<Position,PositionDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/position";
    }
}

