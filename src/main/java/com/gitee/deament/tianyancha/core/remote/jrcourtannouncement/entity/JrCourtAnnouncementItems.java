package com.gitee.deament.tianyancha.core.remote.jrcourtannouncement.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*法院公告
* 属于JrCourtAnnouncement
* /services/open/jr/courtAnnouncement/2.0
*@author deament
**/

public class JrCourtAnnouncementItems implements Serializable{

    /**
     *    公告id
    **/
    @JSONField(name="announce_id")
    private Integer announce_id;
    /**
     *    原告
    **/
    @JSONField(name="party1")
    private String party1;
    /**
     *    原因
    **/
    @JSONField(name="reason")
    private String reason;
    /**
     *    公告号
    **/
    @JSONField(name="bltnno")
    private String bltnno;
    /**
     *    当事人
    **/
    @JSONField(name="party2")
    private String party2;
    /**
     *    公告状态号
    **/
    @JSONField(name="bltnstate")
    private String bltnstate;
    /**
     *    处理等级名称
    **/
    @JSONField(name="dealgradename")
    private String dealgradename;
    /**
     *    法官电话
    **/
    @JSONField(name="judgephone")
    private String judgephone;
    /**
     *    案件号
    **/
    @JSONField(name="caseno")
    private String caseno;
    /**
     *    公告类型名称
    **/
    @JSONField(name="bltntypename")
    private String bltntypename;
    /**
     *    案件内容
    **/
    @JSONField(name="content")
    private String content;
    /**
     *    处理等级
    **/
    @JSONField(name="dealgrade")
    private String dealgrade;
    /**
     *    公告类型
    **/
    @JSONField(name="bltntype")
    private String bltntype;
    /**
     *    法院名
    **/
    @JSONField(name="courtcode")
    private String courtcode;
    /**
     *    省份
    **/
    @JSONField(name="province")
    private String province;
    /**
     *    公司列表
    **/
    @JSONField(name="companyList")
    private List<JrCourtAnnouncementCompanyList> companyList;
    /**
     *    手机号
    **/
    @JSONField(name="mobilephone")
    private String mobilephone;
    /**
     *    刊登版面
    **/
    @JSONField(name="publishpage")
    private String publishpage;
    /**
     *    刊登日期
    **/
    @JSONField(name="publishdate")
    private String publishdate;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    法官
    **/
    @JSONField(name="judge")
    private String judge;


    /**
    *   设置 公告id
    **/
    public void setAnnounce_id(Integer announce_id) {
      this.announce_id = announce_id;
    }
    /**
    *   获取 公告id
    **/
    public Integer getAnnounce_id() {
      return announce_id;
    }
    /**
    *   设置 原告
    **/
    public void setParty1(String party1) {
      this.party1 = party1;
    }
    /**
    *   获取 原告
    **/
    public String getParty1() {
      return party1;
    }
    /**
    *   设置 原因
    **/
    public void setReason(String reason) {
      this.reason = reason;
    }
    /**
    *   获取 原因
    **/
    public String getReason() {
      return reason;
    }
    /**
    *   设置 公告号
    **/
    public void setBltnno(String bltnno) {
      this.bltnno = bltnno;
    }
    /**
    *   获取 公告号
    **/
    public String getBltnno() {
      return bltnno;
    }
    /**
    *   设置 当事人
    **/
    public void setParty2(String party2) {
      this.party2 = party2;
    }
    /**
    *   获取 当事人
    **/
    public String getParty2() {
      return party2;
    }
    /**
    *   设置 公告状态号
    **/
    public void setBltnstate(String bltnstate) {
      this.bltnstate = bltnstate;
    }
    /**
    *   获取 公告状态号
    **/
    public String getBltnstate() {
      return bltnstate;
    }
    /**
    *   设置 处理等级名称
    **/
    public void setDealgradename(String dealgradename) {
      this.dealgradename = dealgradename;
    }
    /**
    *   获取 处理等级名称
    **/
    public String getDealgradename() {
      return dealgradename;
    }
    /**
    *   设置 法官电话
    **/
    public void setJudgephone(String judgephone) {
      this.judgephone = judgephone;
    }
    /**
    *   获取 法官电话
    **/
    public String getJudgephone() {
      return judgephone;
    }
    /**
    *   设置 案件号
    **/
    public void setCaseno(String caseno) {
      this.caseno = caseno;
    }
    /**
    *   获取 案件号
    **/
    public String getCaseno() {
      return caseno;
    }
    /**
    *   设置 公告类型名称
    **/
    public void setBltntypename(String bltntypename) {
      this.bltntypename = bltntypename;
    }
    /**
    *   获取 公告类型名称
    **/
    public String getBltntypename() {
      return bltntypename;
    }
    /**
    *   设置 案件内容
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 案件内容
    **/
    public String getContent() {
      return content;
    }
    /**
    *   设置 处理等级
    **/
    public void setDealgrade(String dealgrade) {
      this.dealgrade = dealgrade;
    }
    /**
    *   获取 处理等级
    **/
    public String getDealgrade() {
      return dealgrade;
    }
    /**
    *   设置 公告类型
    **/
    public void setBltntype(String bltntype) {
      this.bltntype = bltntype;
    }
    /**
    *   获取 公告类型
    **/
    public String getBltntype() {
      return bltntype;
    }
    /**
    *   设置 法院名
    **/
    public void setCourtcode(String courtcode) {
      this.courtcode = courtcode;
    }
    /**
    *   获取 法院名
    **/
    public String getCourtcode() {
      return courtcode;
    }
    /**
    *   设置 省份
    **/
    public void setProvince(String province) {
      this.province = province;
    }
    /**
    *   获取 省份
    **/
    public String getProvince() {
      return province;
    }
    /**
    *   设置 公司列表
    **/
    public void setCompanyList(List<JrCourtAnnouncementCompanyList> companyList) {
      this.companyList = companyList;
    }
    /**
    *   获取 公司列表
    **/
    public List<JrCourtAnnouncementCompanyList> getCompanyList() {
      return companyList;
    }
    /**
    *   设置 手机号
    **/
    public void setMobilephone(String mobilephone) {
      this.mobilephone = mobilephone;
    }
    /**
    *   获取 手机号
    **/
    public String getMobilephone() {
      return mobilephone;
    }
    /**
    *   设置 刊登版面
    **/
    public void setPublishpage(String publishpage) {
      this.publishpage = publishpage;
    }
    /**
    *   获取 刊登版面
    **/
    public String getPublishpage() {
      return publishpage;
    }
    /**
    *   设置 刊登日期
    **/
    public void setPublishdate(String publishdate) {
      this.publishdate = publishdate;
    }
    /**
    *   获取 刊登日期
    **/
    public String getPublishdate() {
      return publishdate;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 法官
    **/
    public void setJudge(String judge) {
      this.judge = judge;
    }
    /**
    *   获取 法官
    **/
    public String getJudge() {
      return judge;
    }



}

