package com.gitee.deament.tianyancha.core.remote.mrstockpledge.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*质押比例
* 属于MrStockPledge
* /services/open/mr/stockPledge/ratio/2.0
*@author deament
**/

public class MrStockPledgeProportion implements Serializable{

    /**
     *    日期
    **/
    @JSONField(name="tradeDateFmt")
    private String tradeDateFmt;
    /**
     *    股票代码
    **/
    @JSONField(name="code")
    private String code;
    /**
     *    近一年涨跌幅
    **/
    @JSONField(name="quoteChange")
    private String quoteChange;
    /**
     *    质押股数（股）
    **/
    @JSONField(name="pledgeAmount")
    private Integer pledgeAmount;
    /**
     *    无限售股质押数（股）
    **/
    @JSONField(name="unlimitedAmount")
    private Integer unlimitedAmount;
    /**
     *    质押笔数
    **/
    @JSONField(name="pledgeNum")
    private Integer pledgeNum;
    /**
     *    日期
    **/
    @JSONField(name="tradeDate")
    private Long tradeDate;
    /**
     *    所属行业
    **/
    @JSONField(name="trade")
    private String trade;
    /**
     *    限售股质押数（股）
    **/
    @JSONField(name="limitedAmount")
    private Integer limitedAmount;
    /**
     *    股票简称
    **/
    @JSONField(name="codeName")
    private String codeName;
    /**
     *    质押比例
    **/
    @JSONField(name="pledgeRatio")
    private String pledgeRatio;
    /**
     *    企业id
    **/
    @JSONField(name="graphId")
    private Integer graphId;
    /**
     *    质押市值（元）
    **/
    @JSONField(name="pledgeMarketValue")
    private Integer pledgeMarketValue;


    /**
    *   设置 日期
    **/
    public void setTradeDateFmt(String tradeDateFmt) {
      this.tradeDateFmt = tradeDateFmt;
    }
    /**
    *   获取 日期
    **/
    public String getTradeDateFmt() {
      return tradeDateFmt;
    }
    /**
    *   设置 股票代码
    **/
    public void setCode(String code) {
      this.code = code;
    }
    /**
    *   获取 股票代码
    **/
    public String getCode() {
      return code;
    }
    /**
    *   设置 近一年涨跌幅
    **/
    public void setQuoteChange(String quoteChange) {
      this.quoteChange = quoteChange;
    }
    /**
    *   获取 近一年涨跌幅
    **/
    public String getQuoteChange() {
      return quoteChange;
    }
    /**
    *   设置 质押股数（股）
    **/
    public void setPledgeAmount(Integer pledgeAmount) {
      this.pledgeAmount = pledgeAmount;
    }
    /**
    *   获取 质押股数（股）
    **/
    public Integer getPledgeAmount() {
      return pledgeAmount;
    }
    /**
    *   设置 无限售股质押数（股）
    **/
    public void setUnlimitedAmount(Integer unlimitedAmount) {
      this.unlimitedAmount = unlimitedAmount;
    }
    /**
    *   获取 无限售股质押数（股）
    **/
    public Integer getUnlimitedAmount() {
      return unlimitedAmount;
    }
    /**
    *   设置 质押笔数
    **/
    public void setPledgeNum(Integer pledgeNum) {
      this.pledgeNum = pledgeNum;
    }
    /**
    *   获取 质押笔数
    **/
    public Integer getPledgeNum() {
      return pledgeNum;
    }
    /**
    *   设置 日期
    **/
    public void setTradeDate(Long tradeDate) {
      this.tradeDate = tradeDate;
    }
    /**
    *   获取 日期
    **/
    public Long getTradeDate() {
      return tradeDate;
    }
    /**
    *   设置 所属行业
    **/
    public void setTrade(String trade) {
      this.trade = trade;
    }
    /**
    *   获取 所属行业
    **/
    public String getTrade() {
      return trade;
    }
    /**
    *   设置 限售股质押数（股）
    **/
    public void setLimitedAmount(Integer limitedAmount) {
      this.limitedAmount = limitedAmount;
    }
    /**
    *   获取 限售股质押数（股）
    **/
    public Integer getLimitedAmount() {
      return limitedAmount;
    }
    /**
    *   设置 股票简称
    **/
    public void setCodeName(String codeName) {
      this.codeName = codeName;
    }
    /**
    *   获取 股票简称
    **/
    public String getCodeName() {
      return codeName;
    }
    /**
    *   设置 质押比例
    **/
    public void setPledgeRatio(String pledgeRatio) {
      this.pledgeRatio = pledgeRatio;
    }
    /**
    *   获取 质押比例
    **/
    public String getPledgeRatio() {
      return pledgeRatio;
    }
    /**
    *   设置 企业id
    **/
    public void setGraphId(Integer graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 企业id
    **/
    public Integer getGraphId() {
      return graphId;
    }
    /**
    *   设置 质押市值（元）
    **/
    public void setPledgeMarketValue(Integer pledgeMarketValue) {
      this.pledgeMarketValue = pledgeMarketValue;
    }
    /**
    *   获取 质押市值（元）
    **/
    public Integer getPledgeMarketValue() {
      return pledgeMarketValue;
    }



}

