package com.gitee.deament.tianyancha.core.remote.stockannouncement.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockannouncement.entity.StockAnnouncement;
import com.gitee.deament.tianyancha.core.remote.stockannouncement.dto.StockAnnouncementDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*上市公告
* 可以通过公司名称或ID获取上市公司上市公告信息，上市公告信息包括公告标题、发布日期、股票名、股票号等
* /services/open/stock/announcement/2.0
*@author deament
**/
@Component("stockAnnouncementRequestImpl")
public class StockAnnouncementRequestImpl extends BaseRequestImpl<StockAnnouncement,StockAnnouncementDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/announcement/2.0";
    }
}

