package com.gitee.deament.tianyancha.core.remote.icparentcompany.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*总公司
* 可以通过公司名称或ID获取企业总公司信息，总公司信息包括总公司名称或ID、企业法人、经营状态、注册资本等字段的详细信息
* /services/open/ic/parentCompany/2.0
*@author deament
**/

public class IcParentCompany implements Serializable{

    /**
     *    法人图片
    **/
    @JSONField(name="personLogo")
    private String personLogo;
    /**
     *    经营状态
    **/
    @JSONField(name="reg_status")
    private String reg_status;
    /**
     *    总公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    注册资本
    **/
    @JSONField(name="reg_capital")
    private String reg_capital;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    公司简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblish_time")
    private String estiblish_time;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;


    /**
    *   设置 法人图片
    **/
    public void setPersonLogo(String personLogo) {
      this.personLogo = personLogo;
    }
    /**
    *   获取 法人图片
    **/
    public String getPersonLogo() {
      return personLogo;
    }
    /**
    *   设置 经营状态
    **/
    public void setReg_status(String reg_status) {
      this.reg_status = reg_status;
    }
    /**
    *   获取 经营状态
    **/
    public String getReg_status() {
      return reg_status;
    }
    /**
    *   设置 总公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 总公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 注册资本
    **/
    public void setReg_capital(String reg_capital) {
      this.reg_capital = reg_capital;
    }
    /**
    *   获取 注册资本
    **/
    public String getReg_capital() {
      return reg_capital;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 公司简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 公司简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblish_time(String estiblish_time) {
      this.estiblish_time = estiblish_time;
    }
    /**
    *   获取 成立日期
    **/
    public String getEstiblish_time() {
      return estiblish_time;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }



}

