package com.gitee.deament.tianyancha.core.remote.relashortpath.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*最短路径发现
* 属于RelaShortPath
* /services/open/rela/shortPath
*@author deament
**/

public class RelaShortPathP_0 implements Serializable{

    /**
     *    
    **/
    @JSONField(name="relationships")
    private List<RelaShortPathRelationships> relationships;
    /**
     *    节点
    **/
    @JSONField(name="nodes")
    private List<RelaShortPathNodes> nodes;


    /**
    *   设置 
    **/
    public void setRelationships(List<RelaShortPathRelationships> relationships) {
      this.relationships = relationships;
    }
    /**
    *   获取 
    **/
    public List<RelaShortPathRelationships> getRelationships() {
      return relationships;
    }
    /**
    *   设置 节点
    **/
    public void setNodes(List<RelaShortPathNodes> nodes) {
      this.nodes = nodes;
    }
    /**
    *   获取 节点
    **/
    public List<RelaShortPathNodes> getNodes() {
      return nodes;
    }



}

