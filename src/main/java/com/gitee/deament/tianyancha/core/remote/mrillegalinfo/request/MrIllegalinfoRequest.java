package com.gitee.deament.tianyancha.core.remote.mrillegalinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrillegalinfo.entity.MrIllegalinfo;
import com.gitee.deament.tianyancha.core.remote.mrillegalinfo.dto.MrIllegalinfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*严重违法
* 可以通过公司名称或ID获取企业严重违法信息，严重违法信息包括列入/移除原因、时间、做出决定机关等字段的详细信息
* /services/open/mr/illegalinfo/2.0
*@author deament
**/

public interface MrIllegalinfoRequest extends BaseRequest<MrIllegalinfoDTO ,MrIllegalinfo>{

}

