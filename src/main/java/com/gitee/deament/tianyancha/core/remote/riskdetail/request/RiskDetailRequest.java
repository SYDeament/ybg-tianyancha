package com.gitee.deament.tianyancha.core.remote.riskdetail.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetail;
import com.gitee.deament.tianyancha.core.remote.riskdetail.dto.RiskDetailDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*天眼风险详情
* 可以通过企业风险ID和人风险ID获取企业或人员相关天眼风险详情，包括企业和人员自身/周边/预警风险信息详情
* /services/v4/open/riskDetail
*@author deament
**/

public interface RiskDetailRequest extends BaseRequest<RiskDetailDTO ,RiskDetail>{

}

