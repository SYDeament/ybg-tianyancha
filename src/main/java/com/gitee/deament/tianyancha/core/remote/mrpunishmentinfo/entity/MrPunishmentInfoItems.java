package com.gitee.deament.tianyancha.core.remote.mrpunishmentinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*行政处罚-工商局
* 属于MrPunishmentInfo
* /services/open/mr/punishmentInfo/2.0
*@author deament
**/

public class MrPunishmentInfoItems implements Serializable{

    /**
     *    作出行政处罚决定机关名称
    **/
    @JSONField(name="departmentName")
    private String departmentName;
    /**
     *    行政处罚决定书文号
    **/
    @JSONField(name="punishNumber")
    private String punishNumber;
    /**
     *    注册号
    **/
    @JSONField(name="regNum")
    private String regNum;
    /**
     *    公司名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公示日期
    **/
    @JSONField(name="publishDate")
    private String publishDate;
    /**
     *    描述
    **/
    @JSONField(name="description")
    private String description;
    /**
     *    违法行为类型
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    行政处罚内容
    **/
    @JSONField(name="content")
    private String content;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;
    /**
     *    作出行政处罚决定日期
    **/
    @JSONField(name="decisionDate")
    private String decisionDate;
    /**
     *    法定代表人（负责人）姓名
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;


    /**
    *   设置 作出行政处罚决定机关名称
    **/
    public void setDepartmentName(String departmentName) {
      this.departmentName = departmentName;
    }
    /**
    *   获取 作出行政处罚决定机关名称
    **/
    public String getDepartmentName() {
      return departmentName;
    }
    /**
    *   设置 行政处罚决定书文号
    **/
    public void setPunishNumber(String punishNumber) {
      this.punishNumber = punishNumber;
    }
    /**
    *   获取 行政处罚决定书文号
    **/
    public String getPunishNumber() {
      return punishNumber;
    }
    /**
    *   设置 注册号
    **/
    public void setRegNum(String regNum) {
      this.regNum = regNum;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNum() {
      return regNum;
    }
    /**
    *   设置 公司名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公示日期
    **/
    public void setPublishDate(String publishDate) {
      this.publishDate = publishDate;
    }
    /**
    *   获取 公示日期
    **/
    public String getPublishDate() {
      return publishDate;
    }
    /**
    *   设置 描述
    **/
    public void setDescription(String description) {
      this.description = description;
    }
    /**
    *   获取 描述
    **/
    public String getDescription() {
      return description;
    }
    /**
    *   设置 违法行为类型
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 违法行为类型
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 行政处罚内容
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 行政处罚内容
    **/
    public String getContent() {
      return content;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }
    /**
    *   设置 作出行政处罚决定日期
    **/
    public void setDecisionDate(String decisionDate) {
      this.decisionDate = decisionDate;
    }
    /**
    *   获取 作出行政处罚决定日期
    **/
    public String getDecisionDate() {
      return decisionDate;
    }
    /**
    *   设置 法定代表人（负责人）姓名
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法定代表人（负责人）姓名
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }



}

