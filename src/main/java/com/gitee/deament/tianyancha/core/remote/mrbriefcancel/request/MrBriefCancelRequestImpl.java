package com.gitee.deament.tianyancha.core.remote.mrbriefcancel.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrbriefcancel.entity.MrBriefCancel;
import com.gitee.deament.tianyancha.core.remote.mrbriefcancel.dto.MrBriefCancelDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*简易注销
* 可以通过公司名称或ID获取企业简易注销公告信息，企业简易注销公告信息包括全体投资人承诺书、异议信息、简易注销结果等字段的详细信息
* /services/open/mr/briefCancel/2.0
*@author deament
**/
@Component("mrBriefCancelRequestImpl")
public class MrBriefCancelRequestImpl extends BaseRequestImpl<MrBriefCancel,MrBriefCancelDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/briefCancel/2.0";
    }
}

