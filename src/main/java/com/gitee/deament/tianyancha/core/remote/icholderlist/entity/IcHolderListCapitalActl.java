package com.gitee.deament.tianyancha.core.remote.icholderlist.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*公司公示-股东出资
* 属于IcHolderList
* /services/open/ic/holderList/2.0
*@author deament
**/

public class IcHolderListCapitalActl implements Serializable{

    /**
     *    实缴金额
    **/
    @JSONField(name="amomon")
    private String amomon;
    /**
     *    实缴形式
    **/
    @JSONField(name="paymet")
    private String paymet;
    /**
     *    实缴时间
    **/
    @JSONField(name="time")
    private String time;


    /**
    *   设置 实缴金额
    **/
    public void setAmomon(String amomon) {
      this.amomon = amomon;
    }
    /**
    *   获取 实缴金额
    **/
    public String getAmomon() {
      return amomon;
    }
    /**
    *   设置 实缴形式
    **/
    public void setPaymet(String paymet) {
      this.paymet = paymet;
    }
    /**
    *   获取 实缴形式
    **/
    public String getPaymet() {
      return paymet;
    }
    /**
    *   设置 实缴时间
    **/
    public void setTime(String time) {
      this.time = time;
    }
    /**
    *   获取 实缴时间
    **/
    public String getTime() {
      return time;
    }



}

