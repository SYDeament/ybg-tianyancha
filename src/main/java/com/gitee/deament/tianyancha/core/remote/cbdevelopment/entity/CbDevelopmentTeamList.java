package com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业发展
* 属于CbDevelopment
* /services/open/cb/development/2.0
*@author deament
**/

public class CbDevelopmentTeamList implements Serializable{

    /**
     *    人id
    **/
    @JSONField(name="hid")
    private Integer hid;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    logo
    **/
    @JSONField(name="icon")
    private String icon;
    /**
     *    姓名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公司id
    **/
    @JSONField(name="graphId")
    private Integer graphId;
    /**
     *    标签
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    oss地址
    **/
    @JSONField(name="iconOssPath")
    private String iconOssPath;
    /**
     *    人员描述
    **/
    @JSONField(name="desc")
    private String desc;
    /**
     *    0-现有成员 1-过往成员
    **/
    @JSONField(name="isDimission")
    private Integer isDimission;


    /**
    *   设置 人id
    **/
    public void setHid(Integer hid) {
      this.hid = hid;
    }
    /**
    *   获取 人id
    **/
    public Integer getHid() {
      return hid;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 logo
    **/
    public void setIcon(String icon) {
      this.icon = icon;
    }
    /**
    *   获取 logo
    **/
    public String getIcon() {
      return icon;
    }
    /**
    *   设置 姓名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 姓名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setGraphId(Integer graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getGraphId() {
      return graphId;
    }
    /**
    *   设置 标签
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标签
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 oss地址
    **/
    public void setIconOssPath(String iconOssPath) {
      this.iconOssPath = iconOssPath;
    }
    /**
    *   获取 oss地址
    **/
    public String getIconOssPath() {
      return iconOssPath;
    }
    /**
    *   设置 人员描述
    **/
    public void setDesc(String desc) {
      this.desc = desc;
    }
    /**
    *   获取 人员描述
    **/
    public String getDesc() {
      return desc;
    }
    /**
    *   设置 0-现有成员 1-过往成员
    **/
    public void setIsDimission(Integer isDimission) {
      this.isDimission = isDimission;
    }
    /**
    *   获取 0-现有成员 1-过往成员
    **/
    public Integer getIsDimission() {
      return isDimission;
    }



}

