package com.gitee.deament.tianyancha.core.remote.hipunishment.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hipunishment.entity.HiPunishmentItems;
/**
*历史行政处罚-其他来源
* 可以通过公司名称或ID获取企业历史的行政处罚信息，历史行政处罚信息包括行政处罚明细、行政处罚公告、行政处罚内容、公示日期、处罚依据、处罚状态等字段信息
* /services/open/hi/punishment/creditChina/2.0
*@author deament
**/

public class HiPunishment implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiPunishmentItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiPunishmentItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiPunishmentItems> getItems() {
      return items;
    }



}

