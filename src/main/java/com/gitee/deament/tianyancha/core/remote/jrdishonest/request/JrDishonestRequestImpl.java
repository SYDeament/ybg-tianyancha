package com.gitee.deament.tianyancha.core.remote.jrdishonest.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrdishonest.entity.JrDishonest;
import com.gitee.deament.tianyancha.core.remote.jrdishonest.dto.JrDishonestDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*失信人
* 可以通过公司名称或ID判定企业失信情况，失信信息包括失信人名称、组织机构代码、履行情况、失信行为具体情形等字段的详细信息
* /services/open/jr/dishonest/2.0
*@author deament
**/
@Component("jrDishonestRequestImpl")
public class JrDishonestRequestImpl extends BaseRequestImpl<JrDishonest,JrDishonestDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/jr/dishonest/2.0";
    }
}

