package com.gitee.deament.tianyancha.core.remote.mrlandmortgage.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*土地抵押详情
* 根据土地抵押ID获取土地抵押详情，土地抵押详情包括抵押面积、土地抵押人名称、抵押土地用途、评估金额、抵押金额、抵押人等字段的详细信息
* /services/open/mr/landMortgage/detail/2.0
*@author deament
**/

@TYCURL(value="/services/open/mr/landMortgage/detail/2.0")
public class MrLandMortgageDTO implements Serializable{

    /**
     *    抵押id
     *
    **/
    @ParamRequire(require = true)
    private Long id;


    /**
    *   设置 抵押id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 抵押id
    **/
    public Long getId() {
      return id;
    }



}

