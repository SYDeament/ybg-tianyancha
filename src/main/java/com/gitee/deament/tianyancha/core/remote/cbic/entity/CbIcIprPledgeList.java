package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcIprPledgeList implements Serializable{

    /**
     *    注销日期
    **/
    @JSONField(name="cancleDate")
    private String cancleDate;
    /**
     *    质权登记期限
    **/
    @JSONField(name="pledgeRegPeriod")
    private String pledgeRegPeriod;
    /**
     *    知识产权登记证号
    **/
    @JSONField(name="iprCertificateNum")
    private String iprCertificateNum;
    /**
     *    变更记录
    **/
    @JSONField(name="changeList")
    private List<CbIcChangeList> changeList;
    /**
     *    公示日期
    **/
    @JSONField(name="publicityDate")
    private String publicityDate;
    /**
     *    质权人名称
    **/
    @JSONField(name="pledgeeName")
    private String pledgeeName;
    /**
     *    表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    状态
    **/
    @JSONField(name="state")
    private String state;
    /**
     *    出质人
    **/
    @JSONField(name="pledgorName")
    private String pledgorName;
    /**
     *    名称
    **/
    @JSONField(name="iprName")
    private String iprName;
    /**
     *    种类
    **/
    @JSONField(name="iprType")
    private String iprType;
    /**
     *    注销原因
    **/
    @JSONField(name="cancleReason")
    private String cancleReason;


    /**
    *   设置 注销日期
    **/
    public void setCancleDate(String cancleDate) {
      this.cancleDate = cancleDate;
    }
    /**
    *   获取 注销日期
    **/
    public String getCancleDate() {
      return cancleDate;
    }
    /**
    *   设置 质权登记期限
    **/
    public void setPledgeRegPeriod(String pledgeRegPeriod) {
      this.pledgeRegPeriod = pledgeRegPeriod;
    }
    /**
    *   获取 质权登记期限
    **/
    public String getPledgeRegPeriod() {
      return pledgeRegPeriod;
    }
    /**
    *   设置 知识产权登记证号
    **/
    public void setIprCertificateNum(String iprCertificateNum) {
      this.iprCertificateNum = iprCertificateNum;
    }
    /**
    *   获取 知识产权登记证号
    **/
    public String getIprCertificateNum() {
      return iprCertificateNum;
    }
    /**
    *   设置 变更记录
    **/
    public void setChangeList(List<CbIcChangeList> changeList) {
      this.changeList = changeList;
    }
    /**
    *   获取 变更记录
    **/
    public List<CbIcChangeList> getChangeList() {
      return changeList;
    }
    /**
    *   设置 公示日期
    **/
    public void setPublicityDate(String publicityDate) {
      this.publicityDate = publicityDate;
    }
    /**
    *   获取 公示日期
    **/
    public String getPublicityDate() {
      return publicityDate;
    }
    /**
    *   设置 质权人名称
    **/
    public void setPledgeeName(String pledgeeName) {
      this.pledgeeName = pledgeeName;
    }
    /**
    *   获取 质权人名称
    **/
    public String getPledgeeName() {
      return pledgeeName;
    }
    /**
    *   设置 表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 状态
    **/
    public void setState(String state) {
      this.state = state;
    }
    /**
    *   获取 状态
    **/
    public String getState() {
      return state;
    }
    /**
    *   设置 出质人
    **/
    public void setPledgorName(String pledgorName) {
      this.pledgorName = pledgorName;
    }
    /**
    *   获取 出质人
    **/
    public String getPledgorName() {
      return pledgorName;
    }
    /**
    *   设置 名称
    **/
    public void setIprName(String iprName) {
      this.iprName = iprName;
    }
    /**
    *   获取 名称
    **/
    public String getIprName() {
      return iprName;
    }
    /**
    *   设置 种类
    **/
    public void setIprType(String iprType) {
      this.iprType = iprType;
    }
    /**
    *   获取 种类
    **/
    public String getIprType() {
      return iprType;
    }
    /**
    *   设置 注销原因
    **/
    public void setCancleReason(String cancleReason) {
      this.cancleReason = cancleReason;
    }
    /**
    *   获取 注销原因
    **/
    public String getCancleReason() {
      return cancleReason;
    }



}

