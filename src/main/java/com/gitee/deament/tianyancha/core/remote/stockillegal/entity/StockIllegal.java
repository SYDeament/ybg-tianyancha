package com.gitee.deament.tianyancha.core.remote.stockillegal.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockillegal.entity.StockIllegalResult;
/**
*违规处理
* 可以通过公司名称或ID获取上市公司违规处理信息，违规处理信息包括公告日期、处罚对象、处罚类型、处罚金额、处理人等
* /services/open/stock/illegal/2.0
*@author deament
**/

public class StockIllegal implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private List<StockIllegalResult> result;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;


    /**
    *   设置 
    **/
    public void setResult(List<StockIllegalResult> result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public List<StockIllegalResult> getResult() {
      return result;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }



}

