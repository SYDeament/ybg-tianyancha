package com.gitee.deament.tianyancha.core.remote.mannouncementreport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*公告研报
* 属于MAnnouncementReport
* /services/open/m/announcementReport/2.0
*@author deament
**/

public class MAnnouncementReportItems implements Serializable{

    /**
     *    发布日期
    **/
    @JSONField(name="date")
    private Long date;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    pdf下载路径
    **/
    @JSONField(name="url")
    private String url;


    /**
    *   设置 发布日期
    **/
    public void setDate(Long date) {
      this.date = date;
    }
    /**
    *   获取 发布日期
    **/
    public Long getDate() {
      return date;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 pdf下载路径
    **/
    public void setUrl(String url) {
      this.url = url;
    }
    /**
    *   获取 pdf下载路径
    **/
    public String getUrl() {
      return url;
    }



}

