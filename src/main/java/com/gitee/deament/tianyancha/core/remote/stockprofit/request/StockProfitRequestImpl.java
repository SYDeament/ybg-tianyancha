package com.gitee.deament.tianyancha.core.remote.stockprofit.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockprofit.entity.StockProfit;
import com.gitee.deament.tianyancha.core.remote.stockprofit.dto.StockProfitDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*利润表
* 可以通过公司名称或ID获取上市公司利润表数据，利润表数据包括营业总收入、营业总成本、其他经营收益、营业利润等
* /services/open/stock/profit/2.0
*@author deament
**/
@Component("stockProfitRequestImpl")
public class StockProfitRequestImpl extends BaseRequestImpl<StockProfit,StockProfitDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/profit/2.0";
    }
}

