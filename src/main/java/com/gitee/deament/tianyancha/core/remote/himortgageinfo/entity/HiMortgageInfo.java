package com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity.HiMortgageInfoBaseInfo;
import com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity.HiMortgageInfoPeopleInfo;
import com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity.HiMortgageInfoPawnInfoList;
import com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity.HiMortgageInfoChangeInfoList;
import com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity.HiMortgageInfoItems;
/**
*历史动产抵押
* 可以通过公司名称或ID获取企业历史的动产抵押公告信息，历史动产抵押公告信息包括被担保债权类型、数额、登记机关等字段信息
* /services/open/hi/mortgageInfo/2.0
*@author deament
**/

public class HiMortgageInfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiMortgageInfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiMortgageInfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiMortgageInfoItems> getItems() {
      return items;
    }



}

