package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryPunishmentCreditchinaList implements Serializable{

    /**
     *    作出行政处罚决定机关名称
    **/
    @JSONField(name="departmentName")
    private String departmentName;
    /**
     *    处罚结果
    **/
    @JSONField(name="result")
    private String result;
    /**
     *    处罚事由
    **/
    @JSONField(name="reason")
    private String reason;
    /**
     *    处罚名称
    **/
    @JSONField(name="punishName")
    private String punishName;
    /**
     *    处罚依据
    **/
    @JSONField(name="evidence")
    private String evidence;
    /**
     *    行政处罚决定书文号
    **/
    @JSONField(name="punishNumber")
    private String punishNumber;
    /**
     *    区域
    **/
    @JSONField(name="areaName")
    private String areaName;
    /**
     *    处罚类别2
    **/
    @JSONField(name="typeSecond")
    private String typeSecond;
    /**
     *    处罚状态(
    **/
    @JSONField(name="punishStatus")
    private String punishStatus;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    处罚类别
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    作出行政处罚决定日期
    **/
    @JSONField(name="decisionDate")
    private String decisionDate;


    /**
    *   设置 作出行政处罚决定机关名称
    **/
    public void setDepartmentName(String departmentName) {
      this.departmentName = departmentName;
    }
    /**
    *   获取 作出行政处罚决定机关名称
    **/
    public String getDepartmentName() {
      return departmentName;
    }
    /**
    *   设置 处罚结果
    **/
    public void setResult(String result) {
      this.result = result;
    }
    /**
    *   获取 处罚结果
    **/
    public String getResult() {
      return result;
    }
    /**
    *   设置 处罚事由
    **/
    public void setReason(String reason) {
      this.reason = reason;
    }
    /**
    *   获取 处罚事由
    **/
    public String getReason() {
      return reason;
    }
    /**
    *   设置 处罚名称
    **/
    public void setPunishName(String punishName) {
      this.punishName = punishName;
    }
    /**
    *   获取 处罚名称
    **/
    public String getPunishName() {
      return punishName;
    }
    /**
    *   设置 处罚依据
    **/
    public void setEvidence(String evidence) {
      this.evidence = evidence;
    }
    /**
    *   获取 处罚依据
    **/
    public String getEvidence() {
      return evidence;
    }
    /**
    *   设置 行政处罚决定书文号
    **/
    public void setPunishNumber(String punishNumber) {
      this.punishNumber = punishNumber;
    }
    /**
    *   获取 行政处罚决定书文号
    **/
    public String getPunishNumber() {
      return punishNumber;
    }
    /**
    *   设置 区域
    **/
    public void setAreaName(String areaName) {
      this.areaName = areaName;
    }
    /**
    *   获取 区域
    **/
    public String getAreaName() {
      return areaName;
    }
    /**
    *   设置 处罚类别2
    **/
    public void setTypeSecond(String typeSecond) {
      this.typeSecond = typeSecond;
    }
    /**
    *   获取 处罚类别2
    **/
    public String getTypeSecond() {
      return typeSecond;
    }
    /**
    *   设置 处罚状态(
    **/
    public void setPunishStatus(String punishStatus) {
      this.punishStatus = punishStatus;
    }
    /**
    *   获取 处罚状态(
    **/
    public String getPunishStatus() {
      return punishStatus;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 处罚类别
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 处罚类别
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 作出行政处罚决定日期
    **/
    public void setDecisionDate(String decisionDate) {
      this.decisionDate = decisionDate;
    }
    /**
    *   获取 作出行政处罚决定日期
    **/
    public String getDecisionDate() {
      return decisionDate;
    }



}

