package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcShareHolderList implements Serializable{

    /**
     *    认缴出资额
    **/
    @JSONField(name="subscribeAmount")
    private String subscribeAmount;
    /**
     *    认缴出资方式
    **/
    @JSONField(name="subscribeType")
    private String subscribeType;
    /**
     *    实缴出资方式
    **/
    @JSONField(name="paidType")
    private String paidType;
    /**
     *    认缴出资时间
    **/
    @JSONField(name="subscribeTime")
    private String subscribeTime;
    /**
     *    股东名称
    **/
    @JSONField(name="investorName")
    private String investorName;
    /**
     *    实缴出资时间
    **/
    @JSONField(name="paidTime")
    private String paidTime;
    /**
     *    实缴出资额
    **/
    @JSONField(name="paidAmount")
    private String paidAmount;


    /**
    *   设置 认缴出资额
    **/
    public void setSubscribeAmount(String subscribeAmount) {
      this.subscribeAmount = subscribeAmount;
    }
    /**
    *   获取 认缴出资额
    **/
    public String getSubscribeAmount() {
      return subscribeAmount;
    }
    /**
    *   设置 认缴出资方式
    **/
    public void setSubscribeType(String subscribeType) {
      this.subscribeType = subscribeType;
    }
    /**
    *   获取 认缴出资方式
    **/
    public String getSubscribeType() {
      return subscribeType;
    }
    /**
    *   设置 实缴出资方式
    **/
    public void setPaidType(String paidType) {
      this.paidType = paidType;
    }
    /**
    *   获取 实缴出资方式
    **/
    public String getPaidType() {
      return paidType;
    }
    /**
    *   设置 认缴出资时间
    **/
    public void setSubscribeTime(String subscribeTime) {
      this.subscribeTime = subscribeTime;
    }
    /**
    *   获取 认缴出资时间
    **/
    public String getSubscribeTime() {
      return subscribeTime;
    }
    /**
    *   设置 股东名称
    **/
    public void setInvestorName(String investorName) {
      this.investorName = investorName;
    }
    /**
    *   获取 股东名称
    **/
    public String getInvestorName() {
      return investorName;
    }
    /**
    *   设置 实缴出资时间
    **/
    public void setPaidTime(String paidTime) {
      this.paidTime = paidTime;
    }
    /**
    *   获取 实缴出资时间
    **/
    public String getPaidTime() {
      return paidTime;
    }
    /**
    *   设置 实缴出资额
    **/
    public void setPaidAmount(String paidAmount) {
      this.paidAmount = paidAmount;
    }
    /**
    *   获取 实缴出资额
    **/
    public String getPaidAmount() {
      return paidAmount;
    }



}

