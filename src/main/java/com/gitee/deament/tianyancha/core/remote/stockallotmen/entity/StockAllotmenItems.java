package com.gitee.deament.tianyancha.core.remote.stockallotmen.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*配股情况
* 属于StockAllotmen
* /services/open/stock/allotmen/2.0
*@author deament
**/

public class StockAllotmenItems implements Serializable{

    /**
     *    实际配股比例
    **/
    @JSONField(name="proportion")
    private String proportion;
    /**
     *    预案配股比例上限
    **/
    @JSONField(name="proportionalLimit")
    private String proportionalLimit;
    /**
     *    配股年份
    **/
    @JSONField(name="year")
    private String year;
    /**
     *    股东大会公告日
    **/
    @JSONField(name="saDate")
    private String saDate;
    /**
     *    实际募集资金净额
    **/
    @JSONField(name="actualRaise")
    private String actualRaise;
    /**
     *    证监会核准公告日
    **/
    @JSONField(name="pubDate")
    private String pubDate;
    /**
     *    发审委公告日
    **/
    @JSONField(name="announceDate")
    private String announceDate;
    /**
     *    董事会公告日
    **/
    @JSONField(name="dDate")
    private String dDate;
    /**
     *    除权日
    **/
    @JSONField(name="exDate")
    private String exDate;
    /**
     *    缴款起止日
    **/
    @JSONField(name="sDate")
    private String sDate;
    /**
     *    每股配股价格
    **/
    @JSONField(name="price")
    private String price;
    /**
     *    配股简称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    方案进度
    **/
    @JSONField(name="progress")
    private String progress;
    /**
     *    配股上市日
    **/
    @JSONField(name="issueDate")
    private String issueDate;
    /**
     *    配股号
    **/
    @JSONField(name="issueCode")
    private String issueCode;
    /**
     *    预案募资金额上限
    **/
    @JSONField(name="raiseCeiling")
    private String raiseCeiling;
    /**
     *    股权登记日
    **/
    @JSONField(name="registerDate")
    private String registerDate;


    /**
    *   设置 实际配股比例
    **/
    public void setProportion(String proportion) {
      this.proportion = proportion;
    }
    /**
    *   获取 实际配股比例
    **/
    public String getProportion() {
      return proportion;
    }
    /**
    *   设置 预案配股比例上限
    **/
    public void setProportionalLimit(String proportionalLimit) {
      this.proportionalLimit = proportionalLimit;
    }
    /**
    *   获取 预案配股比例上限
    **/
    public String getProportionalLimit() {
      return proportionalLimit;
    }
    /**
    *   设置 配股年份
    **/
    public void setYear(String year) {
      this.year = year;
    }
    /**
    *   获取 配股年份
    **/
    public String getYear() {
      return year;
    }
    /**
    *   设置 股东大会公告日
    **/
    public void setSaDate(String saDate) {
      this.saDate = saDate;
    }
    /**
    *   获取 股东大会公告日
    **/
    public String getSaDate() {
      return saDate;
    }
    /**
    *   设置 实际募集资金净额
    **/
    public void setActualRaise(String actualRaise) {
      this.actualRaise = actualRaise;
    }
    /**
    *   获取 实际募集资金净额
    **/
    public String getActualRaise() {
      return actualRaise;
    }
    /**
    *   设置 证监会核准公告日
    **/
    public void setPubDate(String pubDate) {
      this.pubDate = pubDate;
    }
    /**
    *   获取 证监会核准公告日
    **/
    public String getPubDate() {
      return pubDate;
    }
    /**
    *   设置 发审委公告日
    **/
    public void setAnnounceDate(String announceDate) {
      this.announceDate = announceDate;
    }
    /**
    *   获取 发审委公告日
    **/
    public String getAnnounceDate() {
      return announceDate;
    }
    /**
    *   设置 董事会公告日
    **/
    public void setDDate(String dDate) {
      this.dDate = dDate;
    }
    /**
    *   获取 董事会公告日
    **/
    public String getDDate() {
      return dDate;
    }
    /**
    *   设置 除权日
    **/
    public void setExDate(String exDate) {
      this.exDate = exDate;
    }
    /**
    *   获取 除权日
    **/
    public String getExDate() {
      return exDate;
    }
    /**
    *   设置 缴款起止日
    **/
    public void setSDate(String sDate) {
      this.sDate = sDate;
    }
    /**
    *   获取 缴款起止日
    **/
    public String getSDate() {
      return sDate;
    }
    /**
    *   设置 每股配股价格
    **/
    public void setPrice(String price) {
      this.price = price;
    }
    /**
    *   获取 每股配股价格
    **/
    public String getPrice() {
      return price;
    }
    /**
    *   设置 配股简称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 配股简称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 方案进度
    **/
    public void setProgress(String progress) {
      this.progress = progress;
    }
    /**
    *   获取 方案进度
    **/
    public String getProgress() {
      return progress;
    }
    /**
    *   设置 配股上市日
    **/
    public void setIssueDate(String issueDate) {
      this.issueDate = issueDate;
    }
    /**
    *   获取 配股上市日
    **/
    public String getIssueDate() {
      return issueDate;
    }
    /**
    *   设置 配股号
    **/
    public void setIssueCode(String issueCode) {
      this.issueCode = issueCode;
    }
    /**
    *   获取 配股号
    **/
    public String getIssueCode() {
      return issueCode;
    }
    /**
    *   设置 预案募资金额上限
    **/
    public void setRaiseCeiling(String raiseCeiling) {
      this.raiseCeiling = raiseCeiling;
    }
    /**
    *   获取 预案募资金额上限
    **/
    public String getRaiseCeiling() {
      return raiseCeiling;
    }
    /**
    *   设置 股权登记日
    **/
    public void setRegisterDate(String registerDate) {
      this.registerDate = registerDate;
    }
    /**
    *   获取 股权登记日
    **/
    public String getRegisterDate() {
      return registerDate;
    }



}

