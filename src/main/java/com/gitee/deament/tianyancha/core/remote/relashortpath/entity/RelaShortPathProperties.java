package com.gitee.deament.tianyancha.core.remote.relashortpath.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*最短路径发现
* 属于RelaShortPath
* /services/open/rela/shortPath
*@author deament
**/

public class RelaShortPathProperties implements Serializable{

    /**
     *    节点名称
    **/
    @JSONField(name="name")
    private String name;


    /**
    *   设置 节点名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 节点名称
    **/
    public String getName() {
      return name;
    }



}

