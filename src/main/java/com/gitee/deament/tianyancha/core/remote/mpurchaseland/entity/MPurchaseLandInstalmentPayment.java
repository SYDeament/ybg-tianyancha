package com.gitee.deament.tianyancha.core.remote.mpurchaseland.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*购地信息
* 属于MPurchaseLand
* /services/open/m/purchaseLand/2.0
*@author deament
**/

public class MPurchaseLandInstalmentPayment implements Serializable{

    /**
     *    分期支付约定支付期号
    **/
    @JSONField(name="instalmentPaymentContractPaymentPeriodNumber")
    private String instalmentPaymentContractPaymentPeriodNumber;
    /**
     *    分期支付约定约定支付日期
    **/
    @JSONField(name="instalmentPaymentConventionPaymentDate")
    private String instalmentPaymentConventionPaymentDate;
    /**
     *    约定支付金额(万元)
    **/
    @JSONField(name="instalmentPaymentAgreedPaymentAmount")
    private String instalmentPaymentAgreedPaymentAmount;
    /**
     *    分期支付约定备注
    **/
    @JSONField(name="instalmentPaymentAgreementNotes")
    private String instalmentPaymentAgreementNotes;
    /**
     *    电子监管号
    **/
    @JSONField(name="electronicRegulatoryNumber")
    private String electronicRegulatoryNumber;


    /**
    *   设置 分期支付约定支付期号
    **/
    public void setInstalmentPaymentContractPaymentPeriodNumber(String instalmentPaymentContractPaymentPeriodNumber) {
      this.instalmentPaymentContractPaymentPeriodNumber = instalmentPaymentContractPaymentPeriodNumber;
    }
    /**
    *   获取 分期支付约定支付期号
    **/
    public String getInstalmentPaymentContractPaymentPeriodNumber() {
      return instalmentPaymentContractPaymentPeriodNumber;
    }
    /**
    *   设置 分期支付约定约定支付日期
    **/
    public void setInstalmentPaymentConventionPaymentDate(String instalmentPaymentConventionPaymentDate) {
      this.instalmentPaymentConventionPaymentDate = instalmentPaymentConventionPaymentDate;
    }
    /**
    *   获取 分期支付约定约定支付日期
    **/
    public String getInstalmentPaymentConventionPaymentDate() {
      return instalmentPaymentConventionPaymentDate;
    }
    /**
    *   设置 约定支付金额(万元)
    **/
    public void setInstalmentPaymentAgreedPaymentAmount(String instalmentPaymentAgreedPaymentAmount) {
      this.instalmentPaymentAgreedPaymentAmount = instalmentPaymentAgreedPaymentAmount;
    }
    /**
    *   获取 约定支付金额(万元)
    **/
    public String getInstalmentPaymentAgreedPaymentAmount() {
      return instalmentPaymentAgreedPaymentAmount;
    }
    /**
    *   设置 分期支付约定备注
    **/
    public void setInstalmentPaymentAgreementNotes(String instalmentPaymentAgreementNotes) {
      this.instalmentPaymentAgreementNotes = instalmentPaymentAgreementNotes;
    }
    /**
    *   获取 分期支付约定备注
    **/
    public String getInstalmentPaymentAgreementNotes() {
      return instalmentPaymentAgreementNotes;
    }
    /**
    *   设置 电子监管号
    **/
    public void setElectronicRegulatoryNumber(String electronicRegulatoryNumber) {
      this.electronicRegulatoryNumber = electronicRegulatoryNumber;
    }
    /**
    *   获取 电子监管号
    **/
    public String getElectronicRegulatoryNumber() {
      return electronicRegulatoryNumber;
    }



}

