package com.gitee.deament.tianyancha.core.remote.description.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.description.entity.Description;
import com.gitee.deament.tianyancha.core.remote.description.dto.DescriptionDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*企业人员简介
* 可以通过公司名称或ID和人名获取人员简介信息
* /services/v4/open/description
*@author deament
**/
@Component("descriptionRequestImpl")
public class DescriptionRequestImpl extends BaseRequestImpl<Description,DescriptionDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/description";
    }
}

