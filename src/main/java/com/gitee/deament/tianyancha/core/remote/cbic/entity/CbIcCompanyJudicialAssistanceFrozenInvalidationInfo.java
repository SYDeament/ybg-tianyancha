package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcCompanyJudicialAssistanceFrozenInvalidationInfo implements Serializable{

    /**
     *    失效原因
    **/
    @JSONField(name="invalidationReason")
    private String invalidationReason;
    /**
     *    失效日期
    **/
    @JSONField(name="invalidationDate")
    private String invalidationDate;


    /**
    *   设置 失效原因
    **/
    public void setInvalidationReason(String invalidationReason) {
      this.invalidationReason = invalidationReason;
    }
    /**
    *   获取 失效原因
    **/
    public String getInvalidationReason() {
      return invalidationReason;
    }
    /**
    *   设置 失效日期
    **/
    public void setInvalidationDate(String invalidationDate) {
      this.invalidationDate = invalidationDate;
    }
    /**
    *   获取 失效日期
    **/
    public String getInvalidationDate() {
      return invalidationDate;
    }



}

