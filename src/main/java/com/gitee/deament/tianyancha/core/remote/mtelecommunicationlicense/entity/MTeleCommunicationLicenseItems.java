package com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*电信许可
* 属于MTeleCommunicationLicense
* /services/open/m/teleCommunicationLicense/2.0
*@author deament
**/

public class MTeleCommunicationLicenseItems implements Serializable{

    /**
     *    是否有效
    **/
    @JSONField(name="isAvailable")
    private String isAvailable;
    /**
     *    用户投诉量
    **/
    @JSONField(name="userComplainNum")
    private String userComplainNum;
    /**
     *    许可证业务种类
    **/
    @JSONField(name="licenseBusiType")
    private String licenseBusiType;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    企业性质
    **/
    @JSONField(name="companyType")
    private String companyType;
    /**
     *    企业名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    注册属地
    **/
    @JSONField(name="regTerritoriality")
    private String regTerritoriality;
    /**
     *    业务及其覆盖范围
    **/
    @JSONField(name="businessScope")
    private String businessScope;
    /**
     *    注册地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    用户投诉回复率
    **/
    @JSONField(name="userComplainReplyRate")
    private String userComplainReplyRate;
    /**
     *    股票代码
    **/
    @JSONField(name="stockCode")
    private String stockCode;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    法人id
    **/
    @JSONField(name="legalPersonId")
    private Integer legalPersonId;
    /**
     *    企业id
    **/
    @JSONField(name="companyId")
    private Integer companyId;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    上市情况
    **/
    @JSONField(name="listedState")
    private String listedState;
    /**
     *    许可证编号
    **/
    @JSONField(name="licenseNumber")
    private String licenseNumber;
    /**
     *    客户服务投诉电话
    **/
    @JSONField(name="customerComplainPhone")
    private String customerComplainPhone;


    /**
    *   设置 是否有效
    **/
    public void setIsAvailable(String isAvailable) {
      this.isAvailable = isAvailable;
    }
    /**
    *   获取 是否有效
    **/
    public String getIsAvailable() {
      return isAvailable;
    }
    /**
    *   设置 用户投诉量
    **/
    public void setUserComplainNum(String userComplainNum) {
      this.userComplainNum = userComplainNum;
    }
    /**
    *   获取 用户投诉量
    **/
    public String getUserComplainNum() {
      return userComplainNum;
    }
    /**
    *   设置 许可证业务种类
    **/
    public void setLicenseBusiType(String licenseBusiType) {
      this.licenseBusiType = licenseBusiType;
    }
    /**
    *   获取 许可证业务种类
    **/
    public String getLicenseBusiType() {
      return licenseBusiType;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 企业性质
    **/
    public void setCompanyType(String companyType) {
      this.companyType = companyType;
    }
    /**
    *   获取 企业性质
    **/
    public String getCompanyType() {
      return companyType;
    }
    /**
    *   设置 企业名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 企业名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 注册属地
    **/
    public void setRegTerritoriality(String regTerritoriality) {
      this.regTerritoriality = regTerritoriality;
    }
    /**
    *   获取 注册属地
    **/
    public String getRegTerritoriality() {
      return regTerritoriality;
    }
    /**
    *   设置 业务及其覆盖范围
    **/
    public void setBusinessScope(String businessScope) {
      this.businessScope = businessScope;
    }
    /**
    *   获取 业务及其覆盖范围
    **/
    public String getBusinessScope() {
      return businessScope;
    }
    /**
    *   设置 注册地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 注册地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 用户投诉回复率
    **/
    public void setUserComplainReplyRate(String userComplainReplyRate) {
      this.userComplainReplyRate = userComplainReplyRate;
    }
    /**
    *   获取 用户投诉回复率
    **/
    public String getUserComplainReplyRate() {
      return userComplainReplyRate;
    }
    /**
    *   设置 股票代码
    **/
    public void setStockCode(String stockCode) {
      this.stockCode = stockCode;
    }
    /**
    *   获取 股票代码
    **/
    public String getStockCode() {
      return stockCode;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 法人id
    **/
    public void setLegalPersonId(Integer legalPersonId) {
      this.legalPersonId = legalPersonId;
    }
    /**
    *   获取 法人id
    **/
    public Integer getLegalPersonId() {
      return legalPersonId;
    }
    /**
    *   设置 企业id
    **/
    public void setCompanyId(Integer companyId) {
      this.companyId = companyId;
    }
    /**
    *   获取 企业id
    **/
    public Integer getCompanyId() {
      return companyId;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 上市情况
    **/
    public void setListedState(String listedState) {
      this.listedState = listedState;
    }
    /**
    *   获取 上市情况
    **/
    public String getListedState() {
      return listedState;
    }
    /**
    *   设置 许可证编号
    **/
    public void setLicenseNumber(String licenseNumber) {
      this.licenseNumber = licenseNumber;
    }
    /**
    *   获取 许可证编号
    **/
    public String getLicenseNumber() {
      return licenseNumber;
    }
    /**
    *   设置 客户服务投诉电话
    **/
    public void setCustomerComplainPhone(String customerComplainPhone) {
      this.customerComplainPhone = customerComplainPhone;
    }
    /**
    *   获取 客户服务投诉电话
    **/
    public String getCustomerComplainPhone() {
      return customerComplainPhone;
    }



}

