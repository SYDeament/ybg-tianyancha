package com.gitee.deament.tianyancha.core.remote.stockguarantees.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockguarantees.entity.StockGuarantees;
import com.gitee.deament.tianyancha.core.remote.stockguarantees.dto.StockGuaranteesDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*对外担保
* 可以通过公司名称或ID获取上市公司对外担保信息，对外担保信息包括公告日期、担保方、被担保方、担保方式、担保金额等
* /services/open/stock/guarantees/2.0
*@author deament
**/
@Component("stockGuaranteesRequestImpl")
public class StockGuaranteesRequestImpl extends BaseRequestImpl<StockGuarantees,StockGuaranteesDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/guarantees/2.0";
    }
}

