package com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.entity.JrConsumptionRestriction;
import com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.dto.JrConsumptionRestrictionDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*限制消费令
* 可以通过公司名称或ID获取限制消费令信息，限制消费令信息包括执行法院、案件内容、被执行人名称等字段的详细信息
* /services/open/jr/consumptionRestriction/2.0
*@author deament
**/

public interface JrConsumptionRestrictionRequest extends BaseRequest<JrConsumptionRestrictionDTO ,JrConsumptionRestriction>{

}

