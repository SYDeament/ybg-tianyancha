package com.gitee.deament.tianyancha.core.remote.equityratio.entity;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;

/**
 *股权结构图
 * 属于EquityRatio
 * /services/v4/open/equityRatio
 *@author deament
 **/
public class EquityRatioTagList implements Serializable {
    /**
     * 标签名称
     */
    @JSONField(name="name")
    String name;
    /**
     * 标签类型
     */
    @JSONField(name="type")
    String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
