package com.gitee.deament.tianyancha.core.remote.mrjudicialsale.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*司法拍卖
* 属于MrJudicialSale
* /services/open/mr/judicialSale/2.0
*@author deament
**/

public class MrJudicialSaleItems implements Serializable{

    /**
     *    拍卖起止时间
    **/
    @JSONField(name="scopeDate")
    private String scopeDate;
    /**
     *     公告日期
    **/
    @JSONField(name="pubTime")
    private String pubTime;
    /**
     *    详细信息
    **/
    @JSONField(name="detail")
    private List<MrJudicialSaleDetail> detail;
    /**
     *     执行法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    公告url
    **/
    @JSONField(name="url")
    private String url;
    /**
     *    描述
    **/
    @JSONField(name="introduction")
    private String introduction;


    /**
    *   设置 拍卖起止时间
    **/
    public void setScopeDate(String scopeDate) {
      this.scopeDate = scopeDate;
    }
    /**
    *   获取 拍卖起止时间
    **/
    public String getScopeDate() {
      return scopeDate;
    }
    /**
    *   设置  公告日期
    **/
    public void setPubTime(String pubTime) {
      this.pubTime = pubTime;
    }
    /**
    *   获取  公告日期
    **/
    public String getPubTime() {
      return pubTime;
    }
    /**
    *   设置 详细信息
    **/
    public void setDetail(List<MrJudicialSaleDetail> detail) {
      this.detail = detail;
    }
    /**
    *   获取 详细信息
    **/
    public List<MrJudicialSaleDetail> getDetail() {
      return detail;
    }
    /**
    *   设置  执行法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取  执行法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 公告url
    **/
    public void setUrl(String url) {
      this.url = url;
    }
    /**
    *   获取 公告url
    **/
    public String getUrl() {
      return url;
    }
    /**
    *   设置 描述
    **/
    public void setIntroduction(String introduction) {
      this.introduction = introduction;
    }
    /**
    *   获取 描述
    **/
    public String getIntroduction() {
      return introduction;
    }



}

