package com.gitee.deament.tianyancha.core.remote.mrabnormal.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrabnormal.entity.MrAbnormal;
import com.gitee.deament.tianyancha.core.remote.mrabnormal.dto.MrAbnormalDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*经营异常
* 可以通过公司名称或ID获取企业经营异常信息，经营异常信息包括列入/移除原因、时间、做出决定机关等字段的详细信息
* /services/open/mr/abnormal/2.0
*@author deament
**/
@Component("mrAbnormalRequestImpl")
public class MrAbnormalRequestImpl extends BaseRequestImpl<MrAbnormal,MrAbnormalDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/abnormal/2.0";
    }
}

