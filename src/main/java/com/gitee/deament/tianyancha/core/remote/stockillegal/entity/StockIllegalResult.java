package com.gitee.deament.tianyancha.core.remote.stockillegal.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*违规处理
* 属于StockIllegal
* /services/open/stock/illegal/2.0
*@author deament
**/

public class StockIllegalResult implements Serializable{

    /**
     *    处罚类型
    **/
    @JSONField(name="punish_type")
    private String punish_type;
    /**
     *    处分措施
    **/
    @JSONField(name="punish_explain")
    private String punish_explain;
    /**
     *    处罚金额（万元）
    **/
    @JSONField(name="punish_amt")
    private String punish_amt;
    /**
     *    处分措施
    **/
    @JSONField(name="default_type")
    private String default_type;
    /**
     *    公告日期
    **/
    @JSONField(name="announcement_date")
    private Long announcement_date;
    /**
     *    处罚对象
    **/
    @JSONField(name="punish_object")
    private String punish_object;
    /**
     *    处理人
    **/
    @JSONField(name="disposer")
    private String disposer;
    /**
     *    违规行为
    **/
    @JSONField(name="illegal_act")
    private String illegal_act;


    /**
    *   设置 处罚类型
    **/
    public void setPunish_type(String punish_type) {
      this.punish_type = punish_type;
    }
    /**
    *   获取 处罚类型
    **/
    public String getPunish_type() {
      return punish_type;
    }
    /**
    *   设置 处分措施
    **/
    public void setPunish_explain(String punish_explain) {
      this.punish_explain = punish_explain;
    }
    /**
    *   获取 处分措施
    **/
    public String getPunish_explain() {
      return punish_explain;
    }
    /**
    *   设置 处罚金额（万元）
    **/
    public void setPunish_amt(String punish_amt) {
      this.punish_amt = punish_amt;
    }
    /**
    *   获取 处罚金额（万元）
    **/
    public String getPunish_amt() {
      return punish_amt;
    }
    /**
    *   设置 处分措施
    **/
    public void setDefault_type(String default_type) {
      this.default_type = default_type;
    }
    /**
    *   获取 处分措施
    **/
    public String getDefault_type() {
      return default_type;
    }
    /**
    *   设置 公告日期
    **/
    public void setAnnouncement_date(Long announcement_date) {
      this.announcement_date = announcement_date;
    }
    /**
    *   获取 公告日期
    **/
    public Long getAnnouncement_date() {
      return announcement_date;
    }
    /**
    *   设置 处罚对象
    **/
    public void setPunish_object(String punish_object) {
      this.punish_object = punish_object;
    }
    /**
    *   获取 处罚对象
    **/
    public String getPunish_object() {
      return punish_object;
    }
    /**
    *   设置 处理人
    **/
    public void setDisposer(String disposer) {
      this.disposer = disposer;
    }
    /**
    *   获取 处理人
    **/
    public String getDisposer() {
      return disposer;
    }
    /**
    *   设置 违规行为
    **/
    public void setIllegal_act(String illegal_act) {
      this.illegal_act = illegal_act;
    }
    /**
    *   获取 违规行为
    **/
    public String getIllegal_act() {
      return illegal_act;
    }



}

