package com.gitee.deament.tianyancha.core.remote.mbids.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mbids.entity.MBidsItems;
/**
*招投标
* 可以通过公司名称或ID获企业招投标信息，招投标信息包括采购人、发布时间、正文信息等字段的详细信息
* /services/open/m/bids/2.0
*@author deament
**/

public class MBids implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private String total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MBidsItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(String total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public String getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MBidsItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MBidsItems> getItems() {
      return items;
    }



}

