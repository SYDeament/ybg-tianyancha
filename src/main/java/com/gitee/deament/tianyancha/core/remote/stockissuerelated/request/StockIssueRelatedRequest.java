package com.gitee.deament.tianyancha.core.remote.stockissuerelated.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockissuerelated.entity.StockIssueRelated;
import com.gitee.deament.tianyancha.core.remote.stockissuerelated.dto.StockIssueRelatedDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*发行相关
* 可以通过公司名称或ID获取上市公司发行相关信息，发行相关信息包括成立日期、上市日期、发行数量、发行价格等
* /services/open/stock/issueRelated/2.0
*@author deament
**/

public interface StockIssueRelatedRequest extends BaseRequest<StockIssueRelatedDTO ,StockIssueRelated>{

}

