package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype18;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailPlaintiffList;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDefendantList;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype27;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype17;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype25;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype15;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype6;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype23;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailPlaintiff;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailLitigant2;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDefendant;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype13;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype8;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype21;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype11;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailResult;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailAnnouncement;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailObjection;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype31;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDetail;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype29;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype19;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype16;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype9;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype28;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailParty2StrApp;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailParty1StrApp;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype14;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype26;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype12;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype5;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype24;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailBaseInfo;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailCancelInfo;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailPeopleInfo;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailPawnInfoList;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype10;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype7;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype22;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype30;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype1;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype20;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.RiskDetailDataListtype3;
/**
*天眼风险详情
* 可以通过企业风险ID和人风险ID获取企业或人员相关天眼风险详情，包括企业和人员自身/周边/预警风险信息详情
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetail implements Serializable{

    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    1-严重违法，2-失信人，3-失信公司，4-被执行人，5-被执行公司，6-行政处罚，7-经营异常，8-法律诉讼，9-股权出质，10-动产抵押，11-欠税公告，12-名称变更，13-开庭公告，14-法院公告，15-法人变更，16-投资人变更，17-主要人员变更，18-注册资本变更，19-注册地址变更，20-出资情况变更，21-司法协助，22-清算信息，23-知识产权出质，24-环保处罚，25-公示催告，26-送达公告，27-立案信息，28-税收违法，29-司法拍卖，30-土地抵押，31-简易注销
    **/
    @JSONField(name="type")
    private Integer type;

    @JSONField(name="dataList")
    private JSONArray dataList;

    public JSONArray getDataList() {
        return dataList;
    }

    public void setDataList(JSONArray dataList) {
        this.dataList = dataList;
    }

    /**
     *    注册资本变更
    **/
    @JSONField(name="dataListtype18")
    private List<RiskDetailDataListtype18> dataListtype18;
    /**
     *    立案信息
    **/
    @JSONField(name="dataListtype27")
    private List<RiskDetailDataListtype27> dataListtype27;
    /**
     *    主要人员变更
    **/
    @JSONField(name="dataListtype17")
    private List<RiskDetailDataListtype17> dataListtype17;
    /**
     *    公示催告
    **/
    @JSONField(name="dataListtype25")
    private List<RiskDetailDataListtype25> dataListtype25;
    /**
     *    法人变更
    **/
    @JSONField(name="dataListtype15")
    private List<RiskDetailDataListtype15> dataListtype15;
    /**
     *    行政处罚
    **/
    @JSONField(name="dataListtype6")
    private List<RiskDetailDataListtype6> dataListtype6;
    /**
     *    知识产权出质
    **/
    @JSONField(name="dataListtype23")
    private List<RiskDetailDataListtype23> dataListtype23;
    /**
     *    开庭公告
    **/
    @JSONField(name="dataListtype13")
    private List<RiskDetailDataListtype13> dataListtype13;
    /**
     *    法律诉讼
    **/
    @JSONField(name="dataListtype8")
    private List<RiskDetailDataListtype8> dataListtype8;
    /**
     *    司法协助
    **/
    @JSONField(name="dataListtype21")
    private List<RiskDetailDataListtype21> dataListtype21;
    /**
     *    欠税公告
    **/
    @JSONField(name="dataListtype11")
    private List<RiskDetailDataListtype11> dataListtype11;
    /**
     *    简易注销
    **/
    @JSONField(name="dataListtype31")
    private List<RiskDetailDataListtype31> dataListtype31;
    /**
     *    总数
    **/
    @JSONField(name="count")
    private Integer count;
    /**
     *    司法拍卖
    **/
    @JSONField(name="dataListtype29")
    private List<RiskDetailDataListtype29> dataListtype29;
    /**
     *    地址变更
    **/
    @JSONField(name="dataListtype19")
    private List<RiskDetailDataListtype19> dataListtype19;
    /**
     *    投资人变更
    **/
    @JSONField(name="dataListtype16")
    private List<RiskDetailDataListtype16> dataListtype16;
    /**
     *    股权出质
    **/
    @JSONField(name="dataListtype9")
    private List<RiskDetailDataListtype9> dataListtype9;
    /**
     *    税收违法
    **/
    @JSONField(name="dataListtype28")
    private List<RiskDetailDataListtype28> dataListtype28;
    /**
     *    法院公告
    **/
    @JSONField(name="dataListtype14")
    private List<RiskDetailDataListtype14> dataListtype14;
    /**
     *    公司id
    **/
    @JSONField(name="companyId")
    private Integer companyId;
    /**
     *    送达公告
    **/
    @JSONField(name="dataListtype26")
    private List<RiskDetailDataListtype26> dataListtype26;
    /**
     *    名称变更
    **/
    @JSONField(name="dataListtype12")
    private List<RiskDetailDataListtype12> dataListtype12;
    /**
     *    被执行（人）公司
    **/
    @JSONField(name="dataListtype5")
    private List<RiskDetailDataListtype5> dataListtype5;
    /**
     *    环保处罚
    **/
    @JSONField(name="dataListtype24")
    private List<RiskDetailDataListtype24> dataListtype24;
    /**
     *    动产抵押
    **/
    @JSONField(name="dataListtype10")
    private List<RiskDetailDataListtype10> dataListtype10;
    /**
     *    经营异常
    **/
    @JSONField(name="dataListtype7")
    private List<RiskDetailDataListtype7> dataListtype7;
    /**
     *    清算信息
    **/
    @JSONField(name="dataListtype22")
    private List<RiskDetailDataListtype22> dataListtype22;
    /**
     *    土地抵押
    **/
    @JSONField(name="dataListtype30")
    private List<RiskDetailDataListtype30> dataListtype30;
    /**
     *    严重违法
    **/
    @JSONField(name="dataListtype1")
    private List<RiskDetailDataListtype1> dataListtype1;
    /**
     *    出资情况变更
    **/
    @JSONField(name="dataListtype20")
    private List<RiskDetailDataListtype20> dataListtype20;
    /**
     *    失信（人）公司
    **/
    @JSONField(name="dataListtype3")
    private List<RiskDetailDataListtype3> dataListtype3;
    /**
     *    当前页
    **/
    @JSONField(name="pn")
    private Integer pn;


    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 1-严重违法，2-失信人，3-失信公司，4-被执行人，5-被执行公司，6-行政处罚，7-经营异常，8-法律诉讼，9-股权出质，10-动产抵押，11-欠税公告，12-名称变更，13-开庭公告，14-法院公告，15-法人变更，16-投资人变更，17-主要人员变更，18-注册资本变更，19-注册地址变更，20-出资情况变更，21-司法协助，22-清算信息，23-知识产权出质，24-环保处罚，25-公示催告，26-送达公告，27-立案信息，28-税收违法，29-司法拍卖，30-土地抵押，31-简易注销
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-严重违法，2-失信人，3-失信公司，4-被执行人，5-被执行公司，6-行政处罚，7-经营异常，8-法律诉讼，9-股权出质，10-动产抵押，11-欠税公告，12-名称变更，13-开庭公告，14-法院公告，15-法人变更，16-投资人变更，17-主要人员变更，18-注册资本变更，19-注册地址变更，20-出资情况变更，21-司法协助，22-清算信息，23-知识产权出质，24-环保处罚，25-公示催告，26-送达公告，27-立案信息，28-税收违法，29-司法拍卖，30-土地抵押，31-简易注销
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 注册资本变更
    **/
    public void setDataListtype18(List<RiskDetailDataListtype18> dataListtype18) {
      this.dataListtype18 = dataListtype18;
    }
    /**
    *   获取 注册资本变更
    **/
    public List<RiskDetailDataListtype18> getDataListtype18() {
      return dataListtype18;
    }
    /**
    *   设置 立案信息
    **/
    public void setDataListtype27(List<RiskDetailDataListtype27> dataListtype27) {
      this.dataListtype27 = dataListtype27;
    }
    /**
    *   获取 立案信息
    **/
    public List<RiskDetailDataListtype27> getDataListtype27() {
      return dataListtype27;
    }
    /**
    *   设置 主要人员变更
    **/
    public void setDataListtype17(List<RiskDetailDataListtype17> dataListtype17) {
      this.dataListtype17 = dataListtype17;
    }
    /**
    *   获取 主要人员变更
    **/
    public List<RiskDetailDataListtype17> getDataListtype17() {
      return dataListtype17;
    }
    /**
    *   设置 公示催告
    **/
    public void setDataListtype25(List<RiskDetailDataListtype25> dataListtype25) {
      this.dataListtype25 = dataListtype25;
    }
    /**
    *   获取 公示催告
    **/
    public List<RiskDetailDataListtype25> getDataListtype25() {
      return dataListtype25;
    }
    /**
    *   设置 法人变更
    **/
    public void setDataListtype15(List<RiskDetailDataListtype15> dataListtype15) {
      this.dataListtype15 = dataListtype15;
    }
    /**
    *   获取 法人变更
    **/
    public List<RiskDetailDataListtype15> getDataListtype15() {
      return dataListtype15;
    }
    /**
    *   设置 行政处罚
    **/
    public void setDataListtype6(List<RiskDetailDataListtype6> dataListtype6) {
      this.dataListtype6 = dataListtype6;
    }
    /**
    *   获取 行政处罚
    **/
    public List<RiskDetailDataListtype6> getDataListtype6() {
      return dataListtype6;
    }
    /**
    *   设置 知识产权出质
    **/
    public void setDataListtype23(List<RiskDetailDataListtype23> dataListtype23) {
      this.dataListtype23 = dataListtype23;
    }
    /**
    *   获取 知识产权出质
    **/
    public List<RiskDetailDataListtype23> getDataListtype23() {
      return dataListtype23;
    }
    /**
    *   设置 开庭公告
    **/
    public void setDataListtype13(List<RiskDetailDataListtype13> dataListtype13) {
      this.dataListtype13 = dataListtype13;
    }
    /**
    *   获取 开庭公告
    **/
    public List<RiskDetailDataListtype13> getDataListtype13() {
      return dataListtype13;
    }
    /**
    *   设置 法律诉讼
    **/
    public void setDataListtype8(List<RiskDetailDataListtype8> dataListtype8) {
      this.dataListtype8 = dataListtype8;
    }
    /**
    *   获取 法律诉讼
    **/
    public List<RiskDetailDataListtype8> getDataListtype8() {
      return dataListtype8;
    }
    /**
    *   设置 司法协助
    **/
    public void setDataListtype21(List<RiskDetailDataListtype21> dataListtype21) {
      this.dataListtype21 = dataListtype21;
    }
    /**
    *   获取 司法协助
    **/
    public List<RiskDetailDataListtype21> getDataListtype21() {
      return dataListtype21;
    }
    /**
    *   设置 欠税公告
    **/
    public void setDataListtype11(List<RiskDetailDataListtype11> dataListtype11) {
      this.dataListtype11 = dataListtype11;
    }
    /**
    *   获取 欠税公告
    **/
    public List<RiskDetailDataListtype11> getDataListtype11() {
      return dataListtype11;
    }
    /**
    *   设置 简易注销
    **/
    public void setDataListtype31(List<RiskDetailDataListtype31> dataListtype31) {
      this.dataListtype31 = dataListtype31;
    }
    /**
    *   获取 简易注销
    **/
    public List<RiskDetailDataListtype31> getDataListtype31() {
      return dataListtype31;
    }
    /**
    *   设置 总数
    **/
    public void setCount(Integer count) {
      this.count = count;
    }
    /**
    *   获取 总数
    **/
    public Integer getCount() {
      return count;
    }
    /**
    *   设置 司法拍卖
    **/
    public void setDataListtype29(List<RiskDetailDataListtype29> dataListtype29) {
      this.dataListtype29 = dataListtype29;
    }
    /**
    *   获取 司法拍卖
    **/
    public List<RiskDetailDataListtype29> getDataListtype29() {
      return dataListtype29;
    }
    /**
    *   设置 地址变更
    **/
    public void setDataListtype19(List<RiskDetailDataListtype19> dataListtype19) {
      this.dataListtype19 = dataListtype19;
    }
    /**
    *   获取 地址变更
    **/
    public List<RiskDetailDataListtype19> getDataListtype19() {
      return dataListtype19;
    }
    /**
    *   设置 投资人变更
    **/
    public void setDataListtype16(List<RiskDetailDataListtype16> dataListtype16) {
      this.dataListtype16 = dataListtype16;
    }
    /**
    *   获取 投资人变更
    **/
    public List<RiskDetailDataListtype16> getDataListtype16() {
      return dataListtype16;
    }
    /**
    *   设置 股权出质
    **/
    public void setDataListtype9(List<RiskDetailDataListtype9> dataListtype9) {
      this.dataListtype9 = dataListtype9;
    }
    /**
    *   获取 股权出质
    **/
    public List<RiskDetailDataListtype9> getDataListtype9() {
      return dataListtype9;
    }
    /**
    *   设置 税收违法
    **/
    public void setDataListtype28(List<RiskDetailDataListtype28> dataListtype28) {
      this.dataListtype28 = dataListtype28;
    }
    /**
    *   获取 税收违法
    **/
    public List<RiskDetailDataListtype28> getDataListtype28() {
      return dataListtype28;
    }
    /**
    *   设置 法院公告
    **/
    public void setDataListtype14(List<RiskDetailDataListtype14> dataListtype14) {
      this.dataListtype14 = dataListtype14;
    }
    /**
    *   获取 法院公告
    **/
    public List<RiskDetailDataListtype14> getDataListtype14() {
      return dataListtype14;
    }
    /**
    *   设置 公司id
    **/
    public void setCompanyId(Integer companyId) {
      this.companyId = companyId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCompanyId() {
      return companyId;
    }
    /**
    *   设置 送达公告
    **/
    public void setDataListtype26(List<RiskDetailDataListtype26> dataListtype26) {
      this.dataListtype26 = dataListtype26;
    }
    /**
    *   获取 送达公告
    **/
    public List<RiskDetailDataListtype26> getDataListtype26() {
      return dataListtype26;
    }
    /**
    *   设置 名称变更
    **/
    public void setDataListtype12(List<RiskDetailDataListtype12> dataListtype12) {
      this.dataListtype12 = dataListtype12;
    }
    /**
    *   获取 名称变更
    **/
    public List<RiskDetailDataListtype12> getDataListtype12() {
      return dataListtype12;
    }
    /**
    *   设置 被执行（人）公司
    **/
    public void setDataListtype5(List<RiskDetailDataListtype5> dataListtype5) {
      this.dataListtype5 = dataListtype5;
    }
    /**
    *   获取 被执行（人）公司
    **/
    public List<RiskDetailDataListtype5> getDataListtype5() {
      return dataListtype5;
    }
    /**
    *   设置 环保处罚
    **/
    public void setDataListtype24(List<RiskDetailDataListtype24> dataListtype24) {
      this.dataListtype24 = dataListtype24;
    }
    /**
    *   获取 环保处罚
    **/
    public List<RiskDetailDataListtype24> getDataListtype24() {
      return dataListtype24;
    }
    /**
    *   设置 动产抵押
    **/
    public void setDataListtype10(List<RiskDetailDataListtype10> dataListtype10) {
      this.dataListtype10 = dataListtype10;
    }
    /**
    *   获取 动产抵押
    **/
    public List<RiskDetailDataListtype10> getDataListtype10() {
      return dataListtype10;
    }
    /**
    *   设置 经营异常
    **/
    public void setDataListtype7(List<RiskDetailDataListtype7> dataListtype7) {
      this.dataListtype7 = dataListtype7;
    }
    /**
    *   获取 经营异常
    **/
    public List<RiskDetailDataListtype7> getDataListtype7() {
      return dataListtype7;
    }
    /**
    *   设置 清算信息
    **/
    public void setDataListtype22(List<RiskDetailDataListtype22> dataListtype22) {
      this.dataListtype22 = dataListtype22;
    }
    /**
    *   获取 清算信息
    **/
    public List<RiskDetailDataListtype22> getDataListtype22() {
      return dataListtype22;
    }
    /**
    *   设置 土地抵押
    **/
    public void setDataListtype30(List<RiskDetailDataListtype30> dataListtype30) {
      this.dataListtype30 = dataListtype30;
    }
    /**
    *   获取 土地抵押
    **/
    public List<RiskDetailDataListtype30> getDataListtype30() {
      return dataListtype30;
    }
    /**
    *   设置 严重违法
    **/
    public void setDataListtype1(List<RiskDetailDataListtype1> dataListtype1) {
      this.dataListtype1 = dataListtype1;
    }
    /**
    *   获取 严重违法
    **/
    public List<RiskDetailDataListtype1> getDataListtype1() {
      return dataListtype1;
    }
    /**
    *   设置 出资情况变更
    **/
    public void setDataListtype20(List<RiskDetailDataListtype20> dataListtype20) {
      this.dataListtype20 = dataListtype20;
    }
    /**
    *   获取 出资情况变更
    **/
    public List<RiskDetailDataListtype20> getDataListtype20() {
      return dataListtype20;
    }
    /**
    *   设置 失信（人）公司
    **/
    public void setDataListtype3(List<RiskDetailDataListtype3> dataListtype3) {
      this.dataListtype3 = dataListtype3;
    }
    /**
    *   获取 失信（人）公司
    **/
    public List<RiskDetailDataListtype3> getDataListtype3() {
      return dataListtype3;
    }
    /**
    *   设置 当前页
    **/
    public void setPn(Integer pn) {
      this.pn = pn;
    }
    /**
    *   获取 当前页
    **/
    public Integer getPn() {
      return pn;
    }



}

