package com.gitee.deament.tianyancha.core.remote.proximity.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.proximity.entity.Proximity;
import com.gitee.deament.tianyancha.core.remote.proximity.dto.ProximityDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*天眼地图
* 附近公司
* /services/v4/open/proximity
*@author deament
**/

public interface ProximityRequest extends BaseRequest<ProximityDTO ,Proximity>{

}

