package com.gitee.deament.tianyancha.core.remote.stockshareholder.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockshareholder.entity.StockShareholderHolderList;
/**
*十大股东（十大流通股东）
* 可以通过公司名称或ID获取上市公司十大股东和十大流通股东信息，十大股东和十大流通股东信息包括机构或基金、持有数量、持股变化、占股本比例、实际增减持、股份类型等
* /services/open/stock/shareholder/2.0
*@author deament
**/

public class StockShareholder implements Serializable{

    /**
     *    时间列表
    **/
    @JSONField(name="timeList")
    private List<String> timeList;
    /**
     *    股东列表
    **/
    @JSONField(name="holderList")
    private List<StockShareholderHolderList> holderList;


    /**
    *   设置 时间列表
    **/
    public void setTimeList(List<String> timeList) {
      this.timeList = timeList;
    }
    /**
    *   获取 时间列表
    **/
    public List<String> getTimeList() {
      return timeList;
    }
    /**
    *   设置 股东列表
    **/
    public void setHolderList(List<StockShareholderHolderList> holderList) {
      this.holderList = holderList;
    }
    /**
    *   获取 股东列表
    **/
    public List<StockShareholderHolderList> getHolderList() {
      return holderList;
    }



}

