package com.gitee.deament.tianyancha.core.remote.stocksharestructure.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stocksharestructure.entity.StockShareStructure;
import com.gitee.deament.tianyancha.core.remote.stocksharestructure.dto.StockShareStructureDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*股本结构
* 可以通过公司名称或ID获取上市公司股本结构信息，股本结构信息包括总股本、A股总股本、流通A股、限售A股等
* /services/open/stock/shareStructure/2.0
*@author deament
**/

public interface StockShareStructureRequest extends BaseRequest<StockShareStructureDTO ,StockShareStructure>{

}

