package com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*疑似实际控制人
* 属于IcActualControl
* /services/open/ic/actualControl/2.0
*@author deament
**/

public class IcActualControlProperties implements Serializable{

    /**
     *    股东名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    1-人 2-公司
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 股东名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 股东名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 1-人 2-公司
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-人 2-公司
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

