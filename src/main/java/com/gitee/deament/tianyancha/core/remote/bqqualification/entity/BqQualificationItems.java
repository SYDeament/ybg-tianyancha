package com.gitee.deament.tianyancha.core.remote.bqqualification.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-资质资格
* 属于BqQualification
* /services/open/bq/qualification/2.0
*@author deament
**/

public class BqQualificationItems implements Serializable{

    /**
     *    发证机关
    **/
    @JSONField(name="organ")
    private String organ;
    /**
     *    证书有效期
    **/
    @JSONField(name="effectiveTime")
    private String effectiveTime;
    /**
     *    资格id
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    发证日期
    **/
    @JSONField(name="issuingCertificateTime")
    private String issuingCertificateTime;
    /**
     *    资质类别
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    资质证书号
    **/
    @JSONField(name="certificateNum")
    private String certificateNum;
    /**
     *    资质名称
    **/
    @JSONField(name="qualificationName")
    private String qualificationName;


    /**
    *   设置 发证机关
    **/
    public void setOrgan(String organ) {
      this.organ = organ;
    }
    /**
    *   获取 发证机关
    **/
    public String getOrgan() {
      return organ;
    }
    /**
    *   设置 证书有效期
    **/
    public void setEffectiveTime(String effectiveTime) {
      this.effectiveTime = effectiveTime;
    }
    /**
    *   获取 证书有效期
    **/
    public String getEffectiveTime() {
      return effectiveTime;
    }
    /**
    *   设置 资格id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 资格id
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 发证日期
    **/
    public void setIssuingCertificateTime(String issuingCertificateTime) {
      this.issuingCertificateTime = issuingCertificateTime;
    }
    /**
    *   获取 发证日期
    **/
    public String getIssuingCertificateTime() {
      return issuingCertificateTime;
    }
    /**
    *   设置 资质类别
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 资质类别
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 资质证书号
    **/
    public void setCertificateNum(String certificateNum) {
      this.certificateNum = certificateNum;
    }
    /**
    *   获取 资质证书号
    **/
    public String getCertificateNum() {
      return certificateNum;
    }
    /**
    *   设置 资质名称
    **/
    public void setQualificationName(String qualificationName) {
      this.qualificationName = qualificationName;
    }
    /**
    *   获取 资质名称
    **/
    public String getQualificationName() {
      return qualificationName;
    }



}

