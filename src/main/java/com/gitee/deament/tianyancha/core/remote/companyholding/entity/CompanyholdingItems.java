package com.gitee.deament.tianyancha.core.remote.companyholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*实际控制权
* 属于Companyholding
* /services/v4/open/companyholding
*@author deament
**/

public class CompanyholdingItems implements Serializable{

    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    控制人path
    **/
    @JSONField(name="chainList")
    private List<List<CompanyholdingChainList>> chainList;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblishTime")
    private Long estiblishTime;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    企业名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公司id
    **/
    @JSONField(name="start")
    private String start;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    占比
    **/
    @JSONField(name="percent")
    private String percent;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private String cid;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;


    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 控制人path
    **/
    public void setChainList(List<List<CompanyholdingChainList>> chainList) {
      this.chainList = chainList;
    }
    /**
    *   获取 控制人path
    **/
    public List<List<CompanyholdingChainList>> getChainList() {
      return chainList;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblishTime(Long estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立日期
    **/
    public Long getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 企业名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setStart(String start) {
      this.start = start;
    }
    /**
    *   获取 公司id
    **/
    public String getStart() {
      return start;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 占比
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 占比
    **/
    public String getPercent() {
      return percent;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(String cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public String getCid() {
      return cid;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }



}

