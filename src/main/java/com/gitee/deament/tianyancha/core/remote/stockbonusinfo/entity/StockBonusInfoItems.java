package com.gitee.deament.tianyancha.core.remote.stockbonusinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*分红情况
* 属于StockBonusInfo
* /services/open/stock/bonusInfo/2.0
*@author deament
**/

public class StockBonusInfoItems implements Serializable{

    /**
     *    实施日期
    **/
    @JSONField(name="implementationDate")
    private String implementationDate;
    /**
     *    方案进度
    **/
    @JSONField(name="progress")
    private String progress;
    /**
     *    股利支付率(%)
    **/
    @JSONField(name="payment")
    private String payment;
    /**
     *    A股股权登记日
    **/
    @JSONField(name="asharesDate")
    private String asharesDate;
    /**
     *    分红率(%)
    **/
    @JSONField(name="dividendRate")
    private String dividendRate;
    /**
     *    A股除权除息日
    **/
    @JSONField(name="acuxiDate")
    private String acuxiDate;
    /**
     *    分红方案说明
    **/
    @JSONField(name="introduction")
    private String introduction;
    /**
     *    股东大会日期
    **/
    @JSONField(name="shareholderDate")
    private String shareholderDate;
    /**
     *    董事会日期
    **/
    @JSONField(name="boardDate")
    private String boardDate;


    /**
    *   设置 实施日期
    **/
    public void setImplementationDate(String implementationDate) {
      this.implementationDate = implementationDate;
    }
    /**
    *   获取 实施日期
    **/
    public String getImplementationDate() {
      return implementationDate;
    }
    /**
    *   设置 方案进度
    **/
    public void setProgress(String progress) {
      this.progress = progress;
    }
    /**
    *   获取 方案进度
    **/
    public String getProgress() {
      return progress;
    }
    /**
    *   设置 股利支付率(%)
    **/
    public void setPayment(String payment) {
      this.payment = payment;
    }
    /**
    *   获取 股利支付率(%)
    **/
    public String getPayment() {
      return payment;
    }
    /**
    *   设置 A股股权登记日
    **/
    public void setAsharesDate(String asharesDate) {
      this.asharesDate = asharesDate;
    }
    /**
    *   获取 A股股权登记日
    **/
    public String getAsharesDate() {
      return asharesDate;
    }
    /**
    *   设置 分红率(%)
    **/
    public void setDividendRate(String dividendRate) {
      this.dividendRate = dividendRate;
    }
    /**
    *   获取 分红率(%)
    **/
    public String getDividendRate() {
      return dividendRate;
    }
    /**
    *   设置 A股除权除息日
    **/
    public void setAcuxiDate(String acuxiDate) {
      this.acuxiDate = acuxiDate;
    }
    /**
    *   获取 A股除权除息日
    **/
    public String getAcuxiDate() {
      return acuxiDate;
    }
    /**
    *   设置 分红方案说明
    **/
    public void setIntroduction(String introduction) {
      this.introduction = introduction;
    }
    /**
    *   获取 分红方案说明
    **/
    public String getIntroduction() {
      return introduction;
    }
    /**
    *   设置 股东大会日期
    **/
    public void setShareholderDate(String shareholderDate) {
      this.shareholderDate = shareholderDate;
    }
    /**
    *   获取 股东大会日期
    **/
    public String getShareholderDate() {
      return shareholderDate;
    }
    /**
    *   设置 董事会日期
    **/
    public void setBoardDate(String boardDate) {
      this.boardDate = boardDate;
    }
    /**
    *   获取 董事会日期
    **/
    public String getBoardDate() {
      return boardDate;
    }



}

