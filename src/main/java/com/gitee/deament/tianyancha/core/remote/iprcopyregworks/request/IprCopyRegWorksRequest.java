package com.gitee.deament.tianyancha.core.remote.iprcopyregworks.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprcopyregworks.entity.IprCopyRegWorks;
import com.gitee.deament.tianyancha.core.remote.iprcopyregworks.dto.IprCopyRegWorksDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*作品著作权
* 可以通过公司名称或ID获取作品著作权的有关信息，包括作品名称、登记号、登记类别等字段的详细信息
* /services/open/ipr/copyRegWorks/2.0
*@author deament
**/

public interface IprCopyRegWorksRequest extends BaseRequest<IprCopyRegWorksDTO ,IprCopyRegWorks>{

}

