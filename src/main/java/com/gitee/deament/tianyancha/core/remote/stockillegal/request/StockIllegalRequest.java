package com.gitee.deament.tianyancha.core.remote.stockillegal.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockillegal.entity.StockIllegal;
import com.gitee.deament.tianyancha.core.remote.stockillegal.dto.StockIllegalDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*违规处理
* 可以通过公司名称或ID获取上市公司违规处理信息，违规处理信息包括公告日期、处罚对象、处罚类型、处罚金额、处理人等
* /services/open/stock/illegal/2.0
*@author deament
**/

public interface StockIllegalRequest extends BaseRequest<StockIllegalDTO ,StockIllegal>{

}

