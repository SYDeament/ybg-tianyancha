package com.gitee.deament.tianyancha.core.remote.pforginfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pforginfo.entity.PfOrgInfo;
import com.gitee.deament.tianyancha.core.remote.pforginfo.dto.PfOrgInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*机构信息
* 可以通过公司名称或ID获取相关私募基金信息，包括私募基金管理人名称、法定代表人/执行事务合伙人、机构类型、登记编号、成立日期等字段的相关信息
* /services/open/pf/orgInfo/2.0
*@author deament
**/
@Component("pfOrgInfoRequestImpl")
public class PfOrgInfoRequestImpl extends BaseRequestImpl<PfOrgInfo,PfOrgInfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/pf/orgInfo/2.0";
    }
}

