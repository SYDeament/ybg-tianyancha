package com.gitee.deament.tianyancha.core.remote.allcompanys.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.allcompanys.entity.AllCompanys;
import com.gitee.deament.tianyancha.core.remote.allcompanys.dto.AllCompanysDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*人员所有公司
* 可以通过公司名称或ID和人名获取企业人员的所有相关公司，包括其担任法人、股东、董监高的公司信息
* /services/v4/open/allCompanys
*@author deament
**/
@Component("allCompanysRequestImpl")
public class AllCompanysRequestImpl extends BaseRequestImpl<AllCompanys,AllCompanysDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/allCompanys";
    }
}

