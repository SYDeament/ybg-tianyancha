package com.gitee.deament.tianyancha.core.remote.iprcopyregworks.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprcopyregworks.entity.IprCopyRegWorksItems;
/**
*作品著作权
* 可以通过公司名称或ID获取作品著作权的有关信息，包括作品名称、登记号、登记类别等字段的详细信息
* /services/open/ipr/copyRegWorks/2.0
*@author deament
**/

public class IprCopyRegWorks implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IprCopyRegWorksItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IprCopyRegWorksItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IprCopyRegWorksItems> getItems() {
      return items;
    }



}

