package com.gitee.deament.tianyancha.core.remote.mbond.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mbond.entity.MBond;
import com.gitee.deament.tianyancha.core.remote.mbond.dto.MBondDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*债券信息
* 可以通过公司名称或ID获企业发行债券信息，债券信息包括发行日期、债券名称、债券代码、债券类型等字段的详细信息
* /services/open/m/bond/2.0
*@author deament
**/
@Component("mBondRequestImpl")
public class MBondRequestImpl extends BaseRequestImpl<MBond,MBondDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/m/bond/2.0";
    }
}

