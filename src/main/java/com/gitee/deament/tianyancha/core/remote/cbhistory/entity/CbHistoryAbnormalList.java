package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryAbnormalList implements Serializable{

    /**
     *    移除时间
    **/
    @JSONField(name="removeDate")
    private String removeDate;
    /**
     *    列入原因
    **/
    @JSONField(name="putReason")
    private String putReason;
    /**
     *    决定列入部门
    **/
    @JSONField(name="putDepartment")
    private String putDepartment;
    /**
     *    决定移除部门
    **/
    @JSONField(name="removeDepartment")
    private String removeDepartment;
    /**
     *    移除原因
    **/
    @JSONField(name="removeReason")
    private String removeReason;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    列入时间
    **/
    @JSONField(name="putDate")
    private String putDate;


    /**
    *   设置 移除时间
    **/
    public void setRemoveDate(String removeDate) {
      this.removeDate = removeDate;
    }
    /**
    *   获取 移除时间
    **/
    public String getRemoveDate() {
      return removeDate;
    }
    /**
    *   设置 列入原因
    **/
    public void setPutReason(String putReason) {
      this.putReason = putReason;
    }
    /**
    *   获取 列入原因
    **/
    public String getPutReason() {
      return putReason;
    }
    /**
    *   设置 决定列入部门
    **/
    public void setPutDepartment(String putDepartment) {
      this.putDepartment = putDepartment;
    }
    /**
    *   获取 决定列入部门
    **/
    public String getPutDepartment() {
      return putDepartment;
    }
    /**
    *   设置 决定移除部门
    **/
    public void setRemoveDepartment(String removeDepartment) {
      this.removeDepartment = removeDepartment;
    }
    /**
    *   获取 决定移除部门
    **/
    public String getRemoveDepartment() {
      return removeDepartment;
    }
    /**
    *   设置 移除原因
    **/
    public void setRemoveReason(String removeReason) {
      this.removeReason = removeReason;
    }
    /**
    *   获取 移除原因
    **/
    public String getRemoveReason() {
      return removeReason;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 列入时间
    **/
    public void setPutDate(String putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 列入时间
    **/
    public String getPutDate() {
      return putDate;
    }



}

