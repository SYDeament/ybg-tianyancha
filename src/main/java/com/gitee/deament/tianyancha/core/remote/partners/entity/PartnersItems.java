package com.gitee.deament.tianyancha.core.remote.partners.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*人员所有合作伙伴
* 属于Partners
* /services/v4/open/partners
*@author deament
**/

public class PartnersItems implements Serializable{

    /**
     *    公司数量
    **/
    @JSONField(name="companyNum")
    private Integer companyNum;
    /**
     *    人id
    **/
    @JSONField(name="hid")
    private Integer hid;
    /**
     *    合作伙伴
    **/
    @JSONField(name="partners")
    private List<PartnersPartners> partners;
    /**
     *    姓名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    logo
    **/
    @JSONField(name="headUrl")
    private String headUrl;
    /**
     *    公司列表
    **/
    @JSONField(name="office")
    private List<PartnersOffice> office;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;
    /**
     *    职位
    **/
    @JSONField(name="typeJoin")
    private String typeJoin;


    /**
    *   设置 公司数量
    **/
    public void setCompanyNum(Integer companyNum) {
      this.companyNum = companyNum;
    }
    /**
    *   获取 公司数量
    **/
    public Integer getCompanyNum() {
      return companyNum;
    }
    /**
    *   设置 人id
    **/
    public void setHid(Integer hid) {
      this.hid = hid;
    }
    /**
    *   获取 人id
    **/
    public Integer getHid() {
      return hid;
    }
    /**
    *   设置 合作伙伴
    **/
    public void setPartners(List<PartnersPartners> partners) {
      this.partners = partners;
    }
    /**
    *   获取 合作伙伴
    **/
    public List<PartnersPartners> getPartners() {
      return partners;
    }
    /**
    *   设置 姓名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 姓名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 logo
    **/
    public void setHeadUrl(String headUrl) {
      this.headUrl = headUrl;
    }
    /**
    *   获取 logo
    **/
    public String getHeadUrl() {
      return headUrl;
    }
    /**
    *   设置 公司列表
    **/
    public void setOffice(List<PartnersOffice> office) {
      this.office = office;
    }
    /**
    *   获取 公司列表
    **/
    public List<PartnersOffice> getOffice() {
      return office;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }
    /**
    *   设置 职位
    **/
    public void setTypeJoin(String typeJoin) {
      this.typeJoin = typeJoin;
    }
    /**
    *   获取 职位
    **/
    public String getTypeJoin() {
      return typeJoin;
    }



}

