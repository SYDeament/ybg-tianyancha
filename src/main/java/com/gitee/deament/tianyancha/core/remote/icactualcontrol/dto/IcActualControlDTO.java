package com.gitee.deament.tianyancha.core.remote.icactualcontrol.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*疑似实际控制人
* 可以通过公司名称或ID获取股权向上穿透后识别的企业疑似实际控制人，包含（一名）疑似控制人姓名、层级关系等字段信息
* /services/open/ic/actualControl/2.0
*@author deament
**/

@TYCURL(value="/services/open/ic/actualControl/2.0")
public class IcActualControlDTO implements Serializable{

    /**
     *    企业名称
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    企业id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 企业名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 企业id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

