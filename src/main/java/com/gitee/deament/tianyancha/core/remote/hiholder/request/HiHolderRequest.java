package com.gitee.deament.tianyancha.core.remote.hiholder.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiholder.entity.HiHolder;
import com.gitee.deament.tianyancha.core.remote.hiholder.dto.HiHolderDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*历史股东信息
* 可以通过公司名称或ID获取企业历史的股东信息，历史股东信息包括股东名、出资比例、认缴金额、股东总数等字段信息
* /services/open/hi/holder/2.0
*@author deament
**/

public interface HiHolderRequest extends BaseRequest<HiHolderDTO ,HiHolder>{

}

