package com.gitee.deament.tianyancha.core.remote.oibusinessevent.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oibusinessevent.entity.OiBusinessEventItems;
/**
*工商追踪
* 可以通过投资机构名称获取被投资企业名称、logo、投资比例等字段的详细信息
* /services/open/oi/businessEvent/2.0
*@author deament
**/

public class OiBusinessEvent implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiBusinessEventItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiBusinessEventItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiBusinessEventItems> getItems() {
      return items;
    }



}

