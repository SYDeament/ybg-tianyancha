package com.gitee.deament.tianyancha.core.remote.iprpatents.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprpatents.entity.IprPatents;
import com.gitee.deament.tianyancha.core.remote.iprpatents.dto.IprPatentsDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*专利信息
* 可以通过公司名称或ID获取专利的有关信息，包括专利名称、申请号、申请公布号等字段的详细信息
* /services/open/ipr/patents/2.0
*@author deament
**/
@Component("iprPatentsRequestImpl")
public class IprPatentsRequestImpl extends BaseRequestImpl<IprPatents,IprPatentsDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ipr/patents/2.0";
    }
}

