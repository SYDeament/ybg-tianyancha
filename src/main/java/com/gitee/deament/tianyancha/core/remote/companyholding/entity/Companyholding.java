package com.gitee.deament.tianyancha.core.remote.companyholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.companyholding.entity.CompanyholdingChainList;
import com.gitee.deament.tianyancha.core.remote.companyholding.entity.CompanyholdingItems;
/**
*实际控制权
* 可以通过公司名称或ID获取股权向下穿透后识别的当前企业实际控制的企业，包括对应公司名称或ID、出资比例、投资链等
* /services/v4/open/companyholding
*@author deament
**/

public class Companyholding implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CompanyholdingItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CompanyholdingItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CompanyholdingItems> getItems() {
      return items;
    }



}

