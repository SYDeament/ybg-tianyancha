package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity.XgbaseinfoV2Office;
import com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity.XgbaseinfoV2LegalInfo;
/**
*特殊企业基本信息
* 可以通过公司名称或ID获取特殊企业基本信息，包含香港公司、社会组织、律所、事业单位、基金会这些特殊企业，不同社会团体所呈现的信息维度不同
* /services/v4/open/xgbaseinfoV2
*@author deament
**/

public class XgbaseinfoV21 implements Serializable{

    /**
     *    曾用名
    **/
    @JSONField(name="historyNames")
    private String historyNames;
    /**
     *    企业状态 
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    企业邮箱列表
    **/
    @JSONField(name="emailList")
    private List<String> emailList;
    /**
     *    企业联系方式列表
    **/
    @JSONField(name="phoneList")
    private List<String> phoneList;
    /**
     *    股票号
    **/
    @JSONField(name="bondNum")
    private String bondNum;
    /**
     *    无意义
    **/
    @JSONField(name="baiduAuthURLWWW")
    private String baiduAuthURLWWW;
    /**
     *    法人类型，1 人 2 公司
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    股票名称
    **/
    @JSONField(name="bondName")
    private String bondName;
    /**
     *    股权结构图
    **/
    @JSONField(name="equityUrl")
    private String equityUrl;
    /**
     *    弃用
    **/
    @JSONField(name="property4")
    private String property4;
    /**
     *    英文名
    **/
    @JSONField(name="property3")
    private String property3;
    /**
     *    股票曾用名
    **/
    @JSONField(name="usedBondName")
    private String usedBondName;
    /**
     *    核准时间 
    **/
    @JSONField(name="approvedTime")
    private Long approvedTime;
    /**
     *    logo（不建议使用）
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    组织机构代码 
    **/
    @JSONField(name="orgNumber")
    private String orgNumber;
    /**
     *    弃用
    **/
    @JSONField(name="isClaimed")
    private Integer isClaimed;
    /**
     *    数据来源
    **/
    @JSONField(name="sourceFlag")
    private String sourceFlag;
    /**
     *    新公司名id
    **/
    @JSONField(name="correctCompanyId")
    private String correctCompanyId;
    /**
     *    1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会，7-不存在法人、注册资本、统一社会信用代码、经营状态
    **/
    @JSONField(name="entityType")
    private Integer entityType;
    /**
     *    企业描述
    **/
    @JSONField(name="portray")
    private String portray;
    /**
     *    是否有年报
    **/
    @JSONField(name="haveReport")
    private Boolean haveReport;
    /**
     *    经营范围 
    **/
    @JSONField(name="businessScope")
    private String businessScope;
    /**
     *    纳税人识别号 
    **/
    @JSONField(name="taxNumber")
    private String taxNumber;
    /**
     *    企业标签
    **/
    @JSONField(name="tags")
    private String tags;
    /**
     *    对应表id
    **/
    @JSONField(name="companyId")
    private Integer companyId;
    /**
     *    企业联系方式
    **/
    @JSONField(name="phoneNumber")
    private String phoneNumber;
    /**
     *    行业评分
    **/
    @JSONField(name="categoryScore")
    private Number  categoryScore;
    /**
     *    是否高新企业
    **/
    @JSONField(name="isHightTech")
    private String isHightTech;
    /**
     *    公司名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    企业评分(万分制)
    **/
    @JSONField(name="percentileScore")
    private Integer percentileScore;
    /**
     *    企业介绍
    **/
    @JSONField(name="baseInfo")
    private String baseInfo;
    /**
     *    0-显示 1-不显示(弃用)
    **/
    @JSONField(name="flag")
    private Integer flag;
    /**
     *    注册资金
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    行业 
    **/
    @JSONField(name="industry")
    private String industry;
    /**
     *    更新时间
    **/
    @JSONField(name="updateTimes")
    private Long updateTimes;
    /**
     *    无用
    **/
    @JSONField(name="pricingPackage")
    private Integer pricingPackage;
    /**
     *    法人 
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    注册号
    **/
    @JSONField(name="regNumber")
    private String regNumber;
    /**
     *    统一社会信用代码 
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    微博(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="weibo")
    private String weibo;
    /**
     *    经营开始时间
    **/
    @JSONField(name="fromTime")
    private Number  fromTime;
    /**
     *    企业类型
    **/
    @JSONField(name="companyOrgType")
    private String companyOrgType;
    /**
     *    无意义
    **/
    @JSONField(name="baiduAuthURLWAP")
    private String baiduAuthURLWAP;
    /**
     *    经营结束时间  
    **/
    @JSONField(name="toTime")
    private Long toTime;
    /**
     *    邮箱 
    **/
    @JSONField(name="email")
    private String email;
    /**
     *    实收注册资本（工商已不公示）
    **/
    @JSONField(name="actualCapital")
    private String actualCapital;
    /**
     *    成立日期 
    **/
    @JSONField(name="estiblishTime")
    private Long estiblishTime;
    /**
     *    登记机关
    **/
    @JSONField(name="regInstitute")
    private String regInstitute;
    /**
     *    无用
    **/
    @JSONField(name="companyType")
    private Integer companyType;
    /**
     *    注册地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    无用
    **/
    @JSONField(name="property5")
    private String property5;
    /**
     *    网址
    **/
    @JSONField(name="websiteList")
    private String websiteList;
    /**
     *    法人id
    **/
    @JSONField(name="legalPersonId")
    private Integer legalPersonId;
    /**
     *    法人信息
    **/
    @JSONField(name="legalInfo")
    private List<XgbaseinfoV2LegalInfo> legalInfo;
    /**
     *    地址
    **/
    @JSONField(name="rladdress")
    private String rladdress;
    /**
     *    股票类型
    **/
    @JSONField(name="bondType")
    private String bondType;
    /**
     *    更新时间
    **/
    @JSONField(name="updatetime")
    private Long updatetime;
    /**
     *    省份
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 曾用名
    **/
    public void setHistoryNames(String historyNames) {
      this.historyNames = historyNames;
    }
    /**
    *   获取 曾用名
    **/
    public String getHistoryNames() {
      return historyNames;
    }
    /**
    *   设置 企业状态 
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 企业状态 
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 企业邮箱列表
    **/
    public void setEmailList(List<String> emailList) {
      this.emailList = emailList;
    }
    /**
    *   获取 企业邮箱列表
    **/
    public List<String> getEmailList() {
      return emailList;
    }
    /**
    *   设置 企业联系方式列表
    **/
    public void setPhoneList(List<String> phoneList) {
      this.phoneList = phoneList;
    }
    /**
    *   获取 企业联系方式列表
    **/
    public List<String> getPhoneList() {
      return phoneList;
    }
    /**
    *   设置 股票号
    **/
    public void setBondNum(String bondNum) {
      this.bondNum = bondNum;
    }
    /**
    *   获取 股票号
    **/
    public String getBondNum() {
      return bondNum;
    }
    /**
    *   设置 无意义
    **/
    public void setBaiduAuthURLWWW(String baiduAuthURLWWW) {
      this.baiduAuthURLWWW = baiduAuthURLWWW;
    }
    /**
    *   获取 无意义
    **/
    public String getBaiduAuthURLWWW() {
      return baiduAuthURLWWW;
    }
    /**
    *   设置 法人类型，1 人 2 公司
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 法人类型，1 人 2 公司
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 股票名称
    **/
    public void setBondName(String bondName) {
      this.bondName = bondName;
    }
    /**
    *   获取 股票名称
    **/
    public String getBondName() {
      return bondName;
    }
    /**
    *   设置 股权结构图
    **/
    public void setEquityUrl(String equityUrl) {
      this.equityUrl = equityUrl;
    }
    /**
    *   获取 股权结构图
    **/
    public String getEquityUrl() {
      return equityUrl;
    }
    /**
    *   设置 弃用
    **/
    public void setProperty4(String property4) {
      this.property4 = property4;
    }
    /**
    *   获取 弃用
    **/
    public String getProperty4() {
      return property4;
    }
    /**
    *   设置 英文名
    **/
    public void setProperty3(String property3) {
      this.property3 = property3;
    }
    /**
    *   获取 英文名
    **/
    public String getProperty3() {
      return property3;
    }
    /**
    *   设置 股票曾用名
    **/
    public void setUsedBondName(String usedBondName) {
      this.usedBondName = usedBondName;
    }
    /**
    *   获取 股票曾用名
    **/
    public String getUsedBondName() {
      return usedBondName;
    }
    /**
    *   设置 核准时间 
    **/
    public void setApprovedTime(Long approvedTime) {
      this.approvedTime = approvedTime;
    }
    /**
    *   获取 核准时间 
    **/
    public Long getApprovedTime() {
      return approvedTime;
    }
    /**
    *   设置 logo（不建议使用）
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo（不建议使用）
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 组织机构代码 
    **/
    public void setOrgNumber(String orgNumber) {
      this.orgNumber = orgNumber;
    }
    /**
    *   获取 组织机构代码 
    **/
    public String getOrgNumber() {
      return orgNumber;
    }
    /**
    *   设置 弃用
    **/
    public void setIsClaimed(Integer isClaimed) {
      this.isClaimed = isClaimed;
    }
    /**
    *   获取 弃用
    **/
    public Integer getIsClaimed() {
      return isClaimed;
    }
    /**
    *   设置 数据来源
    **/
    public void setSourceFlag(String sourceFlag) {
      this.sourceFlag = sourceFlag;
    }
    /**
    *   获取 数据来源
    **/
    public String getSourceFlag() {
      return sourceFlag;
    }
    /**
    *   设置 新公司名id
    **/
    public void setCorrectCompanyId(String correctCompanyId) {
      this.correctCompanyId = correctCompanyId;
    }
    /**
    *   获取 新公司名id
    **/
    public String getCorrectCompanyId() {
      return correctCompanyId;
    }
    /**
    *   设置 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会，7-不存在法人、注册资本、统一社会信用代码、经营状态
    **/
    public void setEntityType(Integer entityType) {
      this.entityType = entityType;
    }
    /**
    *   获取 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会，7-不存在法人、注册资本、统一社会信用代码、经营状态
    **/
    public Integer getEntityType() {
      return entityType;
    }
    /**
    *   设置 企业描述
    **/
    public void setPortray(String portray) {
      this.portray = portray;
    }
    /**
    *   获取 企业描述
    **/
    public String getPortray() {
      return portray;
    }
    /**
    *   设置 是否有年报
    **/
    public void setHaveReport(Boolean haveReport) {
      this.haveReport = haveReport;
    }
    /**
    *   获取 是否有年报
    **/
    public Boolean getHaveReport() {
      return haveReport;
    }
    /**
    *   设置 经营范围 
    **/
    public void setBusinessScope(String businessScope) {
      this.businessScope = businessScope;
    }
    /**
    *   获取 经营范围 
    **/
    public String getBusinessScope() {
      return businessScope;
    }
    /**
    *   设置 纳税人识别号 
    **/
    public void setTaxNumber(String taxNumber) {
      this.taxNumber = taxNumber;
    }
    /**
    *   获取 纳税人识别号 
    **/
    public String getTaxNumber() {
      return taxNumber;
    }
    /**
    *   设置 企业标签
    **/
    public void setTags(String tags) {
      this.tags = tags;
    }
    /**
    *   获取 企业标签
    **/
    public String getTags() {
      return tags;
    }
    /**
    *   设置 对应表id
    **/
    public void setCompanyId(Integer companyId) {
      this.companyId = companyId;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getCompanyId() {
      return companyId;
    }
    /**
    *   设置 企业联系方式
    **/
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    /**
    *   获取 企业联系方式
    **/
    public String getPhoneNumber() {
      return phoneNumber;
    }
    /**
    *   设置 行业评分
    **/
    public void setCategoryScore(Number  categoryScore) {
      this.categoryScore = categoryScore;
    }
    /**
    *   获取 行业评分
    **/
    public Number  getCategoryScore() {
      return categoryScore;
    }
    /**
    *   设置 是否高新企业
    **/
    public void setIsHightTech(String isHightTech) {
      this.isHightTech = isHightTech;
    }
    /**
    *   获取 是否高新企业
    **/
    public String getIsHightTech() {
      return isHightTech;
    }
    /**
    *   设置 公司名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 企业评分(万分制)
    **/
    public void setPercentileScore(Integer percentileScore) {
      this.percentileScore = percentileScore;
    }
    /**
    *   获取 企业评分(万分制)
    **/
    public Integer getPercentileScore() {
      return percentileScore;
    }
    /**
    *   设置 企业介绍
    **/
    public void setBaseInfo(String baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 企业介绍
    **/
    public String getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 0-显示 1-不显示(弃用)
    **/
    public void setFlag(Integer flag) {
      this.flag = flag;
    }
    /**
    *   获取 0-显示 1-不显示(弃用)
    **/
    public Integer getFlag() {
      return flag;
    }
    /**
    *   设置 注册资金
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资金
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 行业 
    **/
    public void setIndustry(String industry) {
      this.industry = industry;
    }
    /**
    *   获取 行业 
    **/
    public String getIndustry() {
      return industry;
    }
    /**
    *   设置 更新时间
    **/
    public void setUpdateTimes(Long updateTimes) {
      this.updateTimes = updateTimes;
    }
    /**
    *   获取 更新时间
    **/
    public Long getUpdateTimes() {
      return updateTimes;
    }
    /**
    *   设置 无用
    **/
    public void setPricingPackage(Integer pricingPackage) {
      this.pricingPackage = pricingPackage;
    }
    /**
    *   获取 无用
    **/
    public Integer getPricingPackage() {
      return pricingPackage;
    }
    /**
    *   设置 法人 
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人 
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 注册号
    **/
    public void setRegNumber(String regNumber) {
      this.regNumber = regNumber;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNumber() {
      return regNumber;
    }
    /**
    *   设置 统一社会信用代码 
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码 
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 微博(官方未定义字段类型 暂定为String)
    **/
    public void setWeibo(String weibo) {
      this.weibo = weibo;
    }
    /**
    *   获取 微博(官方未定义字段类型 暂定为String)
    **/
    public String getWeibo() {
      return weibo;
    }
    /**
    *   设置 经营开始时间
    **/
    public void setFromTime(Number  fromTime) {
      this.fromTime = fromTime;
    }
    /**
    *   获取 经营开始时间
    **/
    public Number  getFromTime() {
      return fromTime;
    }
    /**
    *   设置 企业类型
    **/
    public void setCompanyOrgType(String companyOrgType) {
      this.companyOrgType = companyOrgType;
    }
    /**
    *   获取 企业类型
    **/
    public String getCompanyOrgType() {
      return companyOrgType;
    }
    /**
    *   设置 无意义
    **/
    public void setBaiduAuthURLWAP(String baiduAuthURLWAP) {
      this.baiduAuthURLWAP = baiduAuthURLWAP;
    }
    /**
    *   获取 无意义
    **/
    public String getBaiduAuthURLWAP() {
      return baiduAuthURLWAP;
    }
    /**
    *   设置 经营结束时间  
    **/
    public void setToTime(Long toTime) {
      this.toTime = toTime;
    }
    /**
    *   获取 经营结束时间  
    **/
    public Long getToTime() {
      return toTime;
    }
    /**
    *   设置 邮箱 
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱 
    **/
    public String getEmail() {
      return email;
    }
    /**
    *   设置 实收注册资本（工商已不公示）
    **/
    public void setActualCapital(String actualCapital) {
      this.actualCapital = actualCapital;
    }
    /**
    *   获取 实收注册资本（工商已不公示）
    **/
    public String getActualCapital() {
      return actualCapital;
    }
    /**
    *   设置 成立日期 
    **/
    public void setEstiblishTime(Long estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立日期 
    **/
    public Long getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 登记机关
    **/
    public void setRegInstitute(String regInstitute) {
      this.regInstitute = regInstitute;
    }
    /**
    *   获取 登记机关
    **/
    public String getRegInstitute() {
      return regInstitute;
    }
    /**
    *   设置 无用
    **/
    public void setCompanyType(Integer companyType) {
      this.companyType = companyType;
    }
    /**
    *   获取 无用
    **/
    public Integer getCompanyType() {
      return companyType;
    }
    /**
    *   设置 注册地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 注册地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 无用
    **/
    public void setProperty5(String property5) {
      this.property5 = property5;
    }
    /**
    *   获取 无用
    **/
    public String getProperty5() {
      return property5;
    }
    /**
    *   设置 网址
    **/
    public void setWebsiteList(String websiteList) {
      this.websiteList = websiteList;
    }
    /**
    *   获取 网址
    **/
    public String getWebsiteList() {
      return websiteList;
    }
    /**
    *   设置 法人id
    **/
    public void setLegalPersonId(Integer legalPersonId) {
      this.legalPersonId = legalPersonId;
    }
    /**
    *   获取 法人id
    **/
    public Integer getLegalPersonId() {
      return legalPersonId;
    }
    /**
    *   设置 法人信息
    **/
    public void setLegalInfo(List<XgbaseinfoV2LegalInfo> legalInfo) {
      this.legalInfo = legalInfo;
    }
    /**
    *   获取 法人信息
    **/
    public List<XgbaseinfoV2LegalInfo> getLegalInfo() {
      return legalInfo;
    }
    /**
    *   设置 地址
    **/
    public void setRladdress(String rladdress) {
      this.rladdress = rladdress;
    }
    /**
    *   获取 地址
    **/
    public String getRladdress() {
      return rladdress;
    }
    /**
    *   设置 股票类型
    **/
    public void setBondType(String bondType) {
      this.bondType = bondType;
    }
    /**
    *   获取 股票类型
    **/
    public String getBondType() {
      return bondType;
    }
    /**
    *   设置 更新时间
    **/
    public void setUpdatetime(Long updatetime) {
      this.updatetime = updatetime;
    }
    /**
    *   获取 更新时间
    **/
    public Long getUpdatetime() {
      return updatetime;
    }
    /**
    *   设置 省份
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份
    **/
    public String getBase() {
      return base;
    }



}

