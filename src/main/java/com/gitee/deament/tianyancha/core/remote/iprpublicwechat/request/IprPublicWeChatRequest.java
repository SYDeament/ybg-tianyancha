package com.gitee.deament.tianyancha.core.remote.iprpublicwechat.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprpublicwechat.entity.IprPublicWeChat;
import com.gitee.deament.tianyancha.core.remote.iprpublicwechat.dto.IprPublicWeChatDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业微信公众号
* 可以通过公司名称或ID获取企业微信公众号的有关信息，包括企业微信公众号名称、微信号、二维码、功能介绍等字段的详细信息
* /services/open/ipr/publicWeChat/2.0
*@author deament
**/

public interface IprPublicWeChatRequest extends BaseRequest<IprPublicWeChatDTO ,IprPublicWeChat>{

}

