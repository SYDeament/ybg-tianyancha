package com.gitee.deament.tianyancha.core.remote.mtaxpayer.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mtaxpayer.entity.MTaxpayerItems;
/**
*一般纳税人
* 可以通过公司名称或ID获取一般纳税人信息，一般纳税人信息包括纳税人名称、纳税人识别号、纳税人资格类型、主管税务机关、有效期起、有效期止等字段的信息
* /services/open/m/taxpayer/2.0
*@author deament
**/

public class MTaxpayer implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MTaxpayerItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MTaxpayerItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MTaxpayerItems> getItems() {
      return items;
    }



}

