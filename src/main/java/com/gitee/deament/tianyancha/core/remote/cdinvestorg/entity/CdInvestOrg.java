package com.gitee.deament.tianyancha.core.remote.cdinvestorg.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdinvestorg.entity.CdInvestOrgItems;
/**
*投资机构
* 可以通过公司名称或ID获企业相关投资机构信息，投资机构信息包括机构名称、成立日期、简介等字段的详细信息
* /services/open/cd/investOrg/2.0
*@author deament
**/

public class CdInvestOrg implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CdInvestOrgItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CdInvestOrgItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CdInvestOrgItems> getItems() {
      return items;
    }



}

