package com.gitee.deament.tianyancha.core.remote.mrowntax.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrowntax.entity.MrOwnTax;
import com.gitee.deament.tianyancha.core.remote.mrowntax.dto.MrOwnTaxDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*欠税公告
* 可以通过公司名称或ID获取企业欠税信息，企业欠税信息包括欠税公告、纳税人识别号、证件号码、经营地点、欠税税种、欠税余额等字段的详细信息
* /services/open/mr/ownTax/2.0
*@author deament
**/
@Component("mrOwnTaxRequestImpl")
public class MrOwnTaxRequestImpl extends BaseRequestImpl<MrOwnTax,MrOwnTaxDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/ownTax/2.0";
    }
}

