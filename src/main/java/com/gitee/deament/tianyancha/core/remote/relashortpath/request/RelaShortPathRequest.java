package com.gitee.deament.tianyancha.core.remote.relashortpath.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.relashortpath.entity.RelaShortPath;
import com.gitee.deament.tianyancha.core.remote.relashortpath.dto.RelaShortPathDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*最短路径发现
* 可以通过公司A，公司B，关系类型（法人/任职/投资/分支/诉讼/竞合/债务，可多选）查询两家公司之间是否存在关联关系
* /services/open/rela/shortPath
*@author deament
**/

public interface RelaShortPathRequest extends BaseRequest<RelaShortPathDTO ,RelaShortPath>{

}

