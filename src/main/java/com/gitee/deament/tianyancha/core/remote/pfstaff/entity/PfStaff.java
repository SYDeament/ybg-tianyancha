package com.gitee.deament.tianyancha.core.remote.pfstaff.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfstaff.entity.PfStaffItems;
/**
*高管情况
* 可以通过公司名称或ID获取相关私募基金高管情况，包括高管姓名、是否有从业资格等字段的相关信息
* /services/open/pf/staff/2.0
*@author deament
**/

public class PfStaff implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<PfStaffItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<PfStaffItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<PfStaffItems> getItems() {
      return items;
    }



}

