package com.gitee.deament.tianyancha.core.remote.bqproject.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-工程项目详情
* 属于BqProject
* /services/open/bq/project/detail/2.0
*@author deament
**/

public class BqProjectContractFiling implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private List<BqProjectResult> result;
    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="pageSize")
    private Integer pageSize;
    /**
     *    
    **/
    @JSONField(name="pageNum")
    private Integer pageNum;


    /**
    *   设置 
    **/
    public void setResult(List<BqProjectResult> result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public List<BqProjectResult> getResult() {
      return result;
    }
    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setPageSize(Integer pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 
    **/
    public Integer getPageSize() {
      return pageSize;
    }
    /**
    *   设置 
    **/
    public void setPageNum(Integer pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 
    **/
    public Integer getPageNum() {
      return pageNum;
    }



}

