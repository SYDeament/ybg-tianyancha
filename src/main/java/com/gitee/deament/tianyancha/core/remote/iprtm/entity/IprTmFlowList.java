package com.gitee.deament.tianyancha.core.remote.iprtm.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*商标信息详情
* 属于IprTm
* /services/open/ipr/tm/detail/2.0
*@author deament
**/

public class IprTmFlowList implements Serializable{

    /**
     *    商标流程文案
    **/
    @JSONField(name="categoryStr")
    private String categoryStr;
    /**
     *    商标流程code
    **/
    @JSONField(name="category")
    private String category;
    /**
     *    商标流程发生日期
    **/
    @JSONField(name="flowDate")
    private Long flowDate;


    /**
    *   设置 商标流程文案
    **/
    public void setCategoryStr(String categoryStr) {
      this.categoryStr = categoryStr;
    }
    /**
    *   获取 商标流程文案
    **/
    public String getCategoryStr() {
      return categoryStr;
    }
    /**
    *   设置 商标流程code
    **/
    public void setCategory(String category) {
      this.category = category;
    }
    /**
    *   获取 商标流程code
    **/
    public String getCategory() {
      return category;
    }
    /**
    *   设置 商标流程发生日期
    **/
    public void setFlowDate(Long flowDate) {
      this.flowDate = flowDate;
    }
    /**
    *   获取 商标流程发生日期
    **/
    public Long getFlowDate() {
      return flowDate;
    }



}

