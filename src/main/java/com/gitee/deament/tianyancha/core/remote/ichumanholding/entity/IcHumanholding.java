package com.gitee.deament.tianyancha.core.remote.ichumanholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.ichumanholding.entity.IcHumanholdingChainList;
import com.gitee.deament.tianyancha.core.remote.ichumanholding.entity.IcHumanholdingItems;
/**
*最终受益人
* 可以通过公司名称或ID获取股权向上穿透后识别的企业最终受益人，包含（一名或多名）股东姓名、持股比例，层级关系等
* /services/open/ic/humanholding/2.0
*@author deament
**/

public class IcHumanholding implements Serializable{

    /**
     *    受益人总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcHumanholdingItems> items;


    /**
    *   设置 受益人总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 受益人总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcHumanholdingItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcHumanholdingItems> getItems() {
      return items;
    }



}

