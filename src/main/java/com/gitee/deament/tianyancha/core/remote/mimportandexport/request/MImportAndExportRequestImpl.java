package com.gitee.deament.tianyancha.core.remote.mimportandexport.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mimportandexport.entity.MImportAndExport;
import com.gitee.deament.tianyancha.core.remote.mimportandexport.dto.MImportAndExportDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*进出口信用
* 可以通过公司名称或ID获取企业进出口信用信息，企业进出口信用信息包括海关注册编码、注册海关、经营类别等字段的详细信息
* /services/open/m/importAndExport/2.0
*@author deament
**/
@Component("mImportAndExportRequestImpl")
public class MImportAndExportRequestImpl extends BaseRequestImpl<MImportAndExport,MImportAndExportDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/m/importAndExport/2.0";
    }
}

