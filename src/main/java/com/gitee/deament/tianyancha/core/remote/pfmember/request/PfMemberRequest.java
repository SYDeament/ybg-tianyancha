package com.gitee.deament.tianyancha.core.remote.pfmember.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfmember.entity.PfMember;
import com.gitee.deament.tianyancha.core.remote.pfmember.dto.PfMemberDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*会员信息
* 可以通过公司名称或ID获取相关私募基金会员信息，包括是否为中国证券投资基金业协会会员、当前会员类型、入会时间等字段的相关信息
* /services/open/pf/member/2.0
*@author deament
**/

public interface PfMemberRequest extends BaseRequest<PfMemberDTO ,PfMember>{

}

