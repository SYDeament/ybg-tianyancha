package com.gitee.deament.tianyancha.core.remote.stockallotmen.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockallotmen.entity.StockAllotmenItems;
/**
*配股情况
* 可以通过公司名称或ID获取上市公司配股情况信息，配股情况信息包括方案进度、配股代码、配股简称、实际配股比例、每套配股价格、实际募集资金净额等
* /services/open/stock/allotmen/2.0
*@author deament
**/

public class StockAllotmen implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<StockAllotmenItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<StockAllotmenItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<StockAllotmenItems> getItems() {
      return items;
    }



}

