package com.gitee.deament.tianyancha.core.remote.oifunds.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oifunds.entity.OiFundsItems;
/**
*管理基金
* 可以通过投资机构名称获取投资机构管理基金信息，包括企业名称、法定代表人、注册资本、成立日期等字段信息
* /services/open/oi/funds/2.0
*@author deament
**/

public class OiFunds implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiFundsItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiFundsItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiFundsItems> getItems() {
      return items;
    }



}

