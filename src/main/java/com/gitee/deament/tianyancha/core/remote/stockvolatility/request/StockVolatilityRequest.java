package com.gitee.deament.tianyancha.core.remote.stockvolatility.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockvolatility.entity.StockVolatility;
import com.gitee.deament.tianyancha.core.remote.stockvolatility.dto.StockVolatilityDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*股票行情
* 可以通过公司名称或ID获取上市公司股票行情数据，上市公司股票行情数据包括股票名、股票号、总市值、流通市值等
* /services/open/stock/volatility/2.0
*@author deament
**/

public interface StockVolatilityRequest extends BaseRequest<StockVolatilityDTO ,StockVolatility>{

}

