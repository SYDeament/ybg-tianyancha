package com.gitee.deament.tianyancha.core.remote.pflegalopinion.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pflegalopinion.entity.PfLegalOpinion;
import com.gitee.deament.tianyancha.core.remote.pflegalopinion.dto.PfLegalOpinionDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*法律意见书信息
* 可以通过公司名称或ID获取相关私募基金法律意见书信息，包括法律意见书状态等
* /services/open/pf/legalOpinion/2.0
*@author deament
**/

public interface PfLegalOpinionRequest extends BaseRequest<PfLegalOpinionDTO ,PfLegalOpinion>{

}

