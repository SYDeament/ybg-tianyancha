package com.gitee.deament.tianyancha.core.remote.mcreditrating.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业信用评级
* 属于MCreditRating
* /services/open/m/creditRating/2.0
*@author deament
**/

public class MCreditRatingItems implements Serializable{

    /**
     *    评级展望
    **/
    @JSONField(name="ratingOutlook")
    private String ratingOutlook;
    /**
     *    评级时间
    **/
    @JSONField(name="ratingDate")
    private String ratingDate;
    /**
     *    评级公司id
    **/
    @JSONField(name="gid")
    private Integer gid;
    /**
     *    评级公司
    **/
    @JSONField(name="ratingCompanyName")
    private String ratingCompanyName;
    /**
     *    债券信用等级
    **/
    @JSONField(name="bondCreditLevel")
    private String bondCreditLevel;
    /**
     *    评级公司的logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    评级公司简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    主体等级
    **/
    @JSONField(name="subjectLevel")
    private String subjectLevel;


    /**
    *   设置 评级展望
    **/
    public void setRatingOutlook(String ratingOutlook) {
      this.ratingOutlook = ratingOutlook;
    }
    /**
    *   获取 评级展望
    **/
    public String getRatingOutlook() {
      return ratingOutlook;
    }
    /**
    *   设置 评级时间
    **/
    public void setRatingDate(String ratingDate) {
      this.ratingDate = ratingDate;
    }
    /**
    *   获取 评级时间
    **/
    public String getRatingDate() {
      return ratingDate;
    }
    /**
    *   设置 评级公司id
    **/
    public void setGid(Integer gid) {
      this.gid = gid;
    }
    /**
    *   获取 评级公司id
    **/
    public Integer getGid() {
      return gid;
    }
    /**
    *   设置 评级公司
    **/
    public void setRatingCompanyName(String ratingCompanyName) {
      this.ratingCompanyName = ratingCompanyName;
    }
    /**
    *   获取 评级公司
    **/
    public String getRatingCompanyName() {
      return ratingCompanyName;
    }
    /**
    *   设置 债券信用等级
    **/
    public void setBondCreditLevel(String bondCreditLevel) {
      this.bondCreditLevel = bondCreditLevel;
    }
    /**
    *   获取 债券信用等级
    **/
    public String getBondCreditLevel() {
      return bondCreditLevel;
    }
    /**
    *   设置 评级公司的logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 评级公司的logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 评级公司简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 评级公司简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 主体等级
    **/
    public void setSubjectLevel(String subjectLevel) {
      this.subjectLevel = subjectLevel;
    }
    /**
    *   获取 主体等级
    **/
    public String getSubjectLevel() {
      return subjectLevel;
    }



}

