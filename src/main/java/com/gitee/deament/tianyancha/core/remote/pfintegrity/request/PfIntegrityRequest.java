package com.gitee.deament.tianyancha.core.remote.pfintegrity.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfintegrity.entity.PfIntegrity;
import com.gitee.deament.tianyancha.core.remote.pfintegrity.dto.PfIntegrityDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*诚信信息
* 可以通过公司名称或ID获取相关私募基金诚信信息，包括机构信息最后更新时间、特别提示信息
* /services/open/pf/integrity/2.0
*@author deament
**/

public interface PfIntegrityRequest extends BaseRequest<PfIntegrityDTO ,PfIntegrity>{

}

