package com.gitee.deament.tianyancha.core.remote.bqreghuman.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-注册人员详情
* 属于BqRegHuman
* /services/open/bq/regHuman/detail/2.0
*@author deament
**/

public class BqRegHumanBaseInfo implements Serializable{

    /**
     *    身份证号
    **/
    @JSONField(name="cardNum")
    private String cardNum;
    /**
     *    性别
    **/
    @JSONField(name="sex")
    private String sex;
    /**
     *    证件类型
    **/
    @JSONField(name="cardType")
    private String cardType;
    /**
     *    姓名
    **/
    @JSONField(name="humanName")
    private String humanName;


    /**
    *   设置 身份证号
    **/
    public void setCardNum(String cardNum) {
      this.cardNum = cardNum;
    }
    /**
    *   获取 身份证号
    **/
    public String getCardNum() {
      return cardNum;
    }
    /**
    *   设置 性别
    **/
    public void setSex(String sex) {
      this.sex = sex;
    }
    /**
    *   获取 性别
    **/
    public String getSex() {
      return sex;
    }
    /**
    *   设置 证件类型
    **/
    public void setCardType(String cardType) {
      this.cardType = cardType;
    }
    /**
    *   获取 证件类型
    **/
    public String getCardType() {
      return cardType;
    }
    /**
    *   设置 姓名
    **/
    public void setHumanName(String humanName) {
      this.humanName = humanName;
    }
    /**
    *   获取 姓名
    **/
    public String getHumanName() {
      return humanName;
    }



}

