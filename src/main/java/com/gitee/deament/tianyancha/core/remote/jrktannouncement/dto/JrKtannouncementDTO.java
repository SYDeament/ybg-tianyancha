package com.gitee.deament.tianyancha.core.remote.jrktannouncement.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*开庭公告
* 可以通过公司名称或ID获取企业开庭公告，开庭公告信息包括被告/被上诉人、法院、原告/上诉人、开庭日期、案由、内部ID、案号等字段的详细信息
* /services/open/jr/ktannouncement/2.0
*@author deament
**/

@TYCURL(value="/services/open/jr/ktannouncement/2.0")
public class JrKtannouncementDTO implements Serializable{

    /**
     *    公司名称（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数（默认10条，最大10）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    公司id（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    当前页数（默认第一页）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 公司名称（id与name只需输入其中一个）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（id与name只需输入其中一个）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（默认10条，最大10）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认10条，最大10）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 公司id（id与name只需输入其中一个）
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id（id与name只需输入其中一个）
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 当前页数（默认第一页）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页数（默认第一页）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

