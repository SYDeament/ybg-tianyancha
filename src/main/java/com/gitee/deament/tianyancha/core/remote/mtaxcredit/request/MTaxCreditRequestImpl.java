package com.gitee.deament.tianyancha.core.remote.mtaxcredit.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mtaxcredit.entity.MTaxCredit;
import com.gitee.deament.tianyancha.core.remote.mtaxcredit.dto.MTaxCreditDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*税务评级
* 可以通过公司名称或ID获取企业税务评级信息，企业税务评级信息包括评价年度、纳税人信用级别、类型、纳税人识别号、评价单位等字段的详细信息
* /services/open/m/taxCredit/2.0
*@author deament
**/
@Component("mTaxCreditRequestImpl")
public class MTaxCreditRequestImpl extends BaseRequestImpl<MTaxCredit,MTaxCreditDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/m/taxCredit/2.0";
    }
}

