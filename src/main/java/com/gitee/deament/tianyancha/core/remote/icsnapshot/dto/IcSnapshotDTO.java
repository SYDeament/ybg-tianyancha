package com.gitee.deament.tianyancha.core.remote.icsnapshot.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*工商快照
* 可以通过公司名称或ID获取工商快照信息，工商快照信息包括工商官网信息快照、营业执照信息、股东及出资信息等
* /services/open/ic/snapshot
*@author deament
**/

@TYCURL(value="/services/open/ic/snapshot")
public class IcSnapshotDTO implements Serializable{

    /**
     *    公司名
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id（与公司名必须输入其中之一）
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id（与公司名必须输入其中之一）
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id（与公司名必须输入其中之一）
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

