package com.gitee.deament.tianyancha.core.remote.mtaxpayer.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*一般纳税人
* 可以通过公司名称或ID获取一般纳税人信息，一般纳税人信息包括纳税人名称、纳税人识别号、纳税人资格类型、主管税务机关、有效期起、有效期止等字段的信息
* /services/open/m/taxpayer/2.0
*@author deament
**/

@TYCURL(value="/services/open/m/taxpayer/2.0")
public class MTaxpayerDTO implements Serializable{

    /**
     *    公司名（与公司id必须输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数（默认20，最大20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    当前页（默认1，最大500）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 公司名（与公司id必须输入其中一个）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名（与公司id必须输入其中一个）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（默认20，最大20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20，最大20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 当前页（默认1，最大500）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1，最大500）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

