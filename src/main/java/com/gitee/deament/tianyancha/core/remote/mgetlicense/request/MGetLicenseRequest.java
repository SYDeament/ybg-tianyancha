package com.gitee.deament.tianyancha.core.remote.mgetlicense.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mgetlicense.entity.MGetLicense;
import com.gitee.deament.tianyancha.core.remote.mgetlicense.dto.MGetLicenseDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*行政许可-工商局
* 可以通过公司名称或ID获取企业行政许可信息，企业行政许可信息包括行政许可决定文书号、许可文件名称、许可机关等字段的详细信息
* /services/open/m/getLicense/2.0
*@author deament
**/

public interface MGetLicenseRequest extends BaseRequest<MGetLicenseDTO ,MGetLicense>{

}

