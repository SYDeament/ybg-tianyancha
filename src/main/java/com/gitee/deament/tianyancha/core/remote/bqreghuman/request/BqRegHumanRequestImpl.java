package com.gitee.deament.tianyancha.core.remote.bqreghuman.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.bqreghuman.entity.BqRegHuman;
import com.gitee.deament.tianyancha.core.remote.bqreghuman.dto.BqRegHumanDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*建筑资质-注册人员详情
* 可以通过公司名称或ID获取建筑资质注册人员信息，包括姓名、注册类别、注册号（执业印章号）、注册专业、证书编号等字段的相关信息
* /services/open/bq/regHuman/detail/2.0
*@author deament
**/
@Component("bqRegHumanRequestImpl")
public class BqRegHumanRequestImpl extends BaseRequestImpl<BqRegHuman,BqRegHumanDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/bq/regHuman/detail/2.0";
    }
}

