package com.gitee.deament.tianyancha.core.remote.mannouncementreport.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*公告研报
* 可以通过公司名称或ID获取企业研报和相关公告信息，企业研报和相关公告信息包括发布数量、发布日期、标题等字段的详细信息
* /services/open/m/announcementReport/2.0
*@author deament
**/

@TYCURL(value="/services/open/m/announcementReport/2.0")
public class MAnnouncementReportDTO implements Serializable{

    /**
     *    企业名称
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数（默认20，最大20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    企业id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    0-企业研报 1-相关公告
     *
    **/
    @ParamRequire(require = false)
    private Long type;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    当前页（默认1）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 企业名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（默认20，最大20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20，最大20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 企业id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 0-企业研报 1-相关公告
    **/
    public void setType(Long type) {
      this.type = type;
    }
    /**
    *   获取 0-企业研报 1-相关公告
    **/
    public Long getType() {
      return type;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 当前页（默认1）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

