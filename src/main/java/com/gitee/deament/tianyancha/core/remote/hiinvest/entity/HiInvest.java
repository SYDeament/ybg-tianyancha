package com.gitee.deament.tianyancha.core.remote.hiinvest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiinvest.entity.HiInvestItems;
/**
*历史对外投资
* 历史对外投资
* /services/open/hi/invest/2.0
*@author deament
**/

public class HiInvest implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiInvestItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiInvestItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiInvestItems> getItems() {
      return items;
    }



}

