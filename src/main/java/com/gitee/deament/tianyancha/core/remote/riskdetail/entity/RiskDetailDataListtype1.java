package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype1 implements Serializable{

    /**
     *    移除时间
    **/
    @JSONField(name="removeDate")
    private Long removeDate;
    /**
     *    列入原因
    **/
    @JSONField(name="putReason")
    private String putReason;
    /**
     *    决定列入部门(作出决定机关)
    **/
    @JSONField(name="putDepartment")
    private String putDepartment;
    /**
     *    决定移除部门
    **/
    @JSONField(name="removeDepartment")
    private String removeDepartment;
    /**
     *    移除原因
    **/
    @JSONField(name="removeReason")
    private String removeReason;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    列入日期
    **/
    @JSONField(name="putDate")
    private Long putDate;


    /**
    *   设置 移除时间
    **/
    public void setRemoveDate(Long removeDate) {
      this.removeDate = removeDate;
    }
    /**
    *   获取 移除时间
    **/
    public Long getRemoveDate() {
      return removeDate;
    }
    /**
    *   设置 列入原因
    **/
    public void setPutReason(String putReason) {
      this.putReason = putReason;
    }
    /**
    *   获取 列入原因
    **/
    public String getPutReason() {
      return putReason;
    }
    /**
    *   设置 决定列入部门(作出决定机关)
    **/
    public void setPutDepartment(String putDepartment) {
      this.putDepartment = putDepartment;
    }
    /**
    *   获取 决定列入部门(作出决定机关)
    **/
    public String getPutDepartment() {
      return putDepartment;
    }
    /**
    *   设置 决定移除部门
    **/
    public void setRemoveDepartment(String removeDepartment) {
      this.removeDepartment = removeDepartment;
    }
    /**
    *   获取 决定移除部门
    **/
    public String getRemoveDepartment() {
      return removeDepartment;
    }
    /**
    *   设置 移除原因
    **/
    public void setRemoveReason(String removeReason) {
      this.removeReason = removeReason;
    }
    /**
    *   获取 移除原因
    **/
    public String getRemoveReason() {
      return removeReason;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 列入日期
    **/
    public void setPutDate(Long putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 列入日期
    **/
    public Long getPutDate() {
      return putDate;
    }



}

