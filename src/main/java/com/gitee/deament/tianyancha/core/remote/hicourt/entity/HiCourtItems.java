package com.gitee.deament.tianyancha.core.remote.hicourt.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史法院公告
* 属于HiCourt
* /services/open/hi/court/2.0
*@author deament
**/

public class HiCourtItems implements Serializable{

    /**
     *    公告id
    **/
    @JSONField(name="announce_id")
    private Integer announce_id;
    /**
     *    原告
    **/
    @JSONField(name="party1")
    private String party1;
    /**
     *    公告号
    **/
    @JSONField(name="bltnno")
    private String bltnno;
    /**
     *    当事人
    **/
    @JSONField(name="party2")
    private String party2;
    /**
     *    法官电话
    **/
    @JSONField(name="judgephone")
    private String judgephone;
    /**
     *    唯一标识符
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    公告类型名称
    **/
    @JSONField(name="bltntypename")
    private String bltntypename;
    /**
     *    案件内容
    **/
    @JSONField(name="content")
    private String content;
    /**
     *    法院名
    **/
    @JSONField(name="courtcode")
    private String courtcode;
    /**
     *    省份
    **/
    @JSONField(name="province")
    private String province;
    /**
     *    相关公司列表
    **/
    @JSONField(name="companyList")
    private List<HiCourtCompanyList> companyList;
    /**
     *    联系方式
    **/
    @JSONField(name="mobilephone")
    private String mobilephone;
    /**
     *    刊登版面
    **/
    @JSONField(name="publishpage")
    private String publishpage;
    /**
     *    刊登日期
    **/
    @JSONField(name="publishdate")
    private String publishdate;
    /**
     *    当事人
    **/
    @JSONField(name="party2Str")
    private String party2Str;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    原告
    **/
    @JSONField(name="party1Str")
    private String party1Str;


    /**
    *   设置 公告id
    **/
    public void setAnnounce_id(Integer announce_id) {
      this.announce_id = announce_id;
    }
    /**
    *   获取 公告id
    **/
    public Integer getAnnounce_id() {
      return announce_id;
    }
    /**
    *   设置 原告
    **/
    public void setParty1(String party1) {
      this.party1 = party1;
    }
    /**
    *   获取 原告
    **/
    public String getParty1() {
      return party1;
    }
    /**
    *   设置 公告号
    **/
    public void setBltnno(String bltnno) {
      this.bltnno = bltnno;
    }
    /**
    *   获取 公告号
    **/
    public String getBltnno() {
      return bltnno;
    }
    /**
    *   设置 当事人
    **/
    public void setParty2(String party2) {
      this.party2 = party2;
    }
    /**
    *   获取 当事人
    **/
    public String getParty2() {
      return party2;
    }
    /**
    *   设置 法官电话
    **/
    public void setJudgephone(String judgephone) {
      this.judgephone = judgephone;
    }
    /**
    *   获取 法官电话
    **/
    public String getJudgephone() {
      return judgephone;
    }
    /**
    *   设置 唯一标识符
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 唯一标识符
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 公告类型名称
    **/
    public void setBltntypename(String bltntypename) {
      this.bltntypename = bltntypename;
    }
    /**
    *   获取 公告类型名称
    **/
    public String getBltntypename() {
      return bltntypename;
    }
    /**
    *   设置 案件内容
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 案件内容
    **/
    public String getContent() {
      return content;
    }
    /**
    *   设置 法院名
    **/
    public void setCourtcode(String courtcode) {
      this.courtcode = courtcode;
    }
    /**
    *   获取 法院名
    **/
    public String getCourtcode() {
      return courtcode;
    }
    /**
    *   设置 省份
    **/
    public void setProvince(String province) {
      this.province = province;
    }
    /**
    *   获取 省份
    **/
    public String getProvince() {
      return province;
    }
    /**
    *   设置 相关公司列表
    **/
    public void setCompanyList(List<HiCourtCompanyList> companyList) {
      this.companyList = companyList;
    }
    /**
    *   获取 相关公司列表
    **/
    public List<HiCourtCompanyList> getCompanyList() {
      return companyList;
    }
    /**
    *   设置 联系方式
    **/
    public void setMobilephone(String mobilephone) {
      this.mobilephone = mobilephone;
    }
    /**
    *   获取 联系方式
    **/
    public String getMobilephone() {
      return mobilephone;
    }
    /**
    *   设置 刊登版面
    **/
    public void setPublishpage(String publishpage) {
      this.publishpage = publishpage;
    }
    /**
    *   获取 刊登版面
    **/
    public String getPublishpage() {
      return publishpage;
    }
    /**
    *   设置 刊登日期
    **/
    public void setPublishdate(String publishdate) {
      this.publishdate = publishdate;
    }
    /**
    *   获取 刊登日期
    **/
    public String getPublishdate() {
      return publishdate;
    }
    /**
    *   设置 当事人
    **/
    public void setParty2Str(String party2Str) {
      this.party2Str = party2Str;
    }
    /**
    *   获取 当事人
    **/
    public String getParty2Str() {
      return party2Str;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 原告
    **/
    public void setParty1Str(String party1Str) {
      this.party1Str = party1Str;
    }
    /**
    *   获取 原告
    **/
    public String getParty1Str() {
      return party1Str;
    }



}

