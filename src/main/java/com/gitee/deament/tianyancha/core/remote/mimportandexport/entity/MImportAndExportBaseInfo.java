package com.gitee.deament.tianyancha.core.remote.mimportandexport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*进出口信用
* 属于MImportAndExport
* /services/open/m/importAndExport/2.0
*@author deament
**/

public class MImportAndExportBaseInfo implements Serializable{

    /**
     *    经济区划
    **/
    @JSONField(name="economicDivision")
    private String economicDivision;
    /**
     *    行政区划
    **/
    @JSONField(name="administrativeDivision")
    private String administrativeDivision;
    /**
     *    跨境贸易电子商务类型
    **/
    @JSONField(name="types")
    private String types;
    /**
     *    特殊贸易区域
    **/
    @JSONField(name="specialTradeArea")
    private String specialTradeArea;
    /**
     *    行业种类
    **/
    @JSONField(name="industryCategory")
    private String industryCategory;
    /**
     *    经营类别
    **/
    @JSONField(name="managementCategory")
    private String managementCategory;
    /**
     *    报关有效期
    **/
    @JSONField(name="validityDate")
    private String validityDate;
    /**
     *    注册日期
    **/
    @JSONField(name="recordDate")
    private String recordDate;
    /**
     *    海关注册号
    **/
    @JSONField(name="crCode")
    private String crCode;
    /**
     *    注册海关
    **/
    @JSONField(name="customsRegisteredAddress")
    private String customsRegisteredAddress;
    /**
     *    年报情况
    **/
    @JSONField(name="annualReport")
    private String annualReport;
    /**
     *    海关注销标识
    **/
    @JSONField(name="status")
    private String status;


    /**
    *   设置 经济区划
    **/
    public void setEconomicDivision(String economicDivision) {
      this.economicDivision = economicDivision;
    }
    /**
    *   获取 经济区划
    **/
    public String getEconomicDivision() {
      return economicDivision;
    }
    /**
    *   设置 行政区划
    **/
    public void setAdministrativeDivision(String administrativeDivision) {
      this.administrativeDivision = administrativeDivision;
    }
    /**
    *   获取 行政区划
    **/
    public String getAdministrativeDivision() {
      return administrativeDivision;
    }
    /**
    *   设置 跨境贸易电子商务类型
    **/
    public void setTypes(String types) {
      this.types = types;
    }
    /**
    *   获取 跨境贸易电子商务类型
    **/
    public String getTypes() {
      return types;
    }
    /**
    *   设置 特殊贸易区域
    **/
    public void setSpecialTradeArea(String specialTradeArea) {
      this.specialTradeArea = specialTradeArea;
    }
    /**
    *   获取 特殊贸易区域
    **/
    public String getSpecialTradeArea() {
      return specialTradeArea;
    }
    /**
    *   设置 行业种类
    **/
    public void setIndustryCategory(String industryCategory) {
      this.industryCategory = industryCategory;
    }
    /**
    *   获取 行业种类
    **/
    public String getIndustryCategory() {
      return industryCategory;
    }
    /**
    *   设置 经营类别
    **/
    public void setManagementCategory(String managementCategory) {
      this.managementCategory = managementCategory;
    }
    /**
    *   获取 经营类别
    **/
    public String getManagementCategory() {
      return managementCategory;
    }
    /**
    *   设置 报关有效期
    **/
    public void setValidityDate(String validityDate) {
      this.validityDate = validityDate;
    }
    /**
    *   获取 报关有效期
    **/
    public String getValidityDate() {
      return validityDate;
    }
    /**
    *   设置 注册日期
    **/
    public void setRecordDate(String recordDate) {
      this.recordDate = recordDate;
    }
    /**
    *   获取 注册日期
    **/
    public String getRecordDate() {
      return recordDate;
    }
    /**
    *   设置 海关注册号
    **/
    public void setCrCode(String crCode) {
      this.crCode = crCode;
    }
    /**
    *   获取 海关注册号
    **/
    public String getCrCode() {
      return crCode;
    }
    /**
    *   设置 注册海关
    **/
    public void setCustomsRegisteredAddress(String customsRegisteredAddress) {
      this.customsRegisteredAddress = customsRegisteredAddress;
    }
    /**
    *   获取 注册海关
    **/
    public String getCustomsRegisteredAddress() {
      return customsRegisteredAddress;
    }
    /**
    *   设置 年报情况
    **/
    public void setAnnualReport(String annualReport) {
      this.annualReport = annualReport;
    }
    /**
    *   获取 年报情况
    **/
    public String getAnnualReport() {
      return annualReport;
    }
    /**
    *   设置 海关注销标识
    **/
    public void setStatus(String status) {
      this.status = status;
    }
    /**
    *   获取 海关注销标识
    **/
    public String getStatus() {
      return status;
    }



}

