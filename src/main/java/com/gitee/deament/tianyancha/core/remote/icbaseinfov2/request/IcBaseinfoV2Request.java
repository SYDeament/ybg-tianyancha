package com.gitee.deament.tianyancha.core.remote.icbaseinfov2.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icbaseinfov2.entity.IcBaseinfoV2;
import com.gitee.deament.tianyancha.core.remote.icbaseinfov2.dto.IcBaseinfoV2DTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业基本信息（含企业联系方式）
* 可以通过公司名称或ID或ID称获取企业基本信息和企业联系方式，包括公司名称或ID、类型、成立日期、电话、邮箱、网址等字段的详细信息
* /services/open/ic/baseinfoV2/2.0
*@author deament
**/

public interface IcBaseinfoV2Request extends BaseRequest<IcBaseinfoV2DTO ,IcBaseinfoV2>{

}

