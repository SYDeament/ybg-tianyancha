package com.gitee.deament.tianyancha.core.remote.hijudicial.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hijudicial.entity.HiJudicial;
import com.gitee.deament.tianyancha.core.remote.hijudicial.dto.HiJudicialDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史司法协助详情
* 根据司法协助ID获取历史的司法协助详情，判断司法协助类型，历史司法协助详情包括股权变更、股权冻结、结解除冻结、司法协助续行、股权数额、司法冻结失效及对应的详细信息
* /services/open/hi/judicial/detail/2.0
*@author deament
**/
@Component("hiJudicialRequestImpl")
public class HiJudicialRequestImpl extends BaseRequestImpl<HiJudicial,HiJudicialDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/judicial/detail/2.0";
    }
}

