package com.gitee.deament.tianyancha.core.remote.jrktannouncement.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrktannouncement.entity.JrKtannouncementPlaintiff;
import com.gitee.deament.tianyancha.core.remote.jrktannouncement.entity.JrKtannouncementDefendant;
import com.gitee.deament.tianyancha.core.remote.jrktannouncement.entity.JrKtannouncementItems;
/**
*开庭公告
* 可以通过公司名称或ID获取企业开庭公告，开庭公告信息包括被告/被上诉人、法院、原告/上诉人、开庭日期、案由、内部ID、案号等字段的详细信息
* /services/open/jr/ktannouncement/2.0
*@author deament
**/

public class JrKtannouncement implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrKtannouncementItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrKtannouncementItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrKtannouncementItems> getItems() {
      return items;
    }



}

