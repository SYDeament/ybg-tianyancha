package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryInvestList implements Serializable{

    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    投资金额
    **/
    @JSONField(name="amount")
    private Integer amount;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblishTime")
    private String estiblishTime;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    占比
    **/
    @JSONField(name="percent")
    private String percent;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    经营范围
    **/
    @JSONField(name="business_scope")
    private String business_scope;
    /**
     *    企业类型
    **/
    @JSONField(name="orgType")
    private String orgType;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    企业简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    行业
    **/
    @JSONField(name="category")
    private String category;
    /**
     *    法人类型 1-人 2-公司
    **/
    @JSONField(name="personType")
    private Integer personType;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 投资金额
    **/
    public void setAmount(Integer amount) {
      this.amount = amount;
    }
    /**
    *   获取 投资金额
    **/
    public Integer getAmount() {
      return amount;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblishTime(String estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立日期
    **/
    public String getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 占比
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 占比
    **/
    public String getPercent() {
      return percent;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 经营范围
    **/
    public void setBusiness_scope(String business_scope) {
      this.business_scope = business_scope;
    }
    /**
    *   获取 经营范围
    **/
    public String getBusiness_scope() {
      return business_scope;
    }
    /**
    *   设置 企业类型
    **/
    public void setOrgType(String orgType) {
      this.orgType = orgType;
    }
    /**
    *   获取 企业类型
    **/
    public String getOrgType() {
      return orgType;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 企业简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 企业简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 行业
    **/
    public void setCategory(String category) {
      this.category = category;
    }
    /**
    *   获取 行业
    **/
    public String getCategory() {
      return category;
    }
    /**
    *   设置 法人类型 1-人 2-公司
    **/
    public void setPersonType(Integer personType) {
      this.personType = personType;
    }
    /**
    *   获取 法人类型 1-人 2-公司
    **/
    public Integer getPersonType() {
      return personType;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }



}

