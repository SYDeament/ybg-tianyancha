package com.gitee.deament.tianyancha.core.remote.icbranch.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icbranch.entity.IcBranch;
import com.gitee.deament.tianyancha.core.remote.icbranch.dto.IcBranchDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*分支机构
* 可以通过公司名称或ID获取企业分支机构信息，分支机构信息包括分公司名称或ID、企业法人、经营状态、分公司总数等字段的详细信息
* /services/open/ic/branch/2.0
*@author deament
**/

public interface IcBranchRequest extends BaseRequest<IcBranchDTO ,IcBranch>{

}

