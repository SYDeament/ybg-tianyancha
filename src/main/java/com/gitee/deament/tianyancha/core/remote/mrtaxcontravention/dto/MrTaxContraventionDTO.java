package com.gitee.deament.tianyancha.core.remote.mrtaxcontravention.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*税收违法详情
* 根据税收违法ID获取税收违法详情，税收违法详情包括纳税人名称、纳税人识别号、注册地址、违法事实、财务负责人信息、负有责任的中介信息等字段的详细信息
* /services/open/mr/taxContravention/detail/2.0
*@author deament
**/

@TYCURL(value="/services/open/mr/taxContravention/detail/2.0")
public class MrTaxContraventionDTO implements Serializable{

    /**
     *    违法id
     *
    **/
    @ParamRequire(require = true)
    private Long id;


    /**
    *   设置 违法id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 违法id
    **/
    public Long getId() {
      return id;
    }



}

