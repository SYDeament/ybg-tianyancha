package com.gitee.deament.tianyancha.core.remote.stockprospectus.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockprospectus.entity.StockProspectusItems;
/**
*招股书
* 可以通过公司名称或ID获取上市公司招股书信息，招股书信息包括招股书标题、股票名称、招股书内容等
* /services/open/stock/prospectus/2.0
*@author deament
**/

public class StockProspectus implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<StockProspectusItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<StockProspectusItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<StockProspectusItems> getItems() {
      return items;
    }



}

