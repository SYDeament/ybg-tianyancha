package com.gitee.deament.tianyancha.core.remote.icholder.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业股东
* 属于IcHolder
* /services/open/ic/holder/2.0
*@author deament
**/

public class IcHolderCapitalActl implements Serializable{

    /**
     *    出资金额
    **/
    @JSONField(name="amomon")
    private String amomon;
    /**
     *    实缴方式
    **/
    @JSONField(name="paymet")
    private String paymet;
    /**
     *    出资时间
    **/
    @JSONField(name="time")
    private String time;
    /**
     *    占比
    **/
    @JSONField(name="percent")
    private String percent;


    /**
    *   设置 出资金额
    **/
    public void setAmomon(String amomon) {
      this.amomon = amomon;
    }
    /**
    *   获取 出资金额
    **/
    public String getAmomon() {
      return amomon;
    }
    /**
    *   设置 实缴方式
    **/
    public void setPaymet(String paymet) {
      this.paymet = paymet;
    }
    /**
    *   获取 实缴方式
    **/
    public String getPaymet() {
      return paymet;
    }
    /**
    *   设置 出资时间
    **/
    public void setTime(String time) {
      this.time = time;
    }
    /**
    *   获取 出资时间
    **/
    public String getTime() {
      return time;
    }
    /**
    *   设置 占比
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 占比
    **/
    public String getPercent() {
      return percent;
    }



}

