package com.gitee.deament.tianyancha.core.remote.oisecretinvest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oisecretinvest.entity.OiSecretInvestInvestor_list;
import com.gitee.deament.tianyancha.core.remote.oisecretinvest.entity.OiSecretInvestItems;
/**
*未公开投资
* 可以通过投资机构名称获取未公开投资企业，包括企业名称、法人、注册资本、成立日期、投资方等字段的详细信息
* /services/open/oi/secretInvest/2.0
*@author deament
**/

public class OiSecretInvest implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiSecretInvestItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiSecretInvestItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiSecretInvestItems> getItems() {
      return items;
    }



}

