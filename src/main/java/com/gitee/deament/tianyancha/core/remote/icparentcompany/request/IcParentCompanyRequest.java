package com.gitee.deament.tianyancha.core.remote.icparentcompany.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icparentcompany.entity.IcParentCompany;
import com.gitee.deament.tianyancha.core.remote.icparentcompany.dto.IcParentCompanyDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*总公司
* 可以通过公司名称或ID获取企业总公司信息，总公司信息包括总公司名称或ID、企业法人、经营状态、注册资本等字段的详细信息
* /services/open/ic/parentCompany/2.0
*@author deament
**/

public interface IcParentCompanyRequest extends BaseRequest<IcParentCompanyDTO ,IcParentCompany>{

}

