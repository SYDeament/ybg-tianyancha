package com.gitee.deament.tianyancha.core.remote.hiinvest.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiinvest.entity.HiInvest;
import com.gitee.deament.tianyancha.core.remote.hiinvest.dto.HiInvestDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*历史对外投资
* 历史对外投资
* /services/open/hi/invest/2.0
*@author deament
**/

public interface HiInvestRequest extends BaseRequest<HiInvestDTO ,HiInvest>{

}

