package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcOutBoundList implements Serializable{

    /**
     *    对外投资企业名称
    **/
    @JSONField(name="outcompanyName")
    private String outcompanyName;
    /**
     *    统一信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    注册号
    **/
    @JSONField(name="regNum")
    private String regNum;


    /**
    *   设置 对外投资企业名称
    **/
    public void setOutcompanyName(String outcompanyName) {
      this.outcompanyName = outcompanyName;
    }
    /**
    *   获取 对外投资企业名称
    **/
    public String getOutcompanyName() {
      return outcompanyName;
    }
    /**
    *   设置 统一信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 注册号
    **/
    public void setRegNum(String regNum) {
      this.regNum = regNum;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNum() {
      return regNum;
    }



}

