package com.gitee.deament.tianyancha.core.remote.stockcorpbasicinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockcorpbasicinfo.entity.StockCorpBasicInfo;
import com.gitee.deament.tianyancha.core.remote.stockcorpbasicinfo.dto.StockCorpBasicInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*重要人员
* 可以通过公司名称或ID获取上市公司重要人员信息，重要人员信息包括总经理、法人代表、董秘、董事长、证券事务代表、独立董事等
* /services/open/stock/corpBasicInfo/2.0
*@author deament
**/

public interface StockCorpBasicInfoRequest extends BaseRequest<StockCorpBasicInfoDTO ,StockCorpBasicInfo>{

}

