package com.gitee.deament.tianyancha.core.remote.ipricp.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.ipricp.entity.IprIcp;
import com.gitee.deament.tianyancha.core.remote.ipricp.dto.IprIcpDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*网站备案
* 可以通过公司名称或ID获取网站备案的有关信息，包括网站名称、网站首页、域名、网站备案/许可证号等字段信息
* /services/open/ipr/icp/3.0
*@author deament
**/
@Component("iprIcpRequestImpl")
public class IprIcpRequestImpl extends BaseRequestImpl<IprIcp,IprIcpDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ipr/icp/3.0";
    }
}

