package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailPeopleInfo implements Serializable{

    /**
     *    抵押权人证照/证件类型
    **/
    @JSONField(name="liceseType")
    private String liceseType;
    /**
     *    抵押权人名称
    **/
    @JSONField(name="peopleName")
    private String peopleName;
    /**
     *    证照/证件号码
    **/
    @JSONField(name="licenseNum")
    private String licenseNum;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;


    /**
    *   设置 抵押权人证照/证件类型
    **/
    public void setLiceseType(String liceseType) {
      this.liceseType = liceseType;
    }
    /**
    *   获取 抵押权人证照/证件类型
    **/
    public String getLiceseType() {
      return liceseType;
    }
    /**
    *   设置 抵押权人名称
    **/
    public void setPeopleName(String peopleName) {
      this.peopleName = peopleName;
    }
    /**
    *   获取 抵押权人名称
    **/
    public String getPeopleName() {
      return peopleName;
    }
    /**
    *   设置 证照/证件号码
    **/
    public void setLicenseNum(String licenseNum) {
      this.licenseNum = licenseNum;
    }
    /**
    *   获取 证照/证件号码
    **/
    public String getLicenseNum() {
      return licenseNum;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }



}

