package com.gitee.deament.tianyancha.core.remote.cdfindtzanli.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdfindtzanli.entity.CdFindTzanliItems;
/**
*投资事件
* 可以通过公司名称或ID获取企业投资事件信息，企业投资事件信息包括投资事件轮次、金额、投资方、投资产品、行业、业务等字段的详细信息
* /services/open/cd/findTzanli/2.0
*@author deament
**/

public class CdFindTzanli implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CdFindTzanliItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CdFindTzanliItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CdFindTzanliItems> getItems() {
      return items;
    }



}

