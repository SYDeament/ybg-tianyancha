package com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.entity.MTeleCommunicationLicense;
import com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.dto.MTeleCommunicationLicenseDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*电信许可
* 可以通过公司名称或ID获取企业电信许可信息，企业电信许可信息包括客户、销售占比、销售金额、报告期等字段的详细信息
* /services/open/m/teleCommunicationLicense/2.0
*@author deament
**/

public interface MTeleCommunicationLicenseRequest extends BaseRequest<MTeleCommunicationLicenseDTO ,MTeleCommunicationLicense>{

}

