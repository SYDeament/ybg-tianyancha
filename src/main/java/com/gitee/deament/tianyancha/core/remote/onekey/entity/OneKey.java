package com.gitee.deament.tianyancha.core.remote.onekey.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.onekey.entity.OneKeyProperties;
import com.gitee.deament.tianyancha.core.remote.onekey.entity.OneKeyRelationships;
import com.gitee.deament.tianyancha.core.remote.onekey.entity.OneKeyProperties;
import com.gitee.deament.tianyancha.core.remote.onekey.entity.OneKeyNodes;
/**
*企业图谱
* 可以通过公司ID获取企业关系图谱，包含法人、股东、高管、对外投资、分支机构列表
* /services/v4/open/oneKey/c
*@author deament
**/

public class OneKey implements Serializable{

    /**
     *    关系
    **/
    @JSONField(name="relationships")
    private List<OneKeyRelationships> relationships;
    /**
     *    节点
    **/
    @JSONField(name="nodes")
    private List<OneKeyNodes> nodes;


    /**
    *   设置 关系
    **/
    public void setRelationships(List<OneKeyRelationships> relationships) {
      this.relationships = relationships;
    }
    /**
    *   获取 关系
    **/
    public List<OneKeyRelationships> getRelationships() {
      return relationships;
    }
    /**
    *   设置 节点
    **/
    public void setNodes(List<OneKeyNodes> nodes) {
      this.nodes = nodes;
    }
    /**
    *   获取 节点
    **/
    public List<OneKeyNodes> getNodes() {
      return nodes;
    }



}

