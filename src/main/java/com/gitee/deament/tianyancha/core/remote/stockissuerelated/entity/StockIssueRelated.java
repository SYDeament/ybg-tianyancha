package com.gitee.deament.tianyancha.core.remote.stockissuerelated.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockissuerelated.entity.StockIssueRelatedMainUnderwriter;
import com.gitee.deament.tianyancha.core.remote.stockissuerelated.entity.StockIssueRelatedListingSponsor;
/**
*发行相关
* 可以通过公司名称或ID获取上市公司发行相关信息，发行相关信息包括成立日期、上市日期、发行数量、发行价格等
* /services/open/stock/issueRelated/2.0
*@author deament
**/

public class StockIssueRelated implements Serializable{

    /**
     *    实际募资
    **/
    @JSONField(name="actualRaised")
    private String actualRaised;
    /**
     *    上市日期
    **/
    @JSONField(name="listingDate")
    private String listingDate;
    /**
     *    发行价格
    **/
    @JSONField(name="issuePrice")
    private String issuePrice;
    /**
     *    发行市盈率
    **/
    @JSONField(name="ipoRatio")
    private String ipoRatio;
    /**
     *    主承销商
    **/
    @JSONField(name="mainUnderwriter")
    private StockIssueRelatedMainUnderwriter mainUnderwriter;
    /**
     *    发行中签率
    **/
    @JSONField(name="rate")
    private String rate;
    /**
     *    发行数量
    **/
    @JSONField(name="issueNumber")
    private String issueNumber;
    /**
     *    上市保荐人
    **/
    @JSONField(name="listingSponsor")
    private StockIssueRelatedListingSponsor listingSponsor;
    /**
     *    历史沿革
    **/
    @JSONField(name="history")
    private String history;
    /**
     *    成立日期
    **/
    @JSONField(name="issueDate")
    private String issueDate;
    /**
     *    预计募资
    **/
    @JSONField(name="expectedToRaise")
    private String expectedToRaise;
    /**
     *    首日开盘价
    **/
    @JSONField(name="openingPrice")
    private String openingPrice;


    /**
    *   设置 实际募资
    **/
    public void setActualRaised(String actualRaised) {
      this.actualRaised = actualRaised;
    }
    /**
    *   获取 实际募资
    **/
    public String getActualRaised() {
      return actualRaised;
    }
    /**
    *   设置 上市日期
    **/
    public void setListingDate(String listingDate) {
      this.listingDate = listingDate;
    }
    /**
    *   获取 上市日期
    **/
    public String getListingDate() {
      return listingDate;
    }
    /**
    *   设置 发行价格
    **/
    public void setIssuePrice(String issuePrice) {
      this.issuePrice = issuePrice;
    }
    /**
    *   获取 发行价格
    **/
    public String getIssuePrice() {
      return issuePrice;
    }
    /**
    *   设置 发行市盈率
    **/
    public void setIpoRatio(String ipoRatio) {
      this.ipoRatio = ipoRatio;
    }
    /**
    *   获取 发行市盈率
    **/
    public String getIpoRatio() {
      return ipoRatio;
    }
    /**
    *   设置 主承销商
    **/
    public void setMainUnderwriter(StockIssueRelatedMainUnderwriter mainUnderwriter) {
      this.mainUnderwriter = mainUnderwriter;
    }
    /**
    *   获取 主承销商
    **/
    public StockIssueRelatedMainUnderwriter getMainUnderwriter() {
      return mainUnderwriter;
    }
    /**
    *   设置 发行中签率
    **/
    public void setRate(String rate) {
      this.rate = rate;
    }
    /**
    *   获取 发行中签率
    **/
    public String getRate() {
      return rate;
    }
    /**
    *   设置 发行数量
    **/
    public void setIssueNumber(String issueNumber) {
      this.issueNumber = issueNumber;
    }
    /**
    *   获取 发行数量
    **/
    public String getIssueNumber() {
      return issueNumber;
    }
    /**
    *   设置 上市保荐人
    **/
    public void setListingSponsor(StockIssueRelatedListingSponsor listingSponsor) {
      this.listingSponsor = listingSponsor;
    }
    /**
    *   获取 上市保荐人
    **/
    public StockIssueRelatedListingSponsor getListingSponsor() {
      return listingSponsor;
    }
    /**
    *   设置 历史沿革
    **/
    public void setHistory(String history) {
      this.history = history;
    }
    /**
    *   获取 历史沿革
    **/
    public String getHistory() {
      return history;
    }
    /**
    *   设置 成立日期
    **/
    public void setIssueDate(String issueDate) {
      this.issueDate = issueDate;
    }
    /**
    *   获取 成立日期
    **/
    public String getIssueDate() {
      return issueDate;
    }
    /**
    *   设置 预计募资
    **/
    public void setExpectedToRaise(String expectedToRaise) {
      this.expectedToRaise = expectedToRaise;
    }
    /**
    *   获取 预计募资
    **/
    public String getExpectedToRaise() {
      return expectedToRaise;
    }
    /**
    *   设置 首日开盘价
    **/
    public void setOpeningPrice(String openingPrice) {
      this.openingPrice = openingPrice;
    }
    /**
    *   获取 首日开盘价
    **/
    public String getOpeningPrice() {
      return openingPrice;
    }



}

