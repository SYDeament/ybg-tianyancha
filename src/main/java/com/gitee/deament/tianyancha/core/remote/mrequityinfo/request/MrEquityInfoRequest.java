package com.gitee.deament.tianyancha.core.remote.mrequityinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrequityinfo.entity.MrEquityInfo;
import com.gitee.deament.tianyancha.core.remote.mrequityinfo.dto.MrEquityInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*股权出质
* 可以通过公司名称或ID获取出质股权标的企业信息，包括企业质权人信息、出质人信息、出质股权数额等字段的详细信息
* /services/open/mr/equityInfo/2.0
*@author deament
**/

public interface MrEquityInfoRequest extends BaseRequest<MrEquityInfoDTO ,MrEquityInfo>{

}

