package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryKtAnnouncementList implements Serializable{

    /**
     *    承办部门
    **/
    @JSONField(name="contractors")
    private String contractors;
    /**
     *    原告/上诉人
    **/
    @JSONField(name="plaintiff")
    private List<CbHistoryPlaintiff> plaintiff;
    /**
     *    法庭
    **/
    @JSONField(name="courtroom")
    private String courtroom;
    /**
     *    当事人
    **/
    @JSONField(name="litigant")
    private String litigant;
    /**
     *    被告/被上诉人
    **/
    @JSONField(name="defendant")
    private List<CbHistoryDefendant> defendant;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    审判长/主审人
    **/
    @JSONField(name="judge")
    private String judge;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    案号
    **/
    @JSONField(name="caseNo")
    private String caseNo;
    /**
     *    开始日期
    **/
    @JSONField(name="startDate")
    private String startDate;
    /**
     *    案由
    **/
    @JSONField(name="caseReason")
    private String caseReason;


    /**
    *   设置 承办部门
    **/
    public void setContractors(String contractors) {
      this.contractors = contractors;
    }
    /**
    *   获取 承办部门
    **/
    public String getContractors() {
      return contractors;
    }
    /**
    *   设置 原告/上诉人
    **/
    public void setPlaintiff(List<CbHistoryPlaintiff> plaintiff) {
      this.plaintiff = plaintiff;
    }
    /**
    *   获取 原告/上诉人
    **/
    public List<CbHistoryPlaintiff> getPlaintiff() {
      return plaintiff;
    }
    /**
    *   设置 法庭
    **/
    public void setCourtroom(String courtroom) {
      this.courtroom = courtroom;
    }
    /**
    *   获取 法庭
    **/
    public String getCourtroom() {
      return courtroom;
    }
    /**
    *   设置 当事人
    **/
    public void setLitigant(String litigant) {
      this.litigant = litigant;
    }
    /**
    *   获取 当事人
    **/
    public String getLitigant() {
      return litigant;
    }
    /**
    *   设置 被告/被上诉人
    **/
    public void setDefendant(List<CbHistoryDefendant> defendant) {
      this.defendant = defendant;
    }
    /**
    *   获取 被告/被上诉人
    **/
    public List<CbHistoryDefendant> getDefendant() {
      return defendant;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 审判长/主审人
    **/
    public void setJudge(String judge) {
      this.judge = judge;
    }
    /**
    *   获取 审判长/主审人
    **/
    public String getJudge() {
      return judge;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 案号
    **/
    public void setCaseNo(String caseNo) {
      this.caseNo = caseNo;
    }
    /**
    *   获取 案号
    **/
    public String getCaseNo() {
      return caseNo;
    }
    /**
    *   设置 开始日期
    **/
    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 开始日期
    **/
    public String getStartDate() {
      return startDate;
    }
    /**
    *   设置 案由
    **/
    public void setCaseReason(String caseReason) {
      this.caseReason = caseReason;
    }
    /**
    *   获取 案由
    **/
    public String getCaseReason() {
      return caseReason;
    }



}

