package com.gitee.deament.tianyancha.core.remote.riskdetail.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*天眼风险详情
* 可以通过企业风险ID和人风险ID获取企业或人员相关天眼风险详情，包括企业和人员自身/周边/预警风险信息详情
* /services/v4/open/riskDetail
*@author deament
**/

@TYCURL(value="/services/v4/open/riskDetail")
public class RiskDetailDTO implements Serializable{

    /**
     *    每页条数（最大10条）
     *
    **/
    @ParamRequire(require = true)
    private String pageSize;
    /**
     *    传入企业风险接口返回值中的id,对应的风险类型不同，返回数据也不同，示例仅供查看对比字段含义
     *
    **/
    @ParamRequire(require = true)
    private Long id;
    /**
     *    当前页
     *
    **/
    @ParamRequire(require = true)
    private Long pageNum;


    /**
    *   设置 每页条数（最大10条）
    **/
    public void setPageSize(String pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（最大10条）
    **/
    public String getPageSize() {
      return pageSize;
    }
    /**
    *   设置 传入企业风险接口返回值中的id,对应的风险类型不同，返回数据也不同，示例仅供查看对比字段含义
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 传入企业风险接口返回值中的id,对应的风险类型不同，返回数据也不同，示例仅供查看对比字段含义
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 当前页
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

