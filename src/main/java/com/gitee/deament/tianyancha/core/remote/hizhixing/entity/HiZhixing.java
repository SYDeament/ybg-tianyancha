package com.gitee.deament.tianyancha.core.remote.hizhixing.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hizhixing.entity.HiZhixingItems;
/**
*历史被执行人
* 可以通过公司名称或ID获取企业历史的被执行人信息，历史被执行人信息包括执行法院、案件内容、执行标的、被执行人名称、组织机构代码等字段信息
* /services/open/hi/zhixing/2.0
*@author deament
**/

public class HiZhixing implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiZhixingItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiZhixingItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiZhixingItems> getItems() {
      return items;
    }



}

