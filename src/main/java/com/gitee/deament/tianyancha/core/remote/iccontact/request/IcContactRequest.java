package com.gitee.deament.tianyancha.core.remote.iccontact.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iccontact.entity.IcContact;
import com.gitee.deament.tianyancha.core.remote.iccontact.dto.IcContactDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业联系方式
* 可以通过公司名称或ID获取企业联系方式信息，企业联系方式信息包括邮箱、网址、电话等字段的详细信息
* /services/open/ic/contact
*@author deament
**/

public interface IcContactRequest extends BaseRequest<IcContactDTO ,IcContact>{

}

