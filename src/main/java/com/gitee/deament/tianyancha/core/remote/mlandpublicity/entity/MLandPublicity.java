package com.gitee.deament.tianyancha.core.remote.mlandpublicity.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*地块公示详情
* 可以通过地块公示ID获取企业地块公示详情，企业地块公示详情包括地块位置、土地用途、面积、发布机关、联系方式等字段的详细信息
* /services/open/m/landPublicity/detail/2.0
*@author deament
**/

public class MLandPublicity implements Serializable{

    /**
     *    受让单位
    **/
    @JSONField(name="landUser_clean")
    private List<String> landUser_clean;
    /**
     *    发布机关
    **/
    @JSONField(name="publication_organize")
    private String publication_organize;
    /**
     *    联系人
    **/
    @JSONField(name="contact_person")
    private String contact_person;
    /**
     *    地块位置
    **/
    @JSONField(name="land_location")
    private String land_location;
    /**
     *    土地面积（公顷）
    **/
    @JSONField(name="land_area")
    private String land_area;
    /**
     *    备注
    **/
    @JSONField(name="remark")
    private String remark;
    /**
     *    公示期
    **/
    @JSONField(name="public_announcement_period")
    private String public_announcement_period;
    /**
     *    宗地编号
    **/
    @JSONField(name="land_num")
    private String land_num;
    /**
     *    项目名称
    **/
    @JSONField(name="project_name")
    private String project_name;
    /**
     *    联系电话
    **/
    @JSONField(name="contact_number")
    private String contact_number;
    /**
     *    电子邮件
    **/
    @JSONField(name="electronic_mail")
    private String electronic_mail;
    /**
     *    单位地址
    **/
    @JSONField(name="organize_location")
    private String organize_location;
    /**
     *    土地用途
    **/
    @JSONField(name="land_usefulness")
    private String land_usefulness;
    /**
     *    意见反馈方式
    **/
    @JSONField(name="feedback_method")
    private String feedback_method;
    /**
     *    发布日期
    **/
    @JSONField(name="publication_date")
    private String publication_date;
    /**
     *    联系单位
    **/
    @JSONField(name="contact_organize")
    private String contact_organize;
    /**
     *    邮政编码
    **/
    @JSONField(name="postal_code")
    private String postal_code;


    /**
    *   设置 受让单位
    **/
    public void setLandUser_clean(List<String> landUser_clean) {
      this.landUser_clean = landUser_clean;
    }
    /**
    *   获取 受让单位
    **/
    public List<String> getLandUser_clean() {
      return landUser_clean;
    }
    /**
    *   设置 发布机关
    **/
    public void setPublication_organize(String publication_organize) {
      this.publication_organize = publication_organize;
    }
    /**
    *   获取 发布机关
    **/
    public String getPublication_organize() {
      return publication_organize;
    }
    /**
    *   设置 联系人
    **/
    public void setContact_person(String contact_person) {
      this.contact_person = contact_person;
    }
    /**
    *   获取 联系人
    **/
    public String getContact_person() {
      return contact_person;
    }
    /**
    *   设置 地块位置
    **/
    public void setLand_location(String land_location) {
      this.land_location = land_location;
    }
    /**
    *   获取 地块位置
    **/
    public String getLand_location() {
      return land_location;
    }
    /**
    *   设置 土地面积（公顷）
    **/
    public void setLand_area(String land_area) {
      this.land_area = land_area;
    }
    /**
    *   获取 土地面积（公顷）
    **/
    public String getLand_area() {
      return land_area;
    }
    /**
    *   设置 备注
    **/
    public void setRemark(String remark) {
      this.remark = remark;
    }
    /**
    *   获取 备注
    **/
    public String getRemark() {
      return remark;
    }
    /**
    *   设置 公示期
    **/
    public void setPublic_announcement_period(String public_announcement_period) {
      this.public_announcement_period = public_announcement_period;
    }
    /**
    *   获取 公示期
    **/
    public String getPublic_announcement_period() {
      return public_announcement_period;
    }
    /**
    *   设置 宗地编号
    **/
    public void setLand_num(String land_num) {
      this.land_num = land_num;
    }
    /**
    *   获取 宗地编号
    **/
    public String getLand_num() {
      return land_num;
    }
    /**
    *   设置 项目名称
    **/
    public void setProject_name(String project_name) {
      this.project_name = project_name;
    }
    /**
    *   获取 项目名称
    **/
    public String getProject_name() {
      return project_name;
    }
    /**
    *   设置 联系电话
    **/
    public void setContact_number(String contact_number) {
      this.contact_number = contact_number;
    }
    /**
    *   获取 联系电话
    **/
    public String getContact_number() {
      return contact_number;
    }
    /**
    *   设置 电子邮件
    **/
    public void setElectronic_mail(String electronic_mail) {
      this.electronic_mail = electronic_mail;
    }
    /**
    *   获取 电子邮件
    **/
    public String getElectronic_mail() {
      return electronic_mail;
    }
    /**
    *   设置 单位地址
    **/
    public void setOrganize_location(String organize_location) {
      this.organize_location = organize_location;
    }
    /**
    *   获取 单位地址
    **/
    public String getOrganize_location() {
      return organize_location;
    }
    /**
    *   设置 土地用途
    **/
    public void setLand_usefulness(String land_usefulness) {
      this.land_usefulness = land_usefulness;
    }
    /**
    *   获取 土地用途
    **/
    public String getLand_usefulness() {
      return land_usefulness;
    }
    /**
    *   设置 意见反馈方式
    **/
    public void setFeedback_method(String feedback_method) {
      this.feedback_method = feedback_method;
    }
    /**
    *   获取 意见反馈方式
    **/
    public String getFeedback_method() {
      return feedback_method;
    }
    /**
    *   设置 发布日期
    **/
    public void setPublication_date(String publication_date) {
      this.publication_date = publication_date;
    }
    /**
    *   获取 发布日期
    **/
    public String getPublication_date() {
      return publication_date;
    }
    /**
    *   设置 联系单位
    **/
    public void setContact_organize(String contact_organize) {
      this.contact_organize = contact_organize;
    }
    /**
    *   获取 联系单位
    **/
    public String getContact_organize() {
      return contact_organize;
    }
    /**
    *   设置 邮政编码
    **/
    public void setPostal_code(String postal_code) {
      this.postal_code = postal_code;
    }
    /**
    *   获取 邮政编码
    **/
    public String getPostal_code() {
      return postal_code;
    }



}

