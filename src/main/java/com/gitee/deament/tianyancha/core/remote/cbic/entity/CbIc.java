package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcBranchList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcCapital;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcShareHolderList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcAbnormalList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcEquityList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcChangeList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcIllegalList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcStaffList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcLiquidatingInfo;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcObjectionList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcBriefCancel;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcInvestList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcChangeList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcIprPledgeList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcPunishList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcPawnList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcChangeList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcPeopleList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcMortList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcShareHolderList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcChangeList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcEquityChangeList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcGuaranteeList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcOutBoundList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcSocialList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcWebList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcReportList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcCompanyJudicialAssistanceFrozenInvalidationInfo;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcCompanyJudicialAssistanceFrozenKeepInfo;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcCompanyJudicialAssistanceFrozenRemInfo;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcCompanyJudicialAssistanceFrozenInfo;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcCompanyJudicialShareholderChangeInfo;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcJudicialList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcCheckList;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIcLicenseList;
/**
*工商信息
* 可以通过公司名称或ID获取包含企业简介、企业基本信息、主要人员、股东信息、对外投资、分支机构等维度的相关信息
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIc implements Serializable{

    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    注销日期(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="cancelDate")
    private String cancelDate;
    /**
     *    分支机构
    **/
    @JSONField(name="branchList")
    private List<CbIcBranchList> branchList;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    股东
    **/
    @JSONField(name="shareHolderList")
    private List<CbIcShareHolderList> shareHolderList;
    /**
     *    经营异常列表
    **/
    @JSONField(name="abnormalList")
    private List<CbIcAbnormalList> abnormalList;
    /**
     *    股权出质
    **/
    @JSONField(name="equityList")
    private List<CbIcEquityList> equityList;
    /**
     *    企业变更
    **/
    @JSONField(name="changeList")
    private List<CbIcChangeList> changeList;
    /**
     *    严重违法
    **/
    @JSONField(name="illegalList")
    private List<CbIcIllegalList> illegalList;
    /**
     *    行业(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="industry")
    private String industry;
    /**
     *    吊销日期(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="revokeDate")
    private String revokeDate;
    /**
     *    主要人员列表
    **/
    @JSONField(name="staffList")
    private List<CbIcStaffList> staffList;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    吊销原因(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="revokeReason")
    private String revokeReason;
    /**
     *    法人类型 ，1 人 2 公司
    **/
    @JSONField(name="legalPersonType")
    private Integer legalPersonType;
    /**
     *    注册号
    **/
    @JSONField(name="regNumber")
    private String regNumber;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    英文名
    **/
    @JSONField(name="property3")
    private String property3;
    /**
     *    清算信息
    **/
    @JSONField(name="liquidatingInfo")
    private CbIcLiquidatingInfo liquidatingInfo;
    /**
     *    
    **/
    @JSONField(name="briefCancel")
    private CbIcBriefCancel briefCancel;
    /**
     *    经营开始时间
    **/
    @JSONField(name="fromTime")
    private String fromTime;
    /**
     *    核准时间
    **/
    @JSONField(name="approvedTime")
    private String approvedTime;
    /**
     *    对外投资
    **/
    @JSONField(name="investList")
    private List<CbIcInvestList> investList;
    /**
     *    企业类型
    **/
    @JSONField(name="companyOrgType")
    private String companyOrgType;
    /**
     *    graphId
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    组织机构代码
    **/
    @JSONField(name="orgNumber")
    private String orgNumber;
    /**
     *    注销原因(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="cancelReason")
    private String cancelReason;
    /**
     *    经营结束时间
    **/
    @JSONField(name="toTime")
    private String toTime;
    /**
     *    邮箱
    **/
    @JSONField(name="email")
    private String email;
    /**
     *    
    **/
    @JSONField(name="iprPledgeList")
    private List<CbIcIprPledgeList> iprPledgeList;
    /**
     *    行政处罚
    **/
    @JSONField(name="punishList")
    private List<CbIcPunishList> punishList;
    /**
     *    动产抵押
    **/
    @JSONField(name="mortList")
    private List<CbIcMortList> mortList;
    /**
     *    网址(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="website")
    private String website;
    /**
     *    成立时间
    **/
    @JSONField(name="estiblishTime")
    private String estiblishTime;
    /**
     *    登记机关
    **/
    @JSONField(name="regInstitute")
    private String regInstitute;
    /**
     *    年报
    **/
    @JSONField(name="reportList")
    private List<CbIcReportList> reportList;
    /**
     *    经营范围
    **/
    @JSONField(name="businessScope")
    private String businessScope;
    /**
     *    纳税人识别号(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="taxNumber")
    private String taxNumber;
    /**
     *    注册地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    司法协助
    **/
    @JSONField(name="judicialList")
    private List<CbIcJudicialList> judicialList;
    /**
     *    company 的id(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="companyId")
    private String companyId;
    /**
     *    联系方式
    **/
    @JSONField(name="phoneNumber")
    private String phoneNumber;
    /**
     *    公司名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    抽查检查
    **/
    @JSONField(name="checkList")
    private List<CbIcCheckList> checkList;
    /**
     *    行政许可
    **/
    @JSONField(name="licenseList")
    private List<CbIcLicenseList> licenseList;
    /**
     *    更新时间
    **/
    @JSONField(name="updatetime")
    private String updatetime;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 注销日期(官方未定义字段类型 暂定为String)
    **/
    public void setCancelDate(String cancelDate) {
      this.cancelDate = cancelDate;
    }
    /**
    *   获取 注销日期(官方未定义字段类型 暂定为String)
    **/
    public String getCancelDate() {
      return cancelDate;
    }
    /**
    *   设置 分支机构
    **/
    public void setBranchList(List<CbIcBranchList> branchList) {
      this.branchList = branchList;
    }
    /**
    *   获取 分支机构
    **/
    public List<CbIcBranchList> getBranchList() {
      return branchList;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 股东
    **/
    public void setShareHolderList(List<CbIcShareHolderList> shareHolderList) {
      this.shareHolderList = shareHolderList;
    }
    /**
    *   获取 股东
    **/
    public List<CbIcShareHolderList> getShareHolderList() {
      return shareHolderList;
    }
    /**
    *   设置 经营异常列表
    **/
    public void setAbnormalList(List<CbIcAbnormalList> abnormalList) {
      this.abnormalList = abnormalList;
    }
    /**
    *   获取 经营异常列表
    **/
    public List<CbIcAbnormalList> getAbnormalList() {
      return abnormalList;
    }
    /**
    *   设置 股权出质
    **/
    public void setEquityList(List<CbIcEquityList> equityList) {
      this.equityList = equityList;
    }
    /**
    *   获取 股权出质
    **/
    public List<CbIcEquityList> getEquityList() {
      return equityList;
    }
    /**
    *   设置 企业变更
    **/
    public void setChangeList(List<CbIcChangeList> changeList) {
      this.changeList = changeList;
    }
    /**
    *   获取 企业变更
    **/
    public List<CbIcChangeList> getChangeList() {
      return changeList;
    }
    /**
    *   设置 严重违法
    **/
    public void setIllegalList(List<CbIcIllegalList> illegalList) {
      this.illegalList = illegalList;
    }
    /**
    *   获取 严重违法
    **/
    public List<CbIcIllegalList> getIllegalList() {
      return illegalList;
    }
    /**
    *   设置 行业(官方未定义字段类型 暂定为String)
    **/
    public void setIndustry(String industry) {
      this.industry = industry;
    }
    /**
    *   获取 行业(官方未定义字段类型 暂定为String)
    **/
    public String getIndustry() {
      return industry;
    }
    /**
    *   设置 吊销日期(官方未定义字段类型 暂定为String)
    **/
    public void setRevokeDate(String revokeDate) {
      this.revokeDate = revokeDate;
    }
    /**
    *   获取 吊销日期(官方未定义字段类型 暂定为String)
    **/
    public String getRevokeDate() {
      return revokeDate;
    }
    /**
    *   设置 主要人员列表
    **/
    public void setStaffList(List<CbIcStaffList> staffList) {
      this.staffList = staffList;
    }
    /**
    *   获取 主要人员列表
    **/
    public List<CbIcStaffList> getStaffList() {
      return staffList;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 吊销原因(官方未定义字段类型 暂定为String)
    **/
    public void setRevokeReason(String revokeReason) {
      this.revokeReason = revokeReason;
    }
    /**
    *   获取 吊销原因(官方未定义字段类型 暂定为String)
    **/
    public String getRevokeReason() {
      return revokeReason;
    }
    /**
    *   设置 法人类型 ，1 人 2 公司
    **/
    public void setLegalPersonType(Integer legalPersonType) {
      this.legalPersonType = legalPersonType;
    }
    /**
    *   获取 法人类型 ，1 人 2 公司
    **/
    public Integer getLegalPersonType() {
      return legalPersonType;
    }
    /**
    *   设置 注册号
    **/
    public void setRegNumber(String regNumber) {
      this.regNumber = regNumber;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNumber() {
      return regNumber;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 英文名
    **/
    public void setProperty3(String property3) {
      this.property3 = property3;
    }
    /**
    *   获取 英文名
    **/
    public String getProperty3() {
      return property3;
    }
    /**
    *   设置 清算信息
    **/
    public void setLiquidatingInfo(CbIcLiquidatingInfo liquidatingInfo) {
      this.liquidatingInfo = liquidatingInfo;
    }
    /**
    *   获取 清算信息
    **/
    public CbIcLiquidatingInfo getLiquidatingInfo() {
      return liquidatingInfo;
    }
    /**
    *   设置 
    **/
    public void setBriefCancel(CbIcBriefCancel briefCancel) {
      this.briefCancel = briefCancel;
    }
    /**
    *   获取 
    **/
    public CbIcBriefCancel getBriefCancel() {
      return briefCancel;
    }
    /**
    *   设置 经营开始时间
    **/
    public void setFromTime(String fromTime) {
      this.fromTime = fromTime;
    }
    /**
    *   获取 经营开始时间
    **/
    public String getFromTime() {
      return fromTime;
    }
    /**
    *   设置 核准时间
    **/
    public void setApprovedTime(String approvedTime) {
      this.approvedTime = approvedTime;
    }
    /**
    *   获取 核准时间
    **/
    public String getApprovedTime() {
      return approvedTime;
    }
    /**
    *   设置 对外投资
    **/
    public void setInvestList(List<CbIcInvestList> investList) {
      this.investList = investList;
    }
    /**
    *   获取 对外投资
    **/
    public List<CbIcInvestList> getInvestList() {
      return investList;
    }
    /**
    *   设置 企业类型
    **/
    public void setCompanyOrgType(String companyOrgType) {
      this.companyOrgType = companyOrgType;
    }
    /**
    *   获取 企业类型
    **/
    public String getCompanyOrgType() {
      return companyOrgType;
    }
    /**
    *   设置 graphId
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 graphId
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 组织机构代码
    **/
    public void setOrgNumber(String orgNumber) {
      this.orgNumber = orgNumber;
    }
    /**
    *   获取 组织机构代码
    **/
    public String getOrgNumber() {
      return orgNumber;
    }
    /**
    *   设置 注销原因(官方未定义字段类型 暂定为String)
    **/
    public void setCancelReason(String cancelReason) {
      this.cancelReason = cancelReason;
    }
    /**
    *   获取 注销原因(官方未定义字段类型 暂定为String)
    **/
    public String getCancelReason() {
      return cancelReason;
    }
    /**
    *   设置 经营结束时间
    **/
    public void setToTime(String toTime) {
      this.toTime = toTime;
    }
    /**
    *   获取 经营结束时间
    **/
    public String getToTime() {
      return toTime;
    }
    /**
    *   设置 邮箱
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱
    **/
    public String getEmail() {
      return email;
    }
    /**
    *   设置 
    **/
    public void setIprPledgeList(List<CbIcIprPledgeList> iprPledgeList) {
      this.iprPledgeList = iprPledgeList;
    }
    /**
    *   获取 
    **/
    public List<CbIcIprPledgeList> getIprPledgeList() {
      return iprPledgeList;
    }
    /**
    *   设置 行政处罚
    **/
    public void setPunishList(List<CbIcPunishList> punishList) {
      this.punishList = punishList;
    }
    /**
    *   获取 行政处罚
    **/
    public List<CbIcPunishList> getPunishList() {
      return punishList;
    }
    /**
    *   设置 动产抵押
    **/
    public void setMortList(List<CbIcMortList> mortList) {
      this.mortList = mortList;
    }
    /**
    *   获取 动产抵押
    **/
    public List<CbIcMortList> getMortList() {
      return mortList;
    }
    /**
    *   设置 网址(官方未定义字段类型 暂定为String)
    **/
    public void setWebsite(String website) {
      this.website = website;
    }
    /**
    *   获取 网址(官方未定义字段类型 暂定为String)
    **/
    public String getWebsite() {
      return website;
    }
    /**
    *   设置 成立时间
    **/
    public void setEstiblishTime(String estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立时间
    **/
    public String getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 登记机关
    **/
    public void setRegInstitute(String regInstitute) {
      this.regInstitute = regInstitute;
    }
    /**
    *   获取 登记机关
    **/
    public String getRegInstitute() {
      return regInstitute;
    }
    /**
    *   设置 年报
    **/
    public void setReportList(List<CbIcReportList> reportList) {
      this.reportList = reportList;
    }
    /**
    *   获取 年报
    **/
    public List<CbIcReportList> getReportList() {
      return reportList;
    }
    /**
    *   设置 经营范围
    **/
    public void setBusinessScope(String businessScope) {
      this.businessScope = businessScope;
    }
    /**
    *   获取 经营范围
    **/
    public String getBusinessScope() {
      return businessScope;
    }
    /**
    *   设置 纳税人识别号(官方未定义字段类型 暂定为String)
    **/
    public void setTaxNumber(String taxNumber) {
      this.taxNumber = taxNumber;
    }
    /**
    *   获取 纳税人识别号(官方未定义字段类型 暂定为String)
    **/
    public String getTaxNumber() {
      return taxNumber;
    }
    /**
    *   设置 注册地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 注册地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 司法协助
    **/
    public void setJudicialList(List<CbIcJudicialList> judicialList) {
      this.judicialList = judicialList;
    }
    /**
    *   获取 司法协助
    **/
    public List<CbIcJudicialList> getJudicialList() {
      return judicialList;
    }
    /**
    *   设置 company 的id(官方未定义字段类型 暂定为String)
    **/
    public void setCompanyId(String companyId) {
      this.companyId = companyId;
    }
    /**
    *   获取 company 的id(官方未定义字段类型 暂定为String)
    **/
    public String getCompanyId() {
      return companyId;
    }
    /**
    *   设置 联系方式
    **/
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    /**
    *   获取 联系方式
    **/
    public String getPhoneNumber() {
      return phoneNumber;
    }
    /**
    *   设置 公司名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 抽查检查
    **/
    public void setCheckList(List<CbIcCheckList> checkList) {
      this.checkList = checkList;
    }
    /**
    *   获取 抽查检查
    **/
    public List<CbIcCheckList> getCheckList() {
      return checkList;
    }
    /**
    *   设置 行政许可
    **/
    public void setLicenseList(List<CbIcLicenseList> licenseList) {
      this.licenseList = licenseList;
    }
    /**
    *   获取 行政许可
    **/
    public List<CbIcLicenseList> getLicenseList() {
      return licenseList;
    }
    /**
    *   设置 更新时间
    **/
    public void setUpdatetime(String updatetime) {
      this.updatetime = updatetime;
    }
    /**
    *   获取 更新时间
    **/
    public String getUpdatetime() {
      return updatetime;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }



}

