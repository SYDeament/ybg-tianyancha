package com.gitee.deament.tianyancha.core.remote.oiteam.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oiteam.entity.OiTeamItems;
/**
*机构成员
* 可以通过投资机构名称获取机构成员信息，机构成员信息包括成员姓名、职位、简介等字段的详细信息
* /services/open/oi/team/2.0
*@author deament
**/

public class OiTeam implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiTeamItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiTeamItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiTeamItems> getItems() {
      return items;
    }



}

