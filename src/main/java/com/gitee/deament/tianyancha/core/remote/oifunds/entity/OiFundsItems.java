package com.gitee.deament.tianyancha.core.remote.oifunds.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*管理基金
* 属于OiFunds
* /services/open/oi/funds/2.0
*@author deament
**/

public class OiFundsItems implements Serializable{

    /**
     *    法人id
    **/
    @JSONField(name="legal_person_id")
    private Integer legal_person_id;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblish_date")
    private String estiblish_date;
    /**
     *    法人类型 1-人 2-公司
    **/
    @JSONField(name="legal_person_type")
    private Integer legal_person_type;
    /**
     *    企业logo
    **/
    @JSONField(name="company_logo")
    private String company_logo;
    /**
     *    法人
    **/
    @JSONField(name="legal_person_name")
    private String legal_person_name;
    /**
     *    企业名
    **/
    @JSONField(name="company_name")
    private String company_name;
    /**
     *    法人logo
    **/
    @JSONField(name="legalLogo")
    private String legalLogo;
    /**
     *    企业简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    注册资金
    **/
    @JSONField(name="reg_capital")
    private String reg_capital;


    /**
    *   设置 法人id
    **/
    public void setLegal_person_id(Integer legal_person_id) {
      this.legal_person_id = legal_person_id;
    }
    /**
    *   获取 法人id
    **/
    public Integer getLegal_person_id() {
      return legal_person_id;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblish_date(String estiblish_date) {
      this.estiblish_date = estiblish_date;
    }
    /**
    *   获取 成立日期
    **/
    public String getEstiblish_date() {
      return estiblish_date;
    }
    /**
    *   设置 法人类型 1-人 2-公司
    **/
    public void setLegal_person_type(Integer legal_person_type) {
      this.legal_person_type = legal_person_type;
    }
    /**
    *   获取 法人类型 1-人 2-公司
    **/
    public Integer getLegal_person_type() {
      return legal_person_type;
    }
    /**
    *   设置 企业logo
    **/
    public void setCompany_logo(String company_logo) {
      this.company_logo = company_logo;
    }
    /**
    *   获取 企业logo
    **/
    public String getCompany_logo() {
      return company_logo;
    }
    /**
    *   设置 法人
    **/
    public void setLegal_person_name(String legal_person_name) {
      this.legal_person_name = legal_person_name;
    }
    /**
    *   获取 法人
    **/
    public String getLegal_person_name() {
      return legal_person_name;
    }
    /**
    *   设置 企业名
    **/
    public void setCompany_name(String company_name) {
      this.company_name = company_name;
    }
    /**
    *   获取 企业名
    **/
    public String getCompany_name() {
      return company_name;
    }
    /**
    *   设置 法人logo
    **/
    public void setLegalLogo(String legalLogo) {
      this.legalLogo = legalLogo;
    }
    /**
    *   获取 法人logo
    **/
    public String getLegalLogo() {
      return legalLogo;
    }
    /**
    *   设置 企业简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 企业简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 注册资金
    **/
    public void setReg_capital(String reg_capital) {
      this.reg_capital = reg_capital;
    }
    /**
    *   获取 注册资金
    **/
    public String getReg_capital() {
      return reg_capital;
    }



}

