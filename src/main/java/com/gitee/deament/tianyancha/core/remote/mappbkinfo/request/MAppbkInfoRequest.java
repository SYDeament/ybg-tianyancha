package com.gitee.deament.tianyancha.core.remote.mappbkinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mappbkinfo.entity.MAppbkInfo;
import com.gitee.deament.tianyancha.core.remote.mappbkinfo.dto.MAppbkInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*产品信息
* 可以通过公司名称或ID获取企业产品信息，企业产品信息包括主要产品、产品分类、产品介绍等字段的详细信息
* /services/open/m/appbkInfo/2.0
*@author deament
**/

public interface MAppbkInfoRequest extends BaseRequest<MAppbkInfoDTO ,MAppbkInfo>{

}

