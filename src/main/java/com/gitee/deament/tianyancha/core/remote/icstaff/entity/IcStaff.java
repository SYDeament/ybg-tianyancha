package com.gitee.deament.tianyancha.core.remote.icstaff.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icstaff.entity.IcStaffItems;
/**
*主要人员
* 可以通过公司名称或ID获取企业主要人员信息，主要人员信息包括董事、监事、高级管理人员姓名、职位、主要人员总数等字段的详细信息
* /services/open/ic/staff/2.0
*@author deament
**/

public class IcStaff implements Serializable{

    /**
     *    主要人员总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcStaffItems> items;


    /**
    *   设置 主要人员总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 主要人员总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcStaffItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcStaffItems> getItems() {
      return items;
    }



}

