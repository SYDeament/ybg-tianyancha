package com.gitee.deament.tianyancha.core.remote.icholder.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业股东
* 属于IcHolder
* /services/open/ic/holder/2.0
*@author deament
**/

public class IcHolderItems implements Serializable{

    /**
     *    认缴
    **/
    @JSONField(name="capital")
    private List<IcHolderCapital> capital;
    /**
     *    股东名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    实缴
    **/
    @JSONField(name="capitalActl")
    private List<IcHolderCapitalActl> capitalActl;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    1-公司  2-人 3-其它
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 认缴
    **/
    public void setCapital(List<IcHolderCapital> capital) {
      this.capital = capital;
    }
    /**
    *   获取 认缴
    **/
    public List<IcHolderCapital> getCapital() {
      return capital;
    }
    /**
    *   设置 股东名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 股东名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 实缴
    **/
    public void setCapitalActl(List<IcHolderCapitalActl> capitalActl) {
      this.capitalActl = capitalActl;
    }
    /**
    *   获取 实缴
    **/
    public List<IcHolderCapitalActl> getCapitalActl() {
      return capitalActl;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 1-公司  2-人 3-其它
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司  2-人 3-其它
    **/
    public Integer getType() {
      return type;
    }



}

