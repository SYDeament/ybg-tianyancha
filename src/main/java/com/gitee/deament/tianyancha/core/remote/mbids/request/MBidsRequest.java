package com.gitee.deament.tianyancha.core.remote.mbids.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mbids.entity.MBids;
import com.gitee.deament.tianyancha.core.remote.mbids.dto.MBidsDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*招投标
* 可以通过公司名称或ID获企业招投标信息，招投标信息包括采购人、发布时间、正文信息等字段的详细信息
* /services/open/m/bids/2.0
*@author deament
**/

public interface MBidsRequest extends BaseRequest<MBidsDTO ,MBids>{

}

