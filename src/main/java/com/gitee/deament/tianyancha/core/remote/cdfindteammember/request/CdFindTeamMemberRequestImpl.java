package com.gitee.deament.tianyancha.core.remote.cdfindteammember.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdfindteammember.entity.CdFindTeamMember;
import com.gitee.deament.tianyancha.core.remote.cdfindteammember.dto.CdFindTeamMemberDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*核心团队
* 可以通过公司名称或ID获取企业核心团队信息，企业核心团队信息包括核心人员姓名、职位、过往经历简介等字段的详细信息
* /services/open/cd/findTeamMember/2.0
*@author deament
**/
@Component("cdFindTeamMemberRequestImpl")
public class CdFindTeamMemberRequestImpl extends BaseRequestImpl<CdFindTeamMember,CdFindTeamMemberDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cd/findTeamMember/2.0";
    }
}

