package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype31 implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private RiskDetailResult result;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="announcement")
    private RiskDetailAnnouncement announcement;
    /**
     *    
    **/
    @JSONField(name="objection")
    private RiskDetailObjection objection;


    /**
    *   设置 
    **/
    public void setResult(RiskDetailResult result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public RiskDetailResult getResult() {
      return result;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setAnnouncement(RiskDetailAnnouncement announcement) {
      this.announcement = announcement;
    }
    /**
    *   获取 
    **/
    public RiskDetailAnnouncement getAnnouncement() {
      return announcement;
    }
    /**
    *   设置 
    **/
    public void setObjection(RiskDetailObjection objection) {
      this.objection = objection;
    }
    /**
    *   获取 
    **/
    public RiskDetailObjection getObjection() {
      return objection;
    }



}

