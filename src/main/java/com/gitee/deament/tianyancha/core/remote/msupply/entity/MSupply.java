package com.gitee.deament.tianyancha.core.remote.msupply.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.msupply.entity.MSupplySuppliesYear;
import com.gitee.deament.tianyancha.core.remote.msupply.entity.MSupplyResult;
import com.gitee.deament.tianyancha.core.remote.msupply.entity.MSupplyPageBean;
/**
*供应商
* 可以通过公司名称或ID获取企业相关供应商信息，企业相关供应商信息包括供应商、采购占比、采购金额、报告期等字段的详细信息
* /services/open/m/supply/2.0
*@author deament
**/

public class MSupply implements Serializable{

    /**
     *    
    **/
    @JSONField(name="suppliesYear")
    private List<MSupplySuppliesYear> suppliesYear;
    /**
     *    供应商列表
    **/
    @JSONField(name="pageBean")
    private MSupplyPageBean pageBean;


    /**
    *   设置 
    **/
    public void setSuppliesYear(List<MSupplySuppliesYear> suppliesYear) {
      this.suppliesYear = suppliesYear;
    }
    /**
    *   获取 
    **/
    public List<MSupplySuppliesYear> getSuppliesYear() {
      return suppliesYear;
    }
    /**
    *   设置 供应商列表
    **/
    public void setPageBean(MSupplyPageBean pageBean) {
      this.pageBean = pageBean;
    }
    /**
    *   获取 供应商列表
    **/
    public MSupplyPageBean getPageBean() {
      return pageBean;
    }



}

