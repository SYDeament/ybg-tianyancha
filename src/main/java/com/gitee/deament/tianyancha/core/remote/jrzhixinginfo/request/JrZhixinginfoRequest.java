package com.gitee.deament.tianyancha.core.remote.jrzhixinginfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrzhixinginfo.entity.JrZhixinginfo;
import com.gitee.deament.tianyancha.core.remote.jrzhixinginfo.dto.JrZhixinginfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*被执行人
* 可以通过公司名称或ID获取被执行人信息，被执行人信息包括执行法院、案件内容、执行标的、被执行人名称、组织机构代码等字段的详细信息
* /services/open/jr/zhixinginfo/2.0
*@author deament
**/

public interface JrZhixinginfoRequest extends BaseRequest<JrZhixinginfoDTO ,JrZhixinginfo>{

}

