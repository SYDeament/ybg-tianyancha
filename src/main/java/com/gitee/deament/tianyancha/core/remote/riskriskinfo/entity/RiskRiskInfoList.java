package com.gitee.deament.tianyancha.core.remote.riskriskinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业天眼风险
* 属于RiskRiskInfo
* /services/open/risk/riskInfo/2.0
*@author deament
**/

public class RiskRiskInfoList implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    风险标签（警示信息，高风险信息，提示信息）
    **/
    @JSONField(name="tag")
    private String tag;
    /**
     *    
    **/
    @JSONField(name="list")
    private List<RiskRiskInfoList> list;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    1-严重违法，3-失信（人）公司，5-被执行（人）公司，6-行政处罚，7-经营异常，8-法律诉讼，9-股权出质，10-动产抵押，11-欠税公告，12-名称变更，13-开庭公告，14-法院公告，15-法人变更，16-投资人变更，17-主要人员变更，18-注册资本变更，19-注册地址变更，20-出资情况变更，21-司法协助，22-清算信息，23-知识产权出质，24-环保处罚，25-公示催告，26-送达公告，27-立案信息，28-税收违法，29-司法拍卖，30-土地抵押，31-简易注销，32-限制消费令（公司），33-限制消费令（人），34-终本案件
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 风险标签（警示信息，高风险信息，提示信息）
    **/
    public void setTag(String tag) {
      this.tag = tag;
    }
    /**
    *   获取 风险标签（警示信息，高风险信息，提示信息）
    **/
    public String getTag() {
      return tag;
    }
    /**
    *   设置 
    **/
    public void setList(List<RiskRiskInfoList> list) {
      this.list = list;
    }
    /**
    *   获取 
    **/
    public List<RiskRiskInfoList> getList() {
      return list;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 1-严重违法，3-失信（人）公司，5-被执行（人）公司，6-行政处罚，7-经营异常，8-法律诉讼，9-股权出质，10-动产抵押，11-欠税公告，12-名称变更，13-开庭公告，14-法院公告，15-法人变更，16-投资人变更，17-主要人员变更，18-注册资本变更，19-注册地址变更，20-出资情况变更，21-司法协助，22-清算信息，23-知识产权出质，24-环保处罚，25-公示催告，26-送达公告，27-立案信息，28-税收违法，29-司法拍卖，30-土地抵押，31-简易注销，32-限制消费令（公司），33-限制消费令（人），34-终本案件
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-严重违法，3-失信（人）公司，5-被执行（人）公司，6-行政处罚，7-经营异常，8-法律诉讼，9-股权出质，10-动产抵押，11-欠税公告，12-名称变更，13-开庭公告，14-法院公告，15-法人变更，16-投资人变更，17-主要人员变更，18-注册资本变更，19-注册地址变更，20-出资情况变更，21-司法协助，22-清算信息，23-知识产权出质，24-环保处罚，25-公示催告，26-送达公告，27-立案信息，28-税收违法，29-司法拍卖，30-土地抵押，31-简易注销，32-限制消费令（公司），33-限制消费令（人），34-终本案件
    **/
    public Integer getType() {
      return type;
    }



}

