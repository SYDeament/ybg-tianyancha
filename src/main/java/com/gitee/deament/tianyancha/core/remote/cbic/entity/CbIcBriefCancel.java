package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcBriefCancel implements Serializable{

    /**
     *    登记机关
    **/
    @JSONField(name="reg_authority")
    private String reg_authority;
    /**
     *    异议
    **/
    @JSONField(name="objectionList")
    private List<CbIcObjectionList> objectionList;
    /**
     *    公告申请日期
    **/
    @JSONField(name="announcement_apply_date")
    private String announcement_apply_date;
    /**
     *    注册号
    **/
    @JSONField(name="reg_num")
    private String reg_num;
    /**
     *    公司名
    **/
    @JSONField(name="company_name")
    private String company_name;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="credit_code")
    private String credit_code;
    /**
     *    公告id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    简易注销结果
    **/
    @JSONField(name="brief_cancel_result")
    private String brief_cancel_result;
    /**
     *    公告期
    **/
    @JSONField(name="announcement_term")
    private String announcement_term;
    /**
     *    原链接
    **/
    @JSONField(name="investor_commitment_download_url")
    private String investor_commitment_download_url;
    /**
     *    公告结束日期
    **/
    @JSONField(name="announcement_end_date")
    private String announcement_end_date;
    /**
     *    oss路径
    **/
    @JSONField(name="ossPath")
    private String ossPath;


    /**
    *   设置 登记机关
    **/
    public void setReg_authority(String reg_authority) {
      this.reg_authority = reg_authority;
    }
    /**
    *   获取 登记机关
    **/
    public String getReg_authority() {
      return reg_authority;
    }
    /**
    *   设置 异议
    **/
    public void setObjectionList(List<CbIcObjectionList> objectionList) {
      this.objectionList = objectionList;
    }
    /**
    *   获取 异议
    **/
    public List<CbIcObjectionList> getObjectionList() {
      return objectionList;
    }
    /**
    *   设置 公告申请日期
    **/
    public void setAnnouncement_apply_date(String announcement_apply_date) {
      this.announcement_apply_date = announcement_apply_date;
    }
    /**
    *   获取 公告申请日期
    **/
    public String getAnnouncement_apply_date() {
      return announcement_apply_date;
    }
    /**
    *   设置 注册号
    **/
    public void setReg_num(String reg_num) {
      this.reg_num = reg_num;
    }
    /**
    *   获取 注册号
    **/
    public String getReg_num() {
      return reg_num;
    }
    /**
    *   设置 公司名
    **/
    public void setCompany_name(String company_name) {
      this.company_name = company_name;
    }
    /**
    *   获取 公司名
    **/
    public String getCompany_name() {
      return company_name;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCredit_code(String credit_code) {
      this.credit_code = credit_code;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCredit_code() {
      return credit_code;
    }
    /**
    *   设置 公告id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公告id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 简易注销结果
    **/
    public void setBrief_cancel_result(String brief_cancel_result) {
      this.brief_cancel_result = brief_cancel_result;
    }
    /**
    *   获取 简易注销结果
    **/
    public String getBrief_cancel_result() {
      return brief_cancel_result;
    }
    /**
    *   设置 公告期
    **/
    public void setAnnouncement_term(String announcement_term) {
      this.announcement_term = announcement_term;
    }
    /**
    *   获取 公告期
    **/
    public String getAnnouncement_term() {
      return announcement_term;
    }
    /**
    *   设置 原链接
    **/
    public void setInvestor_commitment_download_url(String investor_commitment_download_url) {
      this.investor_commitment_download_url = investor_commitment_download_url;
    }
    /**
    *   获取 原链接
    **/
    public String getInvestor_commitment_download_url() {
      return investor_commitment_download_url;
    }
    /**
    *   设置 公告结束日期
    **/
    public void setAnnouncement_end_date(String announcement_end_date) {
      this.announcement_end_date = announcement_end_date;
    }
    /**
    *   获取 公告结束日期
    **/
    public String getAnnouncement_end_date() {
      return announcement_end_date;
    }
    /**
    *   设置 oss路径
    **/
    public void setOssPath(String ossPath) {
      this.ossPath = ossPath;
    }
    /**
    *   获取 oss路径
    **/
    public String getOssPath() {
      return ossPath;
    }



}

