package com.gitee.deament.tianyancha.core.remote.mcustomer.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcustomer.entity.MCustomer;
import com.gitee.deament.tianyancha.core.remote.mcustomer.dto.MCustomerDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*客户
* 可以通过公司名称或ID获取企业相关客户信息，企业相关客户信息包括客户、销售占比、销售金额、报告期等字段的详细信息
* /services/open/m/customer/2.0
*@author deament
**/
@Component("mCustomerRequestImpl")
public class MCustomerRequestImpl extends BaseRequestImpl<MCustomer,MCustomerDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/m/customer/2.0";
    }
}

