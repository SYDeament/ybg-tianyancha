package com.gitee.deament.tianyancha.core.remote.psnews.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.psnews.entity.PsNewsItems;
/**
*新闻舆情
* 根据公司id或公司名称（精确匹配）获取新闻列表
* /services/open/ps/news/2.0
*@author deament
**/

public class PsNews implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<PsNewsItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<PsNewsItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<PsNewsItems> getItems() {
      return items;
    }



}

