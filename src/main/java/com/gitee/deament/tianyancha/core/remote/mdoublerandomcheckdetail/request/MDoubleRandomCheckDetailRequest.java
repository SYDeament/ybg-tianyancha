package com.gitee.deament.tianyancha.core.remote.mdoublerandomcheckdetail.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mdoublerandomcheckdetail.entity.MDoubleRandomCheckDetail;
import com.gitee.deament.tianyancha.core.remote.mdoublerandomcheckdetail.dto.MDoubleRandomCheckDetailDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*双随机抽查详情
* 可以通过双随机抽查ID获取企业双随机抽查详情，企业双随机抽查详情包括检查事项、检查结果等字段的详细信息
* /services/open/m/doubleRandomCheckDetail/2.0
*@author deament
**/

public interface MDoubleRandomCheckDetailRequest extends BaseRequest<MDoubleRandomCheckDetailDTO ,MDoubleRandomCheckDetail>{

}

