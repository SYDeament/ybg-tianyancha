package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype10 implements Serializable{

    /**
     *    
    **/
    @JSONField(name="baseInfo")
    private RiskDetailBaseInfo baseInfo;
    /**
     *    注销信息
    **/
    @JSONField(name="cancelInfo")
    private List<RiskDetailCancelInfo> cancelInfo;
    /**
     *    抵押人信息
    **/
    @JSONField(name="peopleInfo")
    private List<RiskDetailPeopleInfo> peopleInfo;
    /**
     *    抵押物信息
    **/
    @JSONField(name="pawnInfoList")
    private List<RiskDetailPawnInfoList> pawnInfoList;


    /**
    *   设置 
    **/
    public void setBaseInfo(RiskDetailBaseInfo baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 
    **/
    public RiskDetailBaseInfo getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 注销信息
    **/
    public void setCancelInfo(List<RiskDetailCancelInfo> cancelInfo) {
      this.cancelInfo = cancelInfo;
    }
    /**
    *   获取 注销信息
    **/
    public List<RiskDetailCancelInfo> getCancelInfo() {
      return cancelInfo;
    }
    /**
    *   设置 抵押人信息
    **/
    public void setPeopleInfo(List<RiskDetailPeopleInfo> peopleInfo) {
      this.peopleInfo = peopleInfo;
    }
    /**
    *   获取 抵押人信息
    **/
    public List<RiskDetailPeopleInfo> getPeopleInfo() {
      return peopleInfo;
    }
    /**
    *   设置 抵押物信息
    **/
    public void setPawnInfoList(List<RiskDetailPawnInfoList> pawnInfoList) {
      this.pawnInfoList = pawnInfoList;
    }
    /**
    *   获取 抵押物信息
    **/
    public List<RiskDetailPawnInfoList> getPawnInfoList() {
      return pawnInfoList;
    }



}

