package com.gitee.deament.tianyancha.core.remote.onekey.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业图谱
* 属于OneKey
* /services/v4/open/oneKey/c
*@author deament
**/

public class OneKeyNodes implements Serializable{

    /**
     *    节点id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    节点属性
    **/
    @JSONField(name="properties")
    private OneKeyProperties properties;
    /**
     *    节点标签
    **/
    @JSONField(name="labels")
    private List<String> labels;


    /**
    *   设置 节点id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 节点id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 节点属性
    **/
    public void setProperties(OneKeyProperties properties) {
      this.properties = properties;
    }
    /**
    *   获取 节点属性
    **/
    public OneKeyProperties getProperties() {
      return properties;
    }
    /**
    *   设置 节点标签
    **/
    public void setLabels(List<String> labels) {
      this.labels = labels;
    }
    /**
    *   获取 节点标签
    **/
    public List<String> getLabels() {
      return labels;
    }



}

