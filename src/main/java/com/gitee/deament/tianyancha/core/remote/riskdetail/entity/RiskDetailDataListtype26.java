package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype26 implements Serializable{

    /**
     *    公告名称
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    内部使用
    **/
    @JSONField(name="bbusinessId")
    private String bbusinessId;
    /**
     *    公告内容
    **/
    @JSONField(name="content")
    private String content;
    /**
     *    发布日期
    **/
    @JSONField(name="startDate")
    private Long startDate;


    /**
    *   设置 公告名称
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 公告名称
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 内部使用
    **/
    public void setBbusinessId(String bbusinessId) {
      this.bbusinessId = bbusinessId;
    }
    /**
    *   获取 内部使用
    **/
    public String getBbusinessId() {
      return bbusinessId;
    }
    /**
    *   设置 公告内容
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 公告内容
    **/
    public String getContent() {
      return content;
    }
    /**
    *   设置 发布日期
    **/
    public void setStartDate(Long startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 发布日期
    **/
    public Long getStartDate() {
      return startDate;
    }



}

