package com.gitee.deament.tianyancha.core.remote.mcreditrating.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcreditrating.entity.MCreditRatingItems;
/**
*企业信用评级
* 可以通过公司名称或ID获取企业信用评级信息，企业信用评级信息包括评级公司、主体等级、债券信用等级、评级展望、评级时间等字段的信息
* /services/open/m/creditRating/2.0
*@author deament
**/

public class MCreditRating implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MCreditRatingItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MCreditRatingItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MCreditRatingItems> getItems() {
      return items;
    }



}

