package com.gitee.deament.tianyancha.core.remote.cdfindteammember.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdfindteammember.entity.CdFindTeamMemberItems;
/**
*核心团队
* 可以通过公司名称或ID获取企业核心团队信息，企业核心团队信息包括核心人员姓名、职位、过往经历简介等字段的详细信息
* /services/open/cd/findTeamMember/2.0
*@author deament
**/

public class CdFindTeamMember implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CdFindTeamMemberItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CdFindTeamMemberItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CdFindTeamMemberItems> getItems() {
      return items;
    }



}

