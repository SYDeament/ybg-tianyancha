package com.gitee.deament.tianyancha.core.remote.stockissuerelated.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockissuerelated.entity.StockIssueRelated;
import com.gitee.deament.tianyancha.core.remote.stockissuerelated.dto.StockIssueRelatedDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*发行相关
* 可以通过公司名称或ID获取上市公司发行相关信息，发行相关信息包括成立日期、上市日期、发行数量、发行价格等
* /services/open/stock/issueRelated/2.0
*@author deament
**/
@Component("stockIssueRelatedRequestImpl")
public class StockIssueRelatedRequestImpl extends BaseRequestImpl<StockIssueRelated,StockIssueRelatedDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/issueRelated/2.0";
    }
}

