package com.gitee.deament.tianyancha.core.remote.mrbriefcancel.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*简易注销
* 属于MrBriefCancel
* /services/open/mr/briefCancel/2.0
*@author deament
**/

public class MrBriefCancelResult implements Serializable{

    /**
     *    简易注销结果
    **/
    @JSONField(name="brief_cancel_result")
    private String brief_cancel_result;


    /**
    *   设置 简易注销结果
    **/
    public void setBrief_cancel_result(String brief_cancel_result) {
      this.brief_cancel_result = brief_cancel_result;
    }
    /**
    *   获取 简易注销结果
    **/
    public String getBrief_cancel_result() {
      return brief_cancel_result;
    }



}

