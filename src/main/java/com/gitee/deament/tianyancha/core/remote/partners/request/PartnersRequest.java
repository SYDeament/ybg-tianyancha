package com.gitee.deament.tianyancha.core.remote.partners.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.partners.entity.Partners;
import com.gitee.deament.tianyancha.core.remote.partners.dto.PartnersDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*人员所有合作伙伴
* 可以通过公司名称或ID和人名获取人员的所有合作伙伴，包括其合作伙伴的所有相关公司数量、公司名称或ID信息
* /services/v4/open/partners
*@author deament
**/

public interface PartnersRequest extends BaseRequest<PartnersDTO ,Partners>{

}

