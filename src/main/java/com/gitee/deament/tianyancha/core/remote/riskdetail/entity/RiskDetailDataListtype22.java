package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype22 implements Serializable{

    /**
     *    清算组负责人
    **/
    @JSONField(name="manager")
    private String manager;
    /**
     *    清算成员名称
    **/
    @JSONField(name="member")
    private String member;
    /**
     *    人id
    **/
    @JSONField(name="id")
    private Integer id;


    /**
    *   设置 清算组负责人
    **/
    public void setManager(String manager) {
      this.manager = manager;
    }
    /**
    *   获取 清算组负责人
    **/
    public String getManager() {
      return manager;
    }
    /**
    *   设置 清算成员名称
    **/
    public void setMember(String member) {
      this.member = member;
    }
    /**
    *   获取 清算成员名称
    **/
    public String getMember() {
      return member;
    }
    /**
    *   设置 人id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 人id
    **/
    public Integer getId() {
      return id;
    }



}

