package com.gitee.deament.tianyancha.core.remote.stockmainindex.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*主要指标-年度
* 可以通过公司名称或ID获取上市公司年度主要指标数据，年度主要指标数据包括基本每股收益、扣非每股收益、稀释每股收益、每股净资产、每股公积金等
* /services/open/stock/mainIndex/2.0
*@author deament
**/

@TYCURL(value="/services/open/stock/mainIndex/2.0")
public class StockMainIndexDTO implements Serializable{

    /**
     *    企业名
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    企业id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 企业名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 企业id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

