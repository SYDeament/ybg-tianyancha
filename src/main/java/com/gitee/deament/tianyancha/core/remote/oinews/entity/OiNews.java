package com.gitee.deament.tianyancha.core.remote.oinews.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oinews.entity.OiNewsItems;
/**
*相关新闻
* 可以通过投资机构名称获取相关新闻舆情信息，包括发稿媒体、新闻标题、发布时间、网址链接等字段的详细信息
* /services/open/oi/news/2.0
*@author deament
**/

public class OiNews implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiNewsItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiNewsItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiNewsItems> getItems() {
      return items;
    }



}

