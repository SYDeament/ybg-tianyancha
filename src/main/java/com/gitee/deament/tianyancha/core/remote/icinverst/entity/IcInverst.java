package com.gitee.deament.tianyancha.core.remote.icinverst.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icinverst.entity.IcInverstItems;
/**
*对外投资
* 可以通过公司名称或ID获取企业对外投资信息，对外投资信息包括被投资企业、企业法人、投资占比、对外投资总数等字段的详细信息
* /services/open/ic/inverst/2.0
*@author deament
**/

public class IcInverst implements Serializable{

    /**
     *    对外投资总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcInverstItems> items;


    /**
    *   设置 对外投资总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 对外投资总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcInverstItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcInverstItems> getItems() {
      return items;
    }



}

