package com.gitee.deament.tianyancha.core.remote.oistats.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oistats.entity.OiStatsMonth_money;
import com.gitee.deament.tianyancha.core.remote.oistats.entity.OiStatsMonth_count;
import com.gitee.deament.tianyancha.core.remote.oistats.entity.OiStatsRound;
import com.gitee.deament.tianyancha.core.remote.oistats.entity.OiStatsIndustry;
/**
*统计分析
* 可以通过投资机构名称获取投资机构投资统计分析，包括月份、数量、投资规模、投资行业等
* /services/open/oi/stats/2.0
*@author deament
**/

public class OiStats implements Serializable{

    /**
     *    投资规模
    **/
    @JSONField(name="month_money")
    private List<OiStatsMonth_money> month_money;
    /**
     *    年份列表
    **/
    @JSONField(name="year_list")
    private List<Long> year_list;
    /**
     *    月份数量
    **/
    @JSONField(name="month_count")
    private List<OiStatsMonth_count> month_count;
    /**
     *    投资阶段
    **/
    @JSONField(name="round")
    private List<OiStatsRound> round;
    /**
     *    投资行业
    **/
    @JSONField(name="industry")
    private List<OiStatsIndustry> industry;


    /**
    *   设置 投资规模
    **/
    public void setMonth_money(List<OiStatsMonth_money> month_money) {
      this.month_money = month_money;
    }
    /**
    *   获取 投资规模
    **/
    public List<OiStatsMonth_money> getMonth_money() {
      return month_money;
    }
    /**
    *   设置 年份列表
    **/
    public void setYear_list(List<Long> year_list) {
      this.year_list = year_list;
    }
    /**
    *   获取 年份列表
    **/
    public List<Long> getYear_list() {
      return year_list;
    }
    /**
    *   设置 月份数量
    **/
    public void setMonth_count(List<OiStatsMonth_count> month_count) {
      this.month_count = month_count;
    }
    /**
    *   获取 月份数量
    **/
    public List<OiStatsMonth_count> getMonth_count() {
      return month_count;
    }
    /**
    *   设置 投资阶段
    **/
    public void setRound(List<OiStatsRound> round) {
      this.round = round;
    }
    /**
    *   获取 投资阶段
    **/
    public List<OiStatsRound> getRound() {
      return round;
    }
    /**
    *   设置 投资行业
    **/
    public void setIndustry(List<OiStatsIndustry> industry) {
      this.industry = industry;
    }
    /**
    *   获取 投资行业
    **/
    public List<OiStatsIndustry> getIndustry() {
      return industry;
    }



}

