package com.gitee.deament.tianyancha.core.remote.pfmember.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*会员信息
* 可以通过公司名称或ID获取相关私募基金会员信息，包括是否为中国证券投资基金业协会会员、当前会员类型、入会时间等字段的相关信息
* /services/open/pf/member/2.0
*@author deament
**/

public class PfMember implements Serializable{

    /**
     *    当前会员类型
    **/
    @JSONField(name="vip_type")
    private String vip_type;
    /**
     *    是否为会员（中国证券投资基金业协会）
    **/
    @JSONField(name="is_vip")
    private String is_vip;
    /**
     *    入会时间
    **/
    @JSONField(name="become_vip_date")
    private String become_vip_date;
    /**
     *    企业id
    **/
    @JSONField(name="graph_id")
    private Integer graph_id;


    /**
    *   设置 当前会员类型
    **/
    public void setVip_type(String vip_type) {
      this.vip_type = vip_type;
    }
    /**
    *   获取 当前会员类型
    **/
    public String getVip_type() {
      return vip_type;
    }
    /**
    *   设置 是否为会员（中国证券投资基金业协会）
    **/
    public void setIs_vip(String is_vip) {
      this.is_vip = is_vip;
    }
    /**
    *   获取 是否为会员（中国证券投资基金业协会）
    **/
    public String getIs_vip() {
      return is_vip;
    }
    /**
    *   设置 入会时间
    **/
    public void setBecome_vip_date(String become_vip_date) {
      this.become_vip_date = become_vip_date;
    }
    /**
    *   获取 入会时间
    **/
    public String getBecome_vip_date() {
      return become_vip_date;
    }
    /**
    *   设置 企业id
    **/
    public void setGraph_id(Integer graph_id) {
      this.graph_id = graph_id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getGraph_id() {
      return graph_id;
    }



}

