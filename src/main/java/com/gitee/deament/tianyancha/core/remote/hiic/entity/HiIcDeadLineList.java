package com.gitee.deament.tianyancha.core.remote.hiic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史工商信息
* 属于HiIc
* /services/open/hi/ic/2.0
*@author deament
**/

public class HiIcDeadLineList implements Serializable{

    /**
     *    无实际意义
    **/
    @JSONField(name="toco")
    private Integer toco;
    /**
     *    变更前内容
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    变更日期
    **/
    @JSONField(name="time")
    private Long time;
    /**
     *    默认值0
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    无实际意义
    **/
    @JSONField(name="relation")
    private Integer relation;


    /**
    *   设置 无实际意义
    **/
    public void setToco(Integer toco) {
      this.toco = toco;
    }
    /**
    *   获取 无实际意义
    **/
    public Integer getToco() {
      return toco;
    }
    /**
    *   设置 变更前内容
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 变更前内容
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 变更日期
    **/
    public void setTime(Long time) {
      this.time = time;
    }
    /**
    *   获取 变更日期
    **/
    public Long getTime() {
      return time;
    }
    /**
    *   设置 默认值0
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 默认值0
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 无实际意义
    **/
    public void setRelation(Integer relation) {
      this.relation = relation;
    }
    /**
    *   获取 无实际意义
    **/
    public Integer getRelation() {
      return relation;
    }



}

