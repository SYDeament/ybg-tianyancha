package com.gitee.deament.tianyancha.core.remote.cdgetproductinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业业务
* 属于CdGetProductInfo
* /services/open/cd/getProductInfo/2.0
*@author deament
**/

public class CdGetProductInfoItems implements Serializable{

    /**
     *    产品名
    **/
    @JSONField(name="product")
    private String product;
    /**
     *    成立时间
    **/
    @JSONField(name="setupDate")
    private Long setupDate;
    /**
     *    业务范围
    **/
    @JSONField(name="yewu")
    private String yewu;
    /**
     *    融资轮次
    **/
    @JSONField(name="round")
    private String round;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    logo存放位置
    **/
    @JSONField(name="logoOssPath")
    private String logoOssPath;
    /**
     *    公司id
    **/
    @JSONField(name="graphId")
    private Integer graphId;
    /**
     *    行业
    **/
    @JSONField(name="hangye")
    private String hangye;


    /**
    *   设置 产品名
    **/
    public void setProduct(String product) {
      this.product = product;
    }
    /**
    *   获取 产品名
    **/
    public String getProduct() {
      return product;
    }
    /**
    *   设置 成立时间
    **/
    public void setSetupDate(Long setupDate) {
      this.setupDate = setupDate;
    }
    /**
    *   获取 成立时间
    **/
    public Long getSetupDate() {
      return setupDate;
    }
    /**
    *   设置 业务范围
    **/
    public void setYewu(String yewu) {
      this.yewu = yewu;
    }
    /**
    *   获取 业务范围
    **/
    public String getYewu() {
      return yewu;
    }
    /**
    *   设置 融资轮次
    **/
    public void setRound(String round) {
      this.round = round;
    }
    /**
    *   获取 融资轮次
    **/
    public String getRound() {
      return round;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 logo存放位置
    **/
    public void setLogoOssPath(String logoOssPath) {
      this.logoOssPath = logoOssPath;
    }
    /**
    *   获取 logo存放位置
    **/
    public String getLogoOssPath() {
      return logoOssPath;
    }
    /**
    *   设置 公司id
    **/
    public void setGraphId(Integer graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getGraphId() {
      return graphId;
    }
    /**
    *   设置 行业
    **/
    public void setHangye(String hangye) {
      this.hangye = hangye;
    }
    /**
    *   获取 行业
    **/
    public String getHangye() {
      return hangye;
    }



}

