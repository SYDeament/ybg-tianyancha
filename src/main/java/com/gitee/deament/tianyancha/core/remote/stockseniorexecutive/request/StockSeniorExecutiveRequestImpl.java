package com.gitee.deament.tianyancha.core.remote.stockseniorexecutive.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockseniorexecutive.entity.StockSeniorExecutive;
import com.gitee.deament.tianyancha.core.remote.stockseniorexecutive.dto.StockSeniorExecutiveDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*高管信息
* 可以通过公司名称或ID获取上市公司高管信息，上市公司高管信息包括高管姓名、职务、持股数、年龄、学历等
* /services/open/stock/seniorExecutive/2.0
*@author deament
**/
@Component("stockSeniorExecutiveRequestImpl")
public class StockSeniorExecutiveRequestImpl extends BaseRequestImpl<StockSeniorExecutive,StockSeniorExecutiveDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/seniorExecutive/2.0";
    }
}

