package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryChangeList implements Serializable{

    /**
     *    变更内容
    **/
    @JSONField(name="changeContent")
    private String changeContent;
    /**
     *    变更日期
    **/
    @JSONField(name="changeDate")
    private String changeDate;


    /**
    *   设置 变更内容
    **/
    public void setChangeContent(String changeContent) {
      this.changeContent = changeContent;
    }
    /**
    *   获取 变更内容
    **/
    public String getChangeContent() {
      return changeContent;
    }
    /**
    *   设置 变更日期
    **/
    public void setChangeDate(String changeDate) {
      this.changeDate = changeDate;
    }
    /**
    *   获取 变更日期
    **/
    public String getChangeDate() {
      return changeDate;
    }



}

