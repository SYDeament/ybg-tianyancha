package com.gitee.deament.tianyancha.core.base;

import com.gitee.deament.tianyancha.core.TianyanchaResult;
import org.springframework.http.HttpMethod;

/**
 * 请求类
 * @author deament
 * @param <VO> 返回类
 * @param <DTO> 请求参数
 */
public interface BaseRequest<VO,DTO> {
    /**Get请求方式**/
    public TianyanchaResult<VO> get( DTO dto) throws  Exception;
   // /**指定请求类型**/
   // public TianyanchaResult<VO> request(String url, HttpMethod method, DTO dto);
}
