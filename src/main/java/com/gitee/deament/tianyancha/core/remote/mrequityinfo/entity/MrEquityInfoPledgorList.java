package com.gitee.deament.tianyancha.core.remote.mrequityinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*股权出质
* 属于MrEquityInfo
* /services/open/mr/equityInfo/2.0
*@author deament
**/

public class MrEquityInfoPledgorList implements Serializable{

    /**
     *    名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    id
    **/
    @JSONField(name="id")
    private String id;


    /**
    *   设置 名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public String getId() {
      return id;
    }



}

