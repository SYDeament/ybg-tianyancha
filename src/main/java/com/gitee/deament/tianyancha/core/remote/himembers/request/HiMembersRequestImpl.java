package com.gitee.deament.tianyancha.core.remote.himembers.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.himembers.entity.HiMembers;
import com.gitee.deament.tianyancha.core.remote.himembers.dto.HiMembersDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史主要人员
* 可以通过公司名称或ID获取历史主要人员信息，历史主要人员信息包括法人名、变更时间、主要人员名、变更时间、股东名、退股时间等字段的信息
* /services/open/hi/members
*@author deament
**/
@Component("hiMembersRequestImpl")
public class HiMembersRequestImpl extends BaseRequestImpl<HiMembers,HiMembersDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/members";
    }
}

