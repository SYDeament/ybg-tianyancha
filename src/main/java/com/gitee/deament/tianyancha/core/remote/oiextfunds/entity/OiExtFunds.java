package com.gitee.deament.tianyancha.core.remote.oiextfunds.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oiextfunds.entity.OiExtFundsInvestor_list;
import com.gitee.deament.tianyancha.core.remote.oiextfunds.entity.OiExtFundsItems;
/**
* 对外投资基金
* 可以通过投资机构名称获取投资机构对外投资基金信息，包括企业名称、法人、注册资本、成立日期、投资方等字段信息
* /services/open/oi/extFunds/2.0
*@author deament
**/

public class OiExtFunds implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiExtFundsItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiExtFundsItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiExtFundsItems> getItems() {
      return items;
    }



}

