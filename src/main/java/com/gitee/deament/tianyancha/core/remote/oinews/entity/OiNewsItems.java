package com.gitee.deament.tianyancha.core.remote.oinews.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*相关新闻
* 属于OiNews
* /services/open/oi/news/2.0
*@author deament
**/

public class OiNewsItems implements Serializable{

    /**
     *    新闻事件
    **/
    @JSONField(name="news_date")
    private String news_date;
    /**
     *    来源
    **/
    @JSONField(name="news_source")
    private String news_source;
    /**
     *    来源url
    **/
    @JSONField(name="link")
    private String link;
    /**
     *    关联名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    新闻标题
    **/
    @JSONField(name="title")
    private String title;


    /**
    *   设置 新闻事件
    **/
    public void setNews_date(String news_date) {
      this.news_date = news_date;
    }
    /**
    *   获取 新闻事件
    **/
    public String getNews_date() {
      return news_date;
    }
    /**
    *   设置 来源
    **/
    public void setNews_source(String news_source) {
      this.news_source = news_source;
    }
    /**
    *   获取 来源
    **/
    public String getNews_source() {
      return news_source;
    }
    /**
    *   设置 来源url
    **/
    public void setLink(String link) {
      this.link = link;
    }
    /**
    *   获取 来源url
    **/
    public String getLink() {
      return link;
    }
    /**
    *   设置 关联名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 关联名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 新闻标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 新闻标题
    **/
    public String getTitle() {
      return title;
    }



}

