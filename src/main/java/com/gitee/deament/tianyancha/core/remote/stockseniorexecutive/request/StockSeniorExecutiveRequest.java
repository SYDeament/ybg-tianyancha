package com.gitee.deament.tianyancha.core.remote.stockseniorexecutive.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockseniorexecutive.entity.StockSeniorExecutive;
import com.gitee.deament.tianyancha.core.remote.stockseniorexecutive.dto.StockSeniorExecutiveDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*高管信息
* 可以通过公司名称或ID获取上市公司高管信息，上市公司高管信息包括高管姓名、职务、持股数、年龄、学历等
* /services/open/stock/seniorExecutive/2.0
*@author deament
**/

public interface StockSeniorExecutiveRequest extends BaseRequest<StockSeniorExecutiveDTO ,StockSeniorExecutive>{

}

