package com.gitee.deament.tianyancha.core.remote.stockcorpcontactinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*联系信息
* 可以通过公司名称或ID获取上市公司联系信息，联系信息包括联系电话、电子邮箱、传真、公司网址、办公地址、注册地址等
* /services/open/stock/corpContactInfo/2.0
*@author deament
**/

public class StockCorpContactInfo implements Serializable{

    /**
     *    办公地址
    **/
    @JSONField(name="office_address_cn")
    private String office_address_cn;
    /**
     *    法人
    **/
    @JSONField(name="legal_representative")
    private String legal_representative;
    /**
     *    邮编
    **/
    @JSONField(name="postcode")
    private String postcode;
    /**
     *    联系电话
    **/
    @JSONField(name="telephone")
    private String telephone;
    /**
     *    注册号
    **/
    @JSONField(name="business_reg_num")
    private String business_reg_num;
    /**
     *    网址
    **/
    @JSONField(name="org_website")
    private String org_website;
    /**
     *    董事长
    **/
    @JSONField(name="secretary")
    private String secretary;
    /**
     *    省份
    **/
    @JSONField(name="provincial_name")
    private String provincial_name;
    /**
     *    总经理
    **/
    @JSONField(name="general_manager")
    private String general_manager;
    /**
     *    英文名
    **/
    @JSONField(name="org_name_en")
    private String org_name_en;
    /**
     *    人员规模
    **/
    @JSONField(name="staff_num")
    private Integer staff_num;
    /**
     *    传真
    **/
    @JSONField(name="fax")
    private String fax;
    /**
     *    企业名
    **/
    @JSONField(name="org_name_cn")
    private String org_name_cn;
    /**
     *    注册地址
    **/
    @JSONField(name="reg_address_cn")
    private String reg_address_cn;
    /**
     *    邮箱
    **/
    @JSONField(name="email")
    private String email;


    /**
    *   设置 办公地址
    **/
    public void setOffice_address_cn(String office_address_cn) {
      this.office_address_cn = office_address_cn;
    }
    /**
    *   获取 办公地址
    **/
    public String getOffice_address_cn() {
      return office_address_cn;
    }
    /**
    *   设置 法人
    **/
    public void setLegal_representative(String legal_representative) {
      this.legal_representative = legal_representative;
    }
    /**
    *   获取 法人
    **/
    public String getLegal_representative() {
      return legal_representative;
    }
    /**
    *   设置 邮编
    **/
    public void setPostcode(String postcode) {
      this.postcode = postcode;
    }
    /**
    *   获取 邮编
    **/
    public String getPostcode() {
      return postcode;
    }
    /**
    *   设置 联系电话
    **/
    public void setTelephone(String telephone) {
      this.telephone = telephone;
    }
    /**
    *   获取 联系电话
    **/
    public String getTelephone() {
      return telephone;
    }
    /**
    *   设置 注册号
    **/
    public void setBusiness_reg_num(String business_reg_num) {
      this.business_reg_num = business_reg_num;
    }
    /**
    *   获取 注册号
    **/
    public String getBusiness_reg_num() {
      return business_reg_num;
    }
    /**
    *   设置 网址
    **/
    public void setOrg_website(String org_website) {
      this.org_website = org_website;
    }
    /**
    *   获取 网址
    **/
    public String getOrg_website() {
      return org_website;
    }
    /**
    *   设置 董事长
    **/
    public void setSecretary(String secretary) {
      this.secretary = secretary;
    }
    /**
    *   获取 董事长
    **/
    public String getSecretary() {
      return secretary;
    }
    /**
    *   设置 省份
    **/
    public void setProvincial_name(String provincial_name) {
      this.provincial_name = provincial_name;
    }
    /**
    *   获取 省份
    **/
    public String getProvincial_name() {
      return provincial_name;
    }
    /**
    *   设置 总经理
    **/
    public void setGeneral_manager(String general_manager) {
      this.general_manager = general_manager;
    }
    /**
    *   获取 总经理
    **/
    public String getGeneral_manager() {
      return general_manager;
    }
    /**
    *   设置 英文名
    **/
    public void setOrg_name_en(String org_name_en) {
      this.org_name_en = org_name_en;
    }
    /**
    *   获取 英文名
    **/
    public String getOrg_name_en() {
      return org_name_en;
    }
    /**
    *   设置 人员规模
    **/
    public void setStaff_num(Integer staff_num) {
      this.staff_num = staff_num;
    }
    /**
    *   获取 人员规模
    **/
    public Integer getStaff_num() {
      return staff_num;
    }
    /**
    *   设置 传真
    **/
    public void setFax(String fax) {
      this.fax = fax;
    }
    /**
    *   获取 传真
    **/
    public String getFax() {
      return fax;
    }
    /**
    *   设置 企业名
    **/
    public void setOrg_name_cn(String org_name_cn) {
      this.org_name_cn = org_name_cn;
    }
    /**
    *   获取 企业名
    **/
    public String getOrg_name_cn() {
      return org_name_cn;
    }
    /**
    *   设置 注册地址
    **/
    public void setReg_address_cn(String reg_address_cn) {
      this.reg_address_cn = reg_address_cn;
    }
    /**
    *   获取 注册地址
    **/
    public String getReg_address_cn() {
      return reg_address_cn;
    }
    /**
    *   设置 邮箱
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱
    **/
    public String getEmail() {
      return email;
    }



}

