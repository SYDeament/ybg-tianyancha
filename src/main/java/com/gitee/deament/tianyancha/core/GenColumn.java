package com.gitee.deament.tianyancha.core;

import com.gitee.deament.tianyancha.annotation.ParamRequire;

import java.io.Serializable;

public class GenColumn implements Serializable {

    String type;
    String name;
    String remark;
    /**
     * 大写开头
     */
    String attrName;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }
}
