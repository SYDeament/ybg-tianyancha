package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.request;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.gitee.deament.tianyancha.core.TianyanchaResult;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import com.gitee.deament.tianyancha.core.base.HttpRequestI;
import com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.dto.XgbaseinfoV2DTO;
import com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 特殊企业基本信息
 * 属于XgbaseinfoV2
 * /services/v4/open/xgbaseinfoV2
 *
 * @author deament
 **/
@Component("xgbaseinfoV2RequestImpl")
public class XgbaseinfoV2RequestImpl extends BaseRequestImpl<XgbaseinfoV2, XgbaseinfoV2DTO> {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private HttpRequestI httpRequest;

    @Override
    public String getUrl() {
        return "http://open.api.tianyancha.com" + "/services/v4/open/xgbaseinfoV2";
    }

    @Override
    public TianyanchaResult<XgbaseinfoV2> get(XgbaseinfoV2DTO xgbaseinfoV2) throws Exception {
        String url = getUrl();
        String result = httpRequest.get(url, BeanUtil.beanToMap(xgbaseinfoV2));
        logger.info("得到的结果：" + result);
        TianyanchaResult<XgbaseinfoV2> vo = JSONObject.parseObject(result, TianyanchaResult.class);
        if (vo != null && vo.isSuccess() && vo.getResult() != null) {
            Integer integer = JSONObject.parseObject(result).getJSONObject("result").getInteger("entityType");
            String resultObject = JSONObject.parseObject(result).getString("result");
            switch (integer) {
                case 1:
                    vo.getResult().setXgbaseinfoV21(JSONObject.parseObject(resultObject, XgbaseinfoV21.class));
                    break;
                case 2:
                    vo.getResult().setXgbaseinfoV22(JSONObject.parseObject(resultObject, XgbaseinfoV22.class));
                    break;
                case 3:
                    vo.getResult().setXgbaseinfoV23(JSONObject.parseObject(resultObject, XgbaseinfoV23.class));
                    break;
                case 4:
                    vo.getResult().setXgbaseinfoV24(JSONObject.parseObject(resultObject, XgbaseinfoV24.class));
                    break;
                case 5:
                    vo.getResult().setXgbaseinfoV25(JSONObject.parseObject(resultObject, XgbaseinfoV25.class));
                    break;
                case 6:
                    vo.getResult().setXgbaseinfoV26(JSONObject.parseObject(resultObject, XgbaseinfoV26.class));
                    break;
            }
        }
        logger.info("转成对于的实体类:" + JSONObject.toJSONString(vo));
        return vo;
    }
}
