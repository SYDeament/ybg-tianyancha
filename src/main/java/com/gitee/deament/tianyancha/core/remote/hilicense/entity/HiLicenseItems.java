package com.gitee.deament.tianyancha.core.remote.hilicense.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史行政许可-其他来源
* 属于HiLicense
* /services/open/hi/license/creditChina/2.0
*@author deament
**/

public class HiLicenseItems implements Serializable{

    /**
     *    内容许可
    **/
    @JSONField(name="licenceContent")
    private String licenceContent;
    /**
     *    许可有效期
    **/
    @JSONField(name="validityTime")
    private String validityTime;
    /**
     *    许可证号
    **/
    @JSONField(name="licenceNumber")
    private String licenceNumber;
    /**
     *    地方编码
    **/
    @JSONField(name="localCode")
    private String localCode;
    /**
     *    许可机关
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    审核类型
    **/
    @JSONField(name="audiType")
    private String audiType;
    /**
     *    数据更新时间
    **/
    @JSONField(name="dataUpdateTime")
    private String dataUpdateTime;
    /**
     *    许可决定日期
    **/
    @JSONField(name="decisionDate")
    private String decisionDate;
    /**
     *    法定代表人（负责人）姓名
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;


    /**
    *   设置 内容许可
    **/
    public void setLicenceContent(String licenceContent) {
      this.licenceContent = licenceContent;
    }
    /**
    *   获取 内容许可
    **/
    public String getLicenceContent() {
      return licenceContent;
    }
    /**
    *   设置 许可有效期
    **/
    public void setValidityTime(String validityTime) {
      this.validityTime = validityTime;
    }
    /**
    *   获取 许可有效期
    **/
    public String getValidityTime() {
      return validityTime;
    }
    /**
    *   设置 许可证号
    **/
    public void setLicenceNumber(String licenceNumber) {
      this.licenceNumber = licenceNumber;
    }
    /**
    *   获取 许可证号
    **/
    public String getLicenceNumber() {
      return licenceNumber;
    }
    /**
    *   设置 地方编码
    **/
    public void setLocalCode(String localCode) {
      this.localCode = localCode;
    }
    /**
    *   获取 地方编码
    **/
    public String getLocalCode() {
      return localCode;
    }
    /**
    *   设置 许可机关
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 许可机关
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 审核类型
    **/
    public void setAudiType(String audiType) {
      this.audiType = audiType;
    }
    /**
    *   获取 审核类型
    **/
    public String getAudiType() {
      return audiType;
    }
    /**
    *   设置 数据更新时间
    **/
    public void setDataUpdateTime(String dataUpdateTime) {
      this.dataUpdateTime = dataUpdateTime;
    }
    /**
    *   获取 数据更新时间
    **/
    public String getDataUpdateTime() {
      return dataUpdateTime;
    }
    /**
    *   设置 许可决定日期
    **/
    public void setDecisionDate(String decisionDate) {
      this.decisionDate = decisionDate;
    }
    /**
    *   获取 许可决定日期
    **/
    public String getDecisionDate() {
      return decisionDate;
    }
    /**
    *   设置 法定代表人（负责人）姓名
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法定代表人（负责人）姓名
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }



}

