package com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*司法协助详情
* 属于GetJudicialDetail
* /services/v4/open/getJudicialDetail
*@author deament
**/

public class GetJudicialDetailAssDetail implements Serializable{

    /**
     *    被执行人
    **/
    @JSONField(name="executedPerson")
    private String executedPerson;
    /**
     *    执行通知书文号
    **/
    @JSONField(name="executeNoticeNum")
    private String executeNoticeNum;
    /**
     *    被执行人公司id
    **/
    @JSONField(name="executedPersonCid")
    private Integer executedPersonCid;
    /**
     *    股权数额
    **/
    @JSONField(name="equityAmount")
    private String equityAmount;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    类型|状态
    **/
    @JSONField(name="typeState")
    private String typeState;
    /**
     *    被执行人类型1公司，2人
    **/
    @JSONField(name="executedPersonType")
    private String executedPersonType;
    /**
     *    被执行人id
    **/
    @JSONField(name="executedPersonHid")
    private String executedPersonHid;
    /**
     *    执行法院
    **/
    @JSONField(name="executiveCourt")
    private String executiveCourt;


    /**
    *   设置 被执行人
    **/
    public void setExecutedPerson(String executedPerson) {
      this.executedPerson = executedPerson;
    }
    /**
    *   获取 被执行人
    **/
    public String getExecutedPerson() {
      return executedPerson;
    }
    /**
    *   设置 执行通知书文号
    **/
    public void setExecuteNoticeNum(String executeNoticeNum) {
      this.executeNoticeNum = executeNoticeNum;
    }
    /**
    *   获取 执行通知书文号
    **/
    public String getExecuteNoticeNum() {
      return executeNoticeNum;
    }
    /**
    *   设置 被执行人公司id
    **/
    public void setExecutedPersonCid(Integer executedPersonCid) {
      this.executedPersonCid = executedPersonCid;
    }
    /**
    *   获取 被执行人公司id
    **/
    public Integer getExecutedPersonCid() {
      return executedPersonCid;
    }
    /**
    *   设置 股权数额
    **/
    public void setEquityAmount(String equityAmount) {
      this.equityAmount = equityAmount;
    }
    /**
    *   获取 股权数额
    **/
    public String getEquityAmount() {
      return equityAmount;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 类型|状态
    **/
    public void setTypeState(String typeState) {
      this.typeState = typeState;
    }
    /**
    *   获取 类型|状态
    **/
    public String getTypeState() {
      return typeState;
    }
    /**
    *   设置 被执行人类型1公司，2人
    **/
    public void setExecutedPersonType(String executedPersonType) {
      this.executedPersonType = executedPersonType;
    }
    /**
    *   获取 被执行人类型1公司，2人
    **/
    public String getExecutedPersonType() {
      return executedPersonType;
    }
    /**
    *   设置 被执行人id
    **/
    public void setExecutedPersonHid(String executedPersonHid) {
      this.executedPersonHid = executedPersonHid;
    }
    /**
    *   获取 被执行人id
    **/
    public String getExecutedPersonHid() {
      return executedPersonHid;
    }
    /**
    *   设置 执行法院
    **/
    public void setExecutiveCourt(String executiveCourt) {
      this.executiveCourt = executiveCourt;
    }
    /**
    *   获取 执行法院
    **/
    public String getExecutiveCourt() {
      return executiveCourt;
    }



}

