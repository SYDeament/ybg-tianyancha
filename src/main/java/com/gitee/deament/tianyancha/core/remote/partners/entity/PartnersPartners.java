package com.gitee.deament.tianyancha.core.remote.partners.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*人员所有合作伙伴
* 属于Partners
* /services/v4/open/partners
*@author deament
**/

public class PartnersPartners implements Serializable{

    /**
     *    人id
    **/
    @JSONField(name="hid")
    private Integer hid;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    姓名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    logo
    **/
    @JSONField(name="headUrl")
    private String headUrl;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 人id
    **/
    public void setHid(Integer hid) {
      this.hid = hid;
    }
    /**
    *   获取 人id
    **/
    public Integer getHid() {
      return hid;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 姓名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 姓名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 logo
    **/
    public void setHeadUrl(String headUrl) {
      this.headUrl = headUrl;
    }
    /**
    *   获取 logo
    **/
    public String getHeadUrl() {
      return headUrl;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

