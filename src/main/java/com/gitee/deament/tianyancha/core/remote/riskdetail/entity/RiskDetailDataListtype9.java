package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype9 implements Serializable{

    /**
     *    注销原因
    **/
    @JSONField(name="cancel")
    private String cancel;
    /**
     *    注销日期
    **/
    @JSONField(name="cancelDate")
    private Long cancelDate;
    /**
     *    股权出质设立登记日期
    **/
    @JSONField(name="regDate")
    private Long regDate;
    /**
     *    出质人
    **/
    @JSONField(name="pledgor")
    private String pledgor;
    /**
     *    质权人证照/证件号码
    **/
    @JSONField(name="certifNumberR")
    private String certifNumberR;
    /**
     *    质权人
    **/
    @JSONField(name="pledgee")
    private String pledgee;
    /**
     *    变化情况
    **/
    @JSONField(name="changeSituation")
    private String changeSituation;
    /**
     *    登记编号
    **/
    @JSONField(name="regNumber")
    private String regNumber;
    /**
     *    出质人证照/证件号码
    **/
    @JSONField(name="certifNumber")
    private String certifNumber;
    /**
     *    出质股权数额
    **/
    @JSONField(name="equityAmount")
    private String equityAmount;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    状态
    **/
    @JSONField(name="state")
    private String state;
    /**
     *    股权出质设立发布日期
    **/
    @JSONField(name="putDate")
    private Long putDate;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 注销原因
    **/
    public void setCancel(String cancel) {
      this.cancel = cancel;
    }
    /**
    *   获取 注销原因
    **/
    public String getCancel() {
      return cancel;
    }
    /**
    *   设置 注销日期
    **/
    public void setCancelDate(Long cancelDate) {
      this.cancelDate = cancelDate;
    }
    /**
    *   获取 注销日期
    **/
    public Long getCancelDate() {
      return cancelDate;
    }
    /**
    *   设置 股权出质设立登记日期
    **/
    public void setRegDate(Long regDate) {
      this.regDate = regDate;
    }
    /**
    *   获取 股权出质设立登记日期
    **/
    public Long getRegDate() {
      return regDate;
    }
    /**
    *   设置 出质人
    **/
    public void setPledgor(String pledgor) {
      this.pledgor = pledgor;
    }
    /**
    *   获取 出质人
    **/
    public String getPledgor() {
      return pledgor;
    }
    /**
    *   设置 质权人证照/证件号码
    **/
    public void setCertifNumberR(String certifNumberR) {
      this.certifNumberR = certifNumberR;
    }
    /**
    *   获取 质权人证照/证件号码
    **/
    public String getCertifNumberR() {
      return certifNumberR;
    }
    /**
    *   设置 质权人
    **/
    public void setPledgee(String pledgee) {
      this.pledgee = pledgee;
    }
    /**
    *   获取 质权人
    **/
    public String getPledgee() {
      return pledgee;
    }
    /**
    *   设置 变化情况
    **/
    public void setChangeSituation(String changeSituation) {
      this.changeSituation = changeSituation;
    }
    /**
    *   获取 变化情况
    **/
    public String getChangeSituation() {
      return changeSituation;
    }
    /**
    *   设置 登记编号
    **/
    public void setRegNumber(String regNumber) {
      this.regNumber = regNumber;
    }
    /**
    *   获取 登记编号
    **/
    public String getRegNumber() {
      return regNumber;
    }
    /**
    *   设置 出质人证照/证件号码
    **/
    public void setCertifNumber(String certifNumber) {
      this.certifNumber = certifNumber;
    }
    /**
    *   获取 出质人证照/证件号码
    **/
    public String getCertifNumber() {
      return certifNumber;
    }
    /**
    *   设置 出质股权数额
    **/
    public void setEquityAmount(String equityAmount) {
      this.equityAmount = equityAmount;
    }
    /**
    *   获取 出质股权数额
    **/
    public String getEquityAmount() {
      return equityAmount;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 状态
    **/
    public void setState(String state) {
      this.state = state;
    }
    /**
    *   获取 状态
    **/
    public String getState() {
      return state;
    }
    /**
    *   设置 股权出质设立发布日期
    **/
    public void setPutDate(Long putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 股权出质设立发布日期
    **/
    public Long getPutDate() {
      return putDate;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }



}

