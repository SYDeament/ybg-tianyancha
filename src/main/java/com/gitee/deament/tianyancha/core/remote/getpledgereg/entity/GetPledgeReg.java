package com.gitee.deament.tianyancha.core.remote.getpledgereg.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.getpledgereg.entity.GetPledgeRegItems;
/**
*知识产权出质
* 可以通过公司名称或ID获取知识产权出质信息，知识产权出质信息包括质权人名称、知识产权登记证号等字段的详细信息
* /services/v4/open/getPledgeReg
*@author deament
**/

public class GetPledgeReg implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<GetPledgeRegItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<GetPledgeRegItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<GetPledgeRegItems> getItems() {
      return items;
    }



}

