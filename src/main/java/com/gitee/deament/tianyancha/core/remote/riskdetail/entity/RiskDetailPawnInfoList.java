package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailPawnInfoList implements Serializable{

    /**
     *    名称
    **/
    @JSONField(name="pawnName")
    private String pawnName;
    /**
     *    所有权归属
    **/
    @JSONField(name="ownership")
    private String ownership;
    /**
     *    备注
    **/
    @JSONField(name="remark")
    private String remark;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    数量、质量、状况、所在地等情况
    **/
    @JSONField(name="detail")
    private String detail;


    /**
    *   设置 名称
    **/
    public void setPawnName(String pawnName) {
      this.pawnName = pawnName;
    }
    /**
    *   获取 名称
    **/
    public String getPawnName() {
      return pawnName;
    }
    /**
    *   设置 所有权归属
    **/
    public void setOwnership(String ownership) {
      this.ownership = ownership;
    }
    /**
    *   获取 所有权归属
    **/
    public String getOwnership() {
      return ownership;
    }
    /**
    *   设置 备注
    **/
    public void setRemark(String remark) {
      this.remark = remark;
    }
    /**
    *   获取 备注
    **/
    public String getRemark() {
      return remark;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 数量、质量、状况、所在地等情况
    **/
    public void setDetail(String detail) {
      this.detail = detail;
    }
    /**
    *   获取 数量、质量、状况、所在地等情况
    **/
    public String getDetail() {
      return detail;
    }



}

