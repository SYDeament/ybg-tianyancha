package com.gitee.deament.tianyancha.core.remote.himembers.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.himembers.entity.HiMembersPastLegalPersonList;
import com.gitee.deament.tianyancha.core.remote.himembers.entity.HiMembersPastStafferList;
import com.gitee.deament.tianyancha.core.remote.himembers.entity.HiMembersPastholderList;
/**
*历史主要人员
* 可以通过公司名称或ID获取历史主要人员信息，历史主要人员信息包括法人名、变更时间、主要人员名、变更时间、股东名、退股时间等字段的信息
* /services/open/hi/members
*@author deament
**/

public class HiMembers implements Serializable{

    /**
     *    法人
    **/
    @JSONField(name="pastLegalPersonList")
    private List<HiMembersPastLegalPersonList> pastLegalPersonList;
    /**
     *    主要人员
    **/
    @JSONField(name="pastStafferList")
    private List<List<HiMembersPastStafferList>> pastStafferList;
    /**
     *    股东
    **/
    @JSONField(name="pastholderList")
    private List<HiMembersPastholderList> pastholderList;


    /**
    *   设置 法人
    **/
    public void setPastLegalPersonList(List<HiMembersPastLegalPersonList> pastLegalPersonList) {
      this.pastLegalPersonList = pastLegalPersonList;
    }
    /**
    *   获取 法人
    **/
    public List<HiMembersPastLegalPersonList> getPastLegalPersonList() {
      return pastLegalPersonList;
    }
    /**
    *   设置 主要人员
    **/
    public void setPastStafferList(List<List<HiMembersPastStafferList>> pastStafferList) {
      this.pastStafferList = pastStafferList;
    }
    /**
    *   获取 主要人员
    **/
    public List<List<HiMembersPastStafferList>> getPastStafferList() {
      return pastStafferList;
    }
    /**
    *   设置 股东
    **/
    public void setPastholderList(List<HiMembersPastholderList> pastholderList) {
      this.pastholderList = pastholderList;
    }
    /**
    *   获取 股东
    **/
    public List<HiMembersPastholderList> getPastholderList() {
      return pastholderList;
    }



}

