package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*特殊企业基本信息
* 可以通过公司名称或ID获取特殊企业基本信息，包含香港公司、社会组织、律所、事业单位、基金会这些特殊企业，不同社会团体所呈现的信息维度不同
* /services/v4/open/xgbaseinfoV2
*@author deament
**/

public class XgbaseinfoV23 implements Serializable{

    /**
     *    无用
    **/
    @JSONField(name="baseInfo")
    private String baseInfo;
    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    业务主管单位
    **/
    @JSONField(name="businessUnit")
    private String businessUnit;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    企业邮箱列表
    **/
    @JSONField(name="emailList")
    private List<String> emailList;
    /**
     *    企业联系方式列表
    **/
    @JSONField(name="phoneList")
    private List<String> phoneList;
    /**
     *    弃用
    **/
    @JSONField(name="baiduAuthURLWWW")
    private String baiduAuthURLWWW;
    /**
     *    无用
    **/
    @JSONField(name="pricingPackage")
    private Integer pricingPackage;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    证书有效期
    **/
    @JSONField(name="expiryDate")
    private String expiryDate;
    /**
     *    统一信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    微博(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="weibo")
    private String weibo;
    /**
     *    成立时间
    **/
    @JSONField(name="registrationDate")
    private Long registrationDate;
    /**
     *    公司logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    弃用
    **/
    @JSONField(name="baiduAuthURLWAP")
    private String baiduAuthURLWAP;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    弃用
    **/
    @JSONField(name="isClaimed")
    private Integer isClaimed;
    /**
     *    邮箱
    **/
    @JSONField(name="email")
    private String email;
    /**
     *    类型
    **/
    @JSONField(name="types")
    private String types;
    /**
     *    登记管理机关
    **/
    @JSONField(name="regInstitute")
    private String regInstitute;
    /**
     *    1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    @JSONField(name="entityType")
    private Integer entityType;
    /**
     *    是否有年报
    **/
    @JSONField(name="haveReport")
    private Boolean haveReport;
    /**
     *    业务范围
    **/
    @JSONField(name="businessScope")
    private String businessScope;
    /**
     *    注册地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    成立登记日期
    **/
    @JSONField(name="registrationDateStr")
    private String registrationDateStr;
    /**
     *    
    **/
    @JSONField(name="websiteList")
    private String websiteList;
    /**
     *    企业联系方式
    **/
    @JSONField(name="phoneNumber")
    private String phoneNumber;
    /**
     *    地址
    **/
    @JSONField(name="rladdress")
    private String rladdress;
    /**
     *    登记证号
    **/
    @JSONField(name="registrationNumber")
    private String registrationNumber;
    /**
     *    机构名称
    **/
    @JSONField(name="name")
    private String name;


    /**
    *   设置 无用
    **/
    public void setBaseInfo(String baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 无用
    **/
    public String getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 业务主管单位
    **/
    public void setBusinessUnit(String businessUnit) {
      this.businessUnit = businessUnit;
    }
    /**
    *   获取 业务主管单位
    **/
    public String getBusinessUnit() {
      return businessUnit;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 企业邮箱列表
    **/
    public void setEmailList(List<String> emailList) {
      this.emailList = emailList;
    }
    /**
    *   获取 企业邮箱列表
    **/
    public List<String> getEmailList() {
      return emailList;
    }
    /**
    *   设置 企业联系方式列表
    **/
    public void setPhoneList(List<String> phoneList) {
      this.phoneList = phoneList;
    }
    /**
    *   获取 企业联系方式列表
    **/
    public List<String> getPhoneList() {
      return phoneList;
    }
    /**
    *   设置 弃用
    **/
    public void setBaiduAuthURLWWW(String baiduAuthURLWWW) {
      this.baiduAuthURLWWW = baiduAuthURLWWW;
    }
    /**
    *   获取 弃用
    **/
    public String getBaiduAuthURLWWW() {
      return baiduAuthURLWWW;
    }
    /**
    *   设置 无用
    **/
    public void setPricingPackage(Integer pricingPackage) {
      this.pricingPackage = pricingPackage;
    }
    /**
    *   获取 无用
    **/
    public Integer getPricingPackage() {
      return pricingPackage;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 证书有效期
    **/
    public void setExpiryDate(String expiryDate) {
      this.expiryDate = expiryDate;
    }
    /**
    *   获取 证书有效期
    **/
    public String getExpiryDate() {
      return expiryDate;
    }
    /**
    *   设置 统一信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 微博(官方未定义字段类型 暂定为String)
    **/
    public void setWeibo(String weibo) {
      this.weibo = weibo;
    }
    /**
    *   获取 微博(官方未定义字段类型 暂定为String)
    **/
    public String getWeibo() {
      return weibo;
    }
    /**
    *   设置 成立时间
    **/
    public void setRegistrationDate(Long registrationDate) {
      this.registrationDate = registrationDate;
    }
    /**
    *   获取 成立时间
    **/
    public Long getRegistrationDate() {
      return registrationDate;
    }
    /**
    *   设置 公司logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 公司logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 弃用
    **/
    public void setBaiduAuthURLWAP(String baiduAuthURLWAP) {
      this.baiduAuthURLWAP = baiduAuthURLWAP;
    }
    /**
    *   获取 弃用
    **/
    public String getBaiduAuthURLWAP() {
      return baiduAuthURLWAP;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 弃用
    **/
    public void setIsClaimed(Integer isClaimed) {
      this.isClaimed = isClaimed;
    }
    /**
    *   获取 弃用
    **/
    public Integer getIsClaimed() {
      return isClaimed;
    }
    /**
    *   设置 邮箱
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱
    **/
    public String getEmail() {
      return email;
    }
    /**
    *   设置 类型
    **/
    public void setTypes(String types) {
      this.types = types;
    }
    /**
    *   获取 类型
    **/
    public String getTypes() {
      return types;
    }
    /**
    *   设置 登记管理机关
    **/
    public void setRegInstitute(String regInstitute) {
      this.regInstitute = regInstitute;
    }
    /**
    *   获取 登记管理机关
    **/
    public String getRegInstitute() {
      return regInstitute;
    }
    /**
    *   设置 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public void setEntityType(Integer entityType) {
      this.entityType = entityType;
    }
    /**
    *   获取 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public Integer getEntityType() {
      return entityType;
    }
    /**
    *   设置 是否有年报
    **/
    public void setHaveReport(Boolean haveReport) {
      this.haveReport = haveReport;
    }
    /**
    *   获取 是否有年报
    **/
    public Boolean getHaveReport() {
      return haveReport;
    }
    /**
    *   设置 业务范围
    **/
    public void setBusinessScope(String businessScope) {
      this.businessScope = businessScope;
    }
    /**
    *   获取 业务范围
    **/
    public String getBusinessScope() {
      return businessScope;
    }
    /**
    *   设置 注册地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 注册地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 成立登记日期
    **/
    public void setRegistrationDateStr(String registrationDateStr) {
      this.registrationDateStr = registrationDateStr;
    }
    /**
    *   获取 成立登记日期
    **/
    public String getRegistrationDateStr() {
      return registrationDateStr;
    }
    /**
    *   设置 
    **/
    public void setWebsiteList(String websiteList) {
      this.websiteList = websiteList;
    }
    /**
    *   获取 
    **/
    public String getWebsiteList() {
      return websiteList;
    }
    /**
    *   设置 企业联系方式
    **/
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    /**
    *   获取 企业联系方式
    **/
    public String getPhoneNumber() {
      return phoneNumber;
    }
    /**
    *   设置 地址
    **/
    public void setRladdress(String rladdress) {
      this.rladdress = rladdress;
    }
    /**
    *   获取 地址
    **/
    public String getRladdress() {
      return rladdress;
    }
    /**
    *   设置 登记证号
    **/
    public void setRegistrationNumber(String registrationNumber) {
      this.registrationNumber = registrationNumber;
    }
    /**
    *   获取 登记证号
    **/
    public String getRegistrationNumber() {
      return registrationNumber;
    }
    /**
    *   设置 机构名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 机构名称
    **/
    public String getName() {
      return name;
    }



}

