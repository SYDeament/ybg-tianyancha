package com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*疑似实际控制人
* 属于IcActualControl
* /services/open/ic/actualControl/2.0
*@author deament
**/

public class IcActualControlP_0 implements Serializable{

    /**
     *    所有的连接关系集合信息
    **/
    @JSONField(name="relationships")
    private List<IcActualControlRelationships> relationships;
    /**
     *    node 节点信息集合
    **/
    @JSONField(name="nodes")
    private List<IcActualControlNodes> nodes;


    /**
    *   设置 所有的连接关系集合信息
    **/
    public void setRelationships(List<IcActualControlRelationships> relationships) {
      this.relationships = relationships;
    }
    /**
    *   获取 所有的连接关系集合信息
    **/
    public List<IcActualControlRelationships> getRelationships() {
      return relationships;
    }
    /**
    *   设置 node 节点信息集合
    **/
    public void setNodes(List<IcActualControlNodes> nodes) {
      this.nodes = nodes;
    }
    /**
    *   获取 node 节点信息集合
    **/
    public List<IcActualControlNodes> getNodes() {
      return nodes;
    }



}

