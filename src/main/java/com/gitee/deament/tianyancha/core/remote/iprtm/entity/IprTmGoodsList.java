package com.gitee.deament.tianyancha.core.remote.iprtm.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*商标信息详情
* 属于IprTm
* /services/open/ipr/tm/detail/2.0
*@author deament
**/

public class IprTmGoodsList implements Serializable{

    /**
     *    
    **/
    @JSONField(name="code")
    private String code;
    /**
     *    
    **/
    @JSONField(name="title")
    private String title;


    /**
    *   设置 
    **/
    public void setCode(String code) {
      this.code = code;
    }
    /**
    *   获取 
    **/
    public String getCode() {
      return code;
    }
    /**
    *   设置 
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 
    **/
    public String getTitle() {
      return title;
    }



}

