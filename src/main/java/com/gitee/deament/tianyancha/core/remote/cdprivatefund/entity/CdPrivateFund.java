package com.gitee.deament.tianyancha.core.remote.cdprivatefund.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdprivatefund.entity.CdPrivateFundItems;
/**
*私募基金
* 可以通过公司名称或ID获企业相关私募基金信息，私募基金信息包括私募基金管理人名称、法定代表人/执行事务合伙人、机构类型、登记编号等字段的详细信息
* /services/open/cd/privateFund/2.0
*@author deament
**/

public class CdPrivateFund implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CdPrivateFundItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CdPrivateFundItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CdPrivateFundItems> getItems() {
      return items;
    }



}

