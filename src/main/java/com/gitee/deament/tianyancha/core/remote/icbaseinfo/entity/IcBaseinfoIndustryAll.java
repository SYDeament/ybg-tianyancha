package com.gitee.deament.tianyancha.core.remote.icbaseinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业基本信息
* 属于IcBaseinfo
* /services/open/ic/baseinfo/2.0
*@author deament
**/

public class IcBaseinfoIndustryAll implements Serializable{

    /**
     *    国民经济行业分类中类
    **/
    @JSONField(name="categoryMiddle")
    private String categoryMiddle;
    /**
     *    国民经济行业分类大类
    **/
    @JSONField(name="categoryBig")
    private String categoryBig;
    /**
     *    国民经济行业分类门类
    **/
    @JSONField(name="category")
    private String category;
    /**
     *    国民经济行业分类小类（未使用）
    **/
    @JSONField(name="categorySmall")
    private String categorySmall;


    /**
    *   设置 国民经济行业分类中类
    **/
    public void setCategoryMiddle(String categoryMiddle) {
      this.categoryMiddle = categoryMiddle;
    }
    /**
    *   获取 国民经济行业分类中类
    **/
    public String getCategoryMiddle() {
      return categoryMiddle;
    }
    /**
    *   设置 国民经济行业分类大类
    **/
    public void setCategoryBig(String categoryBig) {
      this.categoryBig = categoryBig;
    }
    /**
    *   获取 国民经济行业分类大类
    **/
    public String getCategoryBig() {
      return categoryBig;
    }
    /**
    *   设置 国民经济行业分类门类
    **/
    public void setCategory(String category) {
      this.category = category;
    }
    /**
    *   获取 国民经济行业分类门类
    **/
    public String getCategory() {
      return category;
    }
    /**
    *   设置 国民经济行业分类小类（未使用）
    **/
    public void setCategorySmall(String categorySmall) {
      this.categorySmall = categorySmall;
    }
    /**
    *   获取 国民经济行业分类小类（未使用）
    **/
    public String getCategorySmall() {
      return categorySmall;
    }



}

