package com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.entity.MTeleCommunicationLicenseItems;
/**
*电信许可
* 可以通过公司名称或ID获取企业电信许可信息，企业电信许可信息包括客户、销售占比、销售金额、报告期等字段的详细信息
* /services/open/m/teleCommunicationLicense/2.0
*@author deament
**/

public class MTeleCommunicationLicense implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MTeleCommunicationLicenseItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MTeleCommunicationLicenseItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MTeleCommunicationLicenseItems> getItems() {
      return items;
    }



}

