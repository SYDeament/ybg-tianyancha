package com.gitee.deament.tianyancha.core.remote.mcertificate.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*资质证书
* 属于MCertificate
* /services/open/m/certificate/2.0
*@author deament
**/

public class MCertificateDetail implements Serializable{

    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    内容
    **/
    @JSONField(name="content")
    private String content;


    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 内容
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 内容
    **/
    public String getContent() {
      return content;
    }



}

