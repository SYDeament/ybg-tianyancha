package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*特殊企业基本信息
* 可以通过公司名称或ID获取特殊企业基本信息，包含香港公司、社会组织、律所、事业单位、基金会这些特殊企业，不同社会团体所呈现的信息维度不同
* /services/v4/open/xgbaseinfoV2
*@author deament
**/

public class XgbaseinfoV24 implements Serializable{

    /**
     *    企业简介
    **/
    @JSONField(name="baseInfo")
    private String baseInfo;
    /**
     *    核准时间
    **/
    @JSONField(name="approvalDate")
    private String approvalDate;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    企业邮箱列表
    **/
    @JSONField(name="emailList")
    private List<String> emailList;
    /**
     *    企业联系方式列表
    **/
    @JSONField(name="phoneList")
    private List<String> phoneList;
    /**
     *    无用
    **/
    @JSONField(name="baiduAuthURLWWW")
    private String baiduAuthURLWWW;
    /**
     *    uuid
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    无用
    **/
    @JSONField(name="pricingPackage")
    private Integer pricingPackage;
    /**
     *    负责人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    执业状态、状态
    **/
    @JSONField(name="practiceState")
    private String practiceState;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    微博(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="weibo")
    private String weibo;
    /**
     *    总所/分所
    **/
    @JSONField(name="headquartersBranch")
    private String headquartersBranch;
    /**
     *    业务特长
    **/
    @JSONField(name="businessExpertise")
    private String businessExpertise;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    无用
    **/
    @JSONField(name="baiduAuthURLWAP")
    private String baiduAuthURLWAP;
    /**
     *    
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    无用
    **/
    @JSONField(name="isClaimed")
    private Integer isClaimed;
    /**
     *    信用等级
    **/
    @JSONField(name="creditRating")
    private String creditRating;
    /**
     *    邮箱
    **/
    @JSONField(name="email")
    private String email;
    /**
     *    律所简介
    **/
    @JSONField(name="summary")
    private String summary;
    /**
     *    组织形式
    **/
    @JSONField(name="organizationForm")
    private String organizationForm;
    /**
     *    1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    @JSONField(name="entityType")
    private Integer entityType;
    /**
     *    是否有年报
    **/
    @JSONField(name="haveReport")
    private Boolean haveReport;
    /**
     *    税务登记号
    **/
    @JSONField(name="taxNumber")
    private String taxNumber;
    /**
     *    地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    
    **/
    @JSONField(name="dateOfIssue")
    private String dateOfIssue;
    /**
     *    主管机关
    **/
    @JSONField(name="authorities")
    private String authorities;
    /**
     *    网址
    **/
    @JSONField(name="websiteList")
    private String websiteList;
    /**
     *    企业联系方式
    **/
    @JSONField(name="phoneNumber")
    private String phoneNumber;
    /**
     *    地址
    **/
    @JSONField(name="rladdress")
    private String rladdress;
    /**
     *    证书
    **/
    @JSONField(name="permit")
    private String permit;
    /**
     *    律所名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    成立日期
    **/
    @JSONField(name="dateOfEstablishment")
    private String dateOfEstablishment;


    /**
    *   设置 企业简介
    **/
    public void setBaseInfo(String baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 企业简介
    **/
    public String getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 核准时间
    **/
    public void setApprovalDate(String approvalDate) {
      this.approvalDate = approvalDate;
    }
    /**
    *   获取 核准时间
    **/
    public String getApprovalDate() {
      return approvalDate;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 企业邮箱列表
    **/
    public void setEmailList(List<String> emailList) {
      this.emailList = emailList;
    }
    /**
    *   获取 企业邮箱列表
    **/
    public List<String> getEmailList() {
      return emailList;
    }
    /**
    *   设置 企业联系方式列表
    **/
    public void setPhoneList(List<String> phoneList) {
      this.phoneList = phoneList;
    }
    /**
    *   获取 企业联系方式列表
    **/
    public List<String> getPhoneList() {
      return phoneList;
    }
    /**
    *   设置 无用
    **/
    public void setBaiduAuthURLWWW(String baiduAuthURLWWW) {
      this.baiduAuthURLWWW = baiduAuthURLWWW;
    }
    /**
    *   获取 无用
    **/
    public String getBaiduAuthURLWWW() {
      return baiduAuthURLWWW;
    }
    /**
    *   设置 uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 uuid
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 无用
    **/
    public void setPricingPackage(Integer pricingPackage) {
      this.pricingPackage = pricingPackage;
    }
    /**
    *   获取 无用
    **/
    public Integer getPricingPackage() {
      return pricingPackage;
    }
    /**
    *   设置 负责人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 负责人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 执业状态、状态
    **/
    public void setPracticeState(String practiceState) {
      this.practiceState = practiceState;
    }
    /**
    *   获取 执业状态、状态
    **/
    public String getPracticeState() {
      return practiceState;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 微博(官方未定义字段类型 暂定为String)
    **/
    public void setWeibo(String weibo) {
      this.weibo = weibo;
    }
    /**
    *   获取 微博(官方未定义字段类型 暂定为String)
    **/
    public String getWeibo() {
      return weibo;
    }
    /**
    *   设置 总所/分所
    **/
    public void setHeadquartersBranch(String headquartersBranch) {
      this.headquartersBranch = headquartersBranch;
    }
    /**
    *   获取 总所/分所
    **/
    public String getHeadquartersBranch() {
      return headquartersBranch;
    }
    /**
    *   设置 业务特长
    **/
    public void setBusinessExpertise(String businessExpertise) {
      this.businessExpertise = businessExpertise;
    }
    /**
    *   获取 业务特长
    **/
    public String getBusinessExpertise() {
      return businessExpertise;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 无用
    **/
    public void setBaiduAuthURLWAP(String baiduAuthURLWAP) {
      this.baiduAuthURLWAP = baiduAuthURLWAP;
    }
    /**
    *   获取 无用
    **/
    public String getBaiduAuthURLWAP() {
      return baiduAuthURLWAP;
    }
    /**
    *   设置 
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 无用
    **/
    public void setIsClaimed(Integer isClaimed) {
      this.isClaimed = isClaimed;
    }
    /**
    *   获取 无用
    **/
    public Integer getIsClaimed() {
      return isClaimed;
    }
    /**
    *   设置 信用等级
    **/
    public void setCreditRating(String creditRating) {
      this.creditRating = creditRating;
    }
    /**
    *   获取 信用等级
    **/
    public String getCreditRating() {
      return creditRating;
    }
    /**
    *   设置 邮箱
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱
    **/
    public String getEmail() {
      return email;
    }
    /**
    *   设置 律所简介
    **/
    public void setSummary(String summary) {
      this.summary = summary;
    }
    /**
    *   获取 律所简介
    **/
    public String getSummary() {
      return summary;
    }
    /**
    *   设置 组织形式
    **/
    public void setOrganizationForm(String organizationForm) {
      this.organizationForm = organizationForm;
    }
    /**
    *   获取 组织形式
    **/
    public String getOrganizationForm() {
      return organizationForm;
    }
    /**
    *   设置 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public void setEntityType(Integer entityType) {
      this.entityType = entityType;
    }
    /**
    *   获取 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public Integer getEntityType() {
      return entityType;
    }
    /**
    *   设置 是否有年报
    **/
    public void setHaveReport(Boolean haveReport) {
      this.haveReport = haveReport;
    }
    /**
    *   获取 是否有年报
    **/
    public Boolean getHaveReport() {
      return haveReport;
    }
    /**
    *   设置 税务登记号
    **/
    public void setTaxNumber(String taxNumber) {
      this.taxNumber = taxNumber;
    }
    /**
    *   获取 税务登记号
    **/
    public String getTaxNumber() {
      return taxNumber;
    }
    /**
    *   设置 地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 
    **/
    public void setDateOfIssue(String dateOfIssue) {
      this.dateOfIssue = dateOfIssue;
    }
    /**
    *   获取 
    **/
    public String getDateOfIssue() {
      return dateOfIssue;
    }
    /**
    *   设置 主管机关
    **/
    public void setAuthorities(String authorities) {
      this.authorities = authorities;
    }
    /**
    *   获取 主管机关
    **/
    public String getAuthorities() {
      return authorities;
    }
    /**
    *   设置 网址
    **/
    public void setWebsiteList(String websiteList) {
      this.websiteList = websiteList;
    }
    /**
    *   获取 网址
    **/
    public String getWebsiteList() {
      return websiteList;
    }
    /**
    *   设置 企业联系方式
    **/
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    /**
    *   获取 企业联系方式
    **/
    public String getPhoneNumber() {
      return phoneNumber;
    }
    /**
    *   设置 地址
    **/
    public void setRladdress(String rladdress) {
      this.rladdress = rladdress;
    }
    /**
    *   获取 地址
    **/
    public String getRladdress() {
      return rladdress;
    }
    /**
    *   设置 证书
    **/
    public void setPermit(String permit) {
      this.permit = permit;
    }
    /**
    *   获取 证书
    **/
    public String getPermit() {
      return permit;
    }
    /**
    *   设置 律所名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 律所名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 成立日期
    **/
    public void setDateOfEstablishment(String dateOfEstablishment) {
      this.dateOfEstablishment = dateOfEstablishment;
    }
    /**
    *   获取 成立日期
    **/
    public String getDateOfEstablishment() {
      return dateOfEstablishment;
    }



}

