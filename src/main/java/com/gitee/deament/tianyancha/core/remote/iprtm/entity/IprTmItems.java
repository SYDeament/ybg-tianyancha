package com.gitee.deament.tianyancha.core.remote.iprtm.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*商标信息
* 属于IprTm
* /services/open/ipr/tm/2.0
*@author deament
**/

public class IprTmItems implements Serializable{

    /**
     *    注册号
    **/
    @JSONField(name="regNo")
    private String regNo;
    /**
     *    商标图片
    **/
    @JSONField(name="tmPic")
    private String tmPic;
    /**
     *    无用
    **/
    @JSONField(name="searchType")
    private String searchType;
    /**
     *    跳转天眼查链接
    **/
    @JSONField(name="connList")
    private List<String> connList;
    /**
     *    无用
    **/
    @JSONField(name="_type")
    private String _type;
    /**
     *    国际分类code
    **/
    @JSONField(name="tmClass")
    private String tmClass;
    /**
     *    申请日期
    **/
    @JSONField(name="appDate")
    private String appDate;
    /**
     *    国际分类
    **/
    @JSONField(name="intCls")
    private String intCls;
    /**
     *    申请人
    **/
    @JSONField(name="applicantCn")
    private String applicantCn;
    /**
     *    唯一标识符
    **/
    @JSONField(name="uni")
    private String uni;
    /**
     *    商标名称
    **/
    @JSONField(name="tmName")
    private String tmName;
    /**
     *    商标流程（内部使用，已经废弃）
    **/
    @JSONField(name="tmFlow")
    private String tmFlow;
    /**
     *    无用
    **/
    @JSONField(name="eventTime")
    private String eventTime;
    /**
     *    商标id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    流程类别
    **/
    @JSONField(name="category")
    private String category;
    /**
     *    状态
    **/
    @JSONField(name="status")
    private String status;


    /**
    *   设置 注册号
    **/
    public void setRegNo(String regNo) {
      this.regNo = regNo;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNo() {
      return regNo;
    }
    /**
    *   设置 商标图片
    **/
    public void setTmPic(String tmPic) {
      this.tmPic = tmPic;
    }
    /**
    *   获取 商标图片
    **/
    public String getTmPic() {
      return tmPic;
    }
    /**
    *   设置 无用
    **/
    public void setSearchType(String searchType) {
      this.searchType = searchType;
    }
    /**
    *   获取 无用
    **/
    public String getSearchType() {
      return searchType;
    }
    /**
    *   设置 跳转天眼查链接
    **/
    public void setConnList(List<String> connList) {
      this.connList = connList;
    }
    /**
    *   获取 跳转天眼查链接
    **/
    public List<String> getConnList() {
      return connList;
    }
    /**
    *   设置 无用
    **/
    public void set_type(String _type) {
      this._type = _type;
    }
    /**
    *   获取 无用
    **/
    public String get_type() {
      return _type;
    }
    /**
    *   设置 国际分类code
    **/
    public void setTmClass(String tmClass) {
      this.tmClass = tmClass;
    }
    /**
    *   获取 国际分类code
    **/
    public String getTmClass() {
      return tmClass;
    }
    /**
    *   设置 申请日期
    **/
    public void setAppDate(String appDate) {
      this.appDate = appDate;
    }
    /**
    *   获取 申请日期
    **/
    public String getAppDate() {
      return appDate;
    }
    /**
    *   设置 国际分类
    **/
    public void setIntCls(String intCls) {
      this.intCls = intCls;
    }
    /**
    *   获取 国际分类
    **/
    public String getIntCls() {
      return intCls;
    }
    /**
    *   设置 申请人
    **/
    public void setApplicantCn(String applicantCn) {
      this.applicantCn = applicantCn;
    }
    /**
    *   获取 申请人
    **/
    public String getApplicantCn() {
      return applicantCn;
    }
    /**
    *   设置 唯一标识符
    **/
    public void setUni(String uni) {
      this.uni = uni;
    }
    /**
    *   获取 唯一标识符
    **/
    public String getUni() {
      return uni;
    }
    /**
    *   设置 商标名称
    **/
    public void setTmName(String tmName) {
      this.tmName = tmName;
    }
    /**
    *   获取 商标名称
    **/
    public String getTmName() {
      return tmName;
    }
    /**
    *   设置 商标流程（内部使用，已经废弃）
    **/
    public void setTmFlow(String tmFlow) {
      this.tmFlow = tmFlow;
    }
    /**
    *   获取 商标流程（内部使用，已经废弃）
    **/
    public String getTmFlow() {
      return tmFlow;
    }
    /**
    *   设置 无用
    **/
    public void setEventTime(String eventTime) {
      this.eventTime = eventTime;
    }
    /**
    *   获取 无用
    **/
    public String getEventTime() {
      return eventTime;
    }
    /**
    *   设置 商标id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 商标id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 流程类别
    **/
    public void setCategory(String category) {
      this.category = category;
    }
    /**
    *   获取 流程类别
    **/
    public String getCategory() {
      return category;
    }
    /**
    *   设置 状态
    **/
    public void setStatus(String status) {
      this.status = status;
    }
    /**
    *   获取 状态
    **/
    public String getStatus() {
      return status;
    }



}

