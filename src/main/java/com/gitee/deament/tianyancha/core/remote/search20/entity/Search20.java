package com.gitee.deament.tianyancha.core.remote.search20.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.search20.entity.Search20Items;
/**
*搜索
* 可以通过关键词获取企业列表，企业列表包括公司名称或ID、类型、成立日期、经营状态、统一社会信用代码等字段的详细信息
* /services/open/search/2.0
*@author deament
**/

public class Search20 implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<Search20Items> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<Search20Items> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<Search20Items> getItems() {
      return items;
    }



}

