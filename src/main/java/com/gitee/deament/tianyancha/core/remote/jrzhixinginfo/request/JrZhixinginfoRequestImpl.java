package com.gitee.deament.tianyancha.core.remote.jrzhixinginfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrzhixinginfo.entity.JrZhixinginfo;
import com.gitee.deament.tianyancha.core.remote.jrzhixinginfo.dto.JrZhixinginfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*被执行人
* 可以通过公司名称或ID获取被执行人信息，被执行人信息包括执行法院、案件内容、执行标的、被执行人名称、组织机构代码等字段的详细信息
* /services/open/jr/zhixinginfo/2.0
*@author deament
**/
@Component("jrZhixinginfoRequestImpl")
public class JrZhixinginfoRequestImpl extends BaseRequestImpl<JrZhixinginfo,JrZhixinginfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/jr/zhixinginfo/2.0";
    }
}

