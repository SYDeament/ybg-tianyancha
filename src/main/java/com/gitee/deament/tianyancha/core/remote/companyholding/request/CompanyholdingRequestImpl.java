package com.gitee.deament.tianyancha.core.remote.companyholding.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.companyholding.entity.Companyholding;
import com.gitee.deament.tianyancha.core.remote.companyholding.dto.CompanyholdingDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*实际控制权
* 可以通过公司名称或ID获取股权向下穿透后识别的当前企业实际控制的企业，包括对应公司名称或ID、出资比例、投资链等
* /services/v4/open/companyholding
*@author deament
**/
@Component("companyholdingRequestImpl")
public class CompanyholdingRequestImpl extends BaseRequestImpl<Companyholding,CompanyholdingDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/companyholding";
    }
}

