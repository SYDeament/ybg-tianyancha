package com.gitee.deament.tianyancha.core.remote.humanriskinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.humanriskinfo.entity.HumanRiskInfoList;
import com.gitee.deament.tianyancha.core.remote.humanriskinfo.entity.HumanRiskInfoList;
import com.gitee.deament.tianyancha.core.remote.humanriskinfo.entity.HumanRiskInfo_child;
/**
*人员天眼风险
* 可以通过公司名称或ID和人名获取人员相关天眼风险列表，包括人员周边/预警风险信息
* /services/v4/open/humanRiskInfo
*@author deament
**/

public class HumanRiskInfo implements Serializable{

    /**
     *    
    **/
    @JSONField(name="_child")
    private HumanRiskInfo_child _child;


    /**
    *   设置 
    **/
    public void set_child(HumanRiskInfo_child _child) {
      this._child = _child;
    }
    /**
    *   获取 
    **/
    public HumanRiskInfo_child get_child() {
      return _child;
    }



}

