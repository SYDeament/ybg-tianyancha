package com.gitee.deament.tianyancha.core.remote.hijudicial.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史司法协助详情
* 属于HiJudicial
* /services/open/hi/judicial/detail/2.0
*@author deament
**/

public class HiJudicialFrozen implements Serializable{

    /**
     *    执行通知书文号
    **/
    @JSONField(name="executeNoticeNum")
    private String executeNoticeNum;
    /**
     *    冻结期限
    **/
    @JSONField(name="period")
    private String period;
    /**
     *    冻结期限至
    **/
    @JSONField(name="toDate")
    private Integer toDate;
    /**
     *    被执行人公司id
    **/
    @JSONField(name="executedPersonCid")
    private Integer executedPersonCid;
    /**
     *    执行裁定书文号
    **/
    @JSONField(name="executeOrderNum")
    private String executeOrderNum;
    /**
     *    被执行人证照号码
    **/
    @JSONField(name="licenseNum")
    private String licenseNum;
    /**
     *    被执行人持有股权、其它投资权益的数额
    **/
    @JSONField(name="equityAmountOther")
    private String equityAmountOther;
    /**
     *    被执行人id
    **/
    @JSONField(name="executedPersonHid")
    private Integer executedPersonHid;
    /**
     *    被执行人
    **/
    @JSONField(name="executedPerson")
    private String executedPerson;
    /**
     *    冻结期限自
    **/
    @JSONField(name="fromDate")
    private Integer fromDate;
    /**
     *    被执行人证照种类
    **/
    @JSONField(name="licenseType")
    private String licenseType;
    /**
     *    执行事项
    **/
    @JSONField(name="implementationMatters")
    private String implementationMatters;
    /**
     *    被执行人类型1公司，2人
    **/
    @JSONField(name="executedPersonType")
    private String executedPersonType;
    /**
     *    执行法院
    **/
    @JSONField(name="executiveCourt")
    private String executiveCourt;


    /**
    *   设置 执行通知书文号
    **/
    public void setExecuteNoticeNum(String executeNoticeNum) {
      this.executeNoticeNum = executeNoticeNum;
    }
    /**
    *   获取 执行通知书文号
    **/
    public String getExecuteNoticeNum() {
      return executeNoticeNum;
    }
    /**
    *   设置 冻结期限
    **/
    public void setPeriod(String period) {
      this.period = period;
    }
    /**
    *   获取 冻结期限
    **/
    public String getPeriod() {
      return period;
    }
    /**
    *   设置 冻结期限至
    **/
    public void setToDate(Integer toDate) {
      this.toDate = toDate;
    }
    /**
    *   获取 冻结期限至
    **/
    public Integer getToDate() {
      return toDate;
    }
    /**
    *   设置 被执行人公司id
    **/
    public void setExecutedPersonCid(Integer executedPersonCid) {
      this.executedPersonCid = executedPersonCid;
    }
    /**
    *   获取 被执行人公司id
    **/
    public Integer getExecutedPersonCid() {
      return executedPersonCid;
    }
    /**
    *   设置 执行裁定书文号
    **/
    public void setExecuteOrderNum(String executeOrderNum) {
      this.executeOrderNum = executeOrderNum;
    }
    /**
    *   获取 执行裁定书文号
    **/
    public String getExecuteOrderNum() {
      return executeOrderNum;
    }
    /**
    *   设置 被执行人证照号码
    **/
    public void setLicenseNum(String licenseNum) {
      this.licenseNum = licenseNum;
    }
    /**
    *   获取 被执行人证照号码
    **/
    public String getLicenseNum() {
      return licenseNum;
    }
    /**
    *   设置 被执行人持有股权、其它投资权益的数额
    **/
    public void setEquityAmountOther(String equityAmountOther) {
      this.equityAmountOther = equityAmountOther;
    }
    /**
    *   获取 被执行人持有股权、其它投资权益的数额
    **/
    public String getEquityAmountOther() {
      return equityAmountOther;
    }
    /**
    *   设置 被执行人id
    **/
    public void setExecutedPersonHid(Integer executedPersonHid) {
      this.executedPersonHid = executedPersonHid;
    }
    /**
    *   获取 被执行人id
    **/
    public Integer getExecutedPersonHid() {
      return executedPersonHid;
    }
    /**
    *   设置 被执行人
    **/
    public void setExecutedPerson(String executedPerson) {
      this.executedPerson = executedPerson;
    }
    /**
    *   获取 被执行人
    **/
    public String getExecutedPerson() {
      return executedPerson;
    }
    /**
    *   设置 冻结期限自
    **/
    public void setFromDate(Integer fromDate) {
      this.fromDate = fromDate;
    }
    /**
    *   获取 冻结期限自
    **/
    public Integer getFromDate() {
      return fromDate;
    }
    /**
    *   设置 被执行人证照种类
    **/
    public void setLicenseType(String licenseType) {
      this.licenseType = licenseType;
    }
    /**
    *   获取 被执行人证照种类
    **/
    public String getLicenseType() {
      return licenseType;
    }
    /**
    *   设置 执行事项
    **/
    public void setImplementationMatters(String implementationMatters) {
      this.implementationMatters = implementationMatters;
    }
    /**
    *   获取 执行事项
    **/
    public String getImplementationMatters() {
      return implementationMatters;
    }
    /**
    *   设置 被执行人类型1公司，2人
    **/
    public void setExecutedPersonType(String executedPersonType) {
      this.executedPersonType = executedPersonType;
    }
    /**
    *   获取 被执行人类型1公司，2人
    **/
    public String getExecutedPersonType() {
      return executedPersonType;
    }
    /**
    *   设置 执行法院
    **/
    public void setExecutiveCourt(String executiveCourt) {
      this.executiveCourt = executiveCourt;
    }
    /**
    *   获取 执行法院
    **/
    public String getExecutiveCourt() {
      return executiveCourt;
    }



}

