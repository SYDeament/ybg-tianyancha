package com.gitee.deament.tianyancha.core.remote.mrliquidating.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrliquidating.entity.MrLiquidating;
import com.gitee.deament.tianyancha.core.remote.mrliquidating.dto.MrLiquidatingDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*清算信息
* 可以通过公司名称或ID获取企业清算信息，企业清算信息包括清算组负责人、清算组成员
* /services/open/mr/liquidating/2.0
*@author deament
**/
@Component("mrLiquidatingRequestImpl")
public class MrLiquidatingRequestImpl extends BaseRequestImpl<MrLiquidating,MrLiquidatingDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/liquidating/2.0";
    }
}

