package com.gitee.deament.tianyancha.core.remote.iprtm.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*商标信息详情
* 属于IprTm
* /services/open/ipr/tm/detail/2.0
*@author deament
**/

public class IprTmDetail implements Serializable{

    /**
     *    商标注册号
    **/
    @JSONField(name="regNo")
    private String regNo;
    /**
     *    商标代理/办理机构
    **/
    @JSONField(name="agent")
    private String agent;
    /**
     *    商标图片
    **/
    @JSONField(name="tmPic")
    private String tmPic;
    /**
     *    商标状态展示文案
    **/
    @JSONField(name="statusStr")
    private String statusStr;
    /**
     *    注册公告日期
    **/
    @JSONField(name="regDate")
    private Long regDate;
    /**
     *    商标申请日期
    **/
    @JSONField(name="appDate")
    private Long appDate;
    /**
     *    商标国际分类
    **/
    @JSONField(name="intCls")
    private Integer intCls;
    /**
     *    商标来源
    **/
    @JSONField(name="source")
    private String source;
    /**
     *    专用权期限开始日期
    **/
    @JSONField(name="privateDateStart")
    private Long privateDateStart;
    /**
     *    商标状态code
    **/
    @JSONField(name="statusNew")
    private Integer statusNew;
    /**
     *    商标国际分类描述
    **/
    @JSONField(name="clsDesc1")
    private String clsDesc1;
    /**
     *    商标国际分类描述
    **/
    @JSONField(name="clsDesc2")
    private List<String> clsDesc2;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    专用权期限结束日期
    **/
    @JSONField(name="privateDateEnd")
    private Long privateDateEnd;
    /**
     *    商标注册地址
    **/
    @JSONField(name="addressCn")
    private String addressCn;
    /**
     *    初审公告期号
    **/
    @JSONField(name="announcemenIssue")
    private Integer announcemenIssue;
    /**
     *    初审公告日期
    **/
    @JSONField(name="announcementDate")
    private Long announcementDate;
    /**
     *    更新时间
    **/
    @JSONField(name="updateTime")
    private Long updateTime;
    /**
     *    商标申请人
    **/
    @JSONField(name="applicantCn")
    private String applicantCn;
    /**
     *    商标国际分类文案
    **/
    @JSONField(name="clsStr")
    private String clsStr;
    /**
     *    商标名
    **/
    @JSONField(name="tmName")
    private String tmName;
    /**
     *    0-正常 1-已删除
    **/
    @JSONField(name="deleted")
    private Integer deleted;
    /**
     *    商标流程文案
    **/
    @JSONField(name="categoryStr")
    private String categoryStr;
    /**
     *    注册公告期号
    **/
    @JSONField(name="regIssue")
    private Integer regIssue;
    /**
     *    商标流程code
    **/
    @JSONField(name="category")
    private Integer category;
    /**
     *    是否共有商标
    **/
    @JSONField(name="isShared")
    private Boolean isShared;


    /**
    *   设置 商标注册号
    **/
    public void setRegNo(String regNo) {
      this.regNo = regNo;
    }
    /**
    *   获取 商标注册号
    **/
    public String getRegNo() {
      return regNo;
    }
    /**
    *   设置 商标代理/办理机构
    **/
    public void setAgent(String agent) {
      this.agent = agent;
    }
    /**
    *   获取 商标代理/办理机构
    **/
    public String getAgent() {
      return agent;
    }
    /**
    *   设置 商标图片
    **/
    public void setTmPic(String tmPic) {
      this.tmPic = tmPic;
    }
    /**
    *   获取 商标图片
    **/
    public String getTmPic() {
      return tmPic;
    }
    /**
    *   设置 商标状态展示文案
    **/
    public void setStatusStr(String statusStr) {
      this.statusStr = statusStr;
    }
    /**
    *   获取 商标状态展示文案
    **/
    public String getStatusStr() {
      return statusStr;
    }
    /**
    *   设置 注册公告日期
    **/
    public void setRegDate(Long regDate) {
      this.regDate = regDate;
    }
    /**
    *   获取 注册公告日期
    **/
    public Long getRegDate() {
      return regDate;
    }
    /**
    *   设置 商标申请日期
    **/
    public void setAppDate(Long appDate) {
      this.appDate = appDate;
    }
    /**
    *   获取 商标申请日期
    **/
    public Long getAppDate() {
      return appDate;
    }
    /**
    *   设置 商标国际分类
    **/
    public void setIntCls(Integer intCls) {
      this.intCls = intCls;
    }
    /**
    *   获取 商标国际分类
    **/
    public Integer getIntCls() {
      return intCls;
    }
    /**
    *   设置 商标来源
    **/
    public void setSource(String source) {
      this.source = source;
    }
    /**
    *   获取 商标来源
    **/
    public String getSource() {
      return source;
    }
    /**
    *   设置 专用权期限开始日期
    **/
    public void setPrivateDateStart(Long privateDateStart) {
      this.privateDateStart = privateDateStart;
    }
    /**
    *   获取 专用权期限开始日期
    **/
    public Long getPrivateDateStart() {
      return privateDateStart;
    }
    /**
    *   设置 商标状态code
    **/
    public void setStatusNew(Integer statusNew) {
      this.statusNew = statusNew;
    }
    /**
    *   获取 商标状态code
    **/
    public Integer getStatusNew() {
      return statusNew;
    }
    /**
    *   设置 商标国际分类描述
    **/
    public void setClsDesc1(String clsDesc1) {
      this.clsDesc1 = clsDesc1;
    }
    /**
    *   获取 商标国际分类描述
    **/
    public String getClsDesc1() {
      return clsDesc1;
    }
    /**
    *   设置 商标国际分类描述
    **/
    public void setClsDesc2(List<String> clsDesc2) {
      this.clsDesc2 = clsDesc2;
    }
    /**
    *   获取 商标国际分类描述
    **/
    public List<String> getClsDesc2() {
      return clsDesc2;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 专用权期限结束日期
    **/
    public void setPrivateDateEnd(Long privateDateEnd) {
      this.privateDateEnd = privateDateEnd;
    }
    /**
    *   获取 专用权期限结束日期
    **/
    public Long getPrivateDateEnd() {
      return privateDateEnd;
    }
    /**
    *   设置 商标注册地址
    **/
    public void setAddressCn(String addressCn) {
      this.addressCn = addressCn;
    }
    /**
    *   获取 商标注册地址
    **/
    public String getAddressCn() {
      return addressCn;
    }
    /**
    *   设置 初审公告期号
    **/
    public void setAnnouncemenIssue(Integer announcemenIssue) {
      this.announcemenIssue = announcemenIssue;
    }
    /**
    *   获取 初审公告期号
    **/
    public Integer getAnnouncemenIssue() {
      return announcemenIssue;
    }
    /**
    *   设置 初审公告日期
    **/
    public void setAnnouncementDate(Long announcementDate) {
      this.announcementDate = announcementDate;
    }
    /**
    *   获取 初审公告日期
    **/
    public Long getAnnouncementDate() {
      return announcementDate;
    }
    /**
    *   设置 更新时间
    **/
    public void setUpdateTime(Long updateTime) {
      this.updateTime = updateTime;
    }
    /**
    *   获取 更新时间
    **/
    public Long getUpdateTime() {
      return updateTime;
    }
    /**
    *   设置 商标申请人
    **/
    public void setApplicantCn(String applicantCn) {
      this.applicantCn = applicantCn;
    }
    /**
    *   获取 商标申请人
    **/
    public String getApplicantCn() {
      return applicantCn;
    }
    /**
    *   设置 商标国际分类文案
    **/
    public void setClsStr(String clsStr) {
      this.clsStr = clsStr;
    }
    /**
    *   获取 商标国际分类文案
    **/
    public String getClsStr() {
      return clsStr;
    }
    /**
    *   设置 商标名
    **/
    public void setTmName(String tmName) {
      this.tmName = tmName;
    }
    /**
    *   获取 商标名
    **/
    public String getTmName() {
      return tmName;
    }
    /**
    *   设置 0-正常 1-已删除
    **/
    public void setDeleted(Integer deleted) {
      this.deleted = deleted;
    }
    /**
    *   获取 0-正常 1-已删除
    **/
    public Integer getDeleted() {
      return deleted;
    }
    /**
    *   设置 商标流程文案
    **/
    public void setCategoryStr(String categoryStr) {
      this.categoryStr = categoryStr;
    }
    /**
    *   获取 商标流程文案
    **/
    public String getCategoryStr() {
      return categoryStr;
    }
    /**
    *   设置 注册公告期号
    **/
    public void setRegIssue(Integer regIssue) {
      this.regIssue = regIssue;
    }
    /**
    *   获取 注册公告期号
    **/
    public Integer getRegIssue() {
      return regIssue;
    }
    /**
    *   设置 商标流程code
    **/
    public void setCategory(Integer category) {
      this.category = category;
    }
    /**
    *   获取 商标流程code
    **/
    public Integer getCategory() {
      return category;
    }
    /**
    *   设置 是否共有商标
    **/
    public void setIsShared(Boolean isShared) {
      this.isShared = isShared;
    }
    /**
    *   获取 是否共有商标
    **/
    public Boolean getIsShared() {
      return isShared;
    }



}

