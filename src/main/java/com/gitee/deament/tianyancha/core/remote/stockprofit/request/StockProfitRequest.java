package com.gitee.deament.tianyancha.core.remote.stockprofit.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockprofit.entity.StockProfit;
import com.gitee.deament.tianyancha.core.remote.stockprofit.dto.StockProfitDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*利润表
* 可以通过公司名称或ID获取上市公司利润表数据，利润表数据包括营业总收入、营业总成本、其他经营收益、营业利润等
* /services/open/stock/profit/2.0
*@author deament
**/

public interface StockProfitRequest extends BaseRequest<StockProfitDTO ,StockProfit>{

}

