package com.gitee.deament.tianyancha.core.remote.oibusinessevent.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商追踪
* 属于OiBusinessEvent
* /services/open/oi/businessEvent/2.0
*@author deament
**/

public class OiBusinessEventItems implements Serializable{

    /**
     *    简称
    **/
    @JSONField(name="target_alias")
    private String target_alias;
    /**
     *    被投资企业名称
    **/
    @JSONField(name="target_name")
    private String target_name;
    /**
     *    logo
    **/
    @JSONField(name="target_logo")
    private String target_logo;
    /**
     *    投资比例
    **/
    @JSONField(name="percentage")
    private String percentage;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    投资日期
    **/
    @JSONField(name="invest_time")
    private String invest_time;
    /**
     *    被投资企业id
    **/
    @JSONField(name="target_id")
    private String target_id;
    /**
     *    投资企业id
    **/
    @JSONField(name="source_id")
    private String source_id;
    /**
     *    投资企业名称
    **/
    @JSONField(name="source_name")
    private String source_name;
    /**
     *    logo
    **/
    @JSONField(name="source_logo")
    private String source_logo;


    /**
    *   设置 简称
    **/
    public void setTarget_alias(String target_alias) {
      this.target_alias = target_alias;
    }
    /**
    *   获取 简称
    **/
    public String getTarget_alias() {
      return target_alias;
    }
    /**
    *   设置 被投资企业名称
    **/
    public void setTarget_name(String target_name) {
      this.target_name = target_name;
    }
    /**
    *   获取 被投资企业名称
    **/
    public String getTarget_name() {
      return target_name;
    }
    /**
    *   设置 logo
    **/
    public void setTarget_logo(String target_logo) {
      this.target_logo = target_logo;
    }
    /**
    *   获取 logo
    **/
    public String getTarget_logo() {
      return target_logo;
    }
    /**
    *   设置 投资比例
    **/
    public void setPercentage(String percentage) {
      this.percentage = percentage;
    }
    /**
    *   获取 投资比例
    **/
    public String getPercentage() {
      return percentage;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 投资日期
    **/
    public void setInvest_time(String invest_time) {
      this.invest_time = invest_time;
    }
    /**
    *   获取 投资日期
    **/
    public String getInvest_time() {
      return invest_time;
    }
    /**
    *   设置 被投资企业id
    **/
    public void setTarget_id(String target_id) {
      this.target_id = target_id;
    }
    /**
    *   获取 被投资企业id
    **/
    public String getTarget_id() {
      return target_id;
    }
    /**
    *   设置 投资企业id
    **/
    public void setSource_id(String source_id) {
      this.source_id = source_id;
    }
    /**
    *   获取 投资企业id
    **/
    public String getSource_id() {
      return source_id;
    }
    /**
    *   设置 投资企业名称
    **/
    public void setSource_name(String source_name) {
      this.source_name = source_name;
    }
    /**
    *   获取 投资企业名称
    **/
    public String getSource_name() {
      return source_name;
    }
    /**
    *   设置 logo
    **/
    public void setSource_logo(String source_logo) {
      this.source_logo = source_logo;
    }
    /**
    *   获取 logo
    **/
    public String getSource_logo() {
      return source_logo;
    }



}

