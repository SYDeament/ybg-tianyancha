package com.gitee.deament.tianyancha.core.remote.mcheckinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcheckinfo.entity.MCheckInfoItems;
/**
*抽查检查
* 可以通过公司名称或ID获取企业抽查检查信息，企业抽查检查信息包括类型、结果、实施机关等字段的详细信息
* /services/open/m/checkInfo/2.0
*@author deament
**/

public class MCheckInfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MCheckInfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MCheckInfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MCheckInfoItems> getItems() {
      return items;
    }



}

