package com.gitee.deament.tianyancha.core.remote.cdtags.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*获取标签
* 可以通过公司名称或ID获取企业标签，企业标签包括领域分类、主营业务、榜单等
* /services/open/cd/tags/2.0
*@author deament
**/

@TYCURL(value="/services/open/cd/tags/2.0")
public class CdTagsDTO implements Serializable{

    /**
     *    品牌id
     *
    **/
    @ParamRequire(require = true)
    private String id;


    /**
    *   设置 品牌id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 品牌id
    **/
    public String getId() {
      return id;
    }



}

