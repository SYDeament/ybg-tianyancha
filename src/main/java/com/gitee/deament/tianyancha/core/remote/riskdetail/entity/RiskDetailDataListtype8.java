package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype8 implements Serializable{

    /**
     *    文书类型
    **/
    @JSONField(name="doctype")
    private String doctype;
    /**
     *    提交时间
    **/
    @JSONField(name="submittime")
    private Long submittime;
    /**
     *    代理人
    **/
    @JSONField(name="agent")
    private String agent;
    /**
     *    天眼查显示url
    **/
    @JSONField(name="lawsuitUrl")
    private String lawsuitUrl;
    /**
     *    案件类型
    **/
    @JSONField(name="casetype")
    private String casetype;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    唯一标识符
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    案件号
    **/
    @JSONField(name="caseno")
    private String caseno;
    /**
     *    原文链接地址
    **/
    @JSONField(name="url")
    private String url;


    /**
    *   设置 文书类型
    **/
    public void setDoctype(String doctype) {
      this.doctype = doctype;
    }
    /**
    *   获取 文书类型
    **/
    public String getDoctype() {
      return doctype;
    }
    /**
    *   设置 提交时间
    **/
    public void setSubmittime(Long submittime) {
      this.submittime = submittime;
    }
    /**
    *   获取 提交时间
    **/
    public Long getSubmittime() {
      return submittime;
    }
    /**
    *   设置 代理人
    **/
    public void setAgent(String agent) {
      this.agent = agent;
    }
    /**
    *   获取 代理人
    **/
    public String getAgent() {
      return agent;
    }
    /**
    *   设置 天眼查显示url
    **/
    public void setLawsuitUrl(String lawsuitUrl) {
      this.lawsuitUrl = lawsuitUrl;
    }
    /**
    *   获取 天眼查显示url
    **/
    public String getLawsuitUrl() {
      return lawsuitUrl;
    }
    /**
    *   设置 案件类型
    **/
    public void setCasetype(String casetype) {
      this.casetype = casetype;
    }
    /**
    *   获取 案件类型
    **/
    public String getCasetype() {
      return casetype;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 唯一标识符
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 唯一标识符
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 案件号
    **/
    public void setCaseno(String caseno) {
      this.caseno = caseno;
    }
    /**
    *   获取 案件号
    **/
    public String getCaseno() {
      return caseno;
    }
    /**
    *   设置 原文链接地址
    **/
    public void setUrl(String url) {
      this.url = url;
    }
    /**
    *   获取 原文链接地址
    **/
    public String getUrl() {
      return url;
    }



}

