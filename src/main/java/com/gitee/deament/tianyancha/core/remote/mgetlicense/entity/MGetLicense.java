package com.gitee.deament.tianyancha.core.remote.mgetlicense.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mgetlicense.entity.MGetLicenseItems;
/**
*行政许可-工商局
* 可以通过公司名称或ID获取企业行政许可信息，企业行政许可信息包括行政许可决定文书号、许可文件名称、许可机关等字段的详细信息
* /services/open/m/getLicense/2.0
*@author deament
**/

public class MGetLicense implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MGetLicenseItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MGetLicenseItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MGetLicenseItems> getItems() {
      return items;
    }



}

