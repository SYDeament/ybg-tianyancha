package com.gitee.deament.tianyancha.core.remote.partners.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*人员所有合作伙伴
* 属于Partners
* /services/v4/open/partners
*@author deament
**/

public class PartnersOffice implements Serializable{

    /**
     *    省份
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    无用
    **/
    @JSONField(name="score")
    private Integer score;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    无用
    **/
    @JSONField(name="state")
    private String state;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 省份
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 省份
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 无用
    **/
    public void setScore(Integer score) {
      this.score = score;
    }
    /**
    *   获取 无用
    **/
    public Integer getScore() {
      return score;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 无用
    **/
    public void setState(String state) {
      this.state = state;
    }
    /**
    *   获取 无用
    **/
    public String getState() {
      return state;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

