package com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity.MrMortgageInfo;
import com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.dto.MrMortgageInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*动产抵押
* 可以通过公司名称或ID获取企业动产抵押公告信息，企业动产抵押公告信息包括被担保债权类型、数额、登记机关等字段信息
* /services/open/mr/mortgageInfo/2.0
*@author deament
**/
@Component("mrMortgageInfoRequestImpl")
public class MrMortgageInfoRequestImpl extends BaseRequestImpl<MrMortgageInfo,MrMortgageInfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/mortgageInfo/2.0";
    }
}

