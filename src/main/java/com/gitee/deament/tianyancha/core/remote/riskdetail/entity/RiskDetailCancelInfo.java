package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailCancelInfo implements Serializable{

    /**
     *    注销日期
    **/
    @JSONField(name="cancelDate")
    private String cancelDate;
    /**
     *    注销原因
    **/
    @JSONField(name="cancelReason")
    private String cancelReason;


    /**
    *   设置 注销日期
    **/
    public void setCancelDate(String cancelDate) {
      this.cancelDate = cancelDate;
    }
    /**
    *   获取 注销日期
    **/
    public String getCancelDate() {
      return cancelDate;
    }
    /**
    *   设置 注销原因
    **/
    public void setCancelReason(String cancelReason) {
      this.cancelReason = cancelReason;
    }
    /**
    *   获取 注销原因
    **/
    public String getCancelReason() {
      return cancelReason;
    }



}

