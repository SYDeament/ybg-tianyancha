package com.gitee.deament.tianyancha.core.remote.mimportandexport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*进出口信用
* 属于MImportAndExport
* /services/open/m/importAndExport/2.0
*@author deament
**/

public class MImportAndExportSanction implements Serializable{

    /**
     *    行政处罚决定书编号
    **/
    @JSONField(name="decisionNumber")
    private String decisionNumber;
    /**
     *    处罚日期
    **/
    @JSONField(name="penaltyDate")
    private String penaltyDate;
    /**
     *    案件性质
    **/
    @JSONField(name="natureOfCase")
    private String natureOfCase;
    /**
     *    当事人
    **/
    @JSONField(name="party")
    private String party;


    /**
    *   设置 行政处罚决定书编号
    **/
    public void setDecisionNumber(String decisionNumber) {
      this.decisionNumber = decisionNumber;
    }
    /**
    *   获取 行政处罚决定书编号
    **/
    public String getDecisionNumber() {
      return decisionNumber;
    }
    /**
    *   设置 处罚日期
    **/
    public void setPenaltyDate(String penaltyDate) {
      this.penaltyDate = penaltyDate;
    }
    /**
    *   获取 处罚日期
    **/
    public String getPenaltyDate() {
      return penaltyDate;
    }
    /**
    *   设置 案件性质
    **/
    public void setNatureOfCase(String natureOfCase) {
      this.natureOfCase = natureOfCase;
    }
    /**
    *   获取 案件性质
    **/
    public String getNatureOfCase() {
      return natureOfCase;
    }
    /**
    *   设置 当事人
    **/
    public void setParty(String party) {
      this.party = party;
    }
    /**
    *   获取 当事人
    **/
    public String getParty() {
      return party;
    }



}

