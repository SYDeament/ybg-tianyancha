package com.gitee.deament.tianyancha.core.remote.hiic.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIc;
import com.gitee.deament.tianyancha.core.remote.hiic.dto.HiIcDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史工商信息
* 历史工商信息
* /services/open/hi/ic/2.0
*@author deament
**/
@Component("hiIcRequestImpl")
public class HiIcRequestImpl extends BaseRequestImpl<HiIc,HiIcDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/ic/2.0";
    }
}

