package com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.entity.StockCompanyInfoGeneralManager;
import com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.entity.StockCompanyInfoChairman;
import com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.entity.StockCompanyInfoSecretaries;
import com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.entity.StockCompanyInfoLegal;
/**
*上市公司企业简介
* 可以通过公司名称或ID获取上市公司企业简介，上市公司企业简介包括公司全称、曾用名、工商登记号、注册资本、所属行业、主要人员、控股股东、实际控制人、最终控制人、主营业务等
* /services/open/stock/companyInfo/2.0
*@author deament
**/

public class StockCompanyInfo implements Serializable{

    /**
     *    省份
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    网址
    **/
    @JSONField(name="website")
    private String website;
    /**
     *    股票编号
    **/
    @JSONField(name="code")
    private String code;
    /**
     *    地址
    **/
    @JSONField(name="address")
    private String address;
    /**
     *    总经理
    **/
    @JSONField(name="generalManager")
    private StockCompanyInfoGeneralManager generalManager;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    员工数量
    **/
    @JSONField(name="employeesNum")
    private String employeesNum;
    /**
     *    主营业务
    **/
    @JSONField(name="mainBusiness")
    private String mainBusiness;
    /**
     *    电话
    **/
    @JSONField(name="mobile")
    private String mobile;
    /**
     *    董事长
    **/
    @JSONField(name="chairman")
    private StockCompanyInfoChairman chairman;
    /**
     *    行业
    **/
    @JSONField(name="industry")
    private String industry;
    /**
     *    业务
    **/
    @JSONField(name="productName")
    private String productName;
    /**
     *    董秘
    **/
    @JSONField(name="secretaries")
    private StockCompanyInfoSecretaries secretaries;
    /**
     *    实际控股
    **/
    @JSONField(name="actualController")
    private String actualController;
    /**
     *    控股股东
    **/
    @JSONField(name="controllingShareholder")
    private String controllingShareholder;
    /**
     *    英文名
    **/
    @JSONField(name="engName")
    private String engName;
    /**
     *    注册资金
    **/
    @JSONField(name="registeredCapital")
    private String registeredCapital;
    /**
     *    邮编
    **/
    @JSONField(name="postalcode")
    private String postalcode;
    /**
     *    法人
    **/
    @JSONField(name="legal")
    private StockCompanyInfoLegal legal;
    /**
     *    股票名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    传真
    **/
    @JSONField(name="fax")
    private String fax;
    /**
     *    曾用股票名称
    **/
    @JSONField(name="usedName")
    private String usedName;
    /**
     *    最终控股
    **/
    @JSONField(name="finalController")
    private String finalController;
    /**
     *    介绍
    **/
    @JSONField(name="introduction")
    private String introduction;


    /**
    *   设置 省份
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 省份
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 网址
    **/
    public void setWebsite(String website) {
      this.website = website;
    }
    /**
    *   获取 网址
    **/
    public String getWebsite() {
      return website;
    }
    /**
    *   设置 股票编号
    **/
    public void setCode(String code) {
      this.code = code;
    }
    /**
    *   获取 股票编号
    **/
    public String getCode() {
      return code;
    }
    /**
    *   设置 地址
    **/
    public void setAddress(String address) {
      this.address = address;
    }
    /**
    *   获取 地址
    **/
    public String getAddress() {
      return address;
    }
    /**
    *   设置 总经理
    **/
    public void setGeneralManager(StockCompanyInfoGeneralManager generalManager) {
      this.generalManager = generalManager;
    }
    /**
    *   获取 总经理
    **/
    public StockCompanyInfoGeneralManager getGeneralManager() {
      return generalManager;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 员工数量
    **/
    public void setEmployeesNum(String employeesNum) {
      this.employeesNum = employeesNum;
    }
    /**
    *   获取 员工数量
    **/
    public String getEmployeesNum() {
      return employeesNum;
    }
    /**
    *   设置 主营业务
    **/
    public void setMainBusiness(String mainBusiness) {
      this.mainBusiness = mainBusiness;
    }
    /**
    *   获取 主营业务
    **/
    public String getMainBusiness() {
      return mainBusiness;
    }
    /**
    *   设置 电话
    **/
    public void setMobile(String mobile) {
      this.mobile = mobile;
    }
    /**
    *   获取 电话
    **/
    public String getMobile() {
      return mobile;
    }
    /**
    *   设置 董事长
    **/
    public void setChairman(StockCompanyInfoChairman chairman) {
      this.chairman = chairman;
    }
    /**
    *   获取 董事长
    **/
    public StockCompanyInfoChairman getChairman() {
      return chairman;
    }
    /**
    *   设置 行业
    **/
    public void setIndustry(String industry) {
      this.industry = industry;
    }
    /**
    *   获取 行业
    **/
    public String getIndustry() {
      return industry;
    }
    /**
    *   设置 业务
    **/
    public void setProductName(String productName) {
      this.productName = productName;
    }
    /**
    *   获取 业务
    **/
    public String getProductName() {
      return productName;
    }
    /**
    *   设置 董秘
    **/
    public void setSecretaries(StockCompanyInfoSecretaries secretaries) {
      this.secretaries = secretaries;
    }
    /**
    *   获取 董秘
    **/
    public StockCompanyInfoSecretaries getSecretaries() {
      return secretaries;
    }
    /**
    *   设置 实际控股
    **/
    public void setActualController(String actualController) {
      this.actualController = actualController;
    }
    /**
    *   获取 实际控股
    **/
    public String getActualController() {
      return actualController;
    }
    /**
    *   设置 控股股东
    **/
    public void setControllingShareholder(String controllingShareholder) {
      this.controllingShareholder = controllingShareholder;
    }
    /**
    *   获取 控股股东
    **/
    public String getControllingShareholder() {
      return controllingShareholder;
    }
    /**
    *   设置 英文名
    **/
    public void setEngName(String engName) {
      this.engName = engName;
    }
    /**
    *   获取 英文名
    **/
    public String getEngName() {
      return engName;
    }
    /**
    *   设置 注册资金
    **/
    public void setRegisteredCapital(String registeredCapital) {
      this.registeredCapital = registeredCapital;
    }
    /**
    *   获取 注册资金
    **/
    public String getRegisteredCapital() {
      return registeredCapital;
    }
    /**
    *   设置 邮编
    **/
    public void setPostalcode(String postalcode) {
      this.postalcode = postalcode;
    }
    /**
    *   获取 邮编
    **/
    public String getPostalcode() {
      return postalcode;
    }
    /**
    *   设置 法人
    **/
    public void setLegal(StockCompanyInfoLegal legal) {
      this.legal = legal;
    }
    /**
    *   获取 法人
    **/
    public StockCompanyInfoLegal getLegal() {
      return legal;
    }
    /**
    *   设置 股票名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 股票名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 传真
    **/
    public void setFax(String fax) {
      this.fax = fax;
    }
    /**
    *   获取 传真
    **/
    public String getFax() {
      return fax;
    }
    /**
    *   设置 曾用股票名称
    **/
    public void setUsedName(String usedName) {
      this.usedName = usedName;
    }
    /**
    *   获取 曾用股票名称
    **/
    public String getUsedName() {
      return usedName;
    }
    /**
    *   设置 最终控股
    **/
    public void setFinalController(String finalController) {
      this.finalController = finalController;
    }
    /**
    *   获取 最终控股
    **/
    public String getFinalController() {
      return finalController;
    }
    /**
    *   设置 介绍
    **/
    public void setIntroduction(String introduction) {
      this.introduction = introduction;
    }
    /**
    *   获取 介绍
    **/
    public String getIntroduction() {
      return introduction;
    }



}

