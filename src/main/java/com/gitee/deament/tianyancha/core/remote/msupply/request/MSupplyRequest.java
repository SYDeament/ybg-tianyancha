package com.gitee.deament.tianyancha.core.remote.msupply.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.msupply.entity.MSupply;
import com.gitee.deament.tianyancha.core.remote.msupply.dto.MSupplyDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*供应商
* 可以通过公司名称或ID获取企业相关供应商信息，企业相关供应商信息包括供应商、采购占比、采购金额、报告期等字段的详细信息
* /services/open/m/supply/2.0
*@author deament
**/

public interface MSupplyRequest extends BaseRequest<MSupplyDTO ,MSupply>{

}

