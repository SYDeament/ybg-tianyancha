package com.gitee.deament.tianyancha.core.remote.bqreghuman.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-注册人员
* 属于BqRegHuman
* /services/open/bq/regHuman/2.0
*@author deament
**/

public class BqRegHumanItems implements Serializable{

    /**
     *    人员id
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    注册号（执业印章号）
    **/
    @JSONField(name="practiceSealNo")
    private String practiceSealNo;
    /**
     *    注册类别
    **/
    @JSONField(name="registerType")
    private String registerType;
    /**
     *    姓名
    **/
    @JSONField(name="humanName")
    private String humanName;


    /**
    *   设置 人员id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 人员id
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 注册号（执业印章号）
    **/
    public void setPracticeSealNo(String practiceSealNo) {
      this.practiceSealNo = practiceSealNo;
    }
    /**
    *   获取 注册号（执业印章号）
    **/
    public String getPracticeSealNo() {
      return practiceSealNo;
    }
    /**
    *   设置 注册类别
    **/
    public void setRegisterType(String registerType) {
      this.registerType = registerType;
    }
    /**
    *   获取 注册类别
    **/
    public String getRegisterType() {
      return registerType;
    }
    /**
    *   设置 姓名
    **/
    public void setHumanName(String humanName) {
      this.humanName = humanName;
    }
    /**
    *   获取 姓名
    **/
    public String getHumanName() {
      return humanName;
    }



}

