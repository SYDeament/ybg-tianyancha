package com.gitee.deament.tianyancha.core.remote.icholderchange.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icholderchange.entity.IcHolderChangeItems;
/**
*公司公示-股权变更
* 可以通过公司名称或ID获取企业股权变更信息，股权变更包括变更前后的股东名、变更时间等字段的详细信息
* /services/open/ic/holderChange/2.0
*@author deament
**/

public class IcHolderChange implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcHolderChangeItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcHolderChangeItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcHolderChangeItems> getItems() {
      return items;
    }



}

