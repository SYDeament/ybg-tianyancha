package com.gitee.deament.tianyancha.core.remote.mappbkinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*产品信息
* 属于MAppbkInfo
* /services/open/m/appbkInfo/2.0
*@author deament
**/

public class MAppbkInfoItems implements Serializable{

    /**
     *    描述
    **/
    @JSONField(name="brief")
    private String brief;
    /**
     *    领域
    **/
    @JSONField(name="classes")
    private String classes;
    /**
     *    产品简称
    **/
    @JSONField(name="filterName")
    private String filterName;
    /**
     *    图标
    **/
    @JSONField(name="icon")
    private String icon;
    /**
     *    产品名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    产品分类
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    uuid
    **/
    @JSONField(name="uuid")
    private String uuid;


    /**
    *   设置 描述
    **/
    public void setBrief(String brief) {
      this.brief = brief;
    }
    /**
    *   获取 描述
    **/
    public String getBrief() {
      return brief;
    }
    /**
    *   设置 领域
    **/
    public void setClasses(String classes) {
      this.classes = classes;
    }
    /**
    *   获取 领域
    **/
    public String getClasses() {
      return classes;
    }
    /**
    *   设置 产品简称
    **/
    public void setFilterName(String filterName) {
      this.filterName = filterName;
    }
    /**
    *   获取 产品简称
    **/
    public String getFilterName() {
      return filterName;
    }
    /**
    *   设置 图标
    **/
    public void setIcon(String icon) {
      this.icon = icon;
    }
    /**
    *   获取 图标
    **/
    public String getIcon() {
      return icon;
    }
    /**
    *   设置 产品名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 产品名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 产品分类
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 产品分类
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 uuid
    **/
    public String getUuid() {
      return uuid;
    }



}

