package com.gitee.deament.tianyancha.core.remote.cbipr.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*知识产权
* 属于CbIpr
* /services/open/cb/ipr/2.0
*@author deament
**/

public class CbIprTmList implements Serializable{

    /**
     *    注册号
    **/
    @JSONField(name="regNo")
    private String regNo;
    /**
     *    商标名称
    **/
    @JSONField(name="tmName")
    private String tmName;
    /**
     *    商标图片
    **/
    @JSONField(name="tmPic")
    private String tmPic;
    /**
     *    国际分类code
    **/
    @JSONField(name="tmClass")
    private String tmClass;
    /**
     *    申请日期
    **/
    @JSONField(name="appDate")
    private String appDate;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    国际分类
    **/
    @JSONField(name="intCls")
    private String intCls;
    /**
     *    商标状态
    **/
    @JSONField(name="category")
    private String category;
    /**
     *    申请人
    **/
    @JSONField(name="applicantCn")
    private String applicantCn;
    /**
     *    状态
    **/
    @JSONField(name="status")
    private String status;


    /**
    *   设置 注册号
    **/
    public void setRegNo(String regNo) {
      this.regNo = regNo;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNo() {
      return regNo;
    }
    /**
    *   设置 商标名称
    **/
    public void setTmName(String tmName) {
      this.tmName = tmName;
    }
    /**
    *   获取 商标名称
    **/
    public String getTmName() {
      return tmName;
    }
    /**
    *   设置 商标图片
    **/
    public void setTmPic(String tmPic) {
      this.tmPic = tmPic;
    }
    /**
    *   获取 商标图片
    **/
    public String getTmPic() {
      return tmPic;
    }
    /**
    *   设置 国际分类code
    **/
    public void setTmClass(String tmClass) {
      this.tmClass = tmClass;
    }
    /**
    *   获取 国际分类code
    **/
    public String getTmClass() {
      return tmClass;
    }
    /**
    *   设置 申请日期
    **/
    public void setAppDate(String appDate) {
      this.appDate = appDate;
    }
    /**
    *   获取 申请日期
    **/
    public String getAppDate() {
      return appDate;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 国际分类
    **/
    public void setIntCls(String intCls) {
      this.intCls = intCls;
    }
    /**
    *   获取 国际分类
    **/
    public String getIntCls() {
      return intCls;
    }
    /**
    *   设置 商标状态
    **/
    public void setCategory(String category) {
      this.category = category;
    }
    /**
    *   获取 商标状态
    **/
    public String getCategory() {
      return category;
    }
    /**
    *   设置 申请人
    **/
    public void setApplicantCn(String applicantCn) {
      this.applicantCn = applicantCn;
    }
    /**
    *   获取 申请人
    **/
    public String getApplicantCn() {
      return applicantCn;
    }
    /**
    *   设置 状态
    **/
    public void setStatus(String status) {
      this.status = status;
    }
    /**
    *   获取 状态
    **/
    public String getStatus() {
      return status;
    }



}

