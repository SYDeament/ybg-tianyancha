package com.gitee.deament.tianyancha.core.remote.hidishonest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hidishonest.entity.HiDishonestItems;
/**
*历史失信人
* 可以通过公司名称或ID获取企业历史的失信情况，历史失信信息包括失信人名称、组织机构代码、履行情况、失信行为具体情形等字段信息
* /services/open/hi/dishonest/2.0
*@author deament
**/

public class HiDishonest implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiDishonestItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiDishonestItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiDishonestItems> getItems() {
      return items;
    }



}

