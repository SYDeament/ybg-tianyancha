package com.gitee.deament.tianyancha.core.remote.icholderlist.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*公司公示-股东出资
* 属于IcHolderList
* /services/open/ic/holderList/2.0
*@author deament
**/

public class IcHolderListItems implements Serializable{

    /**
     *    认缴
    **/
    @JSONField(name="capital")
    private List<IcHolderListCapital> capital;
    /**
     *    股东名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    实缴
    **/
    @JSONField(name="capitalActl")
    private List<IcHolderListCapitalActl> capitalActl;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 认缴
    **/
    public void setCapital(List<IcHolderListCapital> capital) {
      this.capital = capital;
    }
    /**
    *   获取 认缴
    **/
    public List<IcHolderListCapital> getCapital() {
      return capital;
    }
    /**
    *   设置 股东名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 股东名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 实缴
    **/
    public void setCapitalActl(List<IcHolderListCapitalActl> capitalActl) {
      this.capitalActl = capitalActl;
    }
    /**
    *   获取 实缴
    **/
    public List<IcHolderListCapitalActl> getCapitalActl() {
      return capitalActl;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }



}

