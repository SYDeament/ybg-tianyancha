package com.gitee.deament.tianyancha.core.remote.stockshareholder.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*十大股东（十大流通股东）
* 属于StockShareholder
* /services/open/stock/shareholder/2.0
*@author deament
**/

public class StockShareholderHolderList implements Serializable{

    /**
     *    累计占股本比例（%）
    **/
    @JSONField(name="tenPercent")
    private String tenPercent;
    /**
     *    总股数
    **/
    @JSONField(name="tenTotal")
    private String tenTotal;
    /**
     *    实际增减持（%）
    **/
    @JSONField(name="actual")
    private String actual;
    /**
     *    占股本比例（%）
    **/
    @JSONField(name="proportion")
    private String proportion;
    /**
     *    类型 1公司 2 人
    **/
    @JSONField(name="cType")
    private Integer cType;
    /**
     *    发布日期
    **/
    @JSONField(name="publishDate")
    private Long publishDate;
    /**
     *    类型
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    持股变化
    **/
    @JSONField(name="compareChange")
    private String compareChange;
    /**
     *    股票类型
    **/
    @JSONField(name="shareType")
    private String shareType;
    /**
     *    持股变化 （万股）
    **/
    @JSONField(name="holdingChange")
    private String holdingChange;
    /**
     *    排序
    **/
    @JSONField(name="sorting")
    private Integer sorting;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    持股数量
    **/
    @JSONField(name="holdingNum")
    private String holdingNum;
    /**
     *    id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    无用
    **/
    @JSONField(name="mtenPercent")
    private String mtenPercent;
    /**
     *    公司id
    **/
    @JSONField(name="graphId")
    private String graphId;
    /**
     *    无用
    **/
    @JSONField(name="mtenTotal")
    private String mtenTotal;


    /**
    *   设置 累计占股本比例（%）
    **/
    public void setTenPercent(String tenPercent) {
      this.tenPercent = tenPercent;
    }
    /**
    *   获取 累计占股本比例（%）
    **/
    public String getTenPercent() {
      return tenPercent;
    }
    /**
    *   设置 总股数
    **/
    public void setTenTotal(String tenTotal) {
      this.tenTotal = tenTotal;
    }
    /**
    *   获取 总股数
    **/
    public String getTenTotal() {
      return tenTotal;
    }
    /**
    *   设置 实际增减持（%）
    **/
    public void setActual(String actual) {
      this.actual = actual;
    }
    /**
    *   获取 实际增减持（%）
    **/
    public String getActual() {
      return actual;
    }
    /**
    *   设置 占股本比例（%）
    **/
    public void setProportion(String proportion) {
      this.proportion = proportion;
    }
    /**
    *   获取 占股本比例（%）
    **/
    public String getProportion() {
      return proportion;
    }
    /**
    *   设置 类型 1公司 2 人
    **/
    public void setCType(Integer cType) {
      this.cType = cType;
    }
    /**
    *   获取 类型 1公司 2 人
    **/
    public Integer getCType() {
      return cType;
    }
    /**
    *   设置 发布日期
    **/
    public void setPublishDate(Long publishDate) {
      this.publishDate = publishDate;
    }
    /**
    *   获取 发布日期
    **/
    public Long getPublishDate() {
      return publishDate;
    }
    /**
    *   设置 类型
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 类型
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 持股变化
    **/
    public void setCompareChange(String compareChange) {
      this.compareChange = compareChange;
    }
    /**
    *   获取 持股变化
    **/
    public String getCompareChange() {
      return compareChange;
    }
    /**
    *   设置 股票类型
    **/
    public void setShareType(String shareType) {
      this.shareType = shareType;
    }
    /**
    *   获取 股票类型
    **/
    public String getShareType() {
      return shareType;
    }
    /**
    *   设置 持股变化 （万股）
    **/
    public void setHoldingChange(String holdingChange) {
      this.holdingChange = holdingChange;
    }
    /**
    *   获取 持股变化 （万股）
    **/
    public String getHoldingChange() {
      return holdingChange;
    }
    /**
    *   设置 排序
    **/
    public void setSorting(Integer sorting) {
      this.sorting = sorting;
    }
    /**
    *   获取 排序
    **/
    public Integer getSorting() {
      return sorting;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 持股数量
    **/
    public void setHoldingNum(String holdingNum) {
      this.holdingNum = holdingNum;
    }
    /**
    *   获取 持股数量
    **/
    public String getHoldingNum() {
      return holdingNum;
    }
    /**
    *   设置 id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 无用
    **/
    public void setMtenPercent(String mtenPercent) {
      this.mtenPercent = mtenPercent;
    }
    /**
    *   获取 无用
    **/
    public String getMtenPercent() {
      return mtenPercent;
    }
    /**
    *   设置 公司id
    **/
    public void setGraphId(String graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 公司id
    **/
    public String getGraphId() {
      return graphId;
    }
    /**
    *   设置 无用
    **/
    public void setMtenTotal(String mtenTotal) {
      this.mtenTotal = mtenTotal;
    }
    /**
    *   获取 无用
    **/
    public String getMtenTotal() {
      return mtenTotal;
    }



}

