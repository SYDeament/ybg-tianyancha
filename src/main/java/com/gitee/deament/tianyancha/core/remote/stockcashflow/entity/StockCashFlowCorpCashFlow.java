package com.gitee.deament.tianyancha.core.remote.stockcashflow.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*现金流量表
* 属于StockCashFlow
* /services/open/stock/cashFlow/2.0
*@author deament
**/

public class StockCashFlowCorpCashFlow implements Serializable{

    /**
     *    支付的各项税费
    **/
    @JSONField(name="payments_of_all_taxes")
    private String payments_of_all_taxes;
    /**
     *    支付其他与经营活动有关的现金
    **/
    @JSONField(name="other_cash_paid_related_to_oa")
    private String other_cash_paid_related_to_oa;
    /**
     *    投资活动现金流入小计
    **/
    @JSONField(name="sub_total_of_ci_from_ia")
    private String sub_total_of_ci_from_ia;
    /**
     *    年度
    **/
    @JSONField(name="showYear")
    private String showYear;
    /**
     *    经营活动现金流出小计
    **/
    @JSONField(name="sub_total_of_cos_from_oa")
    private String sub_total_of_cos_from_oa;
    /**
     *    投资支付的现金
    **/
    @JSONField(name="invest_paid_cash")
    private String invest_paid_cash;
    /**
     *    经营活动产生的现金流量净额
    **/
    @JSONField(name="ncf_from_oa")
    private String ncf_from_oa;
    /**
     *    投资活动现金流出小计
    **/
    @JSONField(name="sub_total_of_cos_from_ia")
    private String sub_total_of_cos_from_ia;
    /**
     *    加:期初现金及现金等价物余额
    **/
    @JSONField(name="initial_balance_of_cce")
    private String initial_balance_of_cce;
    /**
     *    投资活动产生的现金流量净额
    **/
    @JSONField(name="ncf_from_ia")
    private String ncf_from_ia;
    /**
     *    现金及现金等价物净增加额
    **/
    @JSONField(name="net_increase_in_cce")
    private String net_increase_in_cce;
    /**
     *    购建固定资产、无形资产和其他长期资产支付的现金
    **/
    @JSONField(name="cash_paid_for_assets")
    private String cash_paid_for_assets;
    /**
     *    期末现金及现金等价物余额
    **/
    @JSONField(name="final_balance_of_cce")
    private String final_balance_of_cce;
    /**
     *    收到其他与投资活动有关的现金
    **/
    @JSONField(name="cash_received_of_other_ia")
    private String cash_received_of_other_ia;
    /**
     *    筹资活动产生的现金流量净额
    **/
    @JSONField(name="ncf_from_fa")
    private String ncf_from_fa;
    /**
     *    收到其他与经营活动有关的现金
    **/
    @JSONField(name="cash_received_of_other_fa")
    private String cash_received_of_other_fa;
    /**
     *    购买商品、接受劳务支付的现金
    **/
    @JSONField(name="goods_buy_and_service_cash_pay")
    private String goods_buy_and_service_cash_pay;
    /**
     *    收回投资收到的现金
    **/
    @JSONField(name="cash_received_of_dspsl_invest")
    private String cash_received_of_dspsl_invest;
    /**
     *    支付其他与筹资活动有关的现金
    **/
    @JSONField(name="other_cash_paid_relating_to_fa")
    private String other_cash_paid_relating_to_fa;
    /**
     *    筹资活动现金流入小计
    **/
    @JSONField(name="sub_total_of_ci_from_fa")
    private String sub_total_of_ci_from_fa;
    /**
     *    筹资活动现金流出小计
    **/
    @JSONField(name="sub_total_of_cos_from_fa")
    private String sub_total_of_cos_from_fa;
    /**
     *    取得借款收到的现金
    **/
    @JSONField(name="cash_received_of_borrowing")
    private String cash_received_of_borrowing;
    /**
     *    取得投资收益收到的现金
    **/
    @JSONField(name="invest_income_cash_received")
    private String invest_income_cash_received;
    /**
     *    处置固定资产、无形资产和其他长期资产收回的现金净额
    **/
    @JSONField(name="net_cash_of_disposal_assets")
    private String net_cash_of_disposal_assets;
    /**
     *    支付给职工以及为职工支付的现金
    **/
    @JSONField(name="cash_paid_to_staff_etc")
    private String cash_paid_to_staff_etc;
    /**
     *    汇率变动对现金及现金等价物的影响
    **/
    @JSONField(name="effect_of_exchange_chg_on_cce")
    private String effect_of_exchange_chg_on_cce;
    /**
     *    偿还债务支付的现金
    **/
    @JSONField(name="cash_pay_for_debt")
    private String cash_pay_for_debt;
    /**
     *    经营活动现金流入小计
    **/
    @JSONField(name="sub_total_of_ci_from_oa")
    private String sub_total_of_ci_from_oa;
    /**
     *    销售商品、提供劳务收到的现金
    **/
    @JSONField(name="cash_received_of_sales_service")
    private String cash_received_of_sales_service;
    /**
     *    分配股利、利润或偿付利息支付的现金
    **/
    @JSONField(name="cash_paid_of_distribution")
    private String cash_paid_of_distribution;


    /**
    *   设置 支付的各项税费
    **/
    public void setPayments_of_all_taxes(String payments_of_all_taxes) {
      this.payments_of_all_taxes = payments_of_all_taxes;
    }
    /**
    *   获取 支付的各项税费
    **/
    public String getPayments_of_all_taxes() {
      return payments_of_all_taxes;
    }
    /**
    *   设置 支付其他与经营活动有关的现金
    **/
    public void setOther_cash_paid_related_to_oa(String other_cash_paid_related_to_oa) {
      this.other_cash_paid_related_to_oa = other_cash_paid_related_to_oa;
    }
    /**
    *   获取 支付其他与经营活动有关的现金
    **/
    public String getOther_cash_paid_related_to_oa() {
      return other_cash_paid_related_to_oa;
    }
    /**
    *   设置 投资活动现金流入小计
    **/
    public void setSub_total_of_ci_from_ia(String sub_total_of_ci_from_ia) {
      this.sub_total_of_ci_from_ia = sub_total_of_ci_from_ia;
    }
    /**
    *   获取 投资活动现金流入小计
    **/
    public String getSub_total_of_ci_from_ia() {
      return sub_total_of_ci_from_ia;
    }
    /**
    *   设置 年度
    **/
    public void setShowYear(String showYear) {
      this.showYear = showYear;
    }
    /**
    *   获取 年度
    **/
    public String getShowYear() {
      return showYear;
    }
    /**
    *   设置 经营活动现金流出小计
    **/
    public void setSub_total_of_cos_from_oa(String sub_total_of_cos_from_oa) {
      this.sub_total_of_cos_from_oa = sub_total_of_cos_from_oa;
    }
    /**
    *   获取 经营活动现金流出小计
    **/
    public String getSub_total_of_cos_from_oa() {
      return sub_total_of_cos_from_oa;
    }
    /**
    *   设置 投资支付的现金
    **/
    public void setInvest_paid_cash(String invest_paid_cash) {
      this.invest_paid_cash = invest_paid_cash;
    }
    /**
    *   获取 投资支付的现金
    **/
    public String getInvest_paid_cash() {
      return invest_paid_cash;
    }
    /**
    *   设置 经营活动产生的现金流量净额
    **/
    public void setNcf_from_oa(String ncf_from_oa) {
      this.ncf_from_oa = ncf_from_oa;
    }
    /**
    *   获取 经营活动产生的现金流量净额
    **/
    public String getNcf_from_oa() {
      return ncf_from_oa;
    }
    /**
    *   设置 投资活动现金流出小计
    **/
    public void setSub_total_of_cos_from_ia(String sub_total_of_cos_from_ia) {
      this.sub_total_of_cos_from_ia = sub_total_of_cos_from_ia;
    }
    /**
    *   获取 投资活动现金流出小计
    **/
    public String getSub_total_of_cos_from_ia() {
      return sub_total_of_cos_from_ia;
    }
    /**
    *   设置 加:期初现金及现金等价物余额
    **/
    public void setInitial_balance_of_cce(String initial_balance_of_cce) {
      this.initial_balance_of_cce = initial_balance_of_cce;
    }
    /**
    *   获取 加:期初现金及现金等价物余额
    **/
    public String getInitial_balance_of_cce() {
      return initial_balance_of_cce;
    }
    /**
    *   设置 投资活动产生的现金流量净额
    **/
    public void setNcf_from_ia(String ncf_from_ia) {
      this.ncf_from_ia = ncf_from_ia;
    }
    /**
    *   获取 投资活动产生的现金流量净额
    **/
    public String getNcf_from_ia() {
      return ncf_from_ia;
    }
    /**
    *   设置 现金及现金等价物净增加额
    **/
    public void setNet_increase_in_cce(String net_increase_in_cce) {
      this.net_increase_in_cce = net_increase_in_cce;
    }
    /**
    *   获取 现金及现金等价物净增加额
    **/
    public String getNet_increase_in_cce() {
      return net_increase_in_cce;
    }
    /**
    *   设置 购建固定资产、无形资产和其他长期资产支付的现金
    **/
    public void setCash_paid_for_assets(String cash_paid_for_assets) {
      this.cash_paid_for_assets = cash_paid_for_assets;
    }
    /**
    *   获取 购建固定资产、无形资产和其他长期资产支付的现金
    **/
    public String getCash_paid_for_assets() {
      return cash_paid_for_assets;
    }
    /**
    *   设置 期末现金及现金等价物余额
    **/
    public void setFinal_balance_of_cce(String final_balance_of_cce) {
      this.final_balance_of_cce = final_balance_of_cce;
    }
    /**
    *   获取 期末现金及现金等价物余额
    **/
    public String getFinal_balance_of_cce() {
      return final_balance_of_cce;
    }
    /**
    *   设置 收到其他与投资活动有关的现金
    **/
    public void setCash_received_of_other_ia(String cash_received_of_other_ia) {
      this.cash_received_of_other_ia = cash_received_of_other_ia;
    }
    /**
    *   获取 收到其他与投资活动有关的现金
    **/
    public String getCash_received_of_other_ia() {
      return cash_received_of_other_ia;
    }
    /**
    *   设置 筹资活动产生的现金流量净额
    **/
    public void setNcf_from_fa(String ncf_from_fa) {
      this.ncf_from_fa = ncf_from_fa;
    }
    /**
    *   获取 筹资活动产生的现金流量净额
    **/
    public String getNcf_from_fa() {
      return ncf_from_fa;
    }
    /**
    *   设置 收到其他与经营活动有关的现金
    **/
    public void setCash_received_of_other_fa(String cash_received_of_other_fa) {
      this.cash_received_of_other_fa = cash_received_of_other_fa;
    }
    /**
    *   获取 收到其他与经营活动有关的现金
    **/
    public String getCash_received_of_other_fa() {
      return cash_received_of_other_fa;
    }
    /**
    *   设置 购买商品、接受劳务支付的现金
    **/
    public void setGoods_buy_and_service_cash_pay(String goods_buy_and_service_cash_pay) {
      this.goods_buy_and_service_cash_pay = goods_buy_and_service_cash_pay;
    }
    /**
    *   获取 购买商品、接受劳务支付的现金
    **/
    public String getGoods_buy_and_service_cash_pay() {
      return goods_buy_and_service_cash_pay;
    }
    /**
    *   设置 收回投资收到的现金
    **/
    public void setCash_received_of_dspsl_invest(String cash_received_of_dspsl_invest) {
      this.cash_received_of_dspsl_invest = cash_received_of_dspsl_invest;
    }
    /**
    *   获取 收回投资收到的现金
    **/
    public String getCash_received_of_dspsl_invest() {
      return cash_received_of_dspsl_invest;
    }
    /**
    *   设置 支付其他与筹资活动有关的现金
    **/
    public void setOther_cash_paid_relating_to_fa(String other_cash_paid_relating_to_fa) {
      this.other_cash_paid_relating_to_fa = other_cash_paid_relating_to_fa;
    }
    /**
    *   获取 支付其他与筹资活动有关的现金
    **/
    public String getOther_cash_paid_relating_to_fa() {
      return other_cash_paid_relating_to_fa;
    }
    /**
    *   设置 筹资活动现金流入小计
    **/
    public void setSub_total_of_ci_from_fa(String sub_total_of_ci_from_fa) {
      this.sub_total_of_ci_from_fa = sub_total_of_ci_from_fa;
    }
    /**
    *   获取 筹资活动现金流入小计
    **/
    public String getSub_total_of_ci_from_fa() {
      return sub_total_of_ci_from_fa;
    }
    /**
    *   设置 筹资活动现金流出小计
    **/
    public void setSub_total_of_cos_from_fa(String sub_total_of_cos_from_fa) {
      this.sub_total_of_cos_from_fa = sub_total_of_cos_from_fa;
    }
    /**
    *   获取 筹资活动现金流出小计
    **/
    public String getSub_total_of_cos_from_fa() {
      return sub_total_of_cos_from_fa;
    }
    /**
    *   设置 取得借款收到的现金
    **/
    public void setCash_received_of_borrowing(String cash_received_of_borrowing) {
      this.cash_received_of_borrowing = cash_received_of_borrowing;
    }
    /**
    *   获取 取得借款收到的现金
    **/
    public String getCash_received_of_borrowing() {
      return cash_received_of_borrowing;
    }
    /**
    *   设置 取得投资收益收到的现金
    **/
    public void setInvest_income_cash_received(String invest_income_cash_received) {
      this.invest_income_cash_received = invest_income_cash_received;
    }
    /**
    *   获取 取得投资收益收到的现金
    **/
    public String getInvest_income_cash_received() {
      return invest_income_cash_received;
    }
    /**
    *   设置 处置固定资产、无形资产和其他长期资产收回的现金净额
    **/
    public void setNet_cash_of_disposal_assets(String net_cash_of_disposal_assets) {
      this.net_cash_of_disposal_assets = net_cash_of_disposal_assets;
    }
    /**
    *   获取 处置固定资产、无形资产和其他长期资产收回的现金净额
    **/
    public String getNet_cash_of_disposal_assets() {
      return net_cash_of_disposal_assets;
    }
    /**
    *   设置 支付给职工以及为职工支付的现金
    **/
    public void setCash_paid_to_staff_etc(String cash_paid_to_staff_etc) {
      this.cash_paid_to_staff_etc = cash_paid_to_staff_etc;
    }
    /**
    *   获取 支付给职工以及为职工支付的现金
    **/
    public String getCash_paid_to_staff_etc() {
      return cash_paid_to_staff_etc;
    }
    /**
    *   设置 汇率变动对现金及现金等价物的影响
    **/
    public void setEffect_of_exchange_chg_on_cce(String effect_of_exchange_chg_on_cce) {
      this.effect_of_exchange_chg_on_cce = effect_of_exchange_chg_on_cce;
    }
    /**
    *   获取 汇率变动对现金及现金等价物的影响
    **/
    public String getEffect_of_exchange_chg_on_cce() {
      return effect_of_exchange_chg_on_cce;
    }
    /**
    *   设置 偿还债务支付的现金
    **/
    public void setCash_pay_for_debt(String cash_pay_for_debt) {
      this.cash_pay_for_debt = cash_pay_for_debt;
    }
    /**
    *   获取 偿还债务支付的现金
    **/
    public String getCash_pay_for_debt() {
      return cash_pay_for_debt;
    }
    /**
    *   设置 经营活动现金流入小计
    **/
    public void setSub_total_of_ci_from_oa(String sub_total_of_ci_from_oa) {
      this.sub_total_of_ci_from_oa = sub_total_of_ci_from_oa;
    }
    /**
    *   获取 经营活动现金流入小计
    **/
    public String getSub_total_of_ci_from_oa() {
      return sub_total_of_ci_from_oa;
    }
    /**
    *   设置 销售商品、提供劳务收到的现金
    **/
    public void setCash_received_of_sales_service(String cash_received_of_sales_service) {
      this.cash_received_of_sales_service = cash_received_of_sales_service;
    }
    /**
    *   获取 销售商品、提供劳务收到的现金
    **/
    public String getCash_received_of_sales_service() {
      return cash_received_of_sales_service;
    }
    /**
    *   设置 分配股利、利润或偿付利息支付的现金
    **/
    public void setCash_paid_of_distribution(String cash_paid_of_distribution) {
      this.cash_paid_of_distribution = cash_paid_of_distribution;
    }
    /**
    *   获取 分配股利、利润或偿付利息支付的现金
    **/
    public String getCash_paid_of_distribution() {
      return cash_paid_of_distribution;
    }



}

