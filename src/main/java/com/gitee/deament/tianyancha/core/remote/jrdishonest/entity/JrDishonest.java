package com.gitee.deament.tianyancha.core.remote.jrdishonest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrdishonest.entity.JrDishonestStaff;
import com.gitee.deament.tianyancha.core.remote.jrdishonest.entity.JrDishonestItems;
/**
*失信人
* 可以通过公司名称或ID判定企业失信情况，失信信息包括失信人名称、组织机构代码、履行情况、失信行为具体情形等字段的详细信息
* /services/open/jr/dishonest/2.0
*@author deament
**/

public class JrDishonest implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrDishonestItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrDishonestItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrDishonestItems> getItems() {
      return items;
    }



}

