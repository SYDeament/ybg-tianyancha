package com.gitee.deament.tianyancha.core.remote.stockquarmainindex.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*主要指标-季度
* 属于StockQuarMainIndex
* /services/open/stock/quarMainIndex/2.0
*@author deament
**/

public class StockQuarMainIndexCorpTotalMainIndex implements Serializable{

    /**
     *    销售现金流/营业收入
    **/
    @JSONField(name="crfgsasr_to_revenue")
    private Integer crfgsasr_to_revenue;
    /**
     *    扣非净利润同比增长(%)
    **/
    @JSONField(name="np_atsopc_nrgal_yoy")
    private Integer np_atsopc_nrgal_yoy;
    /**
     *    资产负债率(%)
    **/
    @JSONField(name="asset_liab_ratio")
    private Integer asset_liab_ratio;
    /**
     *    营业总收入同比增长(%)
    **/
    @JSONField(name="revenue_yoy")
    private Integer revenue_yoy;
    /**
     *    归属净利润同比增长(%)
    **/
    @JSONField(name="net_profit_atsopc_yoy")
    private Integer net_profit_atsopc_yoy;
    /**
     *    应收账款周转天数(天)
    **/
    @JSONField(name="receivable_turnover_days")
    private Integer receivable_turnover_days;
    /**
     *    流动比率
    **/
    @JSONField(name="current_ratio")
    private Integer current_ratio;
    /**
     *    每股经营现金流(元)
    **/
    @JSONField(name="operate_cash_flow_ps")
    private Integer operate_cash_flow_ps;
    /**
     *    季度
    **/
    @JSONField(name="showYear")
    private String showYear;
    /**
     *    毛利率(%)
    **/
    @JSONField(name="gross_selling_rate")
    private Integer gross_selling_rate;
    /**
     *    流动负债/总负债(%)
    **/
    @JSONField(name="current_liab_to_total_liab")
    private Integer current_liab_to_total_liab;
    /**
     *    速动比率
    **/
    @JSONField(name="quick_ratio")
    private Integer quick_ratio;
    /**
     *    摊薄净资产收益率(%)
    **/
    @JSONField(name="fully_dlt_roe")
    private Integer fully_dlt_roe;
    /**
     *    实际税率(%)
    **/
    @JSONField(name="tax_rate")
    private Integer tax_rate;
    /**
     *    摊薄总资产收益率(%)
    **/
    @JSONField(name="net_interest_of_total_assets")
    private Integer net_interest_of_total_assets;
    /**
     *    营业总收入滚动环比增长(%)
    **/
    @JSONField(name="operating_total_revenue_lrr_sq")
    private Integer operating_total_revenue_lrr_sq;
    /**
     *    扣非净利润滚动环比增长(%)
    **/
    @JSONField(name="profit_deduct_nrgal_lrr_sq")
    private Integer profit_deduct_nrgal_lrr_sq;
    /**
     *    加权净资产收益率(%)
    **/
    @JSONField(name="wgt_avg_roe")
    private Integer wgt_avg_roe;
    /**
     *    每股净资产(元)
    **/
    @JSONField(name="net_profit_per_share")
    private Integer net_profit_per_share;
    /**
     *    扣非净利润(元)
    **/
    @JSONField(name="profit_nrgal_sq")
    private Integer profit_nrgal_sq;
    /**
     *    基本每股收益(元)
    **/
    @JSONField(name="basic_eps")
    private Integer basic_eps;
    /**
     *    净利率(%)
    **/
    @JSONField(name="net_selling_rate")
    private Integer net_selling_rate;
    /**
     *    总资产周转率(次)
    **/
    @JSONField(name="total_capital_turnover")
    private Integer total_capital_turnover;
    /**
     *    归属净利润滚动环比增长(%)
    **/
    @JSONField(name="net_profit_atsopc_lrr_sq")
    private Integer net_profit_atsopc_lrr_sq;
    /**
     *    存货周转天数(天)
    **/
    @JSONField(name="inventory_turnover_days")
    private Integer inventory_turnover_days;
    /**
     *    预收款/营业收入
    **/
    @JSONField(name="pre_receivable")
    private Integer pre_receivable;
    /**
     *    营业总收入(元)
    **/
    @JSONField(name="total_revenue")
    private Integer total_revenue;
    /**
     *    每股未分配利润(元)
    **/
    @JSONField(name="undistri_profit_ps")
    private Integer undistri_profit_ps;
    /**
     *    归属净利润(元)
    **/
    @JSONField(name="net_profit_atsopc")
    private Integer net_profit_atsopc;
    /**
     *    稀释每股收益(元)
    **/
    @JSONField(name="dlt_earnings_per_share")
    private Integer dlt_earnings_per_share;
    /**
     *    扣非每股收益(元)
    **/
    @JSONField(name="basic_e_ps_net_of_nrgal")
    private Integer basic_e_ps_net_of_nrgal;
    /**
     *    每股公积金(元)
    **/
    @JSONField(name="capital_reserve")
    private Integer capital_reserve;


    /**
    *   设置 销售现金流/营业收入
    **/
    public void setCrfgsasr_to_revenue(Integer crfgsasr_to_revenue) {
      this.crfgsasr_to_revenue = crfgsasr_to_revenue;
    }
    /**
    *   获取 销售现金流/营业收入
    **/
    public Integer getCrfgsasr_to_revenue() {
      return crfgsasr_to_revenue;
    }
    /**
    *   设置 扣非净利润同比增长(%)
    **/
    public void setNp_atsopc_nrgal_yoy(Integer np_atsopc_nrgal_yoy) {
      this.np_atsopc_nrgal_yoy = np_atsopc_nrgal_yoy;
    }
    /**
    *   获取 扣非净利润同比增长(%)
    **/
    public Integer getNp_atsopc_nrgal_yoy() {
      return np_atsopc_nrgal_yoy;
    }
    /**
    *   设置 资产负债率(%)
    **/
    public void setAsset_liab_ratio(Integer asset_liab_ratio) {
      this.asset_liab_ratio = asset_liab_ratio;
    }
    /**
    *   获取 资产负债率(%)
    **/
    public Integer getAsset_liab_ratio() {
      return asset_liab_ratio;
    }
    /**
    *   设置 营业总收入同比增长(%)
    **/
    public void setRevenue_yoy(Integer revenue_yoy) {
      this.revenue_yoy = revenue_yoy;
    }
    /**
    *   获取 营业总收入同比增长(%)
    **/
    public Integer getRevenue_yoy() {
      return revenue_yoy;
    }
    /**
    *   设置 归属净利润同比增长(%)
    **/
    public void setNet_profit_atsopc_yoy(Integer net_profit_atsopc_yoy) {
      this.net_profit_atsopc_yoy = net_profit_atsopc_yoy;
    }
    /**
    *   获取 归属净利润同比增长(%)
    **/
    public Integer getNet_profit_atsopc_yoy() {
      return net_profit_atsopc_yoy;
    }
    /**
    *   设置 应收账款周转天数(天)
    **/
    public void setReceivable_turnover_days(Integer receivable_turnover_days) {
      this.receivable_turnover_days = receivable_turnover_days;
    }
    /**
    *   获取 应收账款周转天数(天)
    **/
    public Integer getReceivable_turnover_days() {
      return receivable_turnover_days;
    }
    /**
    *   设置 流动比率
    **/
    public void setCurrent_ratio(Integer current_ratio) {
      this.current_ratio = current_ratio;
    }
    /**
    *   获取 流动比率
    **/
    public Integer getCurrent_ratio() {
      return current_ratio;
    }
    /**
    *   设置 每股经营现金流(元)
    **/
    public void setOperate_cash_flow_ps(Integer operate_cash_flow_ps) {
      this.operate_cash_flow_ps = operate_cash_flow_ps;
    }
    /**
    *   获取 每股经营现金流(元)
    **/
    public Integer getOperate_cash_flow_ps() {
      return operate_cash_flow_ps;
    }
    /**
    *   设置 季度
    **/
    public void setShowYear(String showYear) {
      this.showYear = showYear;
    }
    /**
    *   获取 季度
    **/
    public String getShowYear() {
      return showYear;
    }
    /**
    *   设置 毛利率(%)
    **/
    public void setGross_selling_rate(Integer gross_selling_rate) {
      this.gross_selling_rate = gross_selling_rate;
    }
    /**
    *   获取 毛利率(%)
    **/
    public Integer getGross_selling_rate() {
      return gross_selling_rate;
    }
    /**
    *   设置 流动负债/总负债(%)
    **/
    public void setCurrent_liab_to_total_liab(Integer current_liab_to_total_liab) {
      this.current_liab_to_total_liab = current_liab_to_total_liab;
    }
    /**
    *   获取 流动负债/总负债(%)
    **/
    public Integer getCurrent_liab_to_total_liab() {
      return current_liab_to_total_liab;
    }
    /**
    *   设置 速动比率
    **/
    public void setQuick_ratio(Integer quick_ratio) {
      this.quick_ratio = quick_ratio;
    }
    /**
    *   获取 速动比率
    **/
    public Integer getQuick_ratio() {
      return quick_ratio;
    }
    /**
    *   设置 摊薄净资产收益率(%)
    **/
    public void setFully_dlt_roe(Integer fully_dlt_roe) {
      this.fully_dlt_roe = fully_dlt_roe;
    }
    /**
    *   获取 摊薄净资产收益率(%)
    **/
    public Integer getFully_dlt_roe() {
      return fully_dlt_roe;
    }
    /**
    *   设置 实际税率(%)
    **/
    public void setTax_rate(Integer tax_rate) {
      this.tax_rate = tax_rate;
    }
    /**
    *   获取 实际税率(%)
    **/
    public Integer getTax_rate() {
      return tax_rate;
    }
    /**
    *   设置 摊薄总资产收益率(%)
    **/
    public void setNet_interest_of_total_assets(Integer net_interest_of_total_assets) {
      this.net_interest_of_total_assets = net_interest_of_total_assets;
    }
    /**
    *   获取 摊薄总资产收益率(%)
    **/
    public Integer getNet_interest_of_total_assets() {
      return net_interest_of_total_assets;
    }
    /**
    *   设置 营业总收入滚动环比增长(%)
    **/
    public void setOperating_total_revenue_lrr_sq(Integer operating_total_revenue_lrr_sq) {
      this.operating_total_revenue_lrr_sq = operating_total_revenue_lrr_sq;
    }
    /**
    *   获取 营业总收入滚动环比增长(%)
    **/
    public Integer getOperating_total_revenue_lrr_sq() {
      return operating_total_revenue_lrr_sq;
    }
    /**
    *   设置 扣非净利润滚动环比增长(%)
    **/
    public void setProfit_deduct_nrgal_lrr_sq(Integer profit_deduct_nrgal_lrr_sq) {
      this.profit_deduct_nrgal_lrr_sq = profit_deduct_nrgal_lrr_sq;
    }
    /**
    *   获取 扣非净利润滚动环比增长(%)
    **/
    public Integer getProfit_deduct_nrgal_lrr_sq() {
      return profit_deduct_nrgal_lrr_sq;
    }
    /**
    *   设置 加权净资产收益率(%)
    **/
    public void setWgt_avg_roe(Integer wgt_avg_roe) {
      this.wgt_avg_roe = wgt_avg_roe;
    }
    /**
    *   获取 加权净资产收益率(%)
    **/
    public Integer getWgt_avg_roe() {
      return wgt_avg_roe;
    }
    /**
    *   设置 每股净资产(元)
    **/
    public void setNet_profit_per_share(Integer net_profit_per_share) {
      this.net_profit_per_share = net_profit_per_share;
    }
    /**
    *   获取 每股净资产(元)
    **/
    public Integer getNet_profit_per_share() {
      return net_profit_per_share;
    }
    /**
    *   设置 扣非净利润(元)
    **/
    public void setProfit_nrgal_sq(Integer profit_nrgal_sq) {
      this.profit_nrgal_sq = profit_nrgal_sq;
    }
    /**
    *   获取 扣非净利润(元)
    **/
    public Integer getProfit_nrgal_sq() {
      return profit_nrgal_sq;
    }
    /**
    *   设置 基本每股收益(元)
    **/
    public void setBasic_eps(Integer basic_eps) {
      this.basic_eps = basic_eps;
    }
    /**
    *   获取 基本每股收益(元)
    **/
    public Integer getBasic_eps() {
      return basic_eps;
    }
    /**
    *   设置 净利率(%)
    **/
    public void setNet_selling_rate(Integer net_selling_rate) {
      this.net_selling_rate = net_selling_rate;
    }
    /**
    *   获取 净利率(%)
    **/
    public Integer getNet_selling_rate() {
      return net_selling_rate;
    }
    /**
    *   设置 总资产周转率(次)
    **/
    public void setTotal_capital_turnover(Integer total_capital_turnover) {
      this.total_capital_turnover = total_capital_turnover;
    }
    /**
    *   获取 总资产周转率(次)
    **/
    public Integer getTotal_capital_turnover() {
      return total_capital_turnover;
    }
    /**
    *   设置 归属净利润滚动环比增长(%)
    **/
    public void setNet_profit_atsopc_lrr_sq(Integer net_profit_atsopc_lrr_sq) {
      this.net_profit_atsopc_lrr_sq = net_profit_atsopc_lrr_sq;
    }
    /**
    *   获取 归属净利润滚动环比增长(%)
    **/
    public Integer getNet_profit_atsopc_lrr_sq() {
      return net_profit_atsopc_lrr_sq;
    }
    /**
    *   设置 存货周转天数(天)
    **/
    public void setInventory_turnover_days(Integer inventory_turnover_days) {
      this.inventory_turnover_days = inventory_turnover_days;
    }
    /**
    *   获取 存货周转天数(天)
    **/
    public Integer getInventory_turnover_days() {
      return inventory_turnover_days;
    }
    /**
    *   设置 预收款/营业收入
    **/
    public void setPre_receivable(Integer pre_receivable) {
      this.pre_receivable = pre_receivable;
    }
    /**
    *   获取 预收款/营业收入
    **/
    public Integer getPre_receivable() {
      return pre_receivable;
    }
    /**
    *   设置 营业总收入(元)
    **/
    public void setTotal_revenue(Integer total_revenue) {
      this.total_revenue = total_revenue;
    }
    /**
    *   获取 营业总收入(元)
    **/
    public Integer getTotal_revenue() {
      return total_revenue;
    }
    /**
    *   设置 每股未分配利润(元)
    **/
    public void setUndistri_profit_ps(Integer undistri_profit_ps) {
      this.undistri_profit_ps = undistri_profit_ps;
    }
    /**
    *   获取 每股未分配利润(元)
    **/
    public Integer getUndistri_profit_ps() {
      return undistri_profit_ps;
    }
    /**
    *   设置 归属净利润(元)
    **/
    public void setNet_profit_atsopc(Integer net_profit_atsopc) {
      this.net_profit_atsopc = net_profit_atsopc;
    }
    /**
    *   获取 归属净利润(元)
    **/
    public Integer getNet_profit_atsopc() {
      return net_profit_atsopc;
    }
    /**
    *   设置 稀释每股收益(元)
    **/
    public void setDlt_earnings_per_share(Integer dlt_earnings_per_share) {
      this.dlt_earnings_per_share = dlt_earnings_per_share;
    }
    /**
    *   获取 稀释每股收益(元)
    **/
    public Integer getDlt_earnings_per_share() {
      return dlt_earnings_per_share;
    }
    /**
    *   设置 扣非每股收益(元)
    **/
    public void setBasic_e_ps_net_of_nrgal(Integer basic_e_ps_net_of_nrgal) {
      this.basic_e_ps_net_of_nrgal = basic_e_ps_net_of_nrgal;
    }
    /**
    *   获取 扣非每股收益(元)
    **/
    public Integer getBasic_e_ps_net_of_nrgal() {
      return basic_e_ps_net_of_nrgal;
    }
    /**
    *   设置 每股公积金(元)
    **/
    public void setCapital_reserve(Integer capital_reserve) {
      this.capital_reserve = capital_reserve;
    }
    /**
    *   获取 每股公积金(元)
    **/
    public Integer getCapital_reserve() {
      return capital_reserve;
    }



}

