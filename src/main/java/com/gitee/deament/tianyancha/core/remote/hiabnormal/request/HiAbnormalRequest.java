package com.gitee.deament.tianyancha.core.remote.hiabnormal.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiabnormal.entity.HiAbnormal;
import com.gitee.deament.tianyancha.core.remote.hiabnormal.dto.HiAbnormalDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*历史经营异常
* 可以通过公司名称或ID获取企业历史的经营异常信息，历史经营异常信息包括列入/移除原因、时间、做出决定机关等字段信息
* /services/open/hi/abnormal/2.0
*@author deament
**/

public interface HiAbnormalRequest extends BaseRequest<HiAbnormalDTO ,HiAbnormal>{

}

