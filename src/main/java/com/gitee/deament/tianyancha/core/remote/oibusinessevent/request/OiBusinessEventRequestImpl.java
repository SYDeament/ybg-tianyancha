package com.gitee.deament.tianyancha.core.remote.oibusinessevent.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oibusinessevent.entity.OiBusinessEvent;
import com.gitee.deament.tianyancha.core.remote.oibusinessevent.dto.OiBusinessEventDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*工商追踪
* 可以通过投资机构名称获取被投资企业名称、logo、投资比例等字段的详细信息
* /services/open/oi/businessEvent/2.0
*@author deament
**/
@Component("oiBusinessEventRequestImpl")
public class OiBusinessEventRequestImpl extends BaseRequestImpl<OiBusinessEvent,OiBusinessEventDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/oi/businessEvent/2.0";
    }
}

