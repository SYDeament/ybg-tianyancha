package com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*动产抵押
* 属于MrMortgageInfo
* /services/open/mr/mortgageInfo/2.0
*@author deament
**/

public class MrMortgageInfoPeopleInfo implements Serializable{

    /**
     *    抵押权人证照/证件类型
    **/
    @JSONField(name="liceseType")
    private String liceseType;
    /**
     *    抵押权人名称
    **/
    @JSONField(name="peopleName")
    private String peopleName;
    /**
     *    证照/证件号码
    **/
    @JSONField(name="licenseNum")
    private String licenseNum;


    /**
    *   设置 抵押权人证照/证件类型
    **/
    public void setLiceseType(String liceseType) {
      this.liceseType = liceseType;
    }
    /**
    *   获取 抵押权人证照/证件类型
    **/
    public String getLiceseType() {
      return liceseType;
    }
    /**
    *   设置 抵押权人名称
    **/
    public void setPeopleName(String peopleName) {
      this.peopleName = peopleName;
    }
    /**
    *   获取 抵押权人名称
    **/
    public String getPeopleName() {
      return peopleName;
    }
    /**
    *   设置 证照/证件号码
    **/
    public void setLicenseNum(String licenseNum) {
      this.licenseNum = licenseNum;
    }
    /**
    *   获取 证照/证件号码
    **/
    public String getLicenseNum() {
      return licenseNum;
    }



}

