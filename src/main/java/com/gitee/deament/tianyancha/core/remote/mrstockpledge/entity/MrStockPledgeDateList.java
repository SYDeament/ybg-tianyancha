package com.gitee.deament.tianyancha.core.remote.mrstockpledge.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*质押比例
* 属于MrStockPledge
* /services/open/mr/stockPledge/ratio/2.0
*@author deament
**/

public class MrStockPledgeDateList implements Serializable{

    /**
     *    
    **/
    @JSONField(name="show")
    private String show;
    /**
     *    
    **/
    @JSONField(name="value")
    private String value;


    /**
    *   设置 
    **/
    public void setShow(String show) {
      this.show = show;
    }
    /**
    *   获取 
    **/
    public String getShow() {
      return show;
    }
    /**
    *   设置 
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 
    **/
    public String getValue() {
      return value;
    }



}

