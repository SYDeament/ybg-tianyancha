package com.gitee.deament.tianyancha.core.remote.humanriskinfo.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*人员天眼风险
* 可以通过公司名称或ID和人名获取人员相关天眼风险列表，包括人员周边/预警风险信息
* /services/v4/open/humanRiskInfo
*@author deament
**/

@TYCURL(value="/services/v4/open/humanRiskInfo")
public class HumanRiskInfoDTO implements Serializable{

    /**
     *    人id（humanName和hid只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long hid;
    /**
     *    公司名称（cid和name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    姓名（humanName和hid只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String humanName;
    /**
     *    公司id（cid和name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long cid;


    /**
    *   设置 人id（humanName和hid只需输入其中一个）
    **/
    public void setHid(Long hid) {
      this.hid = hid;
    }
    /**
    *   获取 人id（humanName和hid只需输入其中一个）
    **/
    public Long getHid() {
      return hid;
    }
    /**
    *   设置 公司名称（cid和name只需输入其中一个）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（cid和name只需输入其中一个）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 姓名（humanName和hid只需输入其中一个）
    **/
    public void setHumanName(String humanName) {
      this.humanName = humanName;
    }
    /**
    *   获取 姓名（humanName和hid只需输入其中一个）
    **/
    public String getHumanName() {
      return humanName;
    }
    /**
    *   设置 公司id（cid和name只需输入其中一个）
    **/
    public void setCid(Long cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id（cid和name只需输入其中一个）
    **/
    public Long getCid() {
      return cid;
    }



}

