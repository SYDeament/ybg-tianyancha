package com.gitee.deament.tianyancha.core.remote.humanriskinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*人员天眼风险
* 属于HumanRiskInfo
* /services/v4/open/humanRiskInfo
*@author deament
**/

public class HumanRiskInfo_child implements Serializable{

    /**
     *    风险总数
    **/
    @JSONField(name="count")
    private Integer count;
    /**
     *    风险分类（周边风险，预警提醒） 
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    
    **/
    @JSONField(name="list")
    private List<HumanRiskInfoList> list;


    /**
    *   设置 风险总数
    **/
    public void setCount(Integer count) {
      this.count = count;
    }
    /**
    *   获取 风险总数
    **/
    public Integer getCount() {
      return count;
    }
    /**
    *   设置 风险分类（周边风险，预警提醒） 
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 风险分类（周边风险，预警提醒） 
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 
    **/
    public void setList(List<HumanRiskInfoList> list) {
      this.list = list;
    }
    /**
    *   获取 
    **/
    public List<HumanRiskInfoList> getList() {
      return list;
    }



}

