package com.gitee.deament.tianyancha.core.remote.jrlawsuit.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrlawsuit.entity.JrLawSuitItems;
/**
*法律诉讼
* 可以通过公司名称或ID获取企业法律诉讼信息，法律诉讼包括案件名称、案由、案件身份、案号等字段的详细信息
* /services/open/jr/lawSuit/2.0
*@author deament
**/

public class JrLawSuit implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrLawSuitItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrLawSuitItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrLawSuitItems> getItems() {
      return items;
    }



}

