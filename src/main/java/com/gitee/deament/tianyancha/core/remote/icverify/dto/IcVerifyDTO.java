package com.gitee.deament.tianyancha.core.remote.icverify.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*企业三要素验证
* 可以通过输入企业名称、法人、注册号 /组织机构代码 /统一社会信用代码，验证三者是否匹配一致
* /services/open/ic/verify
*@author deament
**/

@TYCURL(value="/services/open/ic/verify")
public class IcVerifyDTO implements Serializable{

    /**
     *    三码(注册号 /组织机构代码 /统一社会信用代码)
     *
    **/
    @ParamRequire(require = true)
    private String code;
    /**
     *    公司名
     *
    **/
    @ParamRequire(require = true)
    private String name;
    /**
     *    法人
     *
    **/
    @ParamRequire(require = true)
    private String legalPersonName;


    /**
    *   设置 三码(注册号 /组织机构代码 /统一社会信用代码)
    **/
    public void setCode(String code) {
      this.code = code;
    }
    /**
    *   获取 三码(注册号 /组织机构代码 /统一社会信用代码)
    **/
    public String getCode() {
      return code;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }



}

