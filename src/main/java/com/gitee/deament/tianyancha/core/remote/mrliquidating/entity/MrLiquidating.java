package com.gitee.deament.tianyancha.core.remote.mrliquidating.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*清算信息
* 可以通过公司名称或ID获取企业清算信息，企业清算信息包括清算组负责人、清算组成员
* /services/open/mr/liquidating/2.0
*@author deament
**/

public class MrLiquidating implements Serializable{

    /**
     *    清算组负责人
    **/
    @JSONField(name="manager")
    private String manager;
    /**
     *    清算组成员
    **/
    @JSONField(name="member")
    private String member;


    /**
    *   设置 清算组负责人
    **/
    public void setManager(String manager) {
      this.manager = manager;
    }
    /**
    *   获取 清算组负责人
    **/
    public String getManager() {
      return manager;
    }
    /**
    *   设置 清算组成员
    **/
    public void setMember(String member) {
      this.member = member;
    }
    /**
    *   获取 清算组成员
    **/
    public String getMember() {
      return member;
    }



}

