package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcIllegalList implements Serializable{

    /**
     *    移出时间
    **/
    @JSONField(name="removeDate")
    private String removeDate;
    /**
     *    违法事实
    **/
    @JSONField(name="fact")
    private String fact;
    /**
     *    列入原因
    **/
    @JSONField(name="putReason")
    private String putReason;
    /**
     *    决定列入机关
    **/
    @JSONField(name="putDepartment")
    private String putDepartment;
    /**
     *    决定移出机关
    **/
    @JSONField(name="removeDepartment")
    private String removeDepartment;
    /**
     *    移出原因
    **/
    @JSONField(name="removeReason")
    private String removeReason;
    /**
     *    表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    类别
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    列入时间
    **/
    @JSONField(name="putDate")
    private String putDate;


    /**
    *   设置 移出时间
    **/
    public void setRemoveDate(String removeDate) {
      this.removeDate = removeDate;
    }
    /**
    *   获取 移出时间
    **/
    public String getRemoveDate() {
      return removeDate;
    }
    /**
    *   设置 违法事实
    **/
    public void setFact(String fact) {
      this.fact = fact;
    }
    /**
    *   获取 违法事实
    **/
    public String getFact() {
      return fact;
    }
    /**
    *   设置 列入原因
    **/
    public void setPutReason(String putReason) {
      this.putReason = putReason;
    }
    /**
    *   获取 列入原因
    **/
    public String getPutReason() {
      return putReason;
    }
    /**
    *   设置 决定列入机关
    **/
    public void setPutDepartment(String putDepartment) {
      this.putDepartment = putDepartment;
    }
    /**
    *   获取 决定列入机关
    **/
    public String getPutDepartment() {
      return putDepartment;
    }
    /**
    *   设置 决定移出机关
    **/
    public void setRemoveDepartment(String removeDepartment) {
      this.removeDepartment = removeDepartment;
    }
    /**
    *   获取 决定移出机关
    **/
    public String getRemoveDepartment() {
      return removeDepartment;
    }
    /**
    *   设置 移出原因
    **/
    public void setRemoveReason(String removeReason) {
      this.removeReason = removeReason;
    }
    /**
    *   获取 移出原因
    **/
    public String getRemoveReason() {
      return removeReason;
    }
    /**
    *   设置 表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 类别
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 类别
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 列入时间
    **/
    public void setPutDate(String putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 列入时间
    **/
    public String getPutDate() {
      return putDate;
    }



}

