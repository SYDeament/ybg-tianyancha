package com.gitee.deament.tianyancha.core.remote.partners.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.partners.entity.PartnersPartners;
import com.gitee.deament.tianyancha.core.remote.partners.entity.PartnersOffice;
import com.gitee.deament.tianyancha.core.remote.partners.entity.PartnersItems;
/**
*人员所有合作伙伴
* 可以通过公司名称或ID和人名获取人员的所有合作伙伴，包括其合作伙伴的所有相关公司数量、公司名称或ID信息
* /services/v4/open/partners
*@author deament
**/

public class Partners implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<PartnersItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<PartnersItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<PartnersItems> getItems() {
      return items;
    }



}

