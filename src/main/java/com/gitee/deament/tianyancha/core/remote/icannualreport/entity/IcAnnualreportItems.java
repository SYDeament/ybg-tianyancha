package com.gitee.deament.tianyancha.core.remote.icannualreport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业年报
* 属于IcAnnualreport
* /services/open/ic/annualreport/2.0
*@author deament
**/

public class IcAnnualreportItems implements Serializable{

    /**
     *    基本信息
    **/
    @JSONField(name="baseInfo")
    private IcAnnualreportBaseInfo baseInfo;
    /**
     *    公司id
    **/
    @JSONField(name="companyId")
    private String companyId;
    /**
     *    对外投资信息
    **/
    @JSONField(name="outboundInvestmentList")
    private List<IcAnnualreportOutboundInvestmentList> outboundInvestmentList;
    /**
     *    股东及出资信息
    **/
    @JSONField(name="shareholderList")
    private List<IcAnnualreportShareholderList> shareholderList;
    /**
     *    社保信息
    **/
    @JSONField(name="reportSocialSecurityInfo")
    private IcAnnualreportReportSocialSecurityInfo reportSocialSecurityInfo;
    /**
     *    无用
    **/
    @JSONField(name="govReport")
    private String govReport;
    /**
     *    股东股权变更信息
    **/
    @JSONField(name="equityChangeInfoList")
    private List<IcAnnualreportEquityChangeInfoList> equityChangeInfoList;
    /**
     *    无用
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    年报变更
    **/
    @JSONField(name="changeRecordList")
    private List<IcAnnualreportChangeRecordList> changeRecordList;
    /**
     *    网站或网店信息
    **/
    @JSONField(name="webInfoList")
    private List<IcAnnualreportWebInfoList> webInfoList;
    /**
     *    对外提供保证担保信息
    **/
    @JSONField(name="outGuaranteeInfoList")
    private List<IcAnnualreportOutGuaranteeInfoList> outGuaranteeInfoList;


    /**
    *   设置 基本信息
    **/
    public void setBaseInfo(IcAnnualreportBaseInfo baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 基本信息
    **/
    public IcAnnualreportBaseInfo getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 公司id
    **/
    public void setCompanyId(String companyId) {
      this.companyId = companyId;
    }
    /**
    *   获取 公司id
    **/
    public String getCompanyId() {
      return companyId;
    }
    /**
    *   设置 对外投资信息
    **/
    public void setOutboundInvestmentList(List<IcAnnualreportOutboundInvestmentList> outboundInvestmentList) {
      this.outboundInvestmentList = outboundInvestmentList;
    }
    /**
    *   获取 对外投资信息
    **/
    public List<IcAnnualreportOutboundInvestmentList> getOutboundInvestmentList() {
      return outboundInvestmentList;
    }
    /**
    *   设置 股东及出资信息
    **/
    public void setShareholderList(List<IcAnnualreportShareholderList> shareholderList) {
      this.shareholderList = shareholderList;
    }
    /**
    *   获取 股东及出资信息
    **/
    public List<IcAnnualreportShareholderList> getShareholderList() {
      return shareholderList;
    }
    /**
    *   设置 社保信息
    **/
    public void setReportSocialSecurityInfo(IcAnnualreportReportSocialSecurityInfo reportSocialSecurityInfo) {
      this.reportSocialSecurityInfo = reportSocialSecurityInfo;
    }
    /**
    *   获取 社保信息
    **/
    public IcAnnualreportReportSocialSecurityInfo getReportSocialSecurityInfo() {
      return reportSocialSecurityInfo;
    }
    /**
    *   设置 无用
    **/
    public void setGovReport(String govReport) {
      this.govReport = govReport;
    }
    /**
    *   获取 无用
    **/
    public String getGovReport() {
      return govReport;
    }
    /**
    *   设置 股东股权变更信息
    **/
    public void setEquityChangeInfoList(List<IcAnnualreportEquityChangeInfoList> equityChangeInfoList) {
      this.equityChangeInfoList = equityChangeInfoList;
    }
    /**
    *   获取 股东股权变更信息
    **/
    public List<IcAnnualreportEquityChangeInfoList> getEquityChangeInfoList() {
      return equityChangeInfoList;
    }
    /**
    *   设置 无用
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 无用
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 年报变更
    **/
    public void setChangeRecordList(List<IcAnnualreportChangeRecordList> changeRecordList) {
      this.changeRecordList = changeRecordList;
    }
    /**
    *   获取 年报变更
    **/
    public List<IcAnnualreportChangeRecordList> getChangeRecordList() {
      return changeRecordList;
    }
    /**
    *   设置 网站或网店信息
    **/
    public void setWebInfoList(List<IcAnnualreportWebInfoList> webInfoList) {
      this.webInfoList = webInfoList;
    }
    /**
    *   获取 网站或网店信息
    **/
    public List<IcAnnualreportWebInfoList> getWebInfoList() {
      return webInfoList;
    }
    /**
    *   设置 对外提供保证担保信息
    **/
    public void setOutGuaranteeInfoList(List<IcAnnualreportOutGuaranteeInfoList> outGuaranteeInfoList) {
      this.outGuaranteeInfoList = outGuaranteeInfoList;
    }
    /**
    *   获取 对外提供保证担保信息
    **/
    public List<IcAnnualreportOutGuaranteeInfoList> getOutGuaranteeInfoList() {
      return outGuaranteeInfoList;
    }



}

