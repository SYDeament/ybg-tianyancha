package com.gitee.deament.tianyancha.core.remote.relashortpath.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.relashortpath.entity.RelaShortPathProperties;
import com.gitee.deament.tianyancha.core.remote.relashortpath.entity.RelaShortPathRelationships;
import com.gitee.deament.tianyancha.core.remote.relashortpath.entity.RelaShortPathProperties;
import com.gitee.deament.tianyancha.core.remote.relashortpath.entity.RelaShortPathNodes;
import com.gitee.deament.tianyancha.core.remote.relashortpath.entity.RelaShortPathP_0;
/**
*最短路径发现
* 可以通过公司A，公司B，关系类型（法人/任职/投资/分支/诉讼/竞合/债务，可多选）查询两家公司之间是否存在关联关系
* /services/open/rela/shortPath
*@author deament
**/

public class RelaShortPath implements Serializable{

    /**
     *    关系路径
    **/
    @JSONField(name="p_0")
    private RelaShortPathP_0 p_0;

    /**
     *    结束节点
    **/
    @JSONField(name="idsTo")
    private String idsTo;
    /**
     *    
    **/
    @JSONField(name="ids")
    private String ids;
    /**
     *    起始节点
    **/
    @JSONField(name="idsFrom")
    private String idsFrom;


    /**
    *   设置 关系路径
    **/
    public void setP_0(RelaShortPathP_0 p_0) {
      this.p_0 = p_0;
    }
    /**
    *   获取 关系路径
    **/
    public RelaShortPathP_0 getP_0() {
      return p_0;
    }

    /**
    *   设置 结束节点
    **/
    public void setIdsTo(String idsTo) {
      this.idsTo = idsTo;
    }
    /**
    *   获取 结束节点
    **/
    public String getIdsTo() {
      return idsTo;
    }
    /**
    *   设置 
    **/
    public void setIds(String ids) {
      this.ids = ids;
    }
    /**
    *   获取 
    **/
    public String getIds() {
      return ids;
    }
    /**
    *   设置 起始节点
    **/
    public void setIdsFrom(String idsFrom) {
      this.idsFrom = idsFrom;
    }
    /**
    *   获取 起始节点
    **/
    public String getIdsFrom() {
      return idsFrom;
    }



}

