package com.gitee.deament.tianyancha.core.remote.stockholdingcompany.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*参股控股
* 属于StockHoldingCompany
* /services/open/stock/holdingCompany/2.0
*@author deament
**/

public class StockHoldingCompanyItems implements Serializable{

    /**
     *    参控比例
    **/
    @JSONField(name="participationRatio")
    private String participationRatio;
    /**
     *    投资金额(元)
    **/
    @JSONField(name="investmentAmount")
    private String investmentAmount;
    /**
     *    类型 1公司 2 人
    **/
    @JSONField(name="cType")
    private Integer cType;
    /**
     *    关联公司名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    被参股公司主营业务
    **/
    @JSONField(name="mainBusiness")
    private String mainBusiness;
    /**
     *    id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    参控关系
    **/
    @JSONField(name="relationship")
    private String relationship;
    /**
     *    公司id
    **/
    @JSONField(name="graphId")
    private String graphId;
    /**
     *    被参控公司净利润(元)
    **/
    @JSONField(name="profit")
    private String profit;
    /**
     *    是否报表合并
    **/
    @JSONField(name="reportMerge")
    private String reportMerge;


    /**
    *   设置 参控比例
    **/
    public void setParticipationRatio(String participationRatio) {
      this.participationRatio = participationRatio;
    }
    /**
    *   获取 参控比例
    **/
    public String getParticipationRatio() {
      return participationRatio;
    }
    /**
    *   设置 投资金额(元)
    **/
    public void setInvestmentAmount(String investmentAmount) {
      this.investmentAmount = investmentAmount;
    }
    /**
    *   获取 投资金额(元)
    **/
    public String getInvestmentAmount() {
      return investmentAmount;
    }
    /**
    *   设置 类型 1公司 2 人
    **/
    public void setCType(Integer cType) {
      this.cType = cType;
    }
    /**
    *   获取 类型 1公司 2 人
    **/
    public Integer getCType() {
      return cType;
    }
    /**
    *   设置 关联公司名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 关联公司名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 被参股公司主营业务
    **/
    public void setMainBusiness(String mainBusiness) {
      this.mainBusiness = mainBusiness;
    }
    /**
    *   获取 被参股公司主营业务
    **/
    public String getMainBusiness() {
      return mainBusiness;
    }
    /**
    *   设置 id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 参控关系
    **/
    public void setRelationship(String relationship) {
      this.relationship = relationship;
    }
    /**
    *   获取 参控关系
    **/
    public String getRelationship() {
      return relationship;
    }
    /**
    *   设置 公司id
    **/
    public void setGraphId(String graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 公司id
    **/
    public String getGraphId() {
      return graphId;
    }
    /**
    *   设置 被参控公司净利润(元)
    **/
    public void setProfit(String profit) {
      this.profit = profit;
    }
    /**
    *   获取 被参控公司净利润(元)
    **/
    public String getProfit() {
      return profit;
    }
    /**
    *   设置 是否报表合并
    **/
    public void setReportMerge(String reportMerge) {
      this.reportMerge = reportMerge;
    }
    /**
    *   获取 是否报表合并
    **/
    public String getReportMerge() {
      return reportMerge;
    }



}

