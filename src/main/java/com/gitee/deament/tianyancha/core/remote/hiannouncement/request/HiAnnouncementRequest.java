package com.gitee.deament.tianyancha.core.remote.hiannouncement.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiannouncement.entity.HiAnnouncement;
import com.gitee.deament.tianyancha.core.remote.hiannouncement.dto.HiAnnouncementDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*历史开庭公告
* 可以通过公司名称或ID获取企业历史的开庭公告，历史开庭公告信息包括被告/被上诉人、法院、原告/上诉人、开庭日期、案由、内部ID、案号等字段信息
* /services/open/hi/announcement/2.0
*@author deament
**/

public interface HiAnnouncementRequest extends BaseRequest<HiAnnouncementDTO ,HiAnnouncement>{

}

