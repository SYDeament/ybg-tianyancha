package com.gitee.deament.tianyancha.core.remote.stockquarmainindex.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockquarmainindex.entity.StockQuarMainIndexCorpFinancialYears;
import com.gitee.deament.tianyancha.core.remote.stockquarmainindex.entity.StockQuarMainIndexCorpTotalMainIndex;
/**
*主要指标-季度
* 可以通过公司名称或ID获取上市公司季度主要指标数据，季度主要指标数据包括基本每股收益、扣非每股收益、稀释每股收益、每股净资产、每股公积金等
* /services/open/stock/quarMainIndex/2.0
*@author deament
**/

public class StockQuarMainIndex implements Serializable{

    /**
     *    
    **/
    @JSONField(name="corpFinancialYears")
    private List<StockQuarMainIndexCorpFinancialYears> corpFinancialYears;
    /**
     *    
    **/
    @JSONField(name="corpTotalMainIndex")
    private List<StockQuarMainIndexCorpTotalMainIndex> corpTotalMainIndex;


    /**
    *   设置 
    **/
    public void setCorpFinancialYears(List<StockQuarMainIndexCorpFinancialYears> corpFinancialYears) {
      this.corpFinancialYears = corpFinancialYears;
    }
    /**
    *   获取 
    **/
    public List<StockQuarMainIndexCorpFinancialYears> getCorpFinancialYears() {
      return corpFinancialYears;
    }
    /**
    *   设置 
    **/
    public void setCorpTotalMainIndex(List<StockQuarMainIndexCorpTotalMainIndex> corpTotalMainIndex) {
      this.corpTotalMainIndex = corpTotalMainIndex;
    }
    /**
    *   获取 
    **/
    public List<StockQuarMainIndexCorpTotalMainIndex> getCorpTotalMainIndex() {
      return corpTotalMainIndex;
    }



}

