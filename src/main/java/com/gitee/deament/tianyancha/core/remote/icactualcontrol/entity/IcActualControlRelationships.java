package com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*疑似实际控制人
* 属于IcActualControl
* /services/open/ic/actualControl/2.0
*@author deament
**/

public class IcActualControlRelationships implements Serializable{

    /**
     *    开始节点
    **/
    @JSONField(name="startNode")
    private Integer startNode;
    /**
     *    TYPE：自然人人股东->公司：INVEST_H 公司股东->公司：INVEST_C 人->公司（仅限执行事务合伙人→合伙企业）：OWN 公司->公司（仅限执行事务合伙人->合伙企业）：OWN_C 总公司->分公司：BRANCH
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    结束节点
    **/
    @JSONField(name="endNode")
    private String endNode;
    /**
     *    该路径上的边信息集合
    **/
    @JSONField(name="properties")
    private IcActualControlProperties properties;


    /**
    *   设置 开始节点
    **/
    public void setStartNode(Integer startNode) {
      this.startNode = startNode;
    }
    /**
    *   获取 开始节点
    **/
    public Integer getStartNode() {
      return startNode;
    }
    /**
    *   设置 TYPE：自然人人股东->公司：INVEST_H 公司股东->公司：INVEST_C 人->公司（仅限执行事务合伙人→合伙企业）：OWN 公司->公司（仅限执行事务合伙人->合伙企业）：OWN_C 总公司->分公司：BRANCH
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 TYPE：自然人人股东->公司：INVEST_H 公司股东->公司：INVEST_C 人->公司（仅限执行事务合伙人→合伙企业）：OWN 公司->公司（仅限执行事务合伙人->合伙企业）：OWN_C 总公司->分公司：BRANCH
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 结束节点
    **/
    public void setEndNode(String endNode) {
      this.endNode = endNode;
    }
    /**
    *   获取 结束节点
    **/
    public String getEndNode() {
      return endNode;
    }
    /**
    *   设置 该路径上的边信息集合
    **/
    public void setProperties(IcActualControlProperties properties) {
      this.properties = properties;
    }
    /**
    *   获取 该路径上的边信息集合
    **/
    public IcActualControlProperties getProperties() {
      return properties;
    }



}

