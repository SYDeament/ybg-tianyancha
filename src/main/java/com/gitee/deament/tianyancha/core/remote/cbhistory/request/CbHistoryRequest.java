package com.gitee.deament.tianyancha.core.remote.cbhistory.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistory;
import com.gitee.deament.tianyancha.core.remote.cbhistory.dto.CbHistoryDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*历史信息
* 可以通过公司名称或ID获取包含历史工商信息、历史股东信息、历史对外投资、历史行政许可、历史法律诉讼、历史法院公告、历史开庭公告、历史司法协助、历史失信人、历史被执行人、历史经营异常、历史股权出质、历史动产抵押、历史行政处罚、历史商标信息、历史网站备案等维度的相关信息
* /services/open/cb/history/2.0
*@author deament
**/

public interface CbHistoryRequest extends BaseRequest<CbHistoryDTO ,CbHistory>{

}

