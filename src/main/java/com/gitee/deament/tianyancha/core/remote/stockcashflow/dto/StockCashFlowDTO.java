package com.gitee.deament.tianyancha.core.remote.stockcashflow.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*现金流量表
* 可以通过公司名称或ID获取上市公司现金流量表数据，现金流量表数据包括经营活动产生的现金流量、经营活动现金流小计、经营活动现金流出小计、经营活动产生的现金流量净额、投资活动产生的现金流量等
* /services/open/stock/cashFlow/2.0
*@author deament
**/

@TYCURL(value="/services/open/stock/cashFlow/2.0")
public class StockCashFlowDTO implements Serializable{

    /**
     *    年份
     *
    **/
    @ParamRequire(require = false)
    private String year;
    /**
     *    公司名
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 年份
    **/
    public void setYear(String year) {
      this.year = year;
    }
    /**
    *   获取 年份
    **/
    public String getYear() {
      return year;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

