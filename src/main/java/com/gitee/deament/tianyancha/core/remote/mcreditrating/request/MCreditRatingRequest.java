package com.gitee.deament.tianyancha.core.remote.mcreditrating.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcreditrating.entity.MCreditRating;
import com.gitee.deament.tianyancha.core.remote.mcreditrating.dto.MCreditRatingDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业信用评级
* 可以通过公司名称或ID获取企业信用评级信息，企业信用评级信息包括评级公司、主体等级、债券信用等级、评级展望、评级时间等字段的信息
* /services/open/m/creditRating/2.0
*@author deament
**/

public interface MCreditRatingRequest extends BaseRequest<MCreditRatingDTO ,MCreditRating>{

}

