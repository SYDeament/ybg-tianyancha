package com.gitee.deament.tianyancha.core.remote.hiequityinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史股权出质
* 属于HiEquityInfo
* /services/open/hi/equityInfo/2.0
*@author deament
**/

public class HiEquityInfoPledgeeList implements Serializable{

    /**
     *    质权人
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    质权人id
    **/
    @JSONField(name="id")
    private String id;


    /**
    *   设置 质权人
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 质权人
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 质权人id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 质权人id
    **/
    public String getId() {
      return id;
    }



}

