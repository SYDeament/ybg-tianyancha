package com.gitee.deament.tianyancha.core.remote.mrtaxcontravention.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrtaxcontravention.entity.MrTaxContravention;
import com.gitee.deament.tianyancha.core.remote.mrtaxcontravention.dto.MrTaxContraventionDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*税收违法详情
* 根据税收违法ID获取税收违法详情，税收违法详情包括纳税人名称、纳税人识别号、注册地址、违法事实、财务负责人信息、负有责任的中介信息等字段的详细信息
* /services/open/mr/taxContravention/detail/2.0
*@author deament
**/

public interface MrTaxContraventionRequest extends BaseRequest<MrTaxContraventionDTO ,MrTaxContravention>{

}

