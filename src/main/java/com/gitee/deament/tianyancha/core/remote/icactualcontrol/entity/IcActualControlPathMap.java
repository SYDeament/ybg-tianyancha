package com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*疑似实际控制人
* 属于IcActualControl
* /services/open/ic/actualControl/2.0
*@author deament
**/

public class IcActualControlPathMap implements Serializable{

    /**
     *    图谱中所有节点关系集合根目录
    **/
    @JSONField(name="p_0")
    private IcActualControlP_0 p_0;
    /**
     *    该图的一条路径
    **/
    @JSONField(name="p_1")
    private IcActualControlP_1 p_1;


    /**
    *   设置 图谱中所有节点关系集合根目录
    **/
    public void setP_0(IcActualControlP_0 p_0) {
      this.p_0 = p_0;
    }
    /**
    *   获取 图谱中所有节点关系集合根目录
    **/
    public IcActualControlP_0 getP_0() {
      return p_0;
    }
    /**
    *   设置 该图的一条路径
    **/
    public void setP_1(IcActualControlP_1 p_1) {
      this.p_1 = p_1;
    }
    /**
    *   获取 该图的一条路径
    **/
    public IcActualControlP_1 getP_1() {
      return p_1;
    }



}

