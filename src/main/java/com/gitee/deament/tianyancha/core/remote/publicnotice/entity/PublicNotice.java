package com.gitee.deament.tianyancha.core.remote.publicnotice.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.publicnotice.entity.PublicNoticeItems;
/**
*公示催告
* 可以通过公司名称或ID获取企业票据公示催告信息，企业票据公示催告信息包括票据公示催告详情、票据号、票据类型、票面金额、公告日期、公告内容等字段的详细信息
* /services/v4/open/publicNotice
*@author deament
**/

public class PublicNotice implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<PublicNoticeItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<PublicNoticeItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<PublicNoticeItems> getItems() {
      return items;
    }



}

