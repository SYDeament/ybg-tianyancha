package com.gitee.deament.tianyancha.core.remote.proximity.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.proximity.entity.ProximityItems;
/**
*天眼地图
* 附近公司
* /services/v4/open/proximity
*@author deament
**/

public class Proximity implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<ProximityItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<ProximityItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<ProximityItems> getItems() {
      return items;
    }



}

