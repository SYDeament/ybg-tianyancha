package com.gitee.deament.tianyancha.core.remote.searchv3.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*按行业/区域查询公司
* 可以通过关键词、行业、省市区获取企业列表，企业列表包括公司名称或ID、类型、成立日期、经营状态、统一社会信用代码等字段的详细信息
* /services/v4/open/searchV3
*@author deament
**/

@TYCURL(value="/services/v4/open/searchV3")
public class SearchV3DTO implements Serializable{

    /**
     *    行业code（参考文件category.json）
     *
    **/
    @ParamRequire(require = false)
    private String categoryGuobiao;
    /**
     *    区域code（参考文件areaCode.json）
     *
    **/
    @ParamRequire(require = false)
    private Long areaCode;
    /**
     *    关键词
     *
    **/
    @ParamRequire(require = true)
    private String word;
    /**
     *    当前页（最大50页，每页20条数据）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 行业code（参考文件category.json）
    **/
    public void setCategoryGuobiao(String categoryGuobiao) {
      this.categoryGuobiao = categoryGuobiao;
    }
    /**
    *   获取 行业code（参考文件category.json）
    **/
    public String getCategoryGuobiao() {
      return categoryGuobiao;
    }
    /**
    *   设置 区域code（参考文件areaCode.json）
    **/
    public void setAreaCode(Long areaCode) {
      this.areaCode = areaCode;
    }
    /**
    *   获取 区域code（参考文件areaCode.json）
    **/
    public Long getAreaCode() {
      return areaCode;
    }
    /**
    *   设置 关键词
    **/
    public void setWord(String word) {
      this.word = word;
    }
    /**
    *   获取 关键词
    **/
    public String getWord() {
      return word;
    }
    /**
    *   设置 当前页（最大50页，每页20条数据）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（最大50页，每页20条数据）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

