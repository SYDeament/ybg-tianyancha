package com.gitee.deament.tianyancha.core.base;


import java.util.List;
import java.util.Map;

/**
 * 天眼查api请求
 * @author  deament
 */
public interface HttpRequestI {

    /**
     * get 请求
     *
     * @param url    地址
     * @param params 参数
     * @param clazz  反射的类
     * @param <T>
     * @return 反射的类的实例
     * @throws Exception
     */
    public <T> T get(String url, Map<String, Object> params, Class<T> clazz) throws Exception ;

    /**
     * get 请求
     *
     * @param url    地址
     * @param params 参数
     * @param clazz  反射的类
     * @param <T>
     * @return 反射的类的实例
     * @throws Exception
     */
    public <T> List<T> getList(String url, Map<String, Object> params, Class<T> clazz) throws Exception ;


    public String get(String url, Map<String, Object> params) throws Exception;
}
