package com.gitee.deament.tianyancha.core.remote.cbjudicial.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*司法风险
* 属于CbJudicial
* /services/open/cb/judicial/2.0
*@author deament
**/

public class CbJudicialZhixingList implements Serializable{

    /**
     *    案号
    **/
    @JSONField(name="caseCode")
    private String caseCode;
    /**
     *    身份证号／组织机构代码
    **/
    @JSONField(name="partyCardNum")
    private String partyCardNum;
    /**
     *    被执行人名称
    **/
    @JSONField(name="pname")
    private String pname;
    /**
     *    执行法院
    **/
    @JSONField(name="execCourtName")
    private String execCourtName;
    /**
     *    创建时间
    **/
    @JSONField(name="caseCreateTime")
    private String caseCreateTime;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    执行标的
    **/
    @JSONField(name="execMoney")
    private String execMoney;


    /**
    *   设置 案号
    **/
    public void setCaseCode(String caseCode) {
      this.caseCode = caseCode;
    }
    /**
    *   获取 案号
    **/
    public String getCaseCode() {
      return caseCode;
    }
    /**
    *   设置 身份证号／组织机构代码
    **/
    public void setPartyCardNum(String partyCardNum) {
      this.partyCardNum = partyCardNum;
    }
    /**
    *   获取 身份证号／组织机构代码
    **/
    public String getPartyCardNum() {
      return partyCardNum;
    }
    /**
    *   设置 被执行人名称
    **/
    public void setPname(String pname) {
      this.pname = pname;
    }
    /**
    *   获取 被执行人名称
    **/
    public String getPname() {
      return pname;
    }
    /**
    *   设置 执行法院
    **/
    public void setExecCourtName(String execCourtName) {
      this.execCourtName = execCourtName;
    }
    /**
    *   获取 执行法院
    **/
    public String getExecCourtName() {
      return execCourtName;
    }
    /**
    *   设置 创建时间
    **/
    public void setCaseCreateTime(String caseCreateTime) {
      this.caseCreateTime = caseCreateTime;
    }
    /**
    *   获取 创建时间
    **/
    public String getCaseCreateTime() {
      return caseCreateTime;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 执行标的
    **/
    public void setExecMoney(String execMoney) {
      this.execMoney = execMoney;
    }
    /**
    *   获取 执行标的
    **/
    public String getExecMoney() {
      return execMoney;
    }



}

