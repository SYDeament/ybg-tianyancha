package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcPawnList implements Serializable{

    /**
     *    名称
    **/
    @JSONField(name="pawnName")
    private String pawnName;
    /**
     *    所有权归属
    **/
    @JSONField(name="ownership")
    private String ownership;
    /**
     *    备注
    **/
    @JSONField(name="remark")
    private String remark;
    /**
     *    数量、质量、状况、所在地等情况
    **/
    @JSONField(name="detail")
    private String detail;


    /**
    *   设置 名称
    **/
    public void setPawnName(String pawnName) {
      this.pawnName = pawnName;
    }
    /**
    *   获取 名称
    **/
    public String getPawnName() {
      return pawnName;
    }
    /**
    *   设置 所有权归属
    **/
    public void setOwnership(String ownership) {
      this.ownership = ownership;
    }
    /**
    *   获取 所有权归属
    **/
    public String getOwnership() {
      return ownership;
    }
    /**
    *   设置 备注
    **/
    public void setRemark(String remark) {
      this.remark = remark;
    }
    /**
    *   获取 备注
    **/
    public String getRemark() {
      return remark;
    }
    /**
    *   设置 数量、质量、状况、所在地等情况
    **/
    public void setDetail(String detail) {
      this.detail = detail;
    }
    /**
    *   获取 数量、质量、状况、所在地等情况
    **/
    public String getDetail() {
      return detail;
    }



}

