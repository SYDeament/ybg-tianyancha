package com.gitee.deament.tianyancha.core.remote.icchangeinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icchangeinfo.entity.IcChangeinfoItems;
/**
*变更记录
* 可以通过公司名称或ID获取企业变更记录，变更记录包括工商变更事项、变更前后信息等字段的详细信息
* /services/open/ic/changeinfo/2.0
*@author deament
**/

public class IcChangeinfo implements Serializable{

    /**
     *    变更总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcChangeinfoItems> items;


    /**
    *   设置 变更总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 变更总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcChangeinfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcChangeinfoItems> getItems() {
      return items;
    }



}

