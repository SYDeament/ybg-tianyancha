package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryLawSuitList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryPlaintiff;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryDefendant;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryKtAnnouncementList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryCreditCodeList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryCapital;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryShareHolderList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryAbnormalList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryEquityList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryZhixingList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryPubishmentList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryDeadLineList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryRegCapitalList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryHistoryNameList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryCourtAnnouncementList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryDishonestList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryLicenseCreditchinaList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryIcpList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryLocationList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryPawnList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryChangeList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryPeopleList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryMortlist;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryBusinessScopeList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryTypeList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryOrgNumberList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryInvestList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryPunishmentCreditchinaList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryLicenseList;
import com.gitee.deament.tianyancha.core.remote.cbhistory.entity.CbHistoryRegNumberList;
/**
*历史信息
* 可以通过公司名称或ID获取包含历史工商信息、历史股东信息、历史对外投资、历史行政许可、历史法律诉讼、历史法院公告、历史开庭公告、历史司法协助、历史失信人、历史被执行人、历史经营异常、历史股权出质、历史动产抵押、历史行政处罚、历史商标信息、历史网站备案等维度的相关信息
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistory implements Serializable{

    /**
     *    法律诉讼
    **/
    @JSONField(name="lawSuitList")
    private List<CbHistoryLawSuitList> lawSuitList;
    /**
     *    开庭公告
    **/
    @JSONField(name="ktAnnouncementList")
    private List<CbHistoryKtAnnouncementList> ktAnnouncementList;
    /**
     *    历史统一信用代码
    **/
    @JSONField(name="creditCodeList")
    private List<CbHistoryCreditCodeList> creditCodeList;
    /**
     *    股东
    **/
    @JSONField(name="shareHolderList")
    private List<CbHistoryShareHolderList> shareHolderList;
    /**
     *    经营异常
    **/
    @JSONField(name="abnormalList")
    private List<CbHistoryAbnormalList> abnormalList;
    /**
     *    股权出质
    **/
    @JSONField(name="equityList")
    private List<CbHistoryEquityList> equityList;
    /**
     *    被执行人
    **/
    @JSONField(name="zhixingList")
    private List<CbHistoryZhixingList> zhixingList;
    /**
     *    行政处罚
    **/
    @JSONField(name="pubishmentList")
    private List<CbHistoryPubishmentList> pubishmentList;
    /**
     *    历史营业期限
    **/
    @JSONField(name="deadLineList")
    private List<CbHistoryDeadLineList> deadLineList;
    /**
     *    历史注册资本
    **/
    @JSONField(name="regCapitalList")
    private List<CbHistoryRegCapitalList> regCapitalList;
    /**
     *    历史曾用名
    **/
    @JSONField(name="historyNameList")
    private List<CbHistoryHistoryNameList> historyNameList;
    /**
     *    法院公告
    **/
    @JSONField(name="courtAnnouncementList")
    private List<CbHistoryCourtAnnouncementList> courtAnnouncementList;
    /**
     *    失信人
    **/
    @JSONField(name="dishonestList")
    private List<CbHistoryDishonestList> dishonestList;
    /**
     *    行政许可-信用中国
    **/
    @JSONField(name="licenseCreditchinaList")
    private List<CbHistoryLicenseCreditchinaList> licenseCreditchinaList;
    /**
     *    网站备案
    **/
    @JSONField(name="IcpList")
    private List<CbHistoryIcpList> IcpList;
    /**
     *    历史注册地址
    **/
    @JSONField(name="locationList")
    private List<CbHistoryLocationList> locationList;
    /**
     *    动产抵押
    **/
    @JSONField(name="mortlist")
    private List<CbHistoryMortlist> mortlist;
    /**
     *    历史经营范围
    **/
    @JSONField(name="businessScopeList")
    private List<CbHistoryBusinessScopeList> businessScopeList;
    /**
     *    历史企业类型
    **/
    @JSONField(name="typeList")
    private List<CbHistoryTypeList> typeList;
    /**
     *    历史组织机构代码
    **/
    @JSONField(name="orgNumberList")
    private List<CbHistoryOrgNumberList> orgNumberList;
    /**
     *    对外投资
    **/
    @JSONField(name="investList")
    private List<CbHistoryInvestList> investList;
    /**
     *    行政处罚-信用中国
    **/
    @JSONField(name="punishmentCreditchinaList")
    private List<CbHistoryPunishmentCreditchinaList> punishmentCreditchinaList;
    /**
     *    行政许可
    **/
    @JSONField(name="licenseList")
    private List<CbHistoryLicenseList> licenseList;
    /**
     *    历史注册号
    **/
    @JSONField(name="regNumberList")
    private List<CbHistoryRegNumberList> regNumberList;


    /**
    *   设置 法律诉讼
    **/
    public void setLawSuitList(List<CbHistoryLawSuitList> lawSuitList) {
      this.lawSuitList = lawSuitList;
    }
    /**
    *   获取 法律诉讼
    **/
    public List<CbHistoryLawSuitList> getLawSuitList() {
      return lawSuitList;
    }
    /**
    *   设置 开庭公告
    **/
    public void setKtAnnouncementList(List<CbHistoryKtAnnouncementList> ktAnnouncementList) {
      this.ktAnnouncementList = ktAnnouncementList;
    }
    /**
    *   获取 开庭公告
    **/
    public List<CbHistoryKtAnnouncementList> getKtAnnouncementList() {
      return ktAnnouncementList;
    }
    /**
    *   设置 历史统一信用代码
    **/
    public void setCreditCodeList(List<CbHistoryCreditCodeList> creditCodeList) {
      this.creditCodeList = creditCodeList;
    }
    /**
    *   获取 历史统一信用代码
    **/
    public List<CbHistoryCreditCodeList> getCreditCodeList() {
      return creditCodeList;
    }
    /**
    *   设置 股东
    **/
    public void setShareHolderList(List<CbHistoryShareHolderList> shareHolderList) {
      this.shareHolderList = shareHolderList;
    }
    /**
    *   获取 股东
    **/
    public List<CbHistoryShareHolderList> getShareHolderList() {
      return shareHolderList;
    }
    /**
    *   设置 经营异常
    **/
    public void setAbnormalList(List<CbHistoryAbnormalList> abnormalList) {
      this.abnormalList = abnormalList;
    }
    /**
    *   获取 经营异常
    **/
    public List<CbHistoryAbnormalList> getAbnormalList() {
      return abnormalList;
    }
    /**
    *   设置 股权出质
    **/
    public void setEquityList(List<CbHistoryEquityList> equityList) {
      this.equityList = equityList;
    }
    /**
    *   获取 股权出质
    **/
    public List<CbHistoryEquityList> getEquityList() {
      return equityList;
    }
    /**
    *   设置 被执行人
    **/
    public void setZhixingList(List<CbHistoryZhixingList> zhixingList) {
      this.zhixingList = zhixingList;
    }
    /**
    *   获取 被执行人
    **/
    public List<CbHistoryZhixingList> getZhixingList() {
      return zhixingList;
    }
    /**
    *   设置 行政处罚
    **/
    public void setPubishmentList(List<CbHistoryPubishmentList> pubishmentList) {
      this.pubishmentList = pubishmentList;
    }
    /**
    *   获取 行政处罚
    **/
    public List<CbHistoryPubishmentList> getPubishmentList() {
      return pubishmentList;
    }
    /**
    *   设置 历史营业期限
    **/
    public void setDeadLineList(List<CbHistoryDeadLineList> deadLineList) {
      this.deadLineList = deadLineList;
    }
    /**
    *   获取 历史营业期限
    **/
    public List<CbHistoryDeadLineList> getDeadLineList() {
      return deadLineList;
    }
    /**
    *   设置 历史注册资本
    **/
    public void setRegCapitalList(List<CbHistoryRegCapitalList> regCapitalList) {
      this.regCapitalList = regCapitalList;
    }
    /**
    *   获取 历史注册资本
    **/
    public List<CbHistoryRegCapitalList> getRegCapitalList() {
      return regCapitalList;
    }
    /**
    *   设置 历史曾用名
    **/
    public void setHistoryNameList(List<CbHistoryHistoryNameList> historyNameList) {
      this.historyNameList = historyNameList;
    }
    /**
    *   获取 历史曾用名
    **/
    public List<CbHistoryHistoryNameList> getHistoryNameList() {
      return historyNameList;
    }
    /**
    *   设置 法院公告
    **/
    public void setCourtAnnouncementList(List<CbHistoryCourtAnnouncementList> courtAnnouncementList) {
      this.courtAnnouncementList = courtAnnouncementList;
    }
    /**
    *   获取 法院公告
    **/
    public List<CbHistoryCourtAnnouncementList> getCourtAnnouncementList() {
      return courtAnnouncementList;
    }
    /**
    *   设置 失信人
    **/
    public void setDishonestList(List<CbHistoryDishonestList> dishonestList) {
      this.dishonestList = dishonestList;
    }
    /**
    *   获取 失信人
    **/
    public List<CbHistoryDishonestList> getDishonestList() {
      return dishonestList;
    }
    /**
    *   设置 行政许可-信用中国
    **/
    public void setLicenseCreditchinaList(List<CbHistoryLicenseCreditchinaList> licenseCreditchinaList) {
      this.licenseCreditchinaList = licenseCreditchinaList;
    }
    /**
    *   获取 行政许可-信用中国
    **/
    public List<CbHistoryLicenseCreditchinaList> getLicenseCreditchinaList() {
      return licenseCreditchinaList;
    }
    /**
    *   设置 网站备案
    **/
    public void setIcpList(List<CbHistoryIcpList> IcpList) {
      this.IcpList = IcpList;
    }
    /**
    *   获取 网站备案
    **/
    public List<CbHistoryIcpList> getIcpList() {
      return IcpList;
    }
    /**
    *   设置 历史注册地址
    **/
    public void setLocationList(List<CbHistoryLocationList> locationList) {
      this.locationList = locationList;
    }
    /**
    *   获取 历史注册地址
    **/
    public List<CbHistoryLocationList> getLocationList() {
      return locationList;
    }
    /**
    *   设置 动产抵押
    **/
    public void setMortlist(List<CbHistoryMortlist> mortlist) {
      this.mortlist = mortlist;
    }
    /**
    *   获取 动产抵押
    **/
    public List<CbHistoryMortlist> getMortlist() {
      return mortlist;
    }
    /**
    *   设置 历史经营范围
    **/
    public void setBusinessScopeList(List<CbHistoryBusinessScopeList> businessScopeList) {
      this.businessScopeList = businessScopeList;
    }
    /**
    *   获取 历史经营范围
    **/
    public List<CbHistoryBusinessScopeList> getBusinessScopeList() {
      return businessScopeList;
    }
    /**
    *   设置 历史企业类型
    **/
    public void setTypeList(List<CbHistoryTypeList> typeList) {
      this.typeList = typeList;
    }
    /**
    *   获取 历史企业类型
    **/
    public List<CbHistoryTypeList> getTypeList() {
      return typeList;
    }
    /**
    *   设置 历史组织机构代码
    **/
    public void setOrgNumberList(List<CbHistoryOrgNumberList> orgNumberList) {
      this.orgNumberList = orgNumberList;
    }
    /**
    *   获取 历史组织机构代码
    **/
    public List<CbHistoryOrgNumberList> getOrgNumberList() {
      return orgNumberList;
    }
    /**
    *   设置 对外投资
    **/
    public void setInvestList(List<CbHistoryInvestList> investList) {
      this.investList = investList;
    }
    /**
    *   获取 对外投资
    **/
    public List<CbHistoryInvestList> getInvestList() {
      return investList;
    }
    /**
    *   设置 行政处罚-信用中国
    **/
    public void setPunishmentCreditchinaList(List<CbHistoryPunishmentCreditchinaList> punishmentCreditchinaList) {
      this.punishmentCreditchinaList = punishmentCreditchinaList;
    }
    /**
    *   获取 行政处罚-信用中国
    **/
    public List<CbHistoryPunishmentCreditchinaList> getPunishmentCreditchinaList() {
      return punishmentCreditchinaList;
    }
    /**
    *   设置 行政许可
    **/
    public void setLicenseList(List<CbHistoryLicenseList> licenseList) {
      this.licenseList = licenseList;
    }
    /**
    *   获取 行政许可
    **/
    public List<CbHistoryLicenseList> getLicenseList() {
      return licenseList;
    }
    /**
    *   设置 历史注册号
    **/
    public void setRegNumberList(List<CbHistoryRegNumberList> regNumberList) {
      this.regNumberList = regNumberList;
    }
    /**
    *   获取 历史注册号
    **/
    public List<CbHistoryRegNumberList> getRegNumberList() {
      return regNumberList;
    }



}

