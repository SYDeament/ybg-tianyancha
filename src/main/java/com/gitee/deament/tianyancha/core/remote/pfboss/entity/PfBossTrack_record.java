package com.gitee.deament.tianyancha.core.remote.pfboss.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*高管信息
* 属于PfBoss
* /services/open/pf/boss/2.0
*@author deament
**/

public class PfBossTrack_record implements Serializable{

    /**
     *    任职单位
    **/
    @JSONField(name="unit")
    private String unit;
    /**
     *    任职单位id
    **/
    @JSONField(name="unit_graph_id")
    private Integer unit_graph_id;
    /**
     *    职务
    **/
    @JSONField(name="position")
    private String position;
    /**
     *    任职时间
    **/
    @JSONField(name="time_scope")
    private String time_scope;


    /**
    *   设置 任职单位
    **/
    public void setUnit(String unit) {
      this.unit = unit;
    }
    /**
    *   获取 任职单位
    **/
    public String getUnit() {
      return unit;
    }
    /**
    *   设置 任职单位id
    **/
    public void setUnit_graph_id(Integer unit_graph_id) {
      this.unit_graph_id = unit_graph_id;
    }
    /**
    *   获取 任职单位id
    **/
    public Integer getUnit_graph_id() {
      return unit_graph_id;
    }
    /**
    *   设置 职务
    **/
    public void setPosition(String position) {
      this.position = position;
    }
    /**
    *   获取 职务
    **/
    public String getPosition() {
      return position;
    }
    /**
    *   设置 任职时间
    **/
    public void setTime_scope(String time_scope) {
      this.time_scope = time_scope;
    }
    /**
    *   获取 任职时间
    **/
    public String getTime_scope() {
      return time_scope;
    }



}

