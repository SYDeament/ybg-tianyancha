package com.gitee.deament.tianyancha.core.remote.mappbkinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mappbkinfo.entity.MAppbkInfoItems;
/**
*产品信息
* 可以通过公司名称或ID获取企业产品信息，企业产品信息包括主要产品、产品分类、产品介绍等字段的详细信息
* /services/open/m/appbkInfo/2.0
*@author deament
**/

public class MAppbkInfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MAppbkInfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MAppbkInfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MAppbkInfoItems> getItems() {
      return items;
    }



}

