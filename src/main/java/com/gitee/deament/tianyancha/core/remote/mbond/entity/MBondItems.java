package com.gitee.deament.tianyancha.core.remote.mbond.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*债券信息
* 属于MBond
* /services/open/m/bond/2.0
*@author deament
**/

public class MBondItems implements Serializable{

    /**
     *    债券号
    **/
    @JSONField(name="bondNum")
    private String bondNum;
    /**
     *    备注
    **/
    @JSONField(name="remark")
    private String remark;
    /**
     *    利差(BP)
    **/
    @JSONField(name="interestDiff")
    private Integer interestDiff;
    /**
     *    债券名称
    **/
    @JSONField(name="bondName")
    private String bondName;
    /**
     *    债劵摘牌日
    **/
    @JSONField(name="bondStopTime")
    private Integer bondStopTime;
    /**
     *    发行价格(元)
    **/
    @JSONField(name="issuedPrice")
    private Integer issuedPrice;
    /**
     *    注
    **/
    @JSONField(name="tip")
    private String tip;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    托管机构
    **/
    @JSONField(name="escrowAgent")
    private String escrowAgent;
    /**
     *    参考利率
    **/
    @JSONField(name="refInterestRate")
    private Integer refInterestRate;
    /**
     *    债劵期限
    **/
    @JSONField(name="bondTimeLimit")
    private String bondTimeLimit;
    /**
     *    债项评级
    **/
    @JSONField(name="debtRating")
    private String debtRating;
    /**
     *    债劵发行日
    **/
    @JSONField(name="publishTime")
    private Integer publishTime;
    /**
     *    实际发行量(亿)
    **/
    @JSONField(name="realIssuedQuantity")
    private Integer realIssuedQuantity;
    /**
     *    票面利率(%)
    **/
    @JSONField(name="faceInterestRate")
    private Integer faceInterestRate;
    /**
     *    更新时间
    **/
    @JSONField(name="updateTime")
    private Long updateTime;
    /**
     *    行权类型
    **/
    @JSONField(name="exeRightType")
    private String exeRightType;
    /**
     *    计划发行量(亿)
    **/
    @JSONField(name="planIssuedQuantity")
    private Integer planIssuedQuantity;
    /**
     *    计息方式
    **/
    @JSONField(name="calInterestType")
    private String calInterestType;
    /**
     *    债劵到期日
    **/
    @JSONField(name="publishExpireTime")
    private Integer publishExpireTime;
    /**
     *    创建时间
    **/
    @JSONField(name="createTime")
    private Long createTime;
    /**
     *    面值
    **/
    @JSONField(name="faceValue")
    private Integer faceValue;
    /**
     *    发行人
    **/
    @JSONField(name="publisherName")
    private String publisherName;
    /**
     *    流通范围
    **/
    @JSONField(name="flowRange")
    private String flowRange;
    /**
     *    上市交易日
    **/
    @JSONField(name="bondTradeTime")
    private Integer bondTradeTime;
    /**
     *    债券类型
    **/
    @JSONField(name="bondType")
    private String bondType;
    /**
     *    
    **/
    @JSONField(name="exeRightTime")
    private Integer exeRightTime;
    /**
     *    信用评级机构
    **/
    @JSONField(name="creditRatingGov")
    private String creditRatingGov;
    /**
     *    付息频率
    **/
    @JSONField(name="payInterestHZ")
    private String payInterestHZ;
    /**
     *    债券起息日
    **/
    @JSONField(name="startCalInterestTime")
    private Integer startCalInterestTime;


    /**
    *   设置 债券号
    **/
    public void setBondNum(String bondNum) {
      this.bondNum = bondNum;
    }
    /**
    *   获取 债券号
    **/
    public String getBondNum() {
      return bondNum;
    }
    /**
    *   设置 备注
    **/
    public void setRemark(String remark) {
      this.remark = remark;
    }
    /**
    *   获取 备注
    **/
    public String getRemark() {
      return remark;
    }
    /**
    *   设置 利差(BP)
    **/
    public void setInterestDiff(Integer interestDiff) {
      this.interestDiff = interestDiff;
    }
    /**
    *   获取 利差(BP)
    **/
    public Integer getInterestDiff() {
      return interestDiff;
    }
    /**
    *   设置 债券名称
    **/
    public void setBondName(String bondName) {
      this.bondName = bondName;
    }
    /**
    *   获取 债券名称
    **/
    public String getBondName() {
      return bondName;
    }
    /**
    *   设置 债劵摘牌日
    **/
    public void setBondStopTime(Integer bondStopTime) {
      this.bondStopTime = bondStopTime;
    }
    /**
    *   获取 债劵摘牌日
    **/
    public Integer getBondStopTime() {
      return bondStopTime;
    }
    /**
    *   设置 发行价格(元)
    **/
    public void setIssuedPrice(Integer issuedPrice) {
      this.issuedPrice = issuedPrice;
    }
    /**
    *   获取 发行价格(元)
    **/
    public Integer getIssuedPrice() {
      return issuedPrice;
    }
    /**
    *   设置 注
    **/
    public void setTip(String tip) {
      this.tip = tip;
    }
    /**
    *   获取 注
    **/
    public String getTip() {
      return tip;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 托管机构
    **/
    public void setEscrowAgent(String escrowAgent) {
      this.escrowAgent = escrowAgent;
    }
    /**
    *   获取 托管机构
    **/
    public String getEscrowAgent() {
      return escrowAgent;
    }
    /**
    *   设置 参考利率
    **/
    public void setRefInterestRate(Integer refInterestRate) {
      this.refInterestRate = refInterestRate;
    }
    /**
    *   获取 参考利率
    **/
    public Integer getRefInterestRate() {
      return refInterestRate;
    }
    /**
    *   设置 债劵期限
    **/
    public void setBondTimeLimit(String bondTimeLimit) {
      this.bondTimeLimit = bondTimeLimit;
    }
    /**
    *   获取 债劵期限
    **/
    public String getBondTimeLimit() {
      return bondTimeLimit;
    }
    /**
    *   设置 债项评级
    **/
    public void setDebtRating(String debtRating) {
      this.debtRating = debtRating;
    }
    /**
    *   获取 债项评级
    **/
    public String getDebtRating() {
      return debtRating;
    }
    /**
    *   设置 债劵发行日
    **/
    public void setPublishTime(Integer publishTime) {
      this.publishTime = publishTime;
    }
    /**
    *   获取 债劵发行日
    **/
    public Integer getPublishTime() {
      return publishTime;
    }
    /**
    *   设置 实际发行量(亿)
    **/
    public void setRealIssuedQuantity(Integer realIssuedQuantity) {
      this.realIssuedQuantity = realIssuedQuantity;
    }
    /**
    *   获取 实际发行量(亿)
    **/
    public Integer getRealIssuedQuantity() {
      return realIssuedQuantity;
    }
    /**
    *   设置 票面利率(%)
    **/
    public void setFaceInterestRate(Integer faceInterestRate) {
      this.faceInterestRate = faceInterestRate;
    }
    /**
    *   获取 票面利率(%)
    **/
    public Integer getFaceInterestRate() {
      return faceInterestRate;
    }
    /**
    *   设置 更新时间
    **/
    public void setUpdateTime(Long updateTime) {
      this.updateTime = updateTime;
    }
    /**
    *   获取 更新时间
    **/
    public Long getUpdateTime() {
      return updateTime;
    }
    /**
    *   设置 行权类型
    **/
    public void setExeRightType(String exeRightType) {
      this.exeRightType = exeRightType;
    }
    /**
    *   获取 行权类型
    **/
    public String getExeRightType() {
      return exeRightType;
    }
    /**
    *   设置 计划发行量(亿)
    **/
    public void setPlanIssuedQuantity(Integer planIssuedQuantity) {
      this.planIssuedQuantity = planIssuedQuantity;
    }
    /**
    *   获取 计划发行量(亿)
    **/
    public Integer getPlanIssuedQuantity() {
      return planIssuedQuantity;
    }
    /**
    *   设置 计息方式
    **/
    public void setCalInterestType(String calInterestType) {
      this.calInterestType = calInterestType;
    }
    /**
    *   获取 计息方式
    **/
    public String getCalInterestType() {
      return calInterestType;
    }
    /**
    *   设置 债劵到期日
    **/
    public void setPublishExpireTime(Integer publishExpireTime) {
      this.publishExpireTime = publishExpireTime;
    }
    /**
    *   获取 债劵到期日
    **/
    public Integer getPublishExpireTime() {
      return publishExpireTime;
    }
    /**
    *   设置 创建时间
    **/
    public void setCreateTime(Long createTime) {
      this.createTime = createTime;
    }
    /**
    *   获取 创建时间
    **/
    public Long getCreateTime() {
      return createTime;
    }
    /**
    *   设置 面值
    **/
    public void setFaceValue(Integer faceValue) {
      this.faceValue = faceValue;
    }
    /**
    *   获取 面值
    **/
    public Integer getFaceValue() {
      return faceValue;
    }
    /**
    *   设置 发行人
    **/
    public void setPublisherName(String publisherName) {
      this.publisherName = publisherName;
    }
    /**
    *   获取 发行人
    **/
    public String getPublisherName() {
      return publisherName;
    }
    /**
    *   设置 流通范围
    **/
    public void setFlowRange(String flowRange) {
      this.flowRange = flowRange;
    }
    /**
    *   获取 流通范围
    **/
    public String getFlowRange() {
      return flowRange;
    }
    /**
    *   设置 上市交易日
    **/
    public void setBondTradeTime(Integer bondTradeTime) {
      this.bondTradeTime = bondTradeTime;
    }
    /**
    *   获取 上市交易日
    **/
    public Integer getBondTradeTime() {
      return bondTradeTime;
    }
    /**
    *   设置 债券类型
    **/
    public void setBondType(String bondType) {
      this.bondType = bondType;
    }
    /**
    *   获取 债券类型
    **/
    public String getBondType() {
      return bondType;
    }
    /**
    *   设置 
    **/
    public void setExeRightTime(Integer exeRightTime) {
      this.exeRightTime = exeRightTime;
    }
    /**
    *   获取 
    **/
    public Integer getExeRightTime() {
      return exeRightTime;
    }
    /**
    *   设置 信用评级机构
    **/
    public void setCreditRatingGov(String creditRatingGov) {
      this.creditRatingGov = creditRatingGov;
    }
    /**
    *   获取 信用评级机构
    **/
    public String getCreditRatingGov() {
      return creditRatingGov;
    }
    /**
    *   设置 付息频率
    **/
    public void setPayInterestHZ(String payInterestHZ) {
      this.payInterestHZ = payInterestHZ;
    }
    /**
    *   获取 付息频率
    **/
    public String getPayInterestHZ() {
      return payInterestHZ;
    }
    /**
    *   设置 债券起息日
    **/
    public void setStartCalInterestTime(Integer startCalInterestTime) {
      this.startCalInterestTime = startCalInterestTime;
    }
    /**
    *   获取 债券起息日
    **/
    public Integer getStartCalInterestTime() {
      return startCalInterestTime;
    }



}

