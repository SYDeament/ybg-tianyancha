package com.gitee.deament.tianyancha.core.remote.mcertificate.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*资质证书
* 属于MCertificate
* /services/open/m/certificate/2.0
*@author deament
**/

public class MCertificateItems implements Serializable{

    /**
     *    证书编号
    **/
    @JSONField(name="certNo")
    private String certNo;
    /**
     *    截止日期
    **/
    @JSONField(name="endDate")
    private String endDate;
    /**
     *    证书类型
    **/
    @JSONField(name="certificateName")
    private String certificateName;
    /**
     *    uuid
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    详细信息
    **/
    @JSONField(name="detail")
    private List<MCertificateDetail> detail;
    /**
     *    发证日期
    **/
    @JSONField(name="startDate")
    private String startDate;


    /**
    *   设置 证书编号
    **/
    public void setCertNo(String certNo) {
      this.certNo = certNo;
    }
    /**
    *   获取 证书编号
    **/
    public String getCertNo() {
      return certNo;
    }
    /**
    *   设置 截止日期
    **/
    public void setEndDate(String endDate) {
      this.endDate = endDate;
    }
    /**
    *   获取 截止日期
    **/
    public String getEndDate() {
      return endDate;
    }
    /**
    *   设置 证书类型
    **/
    public void setCertificateName(String certificateName) {
      this.certificateName = certificateName;
    }
    /**
    *   获取 证书类型
    **/
    public String getCertificateName() {
      return certificateName;
    }
    /**
    *   设置 uuid
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 uuid
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 详细信息
    **/
    public void setDetail(List<MCertificateDetail> detail) {
      this.detail = detail;
    }
    /**
    *   获取 详细信息
    **/
    public List<MCertificateDetail> getDetail() {
      return detail;
    }
    /**
    *   设置 发证日期
    **/
    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 发证日期
    **/
    public String getStartDate() {
      return startDate;
    }



}

