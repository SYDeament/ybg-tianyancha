package com.gitee.deament.tianyancha.core.remote.searchv3.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.searchv3.entity.SearchV3;
import com.gitee.deament.tianyancha.core.remote.searchv3.dto.SearchV3DTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*按行业/区域查询公司
* 可以通过关键词、行业、省市区获取企业列表，企业列表包括公司名称或ID、类型、成立日期、经营状态、统一社会信用代码等字段的详细信息
* /services/v4/open/searchV3
*@author deament
**/
@Component("searchV3RequestImpl")
public class SearchV3RequestImpl extends BaseRequestImpl<SearchV3,SearchV3DTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/searchV3";
    }
}

