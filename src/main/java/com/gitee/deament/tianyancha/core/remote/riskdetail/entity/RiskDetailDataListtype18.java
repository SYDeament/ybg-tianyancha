package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype18 implements Serializable{

    /**
     *    变更日期
    **/
    @JSONField(name="changeTime")
    private String changeTime;
    /**
     *    mediumtext 变更后
    **/
    @JSONField(name="contentAfter")
    private String contentAfter;
    /**
     *    创建时间
    **/
    @JSONField(name="createTime")
    private String createTime;
    /**
     *    mediumtext 变更前
    **/
    @JSONField(name="contentBefore")
    private String contentBefore;
    /**
     *    变更事项
    **/
    @JSONField(name="changeItem")
    private String changeItem;


    /**
    *   设置 变更日期
    **/
    public void setChangeTime(String changeTime) {
      this.changeTime = changeTime;
    }
    /**
    *   获取 变更日期
    **/
    public String getChangeTime() {
      return changeTime;
    }
    /**
    *   设置 mediumtext 变更后
    **/
    public void setContentAfter(String contentAfter) {
      this.contentAfter = contentAfter;
    }
    /**
    *   获取 mediumtext 变更后
    **/
    public String getContentAfter() {
      return contentAfter;
    }
    /**
    *   设置 创建时间
    **/
    public void setCreateTime(String createTime) {
      this.createTime = createTime;
    }
    /**
    *   获取 创建时间
    **/
    public String getCreateTime() {
      return createTime;
    }
    /**
    *   设置 mediumtext 变更前
    **/
    public void setContentBefore(String contentBefore) {
      this.contentBefore = contentBefore;
    }
    /**
    *   获取 mediumtext 变更前
    **/
    public String getContentBefore() {
      return contentBefore;
    }
    /**
    *   设置 变更事项
    **/
    public void setChangeItem(String changeItem) {
      this.changeItem = changeItem;
    }
    /**
    *   获取 变更事项
    **/
    public String getChangeItem() {
      return changeItem;
    }



}

