package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailParty1StrApp implements Serializable{

    /**
     *    公司（人）名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    类型 1：公司 ，其他：人
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 公司（人）名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司（人）名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 类型 1：公司 ，其他：人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 类型 1：公司 ，其他：人
    **/
    public Integer getType() {
      return type;
    }



}

