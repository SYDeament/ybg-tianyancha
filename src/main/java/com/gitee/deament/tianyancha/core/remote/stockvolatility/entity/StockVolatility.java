package com.gitee.deament.tianyancha.core.remote.stockvolatility.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*股票行情
* 可以通过公司名称或ID获取上市公司股票行情数据，上市公司股票行情数据包括股票名、股票号、总市值、流通市值等
* /services/open/stock/volatility/2.0
*@author deament
**/

public class StockVolatility implements Serializable{

    /**
     *    股票名
    **/
    @JSONField(name="stockname")
    private String stockname;
    /**
     *    成交量
    **/
    @JSONField(name="tamount")
    private String tamount;
    /**
     *    总市值
    **/
    @JSONField(name="tvalue")
    private String tvalue;
    /**
     *    浮动率
    **/
    @JSONField(name="hexm_float_rate")
    private String hexm_float_rate;
    /**
     *    市盈率（动）
    **/
    @JSONField(name="fvaluep")
    private String fvaluep;
    /**
     *    股票类型
    **/
    @JSONField(name="stockType")
    private String stockType;
    /**
     *    昨收
    **/
    @JSONField(name="pprice")
    private String pprice;
    /**
     *    今开
    **/
    @JSONField(name="topenprice")
    private String topenprice;
    /**
     *    市净率
    **/
    @JSONField(name="tvaluep")
    private String tvaluep;
    /**
     *    最低
    **/
    @JSONField(name="tlowprice")
    private String tlowprice;
    /**
     *    换手
    **/
    @JSONField(name="tchange")
    private String tchange;
    /**
     *    跌停
    **/
    @JSONField(name="tminprice")
    private String tminprice;
    /**
     *    振幅
    **/
    @JSONField(name="trange")
    private String trange;
    /**
     *    成交额
    **/
    @JSONField(name="tamounttotal")
    private String tamounttotal;
    /**
     *    更新时间
    **/
    @JSONField(name="timeshow")
    private String timeshow;
    /**
     *    流通市值
    **/
    @JSONField(name="flowvalue")
    private String flowvalue;
    /**
     *    当前价格
    **/
    @JSONField(name="hexm_curPrice")
    private String hexm_curPrice;
    /**
     *    最高
    **/
    @JSONField(name="thighprice")
    private String thighprice;
    /**
     *    公司id
    **/
    @JSONField(name="graphId")
    private String graphId;
    /**
     *    涨停
    **/
    @JSONField(name="tmaxprice")
    private String tmaxprice;
    /**
     *    浮动价格
    **/
    @JSONField(name="hexm_float_price")
    private String hexm_float_price;
    /**
     *    股票号
    **/
    @JSONField(name="stockcode")
    private String stockcode;


    /**
    *   设置 股票名
    **/
    public void setStockname(String stockname) {
      this.stockname = stockname;
    }
    /**
    *   获取 股票名
    **/
    public String getStockname() {
      return stockname;
    }
    /**
    *   设置 成交量
    **/
    public void setTamount(String tamount) {
      this.tamount = tamount;
    }
    /**
    *   获取 成交量
    **/
    public String getTamount() {
      return tamount;
    }
    /**
    *   设置 总市值
    **/
    public void setTvalue(String tvalue) {
      this.tvalue = tvalue;
    }
    /**
    *   获取 总市值
    **/
    public String getTvalue() {
      return tvalue;
    }
    /**
    *   设置 浮动率
    **/
    public void setHexm_float_rate(String hexm_float_rate) {
      this.hexm_float_rate = hexm_float_rate;
    }
    /**
    *   获取 浮动率
    **/
    public String getHexm_float_rate() {
      return hexm_float_rate;
    }
    /**
    *   设置 市盈率（动）
    **/
    public void setFvaluep(String fvaluep) {
      this.fvaluep = fvaluep;
    }
    /**
    *   获取 市盈率（动）
    **/
    public String getFvaluep() {
      return fvaluep;
    }
    /**
    *   设置 股票类型
    **/
    public void setStockType(String stockType) {
      this.stockType = stockType;
    }
    /**
    *   获取 股票类型
    **/
    public String getStockType() {
      return stockType;
    }
    /**
    *   设置 昨收
    **/
    public void setPprice(String pprice) {
      this.pprice = pprice;
    }
    /**
    *   获取 昨收
    **/
    public String getPprice() {
      return pprice;
    }
    /**
    *   设置 今开
    **/
    public void setTopenprice(String topenprice) {
      this.topenprice = topenprice;
    }
    /**
    *   获取 今开
    **/
    public String getTopenprice() {
      return topenprice;
    }
    /**
    *   设置 市净率
    **/
    public void setTvaluep(String tvaluep) {
      this.tvaluep = tvaluep;
    }
    /**
    *   获取 市净率
    **/
    public String getTvaluep() {
      return tvaluep;
    }
    /**
    *   设置 最低
    **/
    public void setTlowprice(String tlowprice) {
      this.tlowprice = tlowprice;
    }
    /**
    *   获取 最低
    **/
    public String getTlowprice() {
      return tlowprice;
    }
    /**
    *   设置 换手
    **/
    public void setTchange(String tchange) {
      this.tchange = tchange;
    }
    /**
    *   获取 换手
    **/
    public String getTchange() {
      return tchange;
    }
    /**
    *   设置 跌停
    **/
    public void setTminprice(String tminprice) {
      this.tminprice = tminprice;
    }
    /**
    *   获取 跌停
    **/
    public String getTminprice() {
      return tminprice;
    }
    /**
    *   设置 振幅
    **/
    public void setTrange(String trange) {
      this.trange = trange;
    }
    /**
    *   获取 振幅
    **/
    public String getTrange() {
      return trange;
    }
    /**
    *   设置 成交额
    **/
    public void setTamounttotal(String tamounttotal) {
      this.tamounttotal = tamounttotal;
    }
    /**
    *   获取 成交额
    **/
    public String getTamounttotal() {
      return tamounttotal;
    }
    /**
    *   设置 更新时间
    **/
    public void setTimeshow(String timeshow) {
      this.timeshow = timeshow;
    }
    /**
    *   获取 更新时间
    **/
    public String getTimeshow() {
      return timeshow;
    }
    /**
    *   设置 流通市值
    **/
    public void setFlowvalue(String flowvalue) {
      this.flowvalue = flowvalue;
    }
    /**
    *   获取 流通市值
    **/
    public String getFlowvalue() {
      return flowvalue;
    }
    /**
    *   设置 当前价格
    **/
    public void setHexm_curPrice(String hexm_curPrice) {
      this.hexm_curPrice = hexm_curPrice;
    }
    /**
    *   获取 当前价格
    **/
    public String getHexm_curPrice() {
      return hexm_curPrice;
    }
    /**
    *   设置 最高
    **/
    public void setThighprice(String thighprice) {
      this.thighprice = thighprice;
    }
    /**
    *   获取 最高
    **/
    public String getThighprice() {
      return thighprice;
    }
    /**
    *   设置 公司id
    **/
    public void setGraphId(String graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 公司id
    **/
    public String getGraphId() {
      return graphId;
    }
    /**
    *   设置 涨停
    **/
    public void setTmaxprice(String tmaxprice) {
      this.tmaxprice = tmaxprice;
    }
    /**
    *   获取 涨停
    **/
    public String getTmaxprice() {
      return tmaxprice;
    }
    /**
    *   设置 浮动价格
    **/
    public void setHexm_float_price(String hexm_float_price) {
      this.hexm_float_price = hexm_float_price;
    }
    /**
    *   获取 浮动价格
    **/
    public String getHexm_float_price() {
      return hexm_float_price;
    }
    /**
    *   设置 股票号
    **/
    public void setStockcode(String stockcode) {
      this.stockcode = stockcode;
    }
    /**
    *   获取 股票号
    **/
    public String getStockcode() {
      return stockcode;
    }



}

