package com.gitee.deament.tianyancha.core.remote.icbaseinfov3.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业基本信息（含主要人员）
* 属于IcBaseinfoV3
* /services/open/ic/baseinfoV3/2.0
*@author deament
**/

public class IcBaseinfoV3StaffList implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private List<IcBaseinfoV3Result> result;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;


    /**
    *   设置 
    **/
    public void setResult(List<IcBaseinfoV3Result> result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public List<IcBaseinfoV3Result> getResult() {
      return result;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }



}

