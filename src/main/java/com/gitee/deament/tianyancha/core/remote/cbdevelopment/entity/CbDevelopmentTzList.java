package com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业发展
* 属于CbDevelopment
* /services/open/cb/development/2.0
*@author deament
**/

public class CbDevelopmentTzList implements Serializable{

    /**
     *    产品名
    **/
    @JSONField(name="product")
    private String product;
    /**
     *    轮次
    **/
    @JSONField(name="lunci")
    private String lunci;
    /**
     *    投资公司
    **/
    @JSONField(name="rongzi_map")
    private String rongzi_map;
    /**
     *    logo
    **/
    @JSONField(name="icon")
    private String icon;
    /**
     *    投资公司
    **/
    @JSONField(name="organization_name")
    private String organization_name;
    /**
     *    投资时间
    **/
    @JSONField(name="tzdate")
    private String tzdate;
    /**
     *    公司id
    **/
    @JSONField(name="graph_id")
    private String graph_id;
    /**
     *    业务范围
    **/
    @JSONField(name="yewu")
    private String yewu;
    /**
     *    金额
    **/
    @JSONField(name="money")
    private String money;
    /**
     *    地区
    **/
    @JSONField(name="location")
    private String location;
    /**
     *    公司名
    **/
    @JSONField(name="company")
    private String company;
    /**
     *    id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    oss路径
    **/
    @JSONField(name="iconOssPath")
    private String iconOssPath;
    /**
     *    行业
    **/
    @JSONField(name="hangye1")
    private String hangye1;


    /**
    *   设置 产品名
    **/
    public void setProduct(String product) {
      this.product = product;
    }
    /**
    *   获取 产品名
    **/
    public String getProduct() {
      return product;
    }
    /**
    *   设置 轮次
    **/
    public void setLunci(String lunci) {
      this.lunci = lunci;
    }
    /**
    *   获取 轮次
    **/
    public String getLunci() {
      return lunci;
    }
    /**
    *   设置 投资公司
    **/
    public void setRongzi_map(String rongzi_map) {
      this.rongzi_map = rongzi_map;
    }
    /**
    *   获取 投资公司
    **/
    public String getRongzi_map() {
      return rongzi_map;
    }
    /**
    *   设置 logo
    **/
    public void setIcon(String icon) {
      this.icon = icon;
    }
    /**
    *   获取 logo
    **/
    public String getIcon() {
      return icon;
    }
    /**
    *   设置 投资公司
    **/
    public void setOrganization_name(String organization_name) {
      this.organization_name = organization_name;
    }
    /**
    *   获取 投资公司
    **/
    public String getOrganization_name() {
      return organization_name;
    }
    /**
    *   设置 投资时间
    **/
    public void setTzdate(String tzdate) {
      this.tzdate = tzdate;
    }
    /**
    *   获取 投资时间
    **/
    public String getTzdate() {
      return tzdate;
    }
    /**
    *   设置 公司id
    **/
    public void setGraph_id(String graph_id) {
      this.graph_id = graph_id;
    }
    /**
    *   获取 公司id
    **/
    public String getGraph_id() {
      return graph_id;
    }
    /**
    *   设置 业务范围
    **/
    public void setYewu(String yewu) {
      this.yewu = yewu;
    }
    /**
    *   获取 业务范围
    **/
    public String getYewu() {
      return yewu;
    }
    /**
    *   设置 金额
    **/
    public void setMoney(String money) {
      this.money = money;
    }
    /**
    *   获取 金额
    **/
    public String getMoney() {
      return money;
    }
    /**
    *   设置 地区
    **/
    public void setLocation(String location) {
      this.location = location;
    }
    /**
    *   获取 地区
    **/
    public String getLocation() {
      return location;
    }
    /**
    *   设置 公司名
    **/
    public void setCompany(String company) {
      this.company = company;
    }
    /**
    *   获取 公司名
    **/
    public String getCompany() {
      return company;
    }
    /**
    *   设置 id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 oss路径
    **/
    public void setIconOssPath(String iconOssPath) {
      this.iconOssPath = iconOssPath;
    }
    /**
    *   获取 oss路径
    **/
    public String getIconOssPath() {
      return iconOssPath;
    }
    /**
    *   设置 行业
    **/
    public void setHangye1(String hangye1) {
      this.hangye1 = hangye1;
    }
    /**
    *   获取 行业
    **/
    public String getHangye1() {
      return hangye1;
    }



}

