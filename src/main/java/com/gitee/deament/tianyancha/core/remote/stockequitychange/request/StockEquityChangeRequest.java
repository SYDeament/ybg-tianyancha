package com.gitee.deament.tianyancha.core.remote.stockequitychange.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockequitychange.entity.StockEquityChange;
import com.gitee.deament.tianyancha.core.remote.stockequitychange.dto.StockEquityChangeDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*股本变动
* 可以通过公司名称或ID获取上市公司股本变动信息，股本变动信息包括变动时间、变动原因、变动后A股总股本、变动后流通A股、变动后限售A股等
* /services/open/stock/equityChange/2.0
*@author deament
**/

public interface StockEquityChangeRequest extends BaseRequest<StockEquityChangeDTO ,StockEquityChange>{

}

