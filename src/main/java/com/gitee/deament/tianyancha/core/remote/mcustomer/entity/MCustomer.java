package com.gitee.deament.tianyancha.core.remote.mcustomer.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcustomer.entity.MCustomerClientsYear;
import com.gitee.deament.tianyancha.core.remote.mcustomer.entity.MCustomerResult;
import com.gitee.deament.tianyancha.core.remote.mcustomer.entity.MCustomerPageBean;
/**
*客户
* 可以通过公司名称或ID获取企业相关客户信息，企业相关客户信息包括客户、销售占比、销售金额、报告期等字段的详细信息
* /services/open/m/customer/2.0
*@author deament
**/

public class MCustomer implements Serializable{

    /**
     *    
    **/
    @JSONField(name="clientsYear")
    private List<MCustomerClientsYear> clientsYear;
    /**
     *    
    **/
    @JSONField(name="pageBean")
    private MCustomerPageBean pageBean;


    /**
    *   设置 
    **/
    public void setClientsYear(List<MCustomerClientsYear> clientsYear) {
      this.clientsYear = clientsYear;
    }
    /**
    *   获取 
    **/
    public List<MCustomerClientsYear> getClientsYear() {
      return clientsYear;
    }
    /**
    *   设置 
    **/
    public void setPageBean(MCustomerPageBean pageBean) {
      this.pageBean = pageBean;
    }
    /**
    *   获取 
    **/
    public MCustomerPageBean getPageBean() {
      return pageBean;
    }



}

