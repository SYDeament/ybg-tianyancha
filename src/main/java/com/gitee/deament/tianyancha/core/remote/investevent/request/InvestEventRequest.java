package com.gitee.deament.tianyancha.core.remote.investevent.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.investevent.entity.InvestEvent;
import com.gitee.deament.tianyancha.core.remote.investevent.dto.InvestEventDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*投资动态
* 可以通过投资机构名称获取投资产品名称、产品logo、投资金额、融资轮次、相关新闻等字段的详细信息
* /services/v4/open/investEvent
*@author deament
**/

public interface InvestEventRequest extends BaseRequest<InvestEventDTO ,InvestEvent>{

}

