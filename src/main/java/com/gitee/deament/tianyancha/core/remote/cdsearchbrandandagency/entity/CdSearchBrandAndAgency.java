package com.gitee.deament.tianyancha.core.remote.cdsearchbrandandagency.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdsearchbrandandagency.entity.CdSearchBrandAndAgencyItems;
/**
*搜索项目品牌/投资机构
* 可以通过关键词获取项目品牌和投资机构信息，包括项目品牌和投资机构名称、logo、融资轮次或投资事件等字段的详细信息
* /services/open/cd/searchBrandAndAgency/2.0
*@author deament
**/

public class CdSearchBrandAndAgency implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CdSearchBrandAndAgencyItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CdSearchBrandAndAgencyItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CdSearchBrandAndAgencyItems> getItems() {
      return items;
    }



}

