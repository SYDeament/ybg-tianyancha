package com.gitee.deament.tianyancha.core.remote.iprpublicwechat.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业微信公众号
* 属于IprPublicWeChat
* /services/open/ipr/publicWeChat/2.0
*@author deament
**/

public class IprPublicWeChatItems implements Serializable{

    /**
     *    微信号
    **/
    @JSONField(name="publicNum")
    private String publicNum;
    /**
     *    二维码
    **/
    @JSONField(name="codeImg")
    private String codeImg;
    /**
     *    功能介绍
    **/
    @JSONField(name="recommend")
    private String recommend;
    /**
     *    微信号名称
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    logo
    **/
    @JSONField(name="titleImgURL")
    private String titleImgURL;


    /**
    *   设置 微信号
    **/
    public void setPublicNum(String publicNum) {
      this.publicNum = publicNum;
    }
    /**
    *   获取 微信号
    **/
    public String getPublicNum() {
      return publicNum;
    }
    /**
    *   设置 二维码
    **/
    public void setCodeImg(String codeImg) {
      this.codeImg = codeImg;
    }
    /**
    *   获取 二维码
    **/
    public String getCodeImg() {
      return codeImg;
    }
    /**
    *   设置 功能介绍
    **/
    public void setRecommend(String recommend) {
      this.recommend = recommend;
    }
    /**
    *   获取 功能介绍
    **/
    public String getRecommend() {
      return recommend;
    }
    /**
    *   设置 微信号名称
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 微信号名称
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 logo
    **/
    public void setTitleImgURL(String titleImgURL) {
      this.titleImgURL = titleImgURL;
    }
    /**
    *   获取 logo
    **/
    public String getTitleImgURL() {
      return titleImgURL;
    }



}

