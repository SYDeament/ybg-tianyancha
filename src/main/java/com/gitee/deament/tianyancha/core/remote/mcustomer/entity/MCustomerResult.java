package com.gitee.deament.tianyancha.core.remote.mcustomer.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*客户
* 属于MCustomer
* /services/open/m/customer/2.0
*@author deament
**/

public class MCustomerResult implements Serializable{

    /**
     *    报告期
    **/
    @JSONField(name="announcement_date")
    private Integer announcement_date;
    /**
     *    销售金额（万元）
    **/
    @JSONField(name="amt")
    private String amt;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    客户id
    **/
    @JSONField(name="client_graphId")
    private Integer client_graphId;
    /**
     *    关联关系
    **/
    @JSONField(name="relationship")
    private String relationship;
    /**
     *    客户名
    **/
    @JSONField(name="client_name")
    private String client_name;
    /**
     *    数据来源
    **/
    @JSONField(name="dataSource")
    private String dataSource;
    /**
     *    销售占比
    **/
    @JSONField(name="ratio")
    private String ratio;


    /**
    *   设置 报告期
    **/
    public void setAnnouncement_date(Integer announcement_date) {
      this.announcement_date = announcement_date;
    }
    /**
    *   获取 报告期
    **/
    public Integer getAnnouncement_date() {
      return announcement_date;
    }
    /**
    *   设置 销售金额（万元）
    **/
    public void setAmt(String amt) {
      this.amt = amt;
    }
    /**
    *   获取 销售金额（万元）
    **/
    public String getAmt() {
      return amt;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 客户id
    **/
    public void setClient_graphId(Integer client_graphId) {
      this.client_graphId = client_graphId;
    }
    /**
    *   获取 客户id
    **/
    public Integer getClient_graphId() {
      return client_graphId;
    }
    /**
    *   设置 关联关系
    **/
    public void setRelationship(String relationship) {
      this.relationship = relationship;
    }
    /**
    *   获取 关联关系
    **/
    public String getRelationship() {
      return relationship;
    }
    /**
    *   设置 客户名
    **/
    public void setClient_name(String client_name) {
      this.client_name = client_name;
    }
    /**
    *   获取 客户名
    **/
    public String getClient_name() {
      return client_name;
    }
    /**
    *   设置 数据来源
    **/
    public void setDataSource(String dataSource) {
      this.dataSource = dataSource;
    }
    /**
    *   获取 数据来源
    **/
    public String getDataSource() {
      return dataSource;
    }
    /**
    *   设置 销售占比
    **/
    public void setRatio(String ratio) {
      this.ratio = ratio;
    }
    /**
    *   获取 销售占比
    **/
    public String getRatio() {
      return ratio;
    }



}

