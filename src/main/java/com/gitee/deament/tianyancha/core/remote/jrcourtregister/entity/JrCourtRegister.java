package com.gitee.deament.tianyancha.core.remote.jrcourtregister.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrcourtregister.entity.JrCourtRegisterItems;
/**
*立案信息
* 可以通过公司名称或ID获取立案信息，立案信息包括案件编号、案由、立案时间、案件原被告双方信息等字段的详细信息
* /services/open/jr/courtRegister/2.0
*@author deament
**/

public class JrCourtRegister implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrCourtRegisterItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrCourtRegisterItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrCourtRegisterItems> getItems() {
      return items;
    }



}

