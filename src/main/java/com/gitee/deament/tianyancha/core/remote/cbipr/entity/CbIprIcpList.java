package com.gitee.deament.tianyancha.core.remote.cbipr.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*知识产权
* 属于CbIpr
* /services/open/cb/ipr/2.0
*@author deament
**/

public class CbIprIcpList implements Serializable{

    /**
     *    网址
    **/
    @JSONField(name="webSite")
    private List<String> webSite;
    /**
     *    域名
    **/
    @JSONField(name="ym")
    private String ym;
    /**
     *    公司类型
    **/
    @JSONField(name="companyType")
    private String companyType;
    /**
     *    许可证
    **/
    @JSONField(name="liscense")
    private String liscense;
    /**
     *    检查时间
    **/
    @JSONField(name="examineDate")
    private String examineDate;
    /**
     *    网站名称
    **/
    @JSONField(name="webName")
    private String webName;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;


    /**
    *   设置 网址
    **/
    public void setWebSite(List<String> webSite) {
      this.webSite = webSite;
    }
    /**
    *   获取 网址
    **/
    public List<String> getWebSite() {
      return webSite;
    }
    /**
    *   设置 域名
    **/
    public void setYm(String ym) {
      this.ym = ym;
    }
    /**
    *   获取 域名
    **/
    public String getYm() {
      return ym;
    }
    /**
    *   设置 公司类型
    **/
    public void setCompanyType(String companyType) {
      this.companyType = companyType;
    }
    /**
    *   获取 公司类型
    **/
    public String getCompanyType() {
      return companyType;
    }
    /**
    *   设置 许可证
    **/
    public void setLiscense(String liscense) {
      this.liscense = liscense;
    }
    /**
    *   获取 许可证
    **/
    public String getLiscense() {
      return liscense;
    }
    /**
    *   设置 检查时间
    **/
    public void setExamineDate(String examineDate) {
      this.examineDate = examineDate;
    }
    /**
    *   获取 检查时间
    **/
    public String getExamineDate() {
      return examineDate;
    }
    /**
    *   设置 网站名称
    **/
    public void setWebName(String webName) {
      this.webName = webName;
    }
    /**
    *   获取 网站名称
    **/
    public String getWebName() {
      return webName;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }



}

