package com.gitee.deament.tianyancha.core.remote.bqqualification.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.bqqualification.entity.BqQualificationQualification;
import com.gitee.deament.tianyancha.core.remote.bqqualification.entity.BqQualificationCertificate;
/**
*建筑资质-资质资格详情
* 可以通过建筑资质资格ID获取建筑资质资格详情，包括发证日期、证书有效期、资质类别、资质证书号  、资质名称、发证机关、证书信息等字段信息
* /services/open/bq/qualification/detail/2.0
*@author deament
**/

public class BqQualification implements Serializable{

    /**
     *    资质资格
    **/
    @JSONField(name="qualification")
    private BqQualificationQualification qualification;
    /**
     *    证书信息
    **/
    @JSONField(name="certificate")
    private BqQualificationCertificate certificate;


    /**
    *   设置 资质资格
    **/
    public void setQualification(BqQualificationQualification qualification) {
      this.qualification = qualification;
    }
    /**
    *   获取 资质资格
    **/
    public BqQualificationQualification getQualification() {
      return qualification;
    }
    /**
    *   设置 证书信息
    **/
    public void setCertificate(BqQualificationCertificate certificate) {
      this.certificate = certificate;
    }
    /**
    *   获取 证书信息
    **/
    public BqQualificationCertificate getCertificate() {
      return certificate;
    }



}

