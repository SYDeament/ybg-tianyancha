package com.gitee.deament.tianyancha.core.remote.hiic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcLocationList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcCreditCodeList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcBusinessScopeList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcTypeList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcOrgNumberList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcDeadLineList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcRegNumberList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcHistoryNameList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcRegCapitalList;
import com.gitee.deament.tianyancha.core.remote.hiic.entity.HiIcPastStafferList;
/**
*历史工商信息
* 历史工商信息
* /services/open/hi/ic/2.0
*@author deament
**/

public class HiIc implements Serializable{

    /**
     *    历史注册地址
    **/
    @JSONField(name="locationList")
    private List<HiIcLocationList> locationList;
    /**
     *    历史统一信用代码
    **/
    @JSONField(name="creditCodeList")
    private List<HiIcCreditCodeList> creditCodeList;
    /**
     *    历史经营范围
    **/
    @JSONField(name="businessScopeList")
    private List<HiIcBusinessScopeList> businessScopeList;
    /**
     *    历史企业类型
    **/
    @JSONField(name="typeList")
    private List<HiIcTypeList> typeList;
    /**
     *    历史组织机构代码
    **/
    @JSONField(name="orgNumberList")
    private List<HiIcOrgNumberList> orgNumberList;
    /**
     *    历史营业期限
    **/
    @JSONField(name="deadLineList")
    private List<HiIcDeadLineList> deadLineList;
    /**
     *    历史注册号
    **/
    @JSONField(name="regNumberList")
    private List<HiIcRegNumberList> regNumberList;
    /**
     *    历史曾用名
    **/
    @JSONField(name="historyNameList")
    private List<HiIcHistoryNameList> historyNameList;
    /**
     *    历史注册资本
    **/
    @JSONField(name="regCapitalList")
    private List<HiIcRegCapitalList> regCapitalList;
    /**
     *    历史主要人员
    **/
    @JSONField(name="pastStafferList")
    private List<List<HiIcPastStafferList>> pastStafferList;


    /**
    *   设置 历史注册地址
    **/
    public void setLocationList(List<HiIcLocationList> locationList) {
      this.locationList = locationList;
    }
    /**
    *   获取 历史注册地址
    **/
    public List<HiIcLocationList> getLocationList() {
      return locationList;
    }
    /**
    *   设置 历史统一信用代码
    **/
    public void setCreditCodeList(List<HiIcCreditCodeList> creditCodeList) {
      this.creditCodeList = creditCodeList;
    }
    /**
    *   获取 历史统一信用代码
    **/
    public List<HiIcCreditCodeList> getCreditCodeList() {
      return creditCodeList;
    }
    /**
    *   设置 历史经营范围
    **/
    public void setBusinessScopeList(List<HiIcBusinessScopeList> businessScopeList) {
      this.businessScopeList = businessScopeList;
    }
    /**
    *   获取 历史经营范围
    **/
    public List<HiIcBusinessScopeList> getBusinessScopeList() {
      return businessScopeList;
    }
    /**
    *   设置 历史企业类型
    **/
    public void setTypeList(List<HiIcTypeList> typeList) {
      this.typeList = typeList;
    }
    /**
    *   获取 历史企业类型
    **/
    public List<HiIcTypeList> getTypeList() {
      return typeList;
    }
    /**
    *   设置 历史组织机构代码
    **/
    public void setOrgNumberList(List<HiIcOrgNumberList> orgNumberList) {
      this.orgNumberList = orgNumberList;
    }
    /**
    *   获取 历史组织机构代码
    **/
    public List<HiIcOrgNumberList> getOrgNumberList() {
      return orgNumberList;
    }
    /**
    *   设置 历史营业期限
    **/
    public void setDeadLineList(List<HiIcDeadLineList> deadLineList) {
      this.deadLineList = deadLineList;
    }
    /**
    *   获取 历史营业期限
    **/
    public List<HiIcDeadLineList> getDeadLineList() {
      return deadLineList;
    }
    /**
    *   设置 历史注册号
    **/
    public void setRegNumberList(List<HiIcRegNumberList> regNumberList) {
      this.regNumberList = regNumberList;
    }
    /**
    *   获取 历史注册号
    **/
    public List<HiIcRegNumberList> getRegNumberList() {
      return regNumberList;
    }
    /**
    *   设置 历史曾用名
    **/
    public void setHistoryNameList(List<HiIcHistoryNameList> historyNameList) {
      this.historyNameList = historyNameList;
    }
    /**
    *   获取 历史曾用名
    **/
    public List<HiIcHistoryNameList> getHistoryNameList() {
      return historyNameList;
    }
    /**
    *   设置 历史注册资本
    **/
    public void setRegCapitalList(List<HiIcRegCapitalList> regCapitalList) {
      this.regCapitalList = regCapitalList;
    }
    /**
    *   获取 历史注册资本
    **/
    public List<HiIcRegCapitalList> getRegCapitalList() {
      return regCapitalList;
    }
    /**
    *   设置 历史主要人员
    **/
    public void setPastStafferList(List<List<HiIcPastStafferList>> pastStafferList) {
      this.pastStafferList = pastStafferList;
    }
    /**
    *   获取 历史主要人员
    **/
    public List<List<HiIcPastStafferList>> getPastStafferList() {
      return pastStafferList;
    }



}

