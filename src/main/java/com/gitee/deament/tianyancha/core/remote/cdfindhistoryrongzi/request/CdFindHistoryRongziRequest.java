package com.gitee.deament.tianyancha.core.remote.cdfindhistoryrongzi.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdfindhistoryrongzi.entity.CdFindHistoryRongzi;
import com.gitee.deament.tianyancha.core.remote.cdfindhistoryrongzi.dto.CdFindHistoryRongziDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*融资历史
* 可以通过公司名称或ID获取企业融资历史信息，企业融资历史信息包括融资轮次、交易金额、估值、投资方等字段的详细信息
* /services/open/cd/findHistoryRongzi/2.0
*@author deament
**/

public interface CdFindHistoryRongziRequest extends BaseRequest<CdFindHistoryRongziDTO ,CdFindHistoryRongzi>{

}

