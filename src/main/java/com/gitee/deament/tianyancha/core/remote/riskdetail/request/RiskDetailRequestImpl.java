package com.gitee.deament.tianyancha.core.remote.riskdetail.request;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gitee.deament.tianyancha.core.TianyanchaResult;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import com.gitee.deament.tianyancha.core.base.HttpRequestI;
import com.gitee.deament.tianyancha.core.remote.riskdetail.dto.RiskDetailDTO;
import com.gitee.deament.tianyancha.core.remote.riskdetail.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 天眼风险详情
 * 可以通过企业风险ID和人风险ID获取企业或人员相关天眼风险详情，包括企业和人员自身/周边/预警风险信息详情
 * /services/v4/open/riskDetail
 *
 * @author deament
 **/
@Component("riskDetailRequestImpl")
public class RiskDetailRequestImpl extends BaseRequestImpl<RiskDetail, RiskDetailDTO> {
    @Override
    public String getUrl() {
        return "http://open.api.tianyancha.com" + "/services/v4/open/riskDetail";
    }

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private HttpRequestI httpRequest;


    @Override
    public TianyanchaResult<RiskDetail> get(RiskDetailDTO dto) throws Exception {
        String url = getUrl();
        String result = httpRequest.get(url, BeanUtil.beanToMap(dto));
        logger.info("得到的结果：" + result);
        TianyanchaResult<RiskDetail> vo = JSONObject.parseObject(result, TianyanchaResult.class);
        if (vo != null && vo.getResult() != null) {
            Integer type = vo.getResult().getType();
            JSONArray dataList = vo.getResult().getDataList();
            switch (type) {
                case 1:
                    vo.getResult().setDataListtype1(dataList.toJavaList(RiskDetailDataListtype1.class));
                    break;
                case 3:
                    vo.getResult().setDataListtype3(dataList.toJavaList(RiskDetailDataListtype3.class));
                    break;
                case 5:
                    vo.getResult().setDataListtype5(dataList.toJavaList(RiskDetailDataListtype5.class));
                    break;
                case 6:
                    vo.getResult().setDataListtype6(dataList.toJavaList(RiskDetailDataListtype6.class));
                    break;
                case 7:
                    vo.getResult().setDataListtype7(dataList.toJavaList(RiskDetailDataListtype7.class));
                    break;
                case 8:
                    vo.getResult().setDataListtype8(dataList.toJavaList(RiskDetailDataListtype8.class));
                    break;
                case 9:
                    vo.getResult().setDataListtype9(dataList.toJavaList(RiskDetailDataListtype9.class));
                    break;
                case 10:
                    vo.getResult().setDataListtype10(dataList.toJavaList(RiskDetailDataListtype10.class));
                    break;
                case 11:
                    vo.getResult().setDataListtype11(dataList.toJavaList(RiskDetailDataListtype11.class));
                    break;
                case 12:
                    vo.getResult().setDataListtype12(dataList.toJavaList(RiskDetailDataListtype12.class));
                    break;
                case 13:
                    vo.getResult().setDataListtype13(dataList.toJavaList(RiskDetailDataListtype13.class));
                    break;
                case 14:
                    vo.getResult().setDataListtype14(dataList.toJavaList(RiskDetailDataListtype14.class));
                    break;
                case 15:
                    vo.getResult().setDataListtype15(dataList.toJavaList(RiskDetailDataListtype15.class));
                    break;
                case 16:
                    vo.getResult().setDataListtype16(dataList.toJavaList(RiskDetailDataListtype16.class));
                    break;
                case 17:
                    vo.getResult().setDataListtype17(dataList.toJavaList(RiskDetailDataListtype17.class));
                    break;
                case 18:
                    vo.getResult().setDataListtype18(dataList.toJavaList(RiskDetailDataListtype18.class));
                    break;
                case 19:
                    vo.getResult().setDataListtype19(dataList.toJavaList(RiskDetailDataListtype19.class));
                    break;
                case 20:
                    vo.getResult().setDataListtype20(dataList.toJavaList(RiskDetailDataListtype20.class));
                    break;
                case 21:
                    vo.getResult().setDataListtype21(dataList.toJavaList(RiskDetailDataListtype21.class));
                    break;
                case 22:
                    vo.getResult().setDataListtype22(dataList.toJavaList(RiskDetailDataListtype22.class));
                    break;
                case 23:
                    vo.getResult().setDataListtype23(dataList.toJavaList(RiskDetailDataListtype23.class));
                    break;
                case 24:
                    vo.getResult().setDataListtype24(dataList.toJavaList(RiskDetailDataListtype24.class));
                    break;
                case 25:
                    vo.getResult().setDataListtype25(dataList.toJavaList(RiskDetailDataListtype25.class));
                    break;
                case 26:
                    vo.getResult().setDataListtype26(dataList.toJavaList(RiskDetailDataListtype26.class));
                    break;
                case 27:
                    vo.getResult().setDataListtype27(dataList.toJavaList(RiskDetailDataListtype27.class));
                    break;
                case 28:
                    vo.getResult().setDataListtype28(dataList.toJavaList(RiskDetailDataListtype28.class));
                    break;
                case 29:
                    vo.getResult().setDataListtype29(dataList.toJavaList(RiskDetailDataListtype29.class));
                    break;
                case 30:
                    vo.getResult().setDataListtype30(dataList.toJavaList(RiskDetailDataListtype30.class));
                    break;
                case 31:
                    vo.getResult().setDataListtype31(dataList.toJavaList(RiskDetailDataListtype31.class));
                    break;


            }
        }
        logger.info("转成对于的实体类:" + JSONObject.toJSONString(vo));
        return vo;
    }
}

