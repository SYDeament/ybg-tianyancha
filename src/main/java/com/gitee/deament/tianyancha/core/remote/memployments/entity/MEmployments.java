package com.gitee.deament.tianyancha.core.remote.memployments.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.memployments.entity.MEmploymentsItems;
/**
*企业招聘
* 可以通过公司名称或ID获取企业招聘相关信息，企业招聘相关信息包括发布日期、招聘职位、月薪、学历、地区等字段的详细信息
* /services/open/m/employments/2.0
*@author deament
**/

public class MEmployments implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MEmploymentsItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MEmploymentsItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MEmploymentsItems> getItems() {
      return items;
    }



}

