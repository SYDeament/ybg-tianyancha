package com.gitee.deament.tianyancha.core.remote.icholder.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icholder.entity.IcHolderCapital;
import com.gitee.deament.tianyancha.core.remote.icholder.entity.IcHolderCapitalActl;
import com.gitee.deament.tianyancha.core.remote.icholder.entity.IcHolderItems;
/**
*企业股东
* 可以通过公司名称或ID获取企业股东信息，股东信息包括股东名、出资比例、出资金额、股东总数等字段的详细信息
* /services/open/ic/holder/2.0
*@author deament
**/

public class IcHolder implements Serializable{

    /**
     *    股东总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcHolderItems> items;


    /**
    *   设置 股东总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 股东总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcHolderItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcHolderItems> getItems() {
      return items;
    }



}

