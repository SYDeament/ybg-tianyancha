package com.gitee.deament.tianyancha.core.remote.humancompanyholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.humancompanyholding.entity.HumanCompanyholdingChainList;
import com.gitee.deament.tianyancha.core.remote.humancompanyholding.entity.HumanCompanyholdingItems;
/**
*人员控股企业
* 可以通过公司名称、人名获取人员控股企业信息，人员控股企业信息包括控股企业ID、投资比例、企业logo、企业简称、控股企业、注册资本、法人类型、经营状态、法人等字段的信息
* /services/open/human/companyholding/2.0
*@author deament
**/

public class HumanCompanyholding implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HumanCompanyholdingItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HumanCompanyholdingItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HumanCompanyholdingItems> getItems() {
      return items;
    }



}

