package com.gitee.deament.tianyancha.core.remote.mdoublerandomcheck.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*双随机抽查
* 属于MDoubleRandomCheck
* /services/open/m/doubleRandomCheck/2.0
*@author deament
**/

public class MDoubleRandomCheckTaskList implements Serializable{

    /**
     *    抽查类型
    **/
    @JSONField(name="checkType")
    private String checkType;
    /**
     *    抽查计划编号
    **/
    @JSONField(name="checkPlanNum")
    private String checkPlanNum;
    /**
     *    抽查机关
    **/
    @JSONField(name="checkDepartment")
    private String checkDepartment;
    /**
     *    抽查计划名称
    **/
    @JSONField(name="checkPlanName")
    private String checkPlanName;
    /**
     *    详情id
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    抽查任务编号
    **/
    @JSONField(name="checkTaskNum")
    private String checkTaskNum;
    /**
     *    抽查完成日期
    **/
    @JSONField(name="checkDate")
    private Long checkDate;
    /**
     *    抽查任务名称
    **/
    @JSONField(name="checkTaskName")
    private String checkTaskName;


    /**
    *   设置 抽查类型
    **/
    public void setCheckType(String checkType) {
      this.checkType = checkType;
    }
    /**
    *   获取 抽查类型
    **/
    public String getCheckType() {
      return checkType;
    }
    /**
    *   设置 抽查计划编号
    **/
    public void setCheckPlanNum(String checkPlanNum) {
      this.checkPlanNum = checkPlanNum;
    }
    /**
    *   获取 抽查计划编号
    **/
    public String getCheckPlanNum() {
      return checkPlanNum;
    }
    /**
    *   设置 抽查机关
    **/
    public void setCheckDepartment(String checkDepartment) {
      this.checkDepartment = checkDepartment;
    }
    /**
    *   获取 抽查机关
    **/
    public String getCheckDepartment() {
      return checkDepartment;
    }
    /**
    *   设置 抽查计划名称
    **/
    public void setCheckPlanName(String checkPlanName) {
      this.checkPlanName = checkPlanName;
    }
    /**
    *   获取 抽查计划名称
    **/
    public String getCheckPlanName() {
      return checkPlanName;
    }
    /**
    *   设置 详情id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 详情id
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 抽查任务编号
    **/
    public void setCheckTaskNum(String checkTaskNum) {
      this.checkTaskNum = checkTaskNum;
    }
    /**
    *   获取 抽查任务编号
    **/
    public String getCheckTaskNum() {
      return checkTaskNum;
    }
    /**
    *   设置 抽查完成日期
    **/
    public void setCheckDate(Long checkDate) {
      this.checkDate = checkDate;
    }
    /**
    *   获取 抽查完成日期
    **/
    public Long getCheckDate() {
      return checkDate;
    }
    /**
    *   设置 抽查任务名称
    **/
    public void setCheckTaskName(String checkTaskName) {
      this.checkTaskName = checkTaskName;
    }
    /**
    *   获取 抽查任务名称
    **/
    public String getCheckTaskName() {
      return checkTaskName;
    }



}

