package com.gitee.deament.tianyancha.core.remote.cbipr.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*知识产权
* 属于CbIpr
* /services/open/cb/ipr/2.0
*@author deament
**/

public class CbIprCopyRegWorksList implements Serializable{

    /**
     *    登记日期
    **/
    @JSONField(name="regtime")
    private String regtime;
    /**
     *    创作完成日期
    **/
    @JSONField(name="finishTime")
    private String finishTime;
    /**
     *    发布日期
    **/
    @JSONField(name="publishtime")
    private String publishtime;
    /**
     *    著作权人姓名/名称
    **/
    @JSONField(name="authorNationality")
    private String authorNationality;
    /**
     *    登记号
    **/
    @JSONField(name="regnum")
    private String regnum;
    /**
     *    作品名称
    **/
    @JSONField(name="fullname")
    private String fullname;
    /**
     *    作品类别
    **/
    @JSONField(name="type")
    private String type;


    /**
    *   设置 登记日期
    **/
    public void setRegtime(String regtime) {
      this.regtime = regtime;
    }
    /**
    *   获取 登记日期
    **/
    public String getRegtime() {
      return regtime;
    }
    /**
    *   设置 创作完成日期
    **/
    public void setFinishTime(String finishTime) {
      this.finishTime = finishTime;
    }
    /**
    *   获取 创作完成日期
    **/
    public String getFinishTime() {
      return finishTime;
    }
    /**
    *   设置 发布日期
    **/
    public void setPublishtime(String publishtime) {
      this.publishtime = publishtime;
    }
    /**
    *   获取 发布日期
    **/
    public String getPublishtime() {
      return publishtime;
    }
    /**
    *   设置 著作权人姓名/名称
    **/
    public void setAuthorNationality(String authorNationality) {
      this.authorNationality = authorNationality;
    }
    /**
    *   获取 著作权人姓名/名称
    **/
    public String getAuthorNationality() {
      return authorNationality;
    }
    /**
    *   设置 登记号
    **/
    public void setRegnum(String regnum) {
      this.regnum = regnum;
    }
    /**
    *   获取 登记号
    **/
    public String getRegnum() {
      return regnum;
    }
    /**
    *   设置 作品名称
    **/
    public void setFullname(String fullname) {
      this.fullname = fullname;
    }
    /**
    *   获取 作品名称
    **/
    public String getFullname() {
      return fullname;
    }
    /**
    *   设置 作品类别
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 作品类别
    **/
    public String getType() {
      return type;
    }



}

