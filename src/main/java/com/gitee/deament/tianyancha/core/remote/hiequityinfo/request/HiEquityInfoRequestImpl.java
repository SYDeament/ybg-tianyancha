package com.gitee.deament.tianyancha.core.remote.hiequityinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiequityinfo.entity.HiEquityInfo;
import com.gitee.deament.tianyancha.core.remote.hiequityinfo.dto.HiEquityInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史股权出质
* 可以通过公司名称或ID获取历史出质股权标的企业信息，包括历史质权人信息、出质人信息、出质股权数额等字段信息
* /services/open/hi/equityInfo/2.0
*@author deament
**/
@Component("hiEquityInfoRequestImpl")
public class HiEquityInfoRequestImpl extends BaseRequestImpl<HiEquityInfo,HiEquityInfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/equityInfo/2.0";
    }
}

