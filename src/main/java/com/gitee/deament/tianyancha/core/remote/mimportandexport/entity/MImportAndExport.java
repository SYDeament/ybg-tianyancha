package com.gitee.deament.tianyancha.core.remote.mimportandexport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mimportandexport.entity.MImportAndExportBaseInfo;
import com.gitee.deament.tianyancha.core.remote.mimportandexport.entity.MImportAndExportSanction;
import com.gitee.deament.tianyancha.core.remote.mimportandexport.entity.MImportAndExportCreditRating;
/**
*进出口信用
* 可以通过公司名称或ID获取企业进出口信用信息，企业进出口信用信息包括海关注册编码、注册海关、经营类别等字段的详细信息
* /services/open/m/importAndExport/2.0
*@author deament
**/

public class MImportAndExport implements Serializable{

    /**
     *    
    **/
    @JSONField(name="baseInfo")
    private MImportAndExportBaseInfo baseInfo;
    /**
     *    行政处罚信息
    **/
    @JSONField(name="sanction")
    private List<MImportAndExportSanction> sanction;
    /**
     *    信用等级
    **/
    @JSONField(name="creditRating")
    private List<MImportAndExportCreditRating> creditRating;


    /**
    *   设置 
    **/
    public void setBaseInfo(MImportAndExportBaseInfo baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 
    **/
    public MImportAndExportBaseInfo getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 行政处罚信息
    **/
    public void setSanction(List<MImportAndExportSanction> sanction) {
      this.sanction = sanction;
    }
    /**
    *   获取 行政处罚信息
    **/
    public List<MImportAndExportSanction> getSanction() {
      return sanction;
    }
    /**
    *   设置 信用等级
    **/
    public void setCreditRating(List<MImportAndExportCreditRating> creditRating) {
      this.creditRating = creditRating;
    }
    /**
    *   获取 信用等级
    **/
    public List<MImportAndExportCreditRating> getCreditRating() {
      return creditRating;
    }



}

