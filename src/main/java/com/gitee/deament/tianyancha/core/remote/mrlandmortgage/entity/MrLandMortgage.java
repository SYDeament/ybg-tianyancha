package com.gitee.deament.tianyancha.core.remote.mrlandmortgage.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*土地抵押详情
* 根据土地抵押ID获取土地抵押详情，土地抵押详情包括抵押面积、土地抵押人名称、抵押土地用途、评估金额、抵押金额、抵押人等字段的详细信息
* /services/open/mr/landMortgage/detail/2.0
*@author deament
**/

public class MrLandMortgage implements Serializable{

    /**
     *    抵押面积（公顷）
    **/
    @JSONField(name="mortgageArea")
    private String mortgageArea;
    /**
     *    土地抵押人性质
    **/
    @JSONField(name="nature")
    private String nature;
    /**
     *    结束时间
    **/
    @JSONField(name="endDate")
    private String endDate;
    /**
     *    土地面积（公顷）
    **/
    @JSONField(name="land_area")
    private String land_area;
    /**
     *    土地他项权利人证号
    **/
    @JSONField(name="otherItemApplicationNameNum")
    private String otherItemApplicationNameNum;
    /**
     *    所在行政区
    **/
    @JSONField(name="landAministrativeArea")
    private String landAministrativeArea;
    /**
     *    备注
    **/
    @JSONField(name="landMark")
    private String landMark;
    /**
     *    宗地座落
    **/
    @JSONField(name="land_loc")
    private String land_loc;
    /**
     *    土地使用权证号
    **/
    @JSONField(name="useRightNum")
    private String useRightNum;
    /**
     *    土地抵押权人
    **/
    @JSONField(name="mortgageApplicationNameClean")
    private String mortgageApplicationNameClean;
    /**
     *    抵押金额（万元）
    **/
    @JSONField(name="mortgageAmount")
    private String mortgageAmount;
    /**
     *    宗地编号
    **/
    @JSONField(name="landNum")
    private String landNum;
    /**
     *    抵押土地权属性质与使用权类型
    **/
    @JSONField(name="userType")
    private String userType;
    /**
     *     抵押土地用途
    **/
    @JSONField(name="mortgageToUser")
    private String mortgageToUser;
    /**
     *    评估金额（万元）
    **/
    @JSONField(name="evaluateAmount")
    private String evaluateAmount;
    /**
     *    土地抵押人名称
    **/
    @JSONField(name="mortgagePersonClean")
    private List<String> mortgagePersonClean;
    /**
     *    开始时间
    **/
    @JSONField(name="startDate")
    private String startDate;


    /**
    *   设置 抵押面积（公顷）
    **/
    public void setMortgageArea(String mortgageArea) {
      this.mortgageArea = mortgageArea;
    }
    /**
    *   获取 抵押面积（公顷）
    **/
    public String getMortgageArea() {
      return mortgageArea;
    }
    /**
    *   设置 土地抵押人性质
    **/
    public void setNature(String nature) {
      this.nature = nature;
    }
    /**
    *   获取 土地抵押人性质
    **/
    public String getNature() {
      return nature;
    }
    /**
    *   设置 结束时间
    **/
    public void setEndDate(String endDate) {
      this.endDate = endDate;
    }
    /**
    *   获取 结束时间
    **/
    public String getEndDate() {
      return endDate;
    }
    /**
    *   设置 土地面积（公顷）
    **/
    public void setLand_area(String land_area) {
      this.land_area = land_area;
    }
    /**
    *   获取 土地面积（公顷）
    **/
    public String getLand_area() {
      return land_area;
    }
    /**
    *   设置 土地他项权利人证号
    **/
    public void setOtherItemApplicationNameNum(String otherItemApplicationNameNum) {
      this.otherItemApplicationNameNum = otherItemApplicationNameNum;
    }
    /**
    *   获取 土地他项权利人证号
    **/
    public String getOtherItemApplicationNameNum() {
      return otherItemApplicationNameNum;
    }
    /**
    *   设置 所在行政区
    **/
    public void setLandAministrativeArea(String landAministrativeArea) {
      this.landAministrativeArea = landAministrativeArea;
    }
    /**
    *   获取 所在行政区
    **/
    public String getLandAministrativeArea() {
      return landAministrativeArea;
    }
    /**
    *   设置 备注
    **/
    public void setLandMark(String landMark) {
      this.landMark = landMark;
    }
    /**
    *   获取 备注
    **/
    public String getLandMark() {
      return landMark;
    }
    /**
    *   设置 宗地座落
    **/
    public void setLand_loc(String land_loc) {
      this.land_loc = land_loc;
    }
    /**
    *   获取 宗地座落
    **/
    public String getLand_loc() {
      return land_loc;
    }
    /**
    *   设置 土地使用权证号
    **/
    public void setUseRightNum(String useRightNum) {
      this.useRightNum = useRightNum;
    }
    /**
    *   获取 土地使用权证号
    **/
    public String getUseRightNum() {
      return useRightNum;
    }
    /**
    *   设置 土地抵押权人
    **/
    public void setMortgageApplicationNameClean(String mortgageApplicationNameClean) {
      this.mortgageApplicationNameClean = mortgageApplicationNameClean;
    }
    /**
    *   获取 土地抵押权人
    **/
    public String getMortgageApplicationNameClean() {
      return mortgageApplicationNameClean;
    }
    /**
    *   设置 抵押金额（万元）
    **/
    public void setMortgageAmount(String mortgageAmount) {
      this.mortgageAmount = mortgageAmount;
    }
    /**
    *   获取 抵押金额（万元）
    **/
    public String getMortgageAmount() {
      return mortgageAmount;
    }
    /**
    *   设置 宗地编号
    **/
    public void setLandNum(String landNum) {
      this.landNum = landNum;
    }
    /**
    *   获取 宗地编号
    **/
    public String getLandNum() {
      return landNum;
    }
    /**
    *   设置 抵押土地权属性质与使用权类型
    **/
    public void setUserType(String userType) {
      this.userType = userType;
    }
    /**
    *   获取 抵押土地权属性质与使用权类型
    **/
    public String getUserType() {
      return userType;
    }
    /**
    *   设置  抵押土地用途
    **/
    public void setMortgageToUser(String mortgageToUser) {
      this.mortgageToUser = mortgageToUser;
    }
    /**
    *   获取  抵押土地用途
    **/
    public String getMortgageToUser() {
      return mortgageToUser;
    }
    /**
    *   设置 评估金额（万元）
    **/
    public void setEvaluateAmount(String evaluateAmount) {
      this.evaluateAmount = evaluateAmount;
    }
    /**
    *   获取 评估金额（万元）
    **/
    public String getEvaluateAmount() {
      return evaluateAmount;
    }
    /**
    *   设置 土地抵押人名称
    **/
    public void setMortgagePersonClean(List<String> mortgagePersonClean) {
      this.mortgagePersonClean = mortgagePersonClean;
    }
    /**
    *   获取 土地抵押人名称
    **/
    public List<String> getMortgagePersonClean() {
      return mortgagePersonClean;
    }
    /**
    *   设置 开始时间
    **/
    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 开始时间
    **/
    public String getStartDate() {
      return startDate;
    }



}

