package com.gitee.deament.tianyancha.core.remote.mgetlicensecreditchina.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*行政许可-其他来源
* 属于MGetLicenseCreditchina
* /services/open/m/getLicenseCreditchina/2.0
*@author deament
**/

public class MGetLicenseCreditchinaItems implements Serializable{

    /**
     *    内容许可
    **/
    @JSONField(name="licenceContent")
    private String licenceContent;
    /**
     *    许可有效期
    **/
    @JSONField(name="validityTime")
    private String validityTime;
    /**
     *    法人id
    **/
    @JSONField(name="legalPersonId")
    private Integer legalPersonId;
    /**
     *    许可截止日期
    **/
    @JSONField(name="endDate")
    private String endDate;
    /**
     *    行政许可决定文书号
    **/
    @JSONField(name="licenceNumber")
    private String licenceNumber;
    /**
     *    地方编码
    **/
    @JSONField(name="localCode")
    private String localCode;
    /**
     *    许可机关
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    审核类型
    **/
    @JSONField(name="audiType")
    private String audiType;
    /**
     *    数据更新时间
    **/
    @JSONField(name="dataUpdateTime")
    private String dataUpdateTime;
    /**
     *    许可决定日期
    **/
    @JSONField(name="decisionDate")
    private String decisionDate;
    /**
     *    法定代表人姓名
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 内容许可
    **/
    public void setLicenceContent(String licenceContent) {
      this.licenceContent = licenceContent;
    }
    /**
    *   获取 内容许可
    **/
    public String getLicenceContent() {
      return licenceContent;
    }
    /**
    *   设置 许可有效期
    **/
    public void setValidityTime(String validityTime) {
      this.validityTime = validityTime;
    }
    /**
    *   获取 许可有效期
    **/
    public String getValidityTime() {
      return validityTime;
    }
    /**
    *   设置 法人id
    **/
    public void setLegalPersonId(Integer legalPersonId) {
      this.legalPersonId = legalPersonId;
    }
    /**
    *   获取 法人id
    **/
    public Integer getLegalPersonId() {
      return legalPersonId;
    }
    /**
    *   设置 许可截止日期
    **/
    public void setEndDate(String endDate) {
      this.endDate = endDate;
    }
    /**
    *   获取 许可截止日期
    **/
    public String getEndDate() {
      return endDate;
    }
    /**
    *   设置 行政许可决定文书号
    **/
    public void setLicenceNumber(String licenceNumber) {
      this.licenceNumber = licenceNumber;
    }
    /**
    *   获取 行政许可决定文书号
    **/
    public String getLicenceNumber() {
      return licenceNumber;
    }
    /**
    *   设置 地方编码
    **/
    public void setLocalCode(String localCode) {
      this.localCode = localCode;
    }
    /**
    *   获取 地方编码
    **/
    public String getLocalCode() {
      return localCode;
    }
    /**
    *   设置 许可机关
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 许可机关
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 审核类型
    **/
    public void setAudiType(String audiType) {
      this.audiType = audiType;
    }
    /**
    *   获取 审核类型
    **/
    public String getAudiType() {
      return audiType;
    }
    /**
    *   设置 数据更新时间
    **/
    public void setDataUpdateTime(String dataUpdateTime) {
      this.dataUpdateTime = dataUpdateTime;
    }
    /**
    *   获取 数据更新时间
    **/
    public String getDataUpdateTime() {
      return dataUpdateTime;
    }
    /**
    *   设置 许可决定日期
    **/
    public void setDecisionDate(String decisionDate) {
      this.decisionDate = decisionDate;
    }
    /**
    *   获取 许可决定日期
    **/
    public String getDecisionDate() {
      return decisionDate;
    }
    /**
    *   设置 法定代表人姓名
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法定代表人姓名
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

