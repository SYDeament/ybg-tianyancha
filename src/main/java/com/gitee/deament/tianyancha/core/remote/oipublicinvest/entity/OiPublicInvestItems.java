package com.gitee.deament.tianyancha.core.remote.oipublicinvest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*公开投资事件
* 属于OiPublicInvest
* /services/open/oi/publicInvest/2.0
*@author deament
**/

public class OiPublicInvestItems implements Serializable{

    /**
     *    产品名称
    **/
    @JSONField(name="product")
    private String product;
    /**
     *    公司id
    **/
    @JSONField(name="companyGraphId")
    private Integer companyGraphId;
    /**
     *    投资金额
    **/
    @JSONField(name="money")
    private String money;
    /**
     *    轮次
    **/
    @JSONField(name="round")
    private String round;
    /**
     *    公司logo
    **/
    @JSONField(name="companyLogo")
    private String companyLogo;
    /**
     *    企业标签
    **/
    @JSONField(name="tag_list")
    private List<String> tag_list;
    /**
     *    产品id
    **/
    @JSONField(name="product_id")
    private String product_id;
    /**
     *    介绍
    **/
    @JSONField(name="intro")
    private String intro;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    公司名
    **/
    @JSONField(name="company")
    private String company;
    /**
     *    投资时间
    **/
    @JSONField(name="invest_date")
    private String invest_date;


    /**
    *   设置 产品名称
    **/
    public void setProduct(String product) {
      this.product = product;
    }
    /**
    *   获取 产品名称
    **/
    public String getProduct() {
      return product;
    }
    /**
    *   设置 公司id
    **/
    public void setCompanyGraphId(Integer companyGraphId) {
      this.companyGraphId = companyGraphId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCompanyGraphId() {
      return companyGraphId;
    }
    /**
    *   设置 投资金额
    **/
    public void setMoney(String money) {
      this.money = money;
    }
    /**
    *   获取 投资金额
    **/
    public String getMoney() {
      return money;
    }
    /**
    *   设置 轮次
    **/
    public void setRound(String round) {
      this.round = round;
    }
    /**
    *   获取 轮次
    **/
    public String getRound() {
      return round;
    }
    /**
    *   设置 公司logo
    **/
    public void setCompanyLogo(String companyLogo) {
      this.companyLogo = companyLogo;
    }
    /**
    *   获取 公司logo
    **/
    public String getCompanyLogo() {
      return companyLogo;
    }
    /**
    *   设置 企业标签
    **/
    public void setTag_list(List<String> tag_list) {
      this.tag_list = tag_list;
    }
    /**
    *   获取 企业标签
    **/
    public List<String> getTag_list() {
      return tag_list;
    }
    /**
    *   设置 产品id
    **/
    public void setProduct_id(String product_id) {
      this.product_id = product_id;
    }
    /**
    *   获取 产品id
    **/
    public String getProduct_id() {
      return product_id;
    }
    /**
    *   设置 介绍
    **/
    public void setIntro(String intro) {
      this.intro = intro;
    }
    /**
    *   获取 介绍
    **/
    public String getIntro() {
      return intro;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 公司名
    **/
    public void setCompany(String company) {
      this.company = company;
    }
    /**
    *   获取 公司名
    **/
    public String getCompany() {
      return company;
    }
    /**
    *   设置 投资时间
    **/
    public void setInvest_date(String invest_date) {
      this.invest_date = invest_date;
    }
    /**
    *   获取 投资时间
    **/
    public String getInvest_date() {
      return invest_date;
    }



}

