package com.gitee.deament.tianyancha.core.remote.judicial.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.judicial.entity.JudicialItems;
/**
*司法协助
* 可以通过公司名称或ID获取司法协助信息，司法协助信息包括执行法院、案件内容、被执行人名称等字段的详细信息
* /services/v4/open/judicial
*@author deament
**/

public class Judicial implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="historyCount")
    private Integer historyCount;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JudicialItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setHistoryCount(Integer historyCount) {
      this.historyCount = historyCount;
    }
    /**
    *   获取 
    **/
    public Integer getHistoryCount() {
      return historyCount;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JudicialItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JudicialItems> getItems() {
      return items;
    }



}

