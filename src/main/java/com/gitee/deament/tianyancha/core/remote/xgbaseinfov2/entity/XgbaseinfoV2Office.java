package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*特殊企业基本信息
* 属于XgbaseinfoV2
* /services/v4/open/xgbaseinfoV2
*@author deament
**/

public class XgbaseinfoV2Office implements Serializable{

    /**
     *    区域
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    弃用
    **/
    @JSONField(name="score")
    private Integer score;
    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    企业名称
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    弃用
    **/
    @JSONField(name="state")
    private String state;
    /**
     *    企业id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 区域
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 区域
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 弃用
    **/
    public void setScore(Integer score) {
      this.score = score;
    }
    /**
    *   获取 弃用
    **/
    public Integer getScore() {
      return score;
    }
    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 企业名称
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 企业名称
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 弃用
    **/
    public void setState(String state) {
      this.state = state;
    }
    /**
    *   获取 弃用
    **/
    public String getState() {
      return state;
    }
    /**
    *   设置 企业id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 企业id
    **/
    public Integer getCid() {
      return cid;
    }



}

