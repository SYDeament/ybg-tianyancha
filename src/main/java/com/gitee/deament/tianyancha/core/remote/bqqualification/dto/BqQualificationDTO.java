package com.gitee.deament.tianyancha.core.remote.bqqualification.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*建筑资质-资质资格详情
* 可以通过建筑资质资格ID获取建筑资质资格详情，包括发证日期、证书有效期、资质类别、资质证书号  、资质名称、发证机关、证书信息等字段信息
* /services/open/bq/qualification/detail/2.0
*@author deament
**/

@TYCURL(value="/services/open/bq/qualification/detail/2.0")
public class BqQualificationDTO implements Serializable{

    /**
     *    资格id
     *
    **/
    @ParamRequire(require = false)
    private String businessId;


    /**
    *   设置 资格id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 资格id
    **/
    public String getBusinessId() {
      return businessId;
    }



}

