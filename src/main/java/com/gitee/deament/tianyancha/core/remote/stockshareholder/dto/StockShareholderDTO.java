package com.gitee.deament.tianyancha.core.remote.stockshareholder.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*十大股东（十大流通股东）
* 可以通过公司名称或ID获取上市公司十大股东和十大流通股东信息，十大股东和十大流通股东信息包括机构或基金、持有数量、持股变化、占股本比例、实际增减持、股份类型等
* /services/open/stock/shareholder/2.0
*@author deament
**/

@TYCURL(value="/services/open/stock/shareholder/2.0")
public class StockShareholderDTO implements Serializable{

    /**
     *    公司名称，可通过查询接口获取
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id，可通过查询接口获取
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    时间
     *
    **/
    @ParamRequire(require = false)
    private String time;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    类型 1 十大股东 2 十大流通股东
     *
    **/
    @ParamRequire(require = true)
    private Long type;


    /**
    *   设置 公司名称，可通过查询接口获取
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称，可通过查询接口获取
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id，可通过查询接口获取
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id，可通过查询接口获取
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 时间
    **/
    public void setTime(String time) {
      this.time = time;
    }
    /**
    *   获取 时间
    **/
    public String getTime() {
      return time;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 类型 1 十大股东 2 十大流通股东
    **/
    public void setType(Long type) {
      this.type = type;
    }
    /**
    *   获取 类型 1 十大股东 2 十大流通股东
    **/
    public Long getType() {
      return type;
    }



}

