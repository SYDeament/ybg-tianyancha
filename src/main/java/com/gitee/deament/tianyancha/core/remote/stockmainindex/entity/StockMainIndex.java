package com.gitee.deament.tianyancha.core.remote.stockmainindex.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockmainindex.entity.StockMainIndex_child;
/**
*主要指标-年度
* 可以通过公司名称或ID获取上市公司年度主要指标数据，年度主要指标数据包括基本每股收益、扣非每股收益、稀释每股收益、每股净资产、每股公积金等
* /services/open/stock/mainIndex/2.0
*@author deament
**/

public class StockMainIndex implements Serializable{

    /**
     *    
    **/
    @JSONField(name="_child")
    private StockMainIndex_child _child;


    /**
    *   设置 
    **/
    public void set_child(StockMainIndex_child _child) {
      this._child = _child;
    }
    /**
    *   获取 
    **/
    public StockMainIndex_child get_child() {
      return _child;
    }



}

