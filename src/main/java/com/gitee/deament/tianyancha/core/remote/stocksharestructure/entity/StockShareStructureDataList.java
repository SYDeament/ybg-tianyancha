package com.gitee.deament.tianyancha.core.remote.stocksharestructure.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*股本结构
* 属于StockShareStructure
* /services/open/stock/shareStructure/2.0
*@author deament
**/

public class StockShareStructureDataList implements Serializable{

    /**
     *    变动原因
    **/
    @JSONField(name="changeReason")
    private String changeReason;
    /**
     *    限售A股
    **/
    @JSONField(name="limitShare")
    private String limitShare;
    /**
     *    限售H股
    **/
    @JSONField(name="hlimitShare")
    private String hlimitShare;
    /**
     *    流通A股
    **/
    @JSONField(name="noLimitShare")
    private String noLimitShare;
    /**
     *    流通H股
    **/
    @JSONField(name="hnoLimitShare")
    private String hnoLimitShare;
    /**
     *    总股本(股)
    **/
    @JSONField(name="shareAll")
    private String shareAll;
    /**
     *    A股总股本
    **/
    @JSONField(name="ashareAll")
    private String ashareAll;
    /**
     *    时间
    **/
    @JSONField(name="pubDate")
    private Long pubDate;
    /**
     *    h股总股本
    **/
    @JSONField(name="hshareAll")
    private String hshareAll;


    /**
    *   设置 变动原因
    **/
    public void setChangeReason(String changeReason) {
      this.changeReason = changeReason;
    }
    /**
    *   获取 变动原因
    **/
    public String getChangeReason() {
      return changeReason;
    }
    /**
    *   设置 限售A股
    **/
    public void setLimitShare(String limitShare) {
      this.limitShare = limitShare;
    }
    /**
    *   获取 限售A股
    **/
    public String getLimitShare() {
      return limitShare;
    }
    /**
    *   设置 限售H股
    **/
    public void setHlimitShare(String hlimitShare) {
      this.hlimitShare = hlimitShare;
    }
    /**
    *   获取 限售H股
    **/
    public String getHlimitShare() {
      return hlimitShare;
    }
    /**
    *   设置 流通A股
    **/
    public void setNoLimitShare(String noLimitShare) {
      this.noLimitShare = noLimitShare;
    }
    /**
    *   获取 流通A股
    **/
    public String getNoLimitShare() {
      return noLimitShare;
    }
    /**
    *   设置 流通H股
    **/
    public void setHnoLimitShare(String hnoLimitShare) {
      this.hnoLimitShare = hnoLimitShare;
    }
    /**
    *   获取 流通H股
    **/
    public String getHnoLimitShare() {
      return hnoLimitShare;
    }
    /**
    *   设置 总股本(股)
    **/
    public void setShareAll(String shareAll) {
      this.shareAll = shareAll;
    }
    /**
    *   获取 总股本(股)
    **/
    public String getShareAll() {
      return shareAll;
    }
    /**
    *   设置 A股总股本
    **/
    public void setAshareAll(String ashareAll) {
      this.ashareAll = ashareAll;
    }
    /**
    *   获取 A股总股本
    **/
    public String getAshareAll() {
      return ashareAll;
    }
    /**
    *   设置 时间
    **/
    public void setPubDate(Long pubDate) {
      this.pubDate = pubDate;
    }
    /**
    *   获取 时间
    **/
    public Long getPubDate() {
      return pubDate;
    }
    /**
    *   设置 h股总股本
    **/
    public void setHshareAll(String hshareAll) {
      this.hshareAll = hshareAll;
    }
    /**
    *   获取 h股总股本
    **/
    public String getHshareAll() {
      return hshareAll;
    }



}

