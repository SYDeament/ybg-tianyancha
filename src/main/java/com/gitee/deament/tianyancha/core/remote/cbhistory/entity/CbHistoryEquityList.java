package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryEquityList implements Serializable{

    /**
     *    登记编号
    **/
    @JSONField(name="regNumber")
    private String regNumber;
    /**
     *    出质人证照/证件号码
    **/
    @JSONField(name="certifNumber")
    private String certifNumber;
    /**
     *    注销日期
    **/
    @JSONField(name="cancelDate")
    private String cancelDate;
    /**
     *    出质股权数额
    **/
    @JSONField(name="equityAmount")
    private String equityAmount;
    /**
     *    股权出质设立登记日期
    **/
    @JSONField(name="regDate")
    private String regDate;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    状态
    **/
    @JSONField(name="state")
    private String state;
    /**
     *    出质人
    **/
    @JSONField(name="pledgor")
    private String pledgor;
    /**
     *    注销原因
    **/
    @JSONField(name="cancelReason")
    private String cancelReason;
    /**
     *    质权人证照/证件号码
    **/
    @JSONField(name="certifNumberR")
    private String certifNumberR;
    /**
     *    股权出质设立发布日期
    **/
    @JSONField(name="putDate")
    private String putDate;
    /**
     *    质权人
    **/
    @JSONField(name="pledgee")
    private String pledgee;


    /**
    *   设置 登记编号
    **/
    public void setRegNumber(String regNumber) {
      this.regNumber = regNumber;
    }
    /**
    *   获取 登记编号
    **/
    public String getRegNumber() {
      return regNumber;
    }
    /**
    *   设置 出质人证照/证件号码
    **/
    public void setCertifNumber(String certifNumber) {
      this.certifNumber = certifNumber;
    }
    /**
    *   获取 出质人证照/证件号码
    **/
    public String getCertifNumber() {
      return certifNumber;
    }
    /**
    *   设置 注销日期
    **/
    public void setCancelDate(String cancelDate) {
      this.cancelDate = cancelDate;
    }
    /**
    *   获取 注销日期
    **/
    public String getCancelDate() {
      return cancelDate;
    }
    /**
    *   设置 出质股权数额
    **/
    public void setEquityAmount(String equityAmount) {
      this.equityAmount = equityAmount;
    }
    /**
    *   获取 出质股权数额
    **/
    public String getEquityAmount() {
      return equityAmount;
    }
    /**
    *   设置 股权出质设立登记日期
    **/
    public void setRegDate(String regDate) {
      this.regDate = regDate;
    }
    /**
    *   获取 股权出质设立登记日期
    **/
    public String getRegDate() {
      return regDate;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 状态
    **/
    public void setState(String state) {
      this.state = state;
    }
    /**
    *   获取 状态
    **/
    public String getState() {
      return state;
    }
    /**
    *   设置 出质人
    **/
    public void setPledgor(String pledgor) {
      this.pledgor = pledgor;
    }
    /**
    *   获取 出质人
    **/
    public String getPledgor() {
      return pledgor;
    }
    /**
    *   设置 注销原因
    **/
    public void setCancelReason(String cancelReason) {
      this.cancelReason = cancelReason;
    }
    /**
    *   获取 注销原因
    **/
    public String getCancelReason() {
      return cancelReason;
    }
    /**
    *   设置 质权人证照/证件号码
    **/
    public void setCertifNumberR(String certifNumberR) {
      this.certifNumberR = certifNumberR;
    }
    /**
    *   获取 质权人证照/证件号码
    **/
    public String getCertifNumberR() {
      return certifNumberR;
    }
    /**
    *   设置 股权出质设立发布日期
    **/
    public void setPutDate(String putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 股权出质设立发布日期
    **/
    public String getPutDate() {
      return putDate;
    }
    /**
    *   设置 质权人
    **/
    public void setPledgee(String pledgee) {
      this.pledgee = pledgee;
    }
    /**
    *   获取 质权人
    **/
    public String getPledgee() {
      return pledgee;
    }



}

