package com.gitee.deament.tianyancha.core.remote.search20.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*搜索
* 可以通过关键词获取企业列表，企业列表包括公司名称或ID、类型、成立日期、经营状态、统一社会信用代码等字段的详细信息
* /services/open/search/2.0
*@author deament
**/

@TYCURL(value="/services/open/search/2.0")
public class Search20DTO implements Serializable{

    /**
     *    必填项
     *
    **/
    @ParamRequire(require = true)
    private String word;


    /**
    *   设置 必填项
    **/
    public void setWord(String word) {
      this.word = word;
    }
    /**
    *   获取 必填项
    **/
    public String getWord() {
      return word;
    }



}

