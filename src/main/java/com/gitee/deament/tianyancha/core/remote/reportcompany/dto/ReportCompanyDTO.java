package com.gitee.deament.tianyancha.core.remote.reportcompany.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*企业信用报告（专业版）
* 可以通过公司名称或ID获取企业信用报告(专业版)，专业版企业信用报告包括工商信息、分支机构、变更记录、主要人员、联系方式、控股企业、股东信息、对外投资信息、股权控制路径、风险信息、知识产权信息、经营信息、年报信息、财务信息等字段的详细信息
* /services/open/report/company/pro
*@author deament
**/

@TYCURL(value="/services/open/report/company/pro")
public class ReportCompanyDTO implements Serializable{

    /**
     *    公司名
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    32位uuid（区分大小写）
     *
    **/
    @ParamRequire(require = true)
    private String uuid;


    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 32位uuid（区分大小写）
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 32位uuid（区分大小写）
    **/
    public String getUuid() {
      return uuid;
    }



}

