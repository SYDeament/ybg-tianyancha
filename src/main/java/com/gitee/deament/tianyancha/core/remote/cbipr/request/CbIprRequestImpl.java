package com.gitee.deament.tianyancha.core.remote.cbipr.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbipr.entity.CbIpr;
import com.gitee.deament.tianyancha.core.remote.cbipr.dto.CbIprDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*知识产权
* 可以通过公司名称或ID获取包含商标、专利、作品著作权、软件著作权、网站备案等维度的相关信息
* /services/open/cb/ipr/2.0
*@author deament
**/
@Component("cbIprRequestImpl")
public class CbIprRequestImpl extends BaseRequestImpl<CbIpr,CbIprDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cb/ipr/2.0";
    }
}

