package com.gitee.deament.tianyancha.core.remote.hizhixing.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hizhixing.entity.HiZhixing;
import com.gitee.deament.tianyancha.core.remote.hizhixing.dto.HiZhixingDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史被执行人
* 可以通过公司名称或ID获取企业历史的被执行人信息，历史被执行人信息包括执行法院、案件内容、执行标的、被执行人名称、组织机构代码等字段信息
* /services/open/hi/zhixing/2.0
*@author deament
**/
@Component("hiZhixingRequestImpl")
public class HiZhixingRequestImpl extends BaseRequestImpl<HiZhixing,HiZhixingDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/zhixing/2.0";
    }
}

