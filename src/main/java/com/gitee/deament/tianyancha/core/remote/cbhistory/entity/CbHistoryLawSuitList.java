package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryLawSuitList implements Serializable{

    /**
     *    发布时间
    **/
    @JSONField(name="submittime")
    private String submittime;
    /**
     *    案由
    **/
    @JSONField(name="casereason")
    private String casereason;
    /**
     *    代理人
    **/
    @JSONField(name="agent")
    private String agent;
    /**
     *    被告
    **/
    @JSONField(name="defendants")
    private String defendants;
    /**
     *    摘要
    **/
    @JSONField(name="abstracts")
    private String abstracts;
    /**
     *    天眼查显示url
    **/
    @JSONField(name="lawsuitUrl")
    private String lawsuitUrl;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    案号
    **/
    @JSONField(name="caseno")
    private String caseno;
    /**
     *    原文链接地址
    **/
    @JSONField(name="url")
    private String url;
    /**
     *    文书类型
    **/
    @JSONField(name="doctype")
    private String doctype;
    /**
     *    第三人
    **/
    @JSONField(name="thirdParties")
    private String thirdParties;
    /**
     *    案件类型
    **/
    @JSONField(name="casetype")
    private String casetype;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    原告
    **/
    @JSONField(name="plaintiffs")
    private String plaintiffs;


    /**
    *   设置 发布时间
    **/
    public void setSubmittime(String submittime) {
      this.submittime = submittime;
    }
    /**
    *   获取 发布时间
    **/
    public String getSubmittime() {
      return submittime;
    }
    /**
    *   设置 案由
    **/
    public void setCasereason(String casereason) {
      this.casereason = casereason;
    }
    /**
    *   获取 案由
    **/
    public String getCasereason() {
      return casereason;
    }
    /**
    *   设置 代理人
    **/
    public void setAgent(String agent) {
      this.agent = agent;
    }
    /**
    *   获取 代理人
    **/
    public String getAgent() {
      return agent;
    }
    /**
    *   设置 被告
    **/
    public void setDefendants(String defendants) {
      this.defendants = defendants;
    }
    /**
    *   获取 被告
    **/
    public String getDefendants() {
      return defendants;
    }
    /**
    *   设置 摘要
    **/
    public void setAbstracts(String abstracts) {
      this.abstracts = abstracts;
    }
    /**
    *   获取 摘要
    **/
    public String getAbstracts() {
      return abstracts;
    }
    /**
    *   设置 天眼查显示url
    **/
    public void setLawsuitUrl(String lawsuitUrl) {
      this.lawsuitUrl = lawsuitUrl;
    }
    /**
    *   获取 天眼查显示url
    **/
    public String getLawsuitUrl() {
      return lawsuitUrl;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 案号
    **/
    public void setCaseno(String caseno) {
      this.caseno = caseno;
    }
    /**
    *   获取 案号
    **/
    public String getCaseno() {
      return caseno;
    }
    /**
    *   设置 原文链接地址
    **/
    public void setUrl(String url) {
      this.url = url;
    }
    /**
    *   获取 原文链接地址
    **/
    public String getUrl() {
      return url;
    }
    /**
    *   设置 文书类型
    **/
    public void setDoctype(String doctype) {
      this.doctype = doctype;
    }
    /**
    *   获取 文书类型
    **/
    public String getDoctype() {
      return doctype;
    }
    /**
    *   设置 第三人
    **/
    public void setThirdParties(String thirdParties) {
      this.thirdParties = thirdParties;
    }
    /**
    *   获取 第三人
    **/
    public String getThirdParties() {
      return thirdParties;
    }
    /**
    *   设置 案件类型
    **/
    public void setCasetype(String casetype) {
      this.casetype = casetype;
    }
    /**
    *   获取 案件类型
    **/
    public String getCasetype() {
      return casetype;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 原告
    **/
    public void setPlaintiffs(String plaintiffs) {
      this.plaintiffs = plaintiffs;
    }
    /**
    *   获取 原告
    **/
    public String getPlaintiffs() {
      return plaintiffs;
    }



}

