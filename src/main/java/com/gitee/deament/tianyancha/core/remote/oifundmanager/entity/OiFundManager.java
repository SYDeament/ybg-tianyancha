package com.gitee.deament.tianyancha.core.remote.oifundmanager.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oifundmanager.entity.OiFundManagerItems;
/**
*基金管理人
* 可以通过投资机构名称获取投资机构基金管理人信息，包括基金管理公司名称或ID、法人、注册资本、成立日期等字段的详细信息
* /services/open/oi/fundManager/2.0
*@author deament
**/

public class OiFundManager implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiFundManagerItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiFundManagerItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiFundManagerItems> getItems() {
      return items;
    }



}

