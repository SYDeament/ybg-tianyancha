package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype30 implements Serializable{

    /**
     *    宗地座落
    **/
    @JSONField(name="land_loc")
    private String land_loc;
    /**
     *    抵押面积(公顷)
    **/
    @JSONField(name="mortgageArea")
    private String mortgageArea;
    /**
     *    地抵押登记结束时间
    **/
    @JSONField(name="endDate")
    private String endDate;
    /**
     *    
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    所在行政区
    **/
    @JSONField(name="landAministrativeArea")
    private String landAministrativeArea;
    /**
     *    抵押土地用途
    **/
    @JSONField(name="mortgageToUser")
    private String mortgageToUser;
    /**
     *    土地抵押登记起始时间
    **/
    @JSONField(name="startDate")
    private String startDate;


    /**
    *   设置 宗地座落
    **/
    public void setLand_loc(String land_loc) {
      this.land_loc = land_loc;
    }
    /**
    *   获取 宗地座落
    **/
    public String getLand_loc() {
      return land_loc;
    }
    /**
    *   设置 抵押面积(公顷)
    **/
    public void setMortgageArea(String mortgageArea) {
      this.mortgageArea = mortgageArea;
    }
    /**
    *   获取 抵押面积(公顷)
    **/
    public String getMortgageArea() {
      return mortgageArea;
    }
    /**
    *   设置 地抵押登记结束时间
    **/
    public void setEndDate(String endDate) {
      this.endDate = endDate;
    }
    /**
    *   获取 地抵押登记结束时间
    **/
    public String getEndDate() {
      return endDate;
    }
    /**
    *   设置 
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 所在行政区
    **/
    public void setLandAministrativeArea(String landAministrativeArea) {
      this.landAministrativeArea = landAministrativeArea;
    }
    /**
    *   获取 所在行政区
    **/
    public String getLandAministrativeArea() {
      return landAministrativeArea;
    }
    /**
    *   设置 抵押土地用途
    **/
    public void setMortgageToUser(String mortgageToUser) {
      this.mortgageToUser = mortgageToUser;
    }
    /**
    *   获取 抵押土地用途
    **/
    public String getMortgageToUser() {
      return mortgageToUser;
    }
    /**
    *   设置 土地抵押登记起始时间
    **/
    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 土地抵押登记起始时间
    **/
    public String getStartDate() {
      return startDate;
    }



}

