package com.gitee.deament.tianyancha.core.remote.mcertificate.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcertificate.entity.MCertificate;
import com.gitee.deament.tianyancha.core.remote.mcertificate.dto.MCertificateDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*资质证书
* 可以通过公司名称或ID获取企业资质证书信息，企业资质证书信息包括证书类型、证书编号、发证日期等字段的详细信息
* /services/open/m/certificate/2.0
*@author deament
**/
@Component("mCertificateRequestImpl")
public class MCertificateRequestImpl extends BaseRequestImpl<MCertificate,MCertificateDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/m/certificate/2.0";
    }
}

