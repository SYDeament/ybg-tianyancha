package com.gitee.deament.tianyancha.core.remote.bqproject.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectResult;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectBidding;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectResult;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectDrawingReview;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectResult;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectContractFiling;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectResult;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectCompletionAcceptanceRecord;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectResult;
import com.gitee.deament.tianyancha.core.remote.bqproject.entity.BqProjectConstructionPermit;
/**
*建筑资质-工程项目详情
* 可以通过建筑资质工程项目ID获取建筑资质工程项目信息，包括项目编号、项目名称、项目属地、项目类别、建设单位、招投标、合同备案、施工许可等字段信息
* /services/open/bq/project/detail/2.0
*@author deament
**/

public class BqProject implements Serializable{

    /**
     *    项目编号
    **/
    @JSONField(name="proNum")
    private String proNum;
    /**
     *    项目分类
    **/
    @JSONField(name="proType")
    private String proType;
    /**
     *    工程用途
    **/
    @JSONField(name="proUse")
    private String proUse;
    /**
     *    招投标
    **/
    @JSONField(name="bidding")
    private BqProjectBidding bidding;
    /**
     *    立项级别
    **/
    @JSONField(name="projectLevel")
    private String projectLevel;
    /**
     *    建设单位id
    **/
    @JSONField(name="buildCompanyId")
    private Integer buildCompanyId;
    /**
     *    施工图审查
    **/
    @JSONField(name="drawingReview")
    private BqProjectDrawingReview drawingReview;
    /**
     *    合同备案
    **/
    @JSONField(name="contractFiling")
    private BqProjectContractFiling contractFiling;
    /**
     *    省级项目编号
    **/
    @JSONField(name="provinceProNum")
    private String provinceProNum;
    /**
     *    建设性质
    **/
    @JSONField(name="buildNature")
    private String buildNature;
    /**
     *    竣工备案
    **/
    @JSONField(name="completionAcceptanceRecord")
    private BqProjectCompletionAcceptanceRecord completionAcceptanceRecord;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    建设单位
    **/
    @JSONField(name="buildCompany")
    private String buildCompany;
    /**
     *    施工许可
    **/
    @JSONField(name="constructionPermit")
    private BqProjectConstructionPermit constructionPermit;
    /**
     *    立项文号
    **/
    @JSONField(name="projectDocumentNumber")
    private String projectDocumentNumber;
    /**
     *    源项目名称
    **/
    @JSONField(name="proName")
    private String proName;
    /**
     *    总投资
    **/
    @JSONField(name="totalInvestment")
    private String totalInvestment;
    /**
     *    所在区划
    **/
    @JSONField(name="base")
    private String base;
    /**
     *    总面积
    **/
    @JSONField(name="totalArea")
    private String totalArea;


    /**
    *   设置 项目编号
    **/
    public void setProNum(String proNum) {
      this.proNum = proNum;
    }
    /**
    *   获取 项目编号
    **/
    public String getProNum() {
      return proNum;
    }
    /**
    *   设置 项目分类
    **/
    public void setProType(String proType) {
      this.proType = proType;
    }
    /**
    *   获取 项目分类
    **/
    public String getProType() {
      return proType;
    }
    /**
    *   设置 工程用途
    **/
    public void setProUse(String proUse) {
      this.proUse = proUse;
    }
    /**
    *   获取 工程用途
    **/
    public String getProUse() {
      return proUse;
    }
    /**
    *   设置 招投标
    **/
    public void setBidding(BqProjectBidding bidding) {
      this.bidding = bidding;
    }
    /**
    *   获取 招投标
    **/
    public BqProjectBidding getBidding() {
      return bidding;
    }
    /**
    *   设置 立项级别
    **/
    public void setProjectLevel(String projectLevel) {
      this.projectLevel = projectLevel;
    }
    /**
    *   获取 立项级别
    **/
    public String getProjectLevel() {
      return projectLevel;
    }
    /**
    *   设置 建设单位id
    **/
    public void setBuildCompanyId(Integer buildCompanyId) {
      this.buildCompanyId = buildCompanyId;
    }
    /**
    *   获取 建设单位id
    **/
    public Integer getBuildCompanyId() {
      return buildCompanyId;
    }
    /**
    *   设置 施工图审查
    **/
    public void setDrawingReview(BqProjectDrawingReview drawingReview) {
      this.drawingReview = drawingReview;
    }
    /**
    *   获取 施工图审查
    **/
    public BqProjectDrawingReview getDrawingReview() {
      return drawingReview;
    }
    /**
    *   设置 合同备案
    **/
    public void setContractFiling(BqProjectContractFiling contractFiling) {
      this.contractFiling = contractFiling;
    }
    /**
    *   获取 合同备案
    **/
    public BqProjectContractFiling getContractFiling() {
      return contractFiling;
    }
    /**
    *   设置 省级项目编号
    **/
    public void setProvinceProNum(String provinceProNum) {
      this.provinceProNum = provinceProNum;
    }
    /**
    *   获取 省级项目编号
    **/
    public String getProvinceProNum() {
      return provinceProNum;
    }
    /**
    *   设置 建设性质
    **/
    public void setBuildNature(String buildNature) {
      this.buildNature = buildNature;
    }
    /**
    *   获取 建设性质
    **/
    public String getBuildNature() {
      return buildNature;
    }
    /**
    *   设置 竣工备案
    **/
    public void setCompletionAcceptanceRecord(BqProjectCompletionAcceptanceRecord completionAcceptanceRecord) {
      this.completionAcceptanceRecord = completionAcceptanceRecord;
    }
    /**
    *   获取 竣工备案
    **/
    public BqProjectCompletionAcceptanceRecord getCompletionAcceptanceRecord() {
      return completionAcceptanceRecord;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 建设单位
    **/
    public void setBuildCompany(String buildCompany) {
      this.buildCompany = buildCompany;
    }
    /**
    *   获取 建设单位
    **/
    public String getBuildCompany() {
      return buildCompany;
    }
    /**
    *   设置 施工许可
    **/
    public void setConstructionPermit(BqProjectConstructionPermit constructionPermit) {
      this.constructionPermit = constructionPermit;
    }
    /**
    *   获取 施工许可
    **/
    public BqProjectConstructionPermit getConstructionPermit() {
      return constructionPermit;
    }
    /**
    *   设置 立项文号
    **/
    public void setProjectDocumentNumber(String projectDocumentNumber) {
      this.projectDocumentNumber = projectDocumentNumber;
    }
    /**
    *   获取 立项文号
    **/
    public String getProjectDocumentNumber() {
      return projectDocumentNumber;
    }
    /**
    *   设置 源项目名称
    **/
    public void setProName(String proName) {
      this.proName = proName;
    }
    /**
    *   获取 源项目名称
    **/
    public String getProName() {
      return proName;
    }
    /**
    *   设置 总投资
    **/
    public void setTotalInvestment(String totalInvestment) {
      this.totalInvestment = totalInvestment;
    }
    /**
    *   获取 总投资
    **/
    public String getTotalInvestment() {
      return totalInvestment;
    }
    /**
    *   设置 所在区划
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 所在区划
    **/
    public String getBase() {
      return base;
    }
    /**
    *   设置 总面积
    **/
    public void setTotalArea(String totalArea) {
      this.totalArea = totalArea;
    }
    /**
    *   获取 总面积
    **/
    public String getTotalArea() {
      return totalArea;
    }



}

