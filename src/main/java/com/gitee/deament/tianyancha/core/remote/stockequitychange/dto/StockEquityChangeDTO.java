package com.gitee.deament.tianyancha.core.remote.stockequitychange.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*股本变动
* 可以通过公司名称或ID获取上市公司股本变动信息，股本变动信息包括变动时间、变动原因、变动后A股总股本、变动后流通A股、变动后限售A股等
* /services/open/stock/equityChange/2.0
*@author deament
**/

@TYCURL(value="/services/open/stock/equityChange/2.0")
public class StockEquityChangeDTO implements Serializable{

    /**
     *    公司名称，可通过查询接口获取
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数（默认20，最大20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    公司id，可通过查询接口获取
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    当前页
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 公司名称，可通过查询接口获取
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称，可通过查询接口获取
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（默认20，最大20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20，最大20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 公司id，可通过查询接口获取
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id，可通过查询接口获取
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 当前页
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

