package com.gitee.deament.tianyancha.core.remote.relashortpath.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*最短路径发现
* 属于RelaShortPath
* /services/open/rela/shortPath
*@author deament
**/

public class RelaShortPathNodes implements Serializable{

    /**
     *    节点id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    节点名
    **/
    @JSONField(name="properties")
    private RelaShortPathProperties properties;
    /**
     *    
    **/
    @JSONField(name="labels")
    private List<String> labels;


    /**
    *   设置 节点id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 节点id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 节点名
    **/
    public void setProperties(RelaShortPathProperties properties) {
      this.properties = properties;
    }
    /**
    *   获取 节点名
    **/
    public RelaShortPathProperties getProperties() {
      return properties;
    }
    /**
    *   设置 
    **/
    public void setLabels(List<String> labels) {
      this.labels = labels;
    }
    /**
    *   获取 
    **/
    public List<String> getLabels() {
      return labels;
    }



}

