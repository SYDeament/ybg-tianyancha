package com.gitee.deament.tianyancha.core.remote.mrjudicialsale.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrjudicialsale.entity.MrJudicialSaleDetail;
import com.gitee.deament.tianyancha.core.remote.mrjudicialsale.entity.MrJudicialSaleItems;
/**
*司法拍卖
* 可以通过公司名称或ID获取企业司法拍卖公告信息，企业司法拍卖公告信息包括拍卖公告标题、执行法院、拍卖时间、拍卖标的、起拍价格等字段的详细信息
* /services/open/mr/judicialSale/2.0
*@author deament
**/

public class MrJudicialSale implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MrJudicialSaleItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MrJudicialSaleItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MrJudicialSaleItems> getItems() {
      return items;
    }



}

