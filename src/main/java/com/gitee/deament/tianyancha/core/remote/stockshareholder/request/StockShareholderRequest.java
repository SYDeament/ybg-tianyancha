package com.gitee.deament.tianyancha.core.remote.stockshareholder.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockshareholder.entity.StockShareholder;
import com.gitee.deament.tianyancha.core.remote.stockshareholder.dto.StockShareholderDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*十大股东（十大流通股东）
* 可以通过公司名称或ID获取上市公司十大股东和十大流通股东信息，十大股东和十大流通股东信息包括机构或基金、持有数量、持股变化、占股本比例、实际增减持、股份类型等
* /services/open/stock/shareholder/2.0
*@author deament
**/

public interface StockShareholderRequest extends BaseRequest<StockShareholderDTO ,StockShareholder>{

}

