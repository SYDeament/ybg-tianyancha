package com.gitee.deament.tianyancha.core.remote.pfboss.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfboss.entity.PfBossTrack_record;
/**
*高管信息
* 可以通过公司名称或ID获取相关私募基金高管信息，包括法定代表人/执行事物合伙人姓名、是否有从业资格、资格取得方式等字段的相关信息
* /services/open/pf/boss/2.0
*@author deament
**/

public class PfBoss implements Serializable{

    /**
     *    法定代表人/执行事物合伙人（委派代表）姓名
    **/
    @JSONField(name="boss_name")
    private String boss_name;
    /**
     *    闵兰钧工作履历
    **/
    @JSONField(name="track_record")
    private List<PfBossTrack_record> track_record;
    /**
     *    高管id
    **/
    @JSONField(name="boss_graph_id")
    private Integer boss_graph_id;
    /**
     *    资格取得方式
    **/
    @JSONField(name="boss_qua_way")
    private String boss_qua_way;
    /**
     *    企业id
    **/
    @JSONField(name="graph_id")
    private Integer graph_id;


    /**
    *   设置 法定代表人/执行事物合伙人（委派代表）姓名
    **/
    public void setBoss_name(String boss_name) {
      this.boss_name = boss_name;
    }
    /**
    *   获取 法定代表人/执行事物合伙人（委派代表）姓名
    **/
    public String getBoss_name() {
      return boss_name;
    }
    /**
    *   设置 闵兰钧工作履历
    **/
    public void setTrack_record(List<PfBossTrack_record> track_record) {
      this.track_record = track_record;
    }
    /**
    *   获取 闵兰钧工作履历
    **/
    public List<PfBossTrack_record> getTrack_record() {
      return track_record;
    }
    /**
    *   设置 高管id
    **/
    public void setBoss_graph_id(Integer boss_graph_id) {
      this.boss_graph_id = boss_graph_id;
    }
    /**
    *   获取 高管id
    **/
    public Integer getBoss_graph_id() {
      return boss_graph_id;
    }
    /**
    *   设置 资格取得方式
    **/
    public void setBoss_qua_way(String boss_qua_way) {
      this.boss_qua_way = boss_qua_way;
    }
    /**
    *   获取 资格取得方式
    **/
    public String getBoss_qua_way() {
      return boss_qua_way;
    }
    /**
    *   设置 企业id
    **/
    public void setGraph_id(Integer graph_id) {
      this.graph_id = graph_id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getGraph_id() {
      return graph_id;
    }



}

