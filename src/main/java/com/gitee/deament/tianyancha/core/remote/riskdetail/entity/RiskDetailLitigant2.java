package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailLitigant2 implements Serializable{

    /**
     *    企业名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    企业id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 企业名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 企业id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }



}

