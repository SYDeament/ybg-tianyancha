package com.gitee.deament.tianyancha.core.remote.msupply.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*供应商
* 属于MSupply
* /services/open/m/supply/2.0
*@author deament
**/

public class MSupplyResult implements Serializable{

    /**
     *    供应商id
    **/
    @JSONField(name="supplier_graphId")
    private Integer supplier_graphId;
    /**
     *    报告期
    **/
    @JSONField(name="announcement_date")
    private Integer announcement_date;
    /**
     *    采购金额（万元）
    **/
    @JSONField(name="amt")
    private String amt;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    供应商名称
    **/
    @JSONField(name="supplier_name")
    private String supplier_name;
    /**
     *    关联关系
    **/
    @JSONField(name="relationship")
    private String relationship;
    /**
     *    数据来源
    **/
    @JSONField(name="dataSource")
    private String dataSource;
    /**
     *    采购占比
    **/
    @JSONField(name="ratio")
    private String ratio;


    /**
    *   设置 供应商id
    **/
    public void setSupplier_graphId(Integer supplier_graphId) {
      this.supplier_graphId = supplier_graphId;
    }
    /**
    *   获取 供应商id
    **/
    public Integer getSupplier_graphId() {
      return supplier_graphId;
    }
    /**
    *   设置 报告期
    **/
    public void setAnnouncement_date(Integer announcement_date) {
      this.announcement_date = announcement_date;
    }
    /**
    *   获取 报告期
    **/
    public Integer getAnnouncement_date() {
      return announcement_date;
    }
    /**
    *   设置 采购金额（万元）
    **/
    public void setAmt(String amt) {
      this.amt = amt;
    }
    /**
    *   获取 采购金额（万元）
    **/
    public String getAmt() {
      return amt;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 供应商名称
    **/
    public void setSupplier_name(String supplier_name) {
      this.supplier_name = supplier_name;
    }
    /**
    *   获取 供应商名称
    **/
    public String getSupplier_name() {
      return supplier_name;
    }
    /**
    *   设置 关联关系
    **/
    public void setRelationship(String relationship) {
      this.relationship = relationship;
    }
    /**
    *   获取 关联关系
    **/
    public String getRelationship() {
      return relationship;
    }
    /**
    *   设置 数据来源
    **/
    public void setDataSource(String dataSource) {
      this.dataSource = dataSource;
    }
    /**
    *   获取 数据来源
    **/
    public String getDataSource() {
      return dataSource;
    }
    /**
    *   设置 采购占比
    **/
    public void setRatio(String ratio) {
      this.ratio = ratio;
    }
    /**
    *   获取 采购占比
    **/
    public String getRatio() {
      return ratio;
    }



}

