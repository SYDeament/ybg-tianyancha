package com.gitee.deament.tianyancha.core.remote.cbdevelopment.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity.CbDevelopment;
import com.gitee.deament.tianyancha.core.remote.cbdevelopment.dto.CbDevelopmentDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*企业发展
* 可以通过公司名称或ID获取包含核心团队、企业业务、融资历史、投资事件、竞品信息、相关项目品牌/投资机构、投资动态等维度的相关信息
* /services/open/cb/development/2.0
*@author deament
**/
@Component("cbDevelopmentRequestImpl")
public class CbDevelopmentRequestImpl extends BaseRequestImpl<CbDevelopment,CbDevelopmentDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cb/development/2.0";
    }
}

