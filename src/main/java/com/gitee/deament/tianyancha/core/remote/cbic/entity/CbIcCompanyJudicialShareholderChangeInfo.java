package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcCompanyJudicialShareholderChangeInfo implements Serializable{

    /**
     *    被执行人
    **/
    @JSONField(name="executedPerson")
    private String executedPerson;
    /**
     *    被执行人证照种类
    **/
    @JSONField(name="licenseType")
    private String licenseType;
    /**
     *    受让人证照号码
    **/
    @JSONField(name="assigneeLicenseNum")
    private String assigneeLicenseNum;
    /**
     *    执行通知书文号
    **/
    @JSONField(name="executeNoticeNum")
    private String executeNoticeNum;
    /**
     *    执行事项
    **/
    @JSONField(name="implementationMatters")
    private String implementationMatters;
    /**
     *    受让人证照种类
    **/
    @JSONField(name="assigneeLicenseType")
    private String assigneeLicenseType;
    /**
     *    协助执行日期
    **/
    @JSONField(name="executionDate")
    private String executionDate;
    /**
     *    执行裁定书文号
    **/
    @JSONField(name="executeOrderNum")
    private String executeOrderNum;
    /**
     *    被执行人证照号码
    **/
    @JSONField(name="licenseNum")
    private String licenseNum;
    /**
     *    被执行人持有股权数额
    **/
    @JSONField(name="equityAmountOther")
    private String equityAmountOther;
    /**
     *    受让人
    **/
    @JSONField(name="assignee")
    private String assignee;
    /**
     *    执行法院
    **/
    @JSONField(name="executiveCourt")
    private String executiveCourt;


    /**
    *   设置 被执行人
    **/
    public void setExecutedPerson(String executedPerson) {
      this.executedPerson = executedPerson;
    }
    /**
    *   获取 被执行人
    **/
    public String getExecutedPerson() {
      return executedPerson;
    }
    /**
    *   设置 被执行人证照种类
    **/
    public void setLicenseType(String licenseType) {
      this.licenseType = licenseType;
    }
    /**
    *   获取 被执行人证照种类
    **/
    public String getLicenseType() {
      return licenseType;
    }
    /**
    *   设置 受让人证照号码
    **/
    public void setAssigneeLicenseNum(String assigneeLicenseNum) {
      this.assigneeLicenseNum = assigneeLicenseNum;
    }
    /**
    *   获取 受让人证照号码
    **/
    public String getAssigneeLicenseNum() {
      return assigneeLicenseNum;
    }
    /**
    *   设置 执行通知书文号
    **/
    public void setExecuteNoticeNum(String executeNoticeNum) {
      this.executeNoticeNum = executeNoticeNum;
    }
    /**
    *   获取 执行通知书文号
    **/
    public String getExecuteNoticeNum() {
      return executeNoticeNum;
    }
    /**
    *   设置 执行事项
    **/
    public void setImplementationMatters(String implementationMatters) {
      this.implementationMatters = implementationMatters;
    }
    /**
    *   获取 执行事项
    **/
    public String getImplementationMatters() {
      return implementationMatters;
    }
    /**
    *   设置 受让人证照种类
    **/
    public void setAssigneeLicenseType(String assigneeLicenseType) {
      this.assigneeLicenseType = assigneeLicenseType;
    }
    /**
    *   获取 受让人证照种类
    **/
    public String getAssigneeLicenseType() {
      return assigneeLicenseType;
    }
    /**
    *   设置 协助执行日期
    **/
    public void setExecutionDate(String executionDate) {
      this.executionDate = executionDate;
    }
    /**
    *   获取 协助执行日期
    **/
    public String getExecutionDate() {
      return executionDate;
    }
    /**
    *   设置 执行裁定书文号
    **/
    public void setExecuteOrderNum(String executeOrderNum) {
      this.executeOrderNum = executeOrderNum;
    }
    /**
    *   获取 执行裁定书文号
    **/
    public String getExecuteOrderNum() {
      return executeOrderNum;
    }
    /**
    *   设置 被执行人证照号码
    **/
    public void setLicenseNum(String licenseNum) {
      this.licenseNum = licenseNum;
    }
    /**
    *   获取 被执行人证照号码
    **/
    public String getLicenseNum() {
      return licenseNum;
    }
    /**
    *   设置 被执行人持有股权数额
    **/
    public void setEquityAmountOther(String equityAmountOther) {
      this.equityAmountOther = equityAmountOther;
    }
    /**
    *   获取 被执行人持有股权数额
    **/
    public String getEquityAmountOther() {
      return equityAmountOther;
    }
    /**
    *   设置 受让人
    **/
    public void setAssignee(String assignee) {
      this.assignee = assignee;
    }
    /**
    *   获取 受让人
    **/
    public String getAssignee() {
      return assignee;
    }
    /**
    *   设置 执行法院
    **/
    public void setExecutiveCourt(String executiveCourt) {
      this.executiveCourt = executiveCourt;
    }
    /**
    *   获取 执行法院
    **/
    public String getExecutiveCourt() {
      return executiveCourt;
    }



}

