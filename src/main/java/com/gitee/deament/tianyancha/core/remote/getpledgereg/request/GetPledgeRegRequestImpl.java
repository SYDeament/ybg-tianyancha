package com.gitee.deament.tianyancha.core.remote.getpledgereg.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.getpledgereg.entity.GetPledgeReg;
import com.gitee.deament.tianyancha.core.remote.getpledgereg.dto.GetPledgeRegDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*知识产权出质
* 可以通过公司名称或ID获取知识产权出质信息，知识产权出质信息包括质权人名称、知识产权登记证号等字段的详细信息
* /services/v4/open/getPledgeReg
*@author deament
**/
@Component("getPledgeRegRequestImpl")
public class GetPledgeRegRequestImpl extends BaseRequestImpl<GetPledgeReg,GetPledgeRegDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/getPledgeReg";
    }
}

