package com.gitee.deament.tianyancha.core.remote.mbp.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mbp.entity.MBp;
import com.gitee.deament.tianyancha.core.remote.mbp.dto.MBpDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业招聘-百聘
* 可以通过公司名称或ID获取企业招聘相关信息，企业招聘相关信息包括发布日期、招聘职位、月薪、学历、地区等字段的详细信息
* /services/open/m/bp/employments/2.0
*@author deament
**/

public interface MBpRequest extends BaseRequest<MBpDTO ,MBp>{

}

