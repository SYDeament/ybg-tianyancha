package com.gitee.deament.tianyancha.core.remote.mcustomer.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*客户
* 属于MCustomer
* /services/open/m/customer/2.0
*@author deament
**/

public class MCustomerPageBean implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private List<MCustomerResult> result;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;


    /**
    *   设置 
    **/
    public void setResult(List<MCustomerResult> result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public List<MCustomerResult> getResult() {
      return result;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }



}

