package com.gitee.deament.tianyancha.core.remote.stockguarantees.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockguarantees.entity.StockGuaranteesResult;
/**
*对外担保
* 可以通过公司名称或ID获取上市公司对外担保信息，对外担保信息包括公告日期、担保方、被担保方、担保方式、担保金额等
* /services/open/stock/guarantees/2.0
*@author deament
**/

public class StockGuarantees implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private List<StockGuaranteesResult> result;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;


    /**
    *   设置 
    **/
    public void setResult(List<StockGuaranteesResult> result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public List<StockGuaranteesResult> getResult() {
      return result;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }



}

