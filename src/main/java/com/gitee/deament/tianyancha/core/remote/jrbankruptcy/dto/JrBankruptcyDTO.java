package com.gitee.deament.tianyancha.core.remote.jrbankruptcy.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*破产重整详情
* 可以通过公司名称或ID获取破产重整信息，破产重整信息包括破产案件公开时间、申请人、被申请人、申请对象、管理人主要负责人等字段的详细信息
* /services/open/jr/bankruptcy/detail/2.0
*@author deament
**/

@TYCURL(value="/services/open/jr/bankruptcy/detail/2.0")
public class JrBankruptcyDTO implements Serializable{

    /**
     *    公司id
     *
    **/
    @ParamRequire(require = true)
    private String gid;
    /**
     *    破产uuid
     *
    **/
    @ParamRequire(require = true)
    private String uuid;


    /**
    *   设置 公司id
    **/
    public void setGid(String gid) {
      this.gid = gid;
    }
    /**
    *   获取 公司id
    **/
    public String getGid() {
      return gid;
    }
    /**
    *   设置 破产uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 破产uuid
    **/
    public String getUuid() {
      return uuid;
    }



}

