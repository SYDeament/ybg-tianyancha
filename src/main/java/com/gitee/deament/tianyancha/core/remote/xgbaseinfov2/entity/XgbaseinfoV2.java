package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;

import java.io.Serializable;

/**
 *特殊企业基本信息
 * 属于XgbaseinfoV2
 * /services/v4/open/xgbaseinfoV2
 *@author deament
 **/
public class XgbaseinfoV2 implements Serializable {
    private XgbaseinfoV21 xgbaseinfoV21;
    private XgbaseinfoV22 xgbaseinfoV22;
    private XgbaseinfoV23 xgbaseinfoV23;
    private XgbaseinfoV24 xgbaseinfoV24;
    private XgbaseinfoV25 xgbaseinfoV25;
    private XgbaseinfoV26 xgbaseinfoV26;

    public XgbaseinfoV21 getXgbaseinfoV21() {
        return xgbaseinfoV21;
    }

    public void setXgbaseinfoV21(XgbaseinfoV21 xgbaseinfoV21) {
        this.xgbaseinfoV21 = xgbaseinfoV21;
    }

    public XgbaseinfoV22 getXgbaseinfoV22() {
        return xgbaseinfoV22;
    }

    public void setXgbaseinfoV22(XgbaseinfoV22 xgbaseinfoV22) {
        this.xgbaseinfoV22 = xgbaseinfoV22;
    }

    public XgbaseinfoV23 getXgbaseinfoV23() {
        return xgbaseinfoV23;
    }

    public void setXgbaseinfoV23(XgbaseinfoV23 xgbaseinfoV23) {
        this.xgbaseinfoV23 = xgbaseinfoV23;
    }

    public XgbaseinfoV24 getXgbaseinfoV24() {
        return xgbaseinfoV24;
    }

    public void setXgbaseinfoV24(XgbaseinfoV24 xgbaseinfoV24) {
        this.xgbaseinfoV24 = xgbaseinfoV24;
    }

    public XgbaseinfoV25 getXgbaseinfoV25() {
        return xgbaseinfoV25;
    }

    public void setXgbaseinfoV25(XgbaseinfoV25 xgbaseinfoV25) {
        this.xgbaseinfoV25 = xgbaseinfoV25;
    }

    public XgbaseinfoV26 getXgbaseinfoV26() {
        return xgbaseinfoV26;
    }

    public void setXgbaseinfoV26(XgbaseinfoV26 xgbaseinfoV26) {
        this.xgbaseinfoV26 = xgbaseinfoV26;
    }
}
