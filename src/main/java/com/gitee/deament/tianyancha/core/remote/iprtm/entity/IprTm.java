package com.gitee.deament.tianyancha.core.remote.iprtm.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprtm.entity.IprTmGoodsList;
import com.gitee.deament.tianyancha.core.remote.iprtm.entity.IprTmDetail;
import com.gitee.deament.tianyancha.core.remote.iprtm.entity.IprTmFlowList;
/**
*商标信息详情
* 可以通过注册号或国际分类获取商标信息详情，商标信息详情包括商标国际分类、商标名称、商标申请日期、商标申请人、商标注册地址、商标合作申请人、商标申请人（英文）、商标地址（英文）、商标初审公告期号、商标初审公告日期、商标注册公告期号、商标注册公告日期、商标专用权期限期限、商标代理/办理机构等字段的信息
* /services/open/ipr/tm/detail/2.0
*@author deament
**/

public class IprTm implements Serializable{

    /**
     *    商品／服务
    **/
    @JSONField(name="goodsList")
    private List<IprTmGoodsList> goodsList;
    /**
     *    
    **/
    @JSONField(name="detail")
    private IprTmDetail detail;
    /**
     *    商标最新流程
    **/
    @JSONField(name="flowStep")
    private String flowStep;
    /**
     *    商标流程
    **/
    @JSONField(name="flowList")
    private List<IprTmFlowList> flowList;


    /**
    *   设置 商品／服务
    **/
    public void setGoodsList(List<IprTmGoodsList> goodsList) {
      this.goodsList = goodsList;
    }
    /**
    *   获取 商品／服务
    **/
    public List<IprTmGoodsList> getGoodsList() {
      return goodsList;
    }
    /**
    *   设置 
    **/
    public void setDetail(IprTmDetail detail) {
      this.detail = detail;
    }
    /**
    *   获取 
    **/
    public IprTmDetail getDetail() {
      return detail;
    }
    /**
    *   设置 商标最新流程
    **/
    public void setFlowStep(String flowStep) {
      this.flowStep = flowStep;
    }
    /**
    *   获取 商标最新流程
    **/
    public String getFlowStep() {
      return flowStep;
    }
    /**
    *   设置 商标流程
    **/
    public void setFlowList(List<IprTmFlowList> flowList) {
      this.flowList = flowList;
    }
    /**
    *   获取 商标流程
    **/
    public List<IprTmFlowList> getFlowList() {
      return flowList;
    }



}

