package com.gitee.deament.tianyancha.core.remote.cbhistory.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史信息
* 属于CbHistory
* /services/open/cb/history/2.0
*@author deament
**/

public class CbHistoryLicenseCreditchinaList implements Serializable{

    /**
     *    内容许可
    **/
    @JSONField(name="licenceContent")
    private String licenceContent;
    /**
     *    许可有效期
    **/
    @JSONField(name="validityTime")
    private String validityTime;
    /**
     *    许可证号
    **/
    @JSONField(name="licenceNumber")
    private String licenceNumber;
    /**
     *    地方编码
    **/
    @JSONField(name="localCode")
    private String localCode;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    许可机关
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    数据更新时间
    **/
    @JSONField(name="dataUpdateTime")
    private String dataUpdateTime;
    /**
     *    审核类型
    **/
    @JSONField(name="auditYpe")
    private String auditYpe;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    许可决定日期
    **/
    @JSONField(name="decisionDate")
    private String decisionDate;


    /**
    *   设置 内容许可
    **/
    public void setLicenceContent(String licenceContent) {
      this.licenceContent = licenceContent;
    }
    /**
    *   获取 内容许可
    **/
    public String getLicenceContent() {
      return licenceContent;
    }
    /**
    *   设置 许可有效期
    **/
    public void setValidityTime(String validityTime) {
      this.validityTime = validityTime;
    }
    /**
    *   获取 许可有效期
    **/
    public String getValidityTime() {
      return validityTime;
    }
    /**
    *   设置 许可证号
    **/
    public void setLicenceNumber(String licenceNumber) {
      this.licenceNumber = licenceNumber;
    }
    /**
    *   获取 许可证号
    **/
    public String getLicenceNumber() {
      return licenceNumber;
    }
    /**
    *   设置 地方编码
    **/
    public void setLocalCode(String localCode) {
      this.localCode = localCode;
    }
    /**
    *   获取 地方编码
    **/
    public String getLocalCode() {
      return localCode;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 许可机关
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 许可机关
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 数据更新时间
    **/
    public void setDataUpdateTime(String dataUpdateTime) {
      this.dataUpdateTime = dataUpdateTime;
    }
    /**
    *   获取 数据更新时间
    **/
    public String getDataUpdateTime() {
      return dataUpdateTime;
    }
    /**
    *   设置 审核类型
    **/
    public void setAuditYpe(String auditYpe) {
      this.auditYpe = auditYpe;
    }
    /**
    *   获取 审核类型
    **/
    public String getAuditYpe() {
      return auditYpe;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 许可决定日期
    **/
    public void setDecisionDate(String decisionDate) {
      this.decisionDate = decisionDate;
    }
    /**
    *   获取 许可决定日期
    **/
    public String getDecisionDate() {
      return decisionDate;
    }



}

