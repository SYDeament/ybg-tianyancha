package com.gitee.deament.tianyancha.core.remote.icannualreport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业年报
* 属于IcAnnualreport
* /services/open/ic/annualreport/2.0
*@author deament
**/

public class IcAnnualreportEquityChangeInfoList implements Serializable{

    /**
     *    股权变更日期
    **/
    @JSONField(name="changeTime")
    private String changeTime;
    /**
     *    年份
    **/
    @JSONField(name="reportYear")
    private String reportYear;
    /**
     *    变更前股权比例
    **/
    @JSONField(name="ratioBefore")
    private String ratioBefore;
    /**
     *    变更后股权比例
    **/
    @JSONField(name="ratioAfter")
    private String ratioAfter;
    /**
     *    股东（发起人）
    **/
    @JSONField(name="investorName")
    private String investorName;


    /**
    *   设置 股权变更日期
    **/
    public void setChangeTime(String changeTime) {
      this.changeTime = changeTime;
    }
    /**
    *   获取 股权变更日期
    **/
    public String getChangeTime() {
      return changeTime;
    }
    /**
    *   设置 年份
    **/
    public void setReportYear(String reportYear) {
      this.reportYear = reportYear;
    }
    /**
    *   获取 年份
    **/
    public String getReportYear() {
      return reportYear;
    }
    /**
    *   设置 变更前股权比例
    **/
    public void setRatioBefore(String ratioBefore) {
      this.ratioBefore = ratioBefore;
    }
    /**
    *   获取 变更前股权比例
    **/
    public String getRatioBefore() {
      return ratioBefore;
    }
    /**
    *   设置 变更后股权比例
    **/
    public void setRatioAfter(String ratioAfter) {
      this.ratioAfter = ratioAfter;
    }
    /**
    *   获取 变更后股权比例
    **/
    public String getRatioAfter() {
      return ratioAfter;
    }
    /**
    *   设置 股东（发起人）
    **/
    public void setInvestorName(String investorName) {
      this.investorName = investorName;
    }
    /**
    *   获取 股东（发起人）
    **/
    public String getInvestorName() {
      return investorName;
    }



}

