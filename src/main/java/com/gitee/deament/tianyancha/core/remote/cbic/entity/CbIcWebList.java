package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcWebList implements Serializable{

    /**
     *    网站类型
    **/
    @JSONField(name="webType")
    private String webType;
    /**
     *    网址
    **/
    @JSONField(name="website")
    private String website;
    /**
     *    名称
    **/
    @JSONField(name="name")
    private String name;


    /**
    *   设置 网站类型
    **/
    public void setWebType(String webType) {
      this.webType = webType;
    }
    /**
    *   获取 网站类型
    **/
    public String getWebType() {
      return webType;
    }
    /**
    *   设置 网址
    **/
    public void setWebsite(String website) {
      this.website = website;
    }
    /**
    *   获取 网址
    **/
    public String getWebsite() {
      return website;
    }
    /**
    *   设置 名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 名称
    **/
    public String getName() {
      return name;
    }



}

