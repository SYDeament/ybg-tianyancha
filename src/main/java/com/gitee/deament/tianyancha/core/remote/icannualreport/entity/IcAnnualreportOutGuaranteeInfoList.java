package com.gitee.deament.tianyancha.core.remote.icannualreport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业年报
* 属于IcAnnualreport
* /services/open/ic/annualreport/2.0
*@author deament
**/

public class IcAnnualreportOutGuaranteeInfoList implements Serializable{

    /**
     *    主债权数额
    **/
    @JSONField(name="creditoAmount")
    private String creditoAmount;
    /**
     *    年份
    **/
    @JSONField(name="reportYear")
    private String reportYear;
    /**
     *    保证的期间
    **/
    @JSONField(name="guaranteeTerm")
    private String guaranteeTerm;
    /**
     *    保证担保的范围
    **/
    @JSONField(name="guaranteeScope")
    private String guaranteeScope;
    /**
     *    债权人
    **/
    @JSONField(name="creditor")
    private String creditor;
    /**
     *    债务人
    **/
    @JSONField(name="obligor")
    private String obligor;
    /**
     *    保证的方式
    **/
    @JSONField(name="guaranteeWay")
    private String guaranteeWay;
    /**
     *    履行债务的期限
    **/
    @JSONField(name="creditoTerm")
    private String creditoTerm;
    /**
     *    主债权种类
    **/
    @JSONField(name="creditoType")
    private String creditoType;


    /**
    *   设置 主债权数额
    **/
    public void setCreditoAmount(String creditoAmount) {
      this.creditoAmount = creditoAmount;
    }
    /**
    *   获取 主债权数额
    **/
    public String getCreditoAmount() {
      return creditoAmount;
    }
    /**
    *   设置 年份
    **/
    public void setReportYear(String reportYear) {
      this.reportYear = reportYear;
    }
    /**
    *   获取 年份
    **/
    public String getReportYear() {
      return reportYear;
    }
    /**
    *   设置 保证的期间
    **/
    public void setGuaranteeTerm(String guaranteeTerm) {
      this.guaranteeTerm = guaranteeTerm;
    }
    /**
    *   获取 保证的期间
    **/
    public String getGuaranteeTerm() {
      return guaranteeTerm;
    }
    /**
    *   设置 保证担保的范围
    **/
    public void setGuaranteeScope(String guaranteeScope) {
      this.guaranteeScope = guaranteeScope;
    }
    /**
    *   获取 保证担保的范围
    **/
    public String getGuaranteeScope() {
      return guaranteeScope;
    }
    /**
    *   设置 债权人
    **/
    public void setCreditor(String creditor) {
      this.creditor = creditor;
    }
    /**
    *   获取 债权人
    **/
    public String getCreditor() {
      return creditor;
    }
    /**
    *   设置 债务人
    **/
    public void setObligor(String obligor) {
      this.obligor = obligor;
    }
    /**
    *   获取 债务人
    **/
    public String getObligor() {
      return obligor;
    }
    /**
    *   设置 保证的方式
    **/
    public void setGuaranteeWay(String guaranteeWay) {
      this.guaranteeWay = guaranteeWay;
    }
    /**
    *   获取 保证的方式
    **/
    public String getGuaranteeWay() {
      return guaranteeWay;
    }
    /**
    *   设置 履行债务的期限
    **/
    public void setCreditoTerm(String creditoTerm) {
      this.creditoTerm = creditoTerm;
    }
    /**
    *   获取 履行债务的期限
    **/
    public String getCreditoTerm() {
      return creditoTerm;
    }
    /**
    *   设置 主债权种类
    **/
    public void setCreditoType(String creditoType) {
      this.creditoType = creditoType;
    }
    /**
    *   获取 主债权种类
    **/
    public String getCreditoType() {
      return creditoType;
    }



}

