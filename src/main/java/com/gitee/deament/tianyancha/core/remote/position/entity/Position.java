package com.gitee.deament.tianyancha.core.remote.position.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业经纬度
* 可以通过公司名称或ID获取企业经纬度信息
* /services/v4/open/position
*@author deament
**/

public class Position implements Serializable{

    /**
     *    省
    **/
    @JSONField(name="province")
    private String province;
    /**
     *    市
    **/
    @JSONField(name="city")
    private String city;
    /**
     *    纬度
    **/
    @JSONField(name="latitude")
    private Integer latitude;
    /**
     *    区县
    **/
    @JSONField(name="district")
    private String district;
    /**
     *    企业名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    注册地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    公司id
    **/
    @JSONField(name="graphId")
    private Integer graphId;
    /**
     *    经度
    **/
    @JSONField(name="longitude")
    private Integer longitude;


    /**
    *   设置 省
    **/
    public void setProvince(String province) {
      this.province = province;
    }
    /**
    *   获取 省
    **/
    public String getProvince() {
      return province;
    }
    /**
    *   设置 市
    **/
    public void setCity(String city) {
      this.city = city;
    }
    /**
    *   获取 市
    **/
    public String getCity() {
      return city;
    }
    /**
    *   设置 纬度
    **/
    public void setLatitude(Integer latitude) {
      this.latitude = latitude;
    }
    /**
    *   获取 纬度
    **/
    public Integer getLatitude() {
      return latitude;
    }
    /**
    *   设置 区县
    **/
    public void setDistrict(String district) {
      this.district = district;
    }
    /**
    *   获取 区县
    **/
    public String getDistrict() {
      return district;
    }
    /**
    *   设置 企业名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 注册地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 注册地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 公司id
    **/
    public void setGraphId(Integer graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getGraphId() {
      return graphId;
    }
    /**
    *   设置 经度
    **/
    public void setLongitude(Integer longitude) {
      this.longitude = longitude;
    }
    /**
    *   获取 经度
    **/
    public Integer getLongitude() {
      return longitude;
    }



}

