package com.gitee.deament.tianyancha.core.remote.pfproduct.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfproduct.entity.PfProduct;
import com.gitee.deament.tianyancha.core.remote.pfproduct.dto.PfProductDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*产品信息
* 可以通过公司名称或ID获取相关私募基金产品信息，包括暂行办法实施前/后成立的基金产品名称、基金编号等字段的相关信息
* /services/open/pf/product/2.0
*@author deament
**/
@Component("pfProductRequestImpl")
public class PfProductRequestImpl extends BaseRequestImpl<PfProduct,PfProductDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/pf/product/2.0";
    }
}

