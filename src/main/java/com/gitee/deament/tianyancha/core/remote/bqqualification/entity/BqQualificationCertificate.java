package com.gitee.deament.tianyancha.core.remote.bqqualification.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-资质资格详情
* 属于BqQualification
* /services/open/bq/qualification/detail/2.0
*@author deament
**/

public class BqQualificationCertificate implements Serializable{

    /**
     *    证书有效期
    **/
    @JSONField(name="validityPeriod")
    private String validityPeriod;
    /**
     *    证书编号
    **/
    @JSONField(name="certNo")
    private String certNo;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    发证日期
    **/
    @JSONField(name="certDate")
    private String certDate;
    /**
     *    资质范围
    **/
    @JSONField(name="qualificationRange")
    private String qualificationRange;


    /**
    *   设置 证书有效期
    **/
    public void setValidityPeriod(String validityPeriod) {
      this.validityPeriod = validityPeriod;
    }
    /**
    *   获取 证书有效期
    **/
    public String getValidityPeriod() {
      return validityPeriod;
    }
    /**
    *   设置 证书编号
    **/
    public void setCertNo(String certNo) {
      this.certNo = certNo;
    }
    /**
    *   获取 证书编号
    **/
    public String getCertNo() {
      return certNo;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 发证日期
    **/
    public void setCertDate(String certDate) {
      this.certDate = certDate;
    }
    /**
    *   获取 发证日期
    **/
    public String getCertDate() {
      return certDate;
    }
    /**
    *   设置 资质范围
    **/
    public void setQualificationRange(String qualificationRange) {
      this.qualificationRange = qualificationRange;
    }
    /**
    *   获取 资质范围
    **/
    public String getQualificationRange() {
      return qualificationRange;
    }



}

