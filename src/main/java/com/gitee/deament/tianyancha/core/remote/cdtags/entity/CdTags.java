package com.gitee.deament.tianyancha.core.remote.cdtags.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*获取标签
* 可以通过公司名称或ID获取企业标签，企业标签包括领域分类、主营业务、榜单等
* /services/open/cd/tags/2.0
*@author deament
**/

public class CdTags implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<String> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<String> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<String> getItems() {
      return items;
    }



}

