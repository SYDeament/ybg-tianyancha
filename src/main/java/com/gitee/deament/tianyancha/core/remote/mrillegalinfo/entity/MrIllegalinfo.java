package com.gitee.deament.tianyancha.core.remote.mrillegalinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrillegalinfo.entity.MrIllegalinfoItems;
/**
*严重违法
* 可以通过公司名称或ID获取企业严重违法信息，严重违法信息包括列入/移除原因、时间、做出决定机关等字段的详细信息
* /services/open/mr/illegalinfo/2.0
*@author deament
**/

public class MrIllegalinfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MrIllegalinfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MrIllegalinfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MrIllegalinfoItems> getItems() {
      return items;
    }



}

