package com.gitee.deament.tianyancha.core.remote.stockcashflow.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockcashflow.entity.StockCashFlow;
import com.gitee.deament.tianyancha.core.remote.stockcashflow.dto.StockCashFlowDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*现金流量表
* 可以通过公司名称或ID获取上市公司现金流量表数据，现金流量表数据包括经营活动产生的现金流量、经营活动现金流小计、经营活动现金流出小计、经营活动产生的现金流量净额、投资活动产生的现金流量等
* /services/open/stock/cashFlow/2.0
*@author deament
**/
@Component("stockCashFlowRequestImpl")
public class StockCashFlowRequestImpl extends BaseRequestImpl<StockCashFlow,StockCashFlowDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/cashFlow/2.0";
    }
}

