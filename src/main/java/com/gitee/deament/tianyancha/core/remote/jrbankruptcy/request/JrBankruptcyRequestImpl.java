package com.gitee.deament.tianyancha.core.remote.jrbankruptcy.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrbankruptcy.entity.JrBankruptcy;
import com.gitee.deament.tianyancha.core.remote.jrbankruptcy.dto.JrBankruptcyDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*破产重整详情
* 可以通过公司名称或ID获取破产重整信息，破产重整信息包括破产案件公开时间、申请人、被申请人、申请对象、管理人主要负责人等字段的详细信息
* /services/open/jr/bankruptcy/detail/2.0
*@author deament
**/
@Component("jrBankruptcyRequestImpl")
public class JrBankruptcyRequestImpl extends BaseRequestImpl<JrBankruptcy,JrBankruptcyDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/jr/bankruptcy/detail/2.0";
    }
}

