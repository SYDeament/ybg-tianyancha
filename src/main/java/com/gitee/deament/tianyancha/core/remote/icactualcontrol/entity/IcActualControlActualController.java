package com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*疑似实际控制人
* 属于IcActualControl
* /services/open/ic/actualControl/2.0
*@author deament
**/

public class IcActualControlActualController implements Serializable{

    /**
     *    人id
    **/
    @JSONField(name="hId")
    private Integer hId;
    /**
     *    公司id
    **/
    @JSONField(name="gId")
    private Integer gId;
    /**
     *    控制人姓名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    1-人 2-公司
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 人id
    **/
    public void setHId(Integer hId) {
      this.hId = hId;
    }
    /**
    *   获取 人id
    **/
    public Integer getHId() {
      return hId;
    }
    /**
    *   设置 公司id
    **/
    public void setGId(Integer gId) {
      this.gId = gId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getGId() {
      return gId;
    }
    /**
    *   设置 控制人姓名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 控制人姓名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 1-人 2-公司
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-人 2-公司
    **/
    public Integer getType() {
      return type;
    }



}

