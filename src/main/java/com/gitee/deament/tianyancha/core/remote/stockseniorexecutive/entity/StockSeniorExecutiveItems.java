package com.gitee.deament.tianyancha.core.remote.stockseniorexecutive.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*高管信息
* 属于StockSeniorExecutive
* /services/open/stock/seniorExecutive/2.0
*@author deament
**/

public class StockSeniorExecutiveItems implements Serializable{

    /**
     *    介绍
    **/
    @JSONField(name="resume")
    private String resume;
    /**
     *    教育
    **/
    @JSONField(name="education")
    private String education;
    /**
     *    类型 1公司 2 人
    **/
    @JSONField(name="cType")
    private Integer cType;
    /**
     *    性别
    **/
    @JSONField(name="sex")
    private String sex;
    /**
     *    持股数
    **/
    @JSONField(name="numberOfShares")
    private String numberOfShares;
    /**
     *    薪资
    **/
    @JSONField(name="salary")
    private String salary;
    /**
     *    所属组 1董事会、2监事会、3高管
    **/
    @JSONField(name="managerGroup")
    private String managerGroup;
    /**
     *    公告日期
    **/
    @JSONField(name="reportDate")
    private String reportDate;
    /**
     *    高管名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    本届任期
    **/
    @JSONField(name="term")
    private String term;
    /**
     *    职位
    **/
    @JSONField(name="position")
    private String position;
    /**
     *    对应公司id
    **/
    @JSONField(name="graphId")
    private String graphId;
    /**
     *    年龄
    **/
    @JSONField(name="age")
    private String age;


    /**
    *   设置 介绍
    **/
    public void setResume(String resume) {
      this.resume = resume;
    }
    /**
    *   获取 介绍
    **/
    public String getResume() {
      return resume;
    }
    /**
    *   设置 教育
    **/
    public void setEducation(String education) {
      this.education = education;
    }
    /**
    *   获取 教育
    **/
    public String getEducation() {
      return education;
    }
    /**
    *   设置 类型 1公司 2 人
    **/
    public void setCType(Integer cType) {
      this.cType = cType;
    }
    /**
    *   获取 类型 1公司 2 人
    **/
    public Integer getCType() {
      return cType;
    }
    /**
    *   设置 性别
    **/
    public void setSex(String sex) {
      this.sex = sex;
    }
    /**
    *   获取 性别
    **/
    public String getSex() {
      return sex;
    }
    /**
    *   设置 持股数
    **/
    public void setNumberOfShares(String numberOfShares) {
      this.numberOfShares = numberOfShares;
    }
    /**
    *   获取 持股数
    **/
    public String getNumberOfShares() {
      return numberOfShares;
    }
    /**
    *   设置 薪资
    **/
    public void setSalary(String salary) {
      this.salary = salary;
    }
    /**
    *   获取 薪资
    **/
    public String getSalary() {
      return salary;
    }
    /**
    *   设置 所属组 1董事会、2监事会、3高管
    **/
    public void setManagerGroup(String managerGroup) {
      this.managerGroup = managerGroup;
    }
    /**
    *   获取 所属组 1董事会、2监事会、3高管
    **/
    public String getManagerGroup() {
      return managerGroup;
    }
    /**
    *   设置 公告日期
    **/
    public void setReportDate(String reportDate) {
      this.reportDate = reportDate;
    }
    /**
    *   获取 公告日期
    **/
    public String getReportDate() {
      return reportDate;
    }
    /**
    *   设置 高管名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 高管名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 本届任期
    **/
    public void setTerm(String term) {
      this.term = term;
    }
    /**
    *   获取 本届任期
    **/
    public String getTerm() {
      return term;
    }
    /**
    *   设置 职位
    **/
    public void setPosition(String position) {
      this.position = position;
    }
    /**
    *   获取 职位
    **/
    public String getPosition() {
      return position;
    }
    /**
    *   设置 对应公司id
    **/
    public void setGraphId(String graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 对应公司id
    **/
    public String getGraphId() {
      return graphId;
    }
    /**
    *   设置 年龄
    **/
    public void setAge(String age) {
      this.age = age;
    }
    /**
    *   获取 年龄
    **/
    public String getAge() {
      return age;
    }



}

