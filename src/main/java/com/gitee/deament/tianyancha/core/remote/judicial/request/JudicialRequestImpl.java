package com.gitee.deament.tianyancha.core.remote.judicial.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.judicial.entity.Judicial;
import com.gitee.deament.tianyancha.core.remote.judicial.dto.JudicialDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*司法协助
* 可以通过公司名称或ID获取司法协助信息，司法协助信息包括执行法院、案件内容、被执行人名称等字段的详细信息
* /services/v4/open/judicial
*@author deament
**/
@Component("judicialRequestImpl")
public class JudicialRequestImpl extends BaseRequestImpl<Judicial,JudicialDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/judicial";
    }
}

