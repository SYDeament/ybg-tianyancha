package com.gitee.deament.tianyancha.core.remote.hiequityinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史股权出质
* 属于HiEquityInfo
* /services/open/hi/equityInfo/2.0
*@author deament
**/

public class HiEquityInfoPledgorList implements Serializable{

    /**
     *    出质人
    **/
    @JSONField(name="name")
    private String name;


    /**
    *   设置 出质人
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 出质人
    **/
    public String getName() {
      return name;
    }



}

