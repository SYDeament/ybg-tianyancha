package com.gitee.deament.tianyancha.core.remote.oiextfunds.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oiextfunds.entity.OiExtFunds;
import com.gitee.deament.tianyancha.core.remote.oiextfunds.dto.OiExtFundsDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
* 对外投资基金
* 可以通过投资机构名称获取投资机构对外投资基金信息，包括企业名称、法人、注册资本、成立日期、投资方等字段信息
* /services/open/oi/extFunds/2.0
*@author deament
**/
@Component("oiExtFundsRequestImpl")
public class OiExtFundsRequestImpl extends BaseRequestImpl<OiExtFunds,OiExtFundsDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/oi/extFunds/2.0";
    }
}

