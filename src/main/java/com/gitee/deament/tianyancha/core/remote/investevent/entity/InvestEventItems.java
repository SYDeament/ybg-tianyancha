package com.gitee.deament.tianyancha.core.remote.investevent.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*投资动态
* 属于InvestEvent
* /services/v4/open/investEvent
*@author deament
**/

public class InvestEventItems implements Serializable{

    /**
     *    产品名称
    **/
    @JSONField(name="product")
    private String product;
    /**
     *    投资金额
    **/
    @JSONField(name="money")
    private String money;
    /**
     *    融资轮次
    **/
    @JSONField(name="round")
    private String round;
    /**
     *    产品id
    **/
    @JSONField(name="product_id")
    private String product_id;
    /**
     *    产品logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    新闻来源
    **/
    @JSONField(name="news_url")
    private String news_url;
    /**
     *    新闻标题
    **/
    @JSONField(name="news_title")
    private String news_title;
    /**
     *    投资时间
    **/
    @JSONField(name="invest_date")
    private String invest_date;


    /**
    *   设置 产品名称
    **/
    public void setProduct(String product) {
      this.product = product;
    }
    /**
    *   获取 产品名称
    **/
    public String getProduct() {
      return product;
    }
    /**
    *   设置 投资金额
    **/
    public void setMoney(String money) {
      this.money = money;
    }
    /**
    *   获取 投资金额
    **/
    public String getMoney() {
      return money;
    }
    /**
    *   设置 融资轮次
    **/
    public void setRound(String round) {
      this.round = round;
    }
    /**
    *   获取 融资轮次
    **/
    public String getRound() {
      return round;
    }
    /**
    *   设置 产品id
    **/
    public void setProduct_id(String product_id) {
      this.product_id = product_id;
    }
    /**
    *   获取 产品id
    **/
    public String getProduct_id() {
      return product_id;
    }
    /**
    *   设置 产品logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 产品logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 新闻来源
    **/
    public void setNews_url(String news_url) {
      this.news_url = news_url;
    }
    /**
    *   获取 新闻来源
    **/
    public String getNews_url() {
      return news_url;
    }
    /**
    *   设置 新闻标题
    **/
    public void setNews_title(String news_title) {
      this.news_title = news_title;
    }
    /**
    *   获取 新闻标题
    **/
    public String getNews_title() {
      return news_title;
    }
    /**
    *   设置 投资时间
    **/
    public void setInvest_date(String invest_date) {
      this.invest_date = invest_date;
    }
    /**
    *   获取 投资时间
    **/
    public String getInvest_date() {
      return invest_date;
    }



}

