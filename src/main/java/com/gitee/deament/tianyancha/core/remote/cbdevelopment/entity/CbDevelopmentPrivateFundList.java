package com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业发展
* 属于CbDevelopment
* /services/open/cb/development/2.0
*@author deament
**/

public class CbDevelopmentPrivateFundList implements Serializable{

    /**
     *    企业id
    **/
    @JSONField(name="company_graph_id")
    private Integer company_graph_id;
    /**
     *    法定代表人/执行事务合伙人
    **/
    @JSONField(name="boss_name")
    private String boss_name;
    /**
     *    私募基金管理人名称
    **/
    @JSONField(name="manager_name")
    private String manager_name;
    /**
     *    登记编号
    **/
    @JSONField(name="reg_no")
    private String reg_no;
    /**
     *    法人id
    **/
    @JSONField(name="human_graph_id")
    private Integer human_graph_id;
    /**
     *    机构类型
    **/
    @JSONField(name="org_type")
    private String org_type;
    /**
     *    成立日期
    **/
    @JSONField(name="est_date")
    private String est_date;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;


    /**
    *   设置 企业id
    **/
    public void setCompany_graph_id(Integer company_graph_id) {
      this.company_graph_id = company_graph_id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getCompany_graph_id() {
      return company_graph_id;
    }
    /**
    *   设置 法定代表人/执行事务合伙人
    **/
    public void setBoss_name(String boss_name) {
      this.boss_name = boss_name;
    }
    /**
    *   获取 法定代表人/执行事务合伙人
    **/
    public String getBoss_name() {
      return boss_name;
    }
    /**
    *   设置 私募基金管理人名称
    **/
    public void setManager_name(String manager_name) {
      this.manager_name = manager_name;
    }
    /**
    *   获取 私募基金管理人名称
    **/
    public String getManager_name() {
      return manager_name;
    }
    /**
    *   设置 登记编号
    **/
    public void setReg_no(String reg_no) {
      this.reg_no = reg_no;
    }
    /**
    *   获取 登记编号
    **/
    public String getReg_no() {
      return reg_no;
    }
    /**
    *   设置 法人id
    **/
    public void setHuman_graph_id(Integer human_graph_id) {
      this.human_graph_id = human_graph_id;
    }
    /**
    *   获取 法人id
    **/
    public Integer getHuman_graph_id() {
      return human_graph_id;
    }
    /**
    *   设置 机构类型
    **/
    public void setOrg_type(String org_type) {
      this.org_type = org_type;
    }
    /**
    *   获取 机构类型
    **/
    public String getOrg_type() {
      return org_type;
    }
    /**
    *   设置 成立日期
    **/
    public void setEst_date(String est_date) {
      this.est_date = est_date;
    }
    /**
    *   获取 成立日期
    **/
    public String getEst_date() {
      return est_date;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }



}

