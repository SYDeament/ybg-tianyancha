package com.gitee.deament.tianyancha.core;


import com.gitee.deament.tianyancha.core.enums.TYCResultCodeEnum;

import java.io.Serializable;

/**
 * @param <T>
 * @author deament
 */

public class TianyanchaResult<T> implements Serializable {
    private int error_code;
    private String reason;
    private T result;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }


    public boolean isSuccess() {
        if (error_code == TYCResultCodeEnum.OK.getCode()) {
            return true;
        }
        return false;
    }
}
