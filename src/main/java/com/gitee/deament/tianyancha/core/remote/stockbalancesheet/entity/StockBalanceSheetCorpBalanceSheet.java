package com.gitee.deament.tianyancha.core.remote.stockbalancesheet.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*资产负债表
* 属于StockBalanceSheet
* /services/open/stock/balanceSheet/2.0
*@author deament
**/

public class StockBalanceSheetCorpBalanceSheet implements Serializable{

    /**
     *    应交税费
    **/
    @JSONField(name="tax_payable")
    private String tax_payable;
    /**
     *    长期待摊费用
    **/
    @JSONField(name="lt_deferred_expense")
    private String lt_deferred_expense;
    /**
     *    流动资产合计
    **/
    @JSONField(name="total_current_assets")
    private String total_current_assets;
    /**
     *    资产总计
    **/
    @JSONField(name="total_assets")
    private String total_assets;
    /**
     *    预付款项
    **/
    @JSONField(name="pre_payment")
    private String pre_payment;
    /**
     *    应付票据
    **/
    @JSONField(name="bill_payable")
    private String bill_payable;
    /**
     *    流动负债合计
    **/
    @JSONField(name="total_current_liab")
    private String total_current_liab;
    /**
     *    季度
    **/
    @JSONField(name="showYear")
    private String showYear;
    /**
     *    存货
    **/
    @JSONField(name="inventory")
    private String inventory;
    /**
     *    归属于母公司股东权益合计
    **/
    @JSONField(name="total_quity_atsopc")
    private String total_quity_atsopc;
    /**
     *    无形资产
    **/
    @JSONField(name="intangible_assets")
    private String intangible_assets;
    /**
     *    实收资本（或股本）
    **/
    @JSONField(name="shares")
    private String shares;
    /**
     *    其他应付款
    **/
    @JSONField(name="othr_payables")
    private String othr_payables;
    /**
     *    未分配利润
    **/
    @JSONField(name="undstrbtd_profit")
    private String undstrbtd_profit;
    /**
     *    其他非流动资产
    **/
    @JSONField(name="othr_noncurrent_assets")
    private String othr_noncurrent_assets;
    /**
     *    应付职工薪酬
    **/
    @JSONField(name="payroll_payable")
    private String payroll_payable;
    /**
     *    递延所得税资产
    **/
    @JSONField(name="dt_assets")
    private String dt_assets;
    /**
     *    在建工程
    **/
    @JSONField(name="construction_in_process")
    private String construction_in_process;
    /**
     *    负债和股东权益合计
    **/
    @JSONField(name="total_liab_and_holders_equity")
    private String total_liab_and_holders_equity;
    /**
     *    投资性房地产
    **/
    @JSONField(name="invest_property")
    private String invest_property;
    /**
     *    固定资产
    **/
    @JSONField(name="fixed_asset")
    private String fixed_asset;
    /**
     *    应付利息
    **/
    @JSONField(name="interest_payable")
    private String interest_payable;
    /**
     *    应收账款
    **/
    @JSONField(name="account_receivable")
    private String account_receivable;
    /**
     *    货币资金
    **/
    @JSONField(name="currency_funds")
    private String currency_funds;
    /**
     *    应付股利
    **/
    @JSONField(name="dividend_payable")
    private String dividend_payable;
    /**
     *    预收款项
    **/
    @JSONField(name="pre_receivable")
    private String pre_receivable;
    /**
     *    非流动资产合计
    **/
    @JSONField(name="total_noncurrent_assets")
    private String total_noncurrent_assets;
    /**
     *    其他流动资产
    **/
    @JSONField(name="othr_current_assets")
    private String othr_current_assets;
    /**
     *    盈余公积
    **/
    @JSONField(name="earned_surplus")
    private String earned_surplus;
    /**
     *    非流动负债合计
    **/
    @JSONField(name="total_noncurrent_liab")
    private String total_noncurrent_liab;
    /**
     *    应收票据
    **/
    @JSONField(name="bills_receivable")
    private String bills_receivable;
    /**
     *    其他应收款
    **/
    @JSONField(name="othr_receivables")
    private String othr_receivables;
    /**
     *    资本公积
    **/
    @JSONField(name="capital_reserve")
    private String capital_reserve;
    /**
     *    股东权益合计
    **/
    @JSONField(name="total_holders_equity")
    private String total_holders_equity;


    /**
    *   设置 应交税费
    **/
    public void setTax_payable(String tax_payable) {
      this.tax_payable = tax_payable;
    }
    /**
    *   获取 应交税费
    **/
    public String getTax_payable() {
      return tax_payable;
    }
    /**
    *   设置 长期待摊费用
    **/
    public void setLt_deferred_expense(String lt_deferred_expense) {
      this.lt_deferred_expense = lt_deferred_expense;
    }
    /**
    *   获取 长期待摊费用
    **/
    public String getLt_deferred_expense() {
      return lt_deferred_expense;
    }
    /**
    *   设置 流动资产合计
    **/
    public void setTotal_current_assets(String total_current_assets) {
      this.total_current_assets = total_current_assets;
    }
    /**
    *   获取 流动资产合计
    **/
    public String getTotal_current_assets() {
      return total_current_assets;
    }
    /**
    *   设置 资产总计
    **/
    public void setTotal_assets(String total_assets) {
      this.total_assets = total_assets;
    }
    /**
    *   获取 资产总计
    **/
    public String getTotal_assets() {
      return total_assets;
    }
    /**
    *   设置 预付款项
    **/
    public void setPre_payment(String pre_payment) {
      this.pre_payment = pre_payment;
    }
    /**
    *   获取 预付款项
    **/
    public String getPre_payment() {
      return pre_payment;
    }
    /**
    *   设置 应付票据
    **/
    public void setBill_payable(String bill_payable) {
      this.bill_payable = bill_payable;
    }
    /**
    *   获取 应付票据
    **/
    public String getBill_payable() {
      return bill_payable;
    }
    /**
    *   设置 流动负债合计
    **/
    public void setTotal_current_liab(String total_current_liab) {
      this.total_current_liab = total_current_liab;
    }
    /**
    *   获取 流动负债合计
    **/
    public String getTotal_current_liab() {
      return total_current_liab;
    }
    /**
    *   设置 季度
    **/
    public void setShowYear(String showYear) {
      this.showYear = showYear;
    }
    /**
    *   获取 季度
    **/
    public String getShowYear() {
      return showYear;
    }
    /**
    *   设置 存货
    **/
    public void setInventory(String inventory) {
      this.inventory = inventory;
    }
    /**
    *   获取 存货
    **/
    public String getInventory() {
      return inventory;
    }
    /**
    *   设置 归属于母公司股东权益合计
    **/
    public void setTotal_quity_atsopc(String total_quity_atsopc) {
      this.total_quity_atsopc = total_quity_atsopc;
    }
    /**
    *   获取 归属于母公司股东权益合计
    **/
    public String getTotal_quity_atsopc() {
      return total_quity_atsopc;
    }
    /**
    *   设置 无形资产
    **/
    public void setIntangible_assets(String intangible_assets) {
      this.intangible_assets = intangible_assets;
    }
    /**
    *   获取 无形资产
    **/
    public String getIntangible_assets() {
      return intangible_assets;
    }
    /**
    *   设置 实收资本（或股本）
    **/
    public void setShares(String shares) {
      this.shares = shares;
    }
    /**
    *   获取 实收资本（或股本）
    **/
    public String getShares() {
      return shares;
    }
    /**
    *   设置 其他应付款
    **/
    public void setOthr_payables(String othr_payables) {
      this.othr_payables = othr_payables;
    }
    /**
    *   获取 其他应付款
    **/
    public String getOthr_payables() {
      return othr_payables;
    }
    /**
    *   设置 未分配利润
    **/
    public void setUndstrbtd_profit(String undstrbtd_profit) {
      this.undstrbtd_profit = undstrbtd_profit;
    }
    /**
    *   获取 未分配利润
    **/
    public String getUndstrbtd_profit() {
      return undstrbtd_profit;
    }
    /**
    *   设置 其他非流动资产
    **/
    public void setOthr_noncurrent_assets(String othr_noncurrent_assets) {
      this.othr_noncurrent_assets = othr_noncurrent_assets;
    }
    /**
    *   获取 其他非流动资产
    **/
    public String getOthr_noncurrent_assets() {
      return othr_noncurrent_assets;
    }
    /**
    *   设置 应付职工薪酬
    **/
    public void setPayroll_payable(String payroll_payable) {
      this.payroll_payable = payroll_payable;
    }
    /**
    *   获取 应付职工薪酬
    **/
    public String getPayroll_payable() {
      return payroll_payable;
    }
    /**
    *   设置 递延所得税资产
    **/
    public void setDt_assets(String dt_assets) {
      this.dt_assets = dt_assets;
    }
    /**
    *   获取 递延所得税资产
    **/
    public String getDt_assets() {
      return dt_assets;
    }
    /**
    *   设置 在建工程
    **/
    public void setConstruction_in_process(String construction_in_process) {
      this.construction_in_process = construction_in_process;
    }
    /**
    *   获取 在建工程
    **/
    public String getConstruction_in_process() {
      return construction_in_process;
    }
    /**
    *   设置 负债和股东权益合计
    **/
    public void setTotal_liab_and_holders_equity(String total_liab_and_holders_equity) {
      this.total_liab_and_holders_equity = total_liab_and_holders_equity;
    }
    /**
    *   获取 负债和股东权益合计
    **/
    public String getTotal_liab_and_holders_equity() {
      return total_liab_and_holders_equity;
    }
    /**
    *   设置 投资性房地产
    **/
    public void setInvest_property(String invest_property) {
      this.invest_property = invest_property;
    }
    /**
    *   获取 投资性房地产
    **/
    public String getInvest_property() {
      return invest_property;
    }
    /**
    *   设置 固定资产
    **/
    public void setFixed_asset(String fixed_asset) {
      this.fixed_asset = fixed_asset;
    }
    /**
    *   获取 固定资产
    **/
    public String getFixed_asset() {
      return fixed_asset;
    }
    /**
    *   设置 应付利息
    **/
    public void setInterest_payable(String interest_payable) {
      this.interest_payable = interest_payable;
    }
    /**
    *   获取 应付利息
    **/
    public String getInterest_payable() {
      return interest_payable;
    }
    /**
    *   设置 应收账款
    **/
    public void setAccount_receivable(String account_receivable) {
      this.account_receivable = account_receivable;
    }
    /**
    *   获取 应收账款
    **/
    public String getAccount_receivable() {
      return account_receivable;
    }
    /**
    *   设置 货币资金
    **/
    public void setCurrency_funds(String currency_funds) {
      this.currency_funds = currency_funds;
    }
    /**
    *   获取 货币资金
    **/
    public String getCurrency_funds() {
      return currency_funds;
    }
    /**
    *   设置 应付股利
    **/
    public void setDividend_payable(String dividend_payable) {
      this.dividend_payable = dividend_payable;
    }
    /**
    *   获取 应付股利
    **/
    public String getDividend_payable() {
      return dividend_payable;
    }
    /**
    *   设置 预收款项
    **/
    public void setPre_receivable(String pre_receivable) {
      this.pre_receivable = pre_receivable;
    }
    /**
    *   获取 预收款项
    **/
    public String getPre_receivable() {
      return pre_receivable;
    }
    /**
    *   设置 非流动资产合计
    **/
    public void setTotal_noncurrent_assets(String total_noncurrent_assets) {
      this.total_noncurrent_assets = total_noncurrent_assets;
    }
    /**
    *   获取 非流动资产合计
    **/
    public String getTotal_noncurrent_assets() {
      return total_noncurrent_assets;
    }
    /**
    *   设置 其他流动资产
    **/
    public void setOthr_current_assets(String othr_current_assets) {
      this.othr_current_assets = othr_current_assets;
    }
    /**
    *   获取 其他流动资产
    **/
    public String getOthr_current_assets() {
      return othr_current_assets;
    }
    /**
    *   设置 盈余公积
    **/
    public void setEarned_surplus(String earned_surplus) {
      this.earned_surplus = earned_surplus;
    }
    /**
    *   获取 盈余公积
    **/
    public String getEarned_surplus() {
      return earned_surplus;
    }
    /**
    *   设置 非流动负债合计
    **/
    public void setTotal_noncurrent_liab(String total_noncurrent_liab) {
      this.total_noncurrent_liab = total_noncurrent_liab;
    }
    /**
    *   获取 非流动负债合计
    **/
    public String getTotal_noncurrent_liab() {
      return total_noncurrent_liab;
    }
    /**
    *   设置 应收票据
    **/
    public void setBills_receivable(String bills_receivable) {
      this.bills_receivable = bills_receivable;
    }
    /**
    *   获取 应收票据
    **/
    public String getBills_receivable() {
      return bills_receivable;
    }
    /**
    *   设置 其他应收款
    **/
    public void setOthr_receivables(String othr_receivables) {
      this.othr_receivables = othr_receivables;
    }
    /**
    *   获取 其他应收款
    **/
    public String getOthr_receivables() {
      return othr_receivables;
    }
    /**
    *   设置 资本公积
    **/
    public void setCapital_reserve(String capital_reserve) {
      this.capital_reserve = capital_reserve;
    }
    /**
    *   获取 资本公积
    **/
    public String getCapital_reserve() {
      return capital_reserve;
    }
    /**
    *   设置 股东权益合计
    **/
    public void setTotal_holders_equity(String total_holders_equity) {
      this.total_holders_equity = total_holders_equity;
    }
    /**
    *   获取 股东权益合计
    **/
    public String getTotal_holders_equity() {
      return total_holders_equity;
    }



}

