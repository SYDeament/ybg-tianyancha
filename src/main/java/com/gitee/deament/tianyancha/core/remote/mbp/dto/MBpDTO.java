package com.gitee.deament.tianyancha.core.remote.mbp.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*企业招聘-百聘
* 可以通过公司名称或ID获取企业招聘相关信息，企业招聘相关信息包括发布日期、招聘职位、月薪、学历、地区等字段的详细信息
* /services/open/m/bp/employments/2.0
*@author deament
**/

@TYCURL(value="/services/open/m/bp/employments/2.0")
public class MBpDTO implements Serializable{

    /**
     *    公司名
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数，默认20，最大20
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    当前页（默认1，最大100）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数，默认20，最大20
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数，默认20，最大20
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 当前页（默认1，最大100）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1，最大100）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

