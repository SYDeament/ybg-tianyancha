package com.gitee.deament.tianyancha.core.remote.cbic.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbic.entity.CbIc;
import com.gitee.deament.tianyancha.core.remote.cbic.dto.CbIcDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*工商信息
* 可以通过公司名称或ID获取包含企业简介、企业基本信息、主要人员、股东信息、对外投资、分支机构等维度的相关信息
* /services/open/cb/ic/2.0
*@author deament
**/

public interface CbIcRequest extends BaseRequest<CbIcDTO ,CbIc>{

}

