package com.gitee.deament.tianyancha.core.remote.icholderchange.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icholderchange.entity.IcHolderChange;
import com.gitee.deament.tianyancha.core.remote.icholderchange.dto.IcHolderChangeDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*公司公示-股权变更
* 可以通过公司名称或ID获取企业股权变更信息，股权变更包括变更前后的股东名、变更时间等字段的详细信息
* /services/open/ic/holderChange/2.0
*@author deament
**/

public interface IcHolderChangeRequest extends BaseRequest<IcHolderChangeDTO ,IcHolderChange>{

}

