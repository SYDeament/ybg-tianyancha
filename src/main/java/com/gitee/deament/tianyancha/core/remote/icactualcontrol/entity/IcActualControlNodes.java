package com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*疑似实际控制人
* 属于IcActualControl
* /services/open/ic/actualControl/2.0
*@author deament
**/

public class IcActualControlNodes implements Serializable{

    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    
    **/
    @JSONField(name="properties")
    private IcActualControlProperties properties;
    /**
     *    
    **/
    @JSONField(name="labels")
    private List<String> labels;


    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 
    **/
    public void setProperties(IcActualControlProperties properties) {
      this.properties = properties;
    }
    /**
    *   获取 
    **/
    public IcActualControlProperties getProperties() {
      return properties;
    }
    /**
    *   设置 
    **/
    public void setLabels(List<String> labels) {
      this.labels = labels;
    }
    /**
    *   获取 
    **/
    public List<String> getLabels() {
      return labels;
    }



}

