package com.gitee.deament.tianyancha.core.remote.oipublicinvest.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oipublicinvest.entity.OiPublicInvest;
import com.gitee.deament.tianyancha.core.remote.oipublicinvest.dto.OiPublicInvestDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*公开投资事件
* 可以通过投资机构名称获取公开投资产品，包括产品名称、所属公司、参与轮次、投资时间、投资金额、产品介绍等字段的详细信息
* /services/open/oi/publicInvest/2.0
*@author deament
**/

public interface OiPublicInvestRequest extends BaseRequest<OiPublicInvestDTO ,OiPublicInvest>{

}

