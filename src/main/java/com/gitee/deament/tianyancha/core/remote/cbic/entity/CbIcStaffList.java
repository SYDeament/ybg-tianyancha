package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcStaffList implements Serializable{

    /**
     *    主要人员职位
    **/
    @JSONField(name="staffTypeName")
    private String staffTypeName;
    /**
     *    主要人员名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    graphId
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    主要人员类型 1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    职位
    **/
    @JSONField(name="typeJoin")
    private List<String> typeJoin;


    /**
    *   设置 主要人员职位
    **/
    public void setStaffTypeName(String staffTypeName) {
      this.staffTypeName = staffTypeName;
    }
    /**
    *   获取 主要人员职位
    **/
    public String getStaffTypeName() {
      return staffTypeName;
    }
    /**
    *   设置 主要人员名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 主要人员名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 graphId
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 graphId
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 主要人员类型 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 主要人员类型 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 职位
    **/
    public void setTypeJoin(List<String> typeJoin) {
      this.typeJoin = typeJoin;
    }
    /**
    *   获取 职位
    **/
    public List<String> getTypeJoin() {
      return typeJoin;
    }



}

