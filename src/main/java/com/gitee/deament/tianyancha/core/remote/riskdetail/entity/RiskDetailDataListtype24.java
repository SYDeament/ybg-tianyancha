package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype24 implements Serializable{

    /**
     *    执行情况
    **/
    @JSONField(name="execution")
    private String execution;
    /**
     *    执法措施
    **/
    @JSONField(name="punish_content")
    private String punish_content;
    /**
     *    处罚单位
    **/
    @JSONField(name="punish_department")
    private String punish_department;
    /**
     *    处罚时间
    **/
    @JSONField(name="publish_time")
    private String publish_time;
    /**
     *    执法依据
    **/
    @JSONField(name="punish_basis")
    private String punish_basis;
    /**
     *    执法依据
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    决定文书号
    **/
    @JSONField(name="punish_number")
    private String punish_number;
    /**
     *    违法行为
    **/
    @JSONField(name="punish_reason")
    private String punish_reason;
    /**
     *    违反法律
    **/
    @JSONField(name="lawbreaking")
    private String lawbreaking;


    /**
    *   设置 执行情况
    **/
    public void setExecution(String execution) {
      this.execution = execution;
    }
    /**
    *   获取 执行情况
    **/
    public String getExecution() {
      return execution;
    }
    /**
    *   设置 执法措施
    **/
    public void setPunish_content(String punish_content) {
      this.punish_content = punish_content;
    }
    /**
    *   获取 执法措施
    **/
    public String getPunish_content() {
      return punish_content;
    }
    /**
    *   设置 处罚单位
    **/
    public void setPunish_department(String punish_department) {
      this.punish_department = punish_department;
    }
    /**
    *   获取 处罚单位
    **/
    public String getPunish_department() {
      return punish_department;
    }
    /**
    *   设置 处罚时间
    **/
    public void setPublish_time(String publish_time) {
      this.publish_time = publish_time;
    }
    /**
    *   获取 处罚时间
    **/
    public String getPublish_time() {
      return publish_time;
    }
    /**
    *   设置 执法依据
    **/
    public void setPunish_basis(String punish_basis) {
      this.punish_basis = punish_basis;
    }
    /**
    *   获取 执法依据
    **/
    public String getPunish_basis() {
      return punish_basis;
    }
    /**
    *   设置 执法依据
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 执法依据
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 决定文书号
    **/
    public void setPunish_number(String punish_number) {
      this.punish_number = punish_number;
    }
    /**
    *   获取 决定文书号
    **/
    public String getPunish_number() {
      return punish_number;
    }
    /**
    *   设置 违法行为
    **/
    public void setPunish_reason(String punish_reason) {
      this.punish_reason = punish_reason;
    }
    /**
    *   获取 违法行为
    **/
    public String getPunish_reason() {
      return punish_reason;
    }
    /**
    *   设置 违反法律
    **/
    public void setLawbreaking(String lawbreaking) {
      this.lawbreaking = lawbreaking;
    }
    /**
    *   获取 违反法律
    **/
    public String getLawbreaking() {
      return lawbreaking;
    }



}

