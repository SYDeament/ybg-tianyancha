package com.gitee.deament.tianyancha.core.remote.mlandpublicity.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*地块公示详情
* 可以通过地块公示ID获取企业地块公示详情，企业地块公示详情包括地块位置、土地用途、面积、发布机关、联系方式等字段的详细信息
* /services/open/m/landPublicity/detail/2.0
*@author deament
**/

@TYCURL(value="/services/open/m/landPublicity/detail/2.0")
public class MLandPublicityDTO implements Serializable{

    /**
     *    公示id
     *
    **/
    @ParamRequire(require = true)
    private Long id;


    /**
    *   设置 公示id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公示id
    **/
    public Long getId() {
      return id;
    }



}

