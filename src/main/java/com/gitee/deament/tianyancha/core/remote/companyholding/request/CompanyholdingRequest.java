package com.gitee.deament.tianyancha.core.remote.companyholding.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.companyholding.entity.Companyholding;
import com.gitee.deament.tianyancha.core.remote.companyholding.dto.CompanyholdingDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*实际控制权
* 可以通过公司名称或ID获取股权向下穿透后识别的当前企业实际控制的企业，包括对应公司名称或ID、出资比例、投资链等
* /services/v4/open/companyholding
*@author deament
**/

public interface CompanyholdingRequest extends BaseRequest<CompanyholdingDTO ,Companyholding>{

}

