package com.gitee.deament.tianyancha.core.remote.pflegalopinion.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*法律意见书信息
* 可以通过公司名称或ID获取相关私募基金法律意见书信息，包括法律意见书状态等
* /services/open/pf/legalOpinion/2.0
*@author deament
**/

@TYCURL(value="/services/open/pf/legalOpinion/2.0")
public class PfLegalOpinionDTO implements Serializable{

    /**
     *    公司名称
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 公司名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

