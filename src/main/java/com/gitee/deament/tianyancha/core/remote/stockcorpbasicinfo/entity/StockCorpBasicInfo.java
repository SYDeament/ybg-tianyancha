package com.gitee.deament.tianyancha.core.remote.stockcorpbasicinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*重要人员
* 可以通过公司名称或ID获取上市公司重要人员信息，重要人员信息包括总经理、法人代表、董秘、董事长、证券事务代表、独立董事等
* /services/open/stock/corpBasicInfo/2.0
*@author deament
**/

public class StockCorpBasicInfo implements Serializable{

    /**
     *    办公地址
    **/
    @JSONField(name="office_address_cn")
    private String office_address_cn;
    /**
     *    法人
    **/
    @JSONField(name="legal_representative")
    private String legal_representative;
    /**
     *    证券事务代表
    **/
    @JSONField(name="sec_representative")
    private String sec_representative;
    /**
     *    董事长
    **/
    @JSONField(name="chairman")
    private String chairman;
    /**
     *    注册号
    **/
    @JSONField(name="business_reg_num")
    private String business_reg_num;
    /**
     *    成立日期
    **/
    @JSONField(name="established_date")
    private Long established_date;
    /**
     *    董秘
    **/
    @JSONField(name="secretary")
    private String secretary;
    /**
     *    总经理
    **/
    @JSONField(name="general_manager")
    private String general_manager;
    /**
     *    英文名
    **/
    @JSONField(name="org_name_en")
    private String org_name_en;
    /**
     *    人员规模
    **/
    @JSONField(name="staff_num")
    private Integer staff_num;
    /**
     *    企业名
    **/
    @JSONField(name="org_name_cn")
    private String org_name_cn;
    /**
     *    注册地址
    **/
    @JSONField(name="reg_address_cn")
    private String reg_address_cn;
    /**
     *    企业id
    **/
    @JSONField(name="cid")
    private Integer cid;
    /**
     *    独立董事
    **/
    @JSONField(name="independentDirector")
    private String independentDirector;


    /**
    *   设置 办公地址
    **/
    public void setOffice_address_cn(String office_address_cn) {
      this.office_address_cn = office_address_cn;
    }
    /**
    *   获取 办公地址
    **/
    public String getOffice_address_cn() {
      return office_address_cn;
    }
    /**
    *   设置 法人
    **/
    public void setLegal_representative(String legal_representative) {
      this.legal_representative = legal_representative;
    }
    /**
    *   获取 法人
    **/
    public String getLegal_representative() {
      return legal_representative;
    }
    /**
    *   设置 证券事务代表
    **/
    public void setSec_representative(String sec_representative) {
      this.sec_representative = sec_representative;
    }
    /**
    *   获取 证券事务代表
    **/
    public String getSec_representative() {
      return sec_representative;
    }
    /**
    *   设置 董事长
    **/
    public void setChairman(String chairman) {
      this.chairman = chairman;
    }
    /**
    *   获取 董事长
    **/
    public String getChairman() {
      return chairman;
    }
    /**
    *   设置 注册号
    **/
    public void setBusiness_reg_num(String business_reg_num) {
      this.business_reg_num = business_reg_num;
    }
    /**
    *   获取 注册号
    **/
    public String getBusiness_reg_num() {
      return business_reg_num;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstablished_date(Long established_date) {
      this.established_date = established_date;
    }
    /**
    *   获取 成立日期
    **/
    public Long getEstablished_date() {
      return established_date;
    }
    /**
    *   设置 董秘
    **/
    public void setSecretary(String secretary) {
      this.secretary = secretary;
    }
    /**
    *   获取 董秘
    **/
    public String getSecretary() {
      return secretary;
    }
    /**
    *   设置 总经理
    **/
    public void setGeneral_manager(String general_manager) {
      this.general_manager = general_manager;
    }
    /**
    *   获取 总经理
    **/
    public String getGeneral_manager() {
      return general_manager;
    }
    /**
    *   设置 英文名
    **/
    public void setOrg_name_en(String org_name_en) {
      this.org_name_en = org_name_en;
    }
    /**
    *   获取 英文名
    **/
    public String getOrg_name_en() {
      return org_name_en;
    }
    /**
    *   设置 人员规模
    **/
    public void setStaff_num(Integer staff_num) {
      this.staff_num = staff_num;
    }
    /**
    *   获取 人员规模
    **/
    public Integer getStaff_num() {
      return staff_num;
    }
    /**
    *   设置 企业名
    **/
    public void setOrg_name_cn(String org_name_cn) {
      this.org_name_cn = org_name_cn;
    }
    /**
    *   获取 企业名
    **/
    public String getOrg_name_cn() {
      return org_name_cn;
    }
    /**
    *   设置 注册地址
    **/
    public void setReg_address_cn(String reg_address_cn) {
      this.reg_address_cn = reg_address_cn;
    }
    /**
    *   获取 注册地址
    **/
    public String getReg_address_cn() {
      return reg_address_cn;
    }
    /**
    *   设置 企业id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 企业id
    **/
    public Integer getCid() {
      return cid;
    }
    /**
    *   设置 独立董事
    **/
    public void setIndependentDirector(String independentDirector) {
      this.independentDirector = independentDirector;
    }
    /**
    *   获取 独立董事
    **/
    public String getIndependentDirector() {
      return independentDirector;
    }



}

