package com.gitee.deament.tianyancha.core.remote.icannualreport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业年报
* 属于IcAnnualreport
* /services/open/ic/annualreport/2.0
*@author deament
**/

public class IcAnnualreportBaseInfo implements Serializable{

    /**
     *    从业人数
    **/
    @JSONField(name="employeeNum")
    private String employeeNum;
    /**
     *    资产总额
    **/
    @JSONField(name="totalAssets")
    private String totalAssets;
    /**
     *    利润总额
    **/
    @JSONField(name="totalProfit")
    private String totalProfit;
    /**
     *    负债总额
    **/
    @JSONField(name="totalLiability")
    private String totalLiability;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    邮编
    **/
    @JSONField(name="postcode")
    private String postcode;
    /**
     *    销售总额(营业总收入)
    **/
    @JSONField(name="totalSales")
    private String totalSales;
    /**
     *    经营者名称
    **/
    @JSONField(name="operatorName")
    private String operatorName;
    /**
     *    净利润
    **/
    @JSONField(name="retainedProfit")
    private String retainedProfit;
    /**
     *    纳税总额
    **/
    @JSONField(name="totalTax")
    private String totalTax;
    /**
     *    年份
    **/
    @JSONField(name="reportYear")
    private String reportYear;
    /**
     *    注册码
    **/
    @JSONField(name="regNumber")
    private String regNumber;
    /**
     *    所有者权益合计
    **/
    @JSONField(name="totalEquity")
    private String totalEquity;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    企业联系方式
    **/
    @JSONField(name="phoneNumber")
    private String phoneNumber;
    /**
     *    企业通信地址
    **/
    @JSONField(name="postalAddress")
    private String postalAddress;
    /**
     *    主营业务收入
    **/
    @JSONField(name="primeBusProfit")
    private String primeBusProfit;
    /**
     *    经营状态
    **/
    @JSONField(name="manageState")
    private String manageState;
    /**
     *    邮箱
    **/
    @JSONField(name="email")
    private String email;


    /**
    *   设置 从业人数
    **/
    public void setEmployeeNum(String employeeNum) {
      this.employeeNum = employeeNum;
    }
    /**
    *   获取 从业人数
    **/
    public String getEmployeeNum() {
      return employeeNum;
    }
    /**
    *   设置 资产总额
    **/
    public void setTotalAssets(String totalAssets) {
      this.totalAssets = totalAssets;
    }
    /**
    *   获取 资产总额
    **/
    public String getTotalAssets() {
      return totalAssets;
    }
    /**
    *   设置 利润总额
    **/
    public void setTotalProfit(String totalProfit) {
      this.totalProfit = totalProfit;
    }
    /**
    *   获取 利润总额
    **/
    public String getTotalProfit() {
      return totalProfit;
    }
    /**
    *   设置 负债总额
    **/
    public void setTotalLiability(String totalLiability) {
      this.totalLiability = totalLiability;
    }
    /**
    *   获取 负债总额
    **/
    public String getTotalLiability() {
      return totalLiability;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 邮编
    **/
    public void setPostcode(String postcode) {
      this.postcode = postcode;
    }
    /**
    *   获取 邮编
    **/
    public String getPostcode() {
      return postcode;
    }
    /**
    *   设置 销售总额(营业总收入)
    **/
    public void setTotalSales(String totalSales) {
      this.totalSales = totalSales;
    }
    /**
    *   获取 销售总额(营业总收入)
    **/
    public String getTotalSales() {
      return totalSales;
    }
    /**
    *   设置 经营者名称
    **/
    public void setOperatorName(String operatorName) {
      this.operatorName = operatorName;
    }
    /**
    *   获取 经营者名称
    **/
    public String getOperatorName() {
      return operatorName;
    }
    /**
    *   设置 净利润
    **/
    public void setRetainedProfit(String retainedProfit) {
      this.retainedProfit = retainedProfit;
    }
    /**
    *   获取 净利润
    **/
    public String getRetainedProfit() {
      return retainedProfit;
    }
    /**
    *   设置 纳税总额
    **/
    public void setTotalTax(String totalTax) {
      this.totalTax = totalTax;
    }
    /**
    *   获取 纳税总额
    **/
    public String getTotalTax() {
      return totalTax;
    }
    /**
    *   设置 年份
    **/
    public void setReportYear(String reportYear) {
      this.reportYear = reportYear;
    }
    /**
    *   获取 年份
    **/
    public String getReportYear() {
      return reportYear;
    }
    /**
    *   设置 注册码
    **/
    public void setRegNumber(String regNumber) {
      this.regNumber = regNumber;
    }
    /**
    *   获取 注册码
    **/
    public String getRegNumber() {
      return regNumber;
    }
    /**
    *   设置 所有者权益合计
    **/
    public void setTotalEquity(String totalEquity) {
      this.totalEquity = totalEquity;
    }
    /**
    *   获取 所有者权益合计
    **/
    public String getTotalEquity() {
      return totalEquity;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 企业联系方式
    **/
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    /**
    *   获取 企业联系方式
    **/
    public String getPhoneNumber() {
      return phoneNumber;
    }
    /**
    *   设置 企业通信地址
    **/
    public void setPostalAddress(String postalAddress) {
      this.postalAddress = postalAddress;
    }
    /**
    *   获取 企业通信地址
    **/
    public String getPostalAddress() {
      return postalAddress;
    }
    /**
    *   设置 主营业务收入
    **/
    public void setPrimeBusProfit(String primeBusProfit) {
      this.primeBusProfit = primeBusProfit;
    }
    /**
    *   获取 主营业务收入
    **/
    public String getPrimeBusProfit() {
      return primeBusProfit;
    }
    /**
    *   设置 经营状态
    **/
    public void setManageState(String manageState) {
      this.manageState = manageState;
    }
    /**
    *   获取 经营状态
    **/
    public String getManageState() {
      return manageState;
    }
    /**
    *   设置 邮箱
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱
    **/
    public String getEmail() {
      return email;
    }



}

