package com.gitee.deament.tianyancha.core.remote.hicourt.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hicourt.entity.HiCourt;
import com.gitee.deament.tianyancha.core.remote.hicourt.dto.HiCourtDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*历史法院公告
* 可以通过公司名称或ID获取企业历史的法院公告，历史法院公告信息包括执行法院、案件内容、公告类型、刊登日期、公司名、当事人等字段信息
* /services/open/hi/court/2.0
*@author deament
**/

public interface HiCourtRequest extends BaseRequest<HiCourtDTO ,HiCourt>{

}

