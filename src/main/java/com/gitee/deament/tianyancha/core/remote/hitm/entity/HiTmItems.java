package com.gitee.deament.tianyancha.core.remote.hitm.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史商标信息
* 属于HiTm
* /services/open/hi/tm/2.0
*@author deament
**/

public class HiTmItems implements Serializable{

    /**
     *    注册号
    **/
    @JSONField(name="regNo")
    private String regNo;
    /**
     *    商标名
    **/
    @JSONField(name="tmName")
    private String tmName;
    /**
     *    图标
    **/
    @JSONField(name="tmPic")
    private String tmPic;
    /**
     *    商标流程
    **/
    @JSONField(name="tmFlow")
    private String tmFlow;
    /**
     *    申请日期
    **/
    @JSONField(name="appDate")
    private String appDate;
    /**
     *    国际分类
    **/
    @JSONField(name="tmClass")
    private String tmClass;
    /**
     *    商标id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    类型
    **/
    @JSONField(name="intCls")
    private String intCls;
    /**
     *    申请人
    **/
    @JSONField(name="applicantCn")
    private String applicantCn;
    /**
     *    状态
    **/
    @JSONField(name="status")
    private String status;


    /**
    *   设置 注册号
    **/
    public void setRegNo(String regNo) {
      this.regNo = regNo;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNo() {
      return regNo;
    }
    /**
    *   设置 商标名
    **/
    public void setTmName(String tmName) {
      this.tmName = tmName;
    }
    /**
    *   获取 商标名
    **/
    public String getTmName() {
      return tmName;
    }
    /**
    *   设置 图标
    **/
    public void setTmPic(String tmPic) {
      this.tmPic = tmPic;
    }
    /**
    *   获取 图标
    **/
    public String getTmPic() {
      return tmPic;
    }
    /**
    *   设置 商标流程
    **/
    public void setTmFlow(String tmFlow) {
      this.tmFlow = tmFlow;
    }
    /**
    *   获取 商标流程
    **/
    public String getTmFlow() {
      return tmFlow;
    }
    /**
    *   设置 申请日期
    **/
    public void setAppDate(String appDate) {
      this.appDate = appDate;
    }
    /**
    *   获取 申请日期
    **/
    public String getAppDate() {
      return appDate;
    }
    /**
    *   设置 国际分类
    **/
    public void setTmClass(String tmClass) {
      this.tmClass = tmClass;
    }
    /**
    *   获取 国际分类
    **/
    public String getTmClass() {
      return tmClass;
    }
    /**
    *   设置 商标id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 商标id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 类型
    **/
    public void setIntCls(String intCls) {
      this.intCls = intCls;
    }
    /**
    *   获取 类型
    **/
    public String getIntCls() {
      return intCls;
    }
    /**
    *   设置 申请人
    **/
    public void setApplicantCn(String applicantCn) {
      this.applicantCn = applicantCn;
    }
    /**
    *   获取 申请人
    **/
    public String getApplicantCn() {
      return applicantCn;
    }
    /**
    *   设置 状态
    **/
    public void setStatus(String status) {
      this.status = status;
    }
    /**
    *   获取 状态
    **/
    public String getStatus() {
      return status;
    }



}

