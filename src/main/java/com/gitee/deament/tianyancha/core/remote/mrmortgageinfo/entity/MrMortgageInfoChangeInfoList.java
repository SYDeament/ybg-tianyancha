package com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*动产抵押
* 属于MrMortgageInfo
* /services/open/mr/mortgageInfo/2.0
*@author deament
**/

public class MrMortgageInfoChangeInfoList implements Serializable{

    /**
     *    变更内容
    **/
    @JSONField(name="changeContent")
    private String changeContent;
    /**
     *    变更日期
    **/
    @JSONField(name="changeDate")
    private Long changeDate;


    /**
    *   设置 变更内容
    **/
    public void setChangeContent(String changeContent) {
      this.changeContent = changeContent;
    }
    /**
    *   获取 变更内容
    **/
    public String getChangeContent() {
      return changeContent;
    }
    /**
    *   设置 变更日期
    **/
    public void setChangeDate(Long changeDate) {
      this.changeDate = changeDate;
    }
    /**
    *   获取 变更日期
    **/
    public Long getChangeDate() {
      return changeDate;
    }



}

