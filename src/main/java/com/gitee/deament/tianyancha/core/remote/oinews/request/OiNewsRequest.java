package com.gitee.deament.tianyancha.core.remote.oinews.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oinews.entity.OiNews;
import com.gitee.deament.tianyancha.core.remote.oinews.dto.OiNewsDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*相关新闻
* 可以通过投资机构名称获取相关新闻舆情信息，包括发稿媒体、新闻标题、发布时间、网址链接等字段的详细信息
* /services/open/oi/news/2.0
*@author deament
**/

public interface OiNewsRequest extends BaseRequest<OiNewsDTO ,OiNews>{

}

