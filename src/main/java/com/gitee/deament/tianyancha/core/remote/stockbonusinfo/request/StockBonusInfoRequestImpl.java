package com.gitee.deament.tianyancha.core.remote.stockbonusinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockbonusinfo.entity.StockBonusInfo;
import com.gitee.deament.tianyancha.core.remote.stockbonusinfo.dto.StockBonusInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*分红情况
* 可以通过公司名称或ID获取上市公司分红情况信息，分红情况信息包括董事会日期、股东大会日期、实施日期、分红方案说明、分红率等
* /services/open/stock/bonusInfo/2.0
*@author deament
**/
@Component("stockBonusInfoRequestImpl")
public class StockBonusInfoRequestImpl extends BaseRequestImpl<StockBonusInfo,StockBonusInfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/bonusInfo/2.0";
    }
}

