package com.gitee.deament.tianyancha.core.remote.icannualreport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业年报
* 属于IcAnnualreport
* /services/open/ic/annualreport/2.0
*@author deament
**/

public class IcAnnualreportOutboundInvestmentList implements Serializable{

    /**
     *    被投资公司
    **/
    @JSONField(name="outcompanyName")
    private String outcompanyName;
    /**
     *    年份
    **/
    @JSONField(name="reportYear")
    private String reportYear;
    /**
     *    社会统一信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    注册码
    **/
    @JSONField(name="regNum")
    private String regNum;
    /**
     *    被投资公司id
    **/
    @JSONField(name="clickId")
    private String clickId;
    /**
     *    1-人 2-公司
    **/
    @JSONField(name="type")
    private String type;


    /**
    *   设置 被投资公司
    **/
    public void setOutcompanyName(String outcompanyName) {
      this.outcompanyName = outcompanyName;
    }
    /**
    *   获取 被投资公司
    **/
    public String getOutcompanyName() {
      return outcompanyName;
    }
    /**
    *   设置 年份
    **/
    public void setReportYear(String reportYear) {
      this.reportYear = reportYear;
    }
    /**
    *   获取 年份
    **/
    public String getReportYear() {
      return reportYear;
    }
    /**
    *   设置 社会统一信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 社会统一信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 注册码
    **/
    public void setRegNum(String regNum) {
      this.regNum = regNum;
    }
    /**
    *   获取 注册码
    **/
    public String getRegNum() {
      return regNum;
    }
    /**
    *   设置 被投资公司id
    **/
    public void setClickId(String clickId) {
      this.clickId = clickId;
    }
    /**
    *   获取 被投资公司id
    **/
    public String getClickId() {
      return clickId;
    }
    /**
    *   设置 1-人 2-公司
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 1-人 2-公司
    **/
    public String getType() {
      return type;
    }



}

