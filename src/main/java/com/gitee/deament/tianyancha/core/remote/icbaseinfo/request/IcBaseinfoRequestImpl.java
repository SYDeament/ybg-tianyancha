package com.gitee.deament.tianyancha.core.remote.icbaseinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icbaseinfo.entity.IcBaseinfo;
import com.gitee.deament.tianyancha.core.remote.icbaseinfo.dto.IcBaseinfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*企业基本信息
* 可以通过公司名称或ID获取企业基本信息，企业基本信息包括公司名称或ID、类型、成立日期、经营状态、注册资本、法人、工商注册号、统一社会信用代码、组织机构代码、纳税人识别号等字段信息
* /services/open/ic/baseinfo/2.0
*@author deament
**/
@Component("icBaseinfoRequestImpl")
public class IcBaseinfoRequestImpl extends BaseRequestImpl<IcBaseinfo,IcBaseinfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ic/baseinfo/2.0";
    }
}

