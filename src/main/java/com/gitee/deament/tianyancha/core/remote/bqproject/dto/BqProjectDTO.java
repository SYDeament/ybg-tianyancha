package com.gitee.deament.tianyancha.core.remote.bqproject.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*建筑资质-工程项目详情
* 可以通过建筑资质工程项目ID获取建筑资质工程项目信息，包括项目编号、项目名称、项目属地、项目类别、建设单位、招投标、合同备案、施工许可等字段信息
* /services/open/bq/project/detail/2.0
*@author deament
**/

@TYCURL(value="/services/open/bq/project/detail/2.0")
public class BqProjectDTO implements Serializable{

    /**
     *    人员id
     *
    **/
    @ParamRequire(require = false)
    private String businessId;
    /**
     *    每页条数（默认20，最大20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    当前页（默认1）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 人员id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 人员id
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 每页条数（默认20，最大20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20，最大20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 当前页（默认1）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

