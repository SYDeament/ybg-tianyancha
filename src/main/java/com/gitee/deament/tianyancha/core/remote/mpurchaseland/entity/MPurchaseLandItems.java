package com.gitee.deament.tianyancha.core.remote.mpurchaseland.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*购地信息
* 属于MPurchaseLand
* /services/open/m/purchaseLand/2.0
*@author deament
**/

public class MPurchaseLandItems implements Serializable{

    /**
     *    项目位置
    **/
    @JSONField(name="projectLocation")
    private String projectLocation;
    /**
     *    面积(公顷)
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    合同签订日期
    **/
    @JSONField(name="contractDate")
    private Long contractDate;
    /**
     *    土地用途
    **/
    @JSONField(name="landUseType")
    private String landUseType;
    /**
     *    约定容积率下限
    **/
    @JSONField(name="contractedVolumeRate")
    private String contractedVolumeRate;
    /**
     *    成交价格(万元)
    **/
    @JSONField(name="transactionPrice")
    private String transactionPrice;
    /**
     *    分期支付约定
    **/
    @JSONField(name="instalmentPayment")
    private List<MPurchaseLandInstalmentPayment> instalmentPayment;
    /**
     *    约定容积率上限
    **/
    @JSONField(name="contractedVolumeRateCeiling")
    private String contractedVolumeRateCeiling;
    /**
     *    电子监管号
    **/
    @JSONField(name="electronicRegulatoryNumber")
    private String electronicRegulatoryNumber;
    /**
     *    约定交地时间
    **/
    @JSONField(name="committedTime")
    private Long committedTime;
    /**
     *    供地方式
    **/
    @JSONField(name="landSupplyMethod")
    private String landSupplyMethod;
    /**
     *    约定竣工时间
    **/
    @JSONField(name="scheduledCompletion")
    private Long scheduledCompletion;
    /**
     *    土地使用权年限
    **/
    @JSONField(name="landUsePeriod")
    private String landUsePeriod;
    /**
     *    土地使用权人
    **/
    @JSONField(name="landUseRightPerson")
    private String landUseRightPerson;
    /**
     *    公司列表
    **/
    @JSONField(name="companyList")
    private List<MPurchaseLandCompanyList> companyList;
    /**
     *    内链
    **/
    @JSONField(name="chainLink")
    private String chainLink;
    /**
     *    批准单位
    **/
    @JSONField(name="authority")
    private String authority;
    /**
     *    行政区
    **/
    @JSONField(name="district")
    private String district;
    /**
     *    土地级别
    **/
    @JSONField(name="landLevel")
    private String landLevel;
    /**
     *    行业分类
    **/
    @JSONField(name="category")
    private String category;
    /**
     *    项目名称
    **/
    @JSONField(name="projectName")
    private String projectName;
    /**
     *    约定开工时间
    **/
    @JSONField(name="agreementStartTime")
    private Long agreementStartTime;
    /**
     *    土地来源显示值，通过将土地来源实际值与土地面积比较来确定显示值
    **/
    @JSONField(name="landSourceView")
    private String landSourceView;


    /**
    *   设置 项目位置
    **/
    public void setProjectLocation(String projectLocation) {
      this.projectLocation = projectLocation;
    }
    /**
    *   获取 项目位置
    **/
    public String getProjectLocation() {
      return projectLocation;
    }
    /**
    *   设置 面积(公顷)
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 面积(公顷)
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 合同签订日期
    **/
    public void setContractDate(Long contractDate) {
      this.contractDate = contractDate;
    }
    /**
    *   获取 合同签订日期
    **/
    public Long getContractDate() {
      return contractDate;
    }
    /**
    *   设置 土地用途
    **/
    public void setLandUseType(String landUseType) {
      this.landUseType = landUseType;
    }
    /**
    *   获取 土地用途
    **/
    public String getLandUseType() {
      return landUseType;
    }
    /**
    *   设置 约定容积率下限
    **/
    public void setContractedVolumeRate(String contractedVolumeRate) {
      this.contractedVolumeRate = contractedVolumeRate;
    }
    /**
    *   获取 约定容积率下限
    **/
    public String getContractedVolumeRate() {
      return contractedVolumeRate;
    }
    /**
    *   设置 成交价格(万元)
    **/
    public void setTransactionPrice(String transactionPrice) {
      this.transactionPrice = transactionPrice;
    }
    /**
    *   获取 成交价格(万元)
    **/
    public String getTransactionPrice() {
      return transactionPrice;
    }
    /**
    *   设置 分期支付约定
    **/
    public void setInstalmentPayment(List<MPurchaseLandInstalmentPayment> instalmentPayment) {
      this.instalmentPayment = instalmentPayment;
    }
    /**
    *   获取 分期支付约定
    **/
    public List<MPurchaseLandInstalmentPayment> getInstalmentPayment() {
      return instalmentPayment;
    }
    /**
    *   设置 约定容积率上限
    **/
    public void setContractedVolumeRateCeiling(String contractedVolumeRateCeiling) {
      this.contractedVolumeRateCeiling = contractedVolumeRateCeiling;
    }
    /**
    *   获取 约定容积率上限
    **/
    public String getContractedVolumeRateCeiling() {
      return contractedVolumeRateCeiling;
    }
    /**
    *   设置 电子监管号
    **/
    public void setElectronicRegulatoryNumber(String electronicRegulatoryNumber) {
      this.electronicRegulatoryNumber = electronicRegulatoryNumber;
    }
    /**
    *   获取 电子监管号
    **/
    public String getElectronicRegulatoryNumber() {
      return electronicRegulatoryNumber;
    }
    /**
    *   设置 约定交地时间
    **/
    public void setCommittedTime(Long committedTime) {
      this.committedTime = committedTime;
    }
    /**
    *   获取 约定交地时间
    **/
    public Long getCommittedTime() {
      return committedTime;
    }
    /**
    *   设置 供地方式
    **/
    public void setLandSupplyMethod(String landSupplyMethod) {
      this.landSupplyMethod = landSupplyMethod;
    }
    /**
    *   获取 供地方式
    **/
    public String getLandSupplyMethod() {
      return landSupplyMethod;
    }
    /**
    *   设置 约定竣工时间
    **/
    public void setScheduledCompletion(Long scheduledCompletion) {
      this.scheduledCompletion = scheduledCompletion;
    }
    /**
    *   获取 约定竣工时间
    **/
    public Long getScheduledCompletion() {
      return scheduledCompletion;
    }
    /**
    *   设置 土地使用权年限
    **/
    public void setLandUsePeriod(String landUsePeriod) {
      this.landUsePeriod = landUsePeriod;
    }
    /**
    *   获取 土地使用权年限
    **/
    public String getLandUsePeriod() {
      return landUsePeriod;
    }
    /**
    *   设置 土地使用权人
    **/
    public void setLandUseRightPerson(String landUseRightPerson) {
      this.landUseRightPerson = landUseRightPerson;
    }
    /**
    *   获取 土地使用权人
    **/
    public String getLandUseRightPerson() {
      return landUseRightPerson;
    }
    /**
    *   设置 公司列表
    **/
    public void setCompanyList(List<MPurchaseLandCompanyList> companyList) {
      this.companyList = companyList;
    }
    /**
    *   获取 公司列表
    **/
    public List<MPurchaseLandCompanyList> getCompanyList() {
      return companyList;
    }
    /**
    *   设置 内链
    **/
    public void setChainLink(String chainLink) {
      this.chainLink = chainLink;
    }
    /**
    *   获取 内链
    **/
    public String getChainLink() {
      return chainLink;
    }
    /**
    *   设置 批准单位
    **/
    public void setAuthority(String authority) {
      this.authority = authority;
    }
    /**
    *   获取 批准单位
    **/
    public String getAuthority() {
      return authority;
    }
    /**
    *   设置 行政区
    **/
    public void setDistrict(String district) {
      this.district = district;
    }
    /**
    *   获取 行政区
    **/
    public String getDistrict() {
      return district;
    }
    /**
    *   设置 土地级别
    **/
    public void setLandLevel(String landLevel) {
      this.landLevel = landLevel;
    }
    /**
    *   获取 土地级别
    **/
    public String getLandLevel() {
      return landLevel;
    }
    /**
    *   设置 行业分类
    **/
    public void setCategory(String category) {
      this.category = category;
    }
    /**
    *   获取 行业分类
    **/
    public String getCategory() {
      return category;
    }
    /**
    *   设置 项目名称
    **/
    public void setProjectName(String projectName) {
      this.projectName = projectName;
    }
    /**
    *   获取 项目名称
    **/
    public String getProjectName() {
      return projectName;
    }
    /**
    *   设置 约定开工时间
    **/
    public void setAgreementStartTime(Long agreementStartTime) {
      this.agreementStartTime = agreementStartTime;
    }
    /**
    *   获取 约定开工时间
    **/
    public Long getAgreementStartTime() {
      return agreementStartTime;
    }
    /**
    *   设置 土地来源显示值，通过将土地来源实际值与土地面积比较来确定显示值
    **/
    public void setLandSourceView(String landSourceView) {
      this.landSourceView = landSourceView;
    }
    /**
    *   获取 土地来源显示值，通过将土地来源实际值与土地面积比较来确定显示值
    **/
    public String getLandSourceView() {
      return landSourceView;
    }



}

