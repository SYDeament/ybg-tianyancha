package com.gitee.deament.tianyancha.core.remote.mcertificate.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcertificate.entity.MCertificateDetail;
import com.gitee.deament.tianyancha.core.remote.mcertificate.entity.MCertificateItems;
/**
*资质证书
* 可以通过公司名称或ID获取企业资质证书信息，企业资质证书信息包括证书类型、证书编号、发证日期等字段的详细信息
* /services/open/m/certificate/2.0
*@author deament
**/

public class MCertificate implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MCertificateItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MCertificateItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MCertificateItems> getItems() {
      return items;
    }



}

