package com.gitee.deament.tianyancha.core.remote.stockcashflow.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockcashflow.entity.StockCashFlowCorpFinancialYears;
import com.gitee.deament.tianyancha.core.remote.stockcashflow.entity.StockCashFlowCorpCashFlow;
/**
*现金流量表
* 可以通过公司名称或ID获取上市公司现金流量表数据，现金流量表数据包括经营活动产生的现金流量、经营活动现金流小计、经营活动现金流出小计、经营活动产生的现金流量净额、投资活动产生的现金流量等
* /services/open/stock/cashFlow/2.0
*@author deament
**/

public class StockCashFlow implements Serializable{

    /**
     *    
    **/
    @JSONField(name="corpFinancialYears")
    private List<StockCashFlowCorpFinancialYears> corpFinancialYears;
    /**
     *    
    **/
    @JSONField(name="corpCashFlow")
    private List<StockCashFlowCorpCashFlow> corpCashFlow;


    /**
    *   设置 
    **/
    public void setCorpFinancialYears(List<StockCashFlowCorpFinancialYears> corpFinancialYears) {
      this.corpFinancialYears = corpFinancialYears;
    }
    /**
    *   获取 
    **/
    public List<StockCashFlowCorpFinancialYears> getCorpFinancialYears() {
      return corpFinancialYears;
    }
    /**
    *   设置 
    **/
    public void setCorpCashFlow(List<StockCashFlowCorpCashFlow> corpCashFlow) {
      this.corpCashFlow = corpCashFlow;
    }
    /**
    *   获取 
    **/
    public List<StockCashFlowCorpCashFlow> getCorpCashFlow() {
      return corpCashFlow;
    }



}

