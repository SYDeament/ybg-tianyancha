package com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlActualController;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlProperties;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlRelationships;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlProperties;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlNodes;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlP_0;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlProperties;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlRelationships;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlProperties;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlNodes;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlP_1;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControlPathMap;
/**
*疑似实际控制人
* 可以通过公司名称或ID获取股权向上穿透后识别的企业疑似实际控制人，包含（一名）疑似控制人姓名、层级关系等字段信息
* /services/open/ic/actualControl/2.0
*@author deament
**/

public class IcActualControl implements Serializable{

    /**
     *    实际控制人
    **/
    @JSONField(name="actualController")
    private IcActualControlActualController actualController;
    /**
     *    
    **/
    @JSONField(name="pathMap")
    private IcActualControlPathMap pathMap;
    /**
     *    占比
    **/
    @JSONField(name="ratio")
    private Integer ratio;


    /**
    *   设置 实际控制人
    **/
    public void setActualController(IcActualControlActualController actualController) {
      this.actualController = actualController;
    }
    /**
    *   获取 实际控制人
    **/
    public IcActualControlActualController getActualController() {
      return actualController;
    }
    /**
    *   设置 
    **/
    public void setPathMap(IcActualControlPathMap pathMap) {
      this.pathMap = pathMap;
    }
    /**
    *   获取 
    **/
    public IcActualControlPathMap getPathMap() {
      return pathMap;
    }
    /**
    *   设置 占比
    **/
    public void setRatio(Integer ratio) {
      this.ratio = ratio;
    }
    /**
    *   获取 占比
    **/
    public Integer getRatio() {
      return ratio;
    }



}

