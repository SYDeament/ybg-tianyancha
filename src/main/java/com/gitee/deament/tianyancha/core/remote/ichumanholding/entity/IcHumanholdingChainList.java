package com.gitee.deament.tianyancha.core.remote.ichumanholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*最终受益人
* 属于IcHumanholding
* /services/open/ic/humanholding/2.0
*@author deament
**/

public class IcHumanholdingChainList implements Serializable{

    /**
     *    姓名或者公司名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    人id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    human  company title percent
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    法人名称或公司名称
    **/
    @JSONField(name="value")
    private String value;
    /**
     *    备注
    **/
    @JSONField(name="info")
    private String info;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 姓名或者公司名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 姓名或者公司名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 人id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 人id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 human  company title percent
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 human  company title percent
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 法人名称或公司名称
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 法人名称或公司名称
    **/
    public String getValue() {
      return value;
    }
    /**
    *   设置 备注
    **/
    public void setInfo(String info) {
      this.info = info;
    }
    /**
    *   获取 备注
    **/
    public String getInfo() {
      return info;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

