package com.gitee.deament.tianyancha.core.remote.oistats.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oistats.entity.OiStats;
import com.gitee.deament.tianyancha.core.remote.oistats.dto.OiStatsDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*统计分析
* 可以通过投资机构名称获取投资机构投资统计分析，包括月份、数量、投资规模、投资行业等
* /services/open/oi/stats/2.0
*@author deament
**/
@Component("oiStatsRequestImpl")
public class OiStatsRequestImpl extends BaseRequestImpl<OiStats,OiStatsDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/oi/stats/2.0";
    }
}

