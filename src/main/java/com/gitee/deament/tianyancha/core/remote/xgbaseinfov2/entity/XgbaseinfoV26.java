package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*特殊企业基本信息
* 可以通过公司名称或ID获取特殊企业基本信息，包含香港公司、社会组织、律所、事业单位、基金会这些特殊企业，不同社会团体所呈现的信息维度不同
* /services/v4/open/xgbaseinfoV2
*@author deament
**/

public class XgbaseinfoV26 implements Serializable{

    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    业务主管单位
    **/
    @JSONField(name="businessUnit")
    private String businessUnit;
    /**
     *    email地址（无法识别的字段 请手动修改）
    **/
    @JSONField(name="emailList")
    private List<Object> emailList;
    /**
     *    负责人中担任过省部级工作人员数
    **/
    @JSONField(name="provinceWorkerNumber")
    private String provinceWorkerNumber;
    /**
     *    企业联系方式列表（无法识别的字段 请手动修改）
    **/
    @JSONField(name="phoneList")
    private List<Object> phoneList;
    /**
     *    无用
    **/
    @JSONField(name="baiduAuthURLWWW")
    private String baiduAuthURLWWW;
    /**
     *    
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    全职员工数量
    **/
    @JSONField(name="employeeNumber")
    private String employeeNumber;
    /**
     *    负责人中国家工作人员数
    **/
    @JSONField(name="nationalWorkerNumber")
    private String nationalWorkerNumber;
    /**
     *    成立时间
    **/
    @JSONField(name="establishTime")
    private Long establishTime;
    /**
     *    志愿者数量
    **/
    @JSONField(name="volunteerNumber")
    private String volunteerNumber;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    基金会id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    传真
    **/
    @JSONField(name="fax")
    private String fax;
    /**
     *    弃用
    **/
    @JSONField(name="isClaimed")
    private Integer isClaimed;
    /**
     *    1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    @JSONField(name="entityType")
    private Integer entityType;
    /**
     *    邮政编码
    **/
    @JSONField(name="postcode")
    private String postcode;
    /**
     *    是否有年报
    **/
    @JSONField(name="haveReport")
    private Boolean haveReport;
    /**
     *    业务范围
    **/
    @JSONField(name="businessScope")
    private String businessScope;
    /**
     *    电话
    **/
    @JSONField(name="telephone")
    private String telephone;
    /**
     *    企业联系方式
    **/
    @JSONField(name="phoneNumber")
    private String phoneNumber;
    /**
     *    关注领域
    **/
    @JSONField(name="field")
    private String field;
    /**
     *    评估等级
    **/
    @JSONField(name="grade")
    private String grade;
    /**
     *    基金名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    优惠资格类型
    **/
    @JSONField(name="preferentialQualificationType")
    private String preferentialQualificationType;
    /**
     *    数据来源
    **/
    @JSONField(name="dataSource")
    private String dataSource;
    /**
     *    基金会介绍
    **/
    @JSONField(name="baseInfo")
    private String baseInfo;
    /**
     *    基金会英文名称
    **/
    @JSONField(name="englishName")
    private String englishName;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    宗旨
    **/
    @JSONField(name="purpose")
    private String purpose;
    /**
     *    历史名
    **/
    @JSONField(name="historyName")
    private String historyName;
    /**
     *    弃用
    **/
    @JSONField(name="pricingPackage")
    private Integer pricingPackage;
    /**
     *    专项基金数
    **/
    @JSONField(name="specialFundNumber")
    private String specialFundNumber;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    秘书长
    **/
    @JSONField(name="secretary")
    private String secretary;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    微博(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="weibo")
    private String weibo;
    /**
     *    组织机构代码
    **/
    @JSONField(name="orgCode")
    private String orgCode;
    /**
     *    基金会范围
    **/
    @JSONField(name="scope")
    private String scope;
    /**
     *    无用
    **/
    @JSONField(name="baiduAuthURLWAP")
    private String baiduAuthURLWAP;
    /**
     *    登记部门
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    邮箱
    **/
    @JSONField(name="email")
    private String email;
    /**
     *    网址
    **/
    @JSONField(name="website")
    private String website;
    /**
     *    
    **/
    @JSONField(name="estiblishTime")
    private Integer estiblishTime;
    /**
     *    地址
    **/
    @JSONField(name="address")
    private String address;
    /**
     *    地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    企业网站
    **/
    @JSONField(name="websiteList")
    private String websiteList;
    /**
     *    法人id
    **/
    @JSONField(name="legalPersonId")
    private Integer legalPersonId;
    /**
     *    创建时间
    **/
    @JSONField(name="createTime")
    private Long createTime;
    /**
     *    公司地址
    **/
    @JSONField(name="rladdress")
    private String rladdress;
    /**
     *    对外联系人姓名
    **/
    @JSONField(name="contacts")
    private String contacts;
    /**
     *    联系人职务
    **/
    @JSONField(name="contactDuty")
    private String contactDuty;


    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 业务主管单位
    **/
    public void setBusinessUnit(String businessUnit) {
      this.businessUnit = businessUnit;
    }
    /**
    *   获取 业务主管单位
    **/
    public String getBusinessUnit() {
      return businessUnit;
    }
    /**
    *   设置 email地址（无法识别的字段 请手动修改）
    **/
    public void setEmailList(List<Object> emailList) {
      this.emailList = emailList;
    }
    /**
    *   获取 email地址（无法识别的字段 请手动修改）
    **/
    public List<Object> getEmailList() {
      return emailList;
    }
    /**
    *   设置 负责人中担任过省部级工作人员数
    **/
    public void setProvinceWorkerNumber(String provinceWorkerNumber) {
      this.provinceWorkerNumber = provinceWorkerNumber;
    }
    /**
    *   获取 负责人中担任过省部级工作人员数
    **/
    public String getProvinceWorkerNumber() {
      return provinceWorkerNumber;
    }
    /**
    *   设置 企业联系方式列表（无法识别的字段 请手动修改）
    **/
    public void setPhoneList(List<Object> phoneList) {
      this.phoneList = phoneList;
    }
    /**
    *   获取 企业联系方式列表（无法识别的字段 请手动修改）
    **/
    public List<Object> getPhoneList() {
      return phoneList;
    }
    /**
    *   设置 无用
    **/
    public void setBaiduAuthURLWWW(String baiduAuthURLWWW) {
      this.baiduAuthURLWWW = baiduAuthURLWWW;
    }
    /**
    *   获取 无用
    **/
    public String getBaiduAuthURLWWW() {
      return baiduAuthURLWWW;
    }
    /**
    *   设置 
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 全职员工数量
    **/
    public void setEmployeeNumber(String employeeNumber) {
      this.employeeNumber = employeeNumber;
    }
    /**
    *   获取 全职员工数量
    **/
    public String getEmployeeNumber() {
      return employeeNumber;
    }
    /**
    *   设置 负责人中国家工作人员数
    **/
    public void setNationalWorkerNumber(String nationalWorkerNumber) {
      this.nationalWorkerNumber = nationalWorkerNumber;
    }
    /**
    *   获取 负责人中国家工作人员数
    **/
    public String getNationalWorkerNumber() {
      return nationalWorkerNumber;
    }
    /**
    *   设置 成立时间
    **/
    public void setEstablishTime(Long establishTime) {
      this.establishTime = establishTime;
    }
    /**
    *   获取 成立时间
    **/
    public Long getEstablishTime() {
      return establishTime;
    }
    /**
    *   设置 志愿者数量
    **/
    public void setVolunteerNumber(String volunteerNumber) {
      this.volunteerNumber = volunteerNumber;
    }
    /**
    *   获取 志愿者数量
    **/
    public String getVolunteerNumber() {
      return volunteerNumber;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 基金会id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 基金会id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 传真
    **/
    public void setFax(String fax) {
      this.fax = fax;
    }
    /**
    *   获取 传真
    **/
    public String getFax() {
      return fax;
    }
    /**
    *   设置 弃用
    **/
    public void setIsClaimed(Integer isClaimed) {
      this.isClaimed = isClaimed;
    }
    /**
    *   获取 弃用
    **/
    public Integer getIsClaimed() {
      return isClaimed;
    }
    /**
    *   设置 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public void setEntityType(Integer entityType) {
      this.entityType = entityType;
    }
    /**
    *   获取 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public Integer getEntityType() {
      return entityType;
    }
    /**
    *   设置 邮政编码
    **/
    public void setPostcode(String postcode) {
      this.postcode = postcode;
    }
    /**
    *   获取 邮政编码
    **/
    public String getPostcode() {
      return postcode;
    }
    /**
    *   设置 是否有年报
    **/
    public void setHaveReport(Boolean haveReport) {
      this.haveReport = haveReport;
    }
    /**
    *   获取 是否有年报
    **/
    public Boolean getHaveReport() {
      return haveReport;
    }
    /**
    *   设置 业务范围
    **/
    public void setBusinessScope(String businessScope) {
      this.businessScope = businessScope;
    }
    /**
    *   获取 业务范围
    **/
    public String getBusinessScope() {
      return businessScope;
    }
    /**
    *   设置 电话
    **/
    public void setTelephone(String telephone) {
      this.telephone = telephone;
    }
    /**
    *   获取 电话
    **/
    public String getTelephone() {
      return telephone;
    }
    /**
    *   设置 企业联系方式
    **/
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    /**
    *   获取 企业联系方式
    **/
    public String getPhoneNumber() {
      return phoneNumber;
    }
    /**
    *   设置 关注领域
    **/
    public void setField(String field) {
      this.field = field;
    }
    /**
    *   获取 关注领域
    **/
    public String getField() {
      return field;
    }
    /**
    *   设置 评估等级
    **/
    public void setGrade(String grade) {
      this.grade = grade;
    }
    /**
    *   获取 评估等级
    **/
    public String getGrade() {
      return grade;
    }
    /**
    *   设置 基金名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 基金名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 优惠资格类型
    **/
    public void setPreferentialQualificationType(String preferentialQualificationType) {
      this.preferentialQualificationType = preferentialQualificationType;
    }
    /**
    *   获取 优惠资格类型
    **/
    public String getPreferentialQualificationType() {
      return preferentialQualificationType;
    }
    /**
    *   设置 数据来源
    **/
    public void setDataSource(String dataSource) {
      this.dataSource = dataSource;
    }
    /**
    *   获取 数据来源
    **/
    public String getDataSource() {
      return dataSource;
    }
    /**
    *   设置 基金会介绍
    **/
    public void setBaseInfo(String baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 基金会介绍
    **/
    public String getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 基金会英文名称
    **/
    public void setEnglishName(String englishName) {
      this.englishName = englishName;
    }
    /**
    *   获取 基金会英文名称
    **/
    public String getEnglishName() {
      return englishName;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 宗旨
    **/
    public void setPurpose(String purpose) {
      this.purpose = purpose;
    }
    /**
    *   获取 宗旨
    **/
    public String getPurpose() {
      return purpose;
    }
    /**
    *   设置 历史名
    **/
    public void setHistoryName(String historyName) {
      this.historyName = historyName;
    }
    /**
    *   获取 历史名
    **/
    public String getHistoryName() {
      return historyName;
    }
    /**
    *   设置 弃用
    **/
    public void setPricingPackage(Integer pricingPackage) {
      this.pricingPackage = pricingPackage;
    }
    /**
    *   获取 弃用
    **/
    public Integer getPricingPackage() {
      return pricingPackage;
    }
    /**
    *   设置 专项基金数
    **/
    public void setSpecialFundNumber(String specialFundNumber) {
      this.specialFundNumber = specialFundNumber;
    }
    /**
    *   获取 专项基金数
    **/
    public String getSpecialFundNumber() {
      return specialFundNumber;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 秘书长
    **/
    public void setSecretary(String secretary) {
      this.secretary = secretary;
    }
    /**
    *   获取 秘书长
    **/
    public String getSecretary() {
      return secretary;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 微博(官方未定义字段类型 暂定为String)
    **/
    public void setWeibo(String weibo) {
      this.weibo = weibo;
    }
    /**
    *   获取 微博(官方未定义字段类型 暂定为String)
    **/
    public String getWeibo() {
      return weibo;
    }
    /**
    *   设置 组织机构代码
    **/
    public void setOrgCode(String orgCode) {
      this.orgCode = orgCode;
    }
    /**
    *   获取 组织机构代码
    **/
    public String getOrgCode() {
      return orgCode;
    }
    /**
    *   设置 基金会范围
    **/
    public void setScope(String scope) {
      this.scope = scope;
    }
    /**
    *   获取 基金会范围
    **/
    public String getScope() {
      return scope;
    }
    /**
    *   设置 无用
    **/
    public void setBaiduAuthURLWAP(String baiduAuthURLWAP) {
      this.baiduAuthURLWAP = baiduAuthURLWAP;
    }
    /**
    *   获取 无用
    **/
    public String getBaiduAuthURLWAP() {
      return baiduAuthURLWAP;
    }
    /**
    *   设置 登记部门
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 登记部门
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 邮箱
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱
    **/
    public String getEmail() {
      return email;
    }
    /**
    *   设置 网址
    **/
    public void setWebsite(String website) {
      this.website = website;
    }
    /**
    *   获取 网址
    **/
    public String getWebsite() {
      return website;
    }
    /**
    *   设置 
    **/
    public void setEstiblishTime(Integer estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 
    **/
    public Integer getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 地址
    **/
    public void setAddress(String address) {
      this.address = address;
    }
    /**
    *   获取 地址
    **/
    public String getAddress() {
      return address;
    }
    /**
    *   设置 地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 企业网站
    **/
    public void setWebsiteList(String websiteList) {
      this.websiteList = websiteList;
    }
    /**
    *   获取 企业网站
    **/
    public String getWebsiteList() {
      return websiteList;
    }
    /**
    *   设置 法人id
    **/
    public void setLegalPersonId(Integer legalPersonId) {
      this.legalPersonId = legalPersonId;
    }
    /**
    *   获取 法人id
    **/
    public Integer getLegalPersonId() {
      return legalPersonId;
    }
    /**
    *   设置 创建时间
    **/
    public void setCreateTime(Long createTime) {
      this.createTime = createTime;
    }
    /**
    *   获取 创建时间
    **/
    public Long getCreateTime() {
      return createTime;
    }
    /**
    *   设置 公司地址
    **/
    public void setRladdress(String rladdress) {
      this.rladdress = rladdress;
    }
    /**
    *   获取 公司地址
    **/
    public String getRladdress() {
      return rladdress;
    }
    /**
    *   设置 对外联系人姓名
    **/
    public void setContacts(String contacts) {
      this.contacts = contacts;
    }
    /**
    *   获取 对外联系人姓名
    **/
    public String getContacts() {
      return contacts;
    }
    /**
    *   设置 联系人职务
    **/
    public void setContactDuty(String contactDuty) {
      this.contactDuty = contactDuty;
    }
    /**
    *   获取 联系人职务
    **/
    public String getContactDuty() {
      return contactDuty;
    }



}

