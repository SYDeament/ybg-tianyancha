package com.gitee.deament.tianyancha.core.remote.stockprospectus.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*招股书
* 属于StockProspectus
* /services/open/stock/prospectus/2.0
*@author deament
**/

public class StockProspectusItems implements Serializable{

    /**
     *    pdf url
    **/
    @JSONField(name="pdfUrl")
    private String pdfUrl;
    /**
     *    股票名称
    **/
    @JSONField(name="stock_name")
    private String stock_name;
    /**
     *    天眼查地址
    **/
    @JSONField(name="ossUrl")
    private String ossUrl;
    /**
     *    标题
    **/
    @JSONField(name="announcement_title")
    private String announcement_title;


    /**
    *   设置 pdf url
    **/
    public void setPdfUrl(String pdfUrl) {
      this.pdfUrl = pdfUrl;
    }
    /**
    *   获取 pdf url
    **/
    public String getPdfUrl() {
      return pdfUrl;
    }
    /**
    *   设置 股票名称
    **/
    public void setStock_name(String stock_name) {
      this.stock_name = stock_name;
    }
    /**
    *   获取 股票名称
    **/
    public String getStock_name() {
      return stock_name;
    }
    /**
    *   设置 天眼查地址
    **/
    public void setOssUrl(String ossUrl) {
      this.ossUrl = ossUrl;
    }
    /**
    *   获取 天眼查地址
    **/
    public String getOssUrl() {
      return ossUrl;
    }
    /**
    *   设置 标题
    **/
    public void setAnnouncement_title(String announcement_title) {
      this.announcement_title = announcement_title;
    }
    /**
    *   获取 标题
    **/
    public String getAnnouncement_title() {
      return announcement_title;
    }



}

