package com.gitee.deament.tianyancha.core.remote.oiinvestevent.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oiinvestevent.entity.OiInvestEvent;
import com.gitee.deament.tianyancha.core.remote.oiinvestevent.dto.OiInvestEventDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*投资动态
* 可以通过投资机构名称获取投资机构投资动态信息，包括投资产品名称、所属公司、参与轮次、投资时间、投资金额、相关新闻标题等字段的详细信息
* /services/open/oi/investEvent/2.0
*@author deament
**/
@Component("oiInvestEventRequestImpl")
public class OiInvestEventRequestImpl extends BaseRequestImpl<OiInvestEvent,OiInvestEventDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/oi/investEvent/2.0";
    }
}

