package com.gitee.deament.tianyancha.core.remote.position.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.position.entity.Position;
import com.gitee.deament.tianyancha.core.remote.position.dto.PositionDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业经纬度
* 可以通过公司名称或ID获取企业经纬度信息
* /services/v4/open/position
*@author deament
**/

public interface PositionRequest extends BaseRequest<PositionDTO ,Position>{

}

