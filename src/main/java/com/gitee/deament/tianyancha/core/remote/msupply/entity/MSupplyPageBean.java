package com.gitee.deament.tianyancha.core.remote.msupply.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*供应商
* 属于MSupply
* /services/open/m/supply/2.0
*@author deament
**/

public class MSupplyPageBean implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private List<MSupplyResult> result;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;


    /**
    *   设置 
    **/
    public void setResult(List<MSupplyResult> result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public List<MSupplyResult> getResult() {
      return result;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }



}

