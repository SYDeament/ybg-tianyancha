package com.gitee.deament.tianyancha.core.remote.humanriskinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.humanriskinfo.entity.HumanRiskInfo;
import com.gitee.deament.tianyancha.core.remote.humanriskinfo.dto.HumanRiskInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*人员天眼风险
* 可以通过公司名称或ID和人名获取人员相关天眼风险列表，包括人员周边/预警风险信息
* /services/v4/open/humanRiskInfo
*@author deament
**/

public interface HumanRiskInfoRequest extends BaseRequest<HumanRiskInfoDTO ,HumanRiskInfo>{

}

