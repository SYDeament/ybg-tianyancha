package com.gitee.deament.tianyancha.core.remote.icparentcompany.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icparentcompany.entity.IcParentCompany;
import com.gitee.deament.tianyancha.core.remote.icparentcompany.dto.IcParentCompanyDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*总公司
* 可以通过公司名称或ID获取企业总公司信息，总公司信息包括总公司名称或ID、企业法人、经营状态、注册资本等字段的详细信息
* /services/open/ic/parentCompany/2.0
*@author deament
**/
@Component("icParentCompanyRequestImpl")
public class IcParentCompanyRequestImpl extends BaseRequestImpl<IcParentCompany,IcParentCompanyDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ic/parentCompany/2.0";
    }
}

