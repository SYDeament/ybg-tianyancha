package com.gitee.deament.tianyancha.core.remote.oisecretinvest.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oisecretinvest.entity.OiSecretInvest;
import com.gitee.deament.tianyancha.core.remote.oisecretinvest.dto.OiSecretInvestDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*未公开投资
* 可以通过投资机构名称获取未公开投资企业，包括企业名称、法人、注册资本、成立日期、投资方等字段的详细信息
* /services/open/oi/secretInvest/2.0
*@author deament
**/
@Component("oiSecretInvestRequestImpl")
public class OiSecretInvestRequestImpl extends BaseRequestImpl<OiSecretInvest,OiSecretInvestDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/oi/secretInvest/2.0";
    }
}

