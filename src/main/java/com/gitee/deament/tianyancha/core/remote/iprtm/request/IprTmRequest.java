package com.gitee.deament.tianyancha.core.remote.iprtm.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprtm.entity.IprTm;
import com.gitee.deament.tianyancha.core.remote.iprtm.dto.IprTmDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*商标信息详情
* 可以通过注册号或国际分类获取商标信息详情，商标信息详情包括商标国际分类、商标名称、商标申请日期、商标申请人、商标注册地址、商标合作申请人、商标申请人（英文）、商标地址（英文）、商标初审公告期号、商标初审公告日期、商标注册公告期号、商标注册公告日期、商标专用权期限期限、商标代理/办理机构等字段的信息
* /services/open/ipr/tm/detail/2.0
*@author deament
**/

public interface IprTmRequest extends BaseRequest<IprTmDTO ,IprTm>{

}

