package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype25 implements Serializable{

    /**
     *    公告法院
    **/
    @JSONField(name="publishOrgName")
    private String publishOrgName;
    /**
     *    持票人
    **/
    @JSONField(name="ownerCompanyName")
    private String ownerCompanyName;
    /**
     *    出票人id
    **/
    @JSONField(name="drawCompanyId")
    private Integer drawCompanyId;
    /**
     *    票据类型
    **/
    @JSONField(name="billType")
    private String billType;
    /**
     *    申请人id
    **/
    @JSONField(name="applyCompanyId")
    private Integer applyCompanyId;
    /**
     *    出票日
    **/
    @JSONField(name="billBeginDt")
    private Integer billBeginDt;
    /**
     *    票据号
    **/
    @JSONField(name="billNum")
    private String billNum;
    /**
     *    出票人
    **/
    @JSONField(name="drawCompanyName")
    private String drawCompanyName;
    /**
     *    票面金额
    **/
    @JSONField(name="billAmt")
    private String billAmt;
    /**
     *    处理类型
    **/
    @JSONField(name="exigentType")
    private String exigentType;
    /**
     *    企业id
    **/
    @JSONField(name="ownerCompanyId")
    private Integer ownerCompanyId;
    /**
     *    申请人
    **/
    @JSONField(name="applyCompanyName")
    private String applyCompanyName;
    /**
     *    公告日期
    **/
    @JSONField(name="publishDt")
    private Long publishDt;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    公告内容
    **/
    @JSONField(name="infoDetail")
    private String infoDetail;
    /**
     *    付款银行
    **/
    @JSONField(name="payCompanyName")
    private String payCompanyName;


    /**
    *   设置 公告法院
    **/
    public void setPublishOrgName(String publishOrgName) {
      this.publishOrgName = publishOrgName;
    }
    /**
    *   获取 公告法院
    **/
    public String getPublishOrgName() {
      return publishOrgName;
    }
    /**
    *   设置 持票人
    **/
    public void setOwnerCompanyName(String ownerCompanyName) {
      this.ownerCompanyName = ownerCompanyName;
    }
    /**
    *   获取 持票人
    **/
    public String getOwnerCompanyName() {
      return ownerCompanyName;
    }
    /**
    *   设置 出票人id
    **/
    public void setDrawCompanyId(Integer drawCompanyId) {
      this.drawCompanyId = drawCompanyId;
    }
    /**
    *   获取 出票人id
    **/
    public Integer getDrawCompanyId() {
      return drawCompanyId;
    }
    /**
    *   设置 票据类型
    **/
    public void setBillType(String billType) {
      this.billType = billType;
    }
    /**
    *   获取 票据类型
    **/
    public String getBillType() {
      return billType;
    }
    /**
    *   设置 申请人id
    **/
    public void setApplyCompanyId(Integer applyCompanyId) {
      this.applyCompanyId = applyCompanyId;
    }
    /**
    *   获取 申请人id
    **/
    public Integer getApplyCompanyId() {
      return applyCompanyId;
    }
    /**
    *   设置 出票日
    **/
    public void setBillBeginDt(Integer billBeginDt) {
      this.billBeginDt = billBeginDt;
    }
    /**
    *   获取 出票日
    **/
    public Integer getBillBeginDt() {
      return billBeginDt;
    }
    /**
    *   设置 票据号
    **/
    public void setBillNum(String billNum) {
      this.billNum = billNum;
    }
    /**
    *   获取 票据号
    **/
    public String getBillNum() {
      return billNum;
    }
    /**
    *   设置 出票人
    **/
    public void setDrawCompanyName(String drawCompanyName) {
      this.drawCompanyName = drawCompanyName;
    }
    /**
    *   获取 出票人
    **/
    public String getDrawCompanyName() {
      return drawCompanyName;
    }
    /**
    *   设置 票面金额
    **/
    public void setBillAmt(String billAmt) {
      this.billAmt = billAmt;
    }
    /**
    *   获取 票面金额
    **/
    public String getBillAmt() {
      return billAmt;
    }
    /**
    *   设置 处理类型
    **/
    public void setExigentType(String exigentType) {
      this.exigentType = exigentType;
    }
    /**
    *   获取 处理类型
    **/
    public String getExigentType() {
      return exigentType;
    }
    /**
    *   设置 企业id
    **/
    public void setOwnerCompanyId(Integer ownerCompanyId) {
      this.ownerCompanyId = ownerCompanyId;
    }
    /**
    *   获取 企业id
    **/
    public Integer getOwnerCompanyId() {
      return ownerCompanyId;
    }
    /**
    *   设置 申请人
    **/
    public void setApplyCompanyName(String applyCompanyName) {
      this.applyCompanyName = applyCompanyName;
    }
    /**
    *   获取 申请人
    **/
    public String getApplyCompanyName() {
      return applyCompanyName;
    }
    /**
    *   设置 公告日期
    **/
    public void setPublishDt(Long publishDt) {
      this.publishDt = publishDt;
    }
    /**
    *   获取 公告日期
    **/
    public Long getPublishDt() {
      return publishDt;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 公告内容
    **/
    public void setInfoDetail(String infoDetail) {
      this.infoDetail = infoDetail;
    }
    /**
    *   获取 公告内容
    **/
    public String getInfoDetail() {
      return infoDetail;
    }
    /**
    *   设置 付款银行
    **/
    public void setPayCompanyName(String payCompanyName) {
      this.payCompanyName = payCompanyName;
    }
    /**
    *   获取 付款银行
    **/
    public String getPayCompanyName() {
      return payCompanyName;
    }



}

