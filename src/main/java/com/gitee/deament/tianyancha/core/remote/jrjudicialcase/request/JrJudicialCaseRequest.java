package com.gitee.deament.tianyancha.core.remote.jrjudicialcase.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrjudicialcase.entity.JrJudicialCase;
import com.gitee.deament.tianyancha.core.remote.jrjudicialcase.dto.JrJudicialCaseDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*司法解析
* 可以通过公司名称或ID获取司法解析信息，司法解析信息包括案件名称、案件类型、案件身份、案由、相关案号、最新审理程序、最新审理日期等字段的详细信息
* /services/open/jr/judicialCase/2.0
*@author deament
**/

public interface JrJudicialCaseRequest extends BaseRequest<JrJudicialCaseDTO ,JrJudicialCase>{

}

