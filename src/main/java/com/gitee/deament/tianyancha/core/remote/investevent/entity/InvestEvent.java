package com.gitee.deament.tianyancha.core.remote.investevent.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.investevent.entity.InvestEventItems;
/**
*投资动态
* 可以通过投资机构名称获取投资产品名称、产品logo、投资金额、融资轮次、相关新闻等字段的详细信息
* /services/v4/open/investEvent
*@author deament
**/

public class InvestEvent implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<InvestEventItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<InvestEventItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<InvestEventItems> getItems() {
      return items;
    }



}

