package com.gitee.deament.tianyancha.core.remote.mgetlicense.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*行政许可-工商局
* 属于MGetLicense
* /services/open/m/getLicense/2.0
*@author deament
**/

public class MGetLicenseItems implements Serializable{

    /**
     *    许可书文编号
    **/
    @JSONField(name="licencenumber")
    private String licencenumber;
    /**
     *    截止日期
    **/
    @JSONField(name="todate")
    private String todate;
    /**
     *    许可证名称
    **/
    @JSONField(name="licencename")
    private String licencename;
    /**
     *    范围
    **/
    @JSONField(name="scope")
    private String scope;
    /**
     *    发证机关
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    起始日期
    **/
    @JSONField(name="fromdate")
    private String fromdate;


    /**
    *   设置 许可书文编号
    **/
    public void setLicencenumber(String licencenumber) {
      this.licencenumber = licencenumber;
    }
    /**
    *   获取 许可书文编号
    **/
    public String getLicencenumber() {
      return licencenumber;
    }
    /**
    *   设置 截止日期
    **/
    public void setTodate(String todate) {
      this.todate = todate;
    }
    /**
    *   获取 截止日期
    **/
    public String getTodate() {
      return todate;
    }
    /**
    *   设置 许可证名称
    **/
    public void setLicencename(String licencename) {
      this.licencename = licencename;
    }
    /**
    *   获取 许可证名称
    **/
    public String getLicencename() {
      return licencename;
    }
    /**
    *   设置 范围
    **/
    public void setScope(String scope) {
      this.scope = scope;
    }
    /**
    *   获取 范围
    **/
    public String getScope() {
      return scope;
    }
    /**
    *   设置 发证机关
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 发证机关
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 起始日期
    **/
    public void setFromdate(String fromdate) {
      this.fromdate = fromdate;
    }
    /**
    *   获取 起始日期
    **/
    public String getFromdate() {
      return fromdate;
    }



}

