package com.gitee.deament.tianyancha.core.remote.jrcourtannouncement.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrcourtannouncement.entity.JrCourtAnnouncementCompanyList;
import com.gitee.deament.tianyancha.core.remote.jrcourtannouncement.entity.JrCourtAnnouncementItems;
/**
*法院公告
* 可以通过公司名称或ID获取企业法院公告，法院公告信息包括执行法院、案件内容、公告类型、刊登日期、公司名、当事人等字段的详细信息
* /services/open/jr/courtAnnouncement/2.0
*@author deament
**/

public class JrCourtAnnouncement implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrCourtAnnouncementItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrCourtAnnouncementItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrCourtAnnouncementItems> getItems() {
      return items;
    }



}

