package com.gitee.deament.tianyancha.core.remote.proximity.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼地图
* 属于Proximity
* /services/v4/open/proximity
*@author deament
**/

public class ProximityItems implements Serializable{

    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblishTime")
    private String estiblishTime;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    联系方式（多个以	;	分割）
    **/
    @JSONField(name="phone")
    private String phone;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    注册地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    行业
    **/
    @JSONField(name="industry")
    private String industry;
    /**
     *    企业id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    省份
    **/
    @JSONField(name="base")
    private String base;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;


    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblishTime(String estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立日期
    **/
    public String getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 联系方式（多个以	;	分割）
    **/
    public void setPhone(String phone) {
      this.phone = phone;
    }
    /**
    *   获取 联系方式（多个以	;	分割）
    **/
    public String getPhone() {
      return phone;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 注册地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 注册地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 行业
    **/
    public void setIndustry(String industry) {
      this.industry = industry;
    }
    /**
    *   获取 行业
    **/
    public String getIndustry() {
      return industry;
    }
    /**
    *   设置 企业id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 省份
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份
    **/
    public String getBase() {
      return base;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }



}

