package com.gitee.deament.tianyancha.core.remote.iprpatents.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*专利信息垂搜
* 属于IprPatents
* /services/open/ipr/patents/search
*@author deament
**/

public class IprPatentsCompanies implements Serializable{

    /**
     *    公司id
    **/
    @JSONField(name="cgid")
    private Integer cgid;
    /**
     *    公司名
    **/
    @JSONField(name="cname")
    private String cname;


    /**
    *   设置 公司id
    **/
    public void setCgid(Integer cgid) {
      this.cgid = cgid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCgid() {
      return cgid;
    }
    /**
    *   设置 公司名
    **/
    public void setCname(String cname) {
      this.cname = cname;
    }
    /**
    *   获取 公司名
    **/
    public String getCname() {
      return cname;
    }



}

