package com.gitee.deament.tianyancha.core.remote.mcustomer.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*客户
* 属于MCustomer
* /services/open/m/customer/2.0
*@author deament
**/

public class MCustomerClientsYear implements Serializable{

    /**
     *    年份（数量）
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    年份
    **/
    @JSONField(name="value")
    private String value;


    /**
    *   设置 年份（数量）
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 年份（数量）
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 年份
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 年份
    **/
    public String getValue() {
      return value;
    }



}

