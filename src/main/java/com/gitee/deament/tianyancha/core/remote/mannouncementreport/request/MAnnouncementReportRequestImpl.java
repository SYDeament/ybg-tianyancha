package com.gitee.deament.tianyancha.core.remote.mannouncementreport.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mannouncementreport.entity.MAnnouncementReport;
import com.gitee.deament.tianyancha.core.remote.mannouncementreport.dto.MAnnouncementReportDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*公告研报
* 可以通过公司名称或ID获取企业研报和相关公告信息，企业研报和相关公告信息包括发布数量、发布日期、标题等字段的详细信息
* /services/open/m/announcementReport/2.0
*@author deament
**/
@Component("mAnnouncementReportRequestImpl")
public class MAnnouncementReportRequestImpl extends BaseRequestImpl<MAnnouncementReport,MAnnouncementReportDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/m/announcementReport/2.0";
    }
}

