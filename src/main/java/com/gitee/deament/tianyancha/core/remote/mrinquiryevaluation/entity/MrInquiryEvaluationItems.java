package com.gitee.deament.tianyancha.core.remote.mrinquiryevaluation.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*询价评估
* 属于MrInquiryEvaluation
* /services/open/mr/inquiryEvaluation/2.0
*@author deament
**/

public class MrInquiryEvaluationItems implements Serializable{

    /**
     *    摇号日期
    **/
    @JSONField(name="insertTime")
    private Long insertTime;
    /**
     *    标的物名称
    **/
    @JSONField(name="subjectname")
    private String subjectname;
    /**
     *    当事人姓名
    **/
    @JSONField(name="ename")
    private String ename;
    /**
     *    案号
    **/
    @JSONField(name="caseNumber")
    private String caseNumber;
    /**
     *    选定评估机构
    **/
    @JSONField(name="evaluationAgency")
    private List<MrInquiryEvaluationEvaluationAgency> evaluationAgency;
    /**
     *    法院
    **/
    @JSONField(name="execCourtName")
    private String execCourtName;
    /**
     *    当事人id
    **/
    @JSONField(name="enameGid")
    private Integer enameGid;
    /**
     *    财产类型
    **/
    @JSONField(name="subjectType")
    private String subjectType;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="enameType")
    private Integer enameType;


    /**
    *   设置 摇号日期
    **/
    public void setInsertTime(Long insertTime) {
      this.insertTime = insertTime;
    }
    /**
    *   获取 摇号日期
    **/
    public Long getInsertTime() {
      return insertTime;
    }
    /**
    *   设置 标的物名称
    **/
    public void setSubjectname(String subjectname) {
      this.subjectname = subjectname;
    }
    /**
    *   获取 标的物名称
    **/
    public String getSubjectname() {
      return subjectname;
    }
    /**
    *   设置 当事人姓名
    **/
    public void setEname(String ename) {
      this.ename = ename;
    }
    /**
    *   获取 当事人姓名
    **/
    public String getEname() {
      return ename;
    }
    /**
    *   设置 案号
    **/
    public void setCaseNumber(String caseNumber) {
      this.caseNumber = caseNumber;
    }
    /**
    *   获取 案号
    **/
    public String getCaseNumber() {
      return caseNumber;
    }
    /**
    *   设置 选定评估机构
    **/
    public void setEvaluationAgency(List<MrInquiryEvaluationEvaluationAgency> evaluationAgency) {
      this.evaluationAgency = evaluationAgency;
    }
    /**
    *   获取 选定评估机构
    **/
    public List<MrInquiryEvaluationEvaluationAgency> getEvaluationAgency() {
      return evaluationAgency;
    }
    /**
    *   设置 法院
    **/
    public void setExecCourtName(String execCourtName) {
      this.execCourtName = execCourtName;
    }
    /**
    *   获取 法院
    **/
    public String getExecCourtName() {
      return execCourtName;
    }
    /**
    *   设置 当事人id
    **/
    public void setEnameGid(Integer enameGid) {
      this.enameGid = enameGid;
    }
    /**
    *   获取 当事人id
    **/
    public Integer getEnameGid() {
      return enameGid;
    }
    /**
    *   设置 财产类型
    **/
    public void setSubjectType(String subjectType) {
      this.subjectType = subjectType;
    }
    /**
    *   获取 财产类型
    **/
    public String getSubjectType() {
      return subjectType;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setEnameType(Integer enameType) {
      this.enameType = enameType;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getEnameType() {
      return enameType;
    }



}

