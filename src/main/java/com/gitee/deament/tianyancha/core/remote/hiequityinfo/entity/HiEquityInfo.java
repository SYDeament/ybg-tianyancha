package com.gitee.deament.tianyancha.core.remote.hiequityinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiequityinfo.entity.HiEquityInfoPledgeeList;
import com.gitee.deament.tianyancha.core.remote.hiequityinfo.entity.HiEquityInfoCompanyList;
import com.gitee.deament.tianyancha.core.remote.hiequityinfo.entity.HiEquityInfoPledgorList;
import com.gitee.deament.tianyancha.core.remote.hiequityinfo.entity.HiEquityInfoItems;
/**
*历史股权出质
* 可以通过公司名称或ID获取历史出质股权标的企业信息，包括历史质权人信息、出质人信息、出质股权数额等字段信息
* /services/open/hi/equityInfo/2.0
*@author deament
**/

public class HiEquityInfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiEquityInfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiEquityInfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiEquityInfoItems> getItems() {
      return items;
    }



}

