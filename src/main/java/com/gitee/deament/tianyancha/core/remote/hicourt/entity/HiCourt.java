package com.gitee.deament.tianyancha.core.remote.hicourt.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hicourt.entity.HiCourtCompanyList;
import com.gitee.deament.tianyancha.core.remote.hicourt.entity.HiCourtItems;
/**
*历史法院公告
* 可以通过公司名称或ID获取企业历史的法院公告，历史法院公告信息包括执行法院、案件内容、公告类型、刊登日期、公司名、当事人等字段信息
* /services/open/hi/court/2.0
*@author deament
**/

public class HiCourt implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiCourtItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiCourtItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiCourtItems> getItems() {
      return items;
    }



}

