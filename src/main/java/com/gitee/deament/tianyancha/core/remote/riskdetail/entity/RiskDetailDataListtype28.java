package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype28 implements Serializable{

    /**
     *    公布时间
    **/
    @JSONField(name="publish_time")
    private String publish_time;
    /**
     *    
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    案件性质
    **/
    @JSONField(name="case_type")
    private String case_type;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    所属税务机关
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    纳税人名称
    **/
    @JSONField(name="taxpayer_name")
    private String taxpayer_name;


    /**
    *   设置 公布时间
    **/
    public void setPublish_time(String publish_time) {
      this.publish_time = publish_time;
    }
    /**
    *   获取 公布时间
    **/
    public String getPublish_time() {
      return publish_time;
    }
    /**
    *   设置 
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 案件性质
    **/
    public void setCase_type(String case_type) {
      this.case_type = case_type;
    }
    /**
    *   获取 案件性质
    **/
    public String getCase_type() {
      return case_type;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 所属税务机关
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 所属税务机关
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 纳税人名称
    **/
    public void setTaxpayer_name(String taxpayer_name) {
      this.taxpayer_name = taxpayer_name;
    }
    /**
    *   获取 纳税人名称
    **/
    public String getTaxpayer_name() {
      return taxpayer_name;
    }



}

