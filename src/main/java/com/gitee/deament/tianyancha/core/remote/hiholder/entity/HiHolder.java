package com.gitee.deament.tianyancha.core.remote.hiholder.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiholder.entity.HiHolderCapital;
import com.gitee.deament.tianyancha.core.remote.hiholder.entity.HiHolderItems;
/**
*历史股东信息
* 可以通过公司名称或ID获取企业历史的股东信息，历史股东信息包括股东名、出资比例、认缴金额、股东总数等字段信息
* /services/open/hi/holder/2.0
*@author deament
**/

public class HiHolder implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiHolderItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiHolderItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiHolderItems> getItems() {
      return items;
    }



}

