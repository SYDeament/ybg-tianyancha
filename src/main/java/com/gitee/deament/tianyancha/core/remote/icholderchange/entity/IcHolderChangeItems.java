package com.gitee.deament.tianyancha.core.remote.icholderchange.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*公司公示-股权变更
* 属于IcHolderChange
* /services/open/ic/holderChange/2.0
*@author deament
**/

public class IcHolderChangeItems implements Serializable{

    /**
     *    公司id
    **/
    @JSONField(name="gid")
    private Integer gid;
    /**
     *    股东名
    **/
    @JSONField(name="investor_name")
    private String investor_name;
    /**
     *    变更后
    **/
    @JSONField(name="ratio_after")
    private String ratio_after;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    变更前
    **/
    @JSONField(name="ratio_before")
    private String ratio_before;
    /**
     *    股东id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    变更时间
    **/
    @JSONField(name="change_time")
    private Long change_time;


    /**
    *   设置 公司id
    **/
    public void setGid(Integer gid) {
      this.gid = gid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getGid() {
      return gid;
    }
    /**
    *   设置 股东名
    **/
    public void setInvestor_name(String investor_name) {
      this.investor_name = investor_name;
    }
    /**
    *   获取 股东名
    **/
    public String getInvestor_name() {
      return investor_name;
    }
    /**
    *   设置 变更后
    **/
    public void setRatio_after(String ratio_after) {
      this.ratio_after = ratio_after;
    }
    /**
    *   获取 变更后
    **/
    public String getRatio_after() {
      return ratio_after;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 变更前
    **/
    public void setRatio_before(String ratio_before) {
      this.ratio_before = ratio_before;
    }
    /**
    *   获取 变更前
    **/
    public String getRatio_before() {
      return ratio_before;
    }
    /**
    *   设置 股东id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 股东id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 变更时间
    **/
    public void setChange_time(Long change_time) {
      this.change_time = change_time;
    }
    /**
    *   获取 变更时间
    **/
    public Long getChange_time() {
      return change_time;
    }



}

