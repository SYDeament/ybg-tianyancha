package com.gitee.deament.tianyancha.core.remote.humancompanyholding.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*人员控股企业
* 可以通过公司名称、人名获取人员控股企业信息，人员控股企业信息包括控股企业ID、投资比例、企业logo、企业简称、控股企业、注册资本、法人类型、经营状态、法人等字段的信息
* /services/open/human/companyholding/2.0
*@author deament
**/

@TYCURL(value="/services/open/human/companyholding/2.0")
public class HumanCompanyholdingDTO implements Serializable{

    /**
     *    人id（humanName和hid只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long hid;
    /**
     *    公司名称（cid和name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数（最大20，默认20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    当前页
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;
    /**
     *    姓名（humanName和hid只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String humanName;
    /**
     *    公司id（cid和name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long cid;


    /**
    *   设置 人id（humanName和hid只需输入其中一个）
    **/
    public void setHid(Long hid) {
      this.hid = hid;
    }
    /**
    *   获取 人id（humanName和hid只需输入其中一个）
    **/
    public Long getHid() {
      return hid;
    }
    /**
    *   设置 公司名称（cid和name只需输入其中一个）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（cid和name只需输入其中一个）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（最大20，默认20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（最大20，默认20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 当前页
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页
    **/
    public Long getPageNum() {
      return pageNum;
    }
    /**
    *   设置 姓名（humanName和hid只需输入其中一个）
    **/
    public void setHumanName(String humanName) {
      this.humanName = humanName;
    }
    /**
    *   获取 姓名（humanName和hid只需输入其中一个）
    **/
    public String getHumanName() {
      return humanName;
    }
    /**
    *   设置 公司id（cid和name只需输入其中一个）
    **/
    public void setCid(Long cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id（cid和name只需输入其中一个）
    **/
    public Long getCid() {
      return cid;
    }



}

