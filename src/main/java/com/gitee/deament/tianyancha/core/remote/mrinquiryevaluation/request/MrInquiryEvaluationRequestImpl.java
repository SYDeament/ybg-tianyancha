package com.gitee.deament.tianyancha.core.remote.mrinquiryevaluation.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrinquiryevaluation.entity.MrInquiryEvaluation;
import com.gitee.deament.tianyancha.core.remote.mrinquiryevaluation.dto.MrInquiryEvaluationDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*询价评估
* 可以通过公司名称或ID获取询价评估信息，询价评估信息包括标的物名称、当事人姓名、评估机构名称等字段的详细信息
* /services/open/mr/inquiryEvaluation/2.0
*@author deament
**/
@Component("mrInquiryEvaluationRequestImpl")
public class MrInquiryEvaluationRequestImpl extends BaseRequestImpl<MrInquiryEvaluation,MrInquiryEvaluationDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/inquiryEvaluation/2.0";
    }
}

