package com.gitee.deament.tianyancha.core.remote.icstaff.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*主要人员
* 属于IcStaff
* /services/open/ic/staff/2.0
*@author deament
**/

public class IcStaffItems implements Serializable{

    /**
     *    主要人员名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    人id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    
    **/
    @JSONField(name="typeJoin")
    private List<String> typeJoin;


    /**
    *   设置 主要人员名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 主要人员名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 人id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 人id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 
    **/
    public void setTypeJoin(List<String> typeJoin) {
      this.typeJoin = typeJoin;
    }
    /**
    *   获取 
    **/
    public List<String> getTypeJoin() {
      return typeJoin;
    }



}

