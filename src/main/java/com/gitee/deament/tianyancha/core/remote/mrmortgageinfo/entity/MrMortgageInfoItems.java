package com.gitee.deament.tianyancha.core.remote.mrmortgageinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*动产抵押
* 属于MrMortgageInfo
* /services/open/mr/mortgageInfo/2.0
*@author deament
**/

public class MrMortgageInfoItems implements Serializable{

    /**
     *    
    **/
    @JSONField(name="baseInfo")
    private MrMortgageInfoBaseInfo baseInfo;
    /**
     *    
    **/
    @JSONField(name="peopleInfo")
    private List<MrMortgageInfoPeopleInfo> peopleInfo;
    /**
     *    
    **/
    @JSONField(name="pawnInfoList")
    private List<MrMortgageInfoPawnInfoList> pawnInfoList;
    /**
     *    
    **/
    @JSONField(name="changeInfoList")
    private List<MrMortgageInfoChangeInfoList> changeInfoList;


    /**
    *   设置 
    **/
    public void setBaseInfo(MrMortgageInfoBaseInfo baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 
    **/
    public MrMortgageInfoBaseInfo getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 
    **/
    public void setPeopleInfo(List<MrMortgageInfoPeopleInfo> peopleInfo) {
      this.peopleInfo = peopleInfo;
    }
    /**
    *   获取 
    **/
    public List<MrMortgageInfoPeopleInfo> getPeopleInfo() {
      return peopleInfo;
    }
    /**
    *   设置 
    **/
    public void setPawnInfoList(List<MrMortgageInfoPawnInfoList> pawnInfoList) {
      this.pawnInfoList = pawnInfoList;
    }
    /**
    *   获取 
    **/
    public List<MrMortgageInfoPawnInfoList> getPawnInfoList() {
      return pawnInfoList;
    }
    /**
    *   设置 
    **/
    public void setChangeInfoList(List<MrMortgageInfoChangeInfoList> changeInfoList) {
      this.changeInfoList = changeInfoList;
    }
    /**
    *   获取 
    **/
    public List<MrMortgageInfoChangeInfoList> getChangeInfoList() {
      return changeInfoList;
    }



}

