package com.gitee.deament.tianyancha.core.remote.stockallotmen.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockallotmen.entity.StockAllotmen;
import com.gitee.deament.tianyancha.core.remote.stockallotmen.dto.StockAllotmenDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*配股情况
* 可以通过公司名称或ID获取上市公司配股情况信息，配股情况信息包括方案进度、配股代码、配股简称、实际配股比例、每套配股价格、实际募集资金净额等
* /services/open/stock/allotmen/2.0
*@author deament
**/
@Component("stockAllotmenRequestImpl")
public class StockAllotmenRequestImpl extends BaseRequestImpl<StockAllotmen,StockAllotmenDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/allotmen/2.0";
    }
}

