package com.gitee.deament.tianyancha.core.remote.mdoublerandomcheck.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*双随机抽查
* 属于MDoubleRandomCheck
* /services/open/m/doubleRandomCheck/2.0
*@author deament
**/

public class MDoubleRandomCheckItems implements Serializable{

    /**
     *    抽查计划编号
    **/
    @JSONField(name="checkPlanNum")
    private String checkPlanNum;
    /**
     *    抽查计划名称
    **/
    @JSONField(name="checkPlanName")
    private String checkPlanName;
    /**
     *    
    **/
    @JSONField(name="taskList")
    private List<MDoubleRandomCheckTaskList> taskList;


    /**
    *   设置 抽查计划编号
    **/
    public void setCheckPlanNum(String checkPlanNum) {
      this.checkPlanNum = checkPlanNum;
    }
    /**
    *   获取 抽查计划编号
    **/
    public String getCheckPlanNum() {
      return checkPlanNum;
    }
    /**
    *   设置 抽查计划名称
    **/
    public void setCheckPlanName(String checkPlanName) {
      this.checkPlanName = checkPlanName;
    }
    /**
    *   获取 抽查计划名称
    **/
    public String getCheckPlanName() {
      return checkPlanName;
    }
    /**
    *   设置 
    **/
    public void setTaskList(List<MDoubleRandomCheckTaskList> taskList) {
      this.taskList = taskList;
    }
    /**
    *   获取 
    **/
    public List<MDoubleRandomCheckTaskList> getTaskList() {
      return taskList;
    }



}

