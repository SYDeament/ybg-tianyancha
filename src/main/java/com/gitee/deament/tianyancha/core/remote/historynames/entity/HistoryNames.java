package com.gitee.deament.tianyancha.core.remote.historynames.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业曾用名
* 可以通过公司名称获取企业曾用名
* /services/v4/open/historyNames
*@author deament
**/

public class HistoryNames implements Serializable{

    /**
     *    曾用名
    **/
    @JSONField(name="historyNames")
    private List<String> historyNames;
    /**
     *    当前名
    **/
    @JSONField(name="name")
    private String name;


    /**
    *   设置 曾用名
    **/
    public void setHistoryNames(List<String> historyNames) {
      this.historyNames = historyNames;
    }
    /**
    *   获取 曾用名
    **/
    public List<String> getHistoryNames() {
      return historyNames;
    }
    /**
    *   设置 当前名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 当前名
    **/
    public String getName() {
      return name;
    }



}

