package com.gitee.deament.tianyancha.core.remote.mlandtransfer.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*土地转让
* 属于MLandTransfer
* /services/open/m/landTransfer/2.0
*@author deament
**/

public class MLandTransferItems implements Serializable{

    /**
     *    土地面积（公顷）
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    成交日期
    **/
    @JSONField(name="merchandise_time")
    private String merchandise_time;
    /**
     *    现土地使用权人
    **/
    @JSONField(name="user_change_now_clean")
    private String user_change_now_clean;
    /**
     *    土地坐落
    **/
    @JSONField(name="location")
    private String location;
    /**
     *    所在行政区
    **/
    @JSONField(name="aministrativeArea")
    private String aministrativeArea;
    /**
     *    转让id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    原土地使用权人
    **/
    @JSONField(name="user_change_pre_clean")
    private String user_change_pre_clean;
    /**
     *    转让价格（万元）
    **/
    @JSONField(name="merchandise_price")
    private String merchandise_price;


    /**
    *   设置 土地面积（公顷）
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 土地面积（公顷）
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 成交日期
    **/
    public void setMerchandise_time(String merchandise_time) {
      this.merchandise_time = merchandise_time;
    }
    /**
    *   获取 成交日期
    **/
    public String getMerchandise_time() {
      return merchandise_time;
    }
    /**
    *   设置 现土地使用权人
    **/
    public void setUser_change_now_clean(String user_change_now_clean) {
      this.user_change_now_clean = user_change_now_clean;
    }
    /**
    *   获取 现土地使用权人
    **/
    public String getUser_change_now_clean() {
      return user_change_now_clean;
    }
    /**
    *   设置 土地坐落
    **/
    public void setLocation(String location) {
      this.location = location;
    }
    /**
    *   获取 土地坐落
    **/
    public String getLocation() {
      return location;
    }
    /**
    *   设置 所在行政区
    **/
    public void setAministrativeArea(String aministrativeArea) {
      this.aministrativeArea = aministrativeArea;
    }
    /**
    *   获取 所在行政区
    **/
    public String getAministrativeArea() {
      return aministrativeArea;
    }
    /**
    *   设置 转让id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 转让id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 原土地使用权人
    **/
    public void setUser_change_pre_clean(String user_change_pre_clean) {
      this.user_change_pre_clean = user_change_pre_clean;
    }
    /**
    *   获取 原土地使用权人
    **/
    public String getUser_change_pre_clean() {
      return user_change_pre_clean;
    }
    /**
    *   设置 转让价格（万元）
    **/
    public void setMerchandise_price(String merchandise_price) {
      this.merchandise_price = merchandise_price;
    }
    /**
    *   获取 转让价格（万元）
    **/
    public String getMerchandise_price() {
      return merchandise_price;
    }



}

