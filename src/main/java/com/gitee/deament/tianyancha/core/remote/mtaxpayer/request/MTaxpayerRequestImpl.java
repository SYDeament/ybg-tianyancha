package com.gitee.deament.tianyancha.core.remote.mtaxpayer.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mtaxpayer.entity.MTaxpayer;
import com.gitee.deament.tianyancha.core.remote.mtaxpayer.dto.MTaxpayerDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*一般纳税人
* 可以通过公司名称或ID获取一般纳税人信息，一般纳税人信息包括纳税人名称、纳税人识别号、纳税人资格类型、主管税务机关、有效期起、有效期止等字段的信息
* /services/open/m/taxpayer/2.0
*@author deament
**/
@Component("mTaxpayerRequestImpl")
public class MTaxpayerRequestImpl extends BaseRequestImpl<MTaxpayer,MTaxpayerDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/m/taxpayer/2.0";
    }
}

