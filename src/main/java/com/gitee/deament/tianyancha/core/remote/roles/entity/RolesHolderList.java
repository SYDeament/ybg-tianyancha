package com.gitee.deament.tianyancha.core.remote.roles.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*人员所有角色
* 属于Roles
* /services/v4/open/roles
*@author deament
**/

public class RolesHolderList implements Serializable{

    /**
     *    法人类型  1-人 2-公司
    **/
    @JSONField(name="legal_person_type")
    private Integer legal_person_type;
    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    成立时间
    **/
    @JSONField(name="estiblishTime")
    private Long estiblishTime;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    类型（固定值sh）
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    出资比例
    **/
    @JSONField(name="percent")
    private String percent;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    无用
    **/
    @JSONField(name="legal_person_id")
    private Integer legal_person_id;
    /**
     *    	投资金额
    **/
    @JSONField(name="subscribed")
    private String subscribed;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 法人类型  1-人 2-公司
    **/
    public void setLegal_person_type(Integer legal_person_type) {
      this.legal_person_type = legal_person_type;
    }
    /**
    *   获取 法人类型  1-人 2-公司
    **/
    public Integer getLegal_person_type() {
      return legal_person_type;
    }
    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 成立时间
    **/
    public void setEstiblishTime(Long estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立时间
    **/
    public Long getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 类型（固定值sh）
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 类型（固定值sh）
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 出资比例
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 出资比例
    **/
    public String getPercent() {
      return percent;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 无用
    **/
    public void setLegal_person_id(Integer legal_person_id) {
      this.legal_person_id = legal_person_id;
    }
    /**
    *   获取 无用
    **/
    public Integer getLegal_person_id() {
      return legal_person_id;
    }
    /**
    *   设置 	投资金额
    **/
    public void setSubscribed(String subscribed) {
      this.subscribed = subscribed;
    }
    /**
    *   获取 	投资金额
    **/
    public String getSubscribed() {
      return subscribed;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

