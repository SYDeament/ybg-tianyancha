package com.gitee.deament.tianyancha.core.remote.allcompanys.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.allcompanys.entity.AllCompanysItems;
/**
*人员所有公司
* 可以通过公司名称或ID和人名获取企业人员的所有相关公司，包括其担任法人、股东、董监高的公司信息
* /services/v4/open/allCompanys
*@author deament
**/

public class AllCompanys implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<AllCompanysItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<AllCompanysItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<AllCompanysItems> getItems() {
      return items;
    }



}

