package com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.entity.StockCompanyInfo;
import com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.dto.StockCompanyInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*上市公司企业简介
* 可以通过公司名称或ID获取上市公司企业简介，上市公司企业简介包括公司全称、曾用名、工商登记号、注册资本、所属行业、主要人员、控股股东、实际控制人、最终控制人、主营业务等
* /services/open/stock/companyInfo/2.0
*@author deament
**/

public interface StockCompanyInfoRequest extends BaseRequest<StockCompanyInfoDTO ,StockCompanyInfo>{

}

