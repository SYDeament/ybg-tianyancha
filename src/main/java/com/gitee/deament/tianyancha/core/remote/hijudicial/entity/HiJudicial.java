package com.gitee.deament.tianyancha.core.remote.hijudicial.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hijudicial.entity.HiJudicialRemoveFrozen;
import com.gitee.deament.tianyancha.core.remote.hijudicial.entity.HiJudicialAssDetail;
import com.gitee.deament.tianyancha.core.remote.hijudicial.entity.HiJudicialShareholderChange;
import com.gitee.deament.tianyancha.core.remote.hijudicial.entity.HiJudicialKeepFrozen;
import com.gitee.deament.tianyancha.core.remote.hijudicial.entity.HiJudicialFrozen;
import com.gitee.deament.tianyancha.core.remote.hijudicial.entity.HiJudicialInvalidationFrozen;
/**
*历史司法协助详情
* 根据司法协助ID获取历史的司法协助详情，判断司法协助类型，历史司法协助详情包括股权变更、股权冻结、结解除冻结、司法协助续行、股权数额、司法冻结失效及对应的详细信息
* /services/open/hi/judicial/detail/2.0
*@author deament
**/

public class HiJudicial implements Serializable{

    /**
     *    解除冻结
    **/
    @JSONField(name="removeFrozen")
    private HiJudicialRemoveFrozen removeFrozen;
    /**
     *    司法协助基本详情
    **/
    @JSONField(name="assDetail")
    private HiJudicialAssDetail assDetail;
    /**
     *    股权变更
    **/
    @JSONField(name="shareholderChange")
    private HiJudicialShareholderChange shareholderChange;
    /**
     *    司法协助续行
    **/
    @JSONField(name="keepFrozen")
    private HiJudicialKeepFrozen keepFrozen;
    /**
     *    股权冻结
    **/
    @JSONField(name="frozen")
    private HiJudicialFrozen frozen;
    /**
     *    司法冻结失效
    **/
    @JSONField(name="invalidationFrozen")
    private HiJudicialInvalidationFrozen invalidationFrozen;


    /**
    *   设置 解除冻结
    **/
    public void setRemoveFrozen(HiJudicialRemoveFrozen removeFrozen) {
      this.removeFrozen = removeFrozen;
    }
    /**
    *   获取 解除冻结
    **/
    public HiJudicialRemoveFrozen getRemoveFrozen() {
      return removeFrozen;
    }
    /**
    *   设置 司法协助基本详情
    **/
    public void setAssDetail(HiJudicialAssDetail assDetail) {
      this.assDetail = assDetail;
    }
    /**
    *   获取 司法协助基本详情
    **/
    public HiJudicialAssDetail getAssDetail() {
      return assDetail;
    }
    /**
    *   设置 股权变更
    **/
    public void setShareholderChange(HiJudicialShareholderChange shareholderChange) {
      this.shareholderChange = shareholderChange;
    }
    /**
    *   获取 股权变更
    **/
    public HiJudicialShareholderChange getShareholderChange() {
      return shareholderChange;
    }
    /**
    *   设置 司法协助续行
    **/
    public void setKeepFrozen(HiJudicialKeepFrozen keepFrozen) {
      this.keepFrozen = keepFrozen;
    }
    /**
    *   获取 司法协助续行
    **/
    public HiJudicialKeepFrozen getKeepFrozen() {
      return keepFrozen;
    }
    /**
    *   设置 股权冻结
    **/
    public void setFrozen(HiJudicialFrozen frozen) {
      this.frozen = frozen;
    }
    /**
    *   获取 股权冻结
    **/
    public HiJudicialFrozen getFrozen() {
      return frozen;
    }
    /**
    *   设置 司法冻结失效
    **/
    public void setInvalidationFrozen(HiJudicialInvalidationFrozen invalidationFrozen) {
      this.invalidationFrozen = invalidationFrozen;
    }
    /**
    *   获取 司法冻结失效
    **/
    public HiJudicialInvalidationFrozen getInvalidationFrozen() {
      return invalidationFrozen;
    }



}

