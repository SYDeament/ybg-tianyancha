package com.gitee.deament.tianyancha.core.remote.stockissuerelated.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*发行相关
* 属于StockIssueRelated
* /services/open/stock/issueRelated/2.0
*@author deament
**/

public class StockIssueRelatedListingSponsor implements Serializable{

    /**
     *    1公司 2人
    **/
    @JSONField(name="cType")
    private Integer cType;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private String id;


    /**
    *   设置 1公司 2人
    **/
    public void setCType(Integer cType) {
      this.cType = cType;
    }
    /**
    *   获取 1公司 2人
    **/
    public Integer getCType() {
      return cType;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public String getId() {
      return id;
    }



}

