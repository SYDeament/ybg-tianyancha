package com.gitee.deament.tianyancha.core.remote.cbhistory.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*历史信息
* 可以通过公司名称或ID获取包含历史工商信息、历史股东信息、历史对外投资、历史行政许可、历史法律诉讼、历史法院公告、历史开庭公告、历史司法协助、历史失信人、历史被执行人、历史经营异常、历史股权出质、历史动产抵押、历史行政处罚、历史商标信息、历史网站备案等维度的相关信息
* /services/open/cb/history/2.0
*@author deament
**/

@TYCURL(value="/services/open/cb/history/2.0")
public class CbHistoryDTO implements Serializable{

    /**
     *    公司名称（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 公司名称（id与name只需输入其中一个）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（id与name只需输入其中一个）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id（id与name只需输入其中一个）
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id（id与name只需输入其中一个）
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

