package com.gitee.deament.tianyancha.core.remote.icbranch.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icbranch.entity.IcBranchItems;
/**
*分支机构
* 可以通过公司名称或ID获取企业分支机构信息，分支机构信息包括分公司名称或ID、企业法人、经营状态、分公司总数等字段的详细信息
* /services/open/ic/branch/2.0
*@author deament
**/

public class IcBranch implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcBranchItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcBranchItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcBranchItems> getItems() {
      return items;
    }



}

