package com.gitee.deament.tianyancha.core.remote.search20.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.search20.entity.Search20;
import com.gitee.deament.tianyancha.core.remote.search20.dto.Search20DTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*搜索
* 可以通过关键词获取企业列表，企业列表包括公司名称或ID、类型、成立日期、经营状态、统一社会信用代码等字段的详细信息
* /services/open/search/2.0
*@author deament
**/
@Component("search20RequestImpl")
public class Search20RequestImpl extends BaseRequestImpl<Search20,Search20DTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/search/2.0";
    }
}

