package com.gitee.deament.tianyancha.core.remote.mtaxcredit.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*税务评级
* 属于MTaxCredit
* /services/open/m/taxCredit/2.0
*@author deament
**/

public class MTaxCreditItems implements Serializable{

    /**
     *    年份
    **/
    @JSONField(name="year")
    private String year;
    /**
     *    纳税等级
    **/
    @JSONField(name="grade")
    private String grade;
    /**
     *    纳税人名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    类型
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    纳税人识别号
    **/
    @JSONField(name="idNumber")
    private String idNumber;
    /**
     *    评价单位
    **/
    @JSONField(name="evalDepartment")
    private String evalDepartment;


    /**
    *   设置 年份
    **/
    public void setYear(String year) {
      this.year = year;
    }
    /**
    *   获取 年份
    **/
    public String getYear() {
      return year;
    }
    /**
    *   设置 纳税等级
    **/
    public void setGrade(String grade) {
      this.grade = grade;
    }
    /**
    *   获取 纳税等级
    **/
    public String getGrade() {
      return grade;
    }
    /**
    *   设置 纳税人名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 纳税人名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 类型
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 类型
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 纳税人识别号
    **/
    public void setIdNumber(String idNumber) {
      this.idNumber = idNumber;
    }
    /**
    *   获取 纳税人识别号
    **/
    public String getIdNumber() {
      return idNumber;
    }
    /**
    *   设置 评价单位
    **/
    public void setEvalDepartment(String evalDepartment) {
      this.evalDepartment = evalDepartment;
    }
    /**
    *   获取 评价单位
    **/
    public String getEvalDepartment() {
      return evalDepartment;
    }



}

