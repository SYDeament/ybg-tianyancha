package com.gitee.deament.tianyancha.core.remote.relashortpath.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*最短路径发现
* 属于RelaShortPath
* /services/open/rela/shortPath
*@author deament
**/

public class RelaShortPathRelationships implements Serializable{

    /**
     *    开始节点
    **/
    @JSONField(name="startNode")
    private Integer startNode;
    /**
     *    关系id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    类型 OWN-法人 SERVE_ALL-任职,INVEST-投资,BRANCH-分支机构,LAW-诉讼,CAC-竞合,EQ-债务, ALL-所有类型
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    结束节点
    **/
    @JSONField(name="endNode")
    private Integer endNode;
    /**
     *    
    **/
    @JSONField(name="properties")
    private RelaShortPathProperties properties;


    /**
    *   设置 开始节点
    **/
    public void setStartNode(Integer startNode) {
      this.startNode = startNode;
    }
    /**
    *   获取 开始节点
    **/
    public Integer getStartNode() {
      return startNode;
    }
    /**
    *   设置 关系id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 关系id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 类型 OWN-法人 SERVE_ALL-任职,INVEST-投资,BRANCH-分支机构,LAW-诉讼,CAC-竞合,EQ-债务, ALL-所有类型
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 类型 OWN-法人 SERVE_ALL-任职,INVEST-投资,BRANCH-分支机构,LAW-诉讼,CAC-竞合,EQ-债务, ALL-所有类型
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 结束节点
    **/
    public void setEndNode(Integer endNode) {
      this.endNode = endNode;
    }
    /**
    *   获取 结束节点
    **/
    public Integer getEndNode() {
      return endNode;
    }
    /**
    *   设置 
    **/
    public void setProperties(RelaShortPathProperties properties) {
      this.properties = properties;
    }
    /**
    *   获取 
    **/
    public RelaShortPathProperties getProperties() {
      return properties;
    }



}

