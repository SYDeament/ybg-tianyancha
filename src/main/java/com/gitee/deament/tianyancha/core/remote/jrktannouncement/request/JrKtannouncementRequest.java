package com.gitee.deament.tianyancha.core.remote.jrktannouncement.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrktannouncement.entity.JrKtannouncement;
import com.gitee.deament.tianyancha.core.remote.jrktannouncement.dto.JrKtannouncementDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*开庭公告
* 可以通过公司名称或ID获取企业开庭公告，开庭公告信息包括被告/被上诉人、法院、原告/上诉人、开庭日期、案由、内部ID、案号等字段的详细信息
* /services/open/jr/ktannouncement/2.0
*@author deament
**/

public interface JrKtannouncementRequest extends BaseRequest<JrKtannouncementDTO ,JrKtannouncement>{

}

