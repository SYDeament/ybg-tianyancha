package com.gitee.deament.tianyancha.core.remote.stockprofit.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*利润表
* 属于StockProfit
* /services/open/stock/profit/2.0
*@author deament
**/

public class StockProfitCorpFinancialYears implements Serializable{

    /**
     *    年度
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    值
    **/
    @JSONField(name="value")
    private String value;


    /**
    *   设置 年度
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 年度
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 值
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 值
    **/
    public String getValue() {
      return value;
    }



}

