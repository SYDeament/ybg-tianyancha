package com.gitee.deament.tianyancha.core.remote.stockbalancesheet.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*资产负债表
* 可以通过公司名称或ID获取上市公司资产负债表数据，资产负债表数据包括流动资产、流动资产合计、非流动资产等
* /services/open/stock/balanceSheet/2.0
*@author deament
**/

@TYCURL(value="/services/open/stock/balanceSheet/2.0")
public class StockBalanceSheetDTO implements Serializable{

    /**
     *    年份
     *
    **/
    @ParamRequire(require = false)
    private String year;
    /**
     *    公司名
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 年份
    **/
    public void setYear(String year) {
      this.year = year;
    }
    /**
    *   获取 年份
    **/
    public String getYear() {
      return year;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

