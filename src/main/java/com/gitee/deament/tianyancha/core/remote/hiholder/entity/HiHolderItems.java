package com.gitee.deament.tianyancha.core.remote.hiholder.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史股东信息
* 属于HiHolder
* /services/open/hi/holder/2.0
*@author deament
**/

public class HiHolderItems implements Serializable{

    /**
     *    拥有公司数量
    **/
    @JSONField(name="toco")
    private Integer toco;
    /**
     *    金额
    **/
    @JSONField(name="amount")
    private String amount;
    /**
     *    认缴
    **/
    @JSONField(name="capital")
    private List<HiHolderCapital> capital;
    /**
     *    股东名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    人或公司id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    0-人 1-公司
    **/
    @JSONField(name="type")
    private Integer type;


    /**
    *   设置 拥有公司数量
    **/
    public void setToco(Integer toco) {
      this.toco = toco;
    }
    /**
    *   获取 拥有公司数量
    **/
    public Integer getToco() {
      return toco;
    }
    /**
    *   设置 金额
    **/
    public void setAmount(String amount) {
      this.amount = amount;
    }
    /**
    *   获取 金额
    **/
    public String getAmount() {
      return amount;
    }
    /**
    *   设置 认缴
    **/
    public void setCapital(List<HiHolderCapital> capital) {
      this.capital = capital;
    }
    /**
    *   获取 认缴
    **/
    public List<HiHolderCapital> getCapital() {
      return capital;
    }
    /**
    *   设置 股东名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 股东名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 人或公司id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 人或公司id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 0-人 1-公司
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 0-人 1-公司
    **/
    public Integer getType() {
      return type;
    }



}

