package com.gitee.deament.tianyancha.core.remote.oiinvestevent.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oiinvestevent.entity.OiInvestEventItems;
/**
*投资动态
* 可以通过投资机构名称获取投资机构投资动态信息，包括投资产品名称、所属公司、参与轮次、投资时间、投资金额、相关新闻标题等字段的详细信息
* /services/open/oi/investEvent/2.0
*@author deament
**/

public class OiInvestEvent implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiInvestEventItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiInvestEventItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiInvestEventItems> getItems() {
      return items;
    }



}

