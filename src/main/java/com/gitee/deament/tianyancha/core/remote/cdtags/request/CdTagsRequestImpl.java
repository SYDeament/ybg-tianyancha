package com.gitee.deament.tianyancha.core.remote.cdtags.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdtags.entity.CdTags;
import com.gitee.deament.tianyancha.core.remote.cdtags.dto.CdTagsDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*获取标签
* 可以通过公司名称或ID获取企业标签，企业标签包括领域分类、主营业务、榜单等
* /services/open/cd/tags/2.0
*@author deament
**/
@Component("cdTagsRequestImpl")
public class CdTagsRequestImpl extends BaseRequestImpl<CdTags,CdTagsDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cd/tags/2.0";
    }
}

