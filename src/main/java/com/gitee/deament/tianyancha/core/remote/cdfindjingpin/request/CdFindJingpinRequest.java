package com.gitee.deament.tianyancha.core.remote.cdfindjingpin.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdfindjingpin.entity.CdFindJingpin;
import com.gitee.deament.tianyancha.core.remote.cdfindjingpin.dto.CdFindJingpinDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*竞品信息
* 可以通过公司名称或ID获取企业竞品信息，企业竞品信息包括竞品名称、行业、业务等字段的详细信息
* /services/open/cd/findJingpin/2.0
*@author deament
**/

public interface CdFindJingpinRequest extends BaseRequest<CdFindJingpinDTO ,CdFindJingpin>{

}

