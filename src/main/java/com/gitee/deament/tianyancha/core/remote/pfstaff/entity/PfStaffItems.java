package com.gitee.deament.tianyancha.core.remote.pfstaff.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*高管情况
* 属于PfStaff
* /services/open/pf/staff/2.0
*@author deament
**/

public class PfStaffItems implements Serializable{

    /**
     *    是否具有基金从业资格
    **/
    @JSONField(name="qualification")
    private String qualification;
    /**
     *    高管姓名
    **/
    @JSONField(name="e_name")
    private String e_name;
    /**
     *    高管id
    **/
    @JSONField(name="e_graph_id")
    private Integer e_graph_id;
    /**
     *    职务
    **/
    @JSONField(name="position")
    private String position;


    /**
    *   设置 是否具有基金从业资格
    **/
    public void setQualification(String qualification) {
      this.qualification = qualification;
    }
    /**
    *   获取 是否具有基金从业资格
    **/
    public String getQualification() {
      return qualification;
    }
    /**
    *   设置 高管姓名
    **/
    public void setE_name(String e_name) {
      this.e_name = e_name;
    }
    /**
    *   获取 高管姓名
    **/
    public String getE_name() {
      return e_name;
    }
    /**
    *   设置 高管id
    **/
    public void setE_graph_id(Integer e_graph_id) {
      this.e_graph_id = e_graph_id;
    }
    /**
    *   获取 高管id
    **/
    public Integer getE_graph_id() {
      return e_graph_id;
    }
    /**
    *   设置 职务
    **/
    public void setPosition(String position) {
      this.position = position;
    }
    /**
    *   获取 职务
    **/
    public String getPosition() {
      return position;
    }



}

