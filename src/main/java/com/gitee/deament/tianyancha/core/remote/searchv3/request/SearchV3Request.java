package com.gitee.deament.tianyancha.core.remote.searchv3.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.searchv3.entity.SearchV3;
import com.gitee.deament.tianyancha.core.remote.searchv3.dto.SearchV3DTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*按行业/区域查询公司
* 可以通过关键词、行业、省市区获取企业列表，企业列表包括公司名称或ID、类型、成立日期、经营状态、统一社会信用代码等字段的详细信息
* /services/v4/open/searchV3
*@author deament
**/

public interface SearchV3Request extends BaseRequest<SearchV3DTO ,SearchV3>{

}

