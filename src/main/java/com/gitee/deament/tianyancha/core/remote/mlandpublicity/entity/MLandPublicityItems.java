package com.gitee.deament.tianyancha.core.remote.mlandpublicity.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*地块公示
* 属于MLandPublicity
* /services/open/m/landPublicity/2.0
*@author deament
**/

public class MLandPublicityItems implements Serializable{

    /**
     *    发布机关
    **/
    @JSONField(name="publication_organize")
    private String publication_organize;
    /**
     *    土地用途
    **/
    @JSONField(name="land_usefulness")
    private String land_usefulness;
    /**
     *    行政区
    **/
    @JSONField(name="administrative_district")
    private String administrative_district;
    /**
     *    地块位置
    **/
    @JSONField(name="land_location")
    private String land_location;
    /**
     *    发布日期
    **/
    @JSONField(name="publication_date")
    private String publication_date;
    /**
     *    土地面积（公顷）
    **/
    @JSONField(name="land_area")
    private String land_area;
    /**
     *    公示id
    **/
    @JSONField(name="id")
    private Integer id;


    /**
    *   设置 发布机关
    **/
    public void setPublication_organize(String publication_organize) {
      this.publication_organize = publication_organize;
    }
    /**
    *   获取 发布机关
    **/
    public String getPublication_organize() {
      return publication_organize;
    }
    /**
    *   设置 土地用途
    **/
    public void setLand_usefulness(String land_usefulness) {
      this.land_usefulness = land_usefulness;
    }
    /**
    *   获取 土地用途
    **/
    public String getLand_usefulness() {
      return land_usefulness;
    }
    /**
    *   设置 行政区
    **/
    public void setAdministrative_district(String administrative_district) {
      this.administrative_district = administrative_district;
    }
    /**
    *   获取 行政区
    **/
    public String getAdministrative_district() {
      return administrative_district;
    }
    /**
    *   设置 地块位置
    **/
    public void setLand_location(String land_location) {
      this.land_location = land_location;
    }
    /**
    *   获取 地块位置
    **/
    public String getLand_location() {
      return land_location;
    }
    /**
    *   设置 发布日期
    **/
    public void setPublication_date(String publication_date) {
      this.publication_date = publication_date;
    }
    /**
    *   获取 发布日期
    **/
    public String getPublication_date() {
      return publication_date;
    }
    /**
    *   设置 土地面积（公顷）
    **/
    public void setLand_area(String land_area) {
      this.land_area = land_area;
    }
    /**
    *   获取 土地面积（公顷）
    **/
    public String getLand_area() {
      return land_area;
    }
    /**
    *   设置 公示id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公示id
    **/
    public Integer getId() {
      return id;
    }



}

