package com.gitee.deament.tianyancha.core.remote.mrillegalinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*严重违法
* 属于MrIllegalinfo
* /services/open/mr/illegalinfo/2.0
*@author deament
**/

public class MrIllegalinfoItems implements Serializable{

    /**
     *    移除日期
    **/
    @JSONField(name="removeDate")
    private Long removeDate;
    /**
     *    列入原因
    **/
    @JSONField(name="putReason")
    private String putReason;
    /**
     *    决定列入部门(作出决定机关)
    **/
    @JSONField(name="putDepartment")
    private String putDepartment;
    /**
     *    决定移除部门
    **/
    @JSONField(name="removeDepartment")
    private String removeDepartment;
    /**
     *    移除原因
    **/
    @JSONField(name="removeReason")
    private String removeReason;
    /**
     *    列入日期
    **/
    @JSONField(name="putDate")
    private Long putDate;


    /**
    *   设置 移除日期
    **/
    public void setRemoveDate(Long removeDate) {
      this.removeDate = removeDate;
    }
    /**
    *   获取 移除日期
    **/
    public Long getRemoveDate() {
      return removeDate;
    }
    /**
    *   设置 列入原因
    **/
    public void setPutReason(String putReason) {
      this.putReason = putReason;
    }
    /**
    *   获取 列入原因
    **/
    public String getPutReason() {
      return putReason;
    }
    /**
    *   设置 决定列入部门(作出决定机关)
    **/
    public void setPutDepartment(String putDepartment) {
      this.putDepartment = putDepartment;
    }
    /**
    *   获取 决定列入部门(作出决定机关)
    **/
    public String getPutDepartment() {
      return putDepartment;
    }
    /**
    *   设置 决定移除部门
    **/
    public void setRemoveDepartment(String removeDepartment) {
      this.removeDepartment = removeDepartment;
    }
    /**
    *   获取 决定移除部门
    **/
    public String getRemoveDepartment() {
      return removeDepartment;
    }
    /**
    *   设置 移除原因
    **/
    public void setRemoveReason(String removeReason) {
      this.removeReason = removeReason;
    }
    /**
    *   获取 移除原因
    **/
    public String getRemoveReason() {
      return removeReason;
    }
    /**
    *   设置 列入日期
    **/
    public void setPutDate(Long putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 列入日期
    **/
    public Long getPutDate() {
      return putDate;
    }



}

