package com.gitee.deament.tianyancha.core.remote.mtaxpayer.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*一般纳税人
* 属于MTaxpayer
* /services/open/m/taxpayer/2.0
*@author deament
**/

public class MTaxpayerItems implements Serializable{

    /**
     *    公司id
    **/
    @JSONField(name="gid")
    private Integer gid;
    /**
     *    纳税人资格类型
    **/
    @JSONField(name="taxpayerQualificationType")
    private String taxpayerQualificationType;
    /**
     *    结束日期
    **/
    @JSONField(name="endDate")
    private String endDate;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公司logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    公司简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    纳税人识别号
    **/
    @JSONField(name="taxpayerIdentificationNumber")
    private String taxpayerIdentificationNumber;
    /**
     *    开始时间
    **/
    @JSONField(name="startDate")
    private String startDate;


    /**
    *   设置 公司id
    **/
    public void setGid(Integer gid) {
      this.gid = gid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getGid() {
      return gid;
    }
    /**
    *   设置 纳税人资格类型
    **/
    public void setTaxpayerQualificationType(String taxpayerQualificationType) {
      this.taxpayerQualificationType = taxpayerQualificationType;
    }
    /**
    *   获取 纳税人资格类型
    **/
    public String getTaxpayerQualificationType() {
      return taxpayerQualificationType;
    }
    /**
    *   设置 结束日期
    **/
    public void setEndDate(String endDate) {
      this.endDate = endDate;
    }
    /**
    *   获取 结束日期
    **/
    public String getEndDate() {
      return endDate;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 公司logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 公司简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 公司简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 纳税人识别号
    **/
    public void setTaxpayerIdentificationNumber(String taxpayerIdentificationNumber) {
      this.taxpayerIdentificationNumber = taxpayerIdentificationNumber;
    }
    /**
    *   获取 纳税人识别号
    **/
    public String getTaxpayerIdentificationNumber() {
      return taxpayerIdentificationNumber;
    }
    /**
    *   设置 开始时间
    **/
    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 开始时间
    **/
    public String getStartDate() {
      return startDate;
    }



}

