package com.gitee.deament.tianyancha.core.remote.mimportandexport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*进出口信用
* 属于MImportAndExport
* /services/open/m/importAndExport/2.0
*@author deament
**/

public class MImportAndExportCreditRating implements Serializable{

    /**
     *    认证证书编码
    **/
    @JSONField(name="authenticationCode")
    private String authenticationCode;
    /**
     *    认证时间
    **/
    @JSONField(name="identificationTime")
    private String identificationTime;
    /**
     *    信用等级
    **/
    @JSONField(name="creditRating")
    private String creditRating;


    /**
    *   设置 认证证书编码
    **/
    public void setAuthenticationCode(String authenticationCode) {
      this.authenticationCode = authenticationCode;
    }
    /**
    *   获取 认证证书编码
    **/
    public String getAuthenticationCode() {
      return authenticationCode;
    }
    /**
    *   设置 认证时间
    **/
    public void setIdentificationTime(String identificationTime) {
      this.identificationTime = identificationTime;
    }
    /**
    *   获取 认证时间
    **/
    public String getIdentificationTime() {
      return identificationTime;
    }
    /**
    *   设置 信用等级
    **/
    public void setCreditRating(String creditRating) {
      this.creditRating = creditRating;
    }
    /**
    *   获取 信用等级
    **/
    public String getCreditRating() {
      return creditRating;
    }



}

