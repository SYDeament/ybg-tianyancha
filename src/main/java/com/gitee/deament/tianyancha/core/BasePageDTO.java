package com.gitee.deament.tianyancha.core;

/**
 * 分页基类
 *
 * @author deament
 */
public class BasePageDTO {
    int pageNum;
    int pageSize;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }
}
