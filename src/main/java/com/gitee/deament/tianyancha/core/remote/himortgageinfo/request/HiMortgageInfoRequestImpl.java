package com.gitee.deament.tianyancha.core.remote.himortgageinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity.HiMortgageInfo;
import com.gitee.deament.tianyancha.core.remote.himortgageinfo.dto.HiMortgageInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史动产抵押
* 可以通过公司名称或ID获取企业历史的动产抵押公告信息，历史动产抵押公告信息包括被担保债权类型、数额、登记机关等字段信息
* /services/open/hi/mortgageInfo/2.0
*@author deament
**/
@Component("hiMortgageInfoRequestImpl")
public class HiMortgageInfoRequestImpl extends BaseRequestImpl<HiMortgageInfo,HiMortgageInfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/mortgageInfo/2.0";
    }
}

