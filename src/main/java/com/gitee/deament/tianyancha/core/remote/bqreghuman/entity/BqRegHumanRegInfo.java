package com.gitee.deament.tianyancha.core.remote.bqreghuman.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-注册人员详情
* 属于BqRegHuman
* /services/open/bq/regHuman/detail/2.0
*@author deament
**/

public class BqRegHumanRegInfo implements Serializable{

    /**
     *    
    **/
    @JSONField(name="result")
    private List<BqRegHumanResult> result;
    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;


    /**
    *   设置 
    **/
    public void setResult(List<BqRegHumanResult> result) {
      this.result = result;
    }
    /**
    *   获取 
    **/
    public List<BqRegHumanResult> getResult() {
      return result;
    }
    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }



}

