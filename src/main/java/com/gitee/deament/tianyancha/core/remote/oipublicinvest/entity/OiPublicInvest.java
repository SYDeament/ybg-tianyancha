package com.gitee.deament.tianyancha.core.remote.oipublicinvest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oipublicinvest.entity.OiPublicInvestItems;
/**
*公开投资事件
* 可以通过投资机构名称获取公开投资产品，包括产品名称、所属公司、参与轮次、投资时间、投资金额、产品介绍等字段的详细信息
* /services/open/oi/publicInvest/2.0
*@author deament
**/

public class OiPublicInvest implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<OiPublicInvestItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<OiPublicInvestItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<OiPublicInvestItems> getItems() {
      return items;
    }



}

