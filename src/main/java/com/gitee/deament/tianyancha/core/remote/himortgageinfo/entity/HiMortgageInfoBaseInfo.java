package com.gitee.deament.tianyancha.core.remote.himortgageinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史动产抵押
* 属于HiMortgageInfo
* /services/open/hi/mortgageInfo/2.0
*@author deament
**/

public class HiMortgageInfoBaseInfo implements Serializable{

    /**
     *    被担保债权数额
    **/
    @JSONField(name="amount")
    private String amount;
    /**
     *    登记机关
    **/
    @JSONField(name="regDepartment")
    private String regDepartment;
    /**
     *    登记编号
    **/
    @JSONField(name="regNum")
    private String regNum;
    /**
     *    担保范围
    **/
    @JSONField(name="scope")
    private String scope;
    /**
     *    公示日期
    **/
    @JSONField(name="publishDate")
    private Long publishDate;
    /**
     *    登记日期
    **/
    @JSONField(name="regDate")
    private String regDate;
    /**
     *    备注
    **/
    @JSONField(name="remark")
    private String remark;
    /**
     *    表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    被担保债权种类
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    概况债务人履行债务的期限
    **/
    @JSONField(name="overviewTerm")
    private String overviewTerm;
    /**
     *    状态
    **/
    @JSONField(name="status")
    private String status;
    /**
     *    省份
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 被担保债权数额
    **/
    public void setAmount(String amount) {
      this.amount = amount;
    }
    /**
    *   获取 被担保债权数额
    **/
    public String getAmount() {
      return amount;
    }
    /**
    *   设置 登记机关
    **/
    public void setRegDepartment(String regDepartment) {
      this.regDepartment = regDepartment;
    }
    /**
    *   获取 登记机关
    **/
    public String getRegDepartment() {
      return regDepartment;
    }
    /**
    *   设置 登记编号
    **/
    public void setRegNum(String regNum) {
      this.regNum = regNum;
    }
    /**
    *   获取 登记编号
    **/
    public String getRegNum() {
      return regNum;
    }
    /**
    *   设置 担保范围
    **/
    public void setScope(String scope) {
      this.scope = scope;
    }
    /**
    *   获取 担保范围
    **/
    public String getScope() {
      return scope;
    }
    /**
    *   设置 公示日期
    **/
    public void setPublishDate(Long publishDate) {
      this.publishDate = publishDate;
    }
    /**
    *   获取 公示日期
    **/
    public Long getPublishDate() {
      return publishDate;
    }
    /**
    *   设置 登记日期
    **/
    public void setRegDate(String regDate) {
      this.regDate = regDate;
    }
    /**
    *   获取 登记日期
    **/
    public String getRegDate() {
      return regDate;
    }
    /**
    *   设置 备注
    **/
    public void setRemark(String remark) {
      this.remark = remark;
    }
    /**
    *   获取 备注
    **/
    public String getRemark() {
      return remark;
    }
    /**
    *   设置 表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 被担保债权种类
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 被担保债权种类
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 概况债务人履行债务的期限
    **/
    public void setOverviewTerm(String overviewTerm) {
      this.overviewTerm = overviewTerm;
    }
    /**
    *   获取 概况债务人履行债务的期限
    **/
    public String getOverviewTerm() {
      return overviewTerm;
    }
    /**
    *   设置 状态
    **/
    public void setStatus(String status) {
      this.status = status;
    }
    /**
    *   获取 状态
    **/
    public String getStatus() {
      return status;
    }
    /**
    *   设置 省份
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份
    **/
    public String getBase() {
      return base;
    }



}

