package com.gitee.deament.tianyancha.core.remote.cdinvestorg.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdinvestorg.entity.CdInvestOrg;
import com.gitee.deament.tianyancha.core.remote.cdinvestorg.dto.CdInvestOrgDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*投资机构
* 可以通过公司名称或ID获企业相关投资机构信息，投资机构信息包括机构名称、成立日期、简介等字段的详细信息
* /services/open/cd/investOrg/2.0
*@author deament
**/
@Component("cdInvestOrgRequestImpl")
public class CdInvestOrgRequestImpl extends BaseRequestImpl<CdInvestOrg,CdInvestOrgDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cd/investOrg/2.0";
    }
}

