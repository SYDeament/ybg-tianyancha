package com.gitee.deament.tianyancha.core.remote.stockquarmainindex.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockquarmainindex.entity.StockQuarMainIndex;
import com.gitee.deament.tianyancha.core.remote.stockquarmainindex.dto.StockQuarMainIndexDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*主要指标-季度
* 可以通过公司名称或ID获取上市公司季度主要指标数据，季度主要指标数据包括基本每股收益、扣非每股收益、稀释每股收益、每股净资产、每股公积金等
* /services/open/stock/quarMainIndex/2.0
*@author deament
**/

public interface StockQuarMainIndexRequest extends BaseRequest<StockQuarMainIndexDTO ,StockQuarMainIndex>{

}

