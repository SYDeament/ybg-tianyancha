package com.gitee.deament.tianyancha.core.remote.stockequitychange.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockequitychange.entity.StockEquityChangeItems;
/**
*股本变动
* 可以通过公司名称或ID获取上市公司股本变动信息，股本变动信息包括变动时间、变动原因、变动后A股总股本、变动后流通A股、变动后限售A股等
* /services/open/stock/equityChange/2.0
*@author deament
**/

public class StockEquityChange implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<StockEquityChangeItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<StockEquityChangeItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<StockEquityChangeItems> getItems() {
      return items;
    }



}

