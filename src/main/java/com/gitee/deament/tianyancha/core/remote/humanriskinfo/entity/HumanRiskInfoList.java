package com.gitee.deament.tianyancha.core.remote.humanriskinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*人员天眼风险
* 属于HumanRiskInfo
* /services/v4/open/humanRiskInfo
*@author deament
**/

public class HumanRiskInfoList implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    风险标签（警示信息，高风险信息，提示信息）
    **/
    @JSONField(name="tag")
    private String tag;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    
    **/
    @JSONField(name="list")
    private List<HumanRiskInfoList> list;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 风险标签（警示信息，高风险信息，提示信息）
    **/
    public void setTag(String tag) {
      this.tag = tag;
    }
    /**
    *   获取 风险标签（警示信息，高风险信息，提示信息）
    **/
    public String getTag() {
      return tag;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 
    **/
    public void setList(List<HumanRiskInfoList> list) {
      this.list = list;
    }
    /**
    *   获取 
    **/
    public List<HumanRiskInfoList> getList() {
      return list;
    }



}

