package com.gitee.deament.tianyancha.core.remote.bqbadconduct.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-不良行为
* 属于BqBadConduct
* /services/open/bq/badConduct/2.0
*@author deament
**/

public class BqBadConductItems implements Serializable{

    /**
     *    诚信记录编号
    **/
    @JSONField(name="integrityRecordNo")
    private String integrityRecordNo;
    /**
     *    查看事由
    **/
    @JSONField(name="reasons")
    private String reasons;
    /**
     *    实施部门（文号）
    **/
    @JSONField(name="implementationDepartment")
    private String implementationDepartment;
    /**
     *    发布有效期
    **/
    @JSONField(name="publishValidityPeriod")
    private String publishValidityPeriod;
    /**
     *    决定内容2
    **/
    @JSONField(name="decisionContent2")
    private String decisionContent2;
    /**
     *    id
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    决定内容
    **/
    @JSONField(name="decisionContent")
    private String decisionContent;
    /**
     *    标签
    **/
    @JSONField(name="creditIcon")
    private String creditIcon;
    /**
     *    诚信记录主体
    **/
    @JSONField(name="integritySubject")
    private String integritySubject;
    /**
     *    决定日期
    **/
    @JSONField(name="decisionDate")
    private String decisionDate;
    /**
     *    文号
    **/
    @JSONField(name="documentDumber")
    private String documentDumber;


    /**
    *   设置 诚信记录编号
    **/
    public void setIntegrityRecordNo(String integrityRecordNo) {
      this.integrityRecordNo = integrityRecordNo;
    }
    /**
    *   获取 诚信记录编号
    **/
    public String getIntegrityRecordNo() {
      return integrityRecordNo;
    }
    /**
    *   设置 查看事由
    **/
    public void setReasons(String reasons) {
      this.reasons = reasons;
    }
    /**
    *   获取 查看事由
    **/
    public String getReasons() {
      return reasons;
    }
    /**
    *   设置 实施部门（文号）
    **/
    public void setImplementationDepartment(String implementationDepartment) {
      this.implementationDepartment = implementationDepartment;
    }
    /**
    *   获取 实施部门（文号）
    **/
    public String getImplementationDepartment() {
      return implementationDepartment;
    }
    /**
    *   设置 发布有效期
    **/
    public void setPublishValidityPeriod(String publishValidityPeriod) {
      this.publishValidityPeriod = publishValidityPeriod;
    }
    /**
    *   获取 发布有效期
    **/
    public String getPublishValidityPeriod() {
      return publishValidityPeriod;
    }
    /**
    *   设置 决定内容2
    **/
    public void setDecisionContent2(String decisionContent2) {
      this.decisionContent2 = decisionContent2;
    }
    /**
    *   获取 决定内容2
    **/
    public String getDecisionContent2() {
      return decisionContent2;
    }
    /**
    *   设置 id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 id
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 决定内容
    **/
    public void setDecisionContent(String decisionContent) {
      this.decisionContent = decisionContent;
    }
    /**
    *   获取 决定内容
    **/
    public String getDecisionContent() {
      return decisionContent;
    }
    /**
    *   设置 标签
    **/
    public void setCreditIcon(String creditIcon) {
      this.creditIcon = creditIcon;
    }
    /**
    *   获取 标签
    **/
    public String getCreditIcon() {
      return creditIcon;
    }
    /**
    *   设置 诚信记录主体
    **/
    public void setIntegritySubject(String integritySubject) {
      this.integritySubject = integritySubject;
    }
    /**
    *   获取 诚信记录主体
    **/
    public String getIntegritySubject() {
      return integritySubject;
    }
    /**
    *   设置 决定日期
    **/
    public void setDecisionDate(String decisionDate) {
      this.decisionDate = decisionDate;
    }
    /**
    *   获取 决定日期
    **/
    public String getDecisionDate() {
      return decisionDate;
    }
    /**
    *   设置 文号
    **/
    public void setDocumentDumber(String documentDumber) {
      this.documentDumber = documentDumber;
    }
    /**
    *   获取 文号
    **/
    public String getDocumentDumber() {
      return documentDumber;
    }



}

