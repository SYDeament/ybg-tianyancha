package com.gitee.deament.tianyancha.core.remote.stockannouncement.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockannouncement.entity.StockAnnouncementItems;
/**
*上市公告
* 可以通过公司名称或ID获取上市公司上市公告信息，上市公告信息包括公告标题、发布日期、股票名、股票号等
* /services/open/stock/announcement/2.0
*@author deament
**/

public class StockAnnouncement implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<StockAnnouncementItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<StockAnnouncementItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<StockAnnouncementItems> getItems() {
      return items;
    }



}

