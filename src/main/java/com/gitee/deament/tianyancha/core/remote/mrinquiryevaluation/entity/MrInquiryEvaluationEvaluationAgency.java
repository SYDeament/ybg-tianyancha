package com.gitee.deament.tianyancha.core.remote.mrinquiryevaluation.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*询价评估
* 属于MrInquiryEvaluation
* /services/open/mr/inquiryEvaluation/2.0
*@author deament
**/

public class MrInquiryEvaluationEvaluationAgency implements Serializable{

    /**
     *    机构名
    **/
    @JSONField(name="orgName")
    private String orgName;
    /**
     *    机构id
    **/
    @JSONField(name="orgGid")
    private Integer orgGid;


    /**
    *   设置 机构名
    **/
    public void setOrgName(String orgName) {
      this.orgName = orgName;
    }
    /**
    *   获取 机构名
    **/
    public String getOrgName() {
      return orgName;
    }
    /**
    *   设置 机构id
    **/
    public void setOrgGid(Integer orgGid) {
      this.orgGid = orgGid;
    }
    /**
    *   获取 机构id
    **/
    public Integer getOrgGid() {
      return orgGid;
    }



}

