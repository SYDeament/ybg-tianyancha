package com.gitee.deament.tianyancha.core.remote.jrsendannouncement.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*送达公告
* 属于JrSendAnnouncement
* /services/open/jr/sendAnnouncement/2.0
*@author deament
**/

public class JrSendAnnouncementItems implements Serializable{

    /**
     *    公告名称
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    法院名称
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    内容
    **/
    @JSONField(name="content")
    private String content;
    /**
     *    发布日期
    **/
    @JSONField(name="startDate")
    private Long startDate;


    /**
    *   设置 公告名称
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 公告名称
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 法院名称
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院名称
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 内容
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 内容
    **/
    public String getContent() {
      return content;
    }
    /**
    *   设置 发布日期
    **/
    public void setStartDate(Long startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 发布日期
    **/
    public Long getStartDate() {
      return startDate;
    }



}

