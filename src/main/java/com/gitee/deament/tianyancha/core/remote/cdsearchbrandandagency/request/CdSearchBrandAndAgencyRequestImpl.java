package com.gitee.deament.tianyancha.core.remote.cdsearchbrandandagency.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdsearchbrandandagency.entity.CdSearchBrandAndAgency;
import com.gitee.deament.tianyancha.core.remote.cdsearchbrandandagency.dto.CdSearchBrandAndAgencyDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*搜索项目品牌/投资机构
* 可以通过关键词获取项目品牌和投资机构信息，包括项目品牌和投资机构名称、logo、融资轮次或投资事件等字段的详细信息
* /services/open/cd/searchBrandAndAgency/2.0
*@author deament
**/
@Component("cdSearchBrandAndAgencyRequestImpl")
public class CdSearchBrandAndAgencyRequestImpl extends BaseRequestImpl<CdSearchBrandAndAgency,CdSearchBrandAndAgencyDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cd/searchBrandAndAgency/2.0";
    }
}

