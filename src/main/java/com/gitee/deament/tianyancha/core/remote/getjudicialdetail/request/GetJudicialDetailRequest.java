package com.gitee.deament.tianyancha.core.remote.getjudicialdetail.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.getjudicialdetail.entity.GetJudicialDetail;
import com.gitee.deament.tianyancha.core.remote.getjudicialdetail.dto.GetJudicialDetailDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*司法协助详情
* 根据司法协助ID获取司法协助详情，判断司法协助类型，包括股权变更、股权冻结、结解除冻结、司法协助续行、股权数额、司法冻结失效及对应的详细信息
* /services/v4/open/getJudicialDetail
*@author deament
**/

public interface GetJudicialDetailRequest extends BaseRequest<GetJudicialDetailDTO ,GetJudicialDetail>{

}

