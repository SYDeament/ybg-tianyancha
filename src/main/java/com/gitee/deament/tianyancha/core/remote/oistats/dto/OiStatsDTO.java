package com.gitee.deament.tianyancha.core.remote.oistats.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*统计分析
* 可以通过投资机构名称获取投资机构投资统计分析，包括月份、数量、投资规模、投资行业等
* /services/open/oi/stats/2.0
*@author deament
**/

@TYCURL(value="/services/open/oi/stats/2.0")
public class OiStatsDTO implements Serializable{

    /**
     *    年份
     *
    **/
    @ParamRequire(require = false)
    private Long year;
    /**
     *    机构名
     *
    **/
    @ParamRequire(require = true)
    private String name;


    /**
    *   设置 年份
    **/
    public void setYear(Long year) {
      this.year = year;
    }
    /**
    *   获取 年份
    **/
    public Long getYear() {
      return year;
    }
    /**
    *   设置 机构名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 机构名
    **/
    public String getName() {
      return name;
    }



}

