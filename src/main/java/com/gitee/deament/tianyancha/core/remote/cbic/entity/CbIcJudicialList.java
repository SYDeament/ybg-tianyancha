package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcJudicialList implements Serializable{

    /**
     *    表id
    **/
    @JSONField(name="assId")
    private Integer assId;
    /**
     *    被执行人
    **/
    @JSONField(name="executedPerson")
    private String executedPerson;
    /**
     *    执行通知书文号
    **/
    @JSONField(name="executeNoticeNum")
    private String executeNoticeNum;
    /**
     *    冻结失效
    **/
    @JSONField(name="companyJudicialAssistanceFrozenInvalidationInfo")
    private CbIcCompanyJudicialAssistanceFrozenInvalidationInfo companyJudicialAssistanceFrozenInvalidationInfo;
    /**
     *    股权数额
    **/
    @JSONField(name="equityAmount")
    private String equityAmount;
    /**
     *    冻结续行
    **/
    @JSONField(name="companyJudicialAssistanceFrozenKeepInfo")
    private CbIcCompanyJudicialAssistanceFrozenKeepInfo companyJudicialAssistanceFrozenKeepInfo;
    /**
     *    解除冻结
    **/
    @JSONField(name="companyJudicialAssistanceFrozenRemInfo")
    private CbIcCompanyJudicialAssistanceFrozenRemInfo companyJudicialAssistanceFrozenRemInfo;
    /**
     *    类型|状态
    **/
    @JSONField(name="typeState")
    private String typeState;
    /**
     *    执行法院
    **/
    @JSONField(name="executiveCourt")
    private String executiveCourt;
    /**
     *    冻结
    **/
    @JSONField(name="companyJudicialAssistanceFrozenInfo")
    private CbIcCompanyJudicialAssistanceFrozenInfo companyJudicialAssistanceFrozenInfo;
    /**
     *    股权变更
    **/
    @JSONField(name="companyJudicialShareholderChangeInfo")
    private CbIcCompanyJudicialShareholderChangeInfo companyJudicialShareholderChangeInfo;


    /**
    *   设置 表id
    **/
    public void setAssId(Integer assId) {
      this.assId = assId;
    }
    /**
    *   获取 表id
    **/
    public Integer getAssId() {
      return assId;
    }
    /**
    *   设置 被执行人
    **/
    public void setExecutedPerson(String executedPerson) {
      this.executedPerson = executedPerson;
    }
    /**
    *   获取 被执行人
    **/
    public String getExecutedPerson() {
      return executedPerson;
    }
    /**
    *   设置 执行通知书文号
    **/
    public void setExecuteNoticeNum(String executeNoticeNum) {
      this.executeNoticeNum = executeNoticeNum;
    }
    /**
    *   获取 执行通知书文号
    **/
    public String getExecuteNoticeNum() {
      return executeNoticeNum;
    }
    /**
    *   设置 冻结失效
    **/
    public void setCompanyJudicialAssistanceFrozenInvalidationInfo(CbIcCompanyJudicialAssistanceFrozenInvalidationInfo companyJudicialAssistanceFrozenInvalidationInfo) {
      this.companyJudicialAssistanceFrozenInvalidationInfo = companyJudicialAssistanceFrozenInvalidationInfo;
    }
    /**
    *   获取 冻结失效
    **/
    public CbIcCompanyJudicialAssistanceFrozenInvalidationInfo getCompanyJudicialAssistanceFrozenInvalidationInfo() {
      return companyJudicialAssistanceFrozenInvalidationInfo;
    }
    /**
    *   设置 股权数额
    **/
    public void setEquityAmount(String equityAmount) {
      this.equityAmount = equityAmount;
    }
    /**
    *   获取 股权数额
    **/
    public String getEquityAmount() {
      return equityAmount;
    }
    /**
    *   设置 冻结续行
    **/
    public void setCompanyJudicialAssistanceFrozenKeepInfo(CbIcCompanyJudicialAssistanceFrozenKeepInfo companyJudicialAssistanceFrozenKeepInfo) {
      this.companyJudicialAssistanceFrozenKeepInfo = companyJudicialAssistanceFrozenKeepInfo;
    }
    /**
    *   获取 冻结续行
    **/
    public CbIcCompanyJudicialAssistanceFrozenKeepInfo getCompanyJudicialAssistanceFrozenKeepInfo() {
      return companyJudicialAssistanceFrozenKeepInfo;
    }
    /**
    *   设置 解除冻结
    **/
    public void setCompanyJudicialAssistanceFrozenRemInfo(CbIcCompanyJudicialAssistanceFrozenRemInfo companyJudicialAssistanceFrozenRemInfo) {
      this.companyJudicialAssistanceFrozenRemInfo = companyJudicialAssistanceFrozenRemInfo;
    }
    /**
    *   获取 解除冻结
    **/
    public CbIcCompanyJudicialAssistanceFrozenRemInfo getCompanyJudicialAssistanceFrozenRemInfo() {
      return companyJudicialAssistanceFrozenRemInfo;
    }
    /**
    *   设置 类型|状态
    **/
    public void setTypeState(String typeState) {
      this.typeState = typeState;
    }
    /**
    *   获取 类型|状态
    **/
    public String getTypeState() {
      return typeState;
    }
    /**
    *   设置 执行法院
    **/
    public void setExecutiveCourt(String executiveCourt) {
      this.executiveCourt = executiveCourt;
    }
    /**
    *   获取 执行法院
    **/
    public String getExecutiveCourt() {
      return executiveCourt;
    }
    /**
    *   设置 冻结
    **/
    public void setCompanyJudicialAssistanceFrozenInfo(CbIcCompanyJudicialAssistanceFrozenInfo companyJudicialAssistanceFrozenInfo) {
      this.companyJudicialAssistanceFrozenInfo = companyJudicialAssistanceFrozenInfo;
    }
    /**
    *   获取 冻结
    **/
    public CbIcCompanyJudicialAssistanceFrozenInfo getCompanyJudicialAssistanceFrozenInfo() {
      return companyJudicialAssistanceFrozenInfo;
    }
    /**
    *   设置 股权变更
    **/
    public void setCompanyJudicialShareholderChangeInfo(CbIcCompanyJudicialShareholderChangeInfo companyJudicialShareholderChangeInfo) {
      this.companyJudicialShareholderChangeInfo = companyJudicialShareholderChangeInfo;
    }
    /**
    *   获取 股权变更
    **/
    public CbIcCompanyJudicialShareholderChangeInfo getCompanyJudicialShareholderChangeInfo() {
      return companyJudicialShareholderChangeInfo;
    }



}

