package com.gitee.deament.tianyancha.core.remote.mrcreditchina.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrcreditchina.entity.MrCreditChinaItems;
/**
*行政处罚-其他来源
* 可以通过公司名称或ID获取企业行政处罚信息，行政处罚信息包括行政处罚明细、行政处罚公告、行政处罚内容、公示日期、处罚依据、处罚状态等字段的详细信息
* /services/open/mr/creditChina/2.0
*@author deament
**/

public class MrCreditChina implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MrCreditChinaItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MrCreditChinaItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MrCreditChinaItems> getItems() {
      return items;
    }



}

