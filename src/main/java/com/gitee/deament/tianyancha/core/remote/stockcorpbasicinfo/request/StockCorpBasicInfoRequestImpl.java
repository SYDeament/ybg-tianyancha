package com.gitee.deament.tianyancha.core.remote.stockcorpbasicinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockcorpbasicinfo.entity.StockCorpBasicInfo;
import com.gitee.deament.tianyancha.core.remote.stockcorpbasicinfo.dto.StockCorpBasicInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*重要人员
* 可以通过公司名称或ID获取上市公司重要人员信息，重要人员信息包括总经理、法人代表、董秘、董事长、证券事务代表、独立董事等
* /services/open/stock/corpBasicInfo/2.0
*@author deament
**/
@Component("stockCorpBasicInfoRequestImpl")
public class StockCorpBasicInfoRequestImpl extends BaseRequestImpl<StockCorpBasicInfo,StockCorpBasicInfoDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/corpBasicInfo/2.0";
    }
}

