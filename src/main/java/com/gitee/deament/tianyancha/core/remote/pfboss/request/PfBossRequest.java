package com.gitee.deament.tianyancha.core.remote.pfboss.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfboss.entity.PfBoss;
import com.gitee.deament.tianyancha.core.remote.pfboss.dto.PfBossDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*高管信息
* 可以通过公司名称或ID获取相关私募基金高管信息，包括法定代表人/执行事物合伙人姓名、是否有从业资格、资格取得方式等字段的相关信息
* /services/open/pf/boss/2.0
*@author deament
**/

public interface PfBossRequest extends BaseRequest<PfBossDTO ,PfBoss>{

}

