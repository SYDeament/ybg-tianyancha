package com.gitee.deament.tianyancha.core.remote.stockbonusinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockbonusinfo.entity.StockBonusInfoItems;
/**
*分红情况
* 可以通过公司名称或ID获取上市公司分红情况信息，分红情况信息包括董事会日期、股东大会日期、实施日期、分红方案说明、分红率等
* /services/open/stock/bonusInfo/2.0
*@author deament
**/

public class StockBonusInfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<StockBonusInfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<StockBonusInfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<StockBonusInfoItems> getItems() {
      return items;
    }



}

