package com.gitee.deament.tianyancha.core.remote.proximity.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*天眼地图
* 附近公司
* /services/v4/open/proximity
*@author deament
**/

@TYCURL(value="/services/v4/open/proximity")
public class ProximityDTO implements Serializable{

    /**
     *    经营状态
     *
    **/
    @ParamRequire(require = false)
    private String regStatus;
    /**
     *    离标记点距离（单位km）
     *
    **/
    @ParamRequire(require = true)
    private Long distance;
    /**
     *    纬度
     *
    **/
    @ParamRequire(require = true)
    private Long latitude;
    /**
     *    经度
     *
    **/
    @ParamRequire(require = true)
    private Long longtitude;
    /**
     *    每页条数（默认10，最大10）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    当前页(默认1)
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 离标记点距离（单位km）
    **/
    public void setDistance(Long distance) {
      this.distance = distance;
    }
    /**
    *   获取 离标记点距离（单位km）
    **/
    public Long getDistance() {
      return distance;
    }
    /**
    *   设置 纬度
    **/
    public void setLatitude(Long latitude) {
      this.latitude = latitude;
    }
    /**
    *   获取 纬度
    **/
    public Long getLatitude() {
      return latitude;
    }
    /**
    *   设置 经度
    **/
    public void setLongtitude(Long longtitude) {
      this.longtitude = longtitude;
    }
    /**
    *   获取 经度
    **/
    public Long getLongtitude() {
      return longtitude;
    }
    /**
    *   设置 每页条数（默认10，最大10）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认10，最大10）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 当前页(默认1)
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页(默认1)
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

