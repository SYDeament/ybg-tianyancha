package com.gitee.deament.tianyancha.core.remote.icannualreport.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreport;
import com.gitee.deament.tianyancha.core.remote.icannualreport.dto.IcAnnualreportDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*企业年报
* 可以通过公司名称或ID获取企业年报，企业年报包括企业基本信息、股东及出资信息、企业资产状况信息、对外投资信息等字段的详细信息
* /services/open/ic/annualreport/2.0
*@author deament
**/
@Component("icAnnualreportRequestImpl")
public class IcAnnualreportRequestImpl extends BaseRequestImpl<IcAnnualreport,IcAnnualreportDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ic/annualreport/2.0";
    }
}

