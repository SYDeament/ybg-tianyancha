package com.gitee.deament.tianyancha.core.remote.cdfindjingpin.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*竞品信息
* 属于CdFindJingpin
* /services/open/cd/findJingpin/2.0
*@author deament
**/

public class CdFindJingpinItems implements Serializable{

    /**
     *    时间
    **/
    @JSONField(name="date")
    private Long date;
    /**
     *    产品
    **/
    @JSONField(name="product")
    private String product;
    /**
     *    公司名
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    logo
    **/
    @JSONField(name="icon")
    private String icon;
    /**
     *    投资时间
    **/
    @JSONField(name="setupDate")
    private Long setupDate;
    /**
     *    业务范围
    **/
    @JSONField(name="yewu")
    private String yewu;
    /**
     *    竞品名
    **/
    @JSONField(name="jingpinProduct")
    private String jingpinProduct;
    /**
     *    当前轮次
    **/
    @JSONField(name="round")
    private String round;
    /**
     *    地区
    **/
    @JSONField(name="location")
    private String location;
    /**
     *    公司id
    **/
    @JSONField(name="graphId")
    private Integer graphId;
    /**
     *    行业
    **/
    @JSONField(name="hangye")
    private String hangye;
    /**
     *    估值
    **/
    @JSONField(name="value")
    private String value;
    /**
     *    logo存放位置
    **/
    @JSONField(name="iconOssPath")
    private String iconOssPath;


    /**
    *   设置 时间
    **/
    public void setDate(Long date) {
      this.date = date;
    }
    /**
    *   获取 时间
    **/
    public Long getDate() {
      return date;
    }
    /**
    *   设置 产品
    **/
    public void setProduct(String product) {
      this.product = product;
    }
    /**
    *   获取 产品
    **/
    public String getProduct() {
      return product;
    }
    /**
    *   设置 公司名
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 logo
    **/
    public void setIcon(String icon) {
      this.icon = icon;
    }
    /**
    *   获取 logo
    **/
    public String getIcon() {
      return icon;
    }
    /**
    *   设置 投资时间
    **/
    public void setSetupDate(Long setupDate) {
      this.setupDate = setupDate;
    }
    /**
    *   获取 投资时间
    **/
    public Long getSetupDate() {
      return setupDate;
    }
    /**
    *   设置 业务范围
    **/
    public void setYewu(String yewu) {
      this.yewu = yewu;
    }
    /**
    *   获取 业务范围
    **/
    public String getYewu() {
      return yewu;
    }
    /**
    *   设置 竞品名
    **/
    public void setJingpinProduct(String jingpinProduct) {
      this.jingpinProduct = jingpinProduct;
    }
    /**
    *   获取 竞品名
    **/
    public String getJingpinProduct() {
      return jingpinProduct;
    }
    /**
    *   设置 当前轮次
    **/
    public void setRound(String round) {
      this.round = round;
    }
    /**
    *   获取 当前轮次
    **/
    public String getRound() {
      return round;
    }
    /**
    *   设置 地区
    **/
    public void setLocation(String location) {
      this.location = location;
    }
    /**
    *   获取 地区
    **/
    public String getLocation() {
      return location;
    }
    /**
    *   设置 公司id
    **/
    public void setGraphId(Integer graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getGraphId() {
      return graphId;
    }
    /**
    *   设置 行业
    **/
    public void setHangye(String hangye) {
      this.hangye = hangye;
    }
    /**
    *   获取 行业
    **/
    public String getHangye() {
      return hangye;
    }
    /**
    *   设置 估值
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 估值
    **/
    public String getValue() {
      return value;
    }
    /**
    *   设置 logo存放位置
    **/
    public void setIconOssPath(String iconOssPath) {
      this.iconOssPath = iconOssPath;
    }
    /**
    *   获取 logo存放位置
    **/
    public String getIconOssPath() {
      return iconOssPath;
    }



}

