package com.gitee.deament.tianyancha.core.remote.stockprofit.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockprofit.entity.StockProfitCorpFinancialYears;
import com.gitee.deament.tianyancha.core.remote.stockprofit.entity.StockProfitCorpProfit;
/**
*利润表
* 可以通过公司名称或ID获取上市公司利润表数据，利润表数据包括营业总收入、营业总成本、其他经营收益、营业利润等
* /services/open/stock/profit/2.0
*@author deament
**/

public class StockProfit implements Serializable{

    /**
     *    
    **/
    @JSONField(name="corpFinancialYears")
    private List<StockProfitCorpFinancialYears> corpFinancialYears;
    /**
     *    
    **/
    @JSONField(name="corpProfit")
    private List<StockProfitCorpProfit> corpProfit;


    /**
    *   设置 
    **/
    public void setCorpFinancialYears(List<StockProfitCorpFinancialYears> corpFinancialYears) {
      this.corpFinancialYears = corpFinancialYears;
    }
    /**
    *   获取 
    **/
    public List<StockProfitCorpFinancialYears> getCorpFinancialYears() {
      return corpFinancialYears;
    }
    /**
    *   设置 
    **/
    public void setCorpProfit(List<StockProfitCorpProfit> corpProfit) {
      this.corpProfit = corpProfit;
    }
    /**
    *   获取 
    **/
    public List<StockProfitCorpProfit> getCorpProfit() {
      return corpProfit;
    }



}

