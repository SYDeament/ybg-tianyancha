package com.gitee.deament.tianyancha.core.remote.equityratio.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.equityratio.entity.EquityRatio;
import com.gitee.deament.tianyancha.core.remote.equityratio.dto.EquityRatioDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*股权结构图
* 可以通过公司ID获取企业股权控制结构及疑似实际控制人
* /services/v4/open/equityRatio
*@author deament
**/

public interface EquityRatioRequest extends BaseRequest<EquityRatioDTO ,EquityRatio>{

}

