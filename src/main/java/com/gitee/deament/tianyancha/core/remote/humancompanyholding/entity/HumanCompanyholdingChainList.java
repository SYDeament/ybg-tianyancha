package com.gitee.deament.tianyancha.core.remote.humancompanyholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*人员控股企业
* 属于HumanCompanyholding
* /services/open/human/companyholding/2.0
*@author deament
**/

public class HumanCompanyholdingChainList implements Serializable{

    /**
     *    
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    
    **/
    @JSONField(name="value")
    private String value;


    /**
    *   设置 
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 
    **/
    public String getValue() {
      return value;
    }



}

