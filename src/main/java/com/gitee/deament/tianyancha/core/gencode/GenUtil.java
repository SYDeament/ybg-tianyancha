package com.gitee.deament.tianyancha.core.gencode;

public class GenUtil {
    //首字母大写
    public static String captureName(String name) {
        if (name.length() <= 0) {
            return name;
        }
        name = name.substring(0, 1).toUpperCase() + name.substring(1);
        return name;

    }
    //首字母小写
    public static String LowName(String name) {
        if (name.length() <= 0) {
            return name;
        }
        name = name.substring(0, 1).toLowerCase() + name.substring(1);
        return name;

    }


}
