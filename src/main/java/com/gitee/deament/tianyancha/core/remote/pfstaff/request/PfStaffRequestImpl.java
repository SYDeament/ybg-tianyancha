package com.gitee.deament.tianyancha.core.remote.pfstaff.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfstaff.entity.PfStaff;
import com.gitee.deament.tianyancha.core.remote.pfstaff.dto.PfStaffDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*高管情况
* 可以通过公司名称或ID获取相关私募基金高管情况，包括高管姓名、是否有从业资格等字段的相关信息
* /services/open/pf/staff/2.0
*@author deament
**/
@Component("pfStaffRequestImpl")
public class PfStaffRequestImpl extends BaseRequestImpl<PfStaff,PfStaffDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/pf/staff/2.0";
    }
}

