package com.gitee.deament.tianyancha.core.remote.jrdishonest.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrdishonest.entity.JrDishonest;
import com.gitee.deament.tianyancha.core.remote.jrdishonest.dto.JrDishonestDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*失信人
* 可以通过公司名称或ID判定企业失信情况，失信信息包括失信人名称、组织机构代码、履行情况、失信行为具体情形等字段的详细信息
* /services/open/jr/dishonest/2.0
*@author deament
**/

public interface JrDishonestRequest extends BaseRequest<JrDishonestDTO ,JrDishonest>{

}

