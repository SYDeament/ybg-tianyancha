package com.gitee.deament.tianyancha.core.remote.riskriskinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.riskriskinfo.entity.RiskRiskInfo;
import com.gitee.deament.tianyancha.core.remote.riskriskinfo.dto.RiskRiskInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业天眼风险
* 可以通过公司名称或ID获取企业相关天眼风险列表，包括企业自身/周边/预警风险信息
* /services/open/risk/riskInfo/2.0
*@author deament
**/

public interface RiskRiskInfoRequest extends BaseRequest<RiskRiskInfoDTO ,RiskRiskInfo>{

}

