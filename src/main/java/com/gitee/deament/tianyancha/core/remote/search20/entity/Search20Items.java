package com.gitee.deament.tianyancha.core.remote.search20.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*搜索
* 属于Search20
* /services/open/search/2.0
*@author deament
**/

public class Search20Items implements Serializable{

    /**
     *    注册号
    **/
    @JSONField(name="regNumber")
    private String regNumber;
    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblishTime")
    private String estiblishTime;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    公司类型 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    @JSONField(name="companyType")
    private Integer companyType;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    组织机构代码
    **/
    @JSONField(name="orgNumber")
    private String orgNumber;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    省份
    **/
    @JSONField(name="base")
    private String base;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;


    /**
    *   设置 注册号
    **/
    public void setRegNumber(String regNumber) {
      this.regNumber = regNumber;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNumber() {
      return regNumber;
    }
    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblishTime(String estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立日期
    **/
    public String getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 公司类型 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public void setCompanyType(Integer companyType) {
      this.companyType = companyType;
    }
    /**
    *   获取 公司类型 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public Integer getCompanyType() {
      return companyType;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 组织机构代码
    **/
    public void setOrgNumber(String orgNumber) {
      this.orgNumber = orgNumber;
    }
    /**
    *   获取 组织机构代码
    **/
    public String getOrgNumber() {
      return orgNumber;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 省份
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份
    **/
    public String getBase() {
      return base;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }



}

