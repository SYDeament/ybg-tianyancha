package com.gitee.deament.tianyancha.core.remote.mweibo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业微博
* 属于MWeibo
* /services/open/m/weibo/2.0
*@author deament
**/

public class MWeiboItems implements Serializable{

    /**
     *    图标
    **/
    @JSONField(name="ico")
    private String ico;
    /**
     *    微博名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    URL
    **/
    @JSONField(name="href")
    private String href;
    /**
     *    简介
    **/
    @JSONField(name="info")
    private String info;
    /**
     *    行业类别
    **/
    @JSONField(name="tags")
    private List<String> tags;


    /**
    *   设置 图标
    **/
    public void setIco(String ico) {
      this.ico = ico;
    }
    /**
    *   获取 图标
    **/
    public String getIco() {
      return ico;
    }
    /**
    *   设置 微博名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 微博名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 URL
    **/
    public void setHref(String href) {
      this.href = href;
    }
    /**
    *   获取 URL
    **/
    public String getHref() {
      return href;
    }
    /**
    *   设置 简介
    **/
    public void setInfo(String info) {
      this.info = info;
    }
    /**
    *   获取 简介
    **/
    public String getInfo() {
      return info;
    }
    /**
    *   设置 行业类别
    **/
    public void setTags(List<String> tags) {
      this.tags = tags;
    }
    /**
    *   获取 行业类别
    **/
    public List<String> getTags() {
      return tags;
    }



}

