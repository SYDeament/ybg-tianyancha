package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype11 implements Serializable{

    /**
     *    
    **/
    @JSONField(name="hid")
    private Integer hid;
    /**
     *    纳税人识别号
    **/
    @JSONField(name="taxIdNumber")
    private String taxIdNumber;
    /**
     *    当前新发生欠税余额
    **/
    @JSONField(name="newOwnTaxBalance")
    private String newOwnTaxBalance;
    /**
     *    发布日期
    **/
    @JSONField(name="publishDate")
    private Long publishDate;
    /**
     *    欠税金额
    **/
    @JSONField(name="ownTaxAmount")
    private String ownTaxAmount;
    /**
     *    欠税余额
    **/
    @JSONField(name="ownTaxBalance")
    private String ownTaxBalance;
    /**
     *    证件号码
    **/
    @JSONField(name="personIdNumber")
    private String personIdNumber;
    /**
     *    欠税税种
    **/
    @JSONField(name="taxCategory")
    private String taxCategory;
    /**
     *    纳税人类型
    **/
    @JSONField(name="taxpayerType")
    private String taxpayerType;
    /**
     *    法人证件名称
    **/
    @JSONField(name="personIdName")
    private String personIdName;
    /**
     *    纳税人名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    经营地点
    **/
    @JSONField(name="location")
    private String location;
    /**
     *    税款所属税务机关
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    
    **/
    @JSONField(name="regType")
    private String regType;
    /**
     *    法人或负责人名称
    **/
    @JSONField(name="legalpersonName")
    private String legalpersonName;
    /**
     *    税务类型:0国税，1地税
    **/
    @JSONField(name="TYPE")
    private String TYPE;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 
    **/
    public void setHid(Integer hid) {
      this.hid = hid;
    }
    /**
    *   获取 
    **/
    public Integer getHid() {
      return hid;
    }
    /**
    *   设置 纳税人识别号
    **/
    public void setTaxIdNumber(String taxIdNumber) {
      this.taxIdNumber = taxIdNumber;
    }
    /**
    *   获取 纳税人识别号
    **/
    public String getTaxIdNumber() {
      return taxIdNumber;
    }
    /**
    *   设置 当前新发生欠税余额
    **/
    public void setNewOwnTaxBalance(String newOwnTaxBalance) {
      this.newOwnTaxBalance = newOwnTaxBalance;
    }
    /**
    *   获取 当前新发生欠税余额
    **/
    public String getNewOwnTaxBalance() {
      return newOwnTaxBalance;
    }
    /**
    *   设置 发布日期
    **/
    public void setPublishDate(Long publishDate) {
      this.publishDate = publishDate;
    }
    /**
    *   获取 发布日期
    **/
    public Long getPublishDate() {
      return publishDate;
    }
    /**
    *   设置 欠税金额
    **/
    public void setOwnTaxAmount(String ownTaxAmount) {
      this.ownTaxAmount = ownTaxAmount;
    }
    /**
    *   获取 欠税金额
    **/
    public String getOwnTaxAmount() {
      return ownTaxAmount;
    }
    /**
    *   设置 欠税余额
    **/
    public void setOwnTaxBalance(String ownTaxBalance) {
      this.ownTaxBalance = ownTaxBalance;
    }
    /**
    *   获取 欠税余额
    **/
    public String getOwnTaxBalance() {
      return ownTaxBalance;
    }
    /**
    *   设置 证件号码
    **/
    public void setPersonIdNumber(String personIdNumber) {
      this.personIdNumber = personIdNumber;
    }
    /**
    *   获取 证件号码
    **/
    public String getPersonIdNumber() {
      return personIdNumber;
    }
    /**
    *   设置 欠税税种
    **/
    public void setTaxCategory(String taxCategory) {
      this.taxCategory = taxCategory;
    }
    /**
    *   获取 欠税税种
    **/
    public String getTaxCategory() {
      return taxCategory;
    }
    /**
    *   设置 纳税人类型
    **/
    public void setTaxpayerType(String taxpayerType) {
      this.taxpayerType = taxpayerType;
    }
    /**
    *   获取 纳税人类型
    **/
    public String getTaxpayerType() {
      return taxpayerType;
    }
    /**
    *   设置 法人证件名称
    **/
    public void setPersonIdName(String personIdName) {
      this.personIdName = personIdName;
    }
    /**
    *   获取 法人证件名称
    **/
    public String getPersonIdName() {
      return personIdName;
    }
    /**
    *   设置 纳税人名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 纳税人名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 经营地点
    **/
    public void setLocation(String location) {
      this.location = location;
    }
    /**
    *   获取 经营地点
    **/
    public String getLocation() {
      return location;
    }
    /**
    *   设置 税款所属税务机关
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 税款所属税务机关
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 
    **/
    public void setRegType(String regType) {
      this.regType = regType;
    }
    /**
    *   获取 
    **/
    public String getRegType() {
      return regType;
    }
    /**
    *   设置 法人或负责人名称
    **/
    public void setLegalpersonName(String legalpersonName) {
      this.legalpersonName = legalpersonName;
    }
    /**
    *   获取 法人或负责人名称
    **/
    public String getLegalpersonName() {
      return legalpersonName;
    }
    /**
    *   设置 税务类型:0国税，1地税
    **/
    public void setTYPE(String TYPE) {
      this.TYPE = TYPE;
    }
    /**
    *   获取 税务类型:0国税，1地税
    **/
    public String getTYPE() {
      return TYPE;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

