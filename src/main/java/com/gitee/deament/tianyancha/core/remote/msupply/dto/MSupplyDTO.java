package com.gitee.deament.tianyancha.core.remote.msupply.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*供应商
* 可以通过公司名称或ID获取企业相关供应商信息，企业相关供应商信息包括供应商、采购占比、采购金额、报告期等字段的详细信息
* /services/open/m/supply/2.0
*@author deament
**/

@TYCURL(value="/services/open/m/supply/2.0")
public class MSupplyDTO implements Serializable{

    /**
     *    年份（默认所有年份）
     *
    **/
    @ParamRequire(require = false)
    private String year;
    /**
     *    公司名
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数，默认20，最大20
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    当前页（默认1，最大100）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 年份（默认所有年份）
    **/
    public void setYear(String year) {
      this.year = year;
    }
    /**
    *   获取 年份（默认所有年份）
    **/
    public String getYear() {
      return year;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数，默认20，最大20
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数，默认20，最大20
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 当前页（默认1，最大100）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1，最大100）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

