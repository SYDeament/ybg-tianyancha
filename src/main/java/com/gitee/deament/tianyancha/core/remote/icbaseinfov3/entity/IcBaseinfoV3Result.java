package com.gitee.deament.tianyancha.core.remote.icbaseinfov3.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业基本信息（含主要人员）
* 属于IcBaseinfoV3
* /services/open/ic/baseinfoV3/2.0
*@author deament
**/

public class IcBaseinfoV3Result implements Serializable{

    /**
     *    主要人员名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    人或公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    1-公司 2-人
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    职位
    **/
    @JSONField(name="typeJoin")
    private List<String> typeJoin;


    /**
    *   设置 主要人员名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 主要人员名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 人或公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 人或公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 1-公司 2-人
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 1-公司 2-人
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 职位
    **/
    public void setTypeJoin(List<String> typeJoin) {
      this.typeJoin = typeJoin;
    }
    /**
    *   获取 职位
    **/
    public List<String> getTypeJoin() {
      return typeJoin;
    }



}

