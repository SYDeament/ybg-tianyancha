package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDetail implements Serializable{

    /**
     *    对应表id
    **/
    @JSONField(name="jid")
    private Integer jid;
    /**
     *    起拍价
    **/
    @JSONField(name="initial_price")
    private Integer initial_price;
    /**
     *    标的名称
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    评估价
    **/
    @JSONField(name="consult_price")
    private Integer consult_price;


    /**
    *   设置 对应表id
    **/
    public void setJid(Integer jid) {
      this.jid = jid;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getJid() {
      return jid;
    }
    /**
    *   设置 起拍价
    **/
    public void setInitial_price(Integer initial_price) {
      this.initial_price = initial_price;
    }
    /**
    *   获取 起拍价
    **/
    public Integer getInitial_price() {
      return initial_price;
    }
    /**
    *   设置 标的名称
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标的名称
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 评估价
    **/
    public void setConsult_price(Integer consult_price) {
      this.consult_price = consult_price;
    }
    /**
    *   获取 评估价
    **/
    public Integer getConsult_price() {
      return consult_price;
    }



}

