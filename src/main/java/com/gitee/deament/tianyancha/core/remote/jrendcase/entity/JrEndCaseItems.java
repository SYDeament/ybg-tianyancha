package com.gitee.deament.tianyancha.core.remote.jrendcase.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*终本案件
* 属于JrEndCase
* /services/open/jr/endCase/2.0
*@author deament
**/

public class JrEndCaseItems implements Serializable{

    /**
     *    案号
    **/
    @JSONField(name="caseCode")
    private String caseCode;
    /**
     *    终本日期
    **/
    @JSONField(name="caseFinalTime")
    private Long caseFinalTime;
    /**
     *    终本案件详情唯一ID
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    立案时间
    **/
    @JSONField(name="caseCreateTime")
    private Long caseCreateTime;
    /**
     *    执行法院
    **/
    @JSONField(name="execCourtName")
    private String execCourtName;
    /**
     *    执行标的
    **/
    @JSONField(name="execMoney")
    private String execMoney;
    /**
     *    被执行人名称
    **/
    @JSONField(name="zname")
    private String zname;
    /**
     *    企业id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 案号
    **/
    public void setCaseCode(String caseCode) {
      this.caseCode = caseCode;
    }
    /**
    *   获取 案号
    **/
    public String getCaseCode() {
      return caseCode;
    }
    /**
    *   设置 终本日期
    **/
    public void setCaseFinalTime(Long caseFinalTime) {
      this.caseFinalTime = caseFinalTime;
    }
    /**
    *   获取 终本日期
    **/
    public Long getCaseFinalTime() {
      return caseFinalTime;
    }
    /**
    *   设置 终本案件详情唯一ID
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 终本案件详情唯一ID
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 立案时间
    **/
    public void setCaseCreateTime(Long caseCreateTime) {
      this.caseCreateTime = caseCreateTime;
    }
    /**
    *   获取 立案时间
    **/
    public Long getCaseCreateTime() {
      return caseCreateTime;
    }
    /**
    *   设置 执行法院
    **/
    public void setExecCourtName(String execCourtName) {
      this.execCourtName = execCourtName;
    }
    /**
    *   获取 执行法院
    **/
    public String getExecCourtName() {
      return execCourtName;
    }
    /**
    *   设置 执行标的
    **/
    public void setExecMoney(String execMoney) {
      this.execMoney = execMoney;
    }
    /**
    *   获取 执行标的
    **/
    public String getExecMoney() {
      return execMoney;
    }
    /**
    *   设置 被执行人名称
    **/
    public void setZname(String zname) {
      this.zname = zname;
    }
    /**
    *   获取 被执行人名称
    **/
    public String getZname() {
      return zname;
    }
    /**
    *   设置 企业id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 企业id
    **/
    public Integer getCid() {
      return cid;
    }



}

