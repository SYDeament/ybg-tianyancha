package com.gitee.deament.tianyancha.core.remote.equityratio.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.equityratio.entity.EquityRatioPath;
import com.gitee.deament.tianyancha.core.remote.equityratio.entity.EquityRatioChildren;
import com.gitee.deament.tianyancha.core.remote.equityratio.entity.EquityRatioStructure;
/**
*股权结构图
* 可以通过公司ID获取企业股权控制结构及疑似实际控制人
* /services/v4/open/equityRatio
*@author deament
**/

public class EquityRatio implements Serializable{

    /**
     *    主要控股
    **/
    @JSONField(name="path")
    private List<EquityRatioPath> path;
    /**
     *    股权结构
    **/
    @JSONField(name="structure")
    private EquityRatioStructure structure;


    /**
    *   设置 主要控股
    **/
    public void setPath(List<EquityRatioPath> path) {
      this.path = path;
    }
    /**
    *   获取 主要控股
    **/
    public List<EquityRatioPath> getPath() {
      return path;
    }
    /**
    *   设置 股权结构
    **/
    public void setStructure(EquityRatioStructure structure) {
      this.structure = structure;
    }
    /**
    *   获取 股权结构
    **/
    public EquityRatioStructure getStructure() {
      return structure;
    }



}

