package com.gitee.deament.tianyancha.core.remote.icannualreport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业年报
* 属于IcAnnualreport
* /services/open/ic/annualreport/2.0
*@author deament
**/

public class IcAnnualreportChangeRecordList implements Serializable{

    /**
     *    变更时间
    **/
    @JSONField(name="changeTime")
    private String changeTime;
    /**
     *    年份
    **/
    @JSONField(name="reportYear")
    private String reportYear;
    /**
     *    变更后
    **/
    @JSONField(name="contentAfter")
    private String contentAfter;
    /**
     *    变更前
    **/
    @JSONField(name="contentBefore")
    private String contentBefore;
    /**
     *    变更事项
    **/
    @JSONField(name="changeItem")
    private String changeItem;


    /**
    *   设置 变更时间
    **/
    public void setChangeTime(String changeTime) {
      this.changeTime = changeTime;
    }
    /**
    *   获取 变更时间
    **/
    public String getChangeTime() {
      return changeTime;
    }
    /**
    *   设置 年份
    **/
    public void setReportYear(String reportYear) {
      this.reportYear = reportYear;
    }
    /**
    *   获取 年份
    **/
    public String getReportYear() {
      return reportYear;
    }
    /**
    *   设置 变更后
    **/
    public void setContentAfter(String contentAfter) {
      this.contentAfter = contentAfter;
    }
    /**
    *   获取 变更后
    **/
    public String getContentAfter() {
      return contentAfter;
    }
    /**
    *   设置 变更前
    **/
    public void setContentBefore(String contentBefore) {
      this.contentBefore = contentBefore;
    }
    /**
    *   获取 变更前
    **/
    public String getContentBefore() {
      return contentBefore;
    }
    /**
    *   设置 变更事项
    **/
    public void setChangeItem(String changeItem) {
      this.changeItem = changeItem;
    }
    /**
    *   获取 变更事项
    **/
    public String getChangeItem() {
      return changeItem;
    }



}

