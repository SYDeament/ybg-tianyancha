package com.gitee.deament.tianyancha.core.remote.iprcopyreg.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprcopyreg.entity.IprCopyReg;
import com.gitee.deament.tianyancha.core.remote.iprcopyreg.dto.IprCopyRegDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*软件著作权
* 可以通过公司名称或ID获取软件著作权的有关信息，包括软件名称、登记号、分类号、版本号等字段的详细信息
* /services/open/ipr/copyReg/2.0
*@author deament
**/
@Component("iprCopyRegRequestImpl")
public class IprCopyRegRequestImpl extends BaseRequestImpl<IprCopyReg,IprCopyRegDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ipr/copyReg/2.0";
    }
}

