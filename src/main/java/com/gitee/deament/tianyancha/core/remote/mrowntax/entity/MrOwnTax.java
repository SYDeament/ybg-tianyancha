package com.gitee.deament.tianyancha.core.remote.mrowntax.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrowntax.entity.MrOwnTaxItems;
/**
*欠税公告
* 可以通过公司名称或ID获取企业欠税信息，企业欠税信息包括欠税公告、纳税人识别号、证件号码、经营地点、欠税税种、欠税余额等字段的详细信息
* /services/open/mr/ownTax/2.0
*@author deament
**/

public class MrOwnTax implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MrOwnTaxItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MrOwnTaxItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MrOwnTaxItems> getItems() {
      return items;
    }



}

