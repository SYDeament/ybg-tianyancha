package com.gitee.deament.tianyancha.core.remote.proximity.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.proximity.entity.Proximity;
import com.gitee.deament.tianyancha.core.remote.proximity.dto.ProximityDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*天眼地图
* 附近公司
* /services/v4/open/proximity
*@author deament
**/
@Component("proximityRequestImpl")
public class ProximityRequestImpl extends BaseRequestImpl<Proximity,ProximityDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/v4/open/proximity";
    }
}

