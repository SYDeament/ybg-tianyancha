package com.gitee.deament.tianyancha.core.remote.cdfindjingpin.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdfindjingpin.entity.CdFindJingpinItems;
/**
*竞品信息
* 可以通过公司名称或ID获取企业竞品信息，企业竞品信息包括竞品名称、行业、业务等字段的详细信息
* /services/open/cd/findJingpin/2.0
*@author deament
**/

public class CdFindJingpin implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CdFindJingpinItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CdFindJingpinItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CdFindJingpinItems> getItems() {
      return items;
    }



}

