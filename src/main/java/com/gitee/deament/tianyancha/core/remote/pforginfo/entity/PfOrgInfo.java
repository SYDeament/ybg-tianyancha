package com.gitee.deament.tianyancha.core.remote.pforginfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*机构信息
* 可以通过公司名称或ID获取相关私募基金信息，包括私募基金管理人名称、法定代表人/执行事务合伙人、机构类型、登记编号、成立日期等字段的相关信息
* /services/open/pf/orgInfo/2.0
*@author deament
**/

public class PfOrgInfo implements Serializable{

    /**
     *    机构诚信信息
    **/
    @JSONField(name="org_integrity_info")
    private String org_integrity_info;
    /**
     *    办公地址
    **/
    @JSONField(name="office_addr")
    private String office_addr;
    /**
     *    登记编号
    **/
    @JSONField(name="reg_no")
    private String reg_no;
    /**
     *    实缴资本（万元）（人民币）
    **/
    @JSONField(name="pay_capital")
    private String pay_capital;
    /**
     *    机构类型
    **/
    @JSONField(name="org_type")
    private String org_type;
    /**
     *    注册资本（万元）（人民币）
    **/
    @JSONField(name="reg_capital")
    private String reg_capital;
    /**
     *    注册资本实缴比例
    **/
    @JSONField(name="pay_capital_scale")
    private String pay_capital_scale;
    /**
     *    企业性质
    **/
    @JSONField(name="company_nature")
    private String company_nature;
    /**
     *    企业id
    **/
    @JSONField(name="graph_id")
    private Integer graph_id;
    /**
     *    基金管理人全称（英文）
    **/
    @JSONField(name="manager_name_en")
    private String manager_name_en;
    /**
     *    机构网址
    **/
    @JSONField(name="org_website")
    private String org_website;
    /**
     *    登记时间
    **/
    @JSONField(name="reg_date")
    private String reg_date;
    /**
     *    基金管理人全称（中文）
    **/
    @JSONField(name="manager_name")
    private String manager_name;
    /**
     *    业务类型
    **/
    @JSONField(name="business_type")
    private String business_type;
    /**
     *    成立时间
    **/
    @JSONField(name="est_date")
    private String est_date;
    /**
     *    注册地址
    **/
    @JSONField(name="reg_addr")
    private String reg_addr;
    /**
     *    组织机构代码
    **/
    @JSONField(name="org_no")
    private String org_no;


    /**
    *   设置 机构诚信信息
    **/
    public void setOrg_integrity_info(String org_integrity_info) {
      this.org_integrity_info = org_integrity_info;
    }
    /**
    *   获取 机构诚信信息
    **/
    public String getOrg_integrity_info() {
      return org_integrity_info;
    }
    /**
    *   设置 办公地址
    **/
    public void setOffice_addr(String office_addr) {
      this.office_addr = office_addr;
    }
    /**
    *   获取 办公地址
    **/
    public String getOffice_addr() {
      return office_addr;
    }
    /**
    *   设置 登记编号
    **/
    public void setReg_no(String reg_no) {
      this.reg_no = reg_no;
    }
    /**
    *   获取 登记编号
    **/
    public String getReg_no() {
      return reg_no;
    }
    /**
    *   设置 实缴资本（万元）（人民币）
    **/
    public void setPay_capital(String pay_capital) {
      this.pay_capital = pay_capital;
    }
    /**
    *   获取 实缴资本（万元）（人民币）
    **/
    public String getPay_capital() {
      return pay_capital;
    }
    /**
    *   设置 机构类型
    **/
    public void setOrg_type(String org_type) {
      this.org_type = org_type;
    }
    /**
    *   获取 机构类型
    **/
    public String getOrg_type() {
      return org_type;
    }
    /**
    *   设置 注册资本（万元）（人民币）
    **/
    public void setReg_capital(String reg_capital) {
      this.reg_capital = reg_capital;
    }
    /**
    *   获取 注册资本（万元）（人民币）
    **/
    public String getReg_capital() {
      return reg_capital;
    }
    /**
    *   设置 注册资本实缴比例
    **/
    public void setPay_capital_scale(String pay_capital_scale) {
      this.pay_capital_scale = pay_capital_scale;
    }
    /**
    *   获取 注册资本实缴比例
    **/
    public String getPay_capital_scale() {
      return pay_capital_scale;
    }
    /**
    *   设置 企业性质
    **/
    public void setCompany_nature(String company_nature) {
      this.company_nature = company_nature;
    }
    /**
    *   获取 企业性质
    **/
    public String getCompany_nature() {
      return company_nature;
    }
    /**
    *   设置 企业id
    **/
    public void setGraph_id(Integer graph_id) {
      this.graph_id = graph_id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getGraph_id() {
      return graph_id;
    }
    /**
    *   设置 基金管理人全称（英文）
    **/
    public void setManager_name_en(String manager_name_en) {
      this.manager_name_en = manager_name_en;
    }
    /**
    *   获取 基金管理人全称（英文）
    **/
    public String getManager_name_en() {
      return manager_name_en;
    }
    /**
    *   设置 机构网址
    **/
    public void setOrg_website(String org_website) {
      this.org_website = org_website;
    }
    /**
    *   获取 机构网址
    **/
    public String getOrg_website() {
      return org_website;
    }
    /**
    *   设置 登记时间
    **/
    public void setReg_date(String reg_date) {
      this.reg_date = reg_date;
    }
    /**
    *   获取 登记时间
    **/
    public String getReg_date() {
      return reg_date;
    }
    /**
    *   设置 基金管理人全称（中文）
    **/
    public void setManager_name(String manager_name) {
      this.manager_name = manager_name;
    }
    /**
    *   获取 基金管理人全称（中文）
    **/
    public String getManager_name() {
      return manager_name;
    }
    /**
    *   设置 业务类型
    **/
    public void setBusiness_type(String business_type) {
      this.business_type = business_type;
    }
    /**
    *   获取 业务类型
    **/
    public String getBusiness_type() {
      return business_type;
    }
    /**
    *   设置 成立时间
    **/
    public void setEst_date(String est_date) {
      this.est_date = est_date;
    }
    /**
    *   获取 成立时间
    **/
    public String getEst_date() {
      return est_date;
    }
    /**
    *   设置 注册地址
    **/
    public void setReg_addr(String reg_addr) {
      this.reg_addr = reg_addr;
    }
    /**
    *   获取 注册地址
    **/
    public String getReg_addr() {
      return reg_addr;
    }
    /**
    *   设置 组织机构代码
    **/
    public void setOrg_no(String org_no) {
      this.org_no = org_no;
    }
    /**
    *   获取 组织机构代码
    **/
    public String getOrg_no() {
      return org_no;
    }



}

