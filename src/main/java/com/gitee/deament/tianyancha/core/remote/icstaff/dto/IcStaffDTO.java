package com.gitee.deament.tianyancha.core.remote.icstaff.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*主要人员
* 可以通过公司名称或ID获取企业主要人员信息，主要人员信息包括董事、监事、高级管理人员姓名、职位、主要人员总数等字段的详细信息
* /services/open/ic/staff/2.0
*@author deament
**/

@TYCURL(value="/services/open/ic/staff/2.0")
public class IcStaffDTO implements Serializable{

    /**
     *    公司名称（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    每页条数（默认20条，最大20条）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    公司id（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;
    /**
     *    当前页数（默认第一页）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 公司名称（id与name只需输入其中一个）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（id与name只需输入其中一个）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（默认20条，最大20条）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20条，最大20条）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 公司id（id与name只需输入其中一个）
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id（id与name只需输入其中一个）
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }
    /**
    *   设置 当前页数（默认第一页）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页数（默认第一页）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

