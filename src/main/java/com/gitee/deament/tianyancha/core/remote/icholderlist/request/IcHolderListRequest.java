package com.gitee.deament.tianyancha.core.remote.icholderlist.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icholderlist.entity.IcHolderList;
import com.gitee.deament.tianyancha.core.remote.icholderlist.dto.IcHolderListDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*公司公示-股东出资
* 可以通过公司名称或ID获取股东及出自信息，股东及出资信息包括股东名、出资比例、出资金额、股东总数等字段的详细信息
* /services/open/ic/holderList/2.0
*@author deament
**/

public interface IcHolderListRequest extends BaseRequest<IcHolderListDTO ,IcHolderList>{

}

