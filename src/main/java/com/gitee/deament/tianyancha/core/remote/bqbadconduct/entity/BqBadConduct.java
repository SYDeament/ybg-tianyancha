package com.gitee.deament.tianyancha.core.remote.bqbadconduct.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.bqbadconduct.entity.BqBadConductItems;
/**
*建筑资质-不良行为
* 可以通过公司名称或ID获取有建筑资质企业的不良行为信息，包括诚信记录编号、查看事由、实施部门（文号）、发布有效期、决定内容等字段的相关信息
* /services/open/bq/badConduct/2.0
*@author deament
**/

public class BqBadConduct implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<BqBadConductItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<BqBadConductItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<BqBadConductItems> getItems() {
      return items;
    }



}

