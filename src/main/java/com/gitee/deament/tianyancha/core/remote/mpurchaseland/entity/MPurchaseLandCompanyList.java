package com.gitee.deament.tianyancha.core.remote.mpurchaseland.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*购地信息
* 属于MPurchaseLand
* /services/open/m/purchaseLand/2.0
*@author deament
**/

public class MPurchaseLandCompanyList implements Serializable{

    /**
     *    土地使用权人
    **/
    @JSONField(name="landUseRightPerson")
    private String landUseRightPerson;
    /**
     *    公司名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    int(10)
    **/
    @JSONField(name="id")
    private Integer id;


    /**
    *   设置 土地使用权人
    **/
    public void setLandUseRightPerson(String landUseRightPerson) {
      this.landUseRightPerson = landUseRightPerson;
    }
    /**
    *   获取 土地使用权人
    **/
    public String getLandUseRightPerson() {
      return landUseRightPerson;
    }
    /**
    *   设置 公司名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 int(10)
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 int(10)
    **/
    public Integer getId() {
      return id;
    }



}

