package com.gitee.deament.tianyancha.core.remote.ichumanholding.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.ichumanholding.entity.IcHumanholding;
import com.gitee.deament.tianyancha.core.remote.ichumanholding.dto.IcHumanholdingDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*最终受益人
* 可以通过公司名称或ID获取股权向上穿透后识别的企业最终受益人，包含（一名或多名）股东姓名、持股比例，层级关系等
* /services/open/ic/humanholding/2.0
*@author deament
**/
@Component("icHumanholdingRequestImpl")
public class IcHumanholdingRequestImpl extends BaseRequestImpl<IcHumanholding,IcHumanholdingDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ic/humanholding/2.0";
    }
}

