package com.gitee.deament.tianyancha.core.remote.cdgetproductinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdgetproductinfo.entity.CdGetProductInfoItems;
/**
*企业业务
* 可以通过公司名称或ID获取企业业务信息，企业业务信息包括主要业务、业务分类、业务介绍等字段的详细信息
* /services/open/cd/getProductInfo/2.0
*@author deament
**/

public class CdGetProductInfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CdGetProductInfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CdGetProductInfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CdGetProductInfoItems> getItems() {
      return items;
    }



}

