package com.gitee.deament.tianyancha.core.remote.iprpublicwechat.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*企业微信公众号
* 可以通过公司名称或ID获取企业微信公众号的有关信息，包括企业微信公众号名称、微信号、二维码、功能介绍等字段的详细信息
* /services/open/ipr/publicWeChat/2.0
*@author deament
**/

@TYCURL(value="/services/open/ipr/publicWeChat/2.0")
public class IprPublicWeChatDTO implements Serializable{

    /**
     *    公司名称（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    当前页数（默认第一页，每页10条）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 公司名称（id与name只需输入其中一个）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（id与name只需输入其中一个）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id（id与name只需输入其中一个）
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id（id与name只需输入其中一个）
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 当前页数（默认第一页，每页10条）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页数（默认第一页，每页10条）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

