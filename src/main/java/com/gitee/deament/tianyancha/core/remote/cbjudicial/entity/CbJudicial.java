package com.gitee.deament.tianyancha.core.remote.cbjudicial.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialLawSuitList;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialPlaintiff;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialDefendant;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialKtAnnouncementList;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialZhixingList;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialCourtAnnouncementList;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialCourtRegisterList;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialSendAnnouncementList;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicialDishonestList;
/**
*司法风险
* 可以通过公司名称或ID获取包含法律诉讼、法院公告、开庭公告、失信人、被执行人、司法协助、立案信息、送达公告、终本案件、限制消费令等维度的相关信息
* /services/open/cb/judicial/2.0
*@author deament
**/

public class CbJudicial implements Serializable{

    /**
     *    诉讼
    **/
    @JSONField(name="lawSuitList")
    private List<CbJudicialLawSuitList> lawSuitList;
    /**
     *    开庭公告
    **/
    @JSONField(name="ktAnnouncementList")
    private List<CbJudicialKtAnnouncementList> ktAnnouncementList;
    /**
     *    被执行人
    **/
    @JSONField(name="zhixingList")
    private List<CbJudicialZhixingList> zhixingList;
    /**
     *    法院公告
    **/
    @JSONField(name="courtAnnouncementList")
    private List<CbJudicialCourtAnnouncementList> courtAnnouncementList;
    /**
     *    立案信息
    **/
    @JSONField(name="courtRegisterList")
    private List<CbJudicialCourtRegisterList> courtRegisterList;
    /**
     *    送达公告
    **/
    @JSONField(name="sendAnnouncementList")
    private List<CbJudicialSendAnnouncementList> sendAnnouncementList;
    /**
     *    失信人
    **/
    @JSONField(name="dishonestList")
    private List<CbJudicialDishonestList> dishonestList;


    /**
    *   设置 诉讼
    **/
    public void setLawSuitList(List<CbJudicialLawSuitList> lawSuitList) {
      this.lawSuitList = lawSuitList;
    }
    /**
    *   获取 诉讼
    **/
    public List<CbJudicialLawSuitList> getLawSuitList() {
      return lawSuitList;
    }
    /**
    *   设置 开庭公告
    **/
    public void setKtAnnouncementList(List<CbJudicialKtAnnouncementList> ktAnnouncementList) {
      this.ktAnnouncementList = ktAnnouncementList;
    }
    /**
    *   获取 开庭公告
    **/
    public List<CbJudicialKtAnnouncementList> getKtAnnouncementList() {
      return ktAnnouncementList;
    }
    /**
    *   设置 被执行人
    **/
    public void setZhixingList(List<CbJudicialZhixingList> zhixingList) {
      this.zhixingList = zhixingList;
    }
    /**
    *   获取 被执行人
    **/
    public List<CbJudicialZhixingList> getZhixingList() {
      return zhixingList;
    }
    /**
    *   设置 法院公告
    **/
    public void setCourtAnnouncementList(List<CbJudicialCourtAnnouncementList> courtAnnouncementList) {
      this.courtAnnouncementList = courtAnnouncementList;
    }
    /**
    *   获取 法院公告
    **/
    public List<CbJudicialCourtAnnouncementList> getCourtAnnouncementList() {
      return courtAnnouncementList;
    }
    /**
    *   设置 立案信息
    **/
    public void setCourtRegisterList(List<CbJudicialCourtRegisterList> courtRegisterList) {
      this.courtRegisterList = courtRegisterList;
    }
    /**
    *   获取 立案信息
    **/
    public List<CbJudicialCourtRegisterList> getCourtRegisterList() {
      return courtRegisterList;
    }
    /**
    *   设置 送达公告
    **/
    public void setSendAnnouncementList(List<CbJudicialSendAnnouncementList> sendAnnouncementList) {
      this.sendAnnouncementList = sendAnnouncementList;
    }
    /**
    *   获取 送达公告
    **/
    public List<CbJudicialSendAnnouncementList> getSendAnnouncementList() {
      return sendAnnouncementList;
    }
    /**
    *   设置 失信人
    **/
    public void setDishonestList(List<CbJudicialDishonestList> dishonestList) {
      this.dishonestList = dishonestList;
    }
    /**
    *   获取 失信人
    **/
    public List<CbJudicialDishonestList> getDishonestList() {
      return dishonestList;
    }



}

