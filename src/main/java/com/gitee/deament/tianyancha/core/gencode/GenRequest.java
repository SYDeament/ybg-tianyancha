package com.gitee.deament.tianyancha.core.gencode;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.template.engine.velocity.VelocityUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gitee.deament.tianyancha.core.GenColumn;
import org.apache.velocity.VelocityContext;

import java.io.File;
import java.util.*;

public class GenRequest {
    /**
     * 基本的包路径（）
     */
    /**
     * 子类的包路径
     */
    private static List<String> subColmonPackagePath=new ArrayList<>();
    private static String modelName;
    public static void gen(String url) {
        String result = HttpUtil.get(url);
        JSONObject jsonObject = JSONObject.parseObject(result);
        JSONArray apiListJsonArray = jsonObject.getJSONObject("data").getJSONArray("items");
        if (apiListJsonArray.size() == 0) {
            System.out.println("没有接口");
            return;
        }
        Map<String, Object> contextMap = new HashMap<>(10);

        final ResourceBundle rb = ResourceBundle.getBundle("TianyanchaGen");
        for (String key : rb.keySet()) {
            String value = rb.getString(key);
            contextMap.put(key, value);

        }
        String[] excludeUrls = rb.getString("excludeUrl").split(",");
        for (int i = 0; i < apiListJsonArray.size(); i++) {
            JSONObject apiInfo = apiListJsonArray.getJSONObject(i);
            if (!apiInfo.getString("furl").equals("/services/open/hi/ic/2.0")) {
                //  continue;
            }
            boolean excludeThisUrl=false;
            for (String excludeUrl : excludeUrls) {
                if(apiInfo.getString("furl").equals(excludeUrl)){
                    //有问题的api文档跳过
                    excludeThisUrl=true;
                   break;
                }
            }
            if(excludeThisUrl){
                continue;
            }
            contextMap.put("tianyancha", apiInfo);
            String className = getClassName(apiInfo.getString("furl"));
            contextMap.put("className", className);
            modelName=className.toLowerCase();
            contextMap.put("parent",rb.getString("parent")+"."+modelName+".request");
            JSONObject returnParam = null;
            try {
                returnParam = JSONObject.parseObject(apiInfo.getString("returnParam").replaceAll("↵", "")).getJSONObject("result").getJSONObject("_");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
                System.out.println(apiInfo.getString("furl") + "该接口含有多个返回的格式");
                System.out.println("■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■");
            }
            if (returnParam == null) {
                continue;
            }
            subColmonPackagePath.add(rb.getString("parent")+"."+modelName+".entity."+className);
            subColmonPackagePath.add(rb.getString("parent")+"."+modelName+".dto."+className+"DTO");
            contextMap.put("subColmonPackagePaths",subColmonPackagePath);

            VelocityContext context = new VelocityContext(contextMap);
            File file = new File("src/main/resources/genCode/Request.ftl");
            String gencontext = FileUtil.readUtf8String(file);
            String data = VelocityUtil.merge(gencontext, context);
            String parent = rb.getString("parent").replaceAll("\\.", "/");
            FileUtil.writeUtf8String(data, rb.getString("OutputDir") + "/" + parent +  "/"+modelName+"/" +"request"+"/"+ className + "Request.java");
            //清空等待下一个
            subColmonPackagePath.clear();
        }


    }
    //获取名称
    private static String getClassName(String fulr) {
        String module_name = fulr.split("/")[3];
        String name = fulr.split("/")[4];
        if (module_name.equalsIgnoreCase("open")) {
            module_name = "";
        }
        return (GenUtil.captureName(module_name) + GenUtil.captureName(name)).replaceAll("\\.", "");
    }
}
