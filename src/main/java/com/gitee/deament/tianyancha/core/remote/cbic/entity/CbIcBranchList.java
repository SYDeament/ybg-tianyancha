package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcBranchList implements Serializable{

    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblishTime")
    private String estiblishTime;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    企业logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    公司graphId
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    行业
    **/
    @JSONField(name="category")
    private String category;
    /**
     *    法人类型 1-人 2-公司
    **/
    @JSONField(name="personType")
    private Integer personType;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblishTime(String estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立日期
    **/
    public String getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 企业logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 企业logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 公司graphId
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司graphId
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 行业
    **/
    public void setCategory(String category) {
      this.category = category;
    }
    /**
    *   获取 行业
    **/
    public String getCategory() {
      return category;
    }
    /**
    *   设置 法人类型 1-人 2-公司
    **/
    public void setPersonType(Integer personType) {
      this.personType = personType;
    }
    /**
    *   获取 法人类型 1-人 2-公司
    **/
    public Integer getPersonType() {
      return personType;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }



}

