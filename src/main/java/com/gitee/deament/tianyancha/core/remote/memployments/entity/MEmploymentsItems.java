package com.gitee.deament.tianyancha.core.remote.memployments.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业招聘
* 属于MEmployments
* /services/open/m/employments/2.0
*@author deament
**/

public class MEmploymentsItems implements Serializable{

    /**
     *    学历
    **/
    @JSONField(name="education")
    private String education;
    /**
     *    一级分类
    **/
    @JSONField(name="jobFirstClass")
    private String jobFirstClass;
    /**
     *    所在城市
    **/
    @JSONField(name="city")
    private String city;
    /**
     *    公司名称
    **/
    @JSONField(name="companyName")
    private String companyName;
    /**
     *    工作描述
    **/
    @JSONField(name="description")
    private String description;
    /**
     *    公司名称
    **/
    @JSONField(name="companyNameMws")
    private String companyNameMws;
    /**
     *    招聘信息来源
    **/
    @JSONField(name="source")
    private String source;
    /**
     *    职位
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    经验
    **/
    @JSONField(name="experience")
    private String experience;
    /**
     *    开始时间
    **/
    @JSONField(name="startdate")
    private Long startdate;
    /**
     *    企业纬度
    **/
    @JSONField(name="companyLng")
    private String companyLng;
    /**
     *    公司状态
    **/
    @JSONField(name="paramRegStatus")
    private String paramRegStatus;
    /**
     *    企业id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    经度
    **/
    @JSONField(name="companyLat")
    private String companyLat;
    /**
     *    薪水
    **/
    @JSONField(name="oriSalary")
    private String oriSalary;
    /**
     *    成立时间
    **/
    @JSONField(name="estiblishTimeLong")
    private String estiblishTimeLong;
    /**
     *    招聘类别：全职，兼职
    **/
    @JSONField(name="class")
    private String clazz;
    /**
     *    公司地址
    **/
    @JSONField(name="companyCity")
    private String companyCity;
    /**
     *    搜索类型
    **/
    @JSONField(name="searchType")
    private String searchType;
    /**
     *    招聘信息地址url
    **/
    @JSONField(name="urlPath")
    private String urlPath;
    /**
     *    雇员数量
    **/
    @JSONField(name="employerNumber")
    private String employerNumber;
    /**
     *    注册资本
    **/
    @JSONField(name="paramRegCapital")
    private String paramRegCapital;
    /**
     *    招聘结束时间
    **/
    @JSONField(name="enddate")
    private Long enddate;
    /**
     *    注册年
    **/
    @JSONField(name="paramRegYear")
    private String paramRegYear;
    /**
     *    公司地址
    **/
    @JSONField(name="fromUrl")
    private String fromUrl;
    /**
     *    区
    **/
    @JSONField(name="district")
    private String district;
    /**
     *    工作地址
    **/
    @JSONField(name="location")
    private String location;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 学历
    **/
    public void setEducation(String education) {
      this.education = education;
    }
    /**
    *   获取 学历
    **/
    public String getEducation() {
      return education;
    }
    /**
    *   设置 一级分类
    **/
    public void setJobFirstClass(String jobFirstClass) {
      this.jobFirstClass = jobFirstClass;
    }
    /**
    *   获取 一级分类
    **/
    public String getJobFirstClass() {
      return jobFirstClass;
    }
    /**
    *   设置 所在城市
    **/
    public void setCity(String city) {
      this.city = city;
    }
    /**
    *   获取 所在城市
    **/
    public String getCity() {
      return city;
    }
    /**
    *   设置 公司名称
    **/
    public void setCompanyName(String companyName) {
      this.companyName = companyName;
    }
    /**
    *   获取 公司名称
    **/
    public String getCompanyName() {
      return companyName;
    }
    /**
    *   设置 工作描述
    **/
    public void setDescription(String description) {
      this.description = description;
    }
    /**
    *   获取 工作描述
    **/
    public String getDescription() {
      return description;
    }
    /**
    *   设置 公司名称
    **/
    public void setCompanyNameMws(String companyNameMws) {
      this.companyNameMws = companyNameMws;
    }
    /**
    *   获取 公司名称
    **/
    public String getCompanyNameMws() {
      return companyNameMws;
    }
    /**
    *   设置 招聘信息来源
    **/
    public void setSource(String source) {
      this.source = source;
    }
    /**
    *   获取 招聘信息来源
    **/
    public String getSource() {
      return source;
    }
    /**
    *   设置 职位
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 职位
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 经验
    **/
    public void setExperience(String experience) {
      this.experience = experience;
    }
    /**
    *   获取 经验
    **/
    public String getExperience() {
      return experience;
    }
    /**
    *   设置 开始时间
    **/
    public void setStartdate(Long startdate) {
      this.startdate = startdate;
    }
    /**
    *   获取 开始时间
    **/
    public Long getStartdate() {
      return startdate;
    }
    /**
    *   设置 企业纬度
    **/
    public void setCompanyLng(String companyLng) {
      this.companyLng = companyLng;
    }
    /**
    *   获取 企业纬度
    **/
    public String getCompanyLng() {
      return companyLng;
    }
    /**
    *   设置 公司状态
    **/
    public void setParamRegStatus(String paramRegStatus) {
      this.paramRegStatus = paramRegStatus;
    }
    /**
    *   获取 公司状态
    **/
    public String getParamRegStatus() {
      return paramRegStatus;
    }
    /**
    *   设置 企业id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 经度
    **/
    public void setCompanyLat(String companyLat) {
      this.companyLat = companyLat;
    }
    /**
    *   获取 经度
    **/
    public String getCompanyLat() {
      return companyLat;
    }
    /**
    *   设置 薪水
    **/
    public void setOriSalary(String oriSalary) {
      this.oriSalary = oriSalary;
    }
    /**
    *   获取 薪水
    **/
    public String getOriSalary() {
      return oriSalary;
    }
    /**
    *   设置 成立时间
    **/
    public void setEstiblishTimeLong(String estiblishTimeLong) {
      this.estiblishTimeLong = estiblishTimeLong;
    }
    /**
    *   获取 成立时间
    **/
    public String getEstiblishTimeLong() {
      return estiblishTimeLong;
    }
    /**
    *   设置 招聘类别：全职，兼职
    **/
    public void setClazz(String clazz) {
      this.clazz = clazz;
    }
    /**
    *   获取 招聘类别：全职，兼职
    **/
    public String getClazz() {
      return clazz;
    }
    /**
    *   设置 公司地址
    **/
    public void setCompanyCity(String companyCity) {
      this.companyCity = companyCity;
    }
    /**
    *   获取 公司地址
    **/
    public String getCompanyCity() {
      return companyCity;
    }
    /**
    *   设置 搜索类型
    **/
    public void setSearchType(String searchType) {
      this.searchType = searchType;
    }
    /**
    *   获取 搜索类型
    **/
    public String getSearchType() {
      return searchType;
    }
    /**
    *   设置 招聘信息地址url
    **/
    public void setUrlPath(String urlPath) {
      this.urlPath = urlPath;
    }
    /**
    *   获取 招聘信息地址url
    **/
    public String getUrlPath() {
      return urlPath;
    }
    /**
    *   设置 雇员数量
    **/
    public void setEmployerNumber(String employerNumber) {
      this.employerNumber = employerNumber;
    }
    /**
    *   获取 雇员数量
    **/
    public String getEmployerNumber() {
      return employerNumber;
    }
    /**
    *   设置 注册资本
    **/
    public void setParamRegCapital(String paramRegCapital) {
      this.paramRegCapital = paramRegCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getParamRegCapital() {
      return paramRegCapital;
    }
    /**
    *   设置 招聘结束时间
    **/
    public void setEnddate(Long enddate) {
      this.enddate = enddate;
    }
    /**
    *   获取 招聘结束时间
    **/
    public Long getEnddate() {
      return enddate;
    }
    /**
    *   设置 注册年
    **/
    public void setParamRegYear(String paramRegYear) {
      this.paramRegYear = paramRegYear;
    }
    /**
    *   获取 注册年
    **/
    public String getParamRegYear() {
      return paramRegYear;
    }
    /**
    *   设置 公司地址
    **/
    public void setFromUrl(String fromUrl) {
      this.fromUrl = fromUrl;
    }
    /**
    *   获取 公司地址
    **/
    public String getFromUrl() {
      return fromUrl;
    }
    /**
    *   设置 区
    **/
    public void setDistrict(String district) {
      this.district = district;
    }
    /**
    *   获取 区
    **/
    public String getDistrict() {
      return district;
    }
    /**
    *   设置 工作地址
    **/
    public void setLocation(String location) {
      this.location = location;
    }
    /**
    *   获取 工作地址
    **/
    public String getLocation() {
      return location;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }



}

