package com.gitee.deament.tianyancha.core.remote.stockcashflow.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*现金流量表
* 属于StockCashFlow
* /services/open/stock/cashFlow/2.0
*@author deament
**/

public class StockCashFlowCorpFinancialYears implements Serializable{

    /**
     *    年度
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    值
    **/
    @JSONField(name="value")
    private String value;


    /**
    *   设置 年度
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 年度
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 值
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 值
    **/
    public String getValue() {
      return value;
    }



}

