package com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*上市公司企业简介
* 属于StockCompanyInfo
* /services/open/stock/companyInfo/2.0
*@author deament
**/

public class StockCompanyInfoSecretaries implements Serializable{

    /**
     *    类型 1公司 2 人
    **/
    @JSONField(name="cType")
    private Integer cType;
    /**
     *    董秘
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    董秘id
    **/
    @JSONField(name="id")
    private String id;


    /**
    *   设置 类型 1公司 2 人
    **/
    public void setCType(Integer cType) {
      this.cType = cType;
    }
    /**
    *   获取 类型 1公司 2 人
    **/
    public Integer getCType() {
      return cType;
    }
    /**
    *   设置 董秘
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 董秘
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 董秘id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 董秘id
    **/
    public String getId() {
      return id;
    }



}

