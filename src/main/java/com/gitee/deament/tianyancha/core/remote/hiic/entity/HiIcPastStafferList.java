package com.gitee.deament.tianyancha.core.remote.hiic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史工商信息
* 属于HiIc
* /services/open/hi/ic/2.0
*@author deament
**/

public class HiIcPastStafferList implements Serializable{

    /**
     *    
    **/
    @JSONField(name="toco")
    private Integer toco;
    /**
     *    
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    
    **/
    @JSONField(name="time")
    private Integer time;
    /**
     *    
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    
    **/
    @JSONField(name="relation")
    private Integer relation;


    /**
    *   设置 
    **/
    public void setToco(Integer toco) {
      this.toco = toco;
    }
    /**
    *   获取 
    **/
    public Integer getToco() {
      return toco;
    }
    /**
    *   设置 
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 
    **/
    public void setTime(Integer time) {
      this.time = time;
    }
    /**
    *   获取 
    **/
    public Integer getTime() {
      return time;
    }
    /**
    *   设置 
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 
    **/
    public void setRelation(Integer relation) {
      this.relation = relation;
    }
    /**
    *   获取 
    **/
    public Integer getRelation() {
      return relation;
    }



}

