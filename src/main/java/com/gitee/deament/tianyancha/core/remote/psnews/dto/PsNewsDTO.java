package com.gitee.deament.tianyancha.core.remote.psnews.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*新闻舆情
* 根据公司id或公司名称（精确匹配）获取新闻列表
* /services/open/ps/news/2.0
*@author deament
**/

@TYCURL(value="/services/open/ps/news/2.0")
public class PsNewsDTO implements Serializable{

    /**
     *    公司名称（精确匹配）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    条数（最大20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    开始时间
     *
    **/
    @ParamRequire(require = true)
    private String startTime;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    结束时间
     *
    **/
    @ParamRequire(require = true)
    private String endTime;
    /**
     *    页数（默认第一页）
     *
    **/
    @ParamRequire(require = true)
    private Long pageNum;
    /**
     *    标签（多个逗号分隔，默认返回全部，标签内容参考文档tags.pdf）
     *
    **/
    @ParamRequire(require = false)
    private String tags;


    /**
    *   设置 公司名称（精确匹配）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（精确匹配）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 条数（最大20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 条数（最大20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 开始时间
    **/
    public void setStartTime(String startTime) {
      this.startTime = startTime;
    }
    /**
    *   获取 开始时间
    **/
    public String getStartTime() {
      return startTime;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 结束时间
    **/
    public void setEndTime(String endTime) {
      this.endTime = endTime;
    }
    /**
    *   获取 结束时间
    **/
    public String getEndTime() {
      return endTime;
    }
    /**
    *   设置 页数（默认第一页）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 页数（默认第一页）
    **/
    public Long getPageNum() {
      return pageNum;
    }
    /**
    *   设置 标签（多个逗号分隔，默认返回全部，标签内容参考文档tags.pdf）
    **/
    public void setTags(String tags) {
      this.tags = tags;
    }
    /**
    *   获取 标签（多个逗号分隔，默认返回全部，标签内容参考文档tags.pdf）
    **/
    public String getTags() {
      return tags;
    }



}

