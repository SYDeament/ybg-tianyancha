package com.gitee.deament.tianyancha.core.remote.getpledgereg.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*知识产权出质
* 属于GetPledgeReg
* /services/v4/open/getPledgeReg
*@author deament
**/

public class GetPledgeRegItems implements Serializable{

    /**
     *    出质人
    **/
    @JSONField(name="pledgorName")
    private String pledgorName;
    /**
     *    名称
    **/
    @JSONField(name="iprName")
    private String iprName;
    /**
     *    出质人类型 1-公司 2-人
    **/
    @JSONField(name="pledgorType")
    private String pledgorType;
    /**
     *    质权人id
    **/
    @JSONField(name="pledgeeHid")
    private Integer pledgeeHid;
    /**
     *    质权登记期限
    **/
    @JSONField(name="pledgeRegPeriod")
    private String pledgeRegPeriod;
    /**
     *    质权人公司id
    **/
    @JSONField(name="pledgeeCid")
    private Integer pledgeeCid;
    /**
     *    出质人公司id
    **/
    @JSONField(name="pledgorCid")
    private Integer pledgorCid;
    /**
     *    知识产权登记证号
    **/
    @JSONField(name="iprCertificateNum")
    private String iprCertificateNum;
    /**
     *    质权人类型 1-公司 2-人
    **/
    @JSONField(name="pledgeeType")
    private String pledgeeType;
    /**
     *    详情(未使用)（无法识别的字段 请手动修改）
    **/
    @JSONField(name="detailList")
    private List<Object> detailList;
    /**
     *    质权人名称
    **/
    @JSONField(name="pledgeeName")
    private String pledgeeName;
    /**
     *    对应表id
    **/
    @JSONField(name="pledgeId")
    private Integer pledgeId;
    /**
     *    状态
    **/
    @JSONField(name="state")
    private String state;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    种类
    **/
    @JSONField(name="iprType")
    private String iprType;


    /**
    *   设置 出质人
    **/
    public void setPledgorName(String pledgorName) {
      this.pledgorName = pledgorName;
    }
    /**
    *   获取 出质人
    **/
    public String getPledgorName() {
      return pledgorName;
    }
    /**
    *   设置 名称
    **/
    public void setIprName(String iprName) {
      this.iprName = iprName;
    }
    /**
    *   获取 名称
    **/
    public String getIprName() {
      return iprName;
    }
    /**
    *   设置 出质人类型 1-公司 2-人
    **/
    public void setPledgorType(String pledgorType) {
      this.pledgorType = pledgorType;
    }
    /**
    *   获取 出质人类型 1-公司 2-人
    **/
    public String getPledgorType() {
      return pledgorType;
    }
    /**
    *   设置 质权人id
    **/
    public void setPledgeeHid(Integer pledgeeHid) {
      this.pledgeeHid = pledgeeHid;
    }
    /**
    *   获取 质权人id
    **/
    public Integer getPledgeeHid() {
      return pledgeeHid;
    }
    /**
    *   设置 质权登记期限
    **/
    public void setPledgeRegPeriod(String pledgeRegPeriod) {
      this.pledgeRegPeriod = pledgeRegPeriod;
    }
    /**
    *   获取 质权登记期限
    **/
    public String getPledgeRegPeriod() {
      return pledgeRegPeriod;
    }
    /**
    *   设置 质权人公司id
    **/
    public void setPledgeeCid(Integer pledgeeCid) {
      this.pledgeeCid = pledgeeCid;
    }
    /**
    *   获取 质权人公司id
    **/
    public Integer getPledgeeCid() {
      return pledgeeCid;
    }
    /**
    *   设置 出质人公司id
    **/
    public void setPledgorCid(Integer pledgorCid) {
      this.pledgorCid = pledgorCid;
    }
    /**
    *   获取 出质人公司id
    **/
    public Integer getPledgorCid() {
      return pledgorCid;
    }
    /**
    *   设置 知识产权登记证号
    **/
    public void setIprCertificateNum(String iprCertificateNum) {
      this.iprCertificateNum = iprCertificateNum;
    }
    /**
    *   获取 知识产权登记证号
    **/
    public String getIprCertificateNum() {
      return iprCertificateNum;
    }
    /**
    *   设置 质权人类型 1-公司 2-人
    **/
    public void setPledgeeType(String pledgeeType) {
      this.pledgeeType = pledgeeType;
    }
    /**
    *   获取 质权人类型 1-公司 2-人
    **/
    public String getPledgeeType() {
      return pledgeeType;
    }
    /**
    *   设置 详情(未使用)（无法识别的字段 请手动修改）
    **/
    public void setDetailList(List<Object> detailList) {
      this.detailList = detailList;
    }
    /**
    *   获取 详情(未使用)（无法识别的字段 请手动修改）
    **/
    public List<Object> getDetailList() {
      return detailList;
    }
    /**
    *   设置 质权人名称
    **/
    public void setPledgeeName(String pledgeeName) {
      this.pledgeeName = pledgeeName;
    }
    /**
    *   获取 质权人名称
    **/
    public String getPledgeeName() {
      return pledgeeName;
    }
    /**
    *   设置 对应表id
    **/
    public void setPledgeId(Integer pledgeId) {
      this.pledgeId = pledgeId;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getPledgeId() {
      return pledgeId;
    }
    /**
    *   设置 状态
    **/
    public void setState(String state) {
      this.state = state;
    }
    /**
    *   获取 状态
    **/
    public String getState() {
      return state;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 种类
    **/
    public void setIprType(String iprType) {
      this.iprType = iprType;
    }
    /**
    *   获取 种类
    **/
    public String getIprType() {
      return iprType;
    }



}

