package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype27 implements Serializable{

    /**
     *    原告/上诉人
    **/
    @JSONField(name="plaintiffList")
    private List<RiskDetailPlaintiffList> plaintiffList;
    /**
     *    当事人gid
    **/
    @JSONField(name="litigantGids")
    private String litigantGids;
    /**
     *    当事人
    **/
    @JSONField(name="litigant")
    private String litigant;
    /**
     *    立案时间
    **/
    @JSONField(name="filingDate")
    private String filingDate;
    /**
     *    案件状态
    **/
    @JSONField(name="caseStatus")
    private String caseStatus;
    /**
     *    
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    来源
    **/
    @JSONField(name="source")
    private String source;
    /**
     *    公告内容
    **/
    @JSONField(name="content")
    private String content;
    /**
     *    案件类型
    **/
    @JSONField(name="caseType")
    private String caseType;
    /**
     *    源链接
    **/
    @JSONField(name="sourceUrl")
    private String sourceUrl;
    /**
     *    备用字段2
    **/
    @JSONField(name="property2")
    private String property2;
    /**
     *    备用字段1
    **/
    @JSONField(name="property1")
    private String property1;
    /**
     *    是否删除（默认0）
    **/
    @JSONField(name="isDeleted")
    private String isDeleted;
    /**
     *    被告/被上诉人
    **/
    @JSONField(name="defendant")
    private String defendant;
    /**
     *    开庭日期
    **/
    @JSONField(name="startTime")
    private String startTime;
    /**
     *    承办法官
    **/
    @JSONField(name="judge")
    private String judge;
    /**
     *    承办部门
    **/
    @JSONField(name="department")
    private String department;
    /**
     *    被告/被上诉人
    **/
    @JSONField(name="defendantList")
    private List<RiskDetailDefendantList> defendantList;
    /**
     *    地区
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    原告/上诉人
    **/
    @JSONField(name="plaintiff")
    private String plaintiff;
    /**
     *    法官助理
    **/
    @JSONField(name="assistant")
    private String assistant;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    案号
    **/
    @JSONField(name="caseNo")
    private String caseNo;
    /**
     *    案由
    **/
    @JSONField(name="caseReason")
    private String caseReason;
    /**
     *    结案时间
    **/
    @JSONField(name="closeDate")
    private String closeDate;
    /**
     *    第三人
    **/
    @JSONField(name="third")
    private String third;
    /**
     *    
    **/
    @JSONField(name="createTime")
    private String createTime;
    /**
     *    企业id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 原告/上诉人
    **/
    public void setPlaintiffList(List<RiskDetailPlaintiffList> plaintiffList) {
      this.plaintiffList = plaintiffList;
    }
    /**
    *   获取 原告/上诉人
    **/
    public List<RiskDetailPlaintiffList> getPlaintiffList() {
      return plaintiffList;
    }
    /**
    *   设置 当事人gid
    **/
    public void setLitigantGids(String litigantGids) {
      this.litigantGids = litigantGids;
    }
    /**
    *   获取 当事人gid
    **/
    public String getLitigantGids() {
      return litigantGids;
    }
    /**
    *   设置 当事人
    **/
    public void setLitigant(String litigant) {
      this.litigant = litigant;
    }
    /**
    *   获取 当事人
    **/
    public String getLitigant() {
      return litigant;
    }
    /**
    *   设置 立案时间
    **/
    public void setFilingDate(String filingDate) {
      this.filingDate = filingDate;
    }
    /**
    *   获取 立案时间
    **/
    public String getFilingDate() {
      return filingDate;
    }
    /**
    *   设置 案件状态
    **/
    public void setCaseStatus(String caseStatus) {
      this.caseStatus = caseStatus;
    }
    /**
    *   获取 案件状态
    **/
    public String getCaseStatus() {
      return caseStatus;
    }
    /**
    *   设置 
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 来源
    **/
    public void setSource(String source) {
      this.source = source;
    }
    /**
    *   获取 来源
    **/
    public String getSource() {
      return source;
    }
    /**
    *   设置 公告内容
    **/
    public void setContent(String content) {
      this.content = content;
    }
    /**
    *   获取 公告内容
    **/
    public String getContent() {
      return content;
    }
    /**
    *   设置 案件类型
    **/
    public void setCaseType(String caseType) {
      this.caseType = caseType;
    }
    /**
    *   获取 案件类型
    **/
    public String getCaseType() {
      return caseType;
    }
    /**
    *   设置 源链接
    **/
    public void setSourceUrl(String sourceUrl) {
      this.sourceUrl = sourceUrl;
    }
    /**
    *   获取 源链接
    **/
    public String getSourceUrl() {
      return sourceUrl;
    }
    /**
    *   设置 备用字段2
    **/
    public void setProperty2(String property2) {
      this.property2 = property2;
    }
    /**
    *   获取 备用字段2
    **/
    public String getProperty2() {
      return property2;
    }
    /**
    *   设置 备用字段1
    **/
    public void setProperty1(String property1) {
      this.property1 = property1;
    }
    /**
    *   获取 备用字段1
    **/
    public String getProperty1() {
      return property1;
    }
    /**
    *   设置 是否删除（默认0）
    **/
    public void setIsDeleted(String isDeleted) {
      this.isDeleted = isDeleted;
    }
    /**
    *   获取 是否删除（默认0）
    **/
    public String getIsDeleted() {
      return isDeleted;
    }
    /**
    *   设置 被告/被上诉人
    **/
    public void setDefendant(String defendant) {
      this.defendant = defendant;
    }
    /**
    *   获取 被告/被上诉人
    **/
    public String getDefendant() {
      return defendant;
    }
    /**
    *   设置 开庭日期
    **/
    public void setStartTime(String startTime) {
      this.startTime = startTime;
    }
    /**
    *   获取 开庭日期
    **/
    public String getStartTime() {
      return startTime;
    }
    /**
    *   设置 承办法官
    **/
    public void setJudge(String judge) {
      this.judge = judge;
    }
    /**
    *   获取 承办法官
    **/
    public String getJudge() {
      return judge;
    }
    /**
    *   设置 承办部门
    **/
    public void setDepartment(String department) {
      this.department = department;
    }
    /**
    *   获取 承办部门
    **/
    public String getDepartment() {
      return department;
    }
    /**
    *   设置 被告/被上诉人
    **/
    public void setDefendantList(List<RiskDetailDefendantList> defendantList) {
      this.defendantList = defendantList;
    }
    /**
    *   获取 被告/被上诉人
    **/
    public List<RiskDetailDefendantList> getDefendantList() {
      return defendantList;
    }
    /**
    *   设置 地区
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 地区
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 原告/上诉人
    **/
    public void setPlaintiff(String plaintiff) {
      this.plaintiff = plaintiff;
    }
    /**
    *   获取 原告/上诉人
    **/
    public String getPlaintiff() {
      return plaintiff;
    }
    /**
    *   设置 法官助理
    **/
    public void setAssistant(String assistant) {
      this.assistant = assistant;
    }
    /**
    *   获取 法官助理
    **/
    public String getAssistant() {
      return assistant;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 案号
    **/
    public void setCaseNo(String caseNo) {
      this.caseNo = caseNo;
    }
    /**
    *   获取 案号
    **/
    public String getCaseNo() {
      return caseNo;
    }
    /**
    *   设置 案由
    **/
    public void setCaseReason(String caseReason) {
      this.caseReason = caseReason;
    }
    /**
    *   获取 案由
    **/
    public String getCaseReason() {
      return caseReason;
    }
    /**
    *   设置 结案时间
    **/
    public void setCloseDate(String closeDate) {
      this.closeDate = closeDate;
    }
    /**
    *   获取 结案时间
    **/
    public String getCloseDate() {
      return closeDate;
    }
    /**
    *   设置 第三人
    **/
    public void setThird(String third) {
      this.third = third;
    }
    /**
    *   获取 第三人
    **/
    public String getThird() {
      return third;
    }
    /**
    *   设置 
    **/
    public void setCreateTime(String createTime) {
      this.createTime = createTime;
    }
    /**
    *   获取 
    **/
    public String getCreateTime() {
      return createTime;
    }
    /**
    *   设置 企业id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 企业id
    **/
    public Integer getCid() {
      return cid;
    }



}

