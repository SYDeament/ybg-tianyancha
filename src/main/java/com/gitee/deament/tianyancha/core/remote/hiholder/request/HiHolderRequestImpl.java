package com.gitee.deament.tianyancha.core.remote.hiholder.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiholder.entity.HiHolder;
import com.gitee.deament.tianyancha.core.remote.hiholder.dto.HiHolderDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史股东信息
* 可以通过公司名称或ID获取企业历史的股东信息，历史股东信息包括股东名、出资比例、认缴金额、股东总数等字段信息
* /services/open/hi/holder/2.0
*@author deament
**/
@Component("hiHolderRequestImpl")
public class HiHolderRequestImpl extends BaseRequestImpl<HiHolder,HiHolderDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/holder/2.0";
    }
}

