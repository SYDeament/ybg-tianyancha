package com.gitee.deament.tianyancha.core.remote.mlandpublicity.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mlandpublicity.entity.MLandPublicity;
import com.gitee.deament.tianyancha.core.remote.mlandpublicity.dto.MLandPublicityDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*地块公示详情
* 可以通过地块公示ID获取企业地块公示详情，企业地块公示详情包括地块位置、土地用途、面积、发布机关、联系方式等字段的详细信息
* /services/open/m/landPublicity/detail/2.0
*@author deament
**/

public interface MLandPublicityRequest extends BaseRequest<MLandPublicityDTO ,MLandPublicity>{

}

