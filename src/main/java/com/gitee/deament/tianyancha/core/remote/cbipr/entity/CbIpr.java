package com.gitee.deament.tianyancha.core.remote.cbipr.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbipr.entity.CbIprTmList;
import com.gitee.deament.tianyancha.core.remote.cbipr.entity.CbIprCopyRegList;
import com.gitee.deament.tianyancha.core.remote.cbipr.entity.CbIprCopyRegWorksList;
import com.gitee.deament.tianyancha.core.remote.cbipr.entity.CbIprIcpList;
import com.gitee.deament.tianyancha.core.remote.cbipr.entity.CbIprLawStatus;
import com.gitee.deament.tianyancha.core.remote.cbipr.entity.CbIprPatentList;
/**
*知识产权
* 可以通过公司名称或ID获取包含商标、专利、作品著作权、软件著作权、网站备案等维度的相关信息
* /services/open/cb/ipr/2.0
*@author deament
**/

public class CbIpr implements Serializable{

    /**
     *    商标
    **/
    @JSONField(name="tmList")
    private List<CbIprTmList> tmList;
    /**
     *    软件著作权
    **/
    @JSONField(name="copyRegList")
    private List<CbIprCopyRegList> copyRegList;
    /**
     *    作品著作权
    **/
    @JSONField(name="copyRegWorksList")
    private List<CbIprCopyRegWorksList> copyRegWorksList;
    /**
     *    网站备案
    **/
    @JSONField(name="icpList")
    private List<CbIprIcpList> icpList;
    /**
     *    专利
    **/
    @JSONField(name="patentList")
    private List<CbIprPatentList> patentList;


    /**
    *   设置 商标
    **/
    public void setTmList(List<CbIprTmList> tmList) {
      this.tmList = tmList;
    }
    /**
    *   获取 商标
    **/
    public List<CbIprTmList> getTmList() {
      return tmList;
    }
    /**
    *   设置 软件著作权
    **/
    public void setCopyRegList(List<CbIprCopyRegList> copyRegList) {
      this.copyRegList = copyRegList;
    }
    /**
    *   获取 软件著作权
    **/
    public List<CbIprCopyRegList> getCopyRegList() {
      return copyRegList;
    }
    /**
    *   设置 作品著作权
    **/
    public void setCopyRegWorksList(List<CbIprCopyRegWorksList> copyRegWorksList) {
      this.copyRegWorksList = copyRegWorksList;
    }
    /**
    *   获取 作品著作权
    **/
    public List<CbIprCopyRegWorksList> getCopyRegWorksList() {
      return copyRegWorksList;
    }
    /**
    *   设置 网站备案
    **/
    public void setIcpList(List<CbIprIcpList> icpList) {
      this.icpList = icpList;
    }
    /**
    *   获取 网站备案
    **/
    public List<CbIprIcpList> getIcpList() {
      return icpList;
    }
    /**
    *   设置 专利
    **/
    public void setPatentList(List<CbIprPatentList> patentList) {
      this.patentList = patentList;
    }
    /**
    *   获取 专利
    **/
    public List<CbIprPatentList> getPatentList() {
      return patentList;
    }



}

