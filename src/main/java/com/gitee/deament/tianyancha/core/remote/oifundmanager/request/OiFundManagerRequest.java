package com.gitee.deament.tianyancha.core.remote.oifundmanager.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oifundmanager.entity.OiFundManager;
import com.gitee.deament.tianyancha.core.remote.oifundmanager.dto.OiFundManagerDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*基金管理人
* 可以通过投资机构名称获取投资机构基金管理人信息，包括基金管理公司名称或ID、法人、注册资本、成立日期等字段的详细信息
* /services/open/oi/fundManager/2.0
*@author deament
**/

public interface OiFundManagerRequest extends BaseRequest<OiFundManagerDTO ,OiFundManager>{

}

