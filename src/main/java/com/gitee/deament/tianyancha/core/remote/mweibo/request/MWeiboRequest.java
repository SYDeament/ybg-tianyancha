package com.gitee.deament.tianyancha.core.remote.mweibo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mweibo.entity.MWeibo;
import com.gitee.deament.tianyancha.core.remote.mweibo.dto.MWeiboDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业微博
* 可以通过公司名称或ID获取企业微博的有关信息，包括企业微博名称、行业类别、简介等字段的详细信息
* /services/open/m/weibo/2.0
*@author deament
**/

public interface MWeiboRequest extends BaseRequest<MWeiboDTO ,MWeibo>{

}

