package com.gitee.deament.tianyancha.core.remote.investtree.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*股权穿透图
* 可以通过公司ID获取企业上下游对外投资关系，包含持股比例、直接与间接投资企业等
* /services/v3/open/investtree
*@author deament
**/

@TYCURL(value="/services/v3/open/investtree")
public class InvesttreeDTO implements Serializable{

    /**
     *    层次，最大6
     *
    **/
    @ParamRequire(require = true)
    private Long flag;
    /**
     *    公司名称（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id（id与name只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    up,down
     *
    **/
    @ParamRequire(require = true)
    private String dir;
    /**
     *    （id、name、统一信用代码，注册码，组织机构代码只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 层次，最大6
    **/
    public void setFlag(Long flag) {
      this.flag = flag;
    }
    /**
    *   获取 层次，最大6
    **/
    public Long getFlag() {
      return flag;
    }
    /**
    *   设置 公司名称（id与name只需输入其中一个）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（id与name只需输入其中一个）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id（id与name只需输入其中一个）
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id（id与name只需输入其中一个）
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 up,down
    **/
    public void setDir(String dir) {
      this.dir = dir;
    }
    /**
    *   获取 up,down
    **/
    public String getDir() {
      return dir;
    }
    /**
    *   设置 （id、name、统一信用代码，注册码，组织机构代码只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，注册码，组织机构代码只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

