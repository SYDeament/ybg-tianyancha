package com.gitee.deament.tianyancha.core.remote.iprpublicwechat.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprpublicwechat.entity.IprPublicWeChatItems;
/**
*企业微信公众号
* 可以通过公司名称或ID获取企业微信公众号的有关信息，包括企业微信公众号名称、微信号、二维码、功能介绍等字段的详细信息
* /services/open/ipr/publicWeChat/2.0
*@author deament
**/

public class IprPublicWeChat implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IprPublicWeChatItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IprPublicWeChatItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IprPublicWeChatItems> getItems() {
      return items;
    }



}

