package com.gitee.deament.tianyancha.core.remote.cbipr.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*知识产权
* 属于CbIpr
* /services/open/cb/ipr/2.0
*@author deament
**/

public class CbIprPatentList implements Serializable{

    /**
     *    代理人
    **/
    @JSONField(name="agent")
    private String agent;
    /**
     *    地址
    **/
    @JSONField(name="address")
    private String address;
    /**
     *    代理机构
    **/
    @JSONField(name="agency")
    private String agency;
    /**
     *    摘要
    **/
    @JSONField(name="abstracts")
    private String abstracts;
    /**
     *    申请人
    **/
    @JSONField(name="applicantName")
    private String applicantName;
    /**
     *    名称
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    申请号/专利号
    **/
    @JSONField(name="patentNum")
    private String patentNum;
    /**
     *    公开公告日
    **/
    @JSONField(name="pubDate")
    private String pubDate;
    /**
     *    申请公布日
    **/
    @JSONField(name="applicationPublishTime")
    private String applicationPublishTime;
    /**
     *    uuid
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    申请号
    **/
    @JSONField(name="appnumber")
    private String appnumber;
    /**
     *    专利类型
    **/
    @JSONField(name="patentType")
    private String patentType;
    /**
     *    图片url
    **/
    @JSONField(name="imgUrl")
    private String imgUrl;
    /**
     *    申请公布号
    **/
    @JSONField(name="pubnumber")
    private String pubnumber;
    /**
     *    主分类号
    **/
    @JSONField(name="mainCatNum")
    private String mainCatNum;
    /**
     *    申请日
    **/
    @JSONField(name="applicationTime")
    private String applicationTime;
    /**
     *    专利
    **/
    @JSONField(name="patentName")
    private String patentName;
    /**
     *    发明人
    **/
    @JSONField(name="inventor")
    private String inventor;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    法律状态
    **/
    @JSONField(name="lawStatus")
    private List<CbIprLawStatus> lawStatus;
    /**
     *    申请公布号
    **/
    @JSONField(name="applicationPublishNum")
    private String applicationPublishNum;
    /**
     *    全部分类号
    **/
    @JSONField(name="allCatNum")
    private String allCatNum;


    /**
    *   设置 代理人
    **/
    public void setAgent(String agent) {
      this.agent = agent;
    }
    /**
    *   获取 代理人
    **/
    public String getAgent() {
      return agent;
    }
    /**
    *   设置 地址
    **/
    public void setAddress(String address) {
      this.address = address;
    }
    /**
    *   获取 地址
    **/
    public String getAddress() {
      return address;
    }
    /**
    *   设置 代理机构
    **/
    public void setAgency(String agency) {
      this.agency = agency;
    }
    /**
    *   获取 代理机构
    **/
    public String getAgency() {
      return agency;
    }
    /**
    *   设置 摘要
    **/
    public void setAbstracts(String abstracts) {
      this.abstracts = abstracts;
    }
    /**
    *   获取 摘要
    **/
    public String getAbstracts() {
      return abstracts;
    }
    /**
    *   设置 申请人
    **/
    public void setApplicantName(String applicantName) {
      this.applicantName = applicantName;
    }
    /**
    *   获取 申请人
    **/
    public String getApplicantName() {
      return applicantName;
    }
    /**
    *   设置 名称
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 名称
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 申请号/专利号
    **/
    public void setPatentNum(String patentNum) {
      this.patentNum = patentNum;
    }
    /**
    *   获取 申请号/专利号
    **/
    public String getPatentNum() {
      return patentNum;
    }
    /**
    *   设置 公开公告日
    **/
    public void setPubDate(String pubDate) {
      this.pubDate = pubDate;
    }
    /**
    *   获取 公开公告日
    **/
    public String getPubDate() {
      return pubDate;
    }
    /**
    *   设置 申请公布日
    **/
    public void setApplicationPublishTime(String applicationPublishTime) {
      this.applicationPublishTime = applicationPublishTime;
    }
    /**
    *   获取 申请公布日
    **/
    public String getApplicationPublishTime() {
      return applicationPublishTime;
    }
    /**
    *   设置 uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 uuid
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 申请号
    **/
    public void setAppnumber(String appnumber) {
      this.appnumber = appnumber;
    }
    /**
    *   获取 申请号
    **/
    public String getAppnumber() {
      return appnumber;
    }
    /**
    *   设置 专利类型
    **/
    public void setPatentType(String patentType) {
      this.patentType = patentType;
    }
    /**
    *   获取 专利类型
    **/
    public String getPatentType() {
      return patentType;
    }
    /**
    *   设置 图片url
    **/
    public void setImgUrl(String imgUrl) {
      this.imgUrl = imgUrl;
    }
    /**
    *   获取 图片url
    **/
    public String getImgUrl() {
      return imgUrl;
    }
    /**
    *   设置 申请公布号
    **/
    public void setPubnumber(String pubnumber) {
      this.pubnumber = pubnumber;
    }
    /**
    *   获取 申请公布号
    **/
    public String getPubnumber() {
      return pubnumber;
    }
    /**
    *   设置 主分类号
    **/
    public void setMainCatNum(String mainCatNum) {
      this.mainCatNum = mainCatNum;
    }
    /**
    *   获取 主分类号
    **/
    public String getMainCatNum() {
      return mainCatNum;
    }
    /**
    *   设置 申请日
    **/
    public void setApplicationTime(String applicationTime) {
      this.applicationTime = applicationTime;
    }
    /**
    *   获取 申请日
    **/
    public String getApplicationTime() {
      return applicationTime;
    }
    /**
    *   设置 专利
    **/
    public void setPatentName(String patentName) {
      this.patentName = patentName;
    }
    /**
    *   获取 专利
    **/
    public String getPatentName() {
      return patentName;
    }
    /**
    *   设置 发明人
    **/
    public void setInventor(String inventor) {
      this.inventor = inventor;
    }
    /**
    *   获取 发明人
    **/
    public String getInventor() {
      return inventor;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 法律状态
    **/
    public void setLawStatus(List<CbIprLawStatus> lawStatus) {
      this.lawStatus = lawStatus;
    }
    /**
    *   获取 法律状态
    **/
    public List<CbIprLawStatus> getLawStatus() {
      return lawStatus;
    }
    /**
    *   设置 申请公布号
    **/
    public void setApplicationPublishNum(String applicationPublishNum) {
      this.applicationPublishNum = applicationPublishNum;
    }
    /**
    *   获取 申请公布号
    **/
    public String getApplicationPublishNum() {
      return applicationPublishNum;
    }
    /**
    *   设置 全部分类号
    **/
    public void setAllCatNum(String allCatNum) {
      this.allCatNum = allCatNum;
    }
    /**
    *   获取 全部分类号
    **/
    public String getAllCatNum() {
      return allCatNum;
    }



}

