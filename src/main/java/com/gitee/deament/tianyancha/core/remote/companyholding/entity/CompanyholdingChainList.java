package com.gitee.deament.tianyancha.core.remote.companyholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*实际控制权
* 属于Companyholding
* /services/v4/open/companyholding
*@author deament
**/

public class CompanyholdingChainList implements Serializable{

    /**
     *    
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    
    **/
    @JSONField(name="value")
    private String value;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 
    **/
    public String getValue() {
      return value;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

