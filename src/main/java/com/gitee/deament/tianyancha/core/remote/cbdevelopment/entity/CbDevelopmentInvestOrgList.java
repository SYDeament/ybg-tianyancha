package com.gitee.deament.tianyancha.core.remote.cbdevelopment.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业发展
* 属于CbDevelopment
* /services/open/cb/development/2.0
*@author deament
**/

public class CbDevelopmentInvestOrgList implements Serializable{

    /**
     *    机构名
    **/
    @JSONField(name="jigou_name")
    private String jigou_name;
    /**
     *    城市
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    机构id
    **/
    @JSONField(name="orgCode")
    private String orgCode;
    /**
     *    logo
    **/
    @JSONField(name="imgPath")
    private String imgPath;
    /**
     *    简介
    **/
    @JSONField(name="desc")
    private String desc;
    /**
     *    成立年份
    **/
    @JSONField(name="foundYear")
    private String foundYear;


    /**
    *   设置 机构名
    **/
    public void setJigou_name(String jigou_name) {
      this.jigou_name = jigou_name;
    }
    /**
    *   获取 机构名
    **/
    public String getJigou_name() {
      return jigou_name;
    }
    /**
    *   设置 城市
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 城市
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 机构id
    **/
    public void setOrgCode(String orgCode) {
      this.orgCode = orgCode;
    }
    /**
    *   获取 机构id
    **/
    public String getOrgCode() {
      return orgCode;
    }
    /**
    *   设置 logo
    **/
    public void setImgPath(String imgPath) {
      this.imgPath = imgPath;
    }
    /**
    *   获取 logo
    **/
    public String getImgPath() {
      return imgPath;
    }
    /**
    *   设置 简介
    **/
    public void setDesc(String desc) {
      this.desc = desc;
    }
    /**
    *   获取 简介
    **/
    public String getDesc() {
      return desc;
    }
    /**
    *   设置 成立年份
    **/
    public void setFoundYear(String foundYear) {
      this.foundYear = foundYear;
    }
    /**
    *   获取 成立年份
    **/
    public String getFoundYear() {
      return foundYear;
    }



}

