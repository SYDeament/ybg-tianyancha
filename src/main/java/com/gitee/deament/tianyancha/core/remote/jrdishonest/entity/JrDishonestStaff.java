package com.gitee.deament.tianyancha.core.remote.jrdishonest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*失信人
* 属于JrDishonest
* /services/open/jr/dishonest/2.0
*@author deament
**/

public class JrDishonestStaff implements Serializable{

    /**
     *    角色
    **/
    @JSONField(name="role")
    private String role;
    /**
     *    无用
    **/
    @JSONField(name="code")
    private String code;
    /**
     *    法人姓名
    **/
    @JSONField(name="name")
    private String name;


    /**
    *   设置 角色
    **/
    public void setRole(String role) {
      this.role = role;
    }
    /**
    *   获取 角色
    **/
    public String getRole() {
      return role;
    }
    /**
    *   设置 无用
    **/
    public void setCode(String code) {
      this.code = code;
    }
    /**
    *   获取 无用
    **/
    public String getCode() {
      return code;
    }
    /**
    *   设置 法人姓名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 法人姓名
    **/
    public String getName() {
      return name;
    }



}

