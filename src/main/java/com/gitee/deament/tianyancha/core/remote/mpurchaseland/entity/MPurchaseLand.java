package com.gitee.deament.tianyancha.core.remote.mpurchaseland.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mpurchaseland.entity.MPurchaseLandInstalmentPayment;
import com.gitee.deament.tianyancha.core.remote.mpurchaseland.entity.MPurchaseLandCompanyList;
import com.gitee.deament.tianyancha.core.remote.mpurchaseland.entity.MPurchaseLandItems;
/**
*购地信息
* 可以通过公司名称或ID获取企业购地信用信息，企业购地信用信息包括土地坐落、土地用途、总面积、供应方式等字段的详细信息
* /services/open/m/purchaseLand/2.0
*@author deament
**/

public class MPurchaseLand implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MPurchaseLandItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MPurchaseLandItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MPurchaseLandItems> getItems() {
      return items;
    }



}

