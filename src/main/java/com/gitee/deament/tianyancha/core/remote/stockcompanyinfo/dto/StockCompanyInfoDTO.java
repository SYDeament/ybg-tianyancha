package com.gitee.deament.tianyancha.core.remote.stockcompanyinfo.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*上市公司企业简介
* 可以通过公司名称或ID获取上市公司企业简介，上市公司企业简介包括公司全称、曾用名、工商登记号、注册资本、所属行业、主要人员、控股股东、实际控制人、最终控制人、主营业务等
* /services/open/stock/companyInfo/2.0
*@author deament
**/

@TYCURL(value="/services/open/stock/companyInfo/2.0")
public class StockCompanyInfoDTO implements Serializable{

    /**
     *    公司名称，可通过查询接口获取
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id，可通过查询接口获取
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name，统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 公司名称，可通过查询接口获取
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称，可通过查询接口获取
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id，可通过查询接口获取
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id，可通过查询接口获取
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name，统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name，统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

