package com.gitee.deament.tianyancha.core.remote.mweibo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mweibo.entity.MWeiboItems;
/**
*企业微博
* 可以通过公司名称或ID获取企业微博的有关信息，包括企业微博名称、行业类别、简介等字段的详细信息
* /services/open/m/weibo/2.0
*@author deament
**/

public class MWeibo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MWeiboItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MWeiboItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MWeiboItems> getItems() {
      return items;
    }



}

