package com.gitee.deament.tianyancha.core.remote.jrjudicialcase.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*司法解析
* 属于JrJudicialCase
* /services/open/jr/judicialCase/2.0
*@author deament
**/

public class JrJudicialCaseItems implements Serializable{

    /**
     *    案号
    **/
    @JSONField(name="caseCode")
    private String caseCode;
    /**
     *    当前审理程序日期
    **/
    @JSONField(name="trialTime")
    private String trialTime;
    /**
     *    
    **/
    @JSONField(name="caseIdentity")
    private List<String> caseIdentity;
    /**
     *    案件名称
    **/
    @JSONField(name="caseTitle")
    private String caseTitle;
    /**
     *    uuid
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    当前审理程序
    **/
    @JSONField(name="trialProcedure")
    private String trialProcedure;
    /**
     *    案由
    **/
    @JSONField(name="caseReason")
    private String caseReason;
    /**
     *    案件类型
    **/
    @JSONField(name="caseType")
    private String caseType;


    /**
    *   设置 案号
    **/
    public void setCaseCode(String caseCode) {
      this.caseCode = caseCode;
    }
    /**
    *   获取 案号
    **/
    public String getCaseCode() {
      return caseCode;
    }
    /**
    *   设置 当前审理程序日期
    **/
    public void setTrialTime(String trialTime) {
      this.trialTime = trialTime;
    }
    /**
    *   获取 当前审理程序日期
    **/
    public String getTrialTime() {
      return trialTime;
    }
    /**
    *   设置 
    **/
    public void setCaseIdentity(List<String> caseIdentity) {
      this.caseIdentity = caseIdentity;
    }
    /**
    *   获取 
    **/
    public List<String> getCaseIdentity() {
      return caseIdentity;
    }
    /**
    *   设置 案件名称
    **/
    public void setCaseTitle(String caseTitle) {
      this.caseTitle = caseTitle;
    }
    /**
    *   获取 案件名称
    **/
    public String getCaseTitle() {
      return caseTitle;
    }
    /**
    *   设置 uuid
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 uuid
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 当前审理程序
    **/
    public void setTrialProcedure(String trialProcedure) {
      this.trialProcedure = trialProcedure;
    }
    /**
    *   获取 当前审理程序
    **/
    public String getTrialProcedure() {
      return trialProcedure;
    }
    /**
    *   设置 案由
    **/
    public void setCaseReason(String caseReason) {
      this.caseReason = caseReason;
    }
    /**
    *   获取 案由
    **/
    public String getCaseReason() {
      return caseReason;
    }
    /**
    *   设置 案件类型
    **/
    public void setCaseType(String caseType) {
      this.caseType = caseType;
    }
    /**
    *   获取 案件类型
    **/
    public String getCaseType() {
      return caseType;
    }



}

