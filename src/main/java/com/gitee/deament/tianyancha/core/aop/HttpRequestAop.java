package com.gitee.deament.tianyancha.core.aop;

import com.gitee.deament.tianyancha.core.TianyanchaResult;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;


/**
 * 切面管理请求
 * @author deament
 */
@Aspect
public class HttpRequestAop implements Ordered {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Before("execution(* com.gitee.deament.tianyancha.core.base.BaseRequest.get(..))")
    public void before() {
        logger.debug("前置");
    }

    @After("execution(* com.gitee.deament.tianyancha.core.base.BaseRequest.get(..))")
    public void after() {
        logger.debug("通知方法返回或者异常后调用");
    }

    @AfterReturning("execution(* com.gitee.deament.tianyancha.core.base.BaseRequest.get(..))")
    public void afterReturning() {
        logger.debug("通知方法完成后调用");
    }

    @AfterThrowing("execution(* com.gitee.deament.tianyancha.core.base.BaseRequest.get(..))")
    public void afterThrowing() {
        logger.debug("通知方法异常后调用");
    }

    @Around("execution(* com.gitee.deament.tianyancha.core.base.BaseRequest.get(..))")
    public TianyanchaResult around(ProceedingJoinPoint pj) {
        try {
            //XXX  此处可以添加接口的拦截代码，比如次数限制 缓存等
            return (TianyanchaResult) pj.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        logger.info("环绕结束");
        return new TianyanchaResult();
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
