package com.gitee.deament.tianyancha.core.remote.mrjudicialsale.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*司法拍卖
* 属于MrJudicialSale
* /services/open/mr/judicialSale/2.0
*@author deament
**/

public class MrJudicialSaleDetail implements Serializable{

    /**
     *    对应表id
    **/
    @JSONField(name="jid")
    private Integer jid;
    /**
     *    起拍价格
    **/
    @JSONField(name="initial_price")
    private Integer initial_price;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *     评估价格
    **/
    @JSONField(name="consult_price")
    private Integer consult_price;


    /**
    *   设置 对应表id
    **/
    public void setJid(Integer jid) {
      this.jid = jid;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getJid() {
      return jid;
    }
    /**
    *   设置 起拍价格
    **/
    public void setInitial_price(Integer initial_price) {
      this.initial_price = initial_price;
    }
    /**
    *   获取 起拍价格
    **/
    public Integer getInitial_price() {
      return initial_price;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置  评估价格
    **/
    public void setConsult_price(Integer consult_price) {
      this.consult_price = consult_price;
    }
    /**
    *   获取  评估价格
    **/
    public Integer getConsult_price() {
      return consult_price;
    }



}

