package com.gitee.deament.tianyancha.core.remote.description.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业人员简介
* 可以通过公司名称或ID和人名获取人员简介信息
* /services/v4/open/description
*@author deament
**/

public class Description implements Serializable{

    /**
     *    weibo号
    **/
    @JSONField(name="weibo")
    private String weibo;
    /**
     *    头像
    **/
    @JSONField(name="headUrl")
    private String headUrl;
    /**
     *    简介
    **/
    @JSONField(name="introduction")
    private String introduction;


    /**
    *   设置 weibo号
    **/
    public void setWeibo(String weibo) {
      this.weibo = weibo;
    }
    /**
    *   获取 weibo号
    **/
    public String getWeibo() {
      return weibo;
    }
    /**
    *   设置 头像
    **/
    public void setHeadUrl(String headUrl) {
      this.headUrl = headUrl;
    }
    /**
    *   获取 头像
    **/
    public String getHeadUrl() {
      return headUrl;
    }
    /**
    *   设置 简介
    **/
    public void setIntroduction(String introduction) {
      this.introduction = introduction;
    }
    /**
    *   获取 简介
    **/
    public String getIntroduction() {
      return introduction;
    }



}

