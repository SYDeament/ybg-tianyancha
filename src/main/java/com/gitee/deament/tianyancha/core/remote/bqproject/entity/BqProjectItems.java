package com.gitee.deament.tianyancha.core.remote.bqproject.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-工程项目
* 属于BqProject
* /services/open/bq/project/2.0
*@author deament
**/

public class BqProjectItems implements Serializable{

    /**
     *    项目编号
    **/
    @JSONField(name="proNum")
    private String proNum;
    /**
     *    项目类别
    **/
    @JSONField(name="proType")
    private String proType;
    /**
     *    建设单位
    **/
    @JSONField(name="buildCompany")
    private String buildCompany;
    /**
     *    项目id
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    项目名称
    **/
    @JSONField(name="proName")
    private String proName;
    /**
     *    项目属地
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 项目编号
    **/
    public void setProNum(String proNum) {
      this.proNum = proNum;
    }
    /**
    *   获取 项目编号
    **/
    public String getProNum() {
      return proNum;
    }
    /**
    *   设置 项目类别
    **/
    public void setProType(String proType) {
      this.proType = proType;
    }
    /**
    *   获取 项目类别
    **/
    public String getProType() {
      return proType;
    }
    /**
    *   设置 建设单位
    **/
    public void setBuildCompany(String buildCompany) {
      this.buildCompany = buildCompany;
    }
    /**
    *   获取 建设单位
    **/
    public String getBuildCompany() {
      return buildCompany;
    }
    /**
    *   设置 项目id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 项目id
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 项目名称
    **/
    public void setProName(String proName) {
      this.proName = proName;
    }
    /**
    *   获取 项目名称
    **/
    public String getProName() {
      return proName;
    }
    /**
    *   设置 项目属地
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 项目属地
    **/
    public String getBase() {
      return base;
    }



}

