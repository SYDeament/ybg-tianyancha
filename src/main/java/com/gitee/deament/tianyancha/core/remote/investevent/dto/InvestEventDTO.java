package com.gitee.deament.tianyancha.core.remote.investevent.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*投资动态
* 可以通过投资机构名称获取投资产品名称、产品logo、投资金额、融资轮次、相关新闻等字段的详细信息
* /services/v4/open/investEvent
*@author deament
**/

@TYCURL(value="/services/v4/open/investEvent")
public class InvestEventDTO implements Serializable{

    /**
     *    机构名
     *
    **/
    @ParamRequire(require = true)
    private String name;
    /**
     *    每页条数（默认20，最大20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    当前页（默认1）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 机构名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 机构名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 每页条数（默认20，最大20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20，最大20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 当前页（默认1）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

