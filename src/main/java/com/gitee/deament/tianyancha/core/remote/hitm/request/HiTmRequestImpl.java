package com.gitee.deament.tianyancha.core.remote.hitm.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hitm.entity.HiTm;
import com.gitee.deament.tianyancha.core.remote.hitm.dto.HiTmDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史商标信息
* 可以通过公司名称或ID获取历史的商标有关信息，历史商标信息包括商标图片、注册号、国际分类等字段信息
* /services/open/hi/tm/2.0
*@author deament
**/
@Component("hiTmRequestImpl")
public class HiTmRequestImpl extends BaseRequestImpl<HiTm,HiTmDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/tm/2.0";
    }
}

