package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*特殊企业基本信息
* 属于XgbaseinfoV2
* /services/v4/open/xgbaseinfoV2
*@author deament
**/

public class XgbaseinfoV2LegalInfo implements Serializable{

    /**
     *    拥有公司数量
    **/
    @JSONField(name="companyNum")
    private Integer companyNum;
    /**
     *    id
    **/
    @JSONField(name="hid")
    private String hid;
    /**
     *    弃用
    **/
    @JSONField(name="partners")
    private String partners;
    /**
     *    名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    头像Url
    **/
    @JSONField(name="headUrl")
    private String headUrl;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    
    **/
    @JSONField(name="office")
    private List<XgbaseinfoV2Office> office;
    /**
     *    
    **/
    @JSONField(name="cid")
    private Integer cid;
    /**
     *    职位
    **/
    @JSONField(name="typeJoin")
    private String typeJoin;


    /**
    *   设置 拥有公司数量
    **/
    public void setCompanyNum(Integer companyNum) {
      this.companyNum = companyNum;
    }
    /**
    *   获取 拥有公司数量
    **/
    public Integer getCompanyNum() {
      return companyNum;
    }
    /**
    *   设置 id
    **/
    public void setHid(String hid) {
      this.hid = hid;
    }
    /**
    *   获取 id
    **/
    public String getHid() {
      return hid;
    }
    /**
    *   设置 弃用
    **/
    public void setPartners(String partners) {
      this.partners = partners;
    }
    /**
    *   获取 弃用
    **/
    public String getPartners() {
      return partners;
    }
    /**
    *   设置 名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 头像Url
    **/
    public void setHeadUrl(String headUrl) {
      this.headUrl = headUrl;
    }
    /**
    *   获取 头像Url
    **/
    public String getHeadUrl() {
      return headUrl;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 
    **/
    public void setOffice(List<XgbaseinfoV2Office> office) {
      this.office = office;
    }
    /**
    *   获取 
    **/
    public List<XgbaseinfoV2Office> getOffice() {
      return office;
    }
    /**
    *   设置 
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 
    **/
    public Integer getCid() {
      return cid;
    }
    /**
    *   设置 职位
    **/
    public void setTypeJoin(String typeJoin) {
      this.typeJoin = typeJoin;
    }
    /**
    *   获取 职位
    **/
    public String getTypeJoin() {
      return typeJoin;
    }



}

