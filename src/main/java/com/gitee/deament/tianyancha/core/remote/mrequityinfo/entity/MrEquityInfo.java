package com.gitee.deament.tianyancha.core.remote.mrequityinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrequityinfo.entity.MrEquityInfoCompanyList;
import com.gitee.deament.tianyancha.core.remote.mrequityinfo.entity.MrEquityInfoPledgeeList;
import com.gitee.deament.tianyancha.core.remote.mrequityinfo.entity.MrEquityInfoPledgorList;
import com.gitee.deament.tianyancha.core.remote.mrequityinfo.entity.MrEquityInfoItems;
/**
*股权出质
* 可以通过公司名称或ID获取出质股权标的企业信息，包括企业质权人信息、出质人信息、出质股权数额等字段的详细信息
* /services/open/mr/equityInfo/2.0
*@author deament
**/

public class MrEquityInfo implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MrEquityInfoItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MrEquityInfoItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MrEquityInfoItems> getItems() {
      return items;
    }



}

