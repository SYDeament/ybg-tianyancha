package com.gitee.deament.tianyancha.core.remote.hizhixing.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hizhixing.entity.HiZhixing;
import com.gitee.deament.tianyancha.core.remote.hizhixing.dto.HiZhixingDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*历史被执行人
* 可以通过公司名称或ID获取企业历史的被执行人信息，历史被执行人信息包括执行法院、案件内容、执行标的、被执行人名称、组织机构代码等字段信息
* /services/open/hi/zhixing/2.0
*@author deament
**/

public interface HiZhixingRequest extends BaseRequest<HiZhixingDTO ,HiZhixing>{

}

