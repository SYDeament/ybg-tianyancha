package com.gitee.deament.tianyancha.core.remote.himembers.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*历史主要人员
* 可以通过公司名称或ID获取历史主要人员信息，历史主要人员信息包括法人名、变更时间、主要人员名、变更时间、股东名、退股时间等字段的信息
* /services/open/hi/members
*@author deament
**/

@TYCURL(value="/services/open/hi/members")
public class HiMembersDTO implements Serializable{

    /**
     *    公司名称（与公司id必须输入其中之一）
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    公司id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 公司名称（与公司id必须输入其中之一）
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称（与公司id必须输入其中之一）
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

