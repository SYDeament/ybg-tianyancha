package com.gitee.deament.tianyancha.core.remote.pfproduct.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfproduct.entity.PfProductBefore;
import com.gitee.deament.tianyancha.core.remote.pfproduct.entity.PfProductAfter;
/**
*产品信息
* 可以通过公司名称或ID获取相关私募基金产品信息，包括暂行办法实施前/后成立的基金产品名称、基金编号等字段的相关信息
* /services/open/pf/product/2.0
*@author deament
**/

public class PfProduct implements Serializable{

    /**
     *    暂行办法实施前成立的基金
    **/
    @JSONField(name="before")
    private List<PfProductBefore> before;
    /**
     *    总数
    **/
    @JSONField(name="count")
    private Integer count;
    /**
     *    暂行办法实施后成立的基金
    **/
    @JSONField(name="after")
    private List<PfProductAfter> after;
    /**
     *    企业id
    **/
    @JSONField(name="graph_id")
    private Integer graph_id;


    /**
    *   设置 暂行办法实施前成立的基金
    **/
    public void setBefore(List<PfProductBefore> before) {
      this.before = before;
    }
    /**
    *   获取 暂行办法实施前成立的基金
    **/
    public List<PfProductBefore> getBefore() {
      return before;
    }
    /**
    *   设置 总数
    **/
    public void setCount(Integer count) {
      this.count = count;
    }
    /**
    *   获取 总数
    **/
    public Integer getCount() {
      return count;
    }
    /**
    *   设置 暂行办法实施后成立的基金
    **/
    public void setAfter(List<PfProductAfter> after) {
      this.after = after;
    }
    /**
    *   获取 暂行办法实施后成立的基金
    **/
    public List<PfProductAfter> getAfter() {
      return after;
    }
    /**
    *   设置 企业id
    **/
    public void setGraph_id(Integer graph_id) {
      this.graph_id = graph_id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getGraph_id() {
      return graph_id;
    }



}

