package com.gitee.deament.tianyancha.core.remote.icactualcontrol.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.entity.IcActualControl;
import com.gitee.deament.tianyancha.core.remote.icactualcontrol.dto.IcActualControlDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*疑似实际控制人
* 可以通过公司名称或ID获取股权向上穿透后识别的企业疑似实际控制人，包含（一名）疑似控制人姓名、层级关系等字段信息
* /services/open/ic/actualControl/2.0
*@author deament
**/
@Component("icActualControlRequestImpl")
public class IcActualControlRequestImpl extends BaseRequestImpl<IcActualControl,IcActualControlDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ic/actualControl/2.0";
    }
}

