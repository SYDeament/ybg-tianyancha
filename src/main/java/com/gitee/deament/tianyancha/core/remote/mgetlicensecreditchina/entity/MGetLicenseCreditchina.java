package com.gitee.deament.tianyancha.core.remote.mgetlicensecreditchina.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mgetlicensecreditchina.entity.MGetLicenseCreditchinaItems;
/**
*行政许可-其他来源
* 可以通过公司名称或ID获取企业行政许可信息，企业行政许可信息包括行政许可决定文书号、许可内容、许可机关、审核类型等字段的详细信息
* /services/open/m/getLicenseCreditchina/2.0
*@author deament
**/

public class MGetLicenseCreditchina implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MGetLicenseCreditchinaItems> items;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MGetLicenseCreditchinaItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MGetLicenseCreditchinaItems> getItems() {
      return items;
    }



}

