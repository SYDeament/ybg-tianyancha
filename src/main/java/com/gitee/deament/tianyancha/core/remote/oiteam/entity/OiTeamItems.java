package com.gitee.deament.tianyancha.core.remote.oiteam.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*机构成员
* 属于OiTeam
* /services/open/oi/team/2.0
*@author deament
**/

public class OiTeamItems implements Serializable{

    /**
     *    成员id
    **/
    @JSONField(name="hid")
    private Integer hid;
    /**
     *    姓名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    机构logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    职位
    **/
    @JSONField(name="position")
    private String position;
    /**
     *    描述
    **/
    @JSONField(name="desc")
    private String desc;
    /**
     *    企业id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 成员id
    **/
    public void setHid(Integer hid) {
      this.hid = hid;
    }
    /**
    *   获取 成员id
    **/
    public Integer getHid() {
      return hid;
    }
    /**
    *   设置 姓名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 姓名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 机构logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 机构logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 职位
    **/
    public void setPosition(String position) {
      this.position = position;
    }
    /**
    *   获取 职位
    **/
    public String getPosition() {
      return position;
    }
    /**
    *   设置 描述
    **/
    public void setDesc(String desc) {
      this.desc = desc;
    }
    /**
    *   获取 描述
    **/
    public String getDesc() {
      return desc;
    }
    /**
    *   设置 企业id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 企业id
    **/
    public Integer getCid() {
      return cid;
    }



}

