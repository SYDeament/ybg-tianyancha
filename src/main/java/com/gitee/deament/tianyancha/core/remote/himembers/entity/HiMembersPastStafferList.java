package com.gitee.deament.tianyancha.core.remote.himembers.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史主要人员
* 属于HiMembers
* /services/open/hi/members
*@author deament
**/

public class HiMembersPastStafferList implements Serializable{

    /**
     *    姓名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    变更时间
    **/
    @JSONField(name="time")
    private String time;


    /**
    *   设置 姓名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 姓名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 变更时间
    **/
    public void setTime(String time) {
      this.time = time;
    }
    /**
    *   获取 变更时间
    **/
    public String getTime() {
      return time;
    }



}

