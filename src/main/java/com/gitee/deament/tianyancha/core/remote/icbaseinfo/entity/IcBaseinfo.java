package com.gitee.deament.tianyancha.core.remote.icbaseinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icbaseinfo.entity.IcBaseinfoIndustryAll;
/**
*企业基本信息
* 可以通过公司名称或ID获取企业基本信息，企业基本信息包括公司名称或ID、类型、成立日期、经营状态、注册资本、法人、工商注册号、统一社会信用代码、组织机构代码、纳税人识别号等字段信息
* /services/open/ic/baseinfo/2.0
*@author deament
**/

public class IcBaseinfo implements Serializable{

    /**
     *    曾用名
    **/
    @JSONField(name="historyNames")
    private String historyNames;
    /**
     *    企业状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    注销日期
    **/
    @JSONField(name="cancelDate")
    private Long cancelDate;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    市
    **/
    @JSONField(name="city")
    private String city;
    /**
     *    人员规模
    **/
    @JSONField(name="staffNumRange")
    private String staffNumRange;
    /**
     *    行业
    **/
    @JSONField(name="industry")
    private String industry;
    /**
     *    曾用名
    **/
    @JSONField(name="historyNameList")
    private List<String> historyNameList;
    /**
     *    股票号
    **/
    @JSONField(name="bondNum")
    private String bondNum;
    /**
     *    法人类型，1 人 2 公司
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    股票名
    **/
    @JSONField(name="bondName")
    private String bondName;
    /**
     *    吊销日期
    **/
    @JSONField(name="revokeDate")
    private Long revokeDate;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    吊销原因
    **/
    @JSONField(name="revokeReason")
    private String revokeReason;
    /**
     *    注册号
    **/
    @JSONField(name="regNumber")
    private String regNumber;
    /**
     *    英文名
    **/
    @JSONField(name="property3")
    private String property3;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="creditCode")
    private String creditCode;
    /**
     *    股票曾用名
    **/
    @JSONField(name="usedBondName")
    private String usedBondName;
    /**
     *    经营开始时间
    **/
    @JSONField(name="fromTime")
    private Long fromTime;
    /**
     *    核准时间
    **/
    @JSONField(name="approvedTime")
    private Long approvedTime;
    /**
     *    参保人数
    **/
    @JSONField(name="socialStaffNum")
    private Integer socialStaffNum;
    /**
     *    简称
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    企业类型
    **/
    @JSONField(name="companyOrgType")
    private String companyOrgType;
    /**
     *    实收注册资本币种  人民币 美元 欧元 等
    **/
    @JSONField(name="actualCapitalCurrency")
    private String actualCapitalCurrency;
    /**
     *    企业id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    组织机构代码
    **/
    @JSONField(name="orgNumber")
    private String orgNumber;
    /**
     *    注销原因
    **/
    @JSONField(name="cancelReason")
    private String cancelReason;
    /**
     *    经营结束时间
    **/
    @JSONField(name="toTime")
    private Long toTime;
    /**
     *    实收注册资金
    **/
    @JSONField(name="actualCapital")
    private String actualCapital;
    /**
     *    成立日期
    **/
    @JSONField(name="estiblishTime")
    private Long estiblishTime;
    /**
     *    登记机关
    **/
    @JSONField(name="regInstitute")
    private String regInstitute;
    /**
     *    纳税人识别号
    **/
    @JSONField(name="taxNumber")
    private String taxNumber;
    /**
     *    经营范围
    **/
    @JSONField(name="businessScope")
    private String businessScope;
    /**
     *    注册地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    注册资本币种  人民币 美元 欧元 等
    **/
    @JSONField(name="regCapitalCurrency")
    private String regCapitalCurrency;
    /**
     *    企业标签
    **/
    @JSONField(name="tags")
    private String tags;
    /**
     *    区
    **/
    @JSONField(name="district")
    private String district;
    /**
     *    企业名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    股票类型
    **/
    @JSONField(name="bondType")
    private String bondType;
    /**
     *    企业评分
    **/
    @JSONField(name="percentileScore")
    private Integer percentileScore;
    /**
     *    国民经济行业分类
    **/
    @JSONField(name="industryAll")
    private IcBaseinfoIndustryAll industryAll;
    /**
     *    是否是小微企业 0不是 1是
    **/
    @JSONField(name="isMicroEnt")
    private Integer isMicroEnt;
    /**
     *    省份简称
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 曾用名
    **/
    public void setHistoryNames(String historyNames) {
      this.historyNames = historyNames;
    }
    /**
    *   获取 曾用名
    **/
    public String getHistoryNames() {
      return historyNames;
    }
    /**
    *   设置 企业状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 企业状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 注销日期
    **/
    public void setCancelDate(Long cancelDate) {
      this.cancelDate = cancelDate;
    }
    /**
    *   获取 注销日期
    **/
    public Long getCancelDate() {
      return cancelDate;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 市
    **/
    public void setCity(String city) {
      this.city = city;
    }
    /**
    *   获取 市
    **/
    public String getCity() {
      return city;
    }
    /**
    *   设置 人员规模
    **/
    public void setStaffNumRange(String staffNumRange) {
      this.staffNumRange = staffNumRange;
    }
    /**
    *   获取 人员规模
    **/
    public String getStaffNumRange() {
      return staffNumRange;
    }
    /**
    *   设置 行业
    **/
    public void setIndustry(String industry) {
      this.industry = industry;
    }
    /**
    *   获取 行业
    **/
    public String getIndustry() {
      return industry;
    }
    /**
    *   设置 曾用名
    **/
    public void setHistoryNameList(List<String> historyNameList) {
      this.historyNameList = historyNameList;
    }
    /**
    *   获取 曾用名
    **/
    public List<String> getHistoryNameList() {
      return historyNameList;
    }
    /**
    *   设置 股票号
    **/
    public void setBondNum(String bondNum) {
      this.bondNum = bondNum;
    }
    /**
    *   获取 股票号
    **/
    public String getBondNum() {
      return bondNum;
    }
    /**
    *   设置 法人类型，1 人 2 公司
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 法人类型，1 人 2 公司
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 股票名
    **/
    public void setBondName(String bondName) {
      this.bondName = bondName;
    }
    /**
    *   获取 股票名
    **/
    public String getBondName() {
      return bondName;
    }
    /**
    *   设置 吊销日期
    **/
    public void setRevokeDate(Long revokeDate) {
      this.revokeDate = revokeDate;
    }
    /**
    *   获取 吊销日期
    **/
    public Long getRevokeDate() {
      return revokeDate;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 吊销原因
    **/
    public void setRevokeReason(String revokeReason) {
      this.revokeReason = revokeReason;
    }
    /**
    *   获取 吊销原因
    **/
    public String getRevokeReason() {
      return revokeReason;
    }
    /**
    *   设置 注册号
    **/
    public void setRegNumber(String regNumber) {
      this.regNumber = regNumber;
    }
    /**
    *   获取 注册号
    **/
    public String getRegNumber() {
      return regNumber;
    }
    /**
    *   设置 英文名
    **/
    public void setProperty3(String property3) {
      this.property3 = property3;
    }
    /**
    *   获取 英文名
    **/
    public String getProperty3() {
      return property3;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setCreditCode(String creditCode) {
      this.creditCode = creditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getCreditCode() {
      return creditCode;
    }
    /**
    *   设置 股票曾用名
    **/
    public void setUsedBondName(String usedBondName) {
      this.usedBondName = usedBondName;
    }
    /**
    *   获取 股票曾用名
    **/
    public String getUsedBondName() {
      return usedBondName;
    }
    /**
    *   设置 经营开始时间
    **/
    public void setFromTime(Long fromTime) {
      this.fromTime = fromTime;
    }
    /**
    *   获取 经营开始时间
    **/
    public Long getFromTime() {
      return fromTime;
    }
    /**
    *   设置 核准时间
    **/
    public void setApprovedTime(Long approvedTime) {
      this.approvedTime = approvedTime;
    }
    /**
    *   获取 核准时间
    **/
    public Long getApprovedTime() {
      return approvedTime;
    }
    /**
    *   设置 参保人数
    **/
    public void setSocialStaffNum(Integer socialStaffNum) {
      this.socialStaffNum = socialStaffNum;
    }
    /**
    *   获取 参保人数
    **/
    public Integer getSocialStaffNum() {
      return socialStaffNum;
    }
    /**
    *   设置 简称
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 简称
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 企业类型
    **/
    public void setCompanyOrgType(String companyOrgType) {
      this.companyOrgType = companyOrgType;
    }
    /**
    *   获取 企业类型
    **/
    public String getCompanyOrgType() {
      return companyOrgType;
    }
    /**
    *   设置 实收注册资本币种  人民币 美元 欧元 等
    **/
    public void setActualCapitalCurrency(String actualCapitalCurrency) {
      this.actualCapitalCurrency = actualCapitalCurrency;
    }
    /**
    *   获取 实收注册资本币种  人民币 美元 欧元 等
    **/
    public String getActualCapitalCurrency() {
      return actualCapitalCurrency;
    }
    /**
    *   设置 企业id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 组织机构代码
    **/
    public void setOrgNumber(String orgNumber) {
      this.orgNumber = orgNumber;
    }
    /**
    *   获取 组织机构代码
    **/
    public String getOrgNumber() {
      return orgNumber;
    }
    /**
    *   设置 注销原因
    **/
    public void setCancelReason(String cancelReason) {
      this.cancelReason = cancelReason;
    }
    /**
    *   获取 注销原因
    **/
    public String getCancelReason() {
      return cancelReason;
    }
    /**
    *   设置 经营结束时间
    **/
    public void setToTime(Long toTime) {
      this.toTime = toTime;
    }
    /**
    *   获取 经营结束时间
    **/
    public Long getToTime() {
      return toTime;
    }
    /**
    *   设置 实收注册资金
    **/
    public void setActualCapital(String actualCapital) {
      this.actualCapital = actualCapital;
    }
    /**
    *   获取 实收注册资金
    **/
    public String getActualCapital() {
      return actualCapital;
    }
    /**
    *   设置 成立日期
    **/
    public void setEstiblishTime(Long estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立日期
    **/
    public Long getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 登记机关
    **/
    public void setRegInstitute(String regInstitute) {
      this.regInstitute = regInstitute;
    }
    /**
    *   获取 登记机关
    **/
    public String getRegInstitute() {
      return regInstitute;
    }
    /**
    *   设置 纳税人识别号
    **/
    public void setTaxNumber(String taxNumber) {
      this.taxNumber = taxNumber;
    }
    /**
    *   获取 纳税人识别号
    **/
    public String getTaxNumber() {
      return taxNumber;
    }
    /**
    *   设置 经营范围
    **/
    public void setBusinessScope(String businessScope) {
      this.businessScope = businessScope;
    }
    /**
    *   获取 经营范围
    **/
    public String getBusinessScope() {
      return businessScope;
    }
    /**
    *   设置 注册地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 注册地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 注册资本币种  人民币 美元 欧元 等
    **/
    public void setRegCapitalCurrency(String regCapitalCurrency) {
      this.regCapitalCurrency = regCapitalCurrency;
    }
    /**
    *   获取 注册资本币种  人民币 美元 欧元 等
    **/
    public String getRegCapitalCurrency() {
      return regCapitalCurrency;
    }
    /**
    *   设置 企业标签
    **/
    public void setTags(String tags) {
      this.tags = tags;
    }
    /**
    *   获取 企业标签
    **/
    public String getTags() {
      return tags;
    }
    /**
    *   设置 区
    **/
    public void setDistrict(String district) {
      this.district = district;
    }
    /**
    *   获取 区
    **/
    public String getDistrict() {
      return district;
    }
    /**
    *   设置 企业名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 股票类型
    **/
    public void setBondType(String bondType) {
      this.bondType = bondType;
    }
    /**
    *   获取 股票类型
    **/
    public String getBondType() {
      return bondType;
    }
    /**
    *   设置 企业评分
    **/
    public void setPercentileScore(Integer percentileScore) {
      this.percentileScore = percentileScore;
    }
    /**
    *   获取 企业评分
    **/
    public Integer getPercentileScore() {
      return percentileScore;
    }
    /**
    *   设置 国民经济行业分类
    **/
    public void setIndustryAll(IcBaseinfoIndustryAll industryAll) {
      this.industryAll = industryAll;
    }
    /**
    *   获取 国民经济行业分类
    **/
    public IcBaseinfoIndustryAll getIndustryAll() {
      return industryAll;
    }
    /**
    *   设置 是否是小微企业 0不是 1是
    **/
    public void setIsMicroEnt(Integer isMicroEnt) {
      this.isMicroEnt = isMicroEnt;
    }
    /**
    *   获取 是否是小微企业 0不是 1是
    **/
    public Integer getIsMicroEnt() {
      return isMicroEnt;
    }
    /**
    *   设置 省份简称
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份简称
    **/
    public String getBase() {
      return base;
    }



}

