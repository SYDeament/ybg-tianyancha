package com.gitee.deament.tianyancha.core.remote.relashortpath.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*最短路径发现
* 可以通过公司A，公司B，关系类型（法人/任职/投资/分支/诉讼/竞合/债务，可多选）查询两家公司之间是否存在关联关系
* /services/open/rela/shortPath
*@author deament
**/

@TYCURL(value="/services/open/rela/shortPath")
public class RelaShortPathDTO implements Serializable{

    /**
     *    类型. OWN-法人 SERVE_ALL-任职,INVEST-投资,BRANCH-分支机构,LAW-诉讼,CAC-竞合,EQ-债务, ALL-所有类型
     *
    **/
    @ParamRequire(require = false)
    private String types;
    /**
     *    深度（默认5）
     *
    **/
    @ParamRequire(require = false)
    private Long depth;
    /**
     *    目标公司id（与目标公司名必须输入其中之一）
     *
    **/
    @ParamRequire(require = false)
    private Long idTo;
    /**
     *    目标公司名
     *
    **/
    @ParamRequire(require = false)
    private String nameTo;
    /**
     *    源公司id（与源公司名必须输入其中之一）
     *
    **/
    @ParamRequire(require = false)
    private Long idFrom;
    /**
     *    源公司名
     *
    **/
    @ParamRequire(require = false)
    private String nameFrom;


    /**
    *   设置 类型. OWN-法人 SERVE_ALL-任职,INVEST-投资,BRANCH-分支机构,LAW-诉讼,CAC-竞合,EQ-债务, ALL-所有类型
    **/
    public void setTypes(String types) {
      this.types = types;
    }
    /**
    *   获取 类型. OWN-法人 SERVE_ALL-任职,INVEST-投资,BRANCH-分支机构,LAW-诉讼,CAC-竞合,EQ-债务, ALL-所有类型
    **/
    public String getTypes() {
      return types;
    }
    /**
    *   设置 深度（默认5）
    **/
    public void setDepth(Long depth) {
      this.depth = depth;
    }
    /**
    *   获取 深度（默认5）
    **/
    public Long getDepth() {
      return depth;
    }
    /**
    *   设置 目标公司id（与目标公司名必须输入其中之一）
    **/
    public void setIdTo(Long idTo) {
      this.idTo = idTo;
    }
    /**
    *   获取 目标公司id（与目标公司名必须输入其中之一）
    **/
    public Long getIdTo() {
      return idTo;
    }
    /**
    *   设置 目标公司名
    **/
    public void setNameTo(String nameTo) {
      this.nameTo = nameTo;
    }
    /**
    *   获取 目标公司名
    **/
    public String getNameTo() {
      return nameTo;
    }
    /**
    *   设置 源公司id（与源公司名必须输入其中之一）
    **/
    public void setIdFrom(Long idFrom) {
      this.idFrom = idFrom;
    }
    /**
    *   获取 源公司id（与源公司名必须输入其中之一）
    **/
    public Long getIdFrom() {
      return idFrom;
    }
    /**
    *   设置 源公司名
    **/
    public void setNameFrom(String nameFrom) {
      this.nameFrom = nameFrom;
    }
    /**
    *   获取 源公司名
    **/
    public String getNameFrom() {
      return nameFrom;
    }



}

