package com.gitee.deament.tianyancha.core.remote.icstaff.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icstaff.entity.IcStaff;
import com.gitee.deament.tianyancha.core.remote.icstaff.dto.IcStaffDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*主要人员
* 可以通过公司名称或ID获取企业主要人员信息，主要人员信息包括董事、监事、高级管理人员姓名、职位、主要人员总数等字段的详细信息
* /services/open/ic/staff/2.0
*@author deament
**/
@Component("icStaffRequestImpl")
public class IcStaffRequestImpl extends BaseRequestImpl<IcStaff,IcStaffDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ic/staff/2.0";
    }
}

