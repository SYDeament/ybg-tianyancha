package com.gitee.deament.tianyancha.core.remote.psnews.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*新闻舆情
* 属于PsNews
* /services/open/ps/news/2.0
*@author deament
**/

public class PsNewsItems implements Serializable{

    /**
     *    数据来源
    **/
    @JSONField(name="website")
    private String website;
    /**
     *    简介
    **/
    @JSONField(name="abstracts")
    private String abstracts;
    /**
     *    新闻唯一标识符
    **/
    @JSONField(name="docid")
    private String docid;
    /**
     *    发布时间
    **/
    @JSONField(name="rtm")
    private Long rtm;
    /**
     *    标题
    **/
    @JSONField(name="title")
    private String title;
    /**
     *    新闻url
    **/
    @JSONField(name="uri")
    private String uri;
    /**
     *    
    **/
    @JSONField(name="tags")
    private List<String> tags;


    /**
    *   设置 数据来源
    **/
    public void setWebsite(String website) {
      this.website = website;
    }
    /**
    *   获取 数据来源
    **/
    public String getWebsite() {
      return website;
    }
    /**
    *   设置 简介
    **/
    public void setAbstracts(String abstracts) {
      this.abstracts = abstracts;
    }
    /**
    *   获取 简介
    **/
    public String getAbstracts() {
      return abstracts;
    }
    /**
    *   设置 新闻唯一标识符
    **/
    public void setDocid(String docid) {
      this.docid = docid;
    }
    /**
    *   获取 新闻唯一标识符
    **/
    public String getDocid() {
      return docid;
    }
    /**
    *   设置 发布时间
    **/
    public void setRtm(Long rtm) {
      this.rtm = rtm;
    }
    /**
    *   获取 发布时间
    **/
    public Long getRtm() {
      return rtm;
    }
    /**
    *   设置 标题
    **/
    public void setTitle(String title) {
      this.title = title;
    }
    /**
    *   获取 标题
    **/
    public String getTitle() {
      return title;
    }
    /**
    *   设置 新闻url
    **/
    public void setUri(String uri) {
      this.uri = uri;
    }
    /**
    *   获取 新闻url
    **/
    public String getUri() {
      return uri;
    }
    /**
    *   设置 
    **/
    public void setTags(List<String> tags) {
      this.tags = tags;
    }
    /**
    *   获取 
    **/
    public List<String> getTags() {
      return tags;
    }



}

