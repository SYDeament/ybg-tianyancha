package com.gitee.deament.tianyancha.core.remote.mrlandmortgage.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*土地抵押
* 属于MrLandMortgage
* /services/open/mr/landMortgage/2.0
*@author deament
**/

public class MrLandMortgageItems implements Serializable{

    /**
     *    土地坐落
    **/
    @JSONField(name="land_loc")
    private String land_loc;
    /**
     *    抵押面积（公顷）
    **/
    @JSONField(name="mortgageArea")
    private String mortgageArea;
    /**
     *    结束时间
    **/
    @JSONField(name="endDate")
    private String endDate;
    /**
     *    抵押id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    行政区
    **/
    @JSONField(name="landAministrativeArea")
    private String landAministrativeArea;
    /**
     *    抵押土地用途
    **/
    @JSONField(name="mortgageToUser")
    private String mortgageToUser;
    /**
     *    开始时间
    **/
    @JSONField(name="startDate")
    private String startDate;


    /**
    *   设置 土地坐落
    **/
    public void setLand_loc(String land_loc) {
      this.land_loc = land_loc;
    }
    /**
    *   获取 土地坐落
    **/
    public String getLand_loc() {
      return land_loc;
    }
    /**
    *   设置 抵押面积（公顷）
    **/
    public void setMortgageArea(String mortgageArea) {
      this.mortgageArea = mortgageArea;
    }
    /**
    *   获取 抵押面积（公顷）
    **/
    public String getMortgageArea() {
      return mortgageArea;
    }
    /**
    *   设置 结束时间
    **/
    public void setEndDate(String endDate) {
      this.endDate = endDate;
    }
    /**
    *   获取 结束时间
    **/
    public String getEndDate() {
      return endDate;
    }
    /**
    *   设置 抵押id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 抵押id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 行政区
    **/
    public void setLandAministrativeArea(String landAministrativeArea) {
      this.landAministrativeArea = landAministrativeArea;
    }
    /**
    *   获取 行政区
    **/
    public String getLandAministrativeArea() {
      return landAministrativeArea;
    }
    /**
    *   设置 抵押土地用途
    **/
    public void setMortgageToUser(String mortgageToUser) {
      this.mortgageToUser = mortgageToUser;
    }
    /**
    *   获取 抵押土地用途
    **/
    public String getMortgageToUser() {
      return mortgageToUser;
    }
    /**
    *   设置 开始时间
    **/
    public void setStartDate(String startDate) {
      this.startDate = startDate;
    }
    /**
    *   获取 开始时间
    **/
    public String getStartDate() {
      return startDate;
    }



}

