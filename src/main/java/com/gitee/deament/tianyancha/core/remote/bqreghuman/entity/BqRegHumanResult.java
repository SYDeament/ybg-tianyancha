package com.gitee.deament.tianyancha.core.remote.bqreghuman.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*建筑资质-注册人员详情
* 属于BqRegHuman
* /services/open/bq/regHuman/detail/2.0
*@author deament
**/

public class BqRegHumanResult implements Serializable{

    /**
     *    有效期
    **/
    @JSONField(name="effectiveTime")
    private String effectiveTime;
    /**
     *    注册单位
    **/
    @JSONField(name="registerCompany")
    private String registerCompany;
    /**
     *    注册号（执业印章号）
    **/
    @JSONField(name="practiceSealNo")
    private String practiceSealNo;
    /**
     *    注册类别
    **/
    @JSONField(name="registerType")
    private String registerType;
    /**
     *    证书编号
    **/
    @JSONField(name="certificateNo")
    private String certificateNo;


    /**
    *   设置 有效期
    **/
    public void setEffectiveTime(String effectiveTime) {
      this.effectiveTime = effectiveTime;
    }
    /**
    *   获取 有效期
    **/
    public String getEffectiveTime() {
      return effectiveTime;
    }
    /**
    *   设置 注册单位
    **/
    public void setRegisterCompany(String registerCompany) {
      this.registerCompany = registerCompany;
    }
    /**
    *   获取 注册单位
    **/
    public String getRegisterCompany() {
      return registerCompany;
    }
    /**
    *   设置 注册号（执业印章号）
    **/
    public void setPracticeSealNo(String practiceSealNo) {
      this.practiceSealNo = practiceSealNo;
    }
    /**
    *   获取 注册号（执业印章号）
    **/
    public String getPracticeSealNo() {
      return practiceSealNo;
    }
    /**
    *   设置 注册类别
    **/
    public void setRegisterType(String registerType) {
      this.registerType = registerType;
    }
    /**
    *   获取 注册类别
    **/
    public String getRegisterType() {
      return registerType;
    }
    /**
    *   设置 证书编号
    **/
    public void setCertificateNo(String certificateNo) {
      this.certificateNo = certificateNo;
    }
    /**
    *   获取 证书编号
    **/
    public String getCertificateNo() {
      return certificateNo;
    }



}

