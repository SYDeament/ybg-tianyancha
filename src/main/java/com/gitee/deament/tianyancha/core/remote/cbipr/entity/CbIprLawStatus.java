package com.gitee.deament.tianyancha.core.remote.cbipr.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*知识产权
* 属于CbIpr
* /services/open/cb/ipr/2.0
*@author deament
**/

public class CbIprLawStatus implements Serializable{

    /**
     *    法律状态公告日
    **/
    @JSONField(name="date")
    private String date;
    /**
     *    法律状态
    **/
    @JSONField(name="status")
    private String status;


    /**
    *   设置 法律状态公告日
    **/
    public void setDate(String date) {
      this.date = date;
    }
    /**
    *   获取 法律状态公告日
    **/
    public String getDate() {
      return date;
    }
    /**
    *   设置 法律状态
    **/
    public void setStatus(String status) {
      this.status = status;
    }
    /**
    *   获取 法律状态
    **/
    public String getStatus() {
      return status;
    }



}

