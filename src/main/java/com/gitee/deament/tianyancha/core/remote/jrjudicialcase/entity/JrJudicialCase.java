package com.gitee.deament.tianyancha.core.remote.jrjudicialcase.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrjudicialcase.entity.JrJudicialCaseItems;
/**
*司法解析
* 可以通过公司名称或ID获取司法解析信息，司法解析信息包括案件名称、案件类型、案件身份、案由、相关案号、最新审理程序、最新审理日期等字段的详细信息
* /services/open/jr/judicialCase/2.0
*@author deament
**/

public class JrJudicialCase implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrJudicialCaseItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrJudicialCaseItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrJudicialCaseItems> getItems() {
      return items;
    }



}

