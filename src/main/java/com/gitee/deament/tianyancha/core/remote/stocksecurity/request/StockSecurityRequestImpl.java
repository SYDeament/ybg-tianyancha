package com.gitee.deament.tianyancha.core.remote.stocksecurity.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stocksecurity.entity.StockSecurity;
import com.gitee.deament.tianyancha.core.remote.stocksecurity.dto.StockSecurityDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*证券信息
* 可以通过公司名称或ID获取上市公司证券信息，证券信息包括A股代码、A股简称、律师事务所、会计师事务所、股票类型等
* /services/open/stock/security/2.0
*@author deament
**/
@Component("stockSecurityRequestImpl")
public class StockSecurityRequestImpl extends BaseRequestImpl<StockSecurity,StockSecurityDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/stock/security/2.0";
    }
}

