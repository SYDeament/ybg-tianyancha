package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcChangeList implements Serializable{

    /**
     *    修改日期
    **/
    @JSONField(name="changeTime")
    private String changeTime;
    /**
     *    修改后
    **/
    @JSONField(name="contentAfter")
    private String contentAfter;
    /**
     *    修改前
    **/
    @JSONField(name="contentBefore")
    private String contentBefore;
    /**
     *    修改事项
    **/
    @JSONField(name="changeItem")
    private String changeItem;


    /**
    *   设置 修改日期
    **/
    public void setChangeTime(String changeTime) {
      this.changeTime = changeTime;
    }
    /**
    *   获取 修改日期
    **/
    public String getChangeTime() {
      return changeTime;
    }
    /**
    *   设置 修改后
    **/
    public void setContentAfter(String contentAfter) {
      this.contentAfter = contentAfter;
    }
    /**
    *   获取 修改后
    **/
    public String getContentAfter() {
      return contentAfter;
    }
    /**
    *   设置 修改前
    **/
    public void setContentBefore(String contentBefore) {
      this.contentBefore = contentBefore;
    }
    /**
    *   获取 修改前
    **/
    public String getContentBefore() {
      return contentBefore;
    }
    /**
    *   设置 修改事项
    **/
    public void setChangeItem(String changeItem) {
      this.changeItem = changeItem;
    }
    /**
    *   获取 修改事项
    **/
    public String getChangeItem() {
      return changeItem;
    }



}

