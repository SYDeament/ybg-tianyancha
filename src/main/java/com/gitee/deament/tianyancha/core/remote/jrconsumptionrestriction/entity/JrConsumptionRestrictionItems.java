package com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*限制消费令
* 属于JrConsumptionRestriction
* /services/open/jr/consumptionRestriction/2.0
*@author deament
**/

public class JrConsumptionRestrictionItems implements Serializable{

    /**
     *    案号
    **/
    @JSONField(name="caseCode")
    private String caseCode;
    /**
     *    企业信息简称
    **/
    @JSONField(name="qyinfoAlias")
    private String qyinfoAlias;
    /**
     *    pdf文件地址
    **/
    @JSONField(name="filePath")
    private String filePath;
    /**
     *    企业信息
    **/
    @JSONField(name="qyinfo")
    private String qyinfo;
    /**
     *    立案时间
    **/
    @JSONField(name="caseCreateTime")
    private Long caseCreateTime;
    /**
     *    别名
    **/
    @JSONField(name="alias")
    private String alias;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    限制消费者名称
    **/
    @JSONField(name="xname")
    private String xname;
    /**
     *    企业id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 案号
    **/
    public void setCaseCode(String caseCode) {
      this.caseCode = caseCode;
    }
    /**
    *   获取 案号
    **/
    public String getCaseCode() {
      return caseCode;
    }
    /**
    *   设置 企业信息简称
    **/
    public void setQyinfoAlias(String qyinfoAlias) {
      this.qyinfoAlias = qyinfoAlias;
    }
    /**
    *   获取 企业信息简称
    **/
    public String getQyinfoAlias() {
      return qyinfoAlias;
    }
    /**
    *   设置 pdf文件地址
    **/
    public void setFilePath(String filePath) {
      this.filePath = filePath;
    }
    /**
    *   获取 pdf文件地址
    **/
    public String getFilePath() {
      return filePath;
    }
    /**
    *   设置 企业信息
    **/
    public void setQyinfo(String qyinfo) {
      this.qyinfo = qyinfo;
    }
    /**
    *   获取 企业信息
    **/
    public String getQyinfo() {
      return qyinfo;
    }
    /**
    *   设置 立案时间
    **/
    public void setCaseCreateTime(Long caseCreateTime) {
      this.caseCreateTime = caseCreateTime;
    }
    /**
    *   获取 立案时间
    **/
    public Long getCaseCreateTime() {
      return caseCreateTime;
    }
    /**
    *   设置 别名
    **/
    public void setAlias(String alias) {
      this.alias = alias;
    }
    /**
    *   获取 别名
    **/
    public String getAlias() {
      return alias;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 限制消费者名称
    **/
    public void setXname(String xname) {
      this.xname = xname;
    }
    /**
    *   获取 限制消费者名称
    **/
    public String getXname() {
      return xname;
    }
    /**
    *   设置 企业id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 企业id
    **/
    public Integer getCid() {
      return cid;
    }



}

