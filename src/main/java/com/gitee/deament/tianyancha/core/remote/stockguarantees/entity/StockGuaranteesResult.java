package com.gitee.deament.tianyancha.core.remote.stockguarantees.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*对外担保
* 属于StockGuarantees
* /services/open/stock/guarantees/2.0
*@author deament
**/

public class StockGuaranteesResult implements Serializable{

    /**
     *    担保起始日
    **/
    @JSONField(name="grnt_sd")
    private Integer grnt_sd;
    /**
     *    是否关联交易
    **/
    @JSONField(name="is_related_trans")
    private String is_related_trans;
    /**
     *    担保终止日
    **/
    @JSONField(name="grnt_ed")
    private Integer grnt_ed;
    /**
     *    担保期限
    **/
    @JSONField(name="grnt_period")
    private String grnt_period;
    /**
     *    公告日期
    **/
    @JSONField(name="announcement_date")
    private Long announcement_date;
    /**
     *    担保方式
    **/
    @JSONField(name="grnt_type")
    private String grnt_type;
    /**
     *    担保金额
    **/
    @JSONField(name="grnt_amt")
    private String grnt_amt;
    /**
     *    担保方
    **/
    @JSONField(name="grnt_corp_name")
    private String grnt_corp_name;
    /**
     *    是否履行完毕
    **/
    @JSONField(name="is_fulfillment")
    private String is_fulfillment;
    /**
     *    被担保方
    **/
    @JSONField(name="secured_org_name")
    private String secured_org_name;
    /**
     *    币种
    **/
    @JSONField(name="currency_variety")
    private String currency_variety;


    /**
    *   设置 担保起始日
    **/
    public void setGrnt_sd(Integer grnt_sd) {
      this.grnt_sd = grnt_sd;
    }
    /**
    *   获取 担保起始日
    **/
    public Integer getGrnt_sd() {
      return grnt_sd;
    }
    /**
    *   设置 是否关联交易
    **/
    public void setIs_related_trans(String is_related_trans) {
      this.is_related_trans = is_related_trans;
    }
    /**
    *   获取 是否关联交易
    **/
    public String getIs_related_trans() {
      return is_related_trans;
    }
    /**
    *   设置 担保终止日
    **/
    public void setGrnt_ed(Integer grnt_ed) {
      this.grnt_ed = grnt_ed;
    }
    /**
    *   获取 担保终止日
    **/
    public Integer getGrnt_ed() {
      return grnt_ed;
    }
    /**
    *   设置 担保期限
    **/
    public void setGrnt_period(String grnt_period) {
      this.grnt_period = grnt_period;
    }
    /**
    *   获取 担保期限
    **/
    public String getGrnt_period() {
      return grnt_period;
    }
    /**
    *   设置 公告日期
    **/
    public void setAnnouncement_date(Long announcement_date) {
      this.announcement_date = announcement_date;
    }
    /**
    *   获取 公告日期
    **/
    public Long getAnnouncement_date() {
      return announcement_date;
    }
    /**
    *   设置 担保方式
    **/
    public void setGrnt_type(String grnt_type) {
      this.grnt_type = grnt_type;
    }
    /**
    *   获取 担保方式
    **/
    public String getGrnt_type() {
      return grnt_type;
    }
    /**
    *   设置 担保金额
    **/
    public void setGrnt_amt(String grnt_amt) {
      this.grnt_amt = grnt_amt;
    }
    /**
    *   获取 担保金额
    **/
    public String getGrnt_amt() {
      return grnt_amt;
    }
    /**
    *   设置 担保方
    **/
    public void setGrnt_corp_name(String grnt_corp_name) {
      this.grnt_corp_name = grnt_corp_name;
    }
    /**
    *   获取 担保方
    **/
    public String getGrnt_corp_name() {
      return grnt_corp_name;
    }
    /**
    *   设置 是否履行完毕
    **/
    public void setIs_fulfillment(String is_fulfillment) {
      this.is_fulfillment = is_fulfillment;
    }
    /**
    *   获取 是否履行完毕
    **/
    public String getIs_fulfillment() {
      return is_fulfillment;
    }
    /**
    *   设置 被担保方
    **/
    public void setSecured_org_name(String secured_org_name) {
      this.secured_org_name = secured_org_name;
    }
    /**
    *   获取 被担保方
    **/
    public String getSecured_org_name() {
      return secured_org_name;
    }
    /**
    *   设置 币种
    **/
    public void setCurrency_variety(String currency_variety) {
      this.currency_variety = currency_variety;
    }
    /**
    *   获取 币种
    **/
    public String getCurrency_variety() {
      return currency_variety;
    }



}

