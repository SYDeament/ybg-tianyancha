package com.gitee.deament.tianyancha.core.remote.mrinquiryevaluation.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrinquiryevaluation.entity.MrInquiryEvaluationEvaluationAgency;
import com.gitee.deament.tianyancha.core.remote.mrinquiryevaluation.entity.MrInquiryEvaluationItems;
/**
*询价评估
* 可以通过公司名称或ID获取询价评估信息，询价评估信息包括标的物名称、当事人姓名、评估机构名称等字段的详细信息
* /services/open/mr/inquiryEvaluation/2.0
*@author deament
**/

public class MrInquiryEvaluation implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MrInquiryEvaluationItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MrInquiryEvaluationItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MrInquiryEvaluationItems> getItems() {
      return items;
    }



}

