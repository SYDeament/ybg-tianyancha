package com.gitee.deament.tianyancha.core.remote.stockbalancesheet.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.stockbalancesheet.entity.StockBalanceSheetCorpFinancialYears;
import com.gitee.deament.tianyancha.core.remote.stockbalancesheet.entity.StockBalanceSheetCorpBalanceSheet;
/**
*资产负债表
* 可以通过公司名称或ID获取上市公司资产负债表数据，资产负债表数据包括流动资产、流动资产合计、非流动资产等
* /services/open/stock/balanceSheet/2.0
*@author deament
**/

public class StockBalanceSheet implements Serializable{

    /**
     *    
    **/
    @JSONField(name="corpFinancialYears")
    private List<StockBalanceSheetCorpFinancialYears> corpFinancialYears;
    /**
     *    
    **/
    @JSONField(name="corpBalanceSheet")
    private List<StockBalanceSheetCorpBalanceSheet> corpBalanceSheet;


    /**
    *   设置 
    **/
    public void setCorpFinancialYears(List<StockBalanceSheetCorpFinancialYears> corpFinancialYears) {
      this.corpFinancialYears = corpFinancialYears;
    }
    /**
    *   获取 
    **/
    public List<StockBalanceSheetCorpFinancialYears> getCorpFinancialYears() {
      return corpFinancialYears;
    }
    /**
    *   设置 
    **/
    public void setCorpBalanceSheet(List<StockBalanceSheetCorpBalanceSheet> corpBalanceSheet) {
      this.corpBalanceSheet = corpBalanceSheet;
    }
    /**
    *   获取 
    **/
    public List<StockBalanceSheetCorpBalanceSheet> getCorpBalanceSheet() {
      return corpBalanceSheet;
    }



}

