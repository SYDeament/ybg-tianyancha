package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.request;

import com.gitee.deament.tianyancha.core.TianyanchaResult;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.remote.stockvolatility.dto.StockVolatilityDTO;
import com.gitee.deament.tianyancha.core.remote.stockvolatility.entity.StockVolatility;
import com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.dto.XgbaseinfoV2DTO;
import com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity.XgbaseinfoV2;


/**
 *特殊企业基本信息
 * 属于XgbaseinfoV2
 * /services/v4/open/xgbaseinfoV2
 *@author deament
 **/
public interface XgbaseinfoV2Request extends BaseRequest<XgbaseinfoV2DTO, XgbaseinfoV2> {

}
