package com.gitee.deament.tianyancha.core.remote.icholder.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icholder.entity.IcHolder;
import com.gitee.deament.tianyancha.core.remote.icholder.dto.IcHolderDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业股东
* 可以通过公司名称或ID获取企业股东信息，股东信息包括股东名、出资比例、出资金额、股东总数等字段的详细信息
* /services/open/ic/holder/2.0
*@author deament
**/

public interface IcHolderRequest extends BaseRequest<IcHolderDTO ,IcHolder>{

}

