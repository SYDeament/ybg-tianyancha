package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcLiquidatingInfo implements Serializable{

    /**
     *    清算组负责人
    **/
    @JSONField(name="manager")
    private String manager;
    /**
     *    清算成员名称
    **/
    @JSONField(name="member")
    private String member;
    /**
     *    表id
    **/
    @JSONField(name="id")
    private Integer id;


    /**
    *   设置 清算组负责人
    **/
    public void setManager(String manager) {
      this.manager = manager;
    }
    /**
    *   获取 清算组负责人
    **/
    public String getManager() {
      return manager;
    }
    /**
    *   设置 清算成员名称
    **/
    public void setMember(String member) {
      this.member = member;
    }
    /**
    *   获取 清算成员名称
    **/
    public String getMember() {
      return member;
    }
    /**
    *   设置 表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 表id
    **/
    public Integer getId() {
      return id;
    }



}

