package com.gitee.deament.tianyancha.core.remote.iccontact.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*企业联系方式
* 可以通过公司名称或ID获取企业联系方式信息，企业联系方式信息包括邮箱、网址、电话等字段的详细信息
* /services/open/ic/contact
*@author deament
**/

public class IcContact implements Serializable{

    /**
     *    电话
    **/
    @JSONField(name="phoneNumber")
    private String phoneNumber;
    /**
     *    注册地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    邮箱
    **/
    @JSONField(name="email")
    private String email;
    /**
     *    网址
    **/
    @JSONField(name="websiteList")
    private String websiteList;


    /**
    *   设置 电话
    **/
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    /**
    *   获取 电话
    **/
    public String getPhoneNumber() {
      return phoneNumber;
    }
    /**
    *   设置 注册地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 注册地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 邮箱
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱
    **/
    public String getEmail() {
      return email;
    }
    /**
    *   设置 网址
    **/
    public void setWebsiteList(String websiteList) {
      this.websiteList = websiteList;
    }
    /**
    *   获取 网址
    **/
    public String getWebsiteList() {
      return websiteList;
    }



}

