package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype13 implements Serializable{

    /**
     *    承办部门
    **/
    @JSONField(name="contractors")
    private String contractors;
    /**
     *    原告/上诉人
    **/
    @JSONField(name="plaintiff")
    private List<RiskDetailPlaintiff> plaintiff;
    /**
     *    案由
    **/
    @JSONField(name="casereason")
    private String casereason;
    /**
     *    当事人
    **/
    @JSONField(name="litigant2")
    private List<RiskDetailLitigant2> litigant2;
    /**
     *    被告/被上诉人
    **/
    @JSONField(name="defendant")
    private List<RiskDetailDefendant> defendant;
    /**
     *    法庭
    **/
    @JSONField(name="courtroom")
    private String courtroom;
    /**
     *    当事人
    **/
    @JSONField(name="litigant")
    private String litigant;
    /**
     *    内部使用
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    审判长/主审人
    **/
    @JSONField(name="judge")
    private String judge;
    /**
     *    开庭日期(时间)
    **/
    @JSONField(name="startdate")
    private Long startdate;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;


    /**
    *   设置 承办部门
    **/
    public void setContractors(String contractors) {
      this.contractors = contractors;
    }
    /**
    *   获取 承办部门
    **/
    public String getContractors() {
      return contractors;
    }
    /**
    *   设置 原告/上诉人
    **/
    public void setPlaintiff(List<RiskDetailPlaintiff> plaintiff) {
      this.plaintiff = plaintiff;
    }
    /**
    *   获取 原告/上诉人
    **/
    public List<RiskDetailPlaintiff> getPlaintiff() {
      return plaintiff;
    }
    /**
    *   设置 案由
    **/
    public void setCasereason(String casereason) {
      this.casereason = casereason;
    }
    /**
    *   获取 案由
    **/
    public String getCasereason() {
      return casereason;
    }
    /**
    *   设置 当事人
    **/
    public void setLitigant2(List<RiskDetailLitigant2> litigant2) {
      this.litigant2 = litigant2;
    }
    /**
    *   获取 当事人
    **/
    public List<RiskDetailLitigant2> getLitigant2() {
      return litigant2;
    }
    /**
    *   设置 被告/被上诉人
    **/
    public void setDefendant(List<RiskDetailDefendant> defendant) {
      this.defendant = defendant;
    }
    /**
    *   获取 被告/被上诉人
    **/
    public List<RiskDetailDefendant> getDefendant() {
      return defendant;
    }
    /**
    *   设置 法庭
    **/
    public void setCourtroom(String courtroom) {
      this.courtroom = courtroom;
    }
    /**
    *   获取 法庭
    **/
    public String getCourtroom() {
      return courtroom;
    }
    /**
    *   设置 当事人
    **/
    public void setLitigant(String litigant) {
      this.litigant = litigant;
    }
    /**
    *   获取 当事人
    **/
    public String getLitigant() {
      return litigant;
    }
    /**
    *   设置 内部使用
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 内部使用
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 审判长/主审人
    **/
    public void setJudge(String judge) {
      this.judge = judge;
    }
    /**
    *   获取 审判长/主审人
    **/
    public String getJudge() {
      return judge;
    }
    /**
    *   设置 开庭日期(时间)
    **/
    public void setStartdate(Long startdate) {
      this.startdate = startdate;
    }
    /**
    *   获取 开庭日期(时间)
    **/
    public Long getStartdate() {
      return startdate;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }



}

