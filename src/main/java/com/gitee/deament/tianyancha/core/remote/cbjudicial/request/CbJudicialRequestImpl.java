package com.gitee.deament.tianyancha.core.remote.cbjudicial.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.entity.CbJudicial;
import com.gitee.deament.tianyancha.core.remote.cbjudicial.dto.CbJudicialDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*司法风险
* 可以通过公司名称或ID获取包含法律诉讼、法院公告、开庭公告、失信人、被执行人、司法协助、立案信息、送达公告、终本案件、限制消费令等维度的相关信息
* /services/open/cb/judicial/2.0
*@author deament
**/
@Component("cbJudicialRequestImpl")
public class CbJudicialRequestImpl extends BaseRequestImpl<CbJudicial,CbJudicialDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cb/judicial/2.0";
    }
}

