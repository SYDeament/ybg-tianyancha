package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcSocialList implements Serializable{

    /**
     *    参加职工基本医疗保险本期实际缴费金额
    **/
    @JSONField(name="medicalInsurancePayAmount")
    private String medicalInsurancePayAmount;
    /**
     *    单位参加失业保险缴费基数
    **/
    @JSONField(name="unemploymentInsuranceBase")
    private String unemploymentInsuranceBase;
    /**
     *    参加失业保险本期实际缴费金额
    **/
    @JSONField(name="unemploymentInsurancePayAmount")
    private String unemploymentInsurancePayAmount;
    /**
     *    单位参加失业保险累计欠缴金额
    **/
    @JSONField(name="unemploymentInsuranceOweAmount")
    private String unemploymentInsuranceOweAmount;
    /**
     *    单位参加职工基本医疗保险累计欠缴金额
    **/
    @JSONField(name="medicalInsuranceOweAmount")
    private String medicalInsuranceOweAmount;
    /**
     *    城镇职工基本养老保险人数
    **/
    @JSONField(name="endowmentInsurance")
    private String endowmentInsurance;
    /**
     *    参加生育保险本期实际缴费金额
    **/
    @JSONField(name="maternityInsurancePayAmount")
    private String maternityInsurancePayAmount;
    /**
     *    参加工伤保险本期实际缴费金额
    **/
    @JSONField(name="employmentInjuryInsurancePayAmount")
    private String employmentInjuryInsurancePayAmount;
    /**
     *    失业保险人数
    **/
    @JSONField(name="unemploymentInsurance")
    private String unemploymentInsurance;
    /**
     *    职工基本医疗保险人数
    **/
    @JSONField(name="medicalInsurance")
    private String medicalInsurance;
    /**
     *    单位参加职工基本医疗保险缴费基数
    **/
    @JSONField(name="medicalInsuranceBase")
    private String medicalInsuranceBase;
    /**
     *    单位参加城镇职工基本养老保险缴费基数
    **/
    @JSONField(name="endowmentInsuranceBase")
    private String endowmentInsuranceBase;
    /**
     *    单位参加生育保险缴费基数
    **/
    @JSONField(name="maternityInsuranceBase")
    private String maternityInsuranceBase;
    /**
     *    参加城镇职工基本养老保险本期实际缴费金额
    **/
    @JSONField(name="endowmentInsurancePayAmount")
    private String endowmentInsurancePayAmount;
    /**
     *    工伤保险
    **/
    @JSONField(name="employmentInjuryInsurance")
    private String employmentInjuryInsurance;
    /**
     *    单位参加城镇职工基本养老保险累计欠缴金额
    **/
    @JSONField(name="endowmentInsuranceOweAmount")
    private String endowmentInsuranceOweAmount;
    /**
     *    生育保险人数
    **/
    @JSONField(name="maternityInsurance")
    private String maternityInsurance;
    /**
     *    单位参加生育保险累计欠缴金额
    **/
    @JSONField(name="maternityInsuranceOweAmount")
    private String maternityInsuranceOweAmount;
    /**
     *    单位参加工伤保险累计欠缴金额
    **/
    @JSONField(name="employmentInjuryInsuranceOweAmount")
    private String employmentInjuryInsuranceOweAmount;


    /**
    *   设置 参加职工基本医疗保险本期实际缴费金额
    **/
    public void setMedicalInsurancePayAmount(String medicalInsurancePayAmount) {
      this.medicalInsurancePayAmount = medicalInsurancePayAmount;
    }
    /**
    *   获取 参加职工基本医疗保险本期实际缴费金额
    **/
    public String getMedicalInsurancePayAmount() {
      return medicalInsurancePayAmount;
    }
    /**
    *   设置 单位参加失业保险缴费基数
    **/
    public void setUnemploymentInsuranceBase(String unemploymentInsuranceBase) {
      this.unemploymentInsuranceBase = unemploymentInsuranceBase;
    }
    /**
    *   获取 单位参加失业保险缴费基数
    **/
    public String getUnemploymentInsuranceBase() {
      return unemploymentInsuranceBase;
    }
    /**
    *   设置 参加失业保险本期实际缴费金额
    **/
    public void setUnemploymentInsurancePayAmount(String unemploymentInsurancePayAmount) {
      this.unemploymentInsurancePayAmount = unemploymentInsurancePayAmount;
    }
    /**
    *   获取 参加失业保险本期实际缴费金额
    **/
    public String getUnemploymentInsurancePayAmount() {
      return unemploymentInsurancePayAmount;
    }
    /**
    *   设置 单位参加失业保险累计欠缴金额
    **/
    public void setUnemploymentInsuranceOweAmount(String unemploymentInsuranceOweAmount) {
      this.unemploymentInsuranceOweAmount = unemploymentInsuranceOweAmount;
    }
    /**
    *   获取 单位参加失业保险累计欠缴金额
    **/
    public String getUnemploymentInsuranceOweAmount() {
      return unemploymentInsuranceOweAmount;
    }
    /**
    *   设置 单位参加职工基本医疗保险累计欠缴金额
    **/
    public void setMedicalInsuranceOweAmount(String medicalInsuranceOweAmount) {
      this.medicalInsuranceOweAmount = medicalInsuranceOweAmount;
    }
    /**
    *   获取 单位参加职工基本医疗保险累计欠缴金额
    **/
    public String getMedicalInsuranceOweAmount() {
      return medicalInsuranceOweAmount;
    }
    /**
    *   设置 城镇职工基本养老保险人数
    **/
    public void setEndowmentInsurance(String endowmentInsurance) {
      this.endowmentInsurance = endowmentInsurance;
    }
    /**
    *   获取 城镇职工基本养老保险人数
    **/
    public String getEndowmentInsurance() {
      return endowmentInsurance;
    }
    /**
    *   设置 参加生育保险本期实际缴费金额
    **/
    public void setMaternityInsurancePayAmount(String maternityInsurancePayAmount) {
      this.maternityInsurancePayAmount = maternityInsurancePayAmount;
    }
    /**
    *   获取 参加生育保险本期实际缴费金额
    **/
    public String getMaternityInsurancePayAmount() {
      return maternityInsurancePayAmount;
    }
    /**
    *   设置 参加工伤保险本期实际缴费金额
    **/
    public void setEmploymentInjuryInsurancePayAmount(String employmentInjuryInsurancePayAmount) {
      this.employmentInjuryInsurancePayAmount = employmentInjuryInsurancePayAmount;
    }
    /**
    *   获取 参加工伤保险本期实际缴费金额
    **/
    public String getEmploymentInjuryInsurancePayAmount() {
      return employmentInjuryInsurancePayAmount;
    }
    /**
    *   设置 失业保险人数
    **/
    public void setUnemploymentInsurance(String unemploymentInsurance) {
      this.unemploymentInsurance = unemploymentInsurance;
    }
    /**
    *   获取 失业保险人数
    **/
    public String getUnemploymentInsurance() {
      return unemploymentInsurance;
    }
    /**
    *   设置 职工基本医疗保险人数
    **/
    public void setMedicalInsurance(String medicalInsurance) {
      this.medicalInsurance = medicalInsurance;
    }
    /**
    *   获取 职工基本医疗保险人数
    **/
    public String getMedicalInsurance() {
      return medicalInsurance;
    }
    /**
    *   设置 单位参加职工基本医疗保险缴费基数
    **/
    public void setMedicalInsuranceBase(String medicalInsuranceBase) {
      this.medicalInsuranceBase = medicalInsuranceBase;
    }
    /**
    *   获取 单位参加职工基本医疗保险缴费基数
    **/
    public String getMedicalInsuranceBase() {
      return medicalInsuranceBase;
    }
    /**
    *   设置 单位参加城镇职工基本养老保险缴费基数
    **/
    public void setEndowmentInsuranceBase(String endowmentInsuranceBase) {
      this.endowmentInsuranceBase = endowmentInsuranceBase;
    }
    /**
    *   获取 单位参加城镇职工基本养老保险缴费基数
    **/
    public String getEndowmentInsuranceBase() {
      return endowmentInsuranceBase;
    }
    /**
    *   设置 单位参加生育保险缴费基数
    **/
    public void setMaternityInsuranceBase(String maternityInsuranceBase) {
      this.maternityInsuranceBase = maternityInsuranceBase;
    }
    /**
    *   获取 单位参加生育保险缴费基数
    **/
    public String getMaternityInsuranceBase() {
      return maternityInsuranceBase;
    }
    /**
    *   设置 参加城镇职工基本养老保险本期实际缴费金额
    **/
    public void setEndowmentInsurancePayAmount(String endowmentInsurancePayAmount) {
      this.endowmentInsurancePayAmount = endowmentInsurancePayAmount;
    }
    /**
    *   获取 参加城镇职工基本养老保险本期实际缴费金额
    **/
    public String getEndowmentInsurancePayAmount() {
      return endowmentInsurancePayAmount;
    }
    /**
    *   设置 工伤保险
    **/
    public void setEmploymentInjuryInsurance(String employmentInjuryInsurance) {
      this.employmentInjuryInsurance = employmentInjuryInsurance;
    }
    /**
    *   获取 工伤保险
    **/
    public String getEmploymentInjuryInsurance() {
      return employmentInjuryInsurance;
    }
    /**
    *   设置 单位参加城镇职工基本养老保险累计欠缴金额
    **/
    public void setEndowmentInsuranceOweAmount(String endowmentInsuranceOweAmount) {
      this.endowmentInsuranceOweAmount = endowmentInsuranceOweAmount;
    }
    /**
    *   获取 单位参加城镇职工基本养老保险累计欠缴金额
    **/
    public String getEndowmentInsuranceOweAmount() {
      return endowmentInsuranceOweAmount;
    }
    /**
    *   设置 生育保险人数
    **/
    public void setMaternityInsurance(String maternityInsurance) {
      this.maternityInsurance = maternityInsurance;
    }
    /**
    *   获取 生育保险人数
    **/
    public String getMaternityInsurance() {
      return maternityInsurance;
    }
    /**
    *   设置 单位参加生育保险累计欠缴金额
    **/
    public void setMaternityInsuranceOweAmount(String maternityInsuranceOweAmount) {
      this.maternityInsuranceOweAmount = maternityInsuranceOweAmount;
    }
    /**
    *   获取 单位参加生育保险累计欠缴金额
    **/
    public String getMaternityInsuranceOweAmount() {
      return maternityInsuranceOweAmount;
    }
    /**
    *   设置 单位参加工伤保险累计欠缴金额
    **/
    public void setEmploymentInjuryInsuranceOweAmount(String employmentInjuryInsuranceOweAmount) {
      this.employmentInjuryInsuranceOweAmount = employmentInjuryInsuranceOweAmount;
    }
    /**
    *   获取 单位参加工伤保险累计欠缴金额
    **/
    public String getEmploymentInjuryInsuranceOweAmount() {
      return employmentInjuryInsuranceOweAmount;
    }



}

