package com.gitee.deament.tianyancha.core.remote.mrstockpledge.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*质押走势
* 属于MrStockPledge
* /services/open/mr/stockPledge/trend/2.0
*@author deament
**/

public class MrStockPledgeTrend implements Serializable{

    /**
     *    时间
    **/
    @JSONField(name="dt")
    private String dt;
    /**
     *    质押比例
    **/
    @JSONField(name="value")
    private Integer value;


    /**
    *   设置 时间
    **/
    public void setDt(String dt) {
      this.dt = dt;
    }
    /**
    *   获取 时间
    **/
    public String getDt() {
      return dt;
    }
    /**
    *   设置 质押比例
    **/
    public void setValue(Integer value) {
      this.value = value;
    }
    /**
    *   获取 质押比例
    **/
    public Integer getValue() {
      return value;
    }



}

