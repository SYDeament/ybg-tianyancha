package com.gitee.deament.tianyancha.core.remote.icchangeinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icchangeinfo.entity.IcChangeinfo;
import com.gitee.deament.tianyancha.core.remote.icchangeinfo.dto.IcChangeinfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*变更记录
* 可以通过公司名称或ID获取企业变更记录，变更记录包括工商变更事项、变更前后信息等字段的详细信息
* /services/open/ic/changeinfo/2.0
*@author deament
**/

public interface IcChangeinfoRequest extends BaseRequest<IcChangeinfoDTO ,IcChangeinfo>{

}

