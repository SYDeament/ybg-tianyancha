package com.gitee.deament.tianyancha.core.remote.mrlandmortgage.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrlandmortgage.entity.MrLandMortgage;
import com.gitee.deament.tianyancha.core.remote.mrlandmortgage.dto.MrLandMortgageDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*土地抵押详情
* 根据土地抵押ID获取土地抵押详情，土地抵押详情包括抵押面积、土地抵押人名称、抵押土地用途、评估金额、抵押金额、抵押人等字段的详细信息
* /services/open/mr/landMortgage/detail/2.0
*@author deament
**/

public interface MrLandMortgageRequest extends BaseRequest<MrLandMortgageDTO ,MrLandMortgage>{

}

