package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcCheckList implements Serializable{

    /**
     *    类型
    **/
    @JSONField(name="checkType")
    private String checkType;
    /**
     *    检查实施机关
    **/
    @JSONField(name="checkOrg")
    private String checkOrg;
    /**
     *    备注
    **/
    @JSONField(name="remark")
    private String remark;
    /**
     *    表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    日期
    **/
    @JSONField(name="checkDate")
    private String checkDate;
    /**
     *    结果
    **/
    @JSONField(name="checkResult")
    private String checkResult;


    /**
    *   设置 类型
    **/
    public void setCheckType(String checkType) {
      this.checkType = checkType;
    }
    /**
    *   获取 类型
    **/
    public String getCheckType() {
      return checkType;
    }
    /**
    *   设置 检查实施机关
    **/
    public void setCheckOrg(String checkOrg) {
      this.checkOrg = checkOrg;
    }
    /**
    *   获取 检查实施机关
    **/
    public String getCheckOrg() {
      return checkOrg;
    }
    /**
    *   设置 备注
    **/
    public void setRemark(String remark) {
      this.remark = remark;
    }
    /**
    *   获取 备注
    **/
    public String getRemark() {
      return remark;
    }
    /**
    *   设置 表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 日期
    **/
    public void setCheckDate(String checkDate) {
      this.checkDate = checkDate;
    }
    /**
    *   获取 日期
    **/
    public String getCheckDate() {
      return checkDate;
    }
    /**
    *   设置 结果
    **/
    public void setCheckResult(String checkResult) {
      this.checkResult = checkResult;
    }
    /**
    *   获取 结果
    **/
    public String getCheckResult() {
      return checkResult;
    }



}

