package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*特殊企业基本信息
* 可以通过公司名称或ID获取特殊企业基本信息，包含香港公司、社会组织、律所、事业单位、基金会这些特殊企业，不同社会团体所呈现的信息维度不同
* /services/v4/open/xgbaseinfoV2
*@author deament
**/

public class XgbaseinfoV22 implements Serializable{

    /**
     *    押记登记册
    **/
    @JSONField(name="mortgageS")
    private String mortgageS;
    /**
     *    公司现状
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    成立时间（毫秒数）
    **/
    @JSONField(name="estiblishTime")
    private Long estiblishTime;
    /**
     *    1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会，7-不存在法人、注册资本、统一社会信用代码、经营状态
    **/
    @JSONField(name="entityType")
    private Integer entityType;
    /**
     *    备注
    **/
    @JSONField(name="remarksS")
    private String remarksS;
    /**
     *    是否有年报
    **/
    @JSONField(name="haveReport")
    private Boolean haveReport;
    /**
     *    英文名
    **/
    @JSONField(name="nameEn")
    private String nameEn;
    /**
     *    无用
    **/
    @JSONField(name="baiduAuthURLWWW")
    private String baiduAuthURLWWW;
    /**
     *    公司编号
    **/
    @JSONField(name="companyNum")
    private String companyNum;
    /**
     *    微博(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="weibo")
    private String weibo;
    /**
     *    公司名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    公司logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    公司类型
    **/
    @JSONField(name="companyOrgType")
    private String companyOrgType;
    /**
     *    清盘模式
    **/
    @JSONField(name="liquidationModeS")
    private String liquidationModeS;
    /**
     *    重要事项
    **/
    @JSONField(name="importantItemsS")
    private String importantItemsS;
    /**
     *    无用
    **/
    @JSONField(name="baiduAuthURLWAP")
    private String baiduAuthURLWAP;
    /**
     *    公司id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    无用
    **/
    @JSONField(name="isClaimed")
    private Integer isClaimed;
    /**
     *    结束时间(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="toTime")
    private String toTime;
    /**
     *    省份
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 押记登记册
    **/
    public void setMortgageS(String mortgageS) {
      this.mortgageS = mortgageS;
    }
    /**
    *   获取 押记登记册
    **/
    public String getMortgageS() {
      return mortgageS;
    }
    /**
    *   设置 公司现状
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 公司现状
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 成立时间（毫秒数）
    **/
    public void setEstiblishTime(Long estiblishTime) {
      this.estiblishTime = estiblishTime;
    }
    /**
    *   获取 成立时间（毫秒数）
    **/
    public Long getEstiblishTime() {
      return estiblishTime;
    }
    /**
    *   设置 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会，7-不存在法人、注册资本、统一社会信用代码、经营状态
    **/
    public void setEntityType(Integer entityType) {
      this.entityType = entityType;
    }
    /**
    *   获取 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会，7-不存在法人、注册资本、统一社会信用代码、经营状态
    **/
    public Integer getEntityType() {
      return entityType;
    }
    /**
    *   设置 备注
    **/
    public void setRemarksS(String remarksS) {
      this.remarksS = remarksS;
    }
    /**
    *   获取 备注
    **/
    public String getRemarksS() {
      return remarksS;
    }
    /**
    *   设置 是否有年报
    **/
    public void setHaveReport(Boolean haveReport) {
      this.haveReport = haveReport;
    }
    /**
    *   获取 是否有年报
    **/
    public Boolean getHaveReport() {
      return haveReport;
    }
    /**
    *   设置 英文名
    **/
    public void setNameEn(String nameEn) {
      this.nameEn = nameEn;
    }
    /**
    *   获取 英文名
    **/
    public String getNameEn() {
      return nameEn;
    }
    /**
    *   设置 无用
    **/
    public void setBaiduAuthURLWWW(String baiduAuthURLWWW) {
      this.baiduAuthURLWWW = baiduAuthURLWWW;
    }
    /**
    *   获取 无用
    **/
    public String getBaiduAuthURLWWW() {
      return baiduAuthURLWWW;
    }
    /**
    *   设置 公司编号
    **/
    public void setCompanyNum(String companyNum) {
      this.companyNum = companyNum;
    }
    /**
    *   获取 公司编号
    **/
    public String getCompanyNum() {
      return companyNum;
    }
    /**
    *   设置 微博(官方未定义字段类型 暂定为String)
    **/
    public void setWeibo(String weibo) {
      this.weibo = weibo;
    }
    /**
    *   获取 微博(官方未定义字段类型 暂定为String)
    **/
    public String getWeibo() {
      return weibo;
    }
    /**
    *   设置 公司名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 公司名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 公司logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 公司logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 公司类型
    **/
    public void setCompanyOrgType(String companyOrgType) {
      this.companyOrgType = companyOrgType;
    }
    /**
    *   获取 公司类型
    **/
    public String getCompanyOrgType() {
      return companyOrgType;
    }
    /**
    *   设置 清盘模式
    **/
    public void setLiquidationModeS(String liquidationModeS) {
      this.liquidationModeS = liquidationModeS;
    }
    /**
    *   获取 清盘模式
    **/
    public String getLiquidationModeS() {
      return liquidationModeS;
    }
    /**
    *   设置 重要事项
    **/
    public void setImportantItemsS(String importantItemsS) {
      this.importantItemsS = importantItemsS;
    }
    /**
    *   获取 重要事项
    **/
    public String getImportantItemsS() {
      return importantItemsS;
    }
    /**
    *   设置 无用
    **/
    public void setBaiduAuthURLWAP(String baiduAuthURLWAP) {
      this.baiduAuthURLWAP = baiduAuthURLWAP;
    }
    /**
    *   获取 无用
    **/
    public String getBaiduAuthURLWAP() {
      return baiduAuthURLWAP;
    }
    /**
    *   设置 公司id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 无用
    **/
    public void setIsClaimed(Integer isClaimed) {
      this.isClaimed = isClaimed;
    }
    /**
    *   获取 无用
    **/
    public Integer getIsClaimed() {
      return isClaimed;
    }
    /**
    *   设置 结束时间(官方未定义字段类型 暂定为String)
    **/
    public void setToTime(String toTime) {
      this.toTime = toTime;
    }
    /**
    *   获取 结束时间(官方未定义字段类型 暂定为String)
    **/
    public String getToTime() {
      return toTime;
    }
    /**
    *   设置 省份
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份
    **/
    public String getBase() {
      return base;
    }



}

