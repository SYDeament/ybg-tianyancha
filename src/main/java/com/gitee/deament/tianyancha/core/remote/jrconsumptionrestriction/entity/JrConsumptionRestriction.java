package com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrconsumptionrestriction.entity.JrConsumptionRestrictionItems;
/**
*限制消费令
* 可以通过公司名称或ID获取限制消费令信息，限制消费令信息包括执行法院、案件内容、被执行人名称等字段的详细信息
* /services/open/jr/consumptionRestriction/2.0
*@author deament
**/

public class JrConsumptionRestriction implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrConsumptionRestrictionItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrConsumptionRestrictionItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrConsumptionRestrictionItems> getItems() {
      return items;
    }



}

