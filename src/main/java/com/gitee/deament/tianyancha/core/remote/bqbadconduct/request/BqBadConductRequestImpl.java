package com.gitee.deament.tianyancha.core.remote.bqbadconduct.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.bqbadconduct.entity.BqBadConduct;
import com.gitee.deament.tianyancha.core.remote.bqbadconduct.dto.BqBadConductDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*建筑资质-不良行为
* 可以通过公司名称或ID获取有建筑资质企业的不良行为信息，包括诚信记录编号、查看事由、实施部门（文号）、发布有效期、决定内容等字段的相关信息
* /services/open/bq/badConduct/2.0
*@author deament
**/
@Component("bqBadConductRequestImpl")
public class BqBadConductRequestImpl extends BaseRequestImpl<BqBadConduct,BqBadConductDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/bq/badConduct/2.0";
    }
}

