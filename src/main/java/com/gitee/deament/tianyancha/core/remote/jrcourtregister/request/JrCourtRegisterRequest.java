package com.gitee.deament.tianyancha.core.remote.jrcourtregister.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrcourtregister.entity.JrCourtRegister;
import com.gitee.deament.tianyancha.core.remote.jrcourtregister.dto.JrCourtRegisterDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*立案信息
* 可以通过公司名称或ID获取立案信息，立案信息包括案件编号、案由、立案时间、案件原被告双方信息等字段的详细信息
* /services/open/jr/courtRegister/2.0
*@author deament
**/

public interface JrCourtRegisterRequest extends BaseRequest<JrCourtRegisterDTO ,JrCourtRegister>{

}

