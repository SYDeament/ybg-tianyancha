package com.gitee.deament.tianyancha.core.remote.icannualreport.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportBaseInfo;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportOutboundInvestmentList;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportShareholderList;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportReportSocialSecurityInfo;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportEquityChangeInfoList;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportChangeRecordList;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportWebInfoList;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportOutGuaranteeInfoList;
import com.gitee.deament.tianyancha.core.remote.icannualreport.entity.IcAnnualreportItems;
/**
*企业年报
* 可以通过公司名称或ID获取企业年报，企业年报包括企业基本信息、股东及出资信息、企业资产状况信息、对外投资信息等字段的详细信息
* /services/open/ic/annualreport/2.0
*@author deament
**/

public class IcAnnualreport implements Serializable{

    /**
     *    年报总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IcAnnualreportItems> items;


    /**
    *   设置 年报总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 年报总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IcAnnualreportItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IcAnnualreportItems> getItems() {
      return items;
    }



}

