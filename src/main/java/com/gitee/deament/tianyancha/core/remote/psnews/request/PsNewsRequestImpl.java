package com.gitee.deament.tianyancha.core.remote.psnews.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.psnews.entity.PsNews;
import com.gitee.deament.tianyancha.core.remote.psnews.dto.PsNewsDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*新闻舆情
* 根据公司id或公司名称（精确匹配）获取新闻列表
* /services/open/ps/news/2.0
*@author deament
**/
@Component("psNewsRequestImpl")
public class PsNewsRequestImpl extends BaseRequestImpl<PsNews,PsNewsDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ps/news/2.0";
    }
}

