package com.gitee.deament.tianyancha.core.remote.bqqualification.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.bqqualification.entity.BqQualification;
import com.gitee.deament.tianyancha.core.remote.bqqualification.dto.BqQualificationDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*建筑资质-资质资格详情
* 可以通过建筑资质资格ID获取建筑资质资格详情，包括发证日期、证书有效期、资质类别、资质证书号  、资质名称、发证机关、证书信息等字段信息
* /services/open/bq/qualification/detail/2.0
*@author deament
**/

public interface BqQualificationRequest extends BaseRequest<BqQualificationDTO ,BqQualification>{

}

