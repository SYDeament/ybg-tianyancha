package com.gitee.deament.tianyancha.core.remote.equityratio.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*股权结构图
* 属于EquityRatio
* /services/v4/open/equityRatio
*@author deament
**/

public class EquityRatioPath implements Serializable{

    /**
     *    人id
    **/
    @JSONField(name="id")
    private String id;
    /**
     *    返回类型
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    值
    **/
    @JSONField(name="value")
    private String value;
    /**
     *    企业cid
    **/
    @JSONField(name="cid")
    private String cid;


    /**
    *   设置 人id
    **/
    public void setId(String id) {
      this.id = id;
    }
    /**
    *   获取 人id
    **/
    public String getId() {
      return id;
    }
    /**
    *   设置 返回类型
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 返回类型
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 值
    **/
    public void setValue(String value) {
      this.value = value;
    }
    /**
    *   获取 值
    **/
    public String getValue() {
      return value;
    }
    /**
    *   设置 企业cid
    **/
    public void setCid(String cid) {
      this.cid = cid;
    }
    /**
    *   获取 企业cid
    **/
    public String getCid() {
      return cid;
    }



}

