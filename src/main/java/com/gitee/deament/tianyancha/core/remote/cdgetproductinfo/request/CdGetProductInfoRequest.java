package com.gitee.deament.tianyancha.core.remote.cdgetproductinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdgetproductinfo.entity.CdGetProductInfo;
import com.gitee.deament.tianyancha.core.remote.cdgetproductinfo.dto.CdGetProductInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*企业业务
* 可以通过公司名称或ID获取企业业务信息，企业业务信息包括主要业务、业务分类、业务介绍等字段的详细信息
* /services/open/cd/getProductInfo/2.0
*@author deament
**/

public interface CdGetProductInfoRequest extends BaseRequest<CdGetProductInfoDTO ,CdGetProductInfo>{

}

