package com.gitee.deament.tianyancha.core.remote.riskdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*天眼风险详情
* 属于RiskDetail
* /services/v4/open/riskDetail
*@author deament
**/

public class RiskDetailDataListtype7 implements Serializable{

    /**
     *    移除时间
    **/
    @JSONField(name="removeDate")
    private String removeDate;
    /**
     *    公司id
    **/
    @JSONField(name="company_id")
    private Integer company_id;
    /**
     *    列入原因
    **/
    @JSONField(name="putReason")
    private String putReason;
    /**
     *    创建时间
    **/
    @JSONField(name="createTime")
    private String createTime;
    /**
     *    决定列入部门
    **/
    @JSONField(name="putDepartment")
    private String putDepartment;
    /**
     *    决定移除部门
    **/
    @JSONField(name="removeDepartment")
    private String removeDepartment;
    /**
     *    移除原因
    **/
    @JSONField(name="removeReason")
    private String removeReason;
    /**
     *    对应表id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    列入世间
    **/
    @JSONField(name="putDate")
    private String putDate;


    /**
    *   设置 移除时间
    **/
    public void setRemoveDate(String removeDate) {
      this.removeDate = removeDate;
    }
    /**
    *   获取 移除时间
    **/
    public String getRemoveDate() {
      return removeDate;
    }
    /**
    *   设置 公司id
    **/
    public void setCompany_id(Integer company_id) {
      this.company_id = company_id;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCompany_id() {
      return company_id;
    }
    /**
    *   设置 列入原因
    **/
    public void setPutReason(String putReason) {
      this.putReason = putReason;
    }
    /**
    *   获取 列入原因
    **/
    public String getPutReason() {
      return putReason;
    }
    /**
    *   设置 创建时间
    **/
    public void setCreateTime(String createTime) {
      this.createTime = createTime;
    }
    /**
    *   获取 创建时间
    **/
    public String getCreateTime() {
      return createTime;
    }
    /**
    *   设置 决定列入部门
    **/
    public void setPutDepartment(String putDepartment) {
      this.putDepartment = putDepartment;
    }
    /**
    *   获取 决定列入部门
    **/
    public String getPutDepartment() {
      return putDepartment;
    }
    /**
    *   设置 决定移除部门
    **/
    public void setRemoveDepartment(String removeDepartment) {
      this.removeDepartment = removeDepartment;
    }
    /**
    *   获取 决定移除部门
    **/
    public String getRemoveDepartment() {
      return removeDepartment;
    }
    /**
    *   设置 移除原因
    **/
    public void setRemoveReason(String removeReason) {
      this.removeReason = removeReason;
    }
    /**
    *   获取 移除原因
    **/
    public String getRemoveReason() {
      return removeReason;
    }
    /**
    *   设置 对应表id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 对应表id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 列入世间
    **/
    public void setPutDate(String putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 列入世间
    **/
    public String getPutDate() {
      return putDate;
    }



}

