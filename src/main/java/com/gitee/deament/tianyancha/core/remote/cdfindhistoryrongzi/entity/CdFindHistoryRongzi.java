package com.gitee.deament.tianyancha.core.remote.cdfindhistoryrongzi.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdfindhistoryrongzi.entity.CdFindHistoryRongziItems;
/**
*融资历史
* 可以通过公司名称或ID获取企业融资历史信息，企业融资历史信息包括融资轮次、交易金额、估值、投资方等字段的详细信息
* /services/open/cd/findHistoryRongzi/2.0
*@author deament
**/

public class CdFindHistoryRongzi implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<CdFindHistoryRongziItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<CdFindHistoryRongziItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<CdFindHistoryRongziItems> getItems() {
      return items;
    }



}

