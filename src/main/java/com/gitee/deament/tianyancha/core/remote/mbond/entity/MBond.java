package com.gitee.deament.tianyancha.core.remote.mbond.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mbond.entity.MBondItems;
/**
*债券信息
* 可以通过公司名称或ID获企业发行债券信息，债券信息包括发行日期、债券名称、债券代码、债券类型等字段的详细信息
* /services/open/m/bond/2.0
*@author deament
**/

public class MBond implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MBondItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MBondItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MBondItems> getItems() {
      return items;
    }



}

