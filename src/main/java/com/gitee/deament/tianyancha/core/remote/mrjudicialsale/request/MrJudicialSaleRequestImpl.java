package com.gitee.deament.tianyancha.core.remote.mrjudicialsale.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrjudicialsale.entity.MrJudicialSale;
import com.gitee.deament.tianyancha.core.remote.mrjudicialsale.dto.MrJudicialSaleDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*司法拍卖
* 可以通过公司名称或ID获取企业司法拍卖公告信息，企业司法拍卖公告信息包括拍卖公告标题、执行法院、拍卖时间、拍卖标的、起拍价格等字段的详细信息
* /services/open/mr/judicialSale/2.0
*@author deament
**/
@Component("mrJudicialSaleRequestImpl")
public class MrJudicialSaleRequestImpl extends BaseRequestImpl<MrJudicialSale,MrJudicialSaleDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/judicialSale/2.0";
    }
}

