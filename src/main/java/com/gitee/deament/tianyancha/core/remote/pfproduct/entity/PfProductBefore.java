package com.gitee.deament.tianyancha.core.remote.pfproduct.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*产品信息
* 属于PfProduct
* /services/open/pf/product/2.0
*@author deament
**/

public class PfProductBefore implements Serializable{

    /**
     *    月报
    **/
    @JSONField(name="monthly_report")
    private String monthly_report;
    /**
     *    半年报
    **/
    @JSONField(name="half_year_report")
    private String half_year_report;
    /**
     *    基金编号
    **/
    @JSONField(name="fund_no")
    private String fund_no;
    /**
     *    年报
    **/
    @JSONField(name="year_report")
    private String year_report;
    /**
     *    季报
    **/
    @JSONField(name="quarterly_report")
    private String quarterly_report;
    /**
     *    产品名称
    **/
    @JSONField(name="fund_name")
    private String fund_name;


    /**
    *   设置 月报
    **/
    public void setMonthly_report(String monthly_report) {
      this.monthly_report = monthly_report;
    }
    /**
    *   获取 月报
    **/
    public String getMonthly_report() {
      return monthly_report;
    }
    /**
    *   设置 半年报
    **/
    public void setHalf_year_report(String half_year_report) {
      this.half_year_report = half_year_report;
    }
    /**
    *   获取 半年报
    **/
    public String getHalf_year_report() {
      return half_year_report;
    }
    /**
    *   设置 基金编号
    **/
    public void setFund_no(String fund_no) {
      this.fund_no = fund_no;
    }
    /**
    *   获取 基金编号
    **/
    public String getFund_no() {
      return fund_no;
    }
    /**
    *   设置 年报
    **/
    public void setYear_report(String year_report) {
      this.year_report = year_report;
    }
    /**
    *   获取 年报
    **/
    public String getYear_report() {
      return year_report;
    }
    /**
    *   设置 季报
    **/
    public void setQuarterly_report(String quarterly_report) {
      this.quarterly_report = quarterly_report;
    }
    /**
    *   获取 季报
    **/
    public String getQuarterly_report() {
      return quarterly_report;
    }
    /**
    *   设置 产品名称
    **/
    public void setFund_name(String fund_name) {
      this.fund_name = fund_name;
    }
    /**
    *   获取 产品名称
    **/
    public String getFund_name() {
      return fund_name;
    }



}

