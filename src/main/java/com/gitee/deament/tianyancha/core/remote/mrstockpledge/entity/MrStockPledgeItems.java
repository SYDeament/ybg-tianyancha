package com.gitee.deament.tianyancha.core.remote.mrstockpledge.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*重要股东质押
* 属于MrStockPledge
* /services/open/mr/stockPledge/shareholder/2.0
*@author deament
**/

public class MrStockPledgeItems implements Serializable{

    /**
     *    股东id
    **/
    @JSONField(name="shareHolderId")
    private Integer shareHolderId;
    /**
     *    最新质押笔数
    **/
    @JSONField(name="latestCount")
    private Integer latestCount;
    /**
     *    更新日期
    **/
    @JSONField(name="updateTimeEastmoney")
    private Long updateTimeEastmoney;
    /**
     *    0-人 1-公司
    **/
    @JSONField(name="shareHolderType")
    private Integer shareHolderType;
    /**
     *    质押id
    **/
    @JSONField(name="businessId")
    private String businessId;
    /**
     *    公司id
    **/
    @JSONField(name="graphId")
    private Integer graphId;
    /**
     *    股东名
    **/
    @JSONField(name="shareHolder")
    private String shareHolder;
    /**
     *    剩余质押股数（股）
    **/
    @JSONField(name="lastAmount")
    private Integer lastAmount;


    /**
    *   设置 股东id
    **/
    public void setShareHolderId(Integer shareHolderId) {
      this.shareHolderId = shareHolderId;
    }
    /**
    *   获取 股东id
    **/
    public Integer getShareHolderId() {
      return shareHolderId;
    }
    /**
    *   设置 最新质押笔数
    **/
    public void setLatestCount(Integer latestCount) {
      this.latestCount = latestCount;
    }
    /**
    *   获取 最新质押笔数
    **/
    public Integer getLatestCount() {
      return latestCount;
    }
    /**
    *   设置 更新日期
    **/
    public void setUpdateTimeEastmoney(Long updateTimeEastmoney) {
      this.updateTimeEastmoney = updateTimeEastmoney;
    }
    /**
    *   获取 更新日期
    **/
    public Long getUpdateTimeEastmoney() {
      return updateTimeEastmoney;
    }
    /**
    *   设置 0-人 1-公司
    **/
    public void setShareHolderType(Integer shareHolderType) {
      this.shareHolderType = shareHolderType;
    }
    /**
    *   获取 0-人 1-公司
    **/
    public Integer getShareHolderType() {
      return shareHolderType;
    }
    /**
    *   设置 质押id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 质押id
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 公司id
    **/
    public void setGraphId(Integer graphId) {
      this.graphId = graphId;
    }
    /**
    *   获取 公司id
    **/
    public Integer getGraphId() {
      return graphId;
    }
    /**
    *   设置 股东名
    **/
    public void setShareHolder(String shareHolder) {
      this.shareHolder = shareHolder;
    }
    /**
    *   获取 股东名
    **/
    public String getShareHolder() {
      return shareHolder;
    }
    /**
    *   设置 剩余质押股数（股）
    **/
    public void setLastAmount(Integer lastAmount) {
      this.lastAmount = lastAmount;
    }
    /**
    *   获取 剩余质押股数（股）
    **/
    public Integer getLastAmount() {
      return lastAmount;
    }



}

