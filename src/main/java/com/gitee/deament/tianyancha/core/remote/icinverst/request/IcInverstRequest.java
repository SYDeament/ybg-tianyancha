package com.gitee.deament.tianyancha.core.remote.icinverst.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icinverst.entity.IcInverst;
import com.gitee.deament.tianyancha.core.remote.icinverst.dto.IcInverstDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*对外投资
* 可以通过公司名称或ID获取企业对外投资信息，对外投资信息包括被投资企业、企业法人、投资占比、对外投资总数等字段的详细信息
* /services/open/ic/inverst/2.0
*@author deament
**/

public interface IcInverstRequest extends BaseRequest<IcInverstDTO ,IcInverst>{

}

