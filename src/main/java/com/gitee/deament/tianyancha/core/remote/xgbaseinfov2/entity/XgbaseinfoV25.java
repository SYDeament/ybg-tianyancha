package com.gitee.deament.tianyancha.core.remote.xgbaseinfov2.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*特殊企业基本信息
* 可以通过公司名称或ID获取特殊企业基本信息，包含香港公司、社会组织、律所、事业单位、基金会这些特殊企业，不同社会团体所呈现的信息维度不同
* /services/v4/open/xgbaseinfoV2
*@author deament
**/

public class XgbaseinfoV25 implements Serializable{

    /**
     *    企业介绍
    **/
    @JSONField(name="baseInfo")
    private String baseInfo;
    /**
     *    经营状态
    **/
    @JSONField(name="regStatus")
    private String regStatus;
    /**
     *    注册资本
    **/
    @JSONField(name="regCapital")
    private String regCapital;
    /**
     *    企业邮箱列表
    **/
    @JSONField(name="emailList")
    private List<String> emailList;
    /**
     *    经费来源
    **/
    @JSONField(name="expendSource")
    private String expendSource;
    /**
     *    企业联系方式列表
    **/
    @JSONField(name="phoneList")
    private List<String> phoneList;
    /**
     *    举办单位
    **/
    @JSONField(name="regUnit")
    private String regUnit;
    /**
     *    无用
    **/
    @JSONField(name="baiduAuthURLWWW")
    private String baiduAuthURLWWW;
    /**
     *    
    **/
    @JSONField(name="type")
    private Integer type;
    /**
     *    第三举办单位
    **/
    @JSONField(name="regUnitNameThird")
    private String regUnitNameThird;
    /**
     *    第二名称
    **/
    @JSONField(name="nameSecond")
    private String nameSecond;
    /**
     *    无用
    **/
    @JSONField(name="pricingPackage")
    private Integer pricingPackage;
    /**
     *    法人
    **/
    @JSONField(name="legalPersonName")
    private String legalPersonName;
    /**
     *    统一社会信用代码
    **/
    @JSONField(name="usCreditCode")
    private String usCreditCode;
    /**
     *    微博(官方未定义字段类型 暂定为String)
    **/
    @JSONField(name="weibo")
    private String weibo;
    /**
     *    宗旨和业务范围
    **/
    @JSONField(name="scope")
    private String scope;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    无用
    **/
    @JSONField(name="baiduAuthURLWAP")
    private String baiduAuthURLWAP;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    举办单位名称
    **/
    @JSONField(name="regUnitName")
    private String regUnitName;
    /**
     *    无用
    **/
    @JSONField(name="isClaimed")
    private Integer isClaimed;
    /**
     *    邮箱
    **/
    @JSONField(name="email")
    private String email;
    /**
     *    登记管理机关
    **/
    @JSONField(name="holdUnit")
    private String holdUnit;
    /**
     *    地址
    **/
    @JSONField(name="address")
    private String address;
    /**
     *    其它举办单位
    **/
    @JSONField(name="regUnitNameOther")
    private String regUnitNameOther;
    /**
     *    1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    @JSONField(name="entityType")
    private Integer entityType;
    /**
     *    是否有年报
    **/
    @JSONField(name="haveReport")
    private Boolean haveReport;
    /**
     *    地址
    **/
    @JSONField(name="regLocation")
    private String regLocation;
    /**
     *    原证书号
    **/
    @JSONField(name="oldCert")
    private String oldCert;
    /**
     *    其他名称
    **/
    @JSONField(name="nameOther")
    private String nameOther;
    /**
     *    网址
    **/
    @JSONField(name="websiteList")
    private String websiteList;
    /**
     *    公司编号
    **/
    @JSONField(name="companyNum")
    private String companyNum;
    /**
     *    法人id
    **/
    @JSONField(name="legalPersonId")
    private Integer legalPersonId;
    /**
     *    设立登记时间
    **/
    @JSONField(name="regTime")
    private String regTime;
    /**
     *    企业联系方式
    **/
    @JSONField(name="phoneNumber")
    private String phoneNumber;
    /**
     *    地址
    **/
    @JSONField(name="rladdress")
    private String rladdress;
    /**
     *    第三名称
    **/
    @JSONField(name="nameThird")
    private String nameThird;
    /**
     *    单位名称
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    有效期
    **/
    @JSONField(name="validTime")
    private String validTime;
    /**
     *    第二举办单位
    **/
    @JSONField(name="regUnitNameSecond")
    private String regUnitNameSecond;
    /**
     *    省份
    **/
    @JSONField(name="base")
    private String base;


    /**
    *   设置 企业介绍
    **/
    public void setBaseInfo(String baseInfo) {
      this.baseInfo = baseInfo;
    }
    /**
    *   获取 企业介绍
    **/
    public String getBaseInfo() {
      return baseInfo;
    }
    /**
    *   设置 经营状态
    **/
    public void setRegStatus(String regStatus) {
      this.regStatus = regStatus;
    }
    /**
    *   获取 经营状态
    **/
    public String getRegStatus() {
      return regStatus;
    }
    /**
    *   设置 注册资本
    **/
    public void setRegCapital(String regCapital) {
      this.regCapital = regCapital;
    }
    /**
    *   获取 注册资本
    **/
    public String getRegCapital() {
      return regCapital;
    }
    /**
    *   设置 企业邮箱列表
    **/
    public void setEmailList(List<String> emailList) {
      this.emailList = emailList;
    }
    /**
    *   获取 企业邮箱列表
    **/
    public List<String> getEmailList() {
      return emailList;
    }
    /**
    *   设置 经费来源
    **/
    public void setExpendSource(String expendSource) {
      this.expendSource = expendSource;
    }
    /**
    *   获取 经费来源
    **/
    public String getExpendSource() {
      return expendSource;
    }
    /**
    *   设置 企业联系方式列表
    **/
    public void setPhoneList(List<String> phoneList) {
      this.phoneList = phoneList;
    }
    /**
    *   获取 企业联系方式列表
    **/
    public List<String> getPhoneList() {
      return phoneList;
    }
    /**
    *   设置 举办单位
    **/
    public void setRegUnit(String regUnit) {
      this.regUnit = regUnit;
    }
    /**
    *   获取 举办单位
    **/
    public String getRegUnit() {
      return regUnit;
    }
    /**
    *   设置 无用
    **/
    public void setBaiduAuthURLWWW(String baiduAuthURLWWW) {
      this.baiduAuthURLWWW = baiduAuthURLWWW;
    }
    /**
    *   获取 无用
    **/
    public String getBaiduAuthURLWWW() {
      return baiduAuthURLWWW;
    }
    /**
    *   设置 
    **/
    public void setType(Integer type) {
      this.type = type;
    }
    /**
    *   获取 
    **/
    public Integer getType() {
      return type;
    }
    /**
    *   设置 第三举办单位
    **/
    public void setRegUnitNameThird(String regUnitNameThird) {
      this.regUnitNameThird = regUnitNameThird;
    }
    /**
    *   获取 第三举办单位
    **/
    public String getRegUnitNameThird() {
      return regUnitNameThird;
    }
    /**
    *   设置 第二名称
    **/
    public void setNameSecond(String nameSecond) {
      this.nameSecond = nameSecond;
    }
    /**
    *   获取 第二名称
    **/
    public String getNameSecond() {
      return nameSecond;
    }
    /**
    *   设置 无用
    **/
    public void setPricingPackage(Integer pricingPackage) {
      this.pricingPackage = pricingPackage;
    }
    /**
    *   获取 无用
    **/
    public Integer getPricingPackage() {
      return pricingPackage;
    }
    /**
    *   设置 法人
    **/
    public void setLegalPersonName(String legalPersonName) {
      this.legalPersonName = legalPersonName;
    }
    /**
    *   获取 法人
    **/
    public String getLegalPersonName() {
      return legalPersonName;
    }
    /**
    *   设置 统一社会信用代码
    **/
    public void setUsCreditCode(String usCreditCode) {
      this.usCreditCode = usCreditCode;
    }
    /**
    *   获取 统一社会信用代码
    **/
    public String getUsCreditCode() {
      return usCreditCode;
    }
    /**
    *   设置 微博(官方未定义字段类型 暂定为String)
    **/
    public void setWeibo(String weibo) {
      this.weibo = weibo;
    }
    /**
    *   获取 微博(官方未定义字段类型 暂定为String)
    **/
    public String getWeibo() {
      return weibo;
    }
    /**
    *   设置 宗旨和业务范围
    **/
    public void setScope(String scope) {
      this.scope = scope;
    }
    /**
    *   获取 宗旨和业务范围
    **/
    public String getScope() {
      return scope;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 无用
    **/
    public void setBaiduAuthURLWAP(String baiduAuthURLWAP) {
      this.baiduAuthURLWAP = baiduAuthURLWAP;
    }
    /**
    *   获取 无用
    **/
    public String getBaiduAuthURLWAP() {
      return baiduAuthURLWAP;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 举办单位名称
    **/
    public void setRegUnitName(String regUnitName) {
      this.regUnitName = regUnitName;
    }
    /**
    *   获取 举办单位名称
    **/
    public String getRegUnitName() {
      return regUnitName;
    }
    /**
    *   设置 无用
    **/
    public void setIsClaimed(Integer isClaimed) {
      this.isClaimed = isClaimed;
    }
    /**
    *   获取 无用
    **/
    public Integer getIsClaimed() {
      return isClaimed;
    }
    /**
    *   设置 邮箱
    **/
    public void setEmail(String email) {
      this.email = email;
    }
    /**
    *   获取 邮箱
    **/
    public String getEmail() {
      return email;
    }
    /**
    *   设置 登记管理机关
    **/
    public void setHoldUnit(String holdUnit) {
      this.holdUnit = holdUnit;
    }
    /**
    *   获取 登记管理机关
    **/
    public String getHoldUnit() {
      return holdUnit;
    }
    /**
    *   设置 地址
    **/
    public void setAddress(String address) {
      this.address = address;
    }
    /**
    *   获取 地址
    **/
    public String getAddress() {
      return address;
    }
    /**
    *   设置 其它举办单位
    **/
    public void setRegUnitNameOther(String regUnitNameOther) {
      this.regUnitNameOther = regUnitNameOther;
    }
    /**
    *   获取 其它举办单位
    **/
    public String getRegUnitNameOther() {
      return regUnitNameOther;
    }
    /**
    *   设置 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public void setEntityType(Integer entityType) {
      this.entityType = entityType;
    }
    /**
    *   获取 1-公司，2-香港公司，3-社会组织，4-律所，5-事业单位，6-基金会
    **/
    public Integer getEntityType() {
      return entityType;
    }
    /**
    *   设置 是否有年报
    **/
    public void setHaveReport(Boolean haveReport) {
      this.haveReport = haveReport;
    }
    /**
    *   获取 是否有年报
    **/
    public Boolean getHaveReport() {
      return haveReport;
    }
    /**
    *   设置 地址
    **/
    public void setRegLocation(String regLocation) {
      this.regLocation = regLocation;
    }
    /**
    *   获取 地址
    **/
    public String getRegLocation() {
      return regLocation;
    }
    /**
    *   设置 原证书号
    **/
    public void setOldCert(String oldCert) {
      this.oldCert = oldCert;
    }
    /**
    *   获取 原证书号
    **/
    public String getOldCert() {
      return oldCert;
    }
    /**
    *   设置 其他名称
    **/
    public void setNameOther(String nameOther) {
      this.nameOther = nameOther;
    }
    /**
    *   获取 其他名称
    **/
    public String getNameOther() {
      return nameOther;
    }
    /**
    *   设置 网址
    **/
    public void setWebsiteList(String websiteList) {
      this.websiteList = websiteList;
    }
    /**
    *   获取 网址
    **/
    public String getWebsiteList() {
      return websiteList;
    }
    /**
    *   设置 公司编号
    **/
    public void setCompanyNum(String companyNum) {
      this.companyNum = companyNum;
    }
    /**
    *   获取 公司编号
    **/
    public String getCompanyNum() {
      return companyNum;
    }
    /**
    *   设置 法人id
    **/
    public void setLegalPersonId(Integer legalPersonId) {
      this.legalPersonId = legalPersonId;
    }
    /**
    *   获取 法人id
    **/
    public Integer getLegalPersonId() {
      return legalPersonId;
    }
    /**
    *   设置 设立登记时间
    **/
    public void setRegTime(String regTime) {
      this.regTime = regTime;
    }
    /**
    *   获取 设立登记时间
    **/
    public String getRegTime() {
      return regTime;
    }
    /**
    *   设置 企业联系方式
    **/
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    /**
    *   获取 企业联系方式
    **/
    public String getPhoneNumber() {
      return phoneNumber;
    }
    /**
    *   设置 地址
    **/
    public void setRladdress(String rladdress) {
      this.rladdress = rladdress;
    }
    /**
    *   获取 地址
    **/
    public String getRladdress() {
      return rladdress;
    }
    /**
    *   设置 第三名称
    **/
    public void setNameThird(String nameThird) {
      this.nameThird = nameThird;
    }
    /**
    *   获取 第三名称
    **/
    public String getNameThird() {
      return nameThird;
    }
    /**
    *   设置 单位名称
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 单位名称
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 有效期
    **/
    public void setValidTime(String validTime) {
      this.validTime = validTime;
    }
    /**
    *   获取 有效期
    **/
    public String getValidTime() {
      return validTime;
    }
    /**
    *   设置 第二举办单位
    **/
    public void setRegUnitNameSecond(String regUnitNameSecond) {
      this.regUnitNameSecond = regUnitNameSecond;
    }
    /**
    *   获取 第二举办单位
    **/
    public String getRegUnitNameSecond() {
      return regUnitNameSecond;
    }
    /**
    *   设置 省份
    **/
    public void setBase(String base) {
      this.base = base;
    }
    /**
    *   获取 省份
    **/
    public String getBase() {
      return base;
    }



}

