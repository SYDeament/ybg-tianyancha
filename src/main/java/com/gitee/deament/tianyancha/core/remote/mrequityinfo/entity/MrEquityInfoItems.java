package com.gitee.deament.tianyancha.core.remote.mrequityinfo.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*股权出质
* 属于MrEquityInfo
* /services/open/mr/equityInfo/2.0
*@author deament
**/

public class MrEquityInfoItems implements Serializable{

    /**
     *    登记编号
    **/
    @JSONField(name="regNumber")
    private String regNumber;
    /**
     *    出质人证照/证件号码
    **/
    @JSONField(name="certifNumber")
    private String certifNumber;
    /**
     *    公司列表
    **/
    @JSONField(name="companyList")
    private List<MrEquityInfoCompanyList> companyList;
    /**
     *    质权人列表
    **/
    @JSONField(name="pledgeeList")
    private List<MrEquityInfoPledgeeList> pledgeeList;
    /**
     *    出质人列表
    **/
    @JSONField(name="pledgorList")
    private List<MrEquityInfoPledgorList> pledgorList;
    /**
     *    出质股权数额
    **/
    @JSONField(name="equityAmount")
    private String equityAmount;
    /**
     *    股权出质设立登记日期
    **/
    @JSONField(name="regDate")
    private Long regDate;
    /**
     *    状态
    **/
    @JSONField(name="state")
    private String state;
    /**
     *    出质人
    **/
    @JSONField(name="pledgor")
    private String pledgor;
    /**
     *    质权人证照/证件号码
    **/
    @JSONField(name="certifNumberR")
    private String certifNumberR;
    /**
     *    股权出质设立发布日期
    **/
    @JSONField(name="putDate")
    private Long putDate;
    /**
     *    质权人
    **/
    @JSONField(name="pledgee")
    private String pledgee;


    /**
    *   设置 登记编号
    **/
    public void setRegNumber(String regNumber) {
      this.regNumber = regNumber;
    }
    /**
    *   获取 登记编号
    **/
    public String getRegNumber() {
      return regNumber;
    }
    /**
    *   设置 出质人证照/证件号码
    **/
    public void setCertifNumber(String certifNumber) {
      this.certifNumber = certifNumber;
    }
    /**
    *   获取 出质人证照/证件号码
    **/
    public String getCertifNumber() {
      return certifNumber;
    }
    /**
    *   设置 公司列表
    **/
    public void setCompanyList(List<MrEquityInfoCompanyList> companyList) {
      this.companyList = companyList;
    }
    /**
    *   获取 公司列表
    **/
    public List<MrEquityInfoCompanyList> getCompanyList() {
      return companyList;
    }
    /**
    *   设置 质权人列表
    **/
    public void setPledgeeList(List<MrEquityInfoPledgeeList> pledgeeList) {
      this.pledgeeList = pledgeeList;
    }
    /**
    *   获取 质权人列表
    **/
    public List<MrEquityInfoPledgeeList> getPledgeeList() {
      return pledgeeList;
    }
    /**
    *   设置 出质人列表
    **/
    public void setPledgorList(List<MrEquityInfoPledgorList> pledgorList) {
      this.pledgorList = pledgorList;
    }
    /**
    *   获取 出质人列表
    **/
    public List<MrEquityInfoPledgorList> getPledgorList() {
      return pledgorList;
    }
    /**
    *   设置 出质股权数额
    **/
    public void setEquityAmount(String equityAmount) {
      this.equityAmount = equityAmount;
    }
    /**
    *   获取 出质股权数额
    **/
    public String getEquityAmount() {
      return equityAmount;
    }
    /**
    *   设置 股权出质设立登记日期
    **/
    public void setRegDate(Long regDate) {
      this.regDate = regDate;
    }
    /**
    *   获取 股权出质设立登记日期
    **/
    public Long getRegDate() {
      return regDate;
    }
    /**
    *   设置 状态
    **/
    public void setState(String state) {
      this.state = state;
    }
    /**
    *   获取 状态
    **/
    public String getState() {
      return state;
    }
    /**
    *   设置 出质人
    **/
    public void setPledgor(String pledgor) {
      this.pledgor = pledgor;
    }
    /**
    *   获取 出质人
    **/
    public String getPledgor() {
      return pledgor;
    }
    /**
    *   设置 质权人证照/证件号码
    **/
    public void setCertifNumberR(String certifNumberR) {
      this.certifNumberR = certifNumberR;
    }
    /**
    *   获取 质权人证照/证件号码
    **/
    public String getCertifNumberR() {
      return certifNumberR;
    }
    /**
    *   设置 股权出质设立发布日期
    **/
    public void setPutDate(Long putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 股权出质设立发布日期
    **/
    public Long getPutDate() {
      return putDate;
    }
    /**
    *   设置 质权人
    **/
    public void setPledgee(String pledgee) {
      this.pledgee = pledgee;
    }
    /**
    *   获取 质权人
    **/
    public String getPledgee() {
      return pledgee;
    }



}

