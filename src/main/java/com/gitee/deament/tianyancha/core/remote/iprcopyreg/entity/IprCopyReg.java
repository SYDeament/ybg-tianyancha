package com.gitee.deament.tianyancha.core.remote.iprcopyreg.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.iprcopyreg.entity.IprCopyRegItems;
/**
*软件著作权
* 可以通过公司名称或ID获取软件著作权的有关信息，包括软件名称、登记号、分类号、版本号等字段的详细信息
* /services/open/ipr/copyReg/2.0
*@author deament
**/

public class IprCopyReg implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private String total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<IprCopyRegItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(String total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public String getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<IprCopyRegItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<IprCopyRegItems> getItems() {
      return items;
    }



}

