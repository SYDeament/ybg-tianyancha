package com.gitee.deament.tianyancha.core.remote.hilawsuit.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hilawsuit.entity.HiLawsuit;
import com.gitee.deament.tianyancha.core.remote.hilawsuit.dto.HiLawsuitDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*历史法律诉讼
* 可以通过公司名称或ID获取企业历史的法律诉讼信息，历史法律诉讼包括案件名称、案由、案件身份、案号等字段信息
* /services/open/hi/lawsuit/2.0
*@author deament
**/
@Component("hiLawsuitRequestImpl")
public class HiLawsuitRequestImpl extends BaseRequestImpl<HiLawsuit,HiLawsuitDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/hi/lawsuit/2.0";
    }
}

