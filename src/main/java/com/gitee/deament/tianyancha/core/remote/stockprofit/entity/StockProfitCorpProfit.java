package com.gitee.deament.tianyancha.core.remote.stockprofit.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*利润表
* 属于StockProfit
* /services/open/stock/profit/2.0
*@author deament
**/

public class StockProfitCorpProfit implements Serializable{

    /**
     *    营业利润
    **/
    @JSONField(name="op")
    private String op;
    /**
     *    归属于母公司所有者的综合收益总额
    **/
    @JSONField(name="total_compre_income_atsopc")
    private String total_compre_income_atsopc;
    /**
     *    资产减值损失
    **/
    @JSONField(name="asset_impairment_loss")
    private String asset_impairment_loss;
    /**
     *    综合收益总额
    **/
    @JSONField(name="total_compre_income")
    private String total_compre_income;
    /**
     *    其他经营收益
    **/
    @JSONField(name="othr_income")
    private String othr_income;
    /**
     *    加:投资收益
    **/
    @JSONField(name="invest_income")
    private String invest_income;
    /**
     *    扣除非经常性损益后的净利润
    **/
    @JSONField(name="profit_deduct_nrgal_ly_sq")
    private String profit_deduct_nrgal_ly_sq;
    /**
     *    研发费用
    **/
    @JSONField(name="rad_cost")
    private String rad_cost;
    /**
     *    营业税金及附加
    **/
    @JSONField(name="operating_taxes_and_surcharge")
    private String operating_taxes_and_surcharge;
    /**
     *    年度
    **/
    @JSONField(name="showYear")
    private String showYear;
    /**
     *    基本每股收益
    **/
    @JSONField(name="basic_eps")
    private String basic_eps;
    /**
     *    财务费用
    **/
    @JSONField(name="financing_expenses")
    private String financing_expenses;
    /**
     *    减:所得税费用
    **/
    @JSONField(name="income_tax_expenses")
    private String income_tax_expenses;
    /**
     *    营业收入
    **/
    @JSONField(name="revenue")
    private String revenue;
    /**
     *    利润总额
    **/
    @JSONField(name="profit_total_amt")
    private String profit_total_amt;
    /**
     *    营业总收入
    **/
    @JSONField(name="total_revenue")
    private String total_revenue;
    /**
     *    营业总成本
    **/
    @JSONField(name="operating_costs")
    private String operating_costs;
    /**
     *    销售费用
    **/
    @JSONField(name="sales_fee")
    private String sales_fee;
    /**
     *    净利润
    **/
    @JSONField(name="net_profit")
    private String net_profit;
    /**
     *    管理费用
    **/
    @JSONField(name="manage_fee")
    private String manage_fee;
    /**
     *    其中:归属于母公司股东的净利润
    **/
    @JSONField(name="net_profit_atsopc")
    private String net_profit_atsopc;
    /**
     *    营业成本
    **/
    @JSONField(name="operating_cost")
    private String operating_cost;
    /**
     *    加:营业外收入
    **/
    @JSONField(name="non_operating_income")
    private String non_operating_income;
    /**
     *    减:营业外支出
    **/
    @JSONField(name="non_operating_payout")
    private String non_operating_payout;


    /**
    *   设置 营业利润
    **/
    public void setOp(String op) {
      this.op = op;
    }
    /**
    *   获取 营业利润
    **/
    public String getOp() {
      return op;
    }
    /**
    *   设置 归属于母公司所有者的综合收益总额
    **/
    public void setTotal_compre_income_atsopc(String total_compre_income_atsopc) {
      this.total_compre_income_atsopc = total_compre_income_atsopc;
    }
    /**
    *   获取 归属于母公司所有者的综合收益总额
    **/
    public String getTotal_compre_income_atsopc() {
      return total_compre_income_atsopc;
    }
    /**
    *   设置 资产减值损失
    **/
    public void setAsset_impairment_loss(String asset_impairment_loss) {
      this.asset_impairment_loss = asset_impairment_loss;
    }
    /**
    *   获取 资产减值损失
    **/
    public String getAsset_impairment_loss() {
      return asset_impairment_loss;
    }
    /**
    *   设置 综合收益总额
    **/
    public void setTotal_compre_income(String total_compre_income) {
      this.total_compre_income = total_compre_income;
    }
    /**
    *   获取 综合收益总额
    **/
    public String getTotal_compre_income() {
      return total_compre_income;
    }
    /**
    *   设置 其他经营收益
    **/
    public void setOthr_income(String othr_income) {
      this.othr_income = othr_income;
    }
    /**
    *   获取 其他经营收益
    **/
    public String getOthr_income() {
      return othr_income;
    }
    /**
    *   设置 加:投资收益
    **/
    public void setInvest_income(String invest_income) {
      this.invest_income = invest_income;
    }
    /**
    *   获取 加:投资收益
    **/
    public String getInvest_income() {
      return invest_income;
    }
    /**
    *   设置 扣除非经常性损益后的净利润
    **/
    public void setProfit_deduct_nrgal_ly_sq(String profit_deduct_nrgal_ly_sq) {
      this.profit_deduct_nrgal_ly_sq = profit_deduct_nrgal_ly_sq;
    }
    /**
    *   获取 扣除非经常性损益后的净利润
    **/
    public String getProfit_deduct_nrgal_ly_sq() {
      return profit_deduct_nrgal_ly_sq;
    }
    /**
    *   设置 研发费用
    **/
    public void setRad_cost(String rad_cost) {
      this.rad_cost = rad_cost;
    }
    /**
    *   获取 研发费用
    **/
    public String getRad_cost() {
      return rad_cost;
    }
    /**
    *   设置 营业税金及附加
    **/
    public void setOperating_taxes_and_surcharge(String operating_taxes_and_surcharge) {
      this.operating_taxes_and_surcharge = operating_taxes_and_surcharge;
    }
    /**
    *   获取 营业税金及附加
    **/
    public String getOperating_taxes_and_surcharge() {
      return operating_taxes_and_surcharge;
    }
    /**
    *   设置 年度
    **/
    public void setShowYear(String showYear) {
      this.showYear = showYear;
    }
    /**
    *   获取 年度
    **/
    public String getShowYear() {
      return showYear;
    }
    /**
    *   设置 基本每股收益
    **/
    public void setBasic_eps(String basic_eps) {
      this.basic_eps = basic_eps;
    }
    /**
    *   获取 基本每股收益
    **/
    public String getBasic_eps() {
      return basic_eps;
    }
    /**
    *   设置 财务费用
    **/
    public void setFinancing_expenses(String financing_expenses) {
      this.financing_expenses = financing_expenses;
    }
    /**
    *   获取 财务费用
    **/
    public String getFinancing_expenses() {
      return financing_expenses;
    }
    /**
    *   设置 减:所得税费用
    **/
    public void setIncome_tax_expenses(String income_tax_expenses) {
      this.income_tax_expenses = income_tax_expenses;
    }
    /**
    *   获取 减:所得税费用
    **/
    public String getIncome_tax_expenses() {
      return income_tax_expenses;
    }
    /**
    *   设置 营业收入
    **/
    public void setRevenue(String revenue) {
      this.revenue = revenue;
    }
    /**
    *   获取 营业收入
    **/
    public String getRevenue() {
      return revenue;
    }
    /**
    *   设置 利润总额
    **/
    public void setProfit_total_amt(String profit_total_amt) {
      this.profit_total_amt = profit_total_amt;
    }
    /**
    *   获取 利润总额
    **/
    public String getProfit_total_amt() {
      return profit_total_amt;
    }
    /**
    *   设置 营业总收入
    **/
    public void setTotal_revenue(String total_revenue) {
      this.total_revenue = total_revenue;
    }
    /**
    *   获取 营业总收入
    **/
    public String getTotal_revenue() {
      return total_revenue;
    }
    /**
    *   设置 营业总成本
    **/
    public void setOperating_costs(String operating_costs) {
      this.operating_costs = operating_costs;
    }
    /**
    *   获取 营业总成本
    **/
    public String getOperating_costs() {
      return operating_costs;
    }
    /**
    *   设置 销售费用
    **/
    public void setSales_fee(String sales_fee) {
      this.sales_fee = sales_fee;
    }
    /**
    *   获取 销售费用
    **/
    public String getSales_fee() {
      return sales_fee;
    }
    /**
    *   设置 净利润
    **/
    public void setNet_profit(String net_profit) {
      this.net_profit = net_profit;
    }
    /**
    *   获取 净利润
    **/
    public String getNet_profit() {
      return net_profit;
    }
    /**
    *   设置 管理费用
    **/
    public void setManage_fee(String manage_fee) {
      this.manage_fee = manage_fee;
    }
    /**
    *   获取 管理费用
    **/
    public String getManage_fee() {
      return manage_fee;
    }
    /**
    *   设置 其中:归属于母公司股东的净利润
    **/
    public void setNet_profit_atsopc(String net_profit_atsopc) {
      this.net_profit_atsopc = net_profit_atsopc;
    }
    /**
    *   获取 其中:归属于母公司股东的净利润
    **/
    public String getNet_profit_atsopc() {
      return net_profit_atsopc;
    }
    /**
    *   设置 营业成本
    **/
    public void setOperating_cost(String operating_cost) {
      this.operating_cost = operating_cost;
    }
    /**
    *   获取 营业成本
    **/
    public String getOperating_cost() {
      return operating_cost;
    }
    /**
    *   设置 加:营业外收入
    **/
    public void setNon_operating_income(String non_operating_income) {
      this.non_operating_income = non_operating_income;
    }
    /**
    *   获取 加:营业外收入
    **/
    public String getNon_operating_income() {
      return non_operating_income;
    }
    /**
    *   设置 减:营业外支出
    **/
    public void setNon_operating_payout(String non_operating_payout) {
      this.non_operating_payout = non_operating_payout;
    }
    /**
    *   获取 减:营业外支出
    **/
    public String getNon_operating_payout() {
      return non_operating_payout;
    }



}

