package com.gitee.deament.tianyancha.core.remote.mdoublerandomcheckdetail.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*双随机抽查详情
* 可以通过双随机抽查ID获取企业双随机抽查详情，企业双随机抽查详情包括检查事项、检查结果等字段的详细信息
* /services/open/m/doubleRandomCheckDetail/2.0
*@author deament
**/

@TYCURL(value="/services/open/m/doubleRandomCheckDetail/2.0")
public class MDoubleRandomCheckDetailDTO implements Serializable{

    /**
     *    抽查id
     *
    **/
    @ParamRequire(require = true)
    private String businessId;
    /**
     *    每页条数（默认20，最大20）
     *
    **/
    @ParamRequire(require = false)
    private Long pageSize;
    /**
     *    当前页（默认1）
     *
    **/
    @ParamRequire(require = false)
    private Long pageNum;


    /**
    *   设置 抽查id
    **/
    public void setBusinessId(String businessId) {
      this.businessId = businessId;
    }
    /**
    *   获取 抽查id
    **/
    public String getBusinessId() {
      return businessId;
    }
    /**
    *   设置 每页条数（默认20，最大20）
    **/
    public void setPageSize(Long pageSize) {
      this.pageSize = pageSize;
    }
    /**
    *   获取 每页条数（默认20，最大20）
    **/
    public Long getPageSize() {
      return pageSize;
    }
    /**
    *   设置 当前页（默认1）
    **/
    public void setPageNum(Long pageNum) {
      this.pageNum = pageNum;
    }
    /**
    *   获取 当前页（默认1）
    **/
    public Long getPageNum() {
      return pageNum;
    }



}

