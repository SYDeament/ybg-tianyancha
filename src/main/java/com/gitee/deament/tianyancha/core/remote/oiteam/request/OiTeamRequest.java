package com.gitee.deament.tianyancha.core.remote.oiteam.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.oiteam.entity.OiTeam;
import com.gitee.deament.tianyancha.core.remote.oiteam.dto.OiTeamDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*机构成员
* 可以通过投资机构名称获取机构成员信息，机构成员信息包括成员姓名、职位、简介等字段的详细信息
* /services/open/oi/team/2.0
*@author deament
**/

public interface OiTeamRequest extends BaseRequest<OiTeamDTO ,OiTeam>{

}

