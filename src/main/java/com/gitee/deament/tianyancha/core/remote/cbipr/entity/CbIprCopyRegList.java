package com.gitee.deament.tianyancha.core.remote.cbipr.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*知识产权
* 属于CbIpr
* /services/open/cb/ipr/2.0
*@author deament
**/

public class CbIprCopyRegList implements Serializable{

    /**
     *    注册时间
    **/
    @JSONField(name="regtime")
    private String regtime;
    /**
     *    发布时间
    **/
    @JSONField(name="publishtime")
    private String publishtime;
    /**
     *    著作权人
    **/
    @JSONField(name="authorNationality")
    private String authorNationality;
    /**
     *    简称
    **/
    @JSONField(name="simplename")
    private String simplename;
    /**
     *    登记号
    **/
    @JSONField(name="regnum")
    private String regnum;
    /**
     *    id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    全称
    **/
    @JSONField(name="fullname")
    private String fullname;
    /**
     *    分类号
    **/
    @JSONField(name="catnum")
    private String catnum;
    /**
     *    版本号
    **/
    @JSONField(name="version")
    private String version;


    /**
    *   设置 注册时间
    **/
    public void setRegtime(String regtime) {
      this.regtime = regtime;
    }
    /**
    *   获取 注册时间
    **/
    public String getRegtime() {
      return regtime;
    }
    /**
    *   设置 发布时间
    **/
    public void setPublishtime(String publishtime) {
      this.publishtime = publishtime;
    }
    /**
    *   获取 发布时间
    **/
    public String getPublishtime() {
      return publishtime;
    }
    /**
    *   设置 著作权人
    **/
    public void setAuthorNationality(String authorNationality) {
      this.authorNationality = authorNationality;
    }
    /**
    *   获取 著作权人
    **/
    public String getAuthorNationality() {
      return authorNationality;
    }
    /**
    *   设置 简称
    **/
    public void setSimplename(String simplename) {
      this.simplename = simplename;
    }
    /**
    *   获取 简称
    **/
    public String getSimplename() {
      return simplename;
    }
    /**
    *   设置 登记号
    **/
    public void setRegnum(String regnum) {
      this.regnum = regnum;
    }
    /**
    *   获取 登记号
    **/
    public String getRegnum() {
      return regnum;
    }
    /**
    *   设置 id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 全称
    **/
    public void setFullname(String fullname) {
      this.fullname = fullname;
    }
    /**
    *   获取 全称
    **/
    public String getFullname() {
      return fullname;
    }
    /**
    *   设置 分类号
    **/
    public void setCatnum(String catnum) {
      this.catnum = catnum;
    }
    /**
    *   获取 分类号
    **/
    public String getCatnum() {
      return catnum;
    }
    /**
    *   设置 版本号
    **/
    public void setVersion(String version) {
      this.version = version;
    }
    /**
    *   获取 版本号
    **/
    public String getVersion() {
      return version;
    }



}

