package com.gitee.deament.tianyancha.core.remote.jrbankruptcy.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*破产重整详情
* 属于JrBankruptcy
* /services/open/jr/bankruptcy/detail/2.0
*@author deament
**/

public class JrBankruptcyApplicant implements Serializable{

    /**
     *    申请人名称
    **/
    @JSONField(name="applicantName")
    private String applicantName;
    /**
     *    申请人id
    **/
    @JSONField(name="applicantGid")
    private String applicantGid;


    /**
    *   设置 申请人名称
    **/
    public void setApplicantName(String applicantName) {
      this.applicantName = applicantName;
    }
    /**
    *   获取 申请人名称
    **/
    public String getApplicantName() {
      return applicantName;
    }
    /**
    *   设置 申请人id
    **/
    public void setApplicantGid(String applicantGid) {
      this.applicantGid = applicantGid;
    }
    /**
    *   获取 申请人id
    **/
    public String getApplicantGid() {
      return applicantGid;
    }



}

