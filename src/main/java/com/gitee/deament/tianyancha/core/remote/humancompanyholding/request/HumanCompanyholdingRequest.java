package com.gitee.deament.tianyancha.core.remote.humancompanyholding.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.humancompanyholding.entity.HumanCompanyholding;
import com.gitee.deament.tianyancha.core.remote.humancompanyholding.dto.HumanCompanyholdingDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*人员控股企业
* 可以通过公司名称、人名获取人员控股企业信息，人员控股企业信息包括控股企业ID、投资比例、企业logo、企业简称、控股企业、注册资本、法人类型、经营状态、法人等字段的信息
* /services/open/human/companyholding/2.0
*@author deament
**/

public interface HumanCompanyholdingRequest extends BaseRequest<HumanCompanyholdingDTO ,HumanCompanyholding>{

}

