package com.gitee.deament.tianyancha.core.remote.pfboss.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.pfboss.entity.PfBoss;
import com.gitee.deament.tianyancha.core.remote.pfboss.dto.PfBossDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*高管信息
* 可以通过公司名称或ID获取相关私募基金高管信息，包括法定代表人/执行事物合伙人姓名、是否有从业资格、资格取得方式等字段的相关信息
* /services/open/pf/boss/2.0
*@author deament
**/
@Component("pfBossRequestImpl")
public class PfBossRequestImpl extends BaseRequestImpl<PfBoss,PfBossDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/pf/boss/2.0";
    }
}

