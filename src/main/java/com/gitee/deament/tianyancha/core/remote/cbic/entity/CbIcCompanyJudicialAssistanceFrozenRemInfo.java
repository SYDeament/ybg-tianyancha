package com.gitee.deament.tianyancha.core.remote.cbic.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*工商信息
* 属于CbIc
* /services/open/cb/ic/2.0
*@author deament
**/

public class CbIcCompanyJudicialAssistanceFrozenRemInfo implements Serializable{

    /**
     *    被执行人
    **/
    @JSONField(name="executedPerson")
    private String executedPerson;
    /**
     *    被执行人证照种类
    **/
    @JSONField(name="licenseType")
    private String licenseType;
    /**
     *    执行通知书文号
    **/
    @JSONField(name="executeNoticeNum")
    private String executeNoticeNum;
    /**
     *    执行事项
    **/
    @JSONField(name="implementationMatters")
    private String implementationMatters;
    /**
     *    公示日期
    **/
    @JSONField(name="publicityDate")
    private String publicityDate;
    /**
     *    执行裁定书文号
    **/
    @JSONField(name="executeOrderNum")
    private String executeOrderNum;
    /**
     *    被执行人证照号码
    **/
    @JSONField(name="licenseNum")
    private String licenseNum;
    /**
     *    被执行人持有股权、其它投资权益的数额
    **/
    @JSONField(name="equityAmountOther")
    private String equityAmountOther;
    /**
     *    执行法院
    **/
    @JSONField(name="executiveCourt")
    private String executiveCourt;
    /**
     *    解除冻结日期
    **/
    @JSONField(name="frozenRemoveDate")
    private String frozenRemoveDate;


    /**
    *   设置 被执行人
    **/
    public void setExecutedPerson(String executedPerson) {
      this.executedPerson = executedPerson;
    }
    /**
    *   获取 被执行人
    **/
    public String getExecutedPerson() {
      return executedPerson;
    }
    /**
    *   设置 被执行人证照种类
    **/
    public void setLicenseType(String licenseType) {
      this.licenseType = licenseType;
    }
    /**
    *   获取 被执行人证照种类
    **/
    public String getLicenseType() {
      return licenseType;
    }
    /**
    *   设置 执行通知书文号
    **/
    public void setExecuteNoticeNum(String executeNoticeNum) {
      this.executeNoticeNum = executeNoticeNum;
    }
    /**
    *   获取 执行通知书文号
    **/
    public String getExecuteNoticeNum() {
      return executeNoticeNum;
    }
    /**
    *   设置 执行事项
    **/
    public void setImplementationMatters(String implementationMatters) {
      this.implementationMatters = implementationMatters;
    }
    /**
    *   获取 执行事项
    **/
    public String getImplementationMatters() {
      return implementationMatters;
    }
    /**
    *   设置 公示日期
    **/
    public void setPublicityDate(String publicityDate) {
      this.publicityDate = publicityDate;
    }
    /**
    *   获取 公示日期
    **/
    public String getPublicityDate() {
      return publicityDate;
    }
    /**
    *   设置 执行裁定书文号
    **/
    public void setExecuteOrderNum(String executeOrderNum) {
      this.executeOrderNum = executeOrderNum;
    }
    /**
    *   获取 执行裁定书文号
    **/
    public String getExecuteOrderNum() {
      return executeOrderNum;
    }
    /**
    *   设置 被执行人证照号码
    **/
    public void setLicenseNum(String licenseNum) {
      this.licenseNum = licenseNum;
    }
    /**
    *   获取 被执行人证照号码
    **/
    public String getLicenseNum() {
      return licenseNum;
    }
    /**
    *   设置 被执行人持有股权、其它投资权益的数额
    **/
    public void setEquityAmountOther(String equityAmountOther) {
      this.equityAmountOther = equityAmountOther;
    }
    /**
    *   获取 被执行人持有股权、其它投资权益的数额
    **/
    public String getEquityAmountOther() {
      return equityAmountOther;
    }
    /**
    *   设置 执行法院
    **/
    public void setExecutiveCourt(String executiveCourt) {
      this.executiveCourt = executiveCourt;
    }
    /**
    *   获取 执行法院
    **/
    public String getExecutiveCourt() {
      return executiveCourt;
    }
    /**
    *   设置 解除冻结日期
    **/
    public void setFrozenRemoveDate(String frozenRemoveDate) {
      this.frozenRemoveDate = frozenRemoveDate;
    }
    /**
    *   获取 解除冻结日期
    **/
    public String getFrozenRemoveDate() {
      return frozenRemoveDate;
    }



}

