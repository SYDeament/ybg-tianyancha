package com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.entity.MTeleCommunicationLicense;
import com.gitee.deament.tianyancha.core.remote.mtelecommunicationlicense.dto.MTeleCommunicationLicenseDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*电信许可
* 可以通过公司名称或ID获取企业电信许可信息，企业电信许可信息包括客户、销售占比、销售金额、报告期等字段的详细信息
* /services/open/m/teleCommunicationLicense/2.0
*@author deament
**/
@Component("mTeleCommunicationLicenseRequestImpl")
public class MTeleCommunicationLicenseRequestImpl extends BaseRequestImpl<MTeleCommunicationLicense,MTeleCommunicationLicenseDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/m/teleCommunicationLicense/2.0";
    }
}

