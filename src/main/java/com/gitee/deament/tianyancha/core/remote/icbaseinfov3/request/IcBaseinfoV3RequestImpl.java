package com.gitee.deament.tianyancha.core.remote.icbaseinfov3.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.icbaseinfov3.entity.IcBaseinfoV3;
import com.gitee.deament.tianyancha.core.remote.icbaseinfov3.dto.IcBaseinfoV3DTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*企业基本信息（含主要人员）
* 可以通过公司名称或ID获取企业基本信息和主要人员信息，包括公司名称或ID、类型、成立日期、联系方式、主要人员名称、职位等字段的详细信息
* /services/open/ic/baseinfoV3/2.0
*@author deament
**/
@Component("icBaseinfoV3RequestImpl")
public class IcBaseinfoV3RequestImpl extends BaseRequestImpl<IcBaseinfoV3,IcBaseinfoV3DTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/ic/baseinfoV3/2.0";
    }
}

