package com.gitee.deament.tianyancha.core.remote.jrendcase.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrendcase.entity.JrEndCaseItems;
/**
*终本案件
* 可以通过公司名称或ID获取终本案件信息，终本案件信息包括执行法院、案件内容、被执行人名称等字段的详细信息
* /services/open/jr/endCase/2.0
*@author deament
**/

public class JrEndCase implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrEndCaseItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrEndCaseItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrEndCaseItems> getItems() {
      return items;
    }



}

