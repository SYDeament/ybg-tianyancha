package com.gitee.deament.tianyancha.core.remote.mcheckinfo.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mcheckinfo.entity.MCheckInfo;
import com.gitee.deament.tianyancha.core.remote.mcheckinfo.dto.MCheckInfoDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
/**
*抽查检查
* 可以通过公司名称或ID获取企业抽查检查信息，企业抽查检查信息包括类型、结果、实施机关等字段的详细信息
* /services/open/m/checkInfo/2.0
*@author deament
**/

public interface MCheckInfoRequest extends BaseRequest<MCheckInfoDTO ,MCheckInfo>{

}

