package com.gitee.deament.tianyancha.core.remote.hidishonest.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*历史失信人
* 属于HiDishonest
* /services/open/hi/dishonest/2.0
*@author deament
**/

public class HiDishonestItems implements Serializable{

    /**
     *    法人、负责人姓名
    **/
    @JSONField(name="businessentity")
    private String businessentity;
    /**
     *    省份地区
    **/
    @JSONField(name="areaname")
    private String areaname;
    /**
     *    法院
    **/
    @JSONField(name="courtname")
    private String courtname;
    /**
     *    失信人类型，0代表人，1代表公司
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    失信人名称
    **/
    @JSONField(name="iname")
    private String iname;
    /**
     *    失信被执行人行为具体情形
    **/
    @JSONField(name="disrupttypename")
    private String disrupttypename;
    /**
     *    案号
    **/
    @JSONField(name="casecode")
    private String casecode;
    /**
     *    身份证号码/组织机构代码
    **/
    @JSONField(name="cardnum")
    private String cardnum;
    /**
     *    履行情况
    **/
    @JSONField(name="performance")
    private String performance;
    /**
     *    立案时间
    **/
    @JSONField(name="regdate")
    private Long regdate;
    /**
     *    发布时间
    **/
    @JSONField(name="publishdate")
    private Long publishdate;
    /**
     *    做出执行的依据单位
    **/
    @JSONField(name="gistunit")
    private String gistunit;
    /**
     *    生效法律文书确定的义务
    **/
    @JSONField(name="duty")
    private String duty;
    /**
     *    执行依据文号
    **/
    @JSONField(name="gistid")
    private String gistid;


    /**
    *   设置 法人、负责人姓名
    **/
    public void setBusinessentity(String businessentity) {
      this.businessentity = businessentity;
    }
    /**
    *   获取 法人、负责人姓名
    **/
    public String getBusinessentity() {
      return businessentity;
    }
    /**
    *   设置 省份地区
    **/
    public void setAreaname(String areaname) {
      this.areaname = areaname;
    }
    /**
    *   获取 省份地区
    **/
    public String getAreaname() {
      return areaname;
    }
    /**
    *   设置 法院
    **/
    public void setCourtname(String courtname) {
      this.courtname = courtname;
    }
    /**
    *   获取 法院
    **/
    public String getCourtname() {
      return courtname;
    }
    /**
    *   设置 失信人类型，0代表人，1代表公司
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 失信人类型，0代表人，1代表公司
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 失信人名称
    **/
    public void setIname(String iname) {
      this.iname = iname;
    }
    /**
    *   获取 失信人名称
    **/
    public String getIname() {
      return iname;
    }
    /**
    *   设置 失信被执行人行为具体情形
    **/
    public void setDisrupttypename(String disrupttypename) {
      this.disrupttypename = disrupttypename;
    }
    /**
    *   获取 失信被执行人行为具体情形
    **/
    public String getDisrupttypename() {
      return disrupttypename;
    }
    /**
    *   设置 案号
    **/
    public void setCasecode(String casecode) {
      this.casecode = casecode;
    }
    /**
    *   获取 案号
    **/
    public String getCasecode() {
      return casecode;
    }
    /**
    *   设置 身份证号码/组织机构代码
    **/
    public void setCardnum(String cardnum) {
      this.cardnum = cardnum;
    }
    /**
    *   获取 身份证号码/组织机构代码
    **/
    public String getCardnum() {
      return cardnum;
    }
    /**
    *   设置 履行情况
    **/
    public void setPerformance(String performance) {
      this.performance = performance;
    }
    /**
    *   获取 履行情况
    **/
    public String getPerformance() {
      return performance;
    }
    /**
    *   设置 立案时间
    **/
    public void setRegdate(Long regdate) {
      this.regdate = regdate;
    }
    /**
    *   获取 立案时间
    **/
    public Long getRegdate() {
      return regdate;
    }
    /**
    *   设置 发布时间
    **/
    public void setPublishdate(Long publishdate) {
      this.publishdate = publishdate;
    }
    /**
    *   获取 发布时间
    **/
    public Long getPublishdate() {
      return publishdate;
    }
    /**
    *   设置 做出执行的依据单位
    **/
    public void setGistunit(String gistunit) {
      this.gistunit = gistunit;
    }
    /**
    *   获取 做出执行的依据单位
    **/
    public String getGistunit() {
      return gistunit;
    }
    /**
    *   设置 生效法律文书确定的义务
    **/
    public void setDuty(String duty) {
      this.duty = duty;
    }
    /**
    *   获取 生效法律文书确定的义务
    **/
    public String getDuty() {
      return duty;
    }
    /**
    *   设置 执行依据文号
    **/
    public void setGistid(String gistid) {
      this.gistid = gistid;
    }
    /**
    *   获取 执行依据文号
    **/
    public String getGistid() {
      return gistid;
    }



}

