package com.gitee.deament.tianyancha.core.remote.mdoublerandomcheckdetail.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mdoublerandomcheckdetail.entity.MDoubleRandomCheckDetailItems;
/**
*双随机抽查详情
* 可以通过双随机抽查ID获取企业双随机抽查详情，企业双随机抽查详情包括检查事项、检查结果等字段的详细信息
* /services/open/m/doubleRandomCheckDetail/2.0
*@author deament
**/

public class MDoubleRandomCheckDetail implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<MDoubleRandomCheckDetailItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<MDoubleRandomCheckDetailItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<MDoubleRandomCheckDetailItems> getItems() {
      return items;
    }



}

