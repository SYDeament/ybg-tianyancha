package com.gitee.deament.tianyancha.core.remote.cdprivatefund.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.cdprivatefund.entity.CdPrivateFund;
import com.gitee.deament.tianyancha.core.remote.cdprivatefund.dto.CdPrivateFundDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*私募基金
* 可以通过公司名称或ID获企业相关私募基金信息，私募基金信息包括私募基金管理人名称、法定代表人/执行事务合伙人、机构类型、登记编号等字段的详细信息
* /services/open/cd/privateFund/2.0
*@author deament
**/
@Component("cdPrivateFundRequestImpl")
public class CdPrivateFundRequestImpl extends BaseRequestImpl<CdPrivateFund,CdPrivateFundDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/cd/privateFund/2.0";
    }
}

