package com.gitee.deament.tianyancha.core.remote.hiannouncement.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiannouncement.entity.HiAnnouncementPlaintiff;
import com.gitee.deament.tianyancha.core.remote.hiannouncement.entity.HiAnnouncementDefendant;
import com.gitee.deament.tianyancha.core.remote.hiannouncement.entity.HiAnnouncementItems;
/**
*历史开庭公告
* 可以通过公司名称或ID获取企业历史的开庭公告，历史开庭公告信息包括被告/被上诉人、法院、原告/上诉人、开庭日期、案由、内部ID、案号等字段信息
* /services/open/hi/announcement/2.0
*@author deament
**/

public class HiAnnouncement implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiAnnouncementItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiAnnouncementItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiAnnouncementItems> getItems() {
      return items;
    }



}

