package com.gitee.deament.tianyancha.core.remote.mbids.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*招投标信息垂搜
* 属于MBids
* /services/open/m/bids/search
*@author deament
**/

public class MBidsCompanies implements Serializable{

    /**
     *    公司
    **/
    @JSONField(name="cgid")
    private Integer cgid;
    /**
     *    公司名
    **/
    @JSONField(name="cname")
    private String cname;


    /**
    *   设置 公司
    **/
    public void setCgid(Integer cgid) {
      this.cgid = cgid;
    }
    /**
    *   获取 公司
    **/
    public Integer getCgid() {
      return cgid;
    }
    /**
    *   设置 公司名
    **/
    public void setCname(String cname) {
      this.cname = cname;
    }
    /**
    *   获取 公司名
    **/
    public String getCname() {
      return cname;
    }



}

