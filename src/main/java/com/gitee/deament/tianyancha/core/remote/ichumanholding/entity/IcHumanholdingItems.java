package com.gitee.deament.tianyancha.core.remote.ichumanholding.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*最终受益人
* 属于IcHumanholding
* /services/open/ic/humanholding/2.0
*@author deament
**/

public class IcHumanholdingItems implements Serializable{

    /**
     *    
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    法人列表
    **/
    @JSONField(name="chainList")
    private List<List<IcHumanholdingChainList>> chainList;
    /**
     *    姓名
    **/
    @JSONField(name="name")
    private String name;
    /**
     *    logo
    **/
    @JSONField(name="logo")
    private String logo;
    /**
     *    人id
    **/
    @JSONField(name="id")
    private Integer id;
    /**
     *    human company
    **/
    @JSONField(name="type")
    private String type;
    /**
     *    占比
    **/
    @JSONField(name="percent")
    private String percent;
    /**
     *    公司id
    **/
    @JSONField(name="cid")
    private Integer cid;


    /**
    *   设置 
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 法人列表
    **/
    public void setChainList(List<List<IcHumanholdingChainList>> chainList) {
      this.chainList = chainList;
    }
    /**
    *   获取 法人列表
    **/
    public List<List<IcHumanholdingChainList>> getChainList() {
      return chainList;
    }
    /**
    *   设置 姓名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 姓名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 logo
    **/
    public void setLogo(String logo) {
      this.logo = logo;
    }
    /**
    *   获取 logo
    **/
    public String getLogo() {
      return logo;
    }
    /**
    *   设置 人id
    **/
    public void setId(Integer id) {
      this.id = id;
    }
    /**
    *   获取 人id
    **/
    public Integer getId() {
      return id;
    }
    /**
    *   设置 human company
    **/
    public void setType(String type) {
      this.type = type;
    }
    /**
    *   获取 human company
    **/
    public String getType() {
      return type;
    }
    /**
    *   设置 占比
    **/
    public void setPercent(String percent) {
      this.percent = percent;
    }
    /**
    *   获取 占比
    **/
    public String getPercent() {
      return percent;
    }
    /**
    *   设置 公司id
    **/
    public void setCid(Integer cid) {
      this.cid = cid;
    }
    /**
    *   获取 公司id
    **/
    public Integer getCid() {
      return cid;
    }



}

