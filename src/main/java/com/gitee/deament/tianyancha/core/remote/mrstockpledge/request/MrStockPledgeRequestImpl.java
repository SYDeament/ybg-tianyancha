package com.gitee.deament.tianyancha.core.remote.mrstockpledge.request;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.mrstockpledge.entity.MrStockPledge;
import com.gitee.deament.tianyancha.core.remote.mrstockpledge.dto.MrStockPledgeDTO;
import com.gitee.deament.tianyancha.core.base.BaseRequest;
import com.gitee.deament.tianyancha.core.base.BaseRequestImpl;
import org.springframework.stereotype.Component;
/**
*质押走势
* 可以通过公司名称或ID获取股权质押明细信息，股权质押走势信息包括股东名称、质押股份数量、质押股份市值等字段的详细信息
* /services/open/mr/stockPledge/trend/2.0
*@author deament
**/
@Component("mrStockPledgeRequestImpl")
public class MrStockPledgeRequestImpl extends BaseRequestImpl<MrStockPledge,MrStockPledgeDTO> {
    @Override
    public  String getUrl(){
        return "http://open.api.tianyancha.com"+"/services/open/mr/stockPledge/trend/2.0";
    }
}

