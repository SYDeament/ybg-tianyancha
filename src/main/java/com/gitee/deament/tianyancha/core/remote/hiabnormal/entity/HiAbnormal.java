package com.gitee.deament.tianyancha.core.remote.hiabnormal.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.hiabnormal.entity.HiAbnormalItems;
/**
*历史经营异常
* 可以通过公司名称或ID获取企业历史的经营异常信息，历史经营异常信息包括列入/移除原因、时间、做出决定机关等字段信息
* /services/open/hi/abnormal/2.0
*@author deament
**/

public class HiAbnormal implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<HiAbnormalItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<HiAbnormalItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<HiAbnormalItems> getItems() {
      return items;
    }



}

