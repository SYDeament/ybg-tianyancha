package com.gitee.deament.tianyancha.core.remote.mlandtransfer.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*土地转让详情
* 可以通过土地转让ID获取企业土地转让详情，企业土地转让详情包括土地坐落、原/现土地使用人、面积、转让价格、转让方式等字段的详细信息
* /services/open/m/landTransfer/detail/2.0
*@author deament
**/

public class MLandTransfer implements Serializable{

    /**
     *    土地面积（公顷）
    **/
    @JSONField(name="area")
    private String area;
    /**
     *    成交日期
    **/
    @JSONField(name="merchandise_time")
    private String merchandise_time;
    /**
     *    土地使用年限
    **/
    @JSONField(name="years_of_use")
    private String years_of_use;
    /**
     *    土地级别
    **/
    @JSONField(name="level")
    private String level;
    /**
     *    宗地编号
    **/
    @JSONField(name="num")
    private String num;
    /**
     *    所在行政区
    **/
    @JSONField(name="aministrativeArea")
    private String aministrativeArea;
    /**
     *    原土地使用权人
    **/
    @JSONField(name="user_change_pre_clean")
    private List<String> user_change_pre_clean;
    /**
     *    转让价格（万元）
    **/
    @JSONField(name="merchandise_price")
    private String merchandise_price;
    /**
     *    土地使用权类型
    **/
    @JSONField(name="use_type")
    private String use_type;
    /**
     *    现土地使用权人
    **/
    @JSONField(name="user_change_now_clean")
    private String user_change_now_clean;
    /**
     *    宗地座落
    **/
    @JSONField(name="location")
    private String location;
    /**
     *    土地用途
    **/
    @JSONField(name="useful")
    private String useful;
    /**
     *    转让方式
    **/
    @JSONField(name="merchandise_type")
    private String merchandise_type;
    /**
     *    备注
    **/
    @JSONField(name="mark")
    private String mark;
    /**
     *    土地利用状况
    **/
    @JSONField(name="situation")
    private String situation;


    /**
    *   设置 土地面积（公顷）
    **/
    public void setArea(String area) {
      this.area = area;
    }
    /**
    *   获取 土地面积（公顷）
    **/
    public String getArea() {
      return area;
    }
    /**
    *   设置 成交日期
    **/
    public void setMerchandise_time(String merchandise_time) {
      this.merchandise_time = merchandise_time;
    }
    /**
    *   获取 成交日期
    **/
    public String getMerchandise_time() {
      return merchandise_time;
    }
    /**
    *   设置 土地使用年限
    **/
    public void setYears_of_use(String years_of_use) {
      this.years_of_use = years_of_use;
    }
    /**
    *   获取 土地使用年限
    **/
    public String getYears_of_use() {
      return years_of_use;
    }
    /**
    *   设置 土地级别
    **/
    public void setLevel(String level) {
      this.level = level;
    }
    /**
    *   获取 土地级别
    **/
    public String getLevel() {
      return level;
    }
    /**
    *   设置 宗地编号
    **/
    public void setNum(String num) {
      this.num = num;
    }
    /**
    *   获取 宗地编号
    **/
    public String getNum() {
      return num;
    }
    /**
    *   设置 所在行政区
    **/
    public void setAministrativeArea(String aministrativeArea) {
      this.aministrativeArea = aministrativeArea;
    }
    /**
    *   获取 所在行政区
    **/
    public String getAministrativeArea() {
      return aministrativeArea;
    }
    /**
    *   设置 原土地使用权人
    **/
    public void setUser_change_pre_clean(List<String> user_change_pre_clean) {
      this.user_change_pre_clean = user_change_pre_clean;
    }
    /**
    *   获取 原土地使用权人
    **/
    public List<String> getUser_change_pre_clean() {
      return user_change_pre_clean;
    }
    /**
    *   设置 转让价格（万元）
    **/
    public void setMerchandise_price(String merchandise_price) {
      this.merchandise_price = merchandise_price;
    }
    /**
    *   获取 转让价格（万元）
    **/
    public String getMerchandise_price() {
      return merchandise_price;
    }
    /**
    *   设置 土地使用权类型
    **/
    public void setUse_type(String use_type) {
      this.use_type = use_type;
    }
    /**
    *   获取 土地使用权类型
    **/
    public String getUse_type() {
      return use_type;
    }
    /**
    *   设置 现土地使用权人
    **/
    public void setUser_change_now_clean(String user_change_now_clean) {
      this.user_change_now_clean = user_change_now_clean;
    }
    /**
    *   获取 现土地使用权人
    **/
    public String getUser_change_now_clean() {
      return user_change_now_clean;
    }
    /**
    *   设置 宗地座落
    **/
    public void setLocation(String location) {
      this.location = location;
    }
    /**
    *   获取 宗地座落
    **/
    public String getLocation() {
      return location;
    }
    /**
    *   设置 土地用途
    **/
    public void setUseful(String useful) {
      this.useful = useful;
    }
    /**
    *   获取 土地用途
    **/
    public String getUseful() {
      return useful;
    }
    /**
    *   设置 转让方式
    **/
    public void setMerchandise_type(String merchandise_type) {
      this.merchandise_type = merchandise_type;
    }
    /**
    *   获取 转让方式
    **/
    public String getMerchandise_type() {
      return merchandise_type;
    }
    /**
    *   设置 备注
    **/
    public void setMark(String mark) {
      this.mark = mark;
    }
    /**
    *   获取 备注
    **/
    public String getMark() {
      return mark;
    }
    /**
    *   设置 土地利用状况
    **/
    public void setSituation(String situation) {
      this.situation = situation;
    }
    /**
    *   获取 土地利用状况
    **/
    public String getSituation() {
      return situation;
    }



}

