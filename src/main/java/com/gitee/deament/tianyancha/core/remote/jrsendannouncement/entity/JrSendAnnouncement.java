package com.gitee.deament.tianyancha.core.remote.jrsendannouncement.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrsendannouncement.entity.JrSendAnnouncementItems;
/**
*送达公告
* 可以通过公司名称或ID获取送达公告信息，送达公告信息包括公告名称、法院名称、公告内容、发布日期等字段的详细信息
* /services/open/jr/sendAnnouncement/2.0
*@author deament
**/

public class JrSendAnnouncement implements Serializable{

    /**
     *    总数
    **/
    @JSONField(name="total")
    private Integer total;
    /**
     *    
    **/
    @JSONField(name="items")
    private List<JrSendAnnouncementItems> items;


    /**
    *   设置 总数
    **/
    public void setTotal(Integer total) {
      this.total = total;
    }
    /**
    *   获取 总数
    **/
    public Integer getTotal() {
      return total;
    }
    /**
    *   设置 
    **/
    public void setItems(List<JrSendAnnouncementItems> items) {
      this.items = items;
    }
    /**
    *   获取 
    **/
    public List<JrSendAnnouncementItems> getItems() {
      return items;
    }



}

