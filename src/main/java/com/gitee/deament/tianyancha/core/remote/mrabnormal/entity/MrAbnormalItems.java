package com.gitee.deament.tianyancha.core.remote.mrabnormal.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
/**
*经营异常
* 属于MrAbnormal
* /services/open/mr/abnormal/2.0
*@author deament
**/

public class MrAbnormalItems implements Serializable{

    /**
     *    移出日期
    **/
    @JSONField(name="removeDate")
    private String removeDate;
    /**
     *    列入异常名录原因
    **/
    @JSONField(name="putReason")
    private String putReason;
    /**
     *    决定列入异常名录部门(作出决定机关)
    **/
    @JSONField(name="putDepartment")
    private String putDepartment;
    /**
     *    移出部门
    **/
    @JSONField(name="removeDepartment")
    private String removeDepartment;
    /**
     *    移除异常名录原因
    **/
    @JSONField(name="removeReason")
    private String removeReason;
    /**
     *    列入日期
    **/
    @JSONField(name="putDate")
    private String putDate;


    /**
    *   设置 移出日期
    **/
    public void setRemoveDate(String removeDate) {
      this.removeDate = removeDate;
    }
    /**
    *   获取 移出日期
    **/
    public String getRemoveDate() {
      return removeDate;
    }
    /**
    *   设置 列入异常名录原因
    **/
    public void setPutReason(String putReason) {
      this.putReason = putReason;
    }
    /**
    *   获取 列入异常名录原因
    **/
    public String getPutReason() {
      return putReason;
    }
    /**
    *   设置 决定列入异常名录部门(作出决定机关)
    **/
    public void setPutDepartment(String putDepartment) {
      this.putDepartment = putDepartment;
    }
    /**
    *   获取 决定列入异常名录部门(作出决定机关)
    **/
    public String getPutDepartment() {
      return putDepartment;
    }
    /**
    *   设置 移出部门
    **/
    public void setRemoveDepartment(String removeDepartment) {
      this.removeDepartment = removeDepartment;
    }
    /**
    *   获取 移出部门
    **/
    public String getRemoveDepartment() {
      return removeDepartment;
    }
    /**
    *   设置 移除异常名录原因
    **/
    public void setRemoveReason(String removeReason) {
      this.removeReason = removeReason;
    }
    /**
    *   获取 移除异常名录原因
    **/
    public String getRemoveReason() {
      return removeReason;
    }
    /**
    *   设置 列入日期
    **/
    public void setPutDate(String putDate) {
      this.putDate = putDate;
    }
    /**
    *   获取 列入日期
    **/
    public String getPutDate() {
      return putDate;
    }



}

