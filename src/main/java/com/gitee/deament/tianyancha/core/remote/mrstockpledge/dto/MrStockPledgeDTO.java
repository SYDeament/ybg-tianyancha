package com.gitee.deament.tianyancha.core.remote.mrstockpledge.dto;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.annotation.ParamRequire;
import com.gitee.deament.tianyancha.annotation.TYCURL;
/**
*质押走势
* 可以通过公司名称或ID获取股权质押明细信息，股权质押走势信息包括股东名称、质押股份数量、质押股份市值等字段的详细信息
* /services/open/mr/stockPledge/trend/2.0
*@author deament
**/

@TYCURL(value="/services/open/mr/stockPledge/trend/2.0")
public class MrStockPledgeDTO implements Serializable{

    /**
     *    企业名
     *
    **/
    @ParamRequire(require = false)
    private String name;
    /**
     *    企业id
     *
    **/
    @ParamRequire(require = false)
    private Long id;
    /**
     *    （id、name、统一信用代码，只需输入其中一个）
     *
    **/
    @ParamRequire(require = false)
    private String keyword;


    /**
    *   设置 企业名
    **/
    public void setName(String name) {
      this.name = name;
    }
    /**
    *   获取 企业名
    **/
    public String getName() {
      return name;
    }
    /**
    *   设置 企业id
    **/
    public void setId(Long id) {
      this.id = id;
    }
    /**
    *   获取 企业id
    **/
    public Long getId() {
      return id;
    }
    /**
    *   设置 （id、name、统一信用代码，只需输入其中一个）
    **/
    public void setKeyword(String keyword) {
      this.keyword = keyword;
    }
    /**
    *   获取 （id、name、统一信用代码，只需输入其中一个）
    **/
    public String getKeyword() {
      return keyword;
    }



}

