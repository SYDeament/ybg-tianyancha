package com.gitee.deament.tianyancha.core.remote.jrbankruptcy.entity;
import java.io.Serializable;
import java.util.List;
import com.alibaba.fastjson.annotation.JSONField;
import com.gitee.deament.tianyancha.core.remote.jrbankruptcy.entity.JrBankruptcyApplicant;
/**
*破产重整详情
* 可以通过公司名称或ID获取破产重整信息，破产重整信息包括破产案件公开时间、申请人、被申请人、申请对象、管理人主要负责人等字段的详细信息
* /services/open/jr/bankruptcy/detail/2.0
*@author deament
**/

public class JrBankruptcy implements Serializable{

    /**
     *    公开时间，发布时间
    **/
    @JSONField(name="submitTime")
    private String submitTime;
    /**
     *    管理人主要负责人
    **/
    @JSONField(name="managerPrincipal")
    private String managerPrincipal;
    /**
     *    被申请人
    **/
    @JSONField(name="respondent")
    private String respondent;
    /**
     *    法院
    **/
    @JSONField(name="court")
    private String court;
    /**
     *    
    **/
    @JSONField(name="uuid")
    private String uuid;
    /**
     *    案号
    **/
    @JSONField(name="caseNo")
    private String caseNo;
    /**
     *    案件类型
    **/
    @JSONField(name="caseType")
    private String caseType;
    /**
     *    申请人对象列表
    **/
    @JSONField(name="applicant")
    private List<JrBankruptcyApplicant> applicant;
    /**
     *    管理人机构
    **/
    @JSONField(name="managerialAgency")
    private String managerialAgency;


    /**
    *   设置 公开时间，发布时间
    **/
    public void setSubmitTime(String submitTime) {
      this.submitTime = submitTime;
    }
    /**
    *   获取 公开时间，发布时间
    **/
    public String getSubmitTime() {
      return submitTime;
    }
    /**
    *   设置 管理人主要负责人
    **/
    public void setManagerPrincipal(String managerPrincipal) {
      this.managerPrincipal = managerPrincipal;
    }
    /**
    *   获取 管理人主要负责人
    **/
    public String getManagerPrincipal() {
      return managerPrincipal;
    }
    /**
    *   设置 被申请人
    **/
    public void setRespondent(String respondent) {
      this.respondent = respondent;
    }
    /**
    *   获取 被申请人
    **/
    public String getRespondent() {
      return respondent;
    }
    /**
    *   设置 法院
    **/
    public void setCourt(String court) {
      this.court = court;
    }
    /**
    *   获取 法院
    **/
    public String getCourt() {
      return court;
    }
    /**
    *   设置 
    **/
    public void setUuid(String uuid) {
      this.uuid = uuid;
    }
    /**
    *   获取 
    **/
    public String getUuid() {
      return uuid;
    }
    /**
    *   设置 案号
    **/
    public void setCaseNo(String caseNo) {
      this.caseNo = caseNo;
    }
    /**
    *   获取 案号
    **/
    public String getCaseNo() {
      return caseNo;
    }
    /**
    *   设置 案件类型
    **/
    public void setCaseType(String caseType) {
      this.caseType = caseType;
    }
    /**
    *   获取 案件类型
    **/
    public String getCaseType() {
      return caseType;
    }
    /**
    *   设置 申请人对象列表
    **/
    public void setApplicant(List<JrBankruptcyApplicant> applicant) {
      this.applicant = applicant;
    }
    /**
    *   获取 申请人对象列表
    **/
    public List<JrBankruptcyApplicant> getApplicant() {
      return applicant;
    }
    /**
    *   设置 管理人机构
    **/
    public void setManagerialAgency(String managerialAgency) {
      this.managerialAgency = managerialAgency;
    }
    /**
    *   获取 管理人机构
    **/
    public String getManagerialAgency() {
      return managerialAgency;
    }



}

