package com.gitee.deament.tianyancha.annotation;

import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.annotation.*;

/**
 * 用作天眼查DTO的类
 * @author  deament
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TYCURL {
    String value();

}
