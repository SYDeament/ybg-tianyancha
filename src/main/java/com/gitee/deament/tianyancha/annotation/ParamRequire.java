package com.gitee.deament.tianyancha.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ParamRequire {
    boolean require() default  false;
}
