package com.gitee.deament.tianyancha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableAspectJAutoProxy(proxyTargetClass = false)
public class TianyanchaApplication {
    public static void main(String[] args) {

        SpringApplication.run(TianyanchaApplication.class, args);

    }
}
