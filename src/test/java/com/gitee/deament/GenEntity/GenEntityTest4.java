package com.gitee.deament.GenEntity;

import com.gitee.deament.tianyancha.core.gencode.GenDTO;
import com.gitee.deament.tianyancha.core.gencode.GenEntity;
import com.gitee.deament.tianyancha.core.gencode.GenRequest;
import com.gitee.deament.tianyancha.core.gencode.GenRequestImpl;

public class GenEntityTest4 {

    public static void main(String[] args) {
//最短路径发现
        String id = "452";
        String url = "https://open.tianyancha.com/cloud-open-admin/_feign/interface/find.json?parentId=" + id + "&opened=1&pn=1&ps=50";
        System.out.println(url);
        gen(url);


    }

    private static void gen(String url) {
        GenEntity.gen(url);
        GenDTO.gen(url);
        GenRequest.gen(url);
        GenRequestImpl.gen(url);
    }
}
