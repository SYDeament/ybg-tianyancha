//package com.gitee.deament.GenEntity;
//
//import com.alibaba.fastjson.JSONObject;
//import com.gitee.deament.tianyancha.TianyanchaApplication;
//import com.gitee.deament.tianyancha.core.TianyanchaResult;
//import com.gitee.deament.tianyancha.core.remote.hiholder.dto.HiHolderDTO;
//import com.gitee.deament.tianyancha.core.remote.hiholder.request.HiHolderRequestImpl;
//import com.gitee.deament.tianyancha.core.remote.icbaseinfov2.dto.IcBaseinfoV2DTO;
//import com.gitee.deament.tianyancha.core.remote.icbaseinfov2.entity.IcBaseinfoV2;
//import com.gitee.deament.tianyancha.core.remote.icbaseinfov2.request.IcBaseinfoV2RequestImpl;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@SpringBootTest(classes = TianyanchaApplication.class)
//@RunWith(SpringRunner.class)
//public class Test1 {
//    @Autowired
//    IcBaseinfoV2RequestImpl hiHolderRequest;
//    @Test
//    public  void main() {
//        try {
//            IcBaseinfoV2DTO dto= new IcBaseinfoV2DTO();
//          //  dto.setKeyword("阿里巴巴");
//            TianyanchaResult<IcBaseinfoV2> icBaseinfoV2TianyanchaResult = hiHolderRequest.get(dto);
//            System.out.println("第一次 无缓存："+JSONObject.toJSONString(icBaseinfoV2TianyanchaResult,true));
//          icBaseinfoV2TianyanchaResult = hiHolderRequest.get(dto);
//            System.out.println("第二次 ："+JSONObject.toJSONString(icBaseinfoV2TianyanchaResult,true));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//}
