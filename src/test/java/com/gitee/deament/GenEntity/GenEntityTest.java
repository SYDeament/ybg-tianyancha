package com.gitee.deament.GenEntity;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gitee.deament.tianyancha.core.gencode.GenDTO;
import com.gitee.deament.tianyancha.core.gencode.GenEntity;
import com.gitee.deament.tianyancha.core.gencode.GenRequest;
import com.gitee.deament.tianyancha.core.gencode.GenRequestImpl;

import java.util.Iterator;

public class GenEntityTest {

    public static void main(String[] args) {
        String parent=HttpUtil.get("https://open.tianyancha.com/cloud-open-admin/interface/parent.json?opened=1");
        JSONArray parentArray = JSONObject.parseObject(parent).getJSONArray("data");
        Iterator<Object> iterator = parentArray.iterator();
        while (iterator.hasNext()){
            Object value=  iterator.next();
            JSONObject jsonObject = JSONObject.parseObject(JSONObject.toJSONString(value));
            String id=  jsonObject.getString("id");
            String url = "https://open.tianyancha.com/cloud-open-admin/_feign/interface/find.json?parentId="+id+"&opened=1&pn=1&ps=50";
            System.out.println(url);
            gen(url);
        }


    }

    private static void gen(String url){
        GenEntity.gen(url);
        GenDTO.gen(url);
        GenRequest.gen(url);
        GenRequestImpl.gen(url);
    }
}
