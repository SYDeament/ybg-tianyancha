package com.gitee.deament.GenEntity;

import com.gitee.deament.tianyancha.core.gencode.GenDTO;
import com.gitee.deament.tianyancha.core.gencode.GenEntity;
import com.gitee.deament.tianyancha.core.gencode.GenRequest;
import com.gitee.deament.tianyancha.core.gencode.GenRequestImpl;

public class GenEntityTest6 {

    public static void main(String[] args) {
//搜索 搞定了
        String id = "361";
        String url = "https://open.tianyancha.com/cloud-open-admin/_feign/interface/find.json?parentId=" + id + "&opened=1&pn=1&ps=50";
        System.out.println(url);
        gen(url);


    }

    private static void gen(String url) {
        GenEntity.gen(url);
        GenDTO.gen(url);
        GenRequest.gen(url);
        GenRequestImpl.gen(url);
    }
}
