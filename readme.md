#框架
springboot2



# 生成代码 
在test 里面找到
TianyanchaGen.properties 是代码生成的配置
```properties
#输出的目录
OutputDir=F:/gen/src/main/java
#没使用
tableName=
#没使用
className=BASE
#作者
author=deament
# 路径
parent=com.gitee.deament.tianyancha.core.remote
# 跳过生成代码的服务，用英文逗号分隔
excludeUrl=/services/v4/open/riskDetail,/services/v4/open/financialAnalysis,/services/v4/open/xgbaseinfoV2,/services/open/hi/ic/2.0,/services/open/rela/shortPath,/services/open/m/employments/2.0,/services/open/search/2.0,/services/open/oi/stats/2.0
```

GenEntityTest 类右键运行即可
可以生成百分之九十的接口 ，复杂的接口需要手写
# 运行
修改 application.properties 的spring.profiles.active值，改成dev
需要把application.properties 的 tianyancha.token=你天眼查的token
# 单元测试
见
\src\test\java\com\gitee\deament\GenEntity\Test1.java
```java

import com.gitee.deament.tianyancha.TianyanchaApplication;
import com.gitee.deament.tianyancha.core.remote.hiholder.dto.HiHolderDTO;
import com.gitee.deament.tianyancha.core.remote.hiholder.request.HiHolderRequestImpl;
import com.gitee.deament.tianyancha.core.remote.icbaseinfov2.dto.IcBaseinfoV2DTO;
import com.gitee.deament.tianyancha.core.remote.icbaseinfov2.request.IcBaseinfoV2RequestImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = TianyanchaApplication.class)
@RunWith(SpringRunner.class)
public class Test1 {
    @Autowired
    IcBaseinfoV2RequestImpl hiHolderRequest;
    @Test
    public  void main() {
        try {
            IcBaseinfoV2DTO dto= new IcBaseinfoV2DTO();
            dto.setKeyword("阿里巴巴");
            hiHolderRequest.get( dto);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
```
## 结果      
```json
{"error_code":0,"reason":"ok","result":{"historyNames":"阿里巴巴\t","regStatus":"存续","regCapital":"2人民币","historyNameList":["阿里巴巴"],"industry":"零售业","type":1,"legalPersonName":"周孔华","regNumber":"542421600010621","creditCode":"92542421MA6T2F1TXY","approvedTime":1523894400000,"actualCapitalCurrency":"人民币","alias":"阿里巴巴","companyOrgType":"个体工商户","id":3029865165,"orgNumber":"MA6T2F1TX","email":"","actualCapital":"","estiblishTime":1348416000000,"regInstitute":"色尼区市场监督管理局","businessScope":"服装、鞋、帽子。【依法须经批准项目，经相关部门批准后方可开展经营活动】","taxNumber":"92542421MA6T2F1TXY","regLocation":"那曲县辽宁路","regCapitalCurrency":"人民币","phoneNumber":"15988989779","name":"那曲阿里巴巴","percentileScore":1781,"industryAll":{"categoryMiddle":"  纺织、服装及日用品专门零售","categoryBig":"零售业","category":"批发和零售业","categorySmall":""},"isMicroEnt":1,"base":"xz"}}
```

# AOP个性化拦截
植入了AOP 进行您的业务扩展
com.gitee.deament.tianyancha.core.aop.HttpRequestAop.java 中进行自己的业务扩展,代码入侵小
```java
@Around("execution(* com.gitee.deament.tianyancha.core.base.BaseRequest.get(..))")
    public void around(ProceedingJoinPoint pj){
        try {
            logger.info("环绕预备");
            //XXX TODO 此处可以添加接口的拦截代码，比如次数限制 缓存等
            Object[] args = pj.getArgs();
            pj.proceed();
            logger.info("环绕完成");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        logger.info("环绕");
    }

```

# 莫要私信 莫要私信 莫要私信